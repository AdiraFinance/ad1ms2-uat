class ShowMandatoryInfoCollaOtoModel {
  final bool isCollateralTypeVisible;
  final bool isRadioValueIsCollaNameSameWithApplicantOtoVisible;
  final bool isNameOnCollateralAutoVisible;
  final bool isIdentityTypeSelectedAutoVisible;
  final bool isIdentityNumberAutoVisible;
  final bool isBirthDateAutoVisible;
  final bool isBirthPlaceValidWithIdentityAutoVisible;
  final bool isBirthPlaceValidWithIdentityLOVAutoVisible;
  final bool isRadioValueIsCollateralSameWithUnitOtoVisible;
  final bool isGroupObjectVisible;
  final bool isObjectVisible;
  final bool isTypeProductVisible;
  final bool isBrandObjectVisible;
  final bool isObjectTypeVisible;
  final bool isModelObjectVisible;
  final bool isUsageObjectModelVisible;
  final bool isYearProductionSelectedVisible;
  final bool isYearRegistrationSelectedVisible;
  final bool isRadioValueYellowPlatVisible;
  final bool isRadioValueBuiltUpNonATPMVisible;
  final bool isPoliceNumberVisible;
  final bool isFrameNumberVisible;
  final bool isMachineNumberVisible;
  final bool isBPKPNumberVisible;
  final bool isGradeUnitVisible;
  final bool isFasilitasUTJVisible;
  final bool isNamaBidderVisible;
  final bool isHargaJualShowroomVisible;
  final bool isPerlengkapanTambahanVisible;
  final bool isMPAdiraVisible;
  final bool isRekondisiFisikVisible;
  final bool isDpGuaranteeVisible;
  final bool isPHMaxAutomotiveVisible;
  final bool isTaksasiPriceAutomotiveVisible;
  final bool isRadioValueWorthyOrUnworthyVisible;
  final bool isUsageCollateralOtoVisible;
  final bool isRadioValueForAllUnitOtoVisible;
  final bool isCollateralTypeMandatory;
  final bool isRadioValueIsCollaNameSameWithApplicantOtoMandatory;
  final bool isNameOnCollateralAutoMandatory;
  final bool isIdentityTypeSelectedAutoMandatory;
  final bool isIdentityNumberAutoMandatory;
  final bool isBirthDateAutoMandatory;
  final bool isBirthPlaceValidWithIdentityAutoMandatory;
  final bool isBirthPlaceValidWithIdentityLOVAutoMandatory;
  final bool isRadioValueIsCollateralSameWithUnitOtoMandatory;
  final bool isGroupObjectMandatory;
  final bool isObjectMandatory;
  final bool isTypeProductMandatory;
  final bool isBrandObjectMandatory;
  final bool isObjectTypeMandatory;
  final bool isModelObjectMandatory;
  final bool isUsageObjectModelMandatory;
  final bool isYearProductionSelectedMandatory;
  final bool isYearRegistrationSelectedMandatory;
  final bool isRadioValueYellowPlatMandatory;
  final bool isRadioValueBuiltUpNonATPMMandatory;
  final bool isPoliceNumberMandatory;
  final bool isFrameNumberMandatory;
  final bool isMachineNumberMandatory;
  final bool isBPKPNumberMandatory;
  final bool isGradeUnitMandatory;
  final bool isFasilitasUTJMandatory;
  final bool isNamaBidderMandatory;
  final bool isHargaJualShowroomMandatory;
  final bool isPerlengkapanTambahanMandatory;
  final bool isMPAdiraMandatory;
  final bool isRekondisiFisikMandatory;
  final bool isDpGuaranteeMandatory;
  final bool isPHMaxAutomotiveMandatory;
  final bool isTaksasiPriceAutomotiveMandatory;
  final bool isRadioValueWorthyOrUnworthyMandatory;
  final bool isUsageCollateralOtoMandatory;
  final bool isRadioValueForAllUnitOtoMandatory;

  ShowMandatoryInfoCollaOtoModel(
      this.isCollateralTypeVisible,
      this.isRadioValueIsCollaNameSameWithApplicantOtoVisible,
      this.isIdentityTypeSelectedAutoVisible,
      this.isIdentityNumberAutoVisible,
      this.isNameOnCollateralAutoVisible,
      this.isBirthDateAutoVisible,
      this.isBirthPlaceValidWithIdentityAutoVisible,
      this.isBirthPlaceValidWithIdentityLOVAutoVisible,
      this.isRadioValueIsCollateralSameWithUnitOtoVisible,
      this.isGroupObjectVisible,
      this.isObjectVisible,
      this.isTypeProductVisible,
      this.isBrandObjectVisible,
      this.isObjectTypeVisible,
      this.isModelObjectVisible,
      this.isUsageObjectModelVisible,
      this.isYearProductionSelectedVisible,
      this.isYearRegistrationSelectedVisible,
      this.isRadioValueYellowPlatVisible,
      this.isRadioValueBuiltUpNonATPMVisible,
      this.isPoliceNumberVisible,
      this.isFrameNumberVisible,
      this.isMachineNumberVisible,
      this.isBPKPNumberVisible,
      this.isGradeUnitVisible,
      this.isFasilitasUTJVisible,
      this.isNamaBidderVisible,
      this.isHargaJualShowroomVisible,
      this.isPerlengkapanTambahanVisible,
      this.isMPAdiraVisible,
      this.isRekondisiFisikVisible,
      this.isDpGuaranteeVisible,
      this.isPHMaxAutomotiveVisible,
      this.isTaksasiPriceAutomotiveVisible,
      this.isRadioValueWorthyOrUnworthyVisible,
      this.isUsageCollateralOtoVisible,
      this.isRadioValueForAllUnitOtoVisible,
      this.isCollateralTypeMandatory,
      this.isRadioValueIsCollaNameSameWithApplicantOtoMandatory,
      this.isIdentityTypeSelectedAutoMandatory,
      this.isIdentityNumberAutoMandatory,
      this.isNameOnCollateralAutoMandatory,
      this.isBirthDateAutoMandatory,
      this.isBirthPlaceValidWithIdentityAutoMandatory,
      this.isBirthPlaceValidWithIdentityLOVAutoMandatory,
      this.isRadioValueIsCollateralSameWithUnitOtoMandatory,
      this.isGroupObjectMandatory,
      this.isObjectMandatory,
      this.isTypeProductMandatory,
      this.isBrandObjectMandatory,
      this.isObjectTypeMandatory,
      this.isModelObjectMandatory,
      this.isUsageObjectModelMandatory,
      this.isYearProductionSelectedMandatory,
      this.isYearRegistrationSelectedMandatory,
      this.isRadioValueYellowPlatMandatory,
      this.isRadioValueBuiltUpNonATPMMandatory,
      this.isPoliceNumberMandatory,
      this.isFrameNumberMandatory,
      this.isMachineNumberMandatory,
      this.isBPKPNumberMandatory,
      this.isGradeUnitMandatory,
      this.isFasilitasUTJMandatory,
      this.isNamaBidderMandatory,
      this.isHargaJualShowroomMandatory,
      this.isPerlengkapanTambahanMandatory,
      this.isMPAdiraMandatory,
      this.isRekondisiFisikMandatory,
      this.isDpGuaranteeMandatory,
      this.isPHMaxAutomotiveMandatory,
      this.isTaksasiPriceAutomotiveMandatory,
      this.isRadioValueWorthyOrUnworthyMandatory,
      this.isUsageCollateralOtoMandatory,
      this.isRadioValueForAllUnitOtoMandatory);
}