class ShowMandatoryInfoStrukturKreditSteppingModel {
  final bool isTotalSteppingSelectedVisible;
  final bool isPercentageVisible;
  final bool isValueVisible;
  final bool isTotalSteppingSelectedMandatory;
  final bool isPercentageMandatory;
  final bool isValueMandatory;

  ShowMandatoryInfoStrukturKreditSteppingModel(
    this.isTotalSteppingSelectedVisible,
    this.isPercentageVisible,
    this.isValueVisible,
    this.isTotalSteppingSelectedMandatory,
    this.isPercentageMandatory,
    this.isValueMandatory);
}