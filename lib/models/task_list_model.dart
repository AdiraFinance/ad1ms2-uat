class TaskListModel{
  final String ORDER_NO;
  final DateTime ORDER_DATE;
  final String CUST_NAME;
  final String CUST_TYPE;
  final String LAST_KNOWN_STATE;
  final String LAST_KNOWN_HANDLED_BY;
  final String IDX;
  final String SOURCE_APPLICATION;
  final String PRIORITY;
  final String OID;
  final String STATUS_AORO;
  final DateTime MS2PROCESS_DATE;
  final String JENIS_PENAWARAN;
  final String NO_REFERENSI;
  final String OPSI_MULTIDISBURSE;

  TaskListModel(this.ORDER_NO, this.ORDER_DATE, this.CUST_NAME, this.CUST_TYPE, this.LAST_KNOWN_STATE, this.LAST_KNOWN_HANDLED_BY, this.IDX, this.SOURCE_APPLICATION, this.PRIORITY, this.OID, this.STATUS_AORO, this.MS2PROCESS_DATE, this.JENIS_PENAWARAN, this.NO_REFERENSI, this.OPSI_MULTIDISBURSE);
}