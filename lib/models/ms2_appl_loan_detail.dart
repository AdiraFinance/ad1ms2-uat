class MS2ApplLoanDetailModel {
  final String ac_installment_id;
  final int ac_installment_no;
  final int ac_percentage;
  final double ac_amount;
  final int active;
  final String created_date;
  final String created_by;
  final String modified_date;
  final String modified_by;

  MS2ApplLoanDetailModel (
      this.ac_installment_id,
      this.ac_installment_no,
      this.ac_percentage,
      this.ac_amount,
      this.active,
      this.created_date,
      this.created_by,
      this.modified_date,
      this.modified_by,
      );
}