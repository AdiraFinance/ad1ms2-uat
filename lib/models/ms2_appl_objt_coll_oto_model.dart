class MS2ApplObjtCollOtoModel {
  final String colla_oto_id;
  final String collateralID;
  final int flag_multi_coll;
  final int flag_coll_is_unit;
  final int flag_coll_name_is_appl;
  final String id_type;
  final String id_type_desc;
  final String id_no;
  final String colla_name;
  final String date_of_birth;
  final String place_of_birth;
  final String place_of_birth_kabkota;
  final String place_of_birth_kabkota_desc;
  final String group_object;
  final String group_object_desc;
  final String object;
  final String object_desc;
  final String product_type;
  final String product_type_desc;
  final String brand_object;
  final String brand_object_desc;
  final String object_type;
  final String object_type_desc;
  final String object_model;
  final String object_model_desc;
  final String object_purpose;
  final String object_purpose_desc;
  final int mfg_year;
  final int registration_year;
  final int yellow_plate;
  final int built_up;
  final String bpkp_no;
  final String frame_no;
  final String engine_no;
  final String police_no;
  final String grade_unit;
  final String utj_facilities;
  final String bidder_name;
  final double showroom_price;
  final String add_equip;
  final double mp_adira;
  final double rekondisi_fisik;
  final int result;
  final int ltv;
  final double dp_jaminan;
  final double max_ph;
  final double taksasi_price;
  final String cola_purpose;
  final String cola_purpose_desc;
  final int capacity;
  final String color;
  final String made_in;
  final String made_in_desc;
  final String serial_no;
  final String no_invoice;
  final String stnk_valid;
  final String bpkp_address;
  final int active;
  final String created_date;
  final String created_by;
  final String modified_date;
  final String modified_by;
  final String bpkb_id_type;
  final String bpkb_id_type_desc;
  final int mp_adira_upld;
  final String edit_flag_multi_coll;
  final String edit_flag_coll_is_unit;
  final String edit_flag_coll_name_is_appl;
  final String edit_id_type;
  final String edit_id_no;
  final String edit_colla_name;
  final String edit_date_of_birth;
  final String edit_place_of_birth;
  final String edit_place_of_birth_kabkota;
  final String edit_group_object;
  final String edit_object;
  final String edit_product_type;
  final String edit_brand_object;
  final String edit_object_type;
  final String edit_object_model;
  final String edit_object_purpose;
  final String edit_mfg_year;
  final String edit_registration_year;
  final String edit_yellow_plate;
  final String edit_built_up;
  final String edit_bpkp_no;
  final String edit_frame_no;
  final String edit_engine_no;
  final String edit_police_no;
  final String edit_grade_unit;
  final String edit_utj_facilities;
  final String edit_bidder_name;
  final String edit_showroom_price;
  final String edit_add_equip;
  final String edit_mp_adira;
  final String edit_mp_adira_upld;
  final String edit_rekondisi_fisik;
  final String edit_result;
  final String edit_ltv;
  final String edit_dp_jaminan;
  final String edit_max_ph;
  final String edit_taksasi_price;
  final String edit_cola_purpose;
  final String edit_capacity;
  final String edit_color;
  final String edit_made_in;
  final String edit_serial_no;
  final String edit_no_invoice;
  final String edit_stnk_valid;
  final String edit_bpkp_address;
  final String edit_bpkb_id_type;

  MS2ApplObjtCollOtoModel (
      this.colla_oto_id,
      this.collateralID,
      this.flag_multi_coll,
      this.flag_coll_is_unit,
      this.flag_coll_name_is_appl,
      this.id_type,
      this.id_type_desc,
      this.id_no,
      this.colla_name,
      this.date_of_birth,
      this.place_of_birth,
      this.place_of_birth_kabkota,
      this.place_of_birth_kabkota_desc,
      this.group_object,
      this.group_object_desc,
      this.object,
      this.object_desc,
      this.product_type,
      this.product_type_desc,
      this.brand_object,
      this.brand_object_desc,
      this.object_type,
      this.object_type_desc,
      this.object_model,
      this.object_model_desc,
      this.object_purpose,
      this.object_purpose_desc,
      this.mfg_year,
      this.registration_year,
      this.yellow_plate,
      this.built_up,
      this.bpkp_no,
      this.frame_no,
      this.engine_no,
      this.police_no,
      this.grade_unit,
      this.utj_facilities,
      this.bidder_name,
      this.showroom_price,
      this.add_equip,
      this.mp_adira,
      this.rekondisi_fisik,
      this.result,
      this.ltv,
      this.dp_jaminan,
      this.max_ph,
      this.taksasi_price,
      this.cola_purpose,
      this.cola_purpose_desc,
      this.capacity,
      this.color,
      this.made_in,
      this.made_in_desc,
      this.serial_no,
      this.no_invoice,
      this.stnk_valid,
      this.bpkp_address,
      this.active,
      this.created_date,
      this.created_by,
      this.modified_date,
      this.modified_by,
      this.bpkb_id_type,
      this.bpkb_id_type_desc,
      this.mp_adira_upld,
      this.edit_flag_multi_coll,
      this.edit_flag_coll_is_unit,
      this.edit_flag_coll_name_is_appl,
      this.edit_id_type,
      this.edit_id_no,
      this.edit_colla_name,
      this.edit_date_of_birth,
      this.edit_place_of_birth,
      this.edit_place_of_birth_kabkota,
      this.edit_group_object,
      this.edit_object,
      this.edit_product_type,
      this.edit_brand_object,
      this.edit_object_type,
      this.edit_object_model,
      this.edit_object_purpose,
      this.edit_mfg_year,
      this.edit_registration_year,
      this.edit_yellow_plate,
      this.edit_built_up,
      this.edit_bpkp_no,
      this.edit_frame_no,
      this.edit_engine_no,
      this.edit_police_no,
      this.edit_grade_unit,
      this.edit_utj_facilities,
      this.edit_bidder_name,
      this.edit_showroom_price,
      this.edit_add_equip,
      this.edit_mp_adira,
      this.edit_mp_adira_upld,
      this.edit_rekondisi_fisik,
      this.edit_result,
      this.edit_ltv,
      this.edit_dp_jaminan,
      this.edit_max_ph,
      this.edit_taksasi_price,
      this.edit_cola_purpose,
      this.edit_capacity,
      this.edit_color,
      this.edit_made_in,
      this.edit_serial_no,
      this.edit_no_invoice,
      this.edit_stnk_valid,
      this.edit_bpkp_address,
      this.edit_bpkb_id_type,
      );
}