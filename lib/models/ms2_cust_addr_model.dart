class MS2CustAddrModel {
  final String order_no;
  final String addressID;
  final String foreignBusinessID;
  final String id_no;
  final String koresponden;
  final String matrix_addr;
  final String address;
  final String rt;
  final String rt_desc;
  final String rw;
  final String rw_desc;
  final String provinsi;
  final String provinsi_desc;
  final String kabkot;
  final String kabkot_desc;
  final String kecamatan;
  final String kecamatan_desc;
  final String kelurahan;
  final String kelurahan_desc;
  final String zip_code;
  final String handphone_no;
  final String phone1;
  final String phone1_area;
  final String phone_2;
  final String phone_2_area;
  final String fax;
  final String fax_area;
  final String addr_type;
  final String addr_desc;
  final String latitude;
  final String longitude;
  final String addr_from_map;
  final String created_date;
  final String created_by;
  final String modified_date;
  final String modified_by;
  final int active;
  final String edit_addr_type;
  final String edit_address;
  final String edit_rt;
  final String edit_rw;
  final String edit_kelurahan;
  final String edit_kecamatan;
  final String edit_kabkot;
  final String edit_provinsi;
  final String edit_zip_code;
  final String edit_phone1_area;
  final String edit_phone1;
  final String edit_phone_2_area;
  final String edit_phone_2;
  final String edit_fax_area;
  final String edit_fax;
  final String edit_address_from_map;

  MS2CustAddrModel(
    this.order_no,
    this.addressID,
    this.foreignBusinessID,
    this.id_no,
    this.koresponden,
    this.matrix_addr,
    this.address,
    this.rt,
    this.rt_desc,
    this.rw,
    this.rw_desc,
    this.provinsi,
    this.provinsi_desc,
    this.kabkot,
    this.kabkot_desc,
    this.kecamatan,
    this.kecamatan_desc,
    this.kelurahan,
    this.kelurahan_desc,
    this.zip_code,
    this.handphone_no,
    this.phone1,
    this.phone1_area,
    this.phone_2,
    this.phone_2_area,
    this.fax,
    this.fax_area,
    this.addr_type,
    this.addr_desc,
    this.latitude,
    this.longitude,
    this.addr_from_map,
    this.created_date,
    this.created_by,
    this.modified_date,
    this.modified_by,
    this.active,
    this.edit_addr_type,
    this.edit_address,
    this.edit_rt,
    this.edit_rw,
    this.edit_kelurahan,
    this.edit_kecamatan,
    this.edit_kabkot,
    this.edit_provinsi,
    this.edit_zip_code,
    this.edit_phone1_area,
    this.edit_phone1,
    this.edit_phone_2_area,
    this.edit_phone_2,
    this.edit_fax_area,
    this.edit_fax,
    this.edit_address_from_map,
  );
}
