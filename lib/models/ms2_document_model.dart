class MS2DocumentModel {
  final String ac_appl_objt_id;
  final String orderSupportingDocumentID;
  final String ac_appl_no;
  final String file_header_id;
  final String file_name;
  final String created_date;
  final String created_by;
  final String modified_date;
  final String modified_by;
  final String document_type_id;
  final String document_type_desc;
  final int mandatory;
  final int display;
  final int flag_unit;
  final String upload_date;
  final String latitude;
  final String longitude;

  MS2DocumentModel(
      this.ac_appl_objt_id,
      this.orderSupportingDocumentID,
      this.ac_appl_no,
      this.file_header_id,
      this.file_name,
      this.created_date,
      this.created_by,
      this.modified_date,
      this.modified_by,
      this.document_type_id,
      this.document_type_desc,
      this.mandatory,
      this.display,
      this.flag_unit,
      this.upload_date,
      this.latitude,
      this.longitude,
      );
}
