class ShowMandatoryFotoModel {
  final bool isFotoTempatTinggalVisible;
  final bool isOccupationVisible;
  final bool isFotoTempatUsahaVisible;
  final bool istypeOfFinancingModelVisible;
  final bool iskegiatanUsahaVisible;
  final bool isJenisKegiatanUsahaVisible;
  final bool isJenisKonsepVisible;
  final bool isListGroupUnitObjectVisible;
  final bool isFotoGroupUnitObjectVisible;
  final bool isListDocumentVisible;
  final bool isFotoTempatTinggalMandatory;
  final bool isOccupationMandatory;
  final bool isFotoTempatUsahaMandatory;
  final bool istypeOfFinancingModelMandatory;
  final bool iskegiatanUsahaMandatory;
  final bool isJenisKegiatanUsahaMandatory;
  final bool isJenisKonsepMandatory;
  final bool isListGroupUnitObjectMandatory;
  final bool isFotoGroupUnitObjectMandatory;
  final bool isListDocumentMandatory;

  ShowMandatoryFotoModel(
      this.isFotoTempatTinggalVisible,
      this.isOccupationVisible,
      this.isFotoTempatUsahaVisible,
      this.istypeOfFinancingModelVisible,
      this.iskegiatanUsahaVisible,
      this.isJenisKegiatanUsahaVisible,
      this.isJenisKonsepVisible,
      this.isListGroupUnitObjectVisible,
      this.isFotoGroupUnitObjectVisible,
      this.isListDocumentVisible,
      this.isFotoTempatTinggalMandatory,
      this.isOccupationMandatory,
      this.isFotoTempatUsahaMandatory,
      this.istypeOfFinancingModelMandatory,
      this.iskegiatanUsahaMandatory,
      this.isJenisKegiatanUsahaMandatory,
      this.isJenisKonsepMandatory,
      this.isListGroupUnitObjectMandatory,
      this.isFotoGroupUnitObjectMandatory,
      this.isListDocumentMandatory);
  }