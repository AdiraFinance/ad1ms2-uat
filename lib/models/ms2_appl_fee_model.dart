class MS2ApplFeeModel {
  final String fee_id;
  final String orderFeeID;
  final String fee_type;
  final String fee_type_desc;
  final double fee_cash;
  final double fee_credit;
  final double total_fee;
  final int active;
  final String created_date;
  final String created_by;
  final String modified_date;
  final String modified_by;
  final String editFeeType;
  final String editFeeCash;
  final String editFeeCredit;
  final String editTotalFee;

  MS2ApplFeeModel(
    this.fee_id,
    this.orderFeeID,
    this.fee_type,
    this.fee_type_desc,
    this.fee_cash,
    this.fee_credit,
    this.total_fee,
    this.active,
    this.created_date,
    this.created_by,
    this.modified_date,
    this.modified_by,
    this.editFeeType,
    this.editFeeCash,
    this.editFeeCredit,
    this.editTotalFee,
  );
}
