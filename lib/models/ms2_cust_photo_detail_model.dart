class MS2CustPhotoDetailModel {
  final String photo_id;
  final String order_no;
  final int index;
  final String residence_photo;
  final String business_photo;
  final String group_unit_photo;
  final String document_unit_photo;
  final String created_date;
  final String created_by;
  final String modified_date;
  final String modified_by;
  final int active;

  MS2CustPhotoDetailModel(
      this.photo_id,
      this.order_no,
      this.index,
      this.residence_photo,
      this.business_photo,
      this.group_unit_photo,
      this.document_unit_photo,
      this.created_date,
      this.created_by,
      this.modified_date,
      this.modified_by,
      this.active,
      );
}
