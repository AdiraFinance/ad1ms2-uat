class DataCLModel {
  final String jenis_kegiatan_usaha;
  final String biaya_hari_berjalan;
  final String biaya_admin_angsuran;
  final String biaya_simpan_bpkb;
  final String biaya_admin_restruktur;
  final String biaya_total;
  final String sisa_ph;
  final String denda;
  final String sisa_tenor;
  final String jumlah_pelunasan;
  final String jumlah_tunggakan;
  final String bunga_hari_berjalan;
  final String bunga_dihapuskan;
  final String pinalty_plus;
  final String max_plafon_cl;
  final String nilai_aktivasi;
  final String tenor_max;
  final String tenor;
  final String min_pencairan;
  final String biaya_fidusia;
  final String biaya_asuransi;
  final String biaya_administrasi;
  final String total_kewajiban;
  final String total_titipan;
  final String total_bayar;
  final String total_pencairan;

  DataCLModel(
      this.jenis_kegiatan_usaha,
      this.biaya_hari_berjalan,
      this.biaya_admin_angsuran,
      this.biaya_simpan_bpkb,
      this.biaya_admin_restruktur,
      this.biaya_total,
      this.sisa_ph,
      this.denda,
      this.sisa_tenor,
      this.jumlah_pelunasan,
      this.jumlah_tunggakan,
      this.bunga_hari_berjalan,
      this.bunga_dihapuskan,
      this.pinalty_plus,
      this.max_plafon_cl,
      this.nilai_aktivasi,
      this.tenor_max,
      this.tenor,
      this.min_pencairan,
      this.biaya_fidusia,
      this.biaya_asuransi,
      this.biaya_administrasi,
      this.total_kewajiban,
      this.total_titipan,
      this.total_bayar,
      this.total_pencairan,
      );
}