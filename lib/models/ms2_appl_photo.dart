import 'package:ad1ms2_dev/models/type_of_financing_model.dart';
import 'package:ad1ms2_dev/shared/document_unit_model.dart';
import 'package:ad1ms2_dev/shared/group_unit_object_model.dart';

import 'form_m_foto_model.dart';

class MS2ApplPhotoModel{
    List<ImageFileModel> listFotoTempatTinggal;
    OccupationModel occupation;
    List<ImageFileModel> listFotoTempatUsaha;
    FinancingTypeModel financial;
    KegiatanUsahaModel kegiatanUsaha;
    JenisKegiatanUsahaModel jenisKegiatanUsaha;
    JenisKonsepModel jenisKonsep;
    List<GroupUnitObjectModel> listGroupUnitObject;
    List<DocumentUnitModel> listDocument;
    String edit_listFotoTempatTinggal;
    String edit_occupation;
    String edit_listFotoTempatUsaha;
    String edit_financial;
    String edit_kegiatanUsaha;
    String edit_jenisKegiatanUsaha;
    String edit_jenisKonsep;
    String edit_listGroupUnitObject;
    String edit_listDocument;

    MS2ApplPhotoModel(
      this.listFotoTempatTinggal,
      this.occupation,
      this.listFotoTempatUsaha,
      this.financial,
      this.kegiatanUsaha,
      this.jenisKegiatanUsaha,
      this.jenisKonsep,
      this.listGroupUnitObject,
      this.listDocument,
      this.edit_listFotoTempatTinggal,
      this.edit_occupation,
      this.edit_listFotoTempatUsaha,
      this.edit_financial,
      this.edit_kegiatanUsaha,
      this.edit_jenisKegiatanUsaha,
      this.edit_jenisKonsep,
      this.edit_listGroupUnitObject,
      this.edit_listDocument);
}