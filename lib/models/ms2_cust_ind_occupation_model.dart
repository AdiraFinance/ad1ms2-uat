class MS2CustIndOccupationModel {
  final String order_no;
  final String customerOccupationID;
  final String occupation;
  final String occupation_desc;
  final String buss_name;
  final String buss_type;
  final String pep_type;
  final String pep_desc;
  final String emp_status;
  final String emp_status_desc;
  final String profession_type;
  final String profession_desc;
  final String sector_economic;
  final String sector_economic_desc;
  final String nature_of_buss;
  final String nature_of_buss_desc;
  final String location_status;
  final String location_status_desc;
  final String buss_location;
  final String buss_location_desc;
  final int total_emp;
  final int no_of_year_work;
  final int no_of_year_buss;
  final String created_date;
  final String created_by;
  final String modified_date;
  final String modified_by;
  final int active;
  final String siup_no;
  final String scheme_kur;
  final int modal_usaha;
  final String edit_occupation;
  final String edit_buss_name;
  final String edit_buss_type;
  final String edit_pep_type;
  final String edit_emp_status;
  final String edit_profession_type;
  final String edit_sector_economic;
  final String edit_nature_of_buss;
  final String edit_location_status;
  final String edit_buss_location;
  final String edit_total_emp;
  final String edit_no_of_year_work;
  final String edit_no_of_year_buss;
  final String edit_siup_no;
  final String edit_scheme_kur;
  final String edit_modal_usaha;

  MS2CustIndOccupationModel(
    this.order_no,
    this.customerOccupationID,
    this.occupation,
    this.occupation_desc,
    this.buss_name,
    this.buss_type,
    this.pep_type,
    this.pep_desc,
    this.emp_status,
    this.emp_status_desc,
    this.profession_type,
    this.profession_desc,
    this.sector_economic,
    this.sector_economic_desc,
    this.nature_of_buss,
    this.nature_of_buss_desc,
    this.location_status,
    this.location_status_desc,
    this.buss_location,
    this.buss_location_desc,
    this.total_emp,
    this.no_of_year_work,
    this.no_of_year_buss,
    this.created_date,
    this.created_by,
    this.modified_date,
    this.modified_by,
    this.active,
    this.siup_no,
    this.scheme_kur,
    this.modal_usaha,
    this.edit_occupation,
    this.edit_buss_name,
    this.edit_buss_type,
    this.edit_pep_type,
    this.edit_emp_status,
    this.edit_profession_type,
    this.edit_sector_economic,
    this.edit_nature_of_buss,
    this.edit_location_status,
    this.edit_buss_location,
    this.edit_total_emp,
    this.edit_no_of_year_work,
    this.edit_no_of_year_buss,
    this.edit_siup_no,
    this.edit_scheme_kur,
    this.edit_modal_usaha,
  );
}
