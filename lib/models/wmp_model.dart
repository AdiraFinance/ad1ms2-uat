class WMPModel {
  final String amount; // max_refund_amt
  final String deskripsi; // wmp_subsidy_type_id_desc
  final String deskripsiProposal; // wmp_type_desc
  final String isHidden;
  final String noProposal; // wmp_no
  final String type; // wmp_type
  final String typeSubsidi; // wmp_subsidy_type_id
  final String orderWmpID; // wmp_id
  bool isEditNoProposal;
  bool isEditType;
  bool isEditTypeSubsidi;
  bool isEditAmount;

  WMPModel(this.amount, this.deskripsi, this.deskripsiProposal, this.isHidden, this.noProposal, this.type, this.typeSubsidi, this.orderWmpID, this.isEditNoProposal, this.isEditType, this.isEditTypeSubsidi, this.isEditAmount);
}