class MS2CustFamilyModel {
  final String order_no;
  final String familyInfoID;
  final String relation_status;
  final String relation_status_desc;
  final String full_name_id;
  final String full_name;
  final String id_no;
  final String date_of_birth;
  final String place_of_birth;
  final String place_of_birth_kabkota;
  final String place_of_birth_kabkota_desc;
  final String gender;
  final String gender_desc;
  final String degree;
  final String id_type;
  final String id_desc;
  final String created_date;
  final String created_by;
  final String modified_date;
  final String modified_by;
  final int active;
  final int dedup_score;
  final String phone1;
  final String phone1_area;
  final String handphone_no;
  final String edit_relation_status;
  final String edit_full_name_id;
  final String edit_full_name;
  final String edit_id_no;
  final String edit_date_of_birth;
  final String edit_place_of_birth_kabkota;
  final String edit_place_of_birth;
  final String edit_gender;
  final String edit_degree;
  final String edit_id_type;
  final String edit_dedup_score;
  final String edit_phone1;
  final String edit_phone1_area;
  final String edit_handphone_no;

  MS2CustFamilyModel(
      this.order_no,
      this.familyInfoID,
      this.relation_status,
      this.relation_status_desc,
      this.full_name_id,
      this.full_name,
      this.id_no,
      this.date_of_birth,
      this.place_of_birth,
      this.place_of_birth_kabkota,
      this.place_of_birth_kabkota_desc,
      this.gender,
      this.gender_desc,
      this.degree,
      this.id_type,
      this.id_desc,
      this.created_date,
      this.created_by,
      this.modified_date,
      this.modified_by,
      this.active,
      this.dedup_score,
      this.phone1,
      this.phone1_area,
      this.handphone_no,
      this.edit_relation_status,
      this.edit_full_name_id,
      this.edit_full_name,
      this.edit_id_no,
      this.edit_date_of_birth,
      this.edit_place_of_birth_kabkota,
      this.edit_place_of_birth,
      this.edit_gender,
      this.edit_degree,
      this.edit_id_type,
      this.edit_dedup_score,
      this.edit_phone1,
      this.edit_phone1_area,
      this.edit_handphone_no,
      );
}
