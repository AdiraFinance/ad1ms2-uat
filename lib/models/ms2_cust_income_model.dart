class MS2CustIncomeModel {
  final String cust_income_id;
  final String customerIncomeID;
  final String type_income_frml;
  final String income_type;
  final String income_desc;
  final String income_value;
  final String created_date;
  final String created_by;
  final String modified_date;
  final String modified_by;
  final int active;
  final int idx;
  final String edit_income_value;

  MS2CustIncomeModel(
      this.cust_income_id,
      this.customerIncomeID,
      this.type_income_frml,
      this.income_type,
      this.income_desc,
      this.income_value,
      this.created_date,
      this.created_by,
      this.modified_date,
      this.modified_by,
      this.active,
      this.idx,
      this.edit_income_value,
      );
}
