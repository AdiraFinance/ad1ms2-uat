class ShowMandatoryInfoStrukturKreditBPModel {
  final bool isRadioValueBalloonPHOrBalloonInstallmentVisible;
  final bool isInstallmentPercentageVisible;
  final bool isInstallmentValueVisible;
  final bool isRadioValueBalloonPHOrBalloonInstallmentMandatory;
  final bool isInstallmentPercentageMandatory;
  final bool isInstallmentValueMandatory;

  ShowMandatoryInfoStrukturKreditBPModel(
    this.isRadioValueBalloonPHOrBalloonInstallmentVisible,
    this.isInstallmentPercentageVisible,
    this.isInstallmentValueVisible,
    this.isRadioValueBalloonPHOrBalloonInstallmentMandatory,
    this.isInstallmentPercentageMandatory,
    this.isInstallmentValueMandatory);
}