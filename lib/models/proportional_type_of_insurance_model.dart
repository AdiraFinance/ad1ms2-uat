class ProportionalTypeOfInsuranceModel {
  final String kode;
  final String description;

  ProportionalTypeOfInsuranceModel(this.kode, this.description);
}
