import 'package:ad1ms2_dev/models/employee_head_model.dart';
import 'package:ad1ms2_dev/models/employee_model.dart';
import 'package:ad1ms2_dev/models/position_model.dart';
import 'package:ad1ms2_dev/models/salesman_type_model.dart';

class InformationSalesModel {
  final SalesmanTypeModel salesmanTypeModel;
  final PositionModel positionModel;
  final EmployeeModel employeeModel;
  final EmployeeHeadModel employeeHeadModel;
  final bool isEdit;
  final bool isSalesmanTypeModelChanges;
  final bool isPositionModelChanges;
  final bool isEmployeeChanges;
  final bool isEmployeeHeadModelChanges;
  final String orderProductSalesID;

  InformationSalesModel(
      this.salesmanTypeModel,
      this.positionModel,
      this.employeeModel,
      this.employeeHeadModel,
      this.isEdit,
      this.isSalesmanTypeModelChanges,
      this.isPositionModelChanges,
      this.isEmployeeChanges,
      this.isEmployeeHeadModelChanges,
      this.orderProductSalesID);
}
