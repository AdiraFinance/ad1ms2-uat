class SetGuarantorIndividu {
  final bool relationshipStatusVisibility;
  final bool identityTypeVisibility;
  final bool identityNoVisibility;
  final bool fullnameIDVisibility;
  final bool fullNameVisibility;
  final bool dateOfBirthVisibility;
  final bool placeOfBirthVisibility;
  final bool placeOfBirthLOVVisibility;
  final bool genderVisibility;
  final bool phoneVisibility;

  final bool relationshipStatusMandatory;
  final bool identityTypeMandatory;
  final bool identityNoMandatory;
  final bool fullnameIDMandatory;
  final bool fullNameMandatory;
  final bool dateOfBirthMandatory;
  final bool placeOfBirthMandatory;
  final bool placeOfBirthLOVMandatory;
  final bool genderMandatory;
  final bool phoneMandatory;

  SetGuarantorIndividu(
      this.relationshipStatusVisibility,
      this.identityTypeVisibility,
      this.identityNoVisibility,
      this.fullnameIDVisibility,
      this.fullNameVisibility,
      this.dateOfBirthVisibility,
      this.placeOfBirthVisibility,
      this.placeOfBirthLOVVisibility,
      this.genderVisibility,
      this.phoneVisibility,
      this.relationshipStatusMandatory,
      this.identityTypeMandatory,
      this.identityNoMandatory,
      this.fullnameIDMandatory,
      this.fullNameMandatory,
      this.dateOfBirthMandatory,
      this.placeOfBirthMandatory,
      this.placeOfBirthLOVMandatory,
      this.genderMandatory,
      this.phoneMandatory);
}
