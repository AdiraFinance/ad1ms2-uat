class SetAddInfoKeluargaModel {
  final bool isRelationshipStatusVisible;
  final bool isIdentityVisible;
  final bool isNoIdentitasVisible;
  final bool isNamaLengkapIdentitasVisible;
  final bool isNamaLengkapVisible;
  final bool isTglLahirVisible;
  final bool isTempatLahirSesuaiIdentitasVisible;
  final bool isTempatLahirSesuaiIdentitasLOVVisible;
  final bool isGenderVisible;
  final bool isKodeAreaVisible;
  final bool isTlpnVisible;
  final bool isNoHpVisible;

  final bool isRelationshipStatusMandatory;
  final bool isIdentityMandatory;
  final bool isNoIdentitasMandatory;
  final bool isNamaLengkapIdentitasMandatory;
  final bool isNamaLengkapMandatory;
  final bool isTglLahirMandatory;
  final bool isTempatLahirSesuaiIdentitasMandatory;
  final bool isTempatLahirSesuaiIdentitasLOVMandatory;
  final bool isGenderMandatory;
  final bool isKodeAreaMandatory;
  final bool isTlpnMandatory;
  final bool isNoHpMandatory;

  SetAddInfoKeluargaModel(
      this.isRelationshipStatusVisible,
      this.isIdentityVisible,
      this.isNoIdentitasVisible,
      this.isNamaLengkapIdentitasVisible,
      this.isNamaLengkapVisible,
      this.isTglLahirVisible,
      this.isTempatLahirSesuaiIdentitasVisible,
      this.isTempatLahirSesuaiIdentitasLOVVisible,
      this.isGenderVisible,
      this.isKodeAreaVisible,
      this.isTlpnVisible,
      this.isNoHpVisible,
      this.isRelationshipStatusMandatory,
      this.isIdentityMandatory,
      this.isNoIdentitasMandatory,
      this.isNamaLengkapIdentitasMandatory,
      this.isNamaLengkapMandatory,
      this.isTglLahirMandatory,
      this.isTempatLahirSesuaiIdentitasMandatory,
      this.isTempatLahirSesuaiIdentitasLOVMandatory,
      this.isGenderMandatory,
      this.isKodeAreaMandatory,
      this.isTlpnMandatory,
      this.isNoHpMandatory);
}
