class CertificateTypeModel{
  final String id;
  final String name;

  CertificateTypeModel(this.id, this.name);
}