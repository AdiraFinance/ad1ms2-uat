class MS2SvyAssgnmtModel {
  final String surveyID;
  final String order_no;
  final String survey_type_id;
  final String survey_type_desc;
  final String empl_nik;
  final String handphone_no;
  final String janji_survey_date;
  final String survey_reason;
  final String process_survey;
  final String flag_brms;
  final String status_survey;
  final int active;
  final String created_date;
  final String created_by;
  final String modified_date;
  final String modified_by;
  final String survey_result;
  final String survey_result_desc;
  final String survey_recomendation;
  final String survey_recomendation_desc;
  final String notes;
  final String survey_result_date;
  final double distance_sentra;
  final double distance_deal;
  final double distance_objt_purp_sentra;
  final String status_eligible_cfo;
  final String job_surveyor;

  MS2SvyAssgnmtModel(
      this.surveyID,
      this.order_no,
      this.survey_type_id,
      this.survey_type_desc,
      this.empl_nik,
      this.handphone_no,
      this.janji_survey_date,
      this.survey_reason,
      this.process_survey,
      this.flag_brms,
      this.status_survey,
      this.active,
      this.created_date,
      this.created_by,
      this.modified_date,
      this.modified_by,
      this.survey_result,
      this.survey_result_desc,
      this.survey_recomendation,
      this.survey_recomendation_desc,
      this.notes,
      this.survey_result_date,
      this.distance_sentra,
      this.distance_deal,
      this.distance_objt_purp_sentra,
      this.status_eligible_cfo,
      this.job_surveyor);
}
