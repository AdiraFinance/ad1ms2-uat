class PeriodOfTimeModel{
  final String id;
  final String name;

  PeriodOfTimeModel(this.id, this.name);
}