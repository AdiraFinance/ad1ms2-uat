import 'package:ad1ms2_dev/models/coverage_type_model.dart';
import 'package:ad1ms2_dev/models/product_model.dart';
import 'package:ad1ms2_dev/models/sub_type_model.dart';
import 'package:ad1ms2_dev/models/type_model.dart';

import 'assurance_model.dart';
import 'company_model.dart';
import 'coverage1_model.dart';
import 'coverage2_model.dart';
import 'form_m_pekerjaan_model.dart';
import 'insurance_type_model.dart';

class MajorInsuranceModel {
  final InsuranceTypeModel insuranceType;
  final String colaType;
  final String numberOfColla;
  final CompanyTypeInsuranceModel company;
  final ProductModel product;
  final String periodType;
  final String type;
  final CoverageModel coverage1;
  final CoverageModel coverage2;
  final SubTypeModel subType;
  CoverageTypeModel coverageType;
  String coverageValue;
  String upperLimit;
  String lowerLimit;
  String upperLimitRate;
  String lowerLimitRate;
  String priceCash;
  String priceCredit;
  String totalPriceSplit;
  String totalPriceRate;
  String totalPrice;
  final bool isEdit;
  final bool insuranceTypeDakor;
  final bool installmentNoDakor;
  final bool companyDakor;
  final bool productDakor;
  final bool periodDakor;
  final bool typeDakor;
  final bool coverage1Dakor;
  final bool coverage2Dakor;
  final bool subTypeDakor;
  final bool coverageTypeDakor;
  final bool coverageValueDakor;
  final bool upperLimitRateDakor;
  final bool upperLimitValueDakor;
  final bool lowerLimitRateDakor;
  final bool lowerLimitValueDakor;
  final bool cashDakor;
  final bool creditDakor;
  final bool totalPriceSplitDakor;
  final bool totalPriceRateDakor;
  final bool totalPriceDakor;
  final String orderProductInsuranceID;

  MajorInsuranceModel(
      this.insuranceType,
      this.colaType,
      this.numberOfColla,
      this.company,
      this.product,
      this.periodType,
      this.type,
      this.coverage1,
      this.coverage2,
      this.subType,
      this.coverageType,
      this.coverageValue,
      this.upperLimit,
      this.lowerLimit,
      this.upperLimitRate,
      this.lowerLimitRate,
      this.priceCash,
      this.priceCredit,
      this.totalPriceSplit,
      this.totalPriceRate,
      this.totalPrice,
      this.isEdit,
      this.insuranceTypeDakor,
      this.installmentNoDakor,
      this.companyDakor,
      this.productDakor,
      this.periodDakor,
      this.typeDakor,
      this.coverage1Dakor,
      this.coverage2Dakor,
      this.subTypeDakor,
      this.coverageTypeDakor,
      this.coverageValueDakor,
      this.upperLimitRateDakor,
      this.upperLimitValueDakor,
      this.lowerLimitRateDakor,
      this.lowerLimitValueDakor,
      this.cashDakor,
      this.creditDakor,
      this.totalPriceSplitDakor,
      this.totalPriceRateDakor,
      this.totalPriceDakor,
      this.orderProductInsuranceID);
}
