import 'package:ad1ms2_dev/models/subsidy_type_model.dart';
import 'add_detail_installment_credit_subsidy.dart';
import 'cutting_method_model.dart';
import 'installment_index_model.dart';

class CreditSubsidyModel{
    final String giver;
    final String orderSubsidyID;
    final SubsidyTypeModel type;
    final CuttingMethodModel cuttingMethod;
    final String value;
    final String claimValue;
    final String rateBeforeEff;
    final String rateBeforeFlat;
    final String totalInstallment;
    final String installmentIndex;
    final List<AddInstallmentDetailModel> installmentDetail;
    final bool isEditDakor;
    final bool giverDakor;
    final bool typeDakor;
    final bool cuttingMethodDakor;
    final bool cuttingValueDakor;
    final bool claimValueDakor;
    final bool rateBeforeEffDakor;
    final bool rateBeforeFlatDakor;
    final bool totalInstallmentDakor;

    CreditSubsidyModel(this.giver, this.orderSubsidyID, this.type, this.cuttingMethod, this.value, this.claimValue, this.rateBeforeEff, this.rateBeforeFlat, this.totalInstallment, this.installmentIndex, this.installmentDetail, this.isEditDakor, this.giverDakor, this.typeDakor, this.cuttingMethodDakor, this.cuttingValueDakor, this.claimValueDakor, this.rateBeforeEffDakor, this.rateBeforeFlatDakor, this.totalInstallmentDakor);
}