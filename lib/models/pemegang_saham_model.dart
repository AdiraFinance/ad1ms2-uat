import 'package:ad1ms2_dev/shared/constants.dart';
import 'address_guarantor_model.dart';
import 'birth_place_model.dart';
import 'form_m_informasi_alamat_model.dart';
import 'form_m_informasi_nasabah_model.dart';

class PemegangSahamPribadiModel {
  final RelationshipStatusModel relationshipStatusSelected;
  final IdentityModel typeIdentitySelected;
  final String identityNumber;
  final String fullNameIdentity;
  final String fullName;
  // final String alias;
  // final String degree;
  final String birthOfDate;
  final String placeOfBirthIdentity;
  final BirthPlaceModel birthPlaceIdentityLOV;
  final String gender;
  final String percentShare;
  final List<AddressModel> listAddress;
  final bool isEdit;
  final bool isRelationshipStatusChange;
  final bool isIdentityTypeChange;
  final bool isIdentityNoChange;
  final bool isFullnameIDChange;
  final bool isFullnameChange;
  final bool isDateOfBirthChange;
  final bool isPlaceOfBirthChange;
  final bool isPlaceOfBirthLOVChange;
  final bool isGenderChange;
  final bool isShareChange;
  final String shareholdersIndividualID;

  PemegangSahamPribadiModel(
      this.relationshipStatusSelected,
      this.typeIdentitySelected,
      this.identityNumber,
      this.fullNameIdentity,
      this.fullName,
      this.birthOfDate,
      this.placeOfBirthIdentity,
      this.birthPlaceIdentityLOV,
      this.gender,
      this.percentShare,
      this.listAddress,
      this.isEdit,
      this.isRelationshipStatusChange,
      this.isIdentityTypeChange,
      this.isIdentityNoChange,
      this.isFullnameIDChange,
      this.isFullnameChange,
      this.isDateOfBirthChange,
      this.isPlaceOfBirthChange,
      this.isPlaceOfBirthLOVChange,
      this.isGenderChange,
      this.isShareChange,this.shareholdersIndividualID);
}

class PemegangSahamLembagaModel {
  final TypeInstitutionModel typeInstitutionModel;
  final String institutionName;
  // final String dateOfEstablishment;
  final String initialDateForDateOfEstablishment;
  final String npwp;
  final String percentShare;
  final List<AddressModelCompany> listAddress;

  final bool isEdit;
  final bool isTypeInstitutionModelChange;
  final bool isInstitutionNameChange;
  // final bool isDateOfEstablishmentChange;
  final bool isInitialDateForDateOfEstablishmentChange;
  final bool isNpwpChange;
  final bool isPercentShareChange;
  final String shareholdersCorporateID;

  PemegangSahamLembagaModel(
      this.typeInstitutionModel,
      this.institutionName,
      this.initialDateForDateOfEstablishment,
      this.npwp,
      this.percentShare,
      this.listAddress,
      this.isEdit,
      this.isTypeInstitutionModelChange,
      this.isInstitutionNameChange,
      this.isInitialDateForDateOfEstablishmentChange,
      this.isNpwpChange,
      this.isPercentShareChange,this.shareholdersCorporateID);
}
