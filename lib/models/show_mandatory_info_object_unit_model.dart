class ShowMandatoryInfoObjectUnitModel {
  final bool isTypeOfFinancingModelVisible;
  final bool isBusinessActivitiesModelVisible;
  final bool isBusinessActivitiesTypeModelVisible;
  final bool isGroupObjectVisible;
  final bool isObjectVisible;
  final bool isTypeProductVisible;
  final bool isBrandObjectVisible;
  final bool isObjectTypeVisible;
  final bool isModelObjectVisible;
  final bool isDetailModelVisible;
  final bool isUsageObjectModelVisible;
  final bool isObjectPurposeVisible;
  final bool isGroupSalesVisible;
  final bool isGrupIdVisible;
  final bool isSourceOrderVisible;
  final bool isSourceOrderNameVisible;
  final bool isThirdPartyTypeVisible;
  final bool isThirdPartyVisible;
  final bool isMatriksDealerVisible;
  final bool isKegiatanVisible;
  final bool isSentraDVisible;
  final bool isUnitDVisible;
  final bool isProgramVisible;
  final bool isRehabTypeVisible;
  final bool isReferenceNumberVisible;
  final bool isTypeOfFinancingModelMandatory;
  final bool isBusinessActivitiesModelMandatory;
  final bool isBusinessActivitiesTypeModelMandatory;
  final bool isGroupObjectMandatory;
  final bool isObjectMandatory;
  final bool isTypeProductMandatory;
  final bool isBrandObjectMandatory;
  final bool isObjectTypeMandatory;
  final bool isModelObjectMandatory;
  final bool isDetailModelMandatory;
  final bool isUsageObjectModelMandatory;
  final bool isObjectPurposeMandatory;
  final bool isGroupSalesMandatory;
  final bool isGrupIdMandatory;
  final bool isSourceOrderMandatory;
  final bool isSourceOrderNameMandatory;
  final bool isThirdPartyTypeMandatory;
  final bool isThirdPartyMandatory;
  final bool isMatriksDealerMandatory;
  final bool isKegiatanMandatory;
  final bool isSentraDMandatory;
  final bool isUnitDMandatory;
  final bool isProgramMandatory;
  final bool isRehabTypeMandatory;
  final bool isReferenceNumberMandatory;

  ShowMandatoryInfoObjectUnitModel(
      this.isTypeOfFinancingModelVisible,
      this.isBusinessActivitiesModelVisible,
      this.isBusinessActivitiesTypeModelVisible,
      this.isGroupObjectVisible,
      this.isObjectVisible,
      this.isTypeProductVisible,
      this.isBrandObjectVisible,
      this.isObjectTypeVisible,
      this.isModelObjectVisible,
      this.isDetailModelVisible,
      this.isUsageObjectModelVisible,
      this.isObjectPurposeVisible,
      this.isGroupSalesVisible,
      this.isGrupIdVisible,
      this.isSourceOrderVisible,
      this.isSourceOrderNameVisible,
      this.isThirdPartyTypeVisible,
      this.isThirdPartyVisible,
      this.isMatriksDealerVisible,
      this.isKegiatanVisible,
      this.isSentraDVisible,
      this.isUnitDVisible,
      this.isProgramVisible,
      this.isRehabTypeVisible,
      this.isReferenceNumberVisible,
      this.isTypeOfFinancingModelMandatory,
      this.isBusinessActivitiesModelMandatory,
      this.isBusinessActivitiesTypeModelMandatory,
      this.isGroupObjectMandatory,
      this.isObjectMandatory,
      this.isTypeProductMandatory,
      this.isBrandObjectMandatory,
      this.isObjectTypeMandatory,
      this.isModelObjectMandatory,
      this.isDetailModelMandatory,
      this.isUsageObjectModelMandatory,
      this.isObjectPurposeMandatory,
      this.isGroupSalesMandatory,
      this.isGrupIdMandatory,
      this.isSourceOrderMandatory,
      this.isSourceOrderNameMandatory,
      this.isThirdPartyTypeMandatory,
      this.isThirdPartyMandatory,
      this.isMatriksDealerMandatory,
      this.isKegiatanMandatory,
      this.isSentraDMandatory,
      this.isUnitDMandatory,
      this.isProgramMandatory,
      this.isRehabTypeMandatory,
      this.isReferenceNumberMandatory);
}