import 'package:flutter/material.dart';

class TaksasiUnitModel{
  final String TAKSASI_CODE;
  final String TAKSASI_DESC;
  final String NILAI;
  List<bool> STATUS;
  final TextEditingController CONTROLLER;
  final TextEditingController CONTROLLER_NILAI_BOBOT;
  bool AUTOVALIDATE;
  final GlobalKey<FormState> KEYTAKSASI;

  TaksasiUnitModel(this.TAKSASI_CODE, this.TAKSASI_DESC, this.NILAI, this.STATUS, this.CONTROLLER, this.CONTROLLER_NILAI_BOBOT, this.AUTOVALIDATE, this.KEYTAKSASI);
}