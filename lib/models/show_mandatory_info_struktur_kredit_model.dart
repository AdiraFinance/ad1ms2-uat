class ShowMandatoryInfoStrukturKreditModel {
  bool isInstallmentTypeSelectedVisible;
  bool isPeriodOfTimeSelectedVisible;
  bool isPaymentPerYearVisible;
  bool isPaymentMethodSelectedVisible;
  bool isInterestRateEffectiveVisible;
  bool isInterestRateFlatVisible;
  bool isObjectPriceVisible;
  bool isKaroseriTotalPriceVisible;
  bool isTotalPriceVisible;
  bool isNetDPVisible;
  bool isBranchDPVisible;
  bool isGrossDPVisible;
  bool isTotalLoanVisible;
  bool isInstallmentVisible;
  bool isLTVVisible;
  bool isInstallmentTypeSelectedMandatory;
  bool isPeriodOfTimeSelectedMandatory;
  bool isPaymentPerYearMandatory;
  bool isPaymentMethodSelectedMandatory;
  bool isInterestRateEffectiveMandatory;
  bool isInterestRateFlatMandatory;
  bool isObjectPriceMandatory;
  bool isKaroseriTotalPriceMandatory;
  bool isTotalPriceMandatory;
  bool isNetDPMandatory;
  bool isBranchDPMandatory;
  bool isGrossDPMandatory;
  bool isTotalLoanMandatory;
  bool isInstallmentMandatory;
  bool isLTVMandatory;

  ShowMandatoryInfoStrukturKreditModel(
      this.isInstallmentTypeSelectedVisible,
      this.isPeriodOfTimeSelectedVisible,
      this.isPaymentPerYearVisible,
      this.isPaymentMethodSelectedVisible,
      this.isInterestRateEffectiveVisible,
      this.isInterestRateFlatVisible,
      this.isObjectPriceVisible,
      this.isKaroseriTotalPriceVisible,
      this.isTotalPriceVisible,
      this.isNetDPVisible,
      this.isBranchDPVisible,
      this.isGrossDPVisible,
      this.isTotalLoanVisible,
      this.isInstallmentVisible,
      this.isLTVVisible,
      this.isInstallmentTypeSelectedMandatory,
      this.isPeriodOfTimeSelectedMandatory,
      this.isPaymentPerYearMandatory,
      this.isPaymentMethodSelectedMandatory,
      this.isInterestRateEffectiveMandatory,
      this.isInterestRateFlatMandatory,
      this.isObjectPriceMandatory,
      this.isKaroseriTotalPriceMandatory,
      this.isTotalPriceMandatory,
      this.isNetDPMandatory,
      this.isBranchDPMandatory,
      this.isGrossDPMandatory,
      this.isTotalLoanMandatory,
      this.isInstallmentMandatory,
      this.isLTVMandatory);
}