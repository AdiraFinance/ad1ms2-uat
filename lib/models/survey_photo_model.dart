import 'package:ad1ms2_dev/models/photo_type_model.dart';

import 'form_m_foto_model.dart';

class SurveyPhotoModel {
  final PhotoTypeModel photoTypeModel;
  final List<ImageFileModel> listImageModel;
  final bool isEdit;
  final bool isPhotoTypeModel;
  final bool isListImageModel;

  SurveyPhotoModel(
      this.photoTypeModel, this.listImageModel,
      this.isEdit, this.isPhotoTypeModel, this.isListImageModel);
}