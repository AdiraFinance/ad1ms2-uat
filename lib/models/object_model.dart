class ObjectModel {
  final String id;
  final String name;

  ObjectModel(this.id, this.name);
}
