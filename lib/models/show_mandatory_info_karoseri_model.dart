class ShowMandatoryInfoKaroseriModel {
  final bool isRadioValuePKSKaroseriVisible;
  final bool isCompanyVisible;
  final bool isKaroseriVisible;
  final bool isJumlahKaroseriVisible;
  final bool isPriceVisible;
  final bool isTotalPriceVisible;
  final bool isRadioValuePKSKaroseriMandatory;
  final bool isCompanyMandatory;
  final bool isKaroseriMandatory;
  final bool isJumlahKaroseriMandatory;
  final bool isPriceMandatory;
  final bool isTotalPriceMandatory;

  ShowMandatoryInfoKaroseriModel(
      this.isRadioValuePKSKaroseriVisible,
      this.isCompanyVisible,
      this.isKaroseriVisible,
      this.isJumlahKaroseriVisible,
      this.isPriceVisible,
      this.isTotalPriceVisible,
      this.isRadioValuePKSKaroseriMandatory,
      this.isCompanyMandatory,
      this.isKaroseriMandatory,
      this.isJumlahKaroseriMandatory,
      this.isPriceMandatory,
      this.isTotalPriceMandatory);
}