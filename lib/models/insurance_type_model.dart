class InsuranceTypeModel{
    final String PARENT_KODE;
    final String KODE;
    final String DESKRIPSI;

    InsuranceTypeModel(this.PARENT_KODE, this.KODE, this.DESKRIPSI);
}