import 'package:ad1ms2_dev/models/address_guarantor_model.dart';
import 'package:ad1ms2_dev/models/birth_place_model.dart';
import 'package:ad1ms2_dev/models/form_m_company_rincian_model.dart';
import 'package:ad1ms2_dev/models/form_m_informasi_nasabah_model.dart';
import 'package:ad1ms2_dev/shared/constants.dart';

import 'form_m_informasi_alamat_model.dart';

class GuarantorIndividualModel {
  final RelationshipStatusModel relationshipStatusModel;
  final IdentityModel identityModel;
  final String identityNumber;
  final String fullNameIdentity;
  final String fullName;
  final String birthDate;
  final String birthPlaceIdentity1;
  final BirthPlaceModel birthPlaceIdentity2;
  final String gender;
  final String cellPhoneNumber;
  final List<AddressModel> listAddressGuarantorModel;
  final bool isEditList;
  final bool relationshipStatusDakor;
  final bool identityTypeDakor;
  final bool identityNoDakor;
  final bool fullnameIDDakor;
  final bool fullNameDakor;
  final bool dateOfBirthDakor;
  final bool placeOfBirthDakor;
  final bool placeOfBirthLOVDakor;
  final bool genderDakor;
  final bool phoneDakor;
  final String guarantorIndividualID;

  GuarantorIndividualModel(
      this.relationshipStatusModel,
      this.identityModel,
      this.identityNumber,
      this.fullNameIdentity,
      this.fullName,
      this.birthDate,
      this.birthPlaceIdentity1,
      this.birthPlaceIdentity2,
      this.listAddressGuarantorModel,
      this.cellPhoneNumber,
      this.gender,
      this.isEditList,
      this.relationshipStatusDakor,
      this.identityTypeDakor,
      this.identityNoDakor,
      this.fullnameIDDakor,
      this.fullNameDakor,
      this.dateOfBirthDakor,
      this.placeOfBirthDakor,
      this.placeOfBirthLOVDakor,
      this.genderDakor,
      this.phoneDakor,
      this.guarantorIndividualID);
}

class GuarantorCompanyModel {
  final TypeInstitutionModel typeInstitutionModel;
  final ProfilModel profilModel;
  final String establishDate;
  final String institutionName;
  final String npwp;
  final List<AddressModelCompany> listAddressGuarantorModel;
  final bool isEditList;
  final bool companyTypeDakor;
  final bool companyNameDakor;
  final bool establishedDateDakor;
  final bool npwpDakor;
  final String guarantorCorporateID;

  GuarantorCompanyModel(
      this.typeInstitutionModel,
      this.profilModel,
      this.establishDate,
      this.institutionName,
      this.npwp,
      this.listAddressGuarantorModel,
      this.isEditList,
      this.companyTypeDakor,
      this.companyNameDakor,
      this.establishedDateDakor,
      this.npwpDakor,
      this.guarantorCorporateID);
}
