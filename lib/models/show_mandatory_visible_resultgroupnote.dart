class ShowMandatoryEnabledResultSurvey {
  //Widget Result Survey Group Note
  final bool isResultSurveyGroupNotesVisible;
  final bool isResultSurveyGroupNotesMandatory;
  final bool isResultSurveyGroupNotesEnabled;

  final bool isSurveyRecommendationVisible;
  final bool isSurveyRecommendationMandatory;
  final bool isSurveyRecommendationEnabled;

  final bool isSurveyNotesVisible;
  final bool isSurveyNotesMandatory;
  final bool isSurveyNotesEnabled;

  final bool isSurveyDateVisible;
  final bool isSurveyDateMandatory;
  final bool isSurveyDateEnabled;

  //Widget Result Survey Photo
  final bool isPhotoTypeVisible;
  final bool isPhotoTypeMandatory;
  final bool isPhotoTypeEnabled;

  //Widget Result Survey Group Location
  final bool isSurveyTypeVisible;
  final bool isSurveyTypeMandatory;
  final bool isSurveyTypeEnabled;

  final bool isDistanceLocationWithSentraUnitVisible;
  final bool isDistanceLocationWithSentraUnitMandatory;
  final bool isDistanceLocationWithSentraUnitEnabled;

  final bool isDistanceLocationWithDealerMerchantAXIVisible;
  final bool isDistanceLocationWithDealerMerchantAXIMandatory;
  final bool isDistanceLocationWithDealerMerchantAXIEnabled;

  final bool isDistanceLocationWithUsageObjectWithSentraUnitVisible;
  final bool isDistanceLocationWithUsageObjectWithSentraUnitMandatory;
  final bool isDistanceLocationWithUsageObjectWithSentraUnitEnabled;

  //Widget Add Edit Asset
  final bool isAssetTypeVisible;
  final bool isAssetTypeMandatory;
  final bool isAssetTypeEnabled;

  final bool isValueAssetVisible;
  final bool isValueAssetMandatory;
  final bool isValueAssetEnabled;

  final bool isIdentityInfoVisible;
  final bool isIdentityInfoMandatory;
  final bool isIdentityInfoEnabled;

  final bool isSurfaceBuildingAreaInfoVisible;
  final bool isSurfaceBuildingAreaInfoMandatory;
  final bool isSurfaceBuildingAreaInfoEnabled;

  final bool isRoadTypeInfoVisible;
  final bool isRoadTypeInfoMandatory;
  final bool isRoadTypeInfoEnabled;

  final bool isElectricityTypeInfoVisible;
  final bool isElectricityTypeInfoMandatory;
  final bool isElectricityTypeInfoEnabled;

  final bool isElectricityBillInfoVisible;
  final bool isElectricityBillInfoMandatory;
  final bool isElectricityBillInfoEnabled;

  final bool isEndDateInfoVisible;
  final bool isEndDateInfoMandatory;
  final bool isEndDateInfoEnabled;

  final bool isLengthStayInfoVisible;
  final bool isLengthStayInfoMandatory;
  final bool isLengthStayInfoEnabled;

  //Widget Add Edit Survey Detail

  final bool isEnvironmentalInfoVisible;
  final bool isEnvironmentalInfoMandatory;
  final bool isEnvironmentalInfoEnabled;

  final bool isSourceInfoVisible;
  final bool isSourceInfoMandatory;
  final bool isSourceInfoEnabled;

  final bool isSourceNameInfoVisible;
  final bool isSourceNameInfoMandatory;
  final bool isSourceNameInfoEnabled;

  ShowMandatoryEnabledResultSurvey(
    //Widget Result Survey Group Note
    this.isResultSurveyGroupNotesVisible,
    this.isResultSurveyGroupNotesMandatory,
    this.isResultSurveyGroupNotesEnabled,

    this.isSurveyRecommendationVisible,
    this.isSurveyRecommendationMandatory,
    this.isSurveyRecommendationEnabled,

    this.isSurveyNotesVisible,
    this.isSurveyNotesMandatory,
    this.isSurveyNotesEnabled,

    this.isSurveyDateVisible,
    this.isSurveyDateMandatory,
    this.isSurveyDateEnabled,

    //Widget Result Survey Photo
    this.isPhotoTypeVisible,
    this.isPhotoTypeMandatory,
    this.isPhotoTypeEnabled,

    //Widget Result Survey Group Location
    this.isSurveyTypeVisible,
    this.isSurveyTypeMandatory,
    this.isSurveyTypeEnabled,

    this.isDistanceLocationWithSentraUnitVisible,
    this.isDistanceLocationWithSentraUnitMandatory,
    this.isDistanceLocationWithSentraUnitEnabled,

    this.isDistanceLocationWithDealerMerchantAXIVisible,
    this.isDistanceLocationWithDealerMerchantAXIMandatory,
    this.isDistanceLocationWithDealerMerchantAXIEnabled,

    this.isDistanceLocationWithUsageObjectWithSentraUnitVisible,
    this.isDistanceLocationWithUsageObjectWithSentraUnitMandatory,
    this.isDistanceLocationWithUsageObjectWithSentraUnitEnabled,

    //Widget Add Edit Asset
    this.isAssetTypeVisible,
    this.isAssetTypeMandatory,
    this.isAssetTypeEnabled,

    this.isValueAssetVisible,
    this.isValueAssetMandatory,
    this.isValueAssetEnabled,

    this.isIdentityInfoVisible,
    this.isIdentityInfoMandatory,
    this.isIdentityInfoEnabled,

    this.isSurfaceBuildingAreaInfoVisible,
    this.isSurfaceBuildingAreaInfoMandatory,
    this.isSurfaceBuildingAreaInfoEnabled,

    this.isRoadTypeInfoVisible,
    this.isRoadTypeInfoMandatory,
    this.isRoadTypeInfoEnabled,

    this.isElectricityTypeInfoVisible,
    this.isElectricityTypeInfoMandatory,
    this.isElectricityTypeInfoEnabled,

    this.isElectricityBillInfoVisible,
    this.isElectricityBillInfoMandatory,
    this.isElectricityBillInfoEnabled,

    this.isEndDateInfoVisible,
    this.isEndDateInfoMandatory,
    this.isEndDateInfoEnabled,

    this.isLengthStayInfoVisible,
    this.isLengthStayInfoMandatory,
    this.isLengthStayInfoEnabled,

    //Widget Add Edit Survey Detail
    this.isEnvironmentalInfoVisible,
    this.isEnvironmentalInfoMandatory,
    this.isEnvironmentalInfoEnabled,

    this.isSourceInfoVisible,
    this.isSourceInfoMandatory,
    this.isSourceInfoEnabled,

    this.isSourceNameInfoVisible,
    this.isSourceNameInfoMandatory,
    this.isSourceNameInfoEnabled,
  );
}
