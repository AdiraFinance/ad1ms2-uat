class ShowMandatoryOccupationModel {
  final bool isLamaBekerjaBerjalan;
  final bool isNamaUsahaWiraswasta;
  final bool isJenisBadanUsahaWiraswasta;
  final bool isSektorEkonomiWiraswasta;
  final bool isLapanganUsahaWiraswasta;
  final bool isStatusLokasiWiraswasta;
  final bool isLokasiUsahaWiraswasta;
  final bool isTotalPegawaiWiraswasta;
  final bool isTotalLamaBekerjaWiraswasta;
  final bool isJenisProfesiProfesional;
  final bool isSektorEkonomiProfesional;
  final bool isLapanganUsahaProfesional;
  final bool isStatusLokasiProfesional;
  final bool isLokasiUsahaProfesional;
  final bool isTotalLamaBekerjaProfesional;
  final bool isNamaPerusahaanLainnya;
  final bool isJenisPerusahaanLainnya;
  final bool isSektorEkonomiLainnya;
  final bool isLapanganUsahaLainnya;
  final bool isTotalPegawaiLainnya;
  final bool isTotalLamaBekerjaLainnya;
  final bool isJenisPepHighRiskLainnya;
  final bool isStatusPegawaiLainnya;

  final bool isLamaBekerjaBerjalanMandatory;
  final bool isNamaUsahaWiraswastaMandatory;
  final bool isJenisBadanUsahaWiraswastaMandatory;
  final bool isSektorEkonomiWiraswastaMandatory;
  final bool isLapanganUsahaWiraswastaMandatory;
  final bool isStatusLokasiWiraswastaMandatory;
  final bool isLokasiUsahaWiraswastaMandatory;
  final bool isTotalPegawaiWiraswastaMandatory;
  final bool isTotalLamaBekerjaWiraswastaMandatory;
  final bool isJenisProfesiProfesionalMandatory;
  final bool isSektorEkonomiProfesionalMandatory;
  final bool isLapanganUsahaProfesionalMandatory;
  final bool isStatusLokasiProfesionalMandatory;
  final bool isLokasiUsahaProfesionalMandatory;
  final bool isTotalLamaBekerjaProfesionalMandatory;
  final bool isNamaPerusahaanLainnyaMandatory;
  final bool isJenisPerusahaanLainnyaMandatory;
  final bool isSektorEkonomiLainnyaMandatory;
  final bool isLapanganUsahaLainnyaMandatory;
  final bool isTotalPegawaiLainnyaMandatory;
  final bool isTotalLamaBekerjaLainnyaMandatory;
  final bool isJenisPepHighRiskLainnyaMandatory;
  final bool isStatusPegawaiLainnyaMandatory;

  ShowMandatoryOccupationModel(
      this.isLamaBekerjaBerjalan,
      this.isNamaUsahaWiraswasta,
      this.isJenisBadanUsahaWiraswasta,
      this.isSektorEkonomiWiraswasta,
      this.isLapanganUsahaWiraswasta,
      this.isStatusLokasiWiraswasta,
      this.isLokasiUsahaWiraswasta,
      this.isTotalPegawaiWiraswasta,
      this.isTotalLamaBekerjaWiraswasta,
      this.isJenisProfesiProfesional,
      this.isSektorEkonomiProfesional,
      this.isLapanganUsahaProfesional,
      this.isStatusLokasiProfesional,
      this.isLokasiUsahaProfesional,
      this.isTotalLamaBekerjaProfesional,
      this.isNamaPerusahaanLainnya,
      this.isJenisPerusahaanLainnya,
      this.isSektorEkonomiLainnya,
      this.isLapanganUsahaLainnya,
      this.isTotalPegawaiLainnya,
      this.isTotalLamaBekerjaLainnya,
      this.isJenisPepHighRiskLainnya,
      this.isStatusPegawaiLainnya,
      this.isLamaBekerjaBerjalanMandatory,
      this.isNamaUsahaWiraswastaMandatory,
      this.isJenisBadanUsahaWiraswastaMandatory,
      this.isSektorEkonomiWiraswastaMandatory,
      this.isLapanganUsahaWiraswastaMandatory,
      this.isStatusLokasiWiraswastaMandatory,
      this.isLokasiUsahaWiraswastaMandatory,
      this.isTotalPegawaiWiraswastaMandatory,
      this.isTotalLamaBekerjaWiraswastaMandatory,
      this.isJenisProfesiProfesionalMandatory,
      this.isSektorEkonomiProfesionalMandatory,
      this.isLapanganUsahaProfesionalMandatory,
      this.isStatusLokasiProfesionalMandatory,
      this.isLokasiUsahaProfesionalMandatory,
      this.isTotalLamaBekerjaProfesionalMandatory,
      this.isNamaPerusahaanLainnyaMandatory,
      this.isJenisPerusahaanLainnyaMandatory,
      this.isSektorEkonomiLainnyaMandatory,
      this.isLapanganUsahaLainnyaMandatory,
      this.isTotalPegawaiLainnyaMandatory,
      this.isTotalLamaBekerjaLainnyaMandatory,
      this.isJenisPepHighRiskLainnyaMandatory,
      this.isStatusPegawaiLainnyaMandatory);
}
