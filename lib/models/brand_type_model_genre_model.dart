import 'package:ad1ms2_dev/models/brand_object_model.dart';
import 'package:ad1ms2_dev/models/model_object_model.dart';
import 'package:ad1ms2_dev/models/object_type_model.dart';
import 'package:ad1ms2_dev/models/object_usage_model.dart';

class BrandTypeModelGenreModel{
  final BrandObjectModel brandObjectModel;
  final ObjectTypeModel objectTypeModel;
  final ModelObjectModel modelObjectModel;
  final ObjectUsageModel objectUsageModel;

  BrandTypeModelGenreModel(this.brandObjectModel, this.objectTypeModel, this.modelObjectModel, this.objectUsageModel);
}