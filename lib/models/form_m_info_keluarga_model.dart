import 'package:ad1ms2_dev/models/birth_place_model.dart';
import 'package:ad1ms2_dev/models/form_m_informasi_nasabah_model.dart';
import 'package:ad1ms2_dev/shared/constants.dart';

class FormMInfoKelModel {
  final IdentityModel identityModel;
  final String familyInfoID;
  final RelationshipStatusModel relationshipStatusModel;
  final String noIdentitas;
  final String namaLengkapSesuaiIdentitas;
  final String namaLengkap;
  final String birthDate;
  final String gender;
  final String kodeArea;
  final String noTlpn;
  final String tmptLahirSesuaiIdentitas;
  final BirthPlaceModel birthPlaceModel;
  final String noHp;
  final bool isidentityModelChanges;
  final bool isrelationshipStatusModelChanges;
  final bool isnoIdentitasChanges;
  final bool isnamaLengkapSesuaiIdentitasChanges;
  final bool isnamaLengkapChanges;
  final bool isbirthDateChanges;
  final bool isgenderChanges;
  final bool iskodeAreaChanges;
  final bool isnoTlpnChanges;
  final bool istmptLahirSesuaiIdentitasChanges;
  final bool isbirthPlaceModelChanges;
  final bool isnoHpChanges;

  FormMInfoKelModel(
      this.identityModel,
      this.familyInfoID,
      this.relationshipStatusModel,
      this.noIdentitas,
      this.namaLengkapSesuaiIdentitas,
      this.namaLengkap,
      this.birthDate,
      this.gender,
      this.kodeArea,
      this.noTlpn,
      this.tmptLahirSesuaiIdentitas,
      this.noHp,
      this.birthPlaceModel,
      this.isidentityModelChanges,
      this.isrelationshipStatusModelChanges,
      this.isnoIdentitasChanges,
      this.isnamaLengkapSesuaiIdentitasChanges,
      this.isnamaLengkapChanges,
      this.isbirthDateChanges,
      this.isgenderChanges,
      this.iskodeAreaChanges,
      this.isnoTlpnChanges,
      this.istmptLahirSesuaiIdentitasChanges,
      this.isnoHpChanges,
      this.isbirthPlaceModelChanges,
      );
}
