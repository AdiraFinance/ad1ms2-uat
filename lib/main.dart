import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/screens/login.dart';
import 'package:ad1ms2_dev/shared/SA_change_notifier/sa_job_mayor_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/form_m_add_additional_insurance_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/form_m_add_major_insurance_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_application_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_credit_structure_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_credit_structure_type_installment_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_credit_subsidy_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_collatelar_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_object_unit_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_salesman_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_object_karoseri_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/marketing_notes_change_notifier.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/dashboard/dashboard_change_notif.dart';
import 'package:ad1ms2_dev/shared/dedup/dedup_company_change_notifier.dart';
import 'package:ad1ms2_dev/shared/dedup/dedup_individu_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_AOS_change_notifier/form_AOS_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_IA_change_notifier/form_IA_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_add_address_company_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_add_guarantor_company_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_add_guarantor_individual_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_company_add_guarantor_company_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_company_add_guarantor_individual_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_company_alamat_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_company_guarantor_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_company_manajemen_pic_alamat_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_company_manajemen_pic_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_company_parent_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_company_parent_change_notifier_old.dart';
import 'package:ad1ms2_dev/shared/form_m_company_pemegang_saham_kelembagaan_alamat_change_notif.dart';
import 'package:ad1ms2_dev/shared/add_pemegang_saham_lembaga_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_company_pemegang_saham_pribadi_alamat_change_notif.dart';
import 'package:ad1ms2_dev/shared/add_pemegang_saham_pribadi_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_company_pendapatan_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_company_penjamin_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_company_rincian_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_credit_limit_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_foto_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_guarantor_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_income_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_individu_change_notifier/form_m_parent_individu_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_info_keluarga_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_info_nasabah_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_informasi_alamat_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_informasi_keluarga(ibu)_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_lookup_utj_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_occupation_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_parent_change_notifier_old.dart';
import 'package:ad1ms2_dev/shared/home/home_change_notifier.dart';
import 'package:ad1ms2_dev/shared/info_wmp_change_notifier.dart';
import 'package:ad1ms2_dev/shared/list_oid_change_notifier.dart';
import 'package:ad1ms2_dev/shared/list_order_tracking_change_notif.dart';
import 'package:ad1ms2_dev/shared/login_change_notifier.dart';
import 'package:ad1ms2_dev/shared/pemegang_saham_lembaga_change_notif.dart';
import 'package:ad1ms2_dev/shared/pemegang_saham_pribadi_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_business_location_change_notif.dart';
import 'package:ad1ms2_dev/shared/search_pep_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_status_location_field_change_notif.dart';
import 'package:ad1ms2_dev/shared/survey/list_survey_photo_change_notifier.dart';
import 'package:ad1ms2_dev/shared/survey/result_survey_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/io_client.dart';
import 'package:location/location.dart' as locationPlugin;
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';

import 'db/database_helper.dart';
import 'shared/change_notifier_app/info_additional_insurance_change_notifier.dart';
import 'shared/change_notifier_app/info_app_document_change_notifier.dart';
import 'shared/change_notifier_app/info_credit_income_change_notifier.dart';
import 'shared/change_notifier_app/info_major_insurance_change_notifier.dart';
import 'shared/change_notifier_app/taksasi_unit_change_notifier.dart';
import 'shared/form_m_add_address_individu_change_notifier.dart';
import 'shared/task_list_change_notifier/task_list_change_notifier.dart';

Color myPrimaryColor = const Color(0xffFEDE00);
Color snackbarColor  = const Color(0xfff0382b);

const MaterialColor primaryBlack = MaterialColor(
  _blackPrimaryValue,
  <int, Color>{
    50: Color(0xFF000000),
    100: Color(0xFF000000),
    200: Color(0xFF000000),
    300: Color(0xFF000000),
    400: Color(0xFF000000),
    500: Color(_blackPrimaryValue),
    600: Color(0xFF000000),
    700: Color(0xFF000000),
    800: Color(0xFF000000),
    900: Color(0xFF000000),
  },
);
const int _blackPrimaryValue = 0xFF000000;

const MaterialColor primaryBlue = MaterialColor(
  _bluePrimaryValue,
  <int, Color>{
    50: Color(0xFF3f4f7f),
    100: Color(0xFF3f4f7f),
    200: Color(0xFF3f4f7f),
    300: Color(0xFF3f4f7f),
    400: Color(0xFF3f4f7f),
    500: Color(_bluePrimaryValue),
    600: Color(0xFF3f4f7f),
    700: Color(0xFF3f4f7f),
    800: Color(0xFF3f4f7f),
    900: Color(0xFF3f4f7f),
  },
);
const int _bluePrimaryValue = 0xFF3f4f7f;

const MaterialColor primaryOrange = MaterialColor(
  _orangePrimaryValue,
  <int, Color>{
    50: Color(0xFFEC7A00),
    100: Color(0xFFEC7A00),
    200: Color(0xFFEC7A00),
    300: Color(0xFFEC7A00),
    400: Color(0xFFEC7A00),
    500: Color(_orangePrimaryValue),
    600: Color(0xFFEC7A00),
    700: Color(0xFFEC7A00),
    800: Color(0xFFEC7A00),
    900: Color(0xFFEC7A00),
  },
);
const int _orangePrimaryValue = 0xFFEC7A00;

const MaterialColor primaryColorIDE = MaterialColor(
  _idePrimaryValue,
  <int, Color>{
    50: Color(0xFF1E88E5),
    100: Color(0xFF1E88E5),
    200: Color(0xFF1E88E5),
    300: Color(0xFF1E88E5),
    400: Color(0xFF1E88E5),
    500: Color(_idePrimaryValue),
    600: Color(0xFF1E88E5),
    700: Color(0xFF1E88E5),
    800: Color(0xFF1E88E5),
    900: Color(0xFF1E88E5),
  },
);
const int _idePrimaryValue = 0xFF1E88E5;

const MaterialColor primaryColorMiniForm = MaterialColor(
  _miniFormPrimaryValue,
  <int, Color>{
    50: Color(0xFF00897B),
    100: Color(0xFF00897B),
    200: Color(0xFF00897B),
    300: Color(0xFF00897B),
    400: Color(0xFF00897B),
    500: Color(_miniFormPrimaryValue),
    600: Color(0xFF00897B),
    700: Color(0xFF00897B),
    800: Color(0xFF00897B),
    900: Color(0xFF00897B),
  },
);
const int _miniFormPrimaryValue = 0xFF00897B;

const MaterialColor primaryColorAOS = MaterialColor(
  _aosPrimaryValue,
  <int, Color>{
    50: Color(0xFFD81B60),
    100: Color(0xFFD81B60),
    200: Color(0xFFD81B60),
    300: Color(0xFFD81B60),
    400: Color(0xFFD81B60),
    500: Color(_aosPrimaryValue),
    600: Color(0xFFD81B60),
    700: Color(0xFFD81B60),
    800: Color(0xFFD81B60),
    900: Color(0xFFD81B60),
  },
);
const int _aosPrimaryValue = 0xFFD81B60;

const MaterialColor primaryColorCONA = MaterialColor(
  _conaPrimaryValue,
  <int, Color>{
    50: Color(0xFFFFB74D),
    100: Color(0xFFFFB74D),
    200: Color(0xFFFFB74D),
    300: Color(0xFFFFB74D),
    400: Color(0xFFFFB74D),
    500: Color(_conaPrimaryValue),
    600: Color(0xFFFFB74D),
    700: Color(0xFFFFB74D),
    800: Color(0xFFFFB74D),
    900: Color(0xFFFFB74D),
  },
);
const int _conaPrimaryValue = 0xFFFFB74D;

const MaterialColor primaryColorRegulerSurvey = MaterialColor(
  _regulerSurveyPrimaryValue,
  <int, Color>{
    50: Color(0xFF8E24AA),
    100: Color(0xFF8E24AA),
    200: Color(0xFF8E24AA),
    300: Color(0xFF8E24AA),
    400: Color(0xFF8E24AA),
    500: Color(_regulerSurveyPrimaryValue),
    600: Color(0xFF8E24AA),
    700: Color(0xFF8E24AA),
    800: Color(0xFF8E24AA),
    900: Color(0xFF8E24AA),
  },
);
const int _regulerSurveyPrimaryValue = 0xFF8E24AA;

const MaterialColor primaryColorPAC = MaterialColor(
  _pacPrimaryValue,
  <int, Color>{
    50: Color(0xFF5E35B1),
    100: Color(0xFF5E35B1),
    200: Color(0xFF5E35B1),
    300: Color(0xFF5E35B1),
    400: Color(0xFF5E35B1),
    500: Color(_pacPrimaryValue),
    600: Color(0xFF5E35B1),
    700: Color(0xFF5E35B1),
    800: Color(0xFF5E35B1),
    900: Color(0xFF5E35B1),
  },
);
const int _pacPrimaryValue = 0xFF5E35B1;

const MaterialColor primaryColorResurvey = MaterialColor(
  _resurveyPrimaryValue,
  <int, Color>{
    50: Color(0xFF757575),
    100: Color(0xFF757575),
    200: Color(0xFF757575),
    300: Color(0xFF757575),
    400: Color(0xFF757575),
    500: Color(_resurveyPrimaryValue),
    600: Color(0xFF757575),
    700: Color(0xFF757575),
    800: Color(0xFF757575),
    900: Color(0xFF757575),
  },
);
const int _resurveyPrimaryValue = 0xFF757575;

const MaterialColor primaryColorDataKoreksi = MaterialColor(
  _dataKoreksiPrimaryValue,
  <int, Color>{
    50: Color(0xFF6D4C41),
    100: Color(0xFF6D4C41),
    200: Color(0xFF6D4C41),
    300: Color(0xFF6D4C41),
    400: Color(0xFF6D4C41),
    500: Color(_dataKoreksiPrimaryValue),
    600: Color(0xFF6D4C41),
    700: Color(0xFF6D4C41),
    800: Color(0xFF6D4C41),
    900: Color(0xFF6D4C41),
  },
);
const int _dataKoreksiPrimaryValue = 0xFF6D4C41;

String globalPath;
String token;
String xApiKey = "";

void main() {
  runApp(
      MultiProvider(
          providers: [
            ChangeNotifierProvider(create: (context) => DashboardChangeNotif()),
            ChangeNotifierProvider(create: (context) => LoginChangeNotifier()),
            ChangeNotifierProvider(create: (context) => ListOidChangeNotifier()),
            ChangeNotifierProvider(create: (context) => FormMParentCompanyChangeNotifier()),
            ChangeNotifierProvider(create: (context) => FormMParentChangeNotifierOld()),
            ChangeNotifierProvider(create: (context) => FormMCreditLimitChangeNotifier()),
            ChangeNotifierProvider(create: (context) => FormMInfoNasabahChangeNotif()),
            ChangeNotifierProvider(create: (context) => FormMFotoChangeNotifier()),
            ChangeNotifierProvider(create: (context) => FormMInfoAlamatChangeNotif()),
            ChangeNotifierProvider(create: (context) => FormMInformasiKeluargaIBUChangeNotif()),
            ChangeNotifierProvider(create: (context) => FormMInfoKeluargaChangeNotif()),
            ChangeNotifierProvider(create: (context) => FormMOccupationChangeNotif()),
            ChangeNotifierProvider(create: (context) => FormMCompanyParentChangeNotifierOld()),
            ChangeNotifierProvider(create: (context) => FormMCompanyRincianChangeNotifier()),
            ChangeNotifierProvider(create: (context) => FormMCompanyAlamatChangeNotifier()),
            ChangeNotifierProvider(create: (context) => FormMCompanyPendapatanChangeNotifier()),
            ChangeNotifierProvider(create: (context) => FormMCompanyPenjaminChangeNotifier()),
            ChangeNotifierProvider(create: (context) => FormMCompanyManajemenPICChangeNotifier()),
            ChangeNotifierProvider(create: (context) => FormMCompanyManajemenPICAlamatChangeNotifier()),
            ChangeNotifierProvider(create: (context) => PemegangSahamPribadiChangeNotifier()),
            ChangeNotifierProvider(create: (context) => FormMCompanyPemegangSahamPribadiAlamatChangeNotifier()),
            ChangeNotifierProvider(create: (context) => PemegangSahamLembagaChangeNotifier()),
            ChangeNotifierProvider(create: (context) => FormMCompanyPemegangSahamKelembagaanAlamatChangeNotifier()),
            ChangeNotifierProvider(create: (context) => FormMIncomeChangeNotifier()),
            ChangeNotifierProvider(create: (context) => FormMGuarantorChangeNotifier()),
            ChangeNotifierProvider(create: (context) => FormMAddGuarantorIndividualChangeNotifier()),
            ChangeNotifierProvider(create: (context) => FormMAddGuarantorCompanyChangeNotifier()),
            ChangeNotifierProvider(create: (context) => FormMCompanyAddGuarantorIndividualChangeNotifier()),
            ChangeNotifierProvider(create: (context) => FormMCompanyGuarantorChangeNotifier()),
            ChangeNotifierProvider(create: (context) => FormMCompanyAddGuarantorCompanyChangeNotifier()),
            ChangeNotifierProvider(create: (context) => InfoAppChangeNotifier()),
            ChangeNotifierProvider(create: (context) => FormMParentIndividualChangeNotifier()),
            ChangeNotifierProvider(create: (context) => InformationObjectUnitChangeNotifier()),
            ChangeNotifierProvider(create: (context) => InformationSalesmanChangeNotifier()),
            ChangeNotifierProvider(create: (context) => InformationCollateralChangeNotifier()),
            ChangeNotifierProvider(create: (context) => InfoObjectKaroseriChangeNotifier()),
            ChangeNotifierProvider(create: (context) => InfoCreditStructureChangeNotifier()),
            ChangeNotifierProvider(create: (context) => InfoCreditStructureTypeInstallmentChangeNotifier()),
            ChangeNotifierProvider(create: (context) => InfoDocumentChangeNotifier()),
            ChangeNotifierProvider(create: (context) => InfoMajorInsuranceChangeNotifier()),
            ChangeNotifierProvider(create: (context) => InfoCreditSubsidyChangeNotifier()),
            ChangeNotifierProvider(create: (context) => InfoAdditionalInsuranceChangeNotifier()),
            ChangeNotifierProvider(create: (context) => InfoCreditIncomeChangeNotifier()),
            ChangeNotifierProvider(create: (context) => TaksasiUnitChangeNotifier()),
            ChangeNotifierProvider(create: (context) => MarketingNotesChangeNotifier()),
            ChangeNotifierProvider(create: (context) => ResultSurveyChangeNotifier()),
            ChangeNotifierProvider(create: (context) => TaskListChangeNotifier()),
            ChangeNotifierProvider(create: (context) => FormAOSChangeNotifier()),
            ChangeNotifierProvider(create: (context) => FormIAChangeNotifier()),
            ChangeNotifierProvider(create: (context) => SAJobMayorChangeNotifier()),
            ChangeNotifierProvider(create: (context) => SearchPEPChangeNotifier()),
            ChangeNotifierProvider(create: (context) => InfoWMPChangeNotifier()),
            ChangeNotifierProvider(create: (context) => HomeChangeNotifier()),
            ChangeNotifierProvider(create: (context) => DedupIndiChangeNotifier()),
            ChangeNotifierProvider(create: (context) => DedupCompanyChangeNotifier()),
            ChangeNotifierProvider(create: (context) => FormMAddAddressIndividuChangeNotifier()),
            ChangeNotifierProvider(create: (context) => FormMAddAddressCompanyChangeNotifier()),
            ChangeNotifierProvider(create: (context) => FormMAddMajorInsuranceChangeNotifier()),
            ChangeNotifierProvider(create: (context) => FormMAddAdditionalInsuranceChangeNotifier()),
            ChangeNotifierProvider(create: (context) => FormMLookupUTJChangeNotif()),
            ChangeNotifierProvider(create: (context) => ListOrderTrackingChangeNotifier()),
            ChangeNotifierProvider(create: (context) => ListSurveyPhotoChangeNotifier()),
            ChangeNotifierProvider(create: (context) => AddPemegangSahamPribadiChangeNotifier()),
            ChangeNotifierProvider(create: (context) => AddPemegangSahamLembagaChangeNotif()),
            ChangeNotifierProvider(create: (context) => SearchStatusLocationChangeNotif()),
            ChangeNotifierProvider(create: (context) => SearchBusinessLocationChangeNotif()),
          ],
          child: MyApp()
      )
  );
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Ad1MS2',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        visualDensity: VisualDensity.adaptivePlatformDensity,
        primarySwatch: primaryOrange,
        fontFamily: "NunitoSans",
        primaryColor: Colors.black,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  DbHelper _dbHelper = DbHelper();
  final storage = FlutterSecureStorage();

  @override
  void initState() {
    super.initState();
    // _checkLocation();
    _checkVersion();
    // _makePath();
    // _getAllURL();
  }

  Future<void> _checkVersion() async{
    print("MASUK CHECK VERSION");
    final ioc = new HttpClient();
    ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;

    final _http = IOClient(ioc);
    print("${BaseUrl.urlGeneralPublic}ms2/api/list-param/get_list_param");
    try{
      final _response = await _http.get("${BaseUrl.urlGeneralPublic}ms2/api/list-param/get_list_param",).timeout(Duration(seconds: 60));
      if(_response.statusCode == 200){
        final _result = jsonDecode(_response.body);
        for(int i=0; i<_result['model'].length; i++){
          //xapikey
          if(_result['model'][i]['PARAM_NAME'] == "KeyECM"){
            xApiKey = _result['model'][i]['PARAM_VALUE'];
            print("api key $xApiKey");
          }
          //check version
          else if(_result['model'][i]['PARAM_NAME'] == "Version"){
            print("versi ${_result['model'][i]['PARAM_VALUE']} == ${versionApp(1)}");
            if(_result['model'][i]['PARAM_VALUE'] == versionApp(1)){
              print("sesuai app");
              _checkLocation();
              _makePath();
              _getAllURL();
            }
            else{
              print("update app");
              return showDialog(
                  barrierDismissible: false,
                  context: this.context,
                  child: AlertDialog(
                    title: Text('Get the latest MS2 updates', style: TextStyle(fontWeight: FontWeight.bold),),
                    content: SingleChildScrollView(
                      child: ListBody(
                        children: <Widget>[
                          Text('Invalid Version, please get the latest MS2 updates.'),
                        ],
                      ),
                    ),
                    // actions: <Widget>[
                    //   TextButton(
                    //     child: Text('Close'),
                    //     onPressed: () {
                    //       Navigator.of(context).pop();
                    //       Navigator.of(context).pop();
                    //     },
                    //   ),
                    // ],
                  )
              );
            }
          }
        }
        print("cek hasil $_result");
      }
      else{
        throw Exception("Failed get data error ${_response.statusCode}");
      }
    }
    on TimeoutException catch(_){
      print("MASUK SINI");
      throw "Timeout connection";
    }
    catch (e) {
      throw "${e.toString()}";
    }
  }

  _checkLocation() async {
    locationPlugin.Location location = new locationPlugin.Location();
    locationPlugin.PermissionStatus _permissionStatus;

    bool _serviceEnable = await location.serviceEnabled();
    _permissionStatus = await location.requestPermission();

    if (_serviceEnable) {
      while (_permissionStatus == locationPlugin.PermissionStatus.denied) {
        _permissionStatus = await location.requestPermission();
      }
    } else {
      while (!_serviceEnable) {
        _serviceEnable = await location.requestService();
      }
      while (_permissionStatus == locationPlugin.PermissionStatus.denied) {
        _permissionStatus = await location.requestPermission();
      }
    }
    if (_permissionStatus != locationPlugin.PermissionStatus.deniedForever) {
      Timer(Duration(milliseconds: 500), () {
        _initDatabase();
      });
    } else {
      _showAlertDialogPermission();
    }
  }

  _initDatabase() async {
    // SetSSL().setSSLFromAsset();
    _dbHelper.initDatabase();
    Navigator.pushReplacement(
        context, MaterialPageRoute(
            builder: (context) => LoginPage(
            //     create: (context) => LoginChangeNotifier(),
            //     child: LoginPage(),
            )
        )
    );
  }

  _makePath() async{
      Directory _dirLoc = await getApplicationDocumentsDirectory();
      globalPath = "${_dirLoc.path}/ADIRA";
      print("ini path : $globalPath");

      var dir = Directory(globalPath);
      bool dirExists = await dir.exists();
      print(dirExists);
      if(!dirExists){
          dir.create(recursive: true); //pass recursive as true if directory is recursive
      }
  }

  _getAllURL() async{
      final ioc = new HttpClient();
      ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;

      final _http = IOClient(ioc);
      print("${BaseUrl.urlGeneralPublic}ms2/api/api-config/get_config");
      try{
          final _response = await _http.get("${BaseUrl.urlGeneralPublic}ms2/api/api-config/get_config",).timeout(Duration(seconds: 60));
          if(_response.statusCode == 200){
              final _result = jsonDecode(_response.body);
              for(int i=0; i<_result.length; i++){
                  await storage.write(key: _result[i]['DESKRIPSI'], value: _result[i]['KODE']);
              }
              print("cek url login ${await storage.read(key: "Login")}");
          }
          else{
              throw Exception("Failed get data error ${_response.statusCode}");
          }
      }
      on TimeoutException catch(_){
        print("MASUK SINI");
          throw "Timeout connection";
      }
      catch (e) {
          throw "${e.toString()}";
      }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: myPrimaryColor,
      body: Center(
        child: Image.asset('img/logo_adira.png'),
      ),
    );
  }

  _showAlertDialogPermission() {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return Theme(
            data: ThemeData(fontFamily: "NunitoSans"),
            child: AlertDialog(
              title: Text("Permission needed"),
              content: Text(
                  "This app need location permission,\nplease allow app for access this location\nin Setting"),
              actions: <Widget>[
                FlatButton(
                    onPressed: () {
                      Navigator.pop(context);
                      SystemChannels.platform
                          .invokeMethod('SystemNavigator.pop');
                    },
                    child: Text("OK"))
              ],
            ),
          );
        });
  }
}

//// import 'dart:async';
//
//// import 'package:connectivity/connectivity.dart';
//import 'package:ad1ms2_dev/screens/login.dart';
//import 'package:ad1ms2_dev/shared/loading.dart';
//import 'package:flutter/material.dart';
//import 'package:location/location.dart';
//// import 'package:AD1MS2/enums/connectivity_status.dart';
//// import 'package:AD1MS2/services/connectivity_service.dart';
//// import 'package:provider/provider.dart';
//import 'package:permission_handler/permission_handler.dart' as handler;
//
//void main() {
//  runApp(Main());
//}
//
//class Main extends StatefulWidget {
//  @override
//  _MainState createState() => _MainState();
//}
//
//class _MainState extends State<Main> {
//  bool _loading = true;
//
//  Location _locationService = new Location();
//
//  @override
//  void initState() {
//    super.initState();
//    // _checkConnection();
//    // check Permission
//    checkPermission();
//  }
//
//  handler.PermissionStatus permissionStatusLocation;
//
//  checkPermission() async {
//    await _locationService.changeSettings(
//        accuracy: LocationAccuracy.high, interval: 1000);
//
//    bool serviceStatus = await _locationService.serviceEnabled();
//
//    if (serviceStatus) {
//      print("serviceStatus $serviceStatus");
//      permissionStatusLocation = await handler.PermissionHandler()
//          .checkPermissionStatus(handler.PermissionGroup.location);
//      if (permissionStatusLocation == handler.PermissionStatus.denied) {
//        Map<handler.PermissionGroup, handler.PermissionStatus> permissions =
//            await handler.PermissionHandler()
//                .requestPermissions([handler.PermissionGroup.location]);
//        print(
//            "cek locPermissionStatus: ${permissions['PermissionGroup.location']}");
//        stateCheck();
//      } else {
//        stateCheck();
//      }
//    }
//  }
//
//  void stateCheck() {
//    setState(() {
//      _loading = !_loading;
//    });
//  }
//
//  @override
//  Widget build(BuildContext context) {
//    return _loading
//        ? Loading()
//        : MaterialApp(
//            debugShowCheckedModeBanner: false,
//            home: Login(),
//          );
//    // return StreamProvider<ConnectivityStatus>(
//    //   create: (context) => ConnectivityService().connectionStatusController,
//    // );
//  }
//}
