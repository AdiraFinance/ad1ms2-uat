import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/asset_type_model.dart';
import 'package:ad1ms2_dev/models/electricity_type_model.dart';
import 'package:ad1ms2_dev/models/ownership_model.dart';
import 'package:ad1ms2_dev/models/result_survey_asset_model.dart';
import 'package:ad1ms2_dev/models/road_type_model.dart';
import 'package:ad1ms2_dev/shared/decimal_text_input_formatter.dart';
import 'package:ad1ms2_dev/shared/survey/result_survey_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class WidgetAddEditAsset extends StatefulWidget {
  final int flag;
  final int index;
  final ResultSurveyAssetModel model;

  const WidgetAddEditAsset({this.flag, this.index, this.model});
  @override
  _WidgetAddEditAssetState createState() => _WidgetAddEditAssetState();
}

class _WidgetAddEditAssetState extends State<WidgetAddEditAsset> {
  Future<void> _setValue;

  @override
  void initState() {
    super.initState();
    // if(widget.flag!=0){
    //   _setValue = Provider.of<ResultSurveyChangeNotifier>(context,listen: false).setValueEditResultSurveyDetailAsset(widget.model);
    // }
    // else{
      _setValue = Provider.of<ResultSurveyChangeNotifier>(context,listen: false).getRoadType(widget.model,widget.flag,widget.index);
    // }
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
          fontFamily: "NunitoSans",
          accentColor: myPrimaryColor,
          primaryColor: Colors.black,
          primarySwatch: primaryOrange
      ),
      child: Scaffold(
        appBar: AppBar(
          title: Text(
              widget.flag == 0 ? "Tambah Aset" : "Edit Aset",
              style: TextStyle(color: Colors.black)
          ),
          backgroundColor: myPrimaryColor,
          iconTheme: IconThemeData(
              color: Colors.black
          ),
        ),
        body: Consumer<ResultSurveyChangeNotifier>(
          builder: (context, value, child) {
            return
            //   widget.flag == 0
            //     ?
            // SingleChildScrollView(
            //   padding: EdgeInsets.symmetric(
            //       vertical: MediaQuery.of(context).size.height / 47,
            //       horizontal: MediaQuery.of(context).size.width / 27),
            //   child: Form(
            //     key: value.keyResultSurveyAsset,
            //     onWillPop: _onWillPop,
            //     child: Column(
            //       children: [
            //         DropdownButtonFormField<AssetTypeModel>(
            //             autovalidate: value.autoValidateResultSurveyCreateEditDetailSurvey,
            //             validator: (e) {
            //               if (e == null) {
            //                 return "Silahkan pilih jenis identitas";
            //               } else {
            //                 return null;
            //               }
            //             },
            //             value: value.assetTypeSelected,
            //             onChanged: (data) {
            //               value.assetTypeSelected = data;
            //             },
            //             onTap: () {
            //               FocusManager.instance.primaryFocus.unfocus();
            //             },
            //             decoration: InputDecoration(
            //               labelText: "Jenis Aset",
            //               border: OutlineInputBorder(),
            //               contentPadding:
            //               EdgeInsets.symmetric(horizontal: 10),
            //             ),
            //             items: value
            //                 .listAssetType
            //                 .map((value) {
            //               return DropdownMenuItem<AssetTypeModel>(
            //                 value: value,
            //                 child: Text(
            //                   value.name,
            //                   overflow: TextOverflow.ellipsis,
            //                 ),
            //               );
            //             }).toList()
            //         ),
            //         SizedBox(height: MediaQuery.of(context).size.height / 47),
            //         TextFormField(
            //             validator: (e) {
            //               if (e.isEmpty) {
            //                 return "Tidak boleh kosong";
            //               } else {
            //                 return null;
            //               }
            //             },
            //             controller: value.controllerValueAsset,
            //             autovalidate: value.autoValidateResultSurveyAssetModel,
            //             decoration: new InputDecoration(
            //                 labelText: 'Nilai',
            //                 labelStyle: TextStyle(color: Colors.black),
            //                 border: OutlineInputBorder(
            //                     borderRadius: BorderRadius.circular(8))
            //             )
            //         ),
            //         SizedBox(height: MediaQuery.of(context).size.height / 47),
            //         DropdownButtonFormField<OwnershipModel>(
            //             autovalidate: value.autoValidateResultSurveyAssetModel,
            //             validator: (e) {
            //               if (e == null) {
            //                 return "Silahkan pilih jenis identitas";
            //               } else {
            //                 return null;
            //               }
            //             },
            //             value: value.ownershipSelected,
            //             onChanged: (data) {
            //               value.ownershipSelected = data;
            //             },
            //             onTap: () {
            //               FocusManager.instance.primaryFocus.unfocus();
            //             },
            //             decoration: InputDecoration(
            //               labelText: "Kepemilikan",
            //               border: OutlineInputBorder(),
            //               contentPadding:
            //               EdgeInsets.symmetric(horizontal: 10),
            //             ),
            //             items: value
            //                 .listOwnership
            //                 .map((value) {
            //               return DropdownMenuItem<OwnershipModel>(
            //                 value: value,
            //                 child: Text(
            //                   value.name,
            //                   overflow: TextOverflow.ellipsis,
            //                 ),
            //               );
            //             }).toList()
            //         ),
            //         SizedBox(height: MediaQuery.of(context).size.height / 47),
            //         value.assetTypeSelected.id != "001" || value.assetTypeSelected.id != "002"
            //             ?
            //         Column(
            //           children: [
            //             TextFormField(
            //                 validator: (e) {
            //                   if (e.isEmpty) {
            //                     return "Tidak boleh kosong";
            //                   } else {
            //                     return null;
            //                   }
            //                 },
            //                 keyboardType: TextInputType.number,
            //                 inputFormatters: [
            //                   WhitelistingTextInputFormatter.digitsOnly
            //                 ],
            //                 controller: value.controllerSurfaceBuildingArea,
            //                 autovalidate: value.autoValidateResultSurveyAssetModel,
            //                 decoration: new InputDecoration(
            //                     labelText: 'Ukuran Luas Tanah/Luas Bangunan (m2)',
            //                     labelStyle: TextStyle(color: Colors.black),
            //                     border: OutlineInputBorder(
            //                         borderRadius: BorderRadius.circular(8))
            //                 )
            //             ),
            //             SizedBox(height: MediaQuery.of(context).size.height / 47),
            //             DropdownButtonFormField<RoadTypeModel>(
            //             // TextFormField (
            //                 autovalidate: value.autoValidateResultSurveyAssetModel,
            //                 validator: (e) {
            //                   if (e == null) {
            //                     return "Silahkan pilih jenis identitas";
            //                   } else {
            //                     return null;
            //                   }
            //                 },
            //                 value: value.roadTypeSelected,
            //                 onChanged: (data) {
            //                   value.roadTypeSelected = data;
            //                 },
            //                 onTap: () {
            //                   FocusManager.instance.primaryFocus.unfocus();
            //                 },
            //                 // controller: value.controllerRoadType,
            //                 // onTap: () {
            //                 //   FocusManager.instance.primaryFocus.unfocus();
            //                 //   value.getRoadType();
            //                 // },
            //                 decoration: InputDecoration(
            //                   labelText: "Jenis Jalan",
            //                   border: OutlineInputBorder(),
            //                   contentPadding:
            //                   EdgeInsets.symmetric(horizontal: 10),
            //                 ),
            //                 items: value
            //                     .listRoadType
            //                     .map((value) {
            //                   return DropdownMenuItem<RoadTypeModel>(
            //                     value: value,
            //                     child: Text(
            //                       "${value.kode} - ${value.deskripsi}",
            //                       overflow: TextOverflow.ellipsis,
            //                     ),
            //                   );
            //                 }).toList()
            //             ),
            //             SizedBox(height: MediaQuery.of(context).size.height / 47),
            //             DropdownButtonFormField<ElectricityTypeModel>(
            //                 autovalidate: value.autoValidateResultSurveyAssetModel,
            //                 validator: (e) {
            //                   if (e == null) {
            //                     return "Silahkan pilih jenis identitas";
            //                   } else {
            //                     return null;
            //                   }
            //                 },
            //                 value: value.electricityTypeSelected,
            //                 onChanged: (data) {
            //                   value.electricityTypeSelected = data;
            //                 },
            //                 onTap: () {
            //                   FocusManager.instance.primaryFocus.unfocus();
            //                 },
            //                 decoration: InputDecoration(
            //                   labelText: "Listrik",
            //                   border: OutlineInputBorder(),
            //                   contentPadding:
            //                   EdgeInsets.symmetric(horizontal: 10),
            //                 ),
            //                 items: value
            //                     .listElectricityType
            //                     .map((value) {
            //                   return DropdownMenuItem<ElectricityTypeModel>(
            //                     value: value,
            //                     child: Text("${value.PARA_ELECTRICITY_ID} - ${value.PARA_ELECTRICITY_NAME}",
            //                       overflow: TextOverflow.ellipsis,
            //                     ),
            //                   );
            //                 }).toList()
            //             ),
            //             SizedBox(height: MediaQuery.of(context).size.height / 47),
            //             TextFormField(
            //                 validator: (e) {
            //                   if (e.isEmpty) {
            //                     return "Tidak boleh kosong";
            //                   } else {
            //                     return null;
            //                   }
            //                 },
            //                 keyboardType: TextInputType.number,
            //                 textAlign: TextAlign.end,
            //                 onFieldSubmitted: (data) {
            //                   value.controllerElectricityBills.text = value.formatCurrency.formatCurrency(data);
            //                 },
            //                 textInputAction: TextInputAction.done,
            //                 inputFormatters: [
            //                   DecimalTextInputFormatter(decimalRange: 2),
            //                 ],
            //                 controller: value.controllerElectricityBills,
            //                 autovalidate: value.autoValidateResultSurveyAssetModel,
            //                 decoration: new InputDecoration(
            //                     labelText: 'Tagihan Listrik',
            //                     labelStyle: TextStyle(color: Colors.black),
            //                     border: OutlineInputBorder(
            //                         borderRadius: BorderRadius.circular(8))
            //                 )
            //             ),
            //             SizedBox(height: MediaQuery.of(context).size.height / 47),
            //             TextFormField(
            //                 keyboardType: TextInputType.number,
            //                 inputFormatters: [
            //                   WhitelistingTextInputFormatter.digitsOnly
            //                 ],
            //                 readOnly: true,
            //                 onTap: (){
            //                   value.selectEndDateLease(context);
            //                 },
            //                 controller: value.controllerEndDateLease,
            //                 decoration: new InputDecoration(
            //                     labelText: 'Tanggal Akhir Kontrak/Sewa',
            //                     labelStyle: TextStyle(color: Colors.black),
            //                     border: OutlineInputBorder(
            //                         borderRadius: BorderRadius.circular(8))
            //                 )
            //             ),
            //             SizedBox(height: MediaQuery.of(context).size.height / 47),
            //             TextFormField(
            //                 validator: (e) {
            //                   if (e.isEmpty) {
            //                     return "Tidak boleh kosong";
            //                   } else {
            //                     return null;
            //                   }
            //                 },
            //                 keyboardType: TextInputType.number,
            //                 textAlign: TextAlign.end,
            //                 textInputAction: TextInputAction.done,
            //                 inputFormatters: [
            //                   WhitelistingTextInputFormatter.digitsOnly
            //                 ],
            //                 controller: value.controllerLengthStay,
            //                 autovalidate: value.autoValidateResultSurveyAssetModel,
            //                 decoration: new InputDecoration(
            //                     labelText: 'Lama Tinggal (bln)',
            //                     labelStyle: TextStyle(color: Colors.black),
            //                     border: OutlineInputBorder(
            //                         borderRadius: BorderRadius.circular(8))
            //                 )
            //             ),
            //           ],
            //         )
            //             :
            //         SizedBox()
            //       ],
            //     ),
            //   ),
            // )
            //     :
            FutureBuilder(
              future: _setValue,
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                }
                return SingleChildScrollView(
                  padding: EdgeInsets.symmetric(
                      vertical: MediaQuery.of(context).size.height / 47,
                      horizontal: MediaQuery.of(context).size.width / 27),
                  child: Form(
                    key: value.keyResultSurveyAsset,
                    onWillPop: _onWillPop,
                    child: Column(
                      children: [
                        Visibility(
                          visible: value.isAssetTypeVisible(),
                          child: IgnorePointer(
                            ignoring: value.isAssetTypeEnabled(),
                            child: DropdownButtonFormField<AssetTypeModel>(
                                autovalidate: value.autoValidateResultSurveyCreateEditDetailSurvey,
                                validator: (e) {
                                  if (e == null && value.mandatoryFieldResultSurveyBerhasil) {
                                    return "Silahkan pilih jenis aset";
                                  } else {
                                    return null;
                                  }
                                },
                                value: value.assetTypeSelected,
                                onChanged: (data) {
                                  value.assetTypeSelected = data;
                                },
                                onTap: () {
                                  FocusManager.instance.primaryFocus.unfocus();
                                },
                                decoration: InputDecoration(
                                  labelText: "Jenis Aset",
                                  border: OutlineInputBorder(),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: value.isAssetTypeChanges ? Colors.purple : Colors.black)
                                  ),
                                  disabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: value.isAssetTypeChanges ? Colors.purple : Colors.black)
                                  ),
                                  contentPadding:
                                  EdgeInsets.symmetric(horizontal: 10),
                                ),
                                items: value.listAssetType.map((value) {
                                  return DropdownMenuItem<AssetTypeModel>(
                                    value: value,
                                    child: Text(
                                      value.name,
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                  );
                                }).toList()
                            ),
                          ),
                        ),
                        Visibility(
                            visible: value.isAssetTypeVisible(),
                            child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                        Visibility(
                          visible: value.isValueAssetVisible(),
                          child: IgnorePointer(
                            ignoring: value.isValueAssetEnabled(),
                            child: TextFormField(
                              validator: (e) {
                                if (e == null && value.mandatoryFieldResultSurveyBerhasil) {
                                  return "Tidak boleh kosong";
                                } else {
                                  return null;
                                }
                              },
                              controller: value.controllerValueAsset,
                              // enabled: value.isValueAssetEnabled(),
                              autovalidate: value.autoValidateResultSurveyAssetModel,
                              decoration: new InputDecoration(
                                labelText: 'Nilai',
                                labelStyle: TextStyle(color: Colors.black),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(8)
                                ),
                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: value.isValueAssetChanges ? Colors.purple : Colors.black)
                                ),
                                disabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: value.isValueAssetChanges ? Colors.purple : Colors.black)
                                ),
                              ),
                              keyboardType: TextInputType.number,
                              textAlign: TextAlign.end,
                              onFieldSubmitted: (data) {
                                value.controllerValueAsset.text = value.formatCurrency.formatCurrency(data);
                              },
                              onTap: () {
                                value.formattingSurveyAsset();
                              },
                              textInputAction: TextInputAction.done,
                              inputFormatters: [
                                DecimalTextInputFormatter(decimalRange: 2),
                                value.amountValidator
                              ],
                            ),
                          ),
                        ),
                        Visibility(
                            visible: value.isValueAssetVisible(),
                            child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                        Visibility(
                          visible: value.isIdentityInfoVisible(),
                          child: IgnorePointer(
                            ignoring: value.isIdentityInfoEnabled(),
                            child: DropdownButtonFormField<OwnershipModel>(
                                autovalidate: value.autoValidateResultSurveyAssetModel,
                                validator: (e) {
                                  if (e == null && value.mandatoryFieldResultSurveyBerhasil) {
                                    return "Silahkan pilih kepemilikan";
                                  } else {
                                    return null;
                                  }
                                },
                                value: value.ownershipSelected,
                                onChanged: (data) {
                                  value.ownershipSelected = data;
                                },
                                onTap: () {
                                  FocusManager.instance.primaryFocus.unfocus();
                                  value.formattingSurveyAsset();
                                },
                                decoration: InputDecoration(
                                  labelText: "Kepemilikan",
                                  border: OutlineInputBorder(),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: value.isIdentityInfoChanges ? Colors.purple : Colors.black)
                                  ),
                                  disabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: value.isIdentityInfoChanges ? Colors.purple : Colors.black)
                                  ),
                                  contentPadding:
                                  EdgeInsets.symmetric(horizontal: 10),
                                ),
                                items: value.listOwnership.map((value) {
                                  return DropdownMenuItem<OwnershipModel>(
                                    value: value,
                                    child: Text(
                                      value.name,
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                  );
                                }).toList()
                            ),
                          ),
                        ),
                        Visibility(
                            visible: value.isIdentityInfoVisible(),
                            child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                        value.assetTypeSelected.id == "003"
                            ?
                        Column(
                          children: [
                            Visibility(
                                visible: value.isSurfaceBuildingAreaInfoVisible(),
                                child: IgnorePointer(
                                  ignoring: value.isSurfaceBuildingAreaInfoEnabled(),
                                  child: TextFormField(
                                      validator: (e) {
                                        if (e == null && value.mandatoryFieldResultSurveyBerhasil) {
                                          return "Tidak boleh kosong";
                                        } else {
                                          return null;
                                        }
                                      },
                                      keyboardType: TextInputType.text,
                                      inputFormatters: [
                                        // WhitelistingTextInputFormatter.digitsOnly,
                                        FilteringTextInputFormatter.allow(RegExp('[0-9 ./]')),
                                      ],
                                      controller: value.controllerSurfaceBuildingArea,
                                      // enabled: value.isSurfaceBuildingAreaInfoEnabled(),
                                      autovalidate: value.autoValidateResultSurveyAssetModel,
                                      decoration: new InputDecoration(
                                        labelText: 'Ukuran Luas Tanah/Luas Bangunan (m2)',
                                        labelStyle: TextStyle(color: Colors.black),
                                        border: OutlineInputBorder(
                                            borderRadius: BorderRadius.circular(8)
                                        ),
                                        enabledBorder: OutlineInputBorder(
                                            borderSide: BorderSide(color: value.isSurfaceBuildingAreaInfoChanges ? Colors.purple : Colors.black)
                                        ),
                                        disabledBorder: OutlineInputBorder(
                                            borderSide: BorderSide(color: value.isSurfaceBuildingAreaInfoChanges ? Colors.purple : Colors.black)
                                        ),
                                      ),
                                      onTap: () {
                                        value.formattingSurveyAsset();
                                      }
                                  ),
                                ),
                            ),
                            Visibility(
                                visible: value.isSurfaceBuildingAreaInfoVisible(),
                                child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                            Visibility(
                                visible: value.isRoadTypeInfoVisible(),
                                child: IgnorePointer(
                                  ignoring: value.isRoadTypeInfoEnabled(),
                                  child: DropdownButtonFormField<RoadTypeModel>(
                                      autovalidate: value.autoValidateResultSurveyAssetModel,
                                      validator: (e) {
                                        if (e == null && value.mandatoryFieldResultSurveyBerhasil) {
                                          return "Silahkan pilih jenis jalan";
                                        } else {
                                          return null;
                                        }
                                      },
                                      value: value.roadTypeSelected,
                                      onChanged: (data) {
                                        value.roadTypeSelected = data;
                                      },
                                      onTap: () {
                                        FocusManager.instance.primaryFocus.unfocus();
                                        value.formattingSurveyAsset();
                                      },
                                      decoration: InputDecoration(
                                        labelText: "Jenis Jalan",
                                        border: OutlineInputBorder(),
                                        enabledBorder: OutlineInputBorder(
                                            borderSide: BorderSide(color: value.isRoadTypeInfoChanges ? Colors.purple : Colors.black)
                                        ),
                                        disabledBorder: OutlineInputBorder(
                                            borderSide: BorderSide(color: value.isRoadTypeInfoChanges ? Colors.purple : Colors.black)
                                        ),
                                        contentPadding:
                                        EdgeInsets.symmetric(horizontal: 10),
                                      ),
                                      items: value
                                          .listRoadType
                                          .map((value) {
                                        return DropdownMenuItem<RoadTypeModel>(
                                          value: value,
                                          child: Text(
                                            "${value.kode} - ${value.deskripsi}",
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                        );
                                      }).toList()
                                  ),
                                ),
                            ),
                            Visibility(
                                visible: value.isRoadTypeInfoVisible(),
                                child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                            Visibility(
                                visible: value.isElectricityTypeInfoVisible(),
                                child: IgnorePointer(
                                  ignoring: value.isElectricityTypeInfoEnabled(),
                                  child: DropdownButtonFormField<ElectricityTypeModel>(
                                      autovalidate: value.autoValidateResultSurveyAssetModel,
                                      validator: (e) {
                                        if (e == null && value.mandatoryFieldResultSurveyBerhasil) {
                                          return "Silahkan pilih listrik";
                                        } else {
                                          return null;
                                        }
                                      },
                                      value: value.electricityTypeSelected,
                                      onChanged: (data) {
                                        value.electricityTypeSelected = data;
                                      },
                                      onTap: () {
                                        FocusManager.instance.primaryFocus.unfocus();
                                        value.formattingSurveyAsset();
                                      },
                                      decoration: InputDecoration(
                                        labelText: "Listrik",
                                        border: OutlineInputBorder(),
                                        enabledBorder: OutlineInputBorder(
                                            borderSide: BorderSide(color: value.isElectricityTypeInfoChanges ? Colors.purple : Colors.black)
                                        ),
                                        disabledBorder: OutlineInputBorder(
                                            borderSide: BorderSide(color: value.isElectricityTypeInfoChanges ? Colors.purple : Colors.black)
                                        ),
                                        contentPadding:
                                        EdgeInsets.symmetric(horizontal: 10),
                                      ),
                                      items: value
                                          .listElectricityType
                                          .map((value) {
                                        return DropdownMenuItem<ElectricityTypeModel>(
                                          value: value,
                                          child: Text("${value.PARA_ELECTRICITY_ID} - ${value.PARA_ELECTRICITY_NAME}",
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                        );
                                      }).toList()
                                  ),
                                ),
                            ),
                            Visibility(
                                visible: value.isElectricityTypeInfoVisible(),
                                child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                            Visibility(
                                visible: value.isElectricityBillInfoVisible(),
                                child: IgnorePointer(
                                  ignoring: value.isElectricityBillInfoEnabled(),
                                  child: TextFormField(
                                      validator: (e) {
                                        if (e == null && value.mandatoryFieldResultSurveyBerhasil) {
                                          return "Tidak boleh kosong";
                                        } else {
                                          return null;
                                        }
                                      },
                                      keyboardType: TextInputType.number,
                                      textAlign: TextAlign.end,
                                      onFieldSubmitted: (data) {
                                        value.controllerElectricityBills.text = value.formatCurrency.formatCurrency(data);
                                      },
                                      onTap: () {
                                        value.formattingSurveyAsset();
                                      },
                                      textInputAction: TextInputAction.done,
                                      inputFormatters: [
                                        DecimalTextInputFormatter(decimalRange: 2),
                                      ],
                                      controller: value.controllerElectricityBills,
                                      // enabled: value.isElectricityBillInfoEnabled(),
                                      autovalidate: value.autoValidateResultSurveyAssetModel,
                                      decoration: new InputDecoration(
                                        labelText: 'Tagihan Listrik',
                                        labelStyle: TextStyle(color: Colors.black),
                                        border: OutlineInputBorder(
                                            borderRadius: BorderRadius.circular(8)
                                        ),
                                        enabledBorder: OutlineInputBorder(
                                            borderSide: BorderSide(color: value.isElectricityBillInfoChanges ? Colors.purple : Colors.black)
                                        ),
                                        disabledBorder: OutlineInputBorder(
                                            borderSide: BorderSide(color: value.isElectricityBillInfoChanges ? Colors.purple : Colors.black)
                                        ),
                                      )
                                  ),
                                ),
                            ),
                            Visibility(
                                visible: value.isElectricityBillInfoVisible(),
                                child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                            Visibility(
                                visible: value.isEndDateInfoVisible(),
                                child: IgnorePointer(
                                  ignoring: value.isEndDateInfoEnabled(),
                                  child: TextFormField(
                                      validator: (e) {
                                        if ((e == null && value.mandatoryTanggalKontrak) || (value.ownershipSelected != null ? value.ownershipSelected.id == "05" && e.isEmpty : false)) { //&& value.isEndDateInfoMandatory()
                                          return "Tidak boleh kosong";
                                        } else {
                                          return null;
                                        }
                                      },
                                      keyboardType: TextInputType.number,
                                      inputFormatters: [
                                        WhitelistingTextInputFormatter.digitsOnly
                                      ],
                                      readOnly: true,
                                      onTap: (){
                                        value.selectEndDateLease(context);
                                        value.formattingSurveyAsset();
                                      },
                                      controller: value.controllerEndDateLease,
                                      autovalidate: value.autoValidateResultSurveyAssetModel,
                                      // enabled: value.isEndDateInfoEnabled(),
                                      decoration: new InputDecoration(
                                        labelText: 'Tanggal Akhir Kontrak/Sewa',
                                        labelStyle: TextStyle(color: Colors.black),
                                        border: OutlineInputBorder(
                                            borderRadius: BorderRadius.circular(8)
                                        ),
                                        enabledBorder: OutlineInputBorder(
                                            borderSide: BorderSide(color: value.isEndDateInfoChanges ? Colors.purple : Colors.black)
                                        ),
                                        disabledBorder: OutlineInputBorder(
                                            borderSide: BorderSide(color: value.isEndDateInfoChanges ? Colors.purple : Colors.black)
                                        ),
                                        suffixIconConstraints: BoxConstraints(
                                          minWidth: 20,
                                          minHeight: 20,
                                          maxHeight: 25,
                                          maxWidth: 30,),
                                        suffixIcon: GestureDetector(
                                          onTap: () {
                                            value.clearFieldData();
                                          },
                                          child: Padding(
                                              padding: EdgeInsets.only(right: 12.0),
                                              child: Container(
                                                child: Icon(Icons.close, color: Colors.grey,),
                                              )
                                          ),
                                        ),
                                      )
                                  ),
                                ),
                            ),
                            Visibility(
                                visible: value.isEndDateInfoVisible(),
                                child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                            Visibility(
                                visible: value.isLengthStayInfoVisible(),
                                child: IgnorePointer(
                                  ignoring: value.isLengthStayInfoEnabled(),
                                  child: TextFormField(
                                      validator: (e) {
                                        if (e == null ) { //&& value.isLengthStayInfoMandatory()
                                          return "Tidak boleh kosong";
                                        } else {
                                          return null;
                                        }
                                      },
                                      keyboardType: TextInputType.number,
                                      textAlign: TextAlign.end,
                                      textInputAction: TextInputAction.done,
                                      inputFormatters: [
                                        WhitelistingTextInputFormatter.digitsOnly
                                      ],
                                      onTap: () {
                                        value.formattingSurveyAsset();
                                      },
                                      controller: value.controllerLengthStay,
                                      // enabled: value.isLengthStayInfoEnabled(),
                                      autovalidate: value.autoValidateResultSurveyAssetModel,
                                      decoration: new InputDecoration(
                                        labelText: 'Lama Tinggal (bln)',
                                        labelStyle: TextStyle(color: Colors.black),
                                        border: OutlineInputBorder(
                                            borderRadius: BorderRadius.circular(8)
                                        ),
                                        enabledBorder: OutlineInputBorder(
                                            borderSide: BorderSide(color: value.isLengthStayInfoChanges ? Colors.purple : Colors.black)
                                        ),
                                        disabledBorder: OutlineInputBorder(
                                            borderSide: BorderSide(color: value.isLengthStayInfoChanges ? Colors.purple : Colors.black)
                                        ),
                                      )
                                  ),
                                ),
                            ),
                            Visibility(
                                visible: value.isLengthStayInfoVisible(),
                                child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                          ],
                        )
                            :
                        SizedBox()
                      ],
                    ),
                  ),
                );
              },
            );
          },
        ),
        bottomNavigationBar: BottomAppBar(
          child:  Container(
            padding: EdgeInsets.symmetric(
                horizontal: MediaQuery.of(context).size.width / 27,
                vertical: MediaQuery.of(context).size.height / 57),
            child: RaisedButton(
                color: myPrimaryColor,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8)
                ),
                onPressed: () {
                  Provider.of<ResultSurveyChangeNotifier>(context,listen: false).checkSurveyAsset(context,widget.flag,widget.index);
                },
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [Text(widget.flag == 0 ? "SAVE" : "UPDATE")]
                )
            ),
          ),
        ),
      ),
    );
  }

  Future<bool> _onWillPop() async {
    var _provider = Provider.of<ResultSurveyChangeNotifier>(context, listen: false);
    if(widget.flag == 0){
      if(_provider.assetTypeSelected.id != "001" || _provider.assetTypeSelected.id != "002"){
        if(_provider.assetTypeSelected != null || _provider.controllerValueAsset.text != ""
            || _provider.ownershipSelected != null || _provider.controllerSurfaceBuildingArea.text != ""
            || _provider.roadTypeSelected != null || _provider.electricityTypeSelected != null
            || _provider.controllerElectricityBills.text != "" || _provider.controllerEndDateLease.text != ""
            || _provider.controllerLengthStay.text != null
        ){
          return (await showDialog(
            context: context,
            builder: (myContext) => AlertDialog(
              title: new Text('Warning'),
              content: new Text('Keluar dengan simpan perubahan?'),
              actions: <Widget>[
                new FlatButton(
                  onPressed: () {
                    _provider.checkSurveyAsset(context,widget.flag, widget.index);
                    Navigator.pop(context);
                  },
                  child: new Text('Ya', style: TextStyle(color: Colors.grey)),
                ),
                new FlatButton(
                  onPressed: () => Navigator.of(context).pop(true),
                  child: new Text('Tidak'),
                ),
              ],
            ),
          )) ?? false;
        }else{
          return true;
        }
      }
      else{
        if(_provider.assetTypeSelected != null || _provider.controllerValueAsset.text != ""
            || _provider.ownershipSelected != null
        ){
          return (await showDialog(
            context: context,
            builder: (myContext) => AlertDialog(
              title: new Text('Warning'),
              content: new Text('Keluar dengan simpan perubahan?'),
              actions: <Widget>[
                new FlatButton(
                  onPressed: () {
                    _provider.checkSurveyAsset(context,widget.flag, widget.index);
                    Navigator.pop(context);
                  },
                  child: new Text('Ya', style: TextStyle(color: Colors.grey)),
                ),
                new FlatButton(
                  onPressed: () => Navigator.of(context).pop(true),
                  child: new Text('Tidak'),
                ),
              ],
            ),
          )) ?? false;
        }else{
          return true;
        }
      }
    }
    else{
      if(_provider.assetTypeSelected.id == "003"){
        if(_provider.assetTypeSelected.id != _provider.assetTypeTemp.id
            || _provider.controllerValueAsset.text != _provider.valueAssetTemp
            || _provider.ownershipSelected.id != _provider.ownershipTemp.id
            || _provider.controllerSurfaceBuildingArea.text != _provider.surfaceBuildingAreaTemp
            || _provider.roadTypeSelected.kode != _provider.roadTypeTemp.kode
            || _provider.electricityTypeSelected.PARA_ELECTRICITY_ID != _provider.electricityTypeTemp.PARA_ELECTRICITY_ID
            || _provider.controllerElectricityBills.text != _provider.electricityBillsTemp
            || _provider.controllerEndDateLease.text != _provider.endDateLeaseTemp
            || _provider.controllerLengthStay.text != _provider.lengthStayTemp
        ){
          return (await showDialog(
            context: context,
            builder: (myContext) => AlertDialog(
              title: new Text('Warning'),
              content: new Text('Simpan perubahan?'),
              actions: <Widget>[
                new FlatButton(
                  onPressed: () {
                    _provider.checkSurveyAsset(context,widget.flag, widget.index);
                    Navigator.pop(context);
                  },
                  child: new Text('Ya', style: TextStyle(color: Colors.grey)),
                ),
                new FlatButton(
                  onPressed: () => Navigator.of(context).pop(true),
                  child: new Text('Tidak'),
                ),
              ],
            ),
          )) ?? false;
        }
        else{
          return true;
        }
      }
      else{
        if(_provider.assetTypeSelected.id != _provider.assetTypeTemp.id || _provider.controllerValueAsset.text != _provider.valueAssetTemp
            || _provider.ownershipSelected.id != _provider.ownershipTemp.id){
          return (await showDialog(
            context: context,
            builder: (myContext) => AlertDialog(
              title: new Text('Warning'),
              content: new Text('Simpan perubahan?'),
              actions: <Widget>[
                new FlatButton(
                  onPressed: () {
                    _provider.checkSurveyAsset(context,widget.flag, widget.index);
                    Navigator.pop(context);
                  },
                  child: new Text('Ya', style: TextStyle(color: Colors.grey)),
                ),
                new FlatButton(
                  onPressed: () => Navigator.of(context).pop(true),
                  child: new Text('Tidak'),
                ),
              ],
            ),
          )) ?? false;
        }
        else{
          return true;
        }
      }
    }
  }
}
