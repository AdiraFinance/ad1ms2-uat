import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/recommendation_surveyor_model.dart';
import 'package:ad1ms2_dev/models/result_survey_model.dart';
import 'package:ad1ms2_dev/shared/survey/result_survey_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class WidgetResultGroupSurveyNotes extends StatefulWidget {
  @override
  _WidgetResultGroupSurveyNotesState createState() => _WidgetResultGroupSurveyNotesState();
}

class _WidgetResultGroupSurveyNotesState extends State<WidgetResultGroupSurveyNotes> {

  @override
  void initState() {
    super.initState();
    if( Provider.of<ResultSurveyChangeNotifier>(context,listen: false).listRecomSurvey.isEmpty){//&&  Provider.of<ResultSurveyChangeNotifier>(context,listen: false).listResultSurvey.isEmpty
      Provider.of<ResultSurveyChangeNotifier>(context,listen: false).getRecomSurveyNote();
      // Provider.of<ResultSurveyChangeNotifier>(context,listen: false).getStatusSurvey();
    }
    Provider.of<ResultSurveyChangeNotifier>(context,listen: false).getListHasilSurvey(context);
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
          fontFamily: "NunitoSans",
          accentColor: myPrimaryColor,
          primaryColor: Colors.black,
          primarySwatch: primaryOrange
      ),
      child: Scaffold(
        key: Provider.of<ResultSurveyChangeNotifier>(context,listen: false).scaffoldKey,
        appBar: AppBar(
          title: Text(
              "Catatan",
              style: TextStyle(color: Colors.black)
          ),
          backgroundColor: myPrimaryColor,
          iconTheme: IconThemeData(
              color: Colors.black
          ),
        ),
        body: SingleChildScrollView(
          padding: EdgeInsets.symmetric(
              horizontal: MediaQuery.of(context).size.width / 27,
              vertical: MediaQuery.of(context).size.height / 57),
          child: Consumer<ResultSurveyChangeNotifier>(
            builder: (context, value, _) {
              return Form(
                onWillPop: value.onBackPressSurveyNote,
                key: value.keyResultSurveyGroupNotes,
                child: Column(
                  children: [
                    Visibility(
                      visible: value.isResultSurveyVisible(),
                      child: IgnorePointer(
                        ignoring: value.isResultSurveyGroupNotesEnabled(),
                        child: DropdownButtonFormField<ResultSurveyModel>(
                            autovalidate: value.autoValidateResultSurveyGroupNote,
                            validator: (e) {
                              if (e == null && value.isResultSurveyMandatory() && (value.typeForm == "AOS" && value.radioValueApproved == "0")) {
                                return "Silahkan pilih hasil survey";
                              } else {
                                return null;
                              }
                            },
                            value: value.resultSurveySelected,
                            onChanged: (data) {
                              value.resultSurveySelected = data;
                              value.checkForMandatoryField(context, data);
                            },
                            onTap: () {
                              FocusManager.instance.primaryFocus.unfocus();
                            },
                            decoration: InputDecoration(
                              labelText: "Hasil Survey",
                              filled: value.isResultSurveyEnabled() ? true : false,
                              fillColor: Colors.black12,
                              border: OutlineInputBorder(),
                              enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: value.isResultSurveyChanges ? Colors.purple : Colors.black)
                              ),
                              disabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: value.isResultSurveyChanges ? Colors.purple : Colors.black)
                              ),
                              contentPadding:
                              EdgeInsets.symmetric(horizontal: 10),
                            ),
                            items: value.listResultSurvey.map((value) {
                              return DropdownMenuItem<ResultSurveyModel>(
                                value: value,
                                child: Text(
                                  value.DESKRIPSI,
                                  overflow: TextOverflow.ellipsis,
                                ),
                              );
                            }).toList()
                        ),
                      ),
                    ),
                    Visibility(
                        visible: value.isResultSurveyVisible(),
                        child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                    Visibility(
                      visible: value.isSurveyRecommendationVisible(),
                      child: IgnorePointer(
                        ignoring: value.isSurveyRecommendationEnabled(),
                        child: DropdownButtonFormField<RecommendationSurveyorModel>(
                            autovalidate: value.autoValidateResultSurveyGroupNote,
                            validator: (e) {
                              if (e == null && value.isSurveyRecommendationMandatory() && (value.typeForm == "AOS" && value.radioValueApproved == "0")) {
                                return "Silahkan pilih rekomendasi survey";
                              } else {
                                return null;
                              }
                            },
                            value: value.recommendationSurveySelected,
                            onChanged: (data) {
                              value.recommendationSurveySelected = data;
                            },
                            onTap: () {
                              FocusManager.instance.primaryFocus.unfocus();
                            },
                            decoration: InputDecoration(
                              labelText: "Rekomendasi Survey",
                              border: OutlineInputBorder(),
                              filled: value.isSurveyRecommendationEnabled() ? true : false,
                              fillColor: Colors.black12,
                              enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: value.isSurveyRecommendationChanges ? Colors.purple : Colors.black)
                              ),
                              disabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: value.isSurveyRecommendationChanges ? Colors.purple : Colors.black)
                              ),
                              contentPadding:
                              EdgeInsets.symmetric(horizontal: 10),
                            ),
                            items: value
                                .listRecomSurvey
                                .map((value) {
                              return DropdownMenuItem<RecommendationSurveyorModel>(
                                value: value,
                                child: Text(
                                  value.DESKRIPSI,
                                  overflow: TextOverflow.ellipsis,
                                ),
                              );
                            }).toList()
                        ),
                      ),
                    ),
                    Visibility(
                        visible: value.isSurveyRecommendationVisible(),
                        child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                    Visibility(
                      visible: value.isSurveyNotesVisible(),
                      child: IgnorePointer(
                        ignoring: value.isResultSurveyGroupNotesEnabled(),
                        child: TextFormField(
                          autovalidate: value.autoValidateResultSurveyGroupNote,
                          validator: (e) {
                            if (e == null && value.isSurveyNotesMandatory() && (value.typeForm == "AOS" && value.radioValueApproved == "0")) {
                              return "Tidak boleh kosong";
                            } else {
                              return null;
                            }
                          },
                          controller: value.controllerNote,
                          // enabled: value.isSurveyNotesEnabled(),
                          keyboardType: TextInputType.text,
                          textCapitalization: TextCapitalization.characters,
                          maxLines: 3,
                          decoration: new InputDecoration(
                            labelText: 'Catatan',
                            labelStyle: TextStyle(color: Colors.black),
                            filled: value.isResultSurveyGroupNotesEnabled() ? true : false,
                            fillColor: Colors.black12,
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8)),
                            enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: value.isSurveyNotesChanges ? Colors.purple : Colors.grey),
                            ),
                            disabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: value.isSurveyNotesChanges ? Colors.purple : Colors.grey),
                            ),
                            contentPadding:
                            EdgeInsets.symmetric(horizontal: 10,vertical:10),
                          ),
                          // inputFormatters: [
                          //   FilteringTextInputFormatter.allow(RegExp('[a-zA-Z 0-9]')),
                          // ],
                        ),
                      ),
                    ),
                    Visibility(
                        visible: value.isSurveyNotesVisible(),
                        child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                    Visibility(
                      visible: value.isSurveyDateVisible(),
                      child: Row(
                        children: [
                          Expanded(
                            flex: 7,
                            child: IgnorePointer(
                              ignoring: value.isSurveyDateEnabled(),
                              child: TextFormField(
                                autovalidate: value.autoValidateResultSurveyGroupNote,
                                validator: (e) {
                                  if (e == null && value.isSurveyDateMandatory() && (value.typeForm == "AOS" && value.radioValueApproved == "0")) {
                                    return "Tidak boleh kosong";
                                  } else {
                                    return null;
                                  }
                                },
                                onTap: (){
                                  value.selectDateSurveyNote(context);
                                },
                                readOnly: true,
                                controller: value.controllerResultSurveyDate,
                                // enabled: value.isSurveyDateEnabled(),
                                decoration: new InputDecoration(
                                  labelText: 'Tanggal Hasil Survey',
                                  labelStyle: TextStyle(color: Colors.black),
                                  filled: value.isSurveyDateEnabled() ? true : false,
                                  fillColor: Colors.black12,
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8)
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: value.isSurveyDateChanges ? Colors.purple : Colors.grey),
                                  ),
                                  disabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: value.isSurveyDateChanges ? Colors.purple : Colors.grey),
                                  ),
                                  contentPadding: EdgeInsets.symmetric(horizontal: 10,vertical:10),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(width: 8),
                          Expanded(
                            flex: 4,
                            child: IgnorePointer(
                              ignoring: value.isSurveyDateEnabled(),
                              child: TextFormField(
                                  validator: (e) {
                                    if (e.isEmpty && (value.typeForm == "AOS" && value.radioValueApproved == "0")) {
                                      return "Tidak boleh kosong";
                                    } else {
                                      return null;
                                    }
                                  },
                                  autovalidate: value.autoValidateResultSurveyGroupNote,
                                  controller: value.controllerResultSurveyTime,
                                  readOnly: true,
                                  onTap: () {
                                    FocusManager.instance.primaryFocus.unfocus();
                                    value.selectTimeSurveyNote(context);
                                  },
                                  decoration: InputDecoration(
                                      labelText: 'Jam Hasil Survey',
                                      labelStyle: TextStyle(color: Colors.black),
                                      filled: value.isSurveyDateEnabled() ? true : false,
                                      fillColor: Colors.black12,
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(8)),
                                      enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: value.isSurveyDateChanges ? Colors.purple : Colors.grey),
                                      ),
                                      disabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: value.isSurveyDateChanges ? Colors.purple : Colors.grey),
                                      ),
                                      contentPadding: EdgeInsets.symmetric(horizontal: 10,vertical:10),
                                  ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              );
            },
          ),
        ),
        bottomNavigationBar: BottomAppBar(
          elevation: 0.0,
          child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Consumer<ResultSurveyChangeNotifier>(
                builder: (context, resultSurveyChangeNotifier, _) {
                  return RaisedButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(8.0)),
                      color: myPrimaryColor,
                      onPressed: () {
                        resultSurveyChangeNotifier.checkSurveyNote(context);
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text("DONE",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 14,
                                  fontWeight: FontWeight.w500,
                                  letterSpacing: 1.25))
                        ],
                      ));
                },
              )),
        ),
      ),
    );
  }
}
