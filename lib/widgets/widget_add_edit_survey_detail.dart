import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/environmental_information_model.dart';
import 'package:ad1ms2_dev/models/recources_survey_info_model.dart';
import 'package:ad1ms2_dev/models/result_survey_detail_survey_model.dart';
import 'package:ad1ms2_dev/shared/survey/result_survey_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class WidgetAddEditSurveyDetail extends StatefulWidget {
  final int flag;
  final int index;
  final ResultSurveyDetailSurveyModel model;
  const WidgetAddEditSurveyDetail({this.flag, this.index, this.model});
  @override
  _WidgetAddEditSurveyDetailState createState() => _WidgetAddEditSurveyDetailState();
}

class _WidgetAddEditSurveyDetailState extends State<WidgetAddEditSurveyDetail> {
  Future<void> _setValue;

  @override
  void initState() {
    super.initState();
    // if(widget.flag!=0){
    //   _setValue = Provider.of<ResultSurveyChangeNotifier>(context,listen: false).setValueEditResultSurveyDetailSurvey(widget.model);
    // }
    _setValue = Provider.of<ResultSurveyChangeNotifier>(context, listen: false).getEnvironmentalInformation(context,widget.model,widget.flag,widget.index);
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
          fontFamily: "NunitoSans",
          accentColor: myPrimaryColor,
          primaryColor: Colors.black,
          primarySwatch: primaryOrange
      ),
      child: Scaffold(
        appBar: AppBar(
          title: Text(
              widget.flag == 0 ? "Tambah Survey Detail" : "Edit Survey Detail",
              style: TextStyle(color: Colors.black)
          ),
          backgroundColor: myPrimaryColor,
          iconTheme: IconThemeData(
              color: Colors.black
          ),
        ),
        body: Consumer<ResultSurveyChangeNotifier>(
          builder: (context, value, child) {
              return
                // widget.flag == 0
                //   ?
                // SingleChildScrollView(
                //   padding: EdgeInsets.symmetric(
                //     vertical: MediaQuery.of(context).size.height / 47,
                //     horizontal: MediaQuery.of(context).size.width / 27),
                //   child: Form(
                //     key: value.keyResultSurveyCreateEditDetailSurvey,
                //     onWillPop: _onWillPop,
                //     child: Column(
                //       children: [
                //         DropdownButtonFormField<EnvironmentalInformationModel>(
                //           autovalidate: value.autoValidateResultSurveyCreateEditDetailSurvey,
                //           validator: (e) {
                //             if (e == null) {
                //               return "Silahkan pilih informasi lingkungan";
                //             } else {
                //               return null;
                //             }
                //           },
                //           value: value.environmentalInformationSelected,
                //           onChanged: (data) {
                //             value.environmentalInformationSelected = data;
                //           },
                //           onTap: () {
                //             FocusManager.instance.primaryFocus.unfocus();
                //           },
                //           decoration: InputDecoration(
                //             labelText: "Informasi Lingkungan",
                //             border: OutlineInputBorder(),
                //             contentPadding:
                //             EdgeInsets.symmetric(horizontal: 10),
                //           ),
                //           items: value
                //               .listEnviInfo
                //               .map((value) {
                //             return DropdownMenuItem<EnvironmentalInformationModel>(
                //               value: value,
                //               child: Text(
                //                 value.name,
                //                 overflow: TextOverflow.ellipsis,
                //               ),
                //             );
                //           }).toList()
                //       ),
                //         SizedBox(height: MediaQuery.of(context).size.height / 47),
                //         DropdownButtonFormField<ResourcesInfoSurveyModel>(
                //           autovalidate: value.autoValidateResultSurveyCreateEditDetailSurvey,
                //           validator: (e) {
                //             if (e == null) {
                //               return "Silahkan pilih sumber informasi";
                //             } else {
                //               return null;
                //             }
                //           },
                //           value: value.resourcesInfoSurveySelected,
                //           onChanged: (data) {
                //             value.resourcesInfoSurveySelected = data;
                //           },
                //           onTap: () {
                //             FocusManager.instance.primaryFocus.unfocus();
                //           },
                //           decoration: InputDecoration(
                //             labelText: "Sumber Informasi",
                //             border: OutlineInputBorder(),
                //             contentPadding:
                //             EdgeInsets.symmetric(horizontal: 10),
                //           ),
                //           items: value
                //               .listResourcesInfoSurvey
                //               .map((value) {
                //             return DropdownMenuItem<ResourcesInfoSurveyModel>(
                //               value: value,
                //               child: Text(
                //                 value.PARA_INFORMATION_SOURCE_NAME,
                //                 overflow: TextOverflow.ellipsis,
                //               ),
                //             );
                //           }).toList()
                //         ),
                //         SizedBox(height: MediaQuery.of(context).size.height / 47),
                //         TextFormField(
                //           validator: (e) {
                //             if (e.isEmpty) {
                //               return "Tidak boleh kosong";
                //             } else {
                //               return null;
                //             }
                //           },
                //           controller: value.controllerResourceInformationName,
                //           autovalidate: value.autoValidateResultSurveyCreateEditDetailSurvey,
                //           decoration: new InputDecoration(
                //               labelText: 'Nama Sumber Informasi',
                //               labelStyle: TextStyle(color: Colors.black),
                //               border: OutlineInputBorder(
                //                   borderRadius: BorderRadius.circular(8))
                //           )
                //         ),
                //       ],
                //     ),
                //   ),
                // )
                //     :
                FutureBuilder(
                  future: _setValue,
                  builder: (context, snapshot) {
                    if (snapshot.connectionState == ConnectionState.waiting) {
                      return Center(
                        child: CircularProgressIndicator(),
                      );
                    }
                    return SingleChildScrollView(
                      padding: EdgeInsets.symmetric(
                          vertical: MediaQuery.of(context).size.height / 47,
                          horizontal: MediaQuery.of(context).size.width / 27),
                      child: Form(
                        key: value.keyResultSurveyCreateEditDetailSurvey,
                        onWillPop: _onWillPop,
                        child: Column(
                          children: [
                            Visibility(
                              visible: value.isEnvironmentalInfoVisible(),
                              child: IgnorePointer(
                                ignoring: value.isEnvironmentalInfoEnabled(),
                                child: DropdownButtonFormField<EnvironmentalInformationModel>(
                                    autovalidate: value.autoValidateResultSurveyCreateEditDetailSurvey,
                                    validator: (e) {
                                      if (e == null && value.mandatoryFieldResultSurveyBerhasil) {
                                        return "Silahkan pilih informasi lingkungan";
                                      } else {
                                        return null;
                                      }
                                    },
                                    value: value.environmentalInformationSelected,
                                    onChanged: (data) {
                                      value.environmentalInformationSelected = data;
                                    },
                                    onTap: () {
                                      FocusManager.instance.primaryFocus.unfocus();
                                    },
                                    decoration: new InputDecoration(
                                      labelText: 'Informasi Lingkungan',
                                      labelStyle: TextStyle(color: Colors.black),
                                      filled: value.isEnvironmentalInfoEnabled() ? true : false,
                                      fillColor: Colors.black12,
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(8)),
                                      enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: value.isEnvInformationChange ? Colors.purple : Colors.grey)),
                                      disabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: value.isEnvInformationChange ? Colors.purple : Colors.grey)),
                                      contentPadding: EdgeInsets.symmetric(horizontal: 10),),
                                    // decoration: InputDecoration(
                                    //   labelText: "Informasi Lingkungan",
                                    //   border: OutlineInputBorder(),
                                    //   contentPadding:
                                    //   EdgeInsets.symmetric(horizontal: 10),
                                    // ),
                                    items: value
                                        .listEnviInfo
                                        .map((value) {
                                      return DropdownMenuItem<EnvironmentalInformationModel>(
                                        value: value,
                                        child: Text(
                                          value.name,
                                          overflow: TextOverflow.ellipsis,
                                        ),
                                      );
                                    }).toList()
                                ),
                              ),
                            ),
                            SizedBox(height: MediaQuery.of(context).size.height / 47),
                            Visibility(
                              visible: value.isSourceInfoVisible(),
                              child: IgnorePointer(
                                ignoring: value.isSourceInfoEnabled(),
                                child: DropdownButtonFormField<ResourcesInfoSurveyModel>(
                                    autovalidate: value.autoValidateResultSurveyCreateEditDetailSurvey,
                                    validator: (e) {
                                      if (e == null && value.mandatoryFieldResultSurveyBerhasil) {
                                        return "Silahkan pilih sumber informasi";
                                      } else {
                                        return null;
                                      }
                                    },
                                    value: value.resourcesInfoSurveySelected,
                                    onChanged: (data) {
                                      value.resourcesInfoSurveySelected = data;
                                    },
                                    onTap: () {
                                      FocusManager.instance.primaryFocus.unfocus();
                                    },
                                    decoration: new InputDecoration(
                                        labelText: 'Sumber Informasi',
                                        labelStyle: TextStyle(color: Colors.black),
                                        filled: value.isSourceInfoEnabled() ? true : false,
                                        fillColor: Colors.black12,
                                        border: OutlineInputBorder(
                                            borderRadius: BorderRadius.circular(8)),
                                        enabledBorder: OutlineInputBorder(
                                            borderSide: BorderSide(color: value.isSourceInfoChanges ? Colors.purple : Colors.grey)),
                                        disabledBorder: OutlineInputBorder(
                                            borderSide: BorderSide(color: value.isSourceInfoChanges ? Colors.purple : Colors.grey)),
                                        contentPadding: EdgeInsets.symmetric(horizontal: 10)
                                    ),
                                    // decoration: InputDecoration(
                                    //   labelText: "Sumber Informasi",
                                    //   border: OutlineInputBorder(),
                                    //   contentPadding:
                                    //   EdgeInsets.symmetric(horizontal: 10),
                                    // ),
                                    items: value
                                        .listResourcesInfoSurvey
                                        .map((value) {
                                      return DropdownMenuItem<ResourcesInfoSurveyModel>(
                                        value: value,
                                        child: Text(
                                          value.PARA_INFORMATION_SOURCE_NAME,
                                          overflow: TextOverflow.ellipsis,
                                        ),
                                      );
                                    }).toList()
                                ),
                              ),
                            ),
                            SizedBox(height: MediaQuery.of(context).size.height / 47),
                            Visibility(
                              visible: value.isSourceNameInfoVisible(),
                              child: IgnorePointer(
                                ignoring: value.isSourceNameInfoEnabled(),
                                child: TextFormField(
                                  validator: (e) {
                                    if (e == null && value.mandatoryFieldResultSurveyBerhasil) {
                                      return "Tidak boleh kosong";
                                    } else {
                                      return null;
                                    }
                                  },
                                  controller: value.controllerResourceInformationName,
                                  autovalidate: value.autoValidateResultSurveyCreateEditDetailSurvey,
                                  keyboardType: TextInputType.text,
                                  textCapitalization: TextCapitalization.characters,
                                  decoration: new InputDecoration(
                                      labelText: 'Nama Sumber Informasi',
                                      labelStyle: TextStyle(color: Colors.black),
                                      filled: value.isSourceNameInfoEnabled() ? true : false,
                                      fillColor: Colors.black12,
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(8)),
                                      enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: value.isSourceNameInfoChanges ? Colors.purple : Colors.grey)),
                                      disabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: value.isSourceNameInfoChanges ? Colors.purple : Colors.grey)),
                                      contentPadding: EdgeInsets.symmetric(horizontal: 10)
                                  ),
                                  // decoration: new InputDecoration(
                                  //     labelText: 'Nama Sumber Informasi',
                                  //     labelStyle: TextStyle(color: Colors.black),
                                  //     border: OutlineInputBorder(
                                  //         borderRadius: BorderRadius.circular(8))
                                  // )
                                  inputFormatters: [
                                    FilteringTextInputFormatter.allow(RegExp('[a-zA-Z  \`0-9]')),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    );
                  },
                );
            },
        ),
        bottomNavigationBar: BottomAppBar(
          child:  Container(
            padding: EdgeInsets.symmetric(
                horizontal: MediaQuery.of(context).size.width / 27,
                vertical: MediaQuery.of(context).size.height / 57),
            child: RaisedButton(
                color: myPrimaryColor,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8)
                ),
                onPressed: () {
                  Provider.of<ResultSurveyChangeNotifier>(context,listen: false).checkSurveyDetail(widget.flag,widget.index,context);
                },
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [Text(widget.flag == 0 ? "SAVE" : "UPDATE")]
                )
            ),
          ),
        ),
      ),
    );
  }

  Future<bool> _onWillPop() async {
    var _provider = Provider.of<ResultSurveyChangeNotifier>(context, listen: false);
    if(widget.flag == 0){
      if(_provider.environmentalInformationSelected != null
          || _provider.resourcesInfoSurveySelected != null
          || _provider.controllerResourceInformationName.text != ""
      ){
        return (await showDialog(
          context: context,
          builder: (myContext) => AlertDialog(
            title: new Text('Warning'),
            content: new Text('Keluar dengan simpan perubahan?'),
            actions: <Widget>[
              new FlatButton(
                onPressed: () {
                  _provider.checkSurveyDetail(widget.flag, widget.index,context);
                  Navigator.pop(context);
                },
                child: new Text('Ya'),
              ),
              new FlatButton(
                onPressed: () => Navigator.of(context).pop(true),
                child: new Text('Tidak', style: TextStyle(color: Colors.grey)),
              ),
            ],
          ),
        )) ?? false;
      }else{
        return true;
      }
    }
    else{
      if(_provider.environmentalInformationSelected.id != _provider.environmentalInformationTemp.id
          || _provider.resourcesInfoSurveySelected.PARA_INFORMATION_SOURCE_ID != _provider.resourcesInfoSurveyTemp.PARA_INFORMATION_SOURCE_ID
          || _provider.controllerResourceInformationName.text != _provider.resourceInfoName
      ){
        return (await showDialog(
          context: context,
          builder: (myContext) => AlertDialog(
            title: new Text('Warning'),
            content: new Text('Simpan perubahan?'),
            actions: <Widget>[
              new FlatButton(
                onPressed: () {
                  _provider.checkSurveyDetail(widget.flag, widget.index,context);
                  Navigator.pop(context);
                },
                child: new Text('Ya'),
              ),
              new FlatButton(
                onPressed: () => Navigator.of(context).pop(true),
                child: new Text('Tidak', style: TextStyle(color: Colors.grey)),
              ),
            ],
          ),
        )) ?? false;
      }
      else{
        return true;
      }
    }
  }
}
