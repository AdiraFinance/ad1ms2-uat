import 'package:ad1ms2_dev/shared/decimal_text_input_formatter.dart';
import 'package:ad1ms2_dev/shared/form_m_income_change_notifier.dart';
import 'package:ad1ms2_dev/shared/format_currency.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class WidgetIncomeOccupationTypeEntrepreneur extends StatefulWidget {
  final String maritalStatusKode;

  const WidgetIncomeOccupationTypeEntrepreneur({Key key, this.maritalStatusKode}) : super(key: key);
  @override
  _WidgetIncomeOccupationTypeEntrepreneurState createState() =>
      _WidgetIncomeOccupationTypeEntrepreneurState();
}

class _WidgetIncomeOccupationTypeEntrepreneurState extends State<WidgetIncomeOccupationTypeEntrepreneur> {
  @override
  void initState() {
    super.initState();
    Provider.of<FormMIncomeChangeNotifier>(context, listen: false).setIsMarried(context);
    Provider.of<FormMIncomeChangeNotifier>(context, listen: false).setPreference(context);
  }
  @override
  Widget build(BuildContext context) {
    return Consumer<FormMIncomeChangeNotifier>(
      builder: (context, formMIncomeChangeNotif, _) {
        return SingleChildScrollView(
          padding: EdgeInsets.symmetric(
            horizontal: MediaQuery.of(context).size.width/37,
            vertical: MediaQuery.of(context).size.height/57,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Visibility(
                visible: Provider.of<FormMIncomeChangeNotifier>(context, listen: false).isIncomePerbulanWiraswastaShow(),
                child: TextFormField(
                  autovalidate: formMIncomeChangeNotif.autoValidate,
                  validator: (e) {
                    if (e.isEmpty && Provider.of<FormMIncomeChangeNotifier>(context, listen: false).isIncomePasanganWiraswastaMandatory()) {
                      return "Tidak boleh kosong";
                    } else {
                      return null;
                    }
                  },
                  controller: formMIncomeChangeNotif.controllerMonthlyIncome,
                  style: new TextStyle(color: Colors.black),
                  decoration: InputDecoration(
                      labelText: "Pendapatan Perbulan",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8)),
                      enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: formMIncomeChangeNotif.editIncomePerbulanWiraswasta ? Colors.purple : Colors.grey)),
                      disabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: formMIncomeChangeNotif.editIncomePerbulanWiraswasta ? Colors.purple : Colors.grey)),
                      contentPadding: EdgeInsets.symmetric(horizontal: 10)
                  ),
                  // decoration: new InputDecoration(
                  //     labelText: 'Pendapatan Perbulan',
                  //     labelStyle: TextStyle(color: Colors.black),
                  //     border: OutlineInputBorder(
                  //         borderRadius: BorderRadius.circular(8))),
                  keyboardType: TextInputType.number,
                  textAlign: TextAlign.end,
                  onFieldSubmitted: (value) {
                    formMIncomeChangeNotif.controllerMonthlyIncome.text = formMIncomeChangeNotif.formatCurrency.formatCurrency(value);
                    formMIncomeChangeNotif.calculateTotalIncomeEntrepreneur();
                    formMIncomeChangeNotif.calculateGrossProfitEntrepreneur();
                    formMIncomeChangeNotif.calculateNetProfitBeforeTaxEntrepreneur();
                    formMIncomeChangeNotif.calculateNetProfitAfterTaxEntrepreneur();
                    formMIncomeChangeNotif.calculatedRestOfIncomeEntrepreneur();
                  },
                  onTap: () {
                    formMIncomeChangeNotif.formattingWiraswasta();
                    formMIncomeChangeNotif.calculateTotalIncomeEntrepreneur();
                    formMIncomeChangeNotif.calculateGrossProfitEntrepreneur();
                    formMIncomeChangeNotif.calculateNetProfitBeforeTaxEntrepreneur();
                    formMIncomeChangeNotif.calculateNetProfitAfterTaxEntrepreneur();
                    formMIncomeChangeNotif.calculatedRestOfIncomeEntrepreneur();
                  },
                  textInputAction: TextInputAction.done,
                  inputFormatters: [
                    // formMIncomeChangeNotif.maskFormatter,
                    DecimalTextInputFormatter(decimalRange: 2),
                    formMIncomeChangeNotif.amountValidator
                  ],
                ),
              ),
              Visibility(
                  visible: Provider.of<FormMIncomeChangeNotifier>(context, listen: false).isIncomePasanganWiraswastaMandatory(),
                  child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
              Visibility(
                visible: Provider.of<FormMIncomeChangeNotifier>(context, listen: false).isIncomeLainnyaWiraswastaShow(),
                child: TextFormField(
                  autovalidate: formMIncomeChangeNotif.autoValidate,
                  validator: (e) {
                    if (e.isEmpty && Provider.of<FormMIncomeChangeNotifier>(context, listen: false).isIncomeLainnyaWiraswastaMandatory()) {
                      return "Tidak boleh kosong";
                    } else {
                      return null;
                    }
                  },
                  controller: formMIncomeChangeNotif.controllerOtherIncome,
                  style: new TextStyle(color: Colors.black),
                  decoration: InputDecoration(
                      labelText: "Pendapatan Lainnya",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8)),
                      enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: formMIncomeChangeNotif.editIncomeLainnyaWiraswasta ? Colors.purple : Colors.grey)),
                      disabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: formMIncomeChangeNotif.editIncomeLainnyaWiraswasta ? Colors.purple : Colors.grey)),
                      contentPadding: EdgeInsets.symmetric(horizontal: 10)
                  ),
                  // decoration: new InputDecoration(
                  //     labelText: 'Pendapatan Lainnya',
                  //     labelStyle: TextStyle(color: Colors.black),
                  //     border: OutlineInputBorder(
                  //         borderRadius: BorderRadius.circular(8))),
                  keyboardType: TextInputType.number,
                  textAlign: TextAlign.end,
                  onFieldSubmitted: (value) {
                    formMIncomeChangeNotif.controllerOtherIncome.text = formMIncomeChangeNotif.formatCurrency.formatCurrency(value);
                    formMIncomeChangeNotif.calculateTotalIncomeEntrepreneur();
                    formMIncomeChangeNotif.calculateGrossProfitEntrepreneur();
                    formMIncomeChangeNotif.calculateNetProfitBeforeTaxEntrepreneur();
                    formMIncomeChangeNotif.calculateNetProfitAfterTaxEntrepreneur();
                    formMIncomeChangeNotif.calculatedRestOfIncomeEntrepreneur();
                  },
                  onTap: () {
                    formMIncomeChangeNotif.formattingWiraswasta();
                    formMIncomeChangeNotif.calculateTotalIncomeEntrepreneur();
                    formMIncomeChangeNotif.calculateGrossProfitEntrepreneur();
                    formMIncomeChangeNotif.calculateNetProfitBeforeTaxEntrepreneur();
                    formMIncomeChangeNotif.calculateNetProfitAfterTaxEntrepreneur();
                    formMIncomeChangeNotif.calculatedRestOfIncomeEntrepreneur();
                  },
                  textInputAction: TextInputAction.done,
                  inputFormatters: [
                    DecimalTextInputFormatter(decimalRange: 2),
                    formMIncomeChangeNotif.amountValidator
                  ],
                ),
              ),
              Visibility(
                  visible: Provider.of<FormMIncomeChangeNotifier>(context, listen: false).isIncomeLainnyaWiraswastaMandatory(),
                  child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
              Visibility(
                visible: Provider.of<FormMIncomeChangeNotifier>(context, listen: false).isTotalIncomeWiraswastaShow(),
                child: TextFormField(
                  enabled: false,
                  autovalidate: formMIncomeChangeNotif.autoValidate,
                  validator: (e) {
                    if (e.isEmpty && Provider.of<FormMIncomeChangeNotifier>(context, listen: false).isTotalIncomeWiraswastaMandatory()) {
                      return "Tidak boleh kosong";
                    } else {
                      return null;
                    }
                  },
                  controller: formMIncomeChangeNotif.controllerTotalIncome,
                  style: new TextStyle(color: Colors.black),
                  textAlign: TextAlign.end,
                  decoration: new InputDecoration(
                      labelText: 'Total Pendapatan',
                      labelStyle: TextStyle(color: Colors.black),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8)),
                      enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: formMIncomeChangeNotif.editTotalIncomeWiraswasta ? Colors.purple : Colors.grey)),
                      disabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: formMIncomeChangeNotif.editTotalIncomeWiraswasta ? Colors.purple : Colors.grey)),
                      contentPadding: EdgeInsets.symmetric(horizontal: 10),
                      filled: true,
                      fillColor: Colors.black12,),
                  keyboardType: TextInputType.number,
                ),
              ),
              Visibility(
                  visible: Provider.of<FormMIncomeChangeNotifier>(context, listen: false).isTotalIncomeWiraswastaShow(),
                  child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
              Visibility(
                visible: Provider.of<FormMIncomeChangeNotifier>(context, listen: false).isPokokIncomeWiraswastaShow(),
                child: TextFormField(
                  autovalidate: formMIncomeChangeNotif.autoValidate,
                  validator: (e) {
                    if (e.isEmpty && Provider.of<FormMIncomeChangeNotifier>(context, listen: false).isPokokIncomeWiraswastaMandatory()) {
                      return "Tidak boleh kosong";
                    } else {
                      return null;
                    }
                  },
                  controller: formMIncomeChangeNotif.controllerCostOfGoodsSold,
                  style: new TextStyle(color: Colors.black),
                  decoration: new InputDecoration(
                      labelText: 'Harga Pokok Penjualan',
                      labelStyle: TextStyle(color: Colors.black),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8)),
                      enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: formMIncomeChangeNotif.editPokokIncomeWiraswasta ? Colors.purple : Colors.grey)),
                      disabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: formMIncomeChangeNotif.editPokokIncomeWiraswasta ? Colors.purple : Colors.grey)),
                      contentPadding: EdgeInsets.symmetric(horizontal: 10)),
                  keyboardType: TextInputType.number,
                  textAlign: TextAlign.end,
                  onFieldSubmitted: (value) {
                    formMIncomeChangeNotif.controllerCostOfGoodsSold.text = formMIncomeChangeNotif.formatCurrency.formatCurrency(value);
                    formMIncomeChangeNotif.calculateTotalIncomeEntrepreneur();
                    formMIncomeChangeNotif.calculateGrossProfitEntrepreneur();
                    formMIncomeChangeNotif.calculateNetProfitBeforeTaxEntrepreneur();
                    formMIncomeChangeNotif.calculateNetProfitAfterTaxEntrepreneur();
                    formMIncomeChangeNotif.calculatedRestOfIncomeEntrepreneur();
                  },
                  onTap: () {
                    formMIncomeChangeNotif.formattingWiraswasta();
                    formMIncomeChangeNotif.calculateTotalIncomeEntrepreneur();
                    formMIncomeChangeNotif.calculateGrossProfitEntrepreneur();
                    formMIncomeChangeNotif.calculateNetProfitBeforeTaxEntrepreneur();
                    formMIncomeChangeNotif.calculateNetProfitAfterTaxEntrepreneur();
                    formMIncomeChangeNotif.calculatedRestOfIncomeEntrepreneur();
                  },
                  textInputAction: TextInputAction.done,
                  inputFormatters: [
                    DecimalTextInputFormatter(decimalRange: 2),
                    formMIncomeChangeNotif.amountValidator
                  ],
                ),
              ),
              Visibility(
                  visible: Provider.of<FormMIncomeChangeNotifier>(context, listen: false).isPokokIncomeWiraswastaShow(),
                  child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
              Visibility(
                visible: Provider.of<FormMIncomeChangeNotifier>(context, listen: false).isLabaKotorWiraswastaShow(),
                child: TextFormField(
                  enabled: false,
                  autovalidate: formMIncomeChangeNotif.autoValidate,
                  validator: (e) {
                    if (e.isEmpty && Provider.of<FormMIncomeChangeNotifier>(context, listen: false).isLabaKotorWiraswastaMandatory()) {
                      return "Tidak boleh kosong";
                    } else {
                      return null;
                    }
                  },
                  controller: formMIncomeChangeNotif.controllerGrossProfit,
                  style: new TextStyle(color: Colors.black),
                  textAlign: TextAlign.end,
                  decoration: new InputDecoration(
                      labelText: 'Laba Kotor',
                      labelStyle: TextStyle(color: Colors.black),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8)),
                      enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: formMIncomeChangeNotif.editLabaKotorWiraswasta ? Colors.purple : Colors.grey)),
                      disabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: formMIncomeChangeNotif.editLabaKotorWiraswasta ? Colors.purple : Colors.grey)),
                      contentPadding: EdgeInsets.symmetric(horizontal: 10),
                      filled: true,
                      fillColor: Colors.black12,),
                  keyboardType: TextInputType.number,
                  textCapitalization: TextCapitalization.characters,
                ),
              ),
              Visibility(
                  visible: Provider.of<FormMIncomeChangeNotifier>(context, listen: false).isLabaKotorWiraswastaShow(),
                  child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
              Visibility(
                visible: Provider.of<FormMIncomeChangeNotifier>(context, listen: false).isBiayaOperasionalWiraswastaShow(),
                child: TextFormField(
                  autovalidate: formMIncomeChangeNotif.autoValidate,
                  validator: (e) {
                    if (e.isEmpty && Provider.of<FormMIncomeChangeNotifier>(context, listen: false).isBiayaOperasionalWiraswastaMandatory()) {
                      return "Tidak boleh kosong";
                    } else {
                      return null;
                    }
                  },
                  controller: formMIncomeChangeNotif.controllerOperatingCosts,
                  style: new TextStyle(color: Colors.black),
                  decoration: new InputDecoration(
                      labelText: 'Biaya Operasional',
                      labelStyle: TextStyle(color: Colors.black),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8)),
                      enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: formMIncomeChangeNotif.editBiayaOperasionalWiraswasta ? Colors.purple : Colors.grey)),
                      disabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: formMIncomeChangeNotif.editBiayaOperasionalWiraswasta ? Colors.purple : Colors.grey)),
                      contentPadding: EdgeInsets.symmetric(horizontal: 10),),
                  keyboardType: TextInputType.numberWithOptions(decimal: true),
                  textAlign: TextAlign.end,
                  onFieldSubmitted: (value) {
                    formMIncomeChangeNotif.controllerOperatingCosts.text = formMIncomeChangeNotif.formatCurrency.formatCurrency(value);
                    formMIncomeChangeNotif.calculateTotalIncomeEntrepreneur();
                    formMIncomeChangeNotif.calculateGrossProfitEntrepreneur();
                    formMIncomeChangeNotif.calculateNetProfitBeforeTaxEntrepreneur();
                    formMIncomeChangeNotif.calculateNetProfitAfterTaxEntrepreneur();
                    formMIncomeChangeNotif.calculatedRestOfIncomeEntrepreneur();
                  },
                  onTap: () {
                    formMIncomeChangeNotif.formattingWiraswasta();
                    formMIncomeChangeNotif.calculateTotalIncomeEntrepreneur();
                    formMIncomeChangeNotif.calculateGrossProfitEntrepreneur();
                    formMIncomeChangeNotif.calculateNetProfitBeforeTaxEntrepreneur();
                    formMIncomeChangeNotif.calculateNetProfitAfterTaxEntrepreneur();
                    formMIncomeChangeNotif.calculatedRestOfIncomeEntrepreneur();
                  },
                  textInputAction: TextInputAction.done,
                  inputFormatters: [
                    DecimalTextInputFormatter(decimalRange: 2),
                    formMIncomeChangeNotif.amountValidator
                  ],
                ),
              ),
              Visibility(
                  visible: Provider.of<FormMIncomeChangeNotifier>(context, listen: false).isBiayaOperasionalWiraswastaShow(),
                  child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
              Visibility(
                visible: Provider.of<FormMIncomeChangeNotifier>(context, listen: false).isBiayaLainnyaWiraswastaShow(),
                child: TextFormField(
                  autovalidate: formMIncomeChangeNotif.autoValidate,
                  validator: (e) {
                    if (e.isEmpty && Provider.of<FormMIncomeChangeNotifier>(context, listen: false).isBiayaLainnyaWiraswastaMandatory()) {
                      return "Tidak boleh kosong";
                    } else {
                      return null;
                    }
                  },
                  controller: formMIncomeChangeNotif.controllerOtherCosts,
                  style: TextStyle(color: Colors.black),
                  decoration: new InputDecoration(
                      labelText: 'Biaya Lainnya',
                      labelStyle: TextStyle(color: Colors.black),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8)),
                      enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: formMIncomeChangeNotif.editBiayaLainnyaWiraswasta ? Colors.purple : Colors.grey)),
                      disabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: formMIncomeChangeNotif.editBiayaLainnyaWiraswasta ? Colors.purple : Colors.grey)),
                      contentPadding: EdgeInsets.symmetric(horizontal: 10),),
                  keyboardType: TextInputType.number,
                  textAlign: TextAlign.end,
                  onFieldSubmitted: (value) {
                    formMIncomeChangeNotif.controllerOtherCosts.text = formMIncomeChangeNotif.formatCurrency.formatCurrency(value);
                    formMIncomeChangeNotif.calculateTotalIncomeEntrepreneur();
                    formMIncomeChangeNotif.calculateGrossProfitEntrepreneur();
                    formMIncomeChangeNotif.calculateNetProfitBeforeTaxEntrepreneur();
                    formMIncomeChangeNotif.calculateNetProfitAfterTaxEntrepreneur();
                    formMIncomeChangeNotif.calculatedRestOfIncomeEntrepreneur();
                  },
                  onTap: () {
                    formMIncomeChangeNotif.formattingWiraswasta();
                    formMIncomeChangeNotif.calculateTotalIncomeEntrepreneur();
                    formMIncomeChangeNotif.calculateGrossProfitEntrepreneur();
                    formMIncomeChangeNotif.calculateNetProfitBeforeTaxEntrepreneur();
                    formMIncomeChangeNotif.calculateNetProfitAfterTaxEntrepreneur();
                    formMIncomeChangeNotif.calculatedRestOfIncomeEntrepreneur();
                  },
                  textInputAction: TextInputAction.done,
                  inputFormatters: [
                    DecimalTextInputFormatter(decimalRange: 2),
                    formMIncomeChangeNotif.amountValidator
                  ],
                ),
              ),
              Visibility(
                  visible: Provider.of<FormMIncomeChangeNotifier>(context, listen: false).isBiayaLainnyaWiraswastaShow(),
                  child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
              Visibility(
                visible: Provider.of<FormMIncomeChangeNotifier>(context, listen: false).isNetSebelumPajakWiraswastaShow(),
                child: TextFormField(
                  enabled: false,
                  autovalidate: formMIncomeChangeNotif.autoValidate,
                  validator: (e) {
                    if (e.isEmpty && Provider.of<FormMIncomeChangeNotifier>(context, listen: false).isNetSebelumPajakWiraswastaMandatory()) {
                      return "Tidak boleh kosong";
                    } else {
                      return null;
                    }
                  },
                  controller: formMIncomeChangeNotif.controllerNetProfitBeforeTax,
                  style: new TextStyle(color: Colors.black),
                  textAlign: TextAlign.end,
                  decoration: new InputDecoration(
                      labelText: 'Laba Bersih Sebelum Pajak',
                      labelStyle: TextStyle(color: Colors.black),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8)),
                      enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: formMIncomeChangeNotif.editNetSebelumPajakWiraswasta ? Colors.purple : Colors.grey)),
                      disabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: formMIncomeChangeNotif.editNetSebelumPajakWiraswasta ? Colors.purple : Colors.grey)),
                      contentPadding: EdgeInsets.symmetric(horizontal: 10),
                      filled: true,
                      fillColor: Colors.black12,),
                  keyboardType: TextInputType.number,
                  textCapitalization: TextCapitalization.characters,
                ),
              ),
              Visibility(
                  visible: Provider.of<FormMIncomeChangeNotifier>(context, listen: false).isNetSebelumPajakWiraswastaShow(),
                  child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
              Visibility(
                visible: Provider.of<FormMIncomeChangeNotifier>(context, listen: false).isPajakWiraswastaShow(),
                child: TextFormField(
                  autovalidate: formMIncomeChangeNotif.autoValidate,
                  validator: (e) {
                    if (e.isEmpty && Provider.of<FormMIncomeChangeNotifier>(context, listen: false).isPajakWiraswastaMandatory()) {
                      return "Tidak boleh kosong";
                    } else {
                      return null;
                    }
                  },
                  controller: formMIncomeChangeNotif.controllerTax,
                  style: new TextStyle(color: Colors.black),
                  decoration: new InputDecoration(
                      labelText: 'Pajak',
                      labelStyle: TextStyle(color: Colors.black),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8)),
                      enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: formMIncomeChangeNotif.editPajakWiraswasta ? Colors.purple : Colors.grey)),
                      disabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: formMIncomeChangeNotif.editPajakWiraswasta ? Colors.purple : Colors.grey)),
                      contentPadding: EdgeInsets.symmetric(horizontal: 10),),
                  keyboardType: TextInputType.number,
                  textAlign: TextAlign.end,
                  onFieldSubmitted: (value) {
                    formMIncomeChangeNotif.controllerTax.text = formMIncomeChangeNotif.formatCurrency.formatCurrency(value);
                    formMIncomeChangeNotif.calculateTotalIncomeEntrepreneur();
                    formMIncomeChangeNotif.calculateGrossProfitEntrepreneur();
                    formMIncomeChangeNotif.calculateNetProfitBeforeTaxEntrepreneur();
                    formMIncomeChangeNotif.calculateNetProfitAfterTaxEntrepreneur();
                    formMIncomeChangeNotif.calculatedRestOfIncomeEntrepreneur();
                  },
                  onTap: () {
                    formMIncomeChangeNotif.formattingWiraswasta();
                    formMIncomeChangeNotif.calculateTotalIncomeEntrepreneur();
                    formMIncomeChangeNotif.calculateGrossProfitEntrepreneur();
                    formMIncomeChangeNotif.calculateNetProfitBeforeTaxEntrepreneur();
                    formMIncomeChangeNotif.calculateNetProfitAfterTaxEntrepreneur();
                    formMIncomeChangeNotif.calculatedRestOfIncomeEntrepreneur();
                  },
                  textInputAction: TextInputAction.done,
                  inputFormatters: [
                    DecimalTextInputFormatter(decimalRange: 2),
                    formMIncomeChangeNotif.amountValidator
                  ],
                ),
              ),
              Visibility(
                  visible: Provider.of<FormMIncomeChangeNotifier>(context, listen: false).isPajakWiraswastaShow(),
                  child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
              Visibility(
                visible: Provider.of<FormMIncomeChangeNotifier>(context, listen: false).isNetSetelahPajakWiraswastaShow(),
                child: TextFormField(
                  enabled: false,
                  autovalidate: formMIncomeChangeNotif.autoValidate,
                  validator: (e) {
                    if (e.isEmpty && Provider.of<FormMIncomeChangeNotifier>(context, listen: false).isNetSetelahPajakWiraswastaMandatory()) {
                      return "Tidak boleh kosong";
                    } else {
                      return null;
                    }
                  },
                  controller: formMIncomeChangeNotif.controllerNetProfitAfterTax,
                  style: new TextStyle(color: Colors.black),
                  textAlign: TextAlign.end,
                  decoration: new InputDecoration(
                      labelText: 'Laba Bersih Setelah Pajak',
                      labelStyle: TextStyle(color: Colors.black),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8)),
                      enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: formMIncomeChangeNotif.editNetSetelahPajakWiraswasta ? Colors.purple : Colors.grey)),
                      disabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: formMIncomeChangeNotif.editNetSetelahPajakWiraswasta ? Colors.purple : Colors.grey)),
                      contentPadding: EdgeInsets.symmetric(horizontal: 10),
                      filled: true,
                      fillColor: Colors.black12,),
                  keyboardType: TextInputType.number,
                  textCapitalization: TextCapitalization.characters,
                ),
              ),
              Visibility(
                  visible: Provider.of<FormMIncomeChangeNotifier>(context, listen: false).isNetSetelahPajakWiraswastaShow(),
                  child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
              Visibility(
                visible: Provider.of<FormMIncomeChangeNotifier>(context, listen: false).isBiayaHidupWiraswastaShow(),
                child: TextFormField(
//              enabled: false,
                  autovalidate: formMIncomeChangeNotif.autoValidate,
                  validator: (e) {
                    if (e.isEmpty && Provider.of<FormMIncomeChangeNotifier>(context, listen: false).isBiayaHidupWiraswastaMandatory()) {
                      return "Tidak boleh kosong";
                    } else {
                      return null;
                    }
                  },
                  controller: formMIncomeChangeNotif.controllerCostOfLiving,
                  style: new TextStyle(color: Colors.black),
                  decoration: new InputDecoration(
                      labelText: 'Biaya Hidup',
                      labelStyle: TextStyle(color: Colors.black),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8)),
                      enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: formMIncomeChangeNotif.editBiayaHidupWiraswasta ? Colors.purple : Colors.grey)),
                      disabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: formMIncomeChangeNotif.editBiayaHidupWiraswasta ? Colors.purple : Colors.grey)),
                      contentPadding: EdgeInsets.symmetric(horizontal: 10),),
                  keyboardType: TextInputType.number,
                  textAlign: TextAlign.end,
                  onFieldSubmitted: (value) {
                    formMIncomeChangeNotif.controllerCostOfLiving.text = formMIncomeChangeNotif.formatCurrency.formatCurrency(value);
                    formMIncomeChangeNotif.calculateTotalIncomeEntrepreneur();
                    formMIncomeChangeNotif.calculateGrossProfitEntrepreneur();
                    formMIncomeChangeNotif.calculateNetProfitBeforeTaxEntrepreneur();
                    formMIncomeChangeNotif.calculateNetProfitAfterTaxEntrepreneur();
                    formMIncomeChangeNotif.calculatedRestOfIncomeEntrepreneur();
                  },
                  onTap: () {
                    formMIncomeChangeNotif.formattingWiraswasta();
                    formMIncomeChangeNotif.calculateTotalIncomeEntrepreneur();
                    formMIncomeChangeNotif.calculateGrossProfitEntrepreneur();
                    formMIncomeChangeNotif.calculateNetProfitBeforeTaxEntrepreneur();
                    formMIncomeChangeNotif.calculateNetProfitAfterTaxEntrepreneur();
                    formMIncomeChangeNotif.calculatedRestOfIncomeEntrepreneur();
                  },
                  textInputAction: TextInputAction.done,
                  inputFormatters: [
                    DecimalTextInputFormatter(decimalRange: 2),
                    formMIncomeChangeNotif.amountValidator
                  ],
                ),
              ),
              Visibility(
                  visible: Provider.of<FormMIncomeChangeNotifier>(context, listen: false).isBiayaHidupWiraswastaShow(),
                  child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
              Visibility(
                visible: Provider.of<FormMIncomeChangeNotifier>(context, listen: false).isSisaIncomeWiraswastaShow(),
                child: TextFormField(
                  enabled: false,
                  autovalidate: formMIncomeChangeNotif.autoValidate,
                  validator: (e) {
                    if (e.isEmpty && Provider.of<FormMIncomeChangeNotifier>(context, listen: false).isSisaIncomeWiraswastaMandatory()) {
                      return "Tidak boleh kosong";
                    } else {
                      return null;
                    }
                  },
                  controller: formMIncomeChangeNotif.controllerRestIncome,
                  style: new TextStyle(color: Colors.black),
                  textAlign: TextAlign.end,
                  decoration: new InputDecoration(
                      labelText: 'Sisa Pendapatan',
                      labelStyle: TextStyle(color: Colors.black),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8)),
                      enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: formMIncomeChangeNotif.editSisaIncomeWiraswasta ? Colors.purple : Colors.grey)),
                      disabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: formMIncomeChangeNotif.editSisaIncomeWiraswasta ? Colors.purple : Colors.grey)),
                      contentPadding: EdgeInsets.symmetric(horizontal: 10),
                      filled: true,
                      fillColor: Colors.black12,),
                  keyboardType: TextInputType.number,
                  textCapitalization: TextCapitalization.characters,
                ),
              ),
              formMIncomeChangeNotif.isMarried == true
                  ? SizedBox(height: MediaQuery.of(context).size.height / 47)
                  : SizedBox(),
              Visibility(
                visible: formMIncomeChangeNotif.isMarried,
                child: Visibility(
                  visible: Provider.of<FormMIncomeChangeNotifier>(context, listen: false).isIncomePasanganWiraswastaShow(),
                  child: TextFormField(
//              enabled: false,
                    autovalidate: formMIncomeChangeNotif.autoValidate,
                    validator: (e) {
                      if (e.isEmpty && Provider.of<FormMIncomeChangeNotifier>(context, listen: false).isIncomePasanganWiraswastaMandatory()) {
                        return "Tidak boleh kosong";
                      } else {
                        return null;
                      }
                    },
                    controller: formMIncomeChangeNotif.controllerSpouseIncome,
                    style: new TextStyle(color: Colors.black),
                    decoration: new InputDecoration(
                        labelText: 'Pendapatan Pasangan',
                        labelStyle: TextStyle(color: Colors.black),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8)),
                        enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: formMIncomeChangeNotif.editIncomePasanganWiraswasta ? Colors.purple : Colors.grey)),
                        disabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: formMIncomeChangeNotif.editIncomePasanganWiraswasta ? Colors.purple : Colors.grey)),
                        contentPadding: EdgeInsets.symmetric(horizontal: 10),),
                    keyboardType: TextInputType.numberWithOptions(decimal: true),
                    textAlign: TextAlign.end,
                    onFieldSubmitted: (value) {
                      formMIncomeChangeNotif.controllerSpouseIncome.text = formMIncomeChangeNotif.formatCurrency.formatCurrency(value);
                      formMIncomeChangeNotif.calculateTotalIncomeEntrepreneur();
                      formMIncomeChangeNotif.calculateGrossProfitEntrepreneur();
                      formMIncomeChangeNotif.calculateNetProfitBeforeTaxEntrepreneur();
                      formMIncomeChangeNotif.calculateNetProfitAfterTaxEntrepreneur();
                      formMIncomeChangeNotif.calculatedRestOfIncomeEntrepreneur();
                    },
                    onTap: () {
                      formMIncomeChangeNotif.formattingWiraswasta();
                      formMIncomeChangeNotif.calculateTotalIncomeEntrepreneur();
                      formMIncomeChangeNotif.calculateGrossProfitEntrepreneur();
                      formMIncomeChangeNotif.calculateNetProfitBeforeTaxEntrepreneur();
                      formMIncomeChangeNotif.calculateNetProfitAfterTaxEntrepreneur();
                      formMIncomeChangeNotif.calculatedRestOfIncomeEntrepreneur();
                    },
                    textInputAction: TextInputAction.done,
                    inputFormatters: [
                      DecimalTextInputFormatter(decimalRange: 2),
                      formMIncomeChangeNotif.amountValidator
                    ],
                  ),
                ),
              ),
              Visibility(
                  visible: Provider.of<FormMIncomeChangeNotifier>(context, listen: false).isIncomePasanganWiraswastaShow(),
                  child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
              Visibility(
                visible: Provider.of<FormMIncomeChangeNotifier>(context, listen: false).isAngsuranLainnyaWiraswastaShow(),
                child: TextFormField(
                  autovalidate: formMIncomeChangeNotif.autoValidate,
                  validator: (e) {
                    if (e.isEmpty && Provider.of<FormMIncomeChangeNotifier>(context, listen: false).isAngsuranLainnyaWiraswastaMandatory()) {
                      return "Tidak boleh kosong";
                    } else {
                      return null;
                    }
                  },
                  controller: formMIncomeChangeNotif.controllerOtherInstallments,
                  style: new TextStyle(color: Colors.black),
                  decoration: new InputDecoration(
                      labelText: 'Angsuran Lainnya',
                      labelStyle: TextStyle(color: Colors.black),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8)),
                      enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: formMIncomeChangeNotif.editAngsuranLainnyaWiraswasta ? Colors.purple : Colors.grey)),
                      disabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: formMIncomeChangeNotif.editAngsuranLainnyaWiraswasta ? Colors.purple : Colors.grey)),
                      contentPadding: EdgeInsets.symmetric(horizontal: 10),),
                  keyboardType: TextInputType.number,
                  textAlign: TextAlign.end,
                  onFieldSubmitted: (value) {
                    formMIncomeChangeNotif.controllerOtherInstallments.text = formMIncomeChangeNotif.formatCurrency.formatCurrency(value);
                    formMIncomeChangeNotif.calculateTotalIncomeEntrepreneur();
                    formMIncomeChangeNotif.calculateGrossProfitEntrepreneur();
                    formMIncomeChangeNotif.calculateNetProfitBeforeTaxEntrepreneur();
                    formMIncomeChangeNotif.calculateNetProfitAfterTaxEntrepreneur();
                    formMIncomeChangeNotif.calculatedRestOfIncomeEntrepreneur();
                  },
                  onTap: () {
                    formMIncomeChangeNotif.formattingWiraswasta();
                    formMIncomeChangeNotif.calculateTotalIncomeEntrepreneur();
                    formMIncomeChangeNotif.calculateGrossProfitEntrepreneur();
                    formMIncomeChangeNotif.calculateNetProfitBeforeTaxEntrepreneur();
                    formMIncomeChangeNotif.calculateNetProfitAfterTaxEntrepreneur();
                    formMIncomeChangeNotif.calculatedRestOfIncomeEntrepreneur();
                  },
                  textInputAction: TextInputAction.done,
                  inputFormatters: [
                    DecimalTextInputFormatter(decimalRange: 2),
                    formMIncomeChangeNotif.amountValidator
                  ],
                ),
              ),
              SizedBox(height: MediaQuery.of(context).size.height / 47),
            ],
          ),
        );
      },
    );
  }
}
