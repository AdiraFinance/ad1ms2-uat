import 'package:ad1ms2_dev/shared/change_notifier_app/information_object_unit_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class TitleAppBarFormM extends StatelessWidget {
  final int selectedIndex;
  const TitleAppBarFormM({this.selectedIndex});

  @override
  Widget build(BuildContext context) {
    var _provider = Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false);
    var _text = "";
    // if (selectedIndex == 0) {
    //   return Text("Credit Limit",style: TextStyle(color: Colors.black));
    // } else
    if(selectedIndex == 0){
      return Text("Rincian Foto",style: TextStyle(color: Colors.black));
    } else if(selectedIndex == 1){
      return Text("Rincian Nasabah",style: TextStyle(color: Colors.black));
    }

//    else if (selectedIndex == 1) {
//      return Text("Informasi Nasabah",style: TextStyle(color: Colors.black));
//    } else if (selectedIndex == 2) {
//      return Text("Informasi Alamat",style: TextStyle(color: Colors.black));
//    } else if (selectedIndex == 3) {
//      return Text("Informasi Keluarga(Ibu)",style: TextStyle(color: Colors.black));
//    }
//    else if (selectedIndex == 2) {
//      return Text("Informasi Keluarga",style: TextStyle(color: Colors.black));
//    }
    else if (selectedIndex == 2) {
      return Text("Pekerjaan",style: TextStyle(color: Colors.black));
    } else if (selectedIndex == 3) {
      return Text("Pendapatan",style: TextStyle(color: Colors.black));
    } else if (selectedIndex == 4) {
      return Text("Penjamin",style: TextStyle(color: Colors.black));
    } else if (selectedIndex == 5) {
      return Text("Aplikasi",style: TextStyle(color: Colors.black));
    }
//    else if (selectedIndex == 7) {
//      return Text("Info Unit Objek",style: TextStyle(color: Colors.black));
//    } else if (selectedIndex == 8) {
//      return Text("Info Sales",style: TextStyle(color: Colors.black));
//    } else if (selectedIndex == 9) {
//      return Text("Info Collateral",style: TextStyle(color: Colors.black));
//    }

    else if (selectedIndex == 6) {
      return Text("Informasi Objek",style: TextStyle(color: Colors.black));
    }
    else if (selectedIndex == 7) {
      return Text("Karoseri",style: TextStyle(color: Colors.black));
    }
    else if (selectedIndex == 8) {
      return Text("Rincian Pinjaman",style: TextStyle(color: Colors.black));
    }

//    else if (selectedIndex == 9) {
//      return Text("Info Struktur Kredit",style: TextStyle(color: Colors.black));
//    }
//    else if (selectedIndex == 10) {
//      return Text("Info Struktur Kredit Jenis Angsuran",style: TextStyle(color: Colors.black));
//    }
//    else if(selectedIndex == 11){
//      return Text("Info Asuransi Utama",style: TextStyle(color: Colors.black));
//    }
//    else if(selectedIndex == 12){
//      return Text("Info Asuransi Tambahan",style: TextStyle(color: Colors.black));
//    }
//    else if(selectedIndex == 13){
//      return Text("Info WMP",style: TextStyle(color: Colors.black));
//    }

//    else if(selectedIndex == 10){
//      return Text("Info Kredit Income",style: TextStyle(color: Colors.black));
//    }
    else if(selectedIndex == 9){
      return Text("Subsidi",style: TextStyle(color: Colors.black));
    }
    else if(selectedIndex == 10){
      return Text("Informasi Dokumen",style: TextStyle(color: Colors.black));
    }
    else if(selectedIndex == 11){
      if(_provider.groupObjectSelected != null){
        if(_provider.groupObjectSelected.KODE == "001"){
          _text = "Motor";
        }
        else if(_provider.groupObjectSelected.KODE == "001"){
          _text = "Mobil";
        }
      }
      return Text(
          "Taksasi Unit $_text",
          style: TextStyle(color: Colors.black)
      );
    }
    else if(selectedIndex == 12){
      return Text("Marketing Notes",style: TextStyle(color: Colors.black));
    }
    else{
      return Text("Hasil Survey",style: TextStyle(color: Colors.black));
    }
  }
}
