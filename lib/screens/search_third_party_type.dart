import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/shared/search_third_party_type_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SearchThirdPartyType extends StatefulWidget {
  @override
  _SearchThirdPartyTypeState createState() => _SearchThirdPartyTypeState();
}

class _SearchThirdPartyTypeState extends State<SearchThirdPartyType> {

  @override
  void initState() {
    super.initState();
    Provider.of<SearchThirdPartyTypeChangeNotifier>(context,listen: false).getThirdPartyType(context);
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
          primaryColor: Colors.black,
          accentColor: myPrimaryColor,
          fontFamily: "NunitoSans",
          primarySwatch: primaryOrange
      ),
      child: Scaffold(
        key: Provider.of<SearchThirdPartyTypeChangeNotifier>(context,listen: false).scaffoldKey,
        appBar: AppBar(
          title: Consumer<SearchThirdPartyTypeChangeNotifier>(
            builder: (context, searchThirdPartyTypeChangeNotifier, _) {
              return TextFormField(
                controller: searchThirdPartyTypeChangeNotifier.controllerSearch,
                style: TextStyle(color: Colors.black),
                textInputAction: TextInputAction.search,
                onFieldSubmitted: (query) {
                  searchThirdPartyTypeChangeNotifier.searchThirdPartyType(query);
                },
                onChanged: (e) {
                  searchThirdPartyTypeChangeNotifier.changeAction(e);
                },
                cursorColor: Colors.black,
                decoration: new InputDecoration(
                  hintText: "Cari Jenis Pihak Ketiga (minimal 3 karakter)",
                  hintStyle: TextStyle(color: Colors.black),
                  enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: myPrimaryColor),
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: myPrimaryColor),
                  ),
                ),
                autofocus: true,
                textCapitalization: TextCapitalization.characters,
              );
            },
          ),
          backgroundColor: myPrimaryColor,
          iconTheme: IconThemeData(color: Colors.black),
          actions: <Widget>[
            Provider.of<SearchThirdPartyTypeChangeNotifier>(context, listen: true)
                    .showClear
                ? IconButton(
                    icon: Icon(Icons.clear),
                    onPressed: () {
                      Provider.of<SearchThirdPartyTypeChangeNotifier>(context,
                              listen: false)
                          .controllerSearch
                          .clear();
                      Provider.of<SearchThirdPartyTypeChangeNotifier>(context,
                              listen: false)
                          .changeAction(
                              Provider.of<SearchThirdPartyTypeChangeNotifier>(
                                      context,
                                      listen: false)
                                  .controllerSearch
                                  .text);
                    })
                : SizedBox(
                    width: 0.0,
                    height: 0.0,
                  )
          ],
        ),
        body: Consumer<SearchThirdPartyTypeChangeNotifier>(
          builder: (context, searchThirdPartyTypeChangeNotifier, _) {
            return searchThirdPartyTypeChangeNotifier.loadData
                ?
            Center(
              child: CircularProgressIndicator(),
            )
                :
            searchThirdPartyTypeChangeNotifier.listThirdPartyTypeTemp.isNotEmpty
                ?
            ListView.separated(
              padding: EdgeInsets.symmetric(
                  vertical: MediaQuery.of(context).size.height / 57,
                  horizontal: MediaQuery.of(context).size.width / 27),
              itemCount:
              searchThirdPartyTypeChangeNotifier.listThirdPartyTypeTemp.length,
              itemBuilder: (listContext, index) {
                return InkWell(
                  onTap: () {
                    Navigator.pop(
                        context,
                        searchThirdPartyTypeChangeNotifier
                            .listThirdPartyTypeTemp[index]);
                  },
                  child: Container(
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Text(
                          "${searchThirdPartyTypeChangeNotifier.listThirdPartyTypeTemp[index].kode} - "
                              "${searchThirdPartyTypeChangeNotifier.listThirdPartyTypeTemp[index].deskripsi} ",
                          style: TextStyle(fontSize: 16),
                        )
                      ],
                    ),
                  ),
                );
              },
              separatorBuilder: (context, index) {
                return Divider();
              },
            )
                :
            ListView.separated(
              padding: EdgeInsets.symmetric(
                  vertical: MediaQuery.of(context).size.height / 57,
                  horizontal: MediaQuery.of(context).size.width / 27),
              itemCount:
                  searchThirdPartyTypeChangeNotifier.listThirdPartyType.length,
              itemBuilder: (listContext, index) {
                return InkWell(
                  onTap: () {
                    Navigator.pop(
                        context,
                        searchThirdPartyTypeChangeNotifier
                            .listThirdPartyType[index]);
                  },
                  child: Container(
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Text(
                          "${searchThirdPartyTypeChangeNotifier.listThirdPartyType[index].kode} - "
                          "${searchThirdPartyTypeChangeNotifier.listThirdPartyType[index].deskripsi} ",
                          style: TextStyle(fontSize: 16),
                        )
                      ],
                    ),
                  ),
                );
              },
              separatorBuilder: (context, index) {
                return Divider();
              },
            );
          },
        ),
      ),
    );
  }
}
