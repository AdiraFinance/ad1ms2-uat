import 'dart:io';
import 'dart:math';
import 'dart:typed_data';
//import 'package:esys_flutter_share/esys_flutter_share.dart';
//import 'package:exif/exif.dart';
import 'package:ad1ms2_dev/main.dart';
import 'package:firebase_ml_vision/firebase_ml_vision.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_exif_rotation/flutter_exif_rotation.dart';
//import 'package:flutter_exif_rotation/flutter_exif_rotation.dart';
// import 'package:image_cropper/image_cropper.dart';
// import 'package:native_device_orientation/native_device_orientation.dart';
// import 'package:nyobak_camera/previewscreen/crop_image.dart';
import 'package:path/path.dart';
import 'package:image/image.dart' as IMG;

class PreviewImageScreen extends StatefulWidget {
  final String imagePath;

  PreviewImageScreen({this.imagePath});

  @override
  _PreviewImageScreenState createState() => _PreviewImageScreenState();
}

class _PreviewImageScreenState extends State<PreviewImageScreen> {
  File _imageFile;
  Uint8List _image;
  bool _processImage = false;
  String _data;
  String _nik = "";
  String _width = "";
  String _height = "";
  double _screenHeight;

  @override
  void initState() {
    super.initState();
    _resizePhoto(widget.imagePath);
//    _cropImage();
  }

//  Future<Null> _cropImage() async {
//    File croppedFile = await ImageCropper.cropImage(
//        sourcePath: widget.imagePath,
//        aspectRatioPresets: Platform.isAndroid
//            ? [
//                CropAspectRatioPreset.square,
//                CropAspectRatioPreset.ratio3x2,
//                CropAspectRatioPreset.original,
//                CropAspectRatioPreset.ratio4x3,
//                CropAspectRatioPreset.ratio16x9
//              ]
//            : [
//                CropAspectRatioPreset.original,
//                CropAspectRatioPreset.square,
//                CropAspectRatioPreset.ratio3x2,
//                CropAspectRatioPreset.ratio4x3,
//                CropAspectRatioPreset.ratio5x3,
//                CropAspectRatioPreset.ratio5x4,
//                CropAspectRatioPreset.ratio7x5,
//                CropAspectRatioPreset.ratio16x9
//              ],
//        androidUiSettings: AndroidUiSettings(
//            toolbarTitle: 'Cropper',
//            toolbarColor: Colors.deepOrange,
//            toolbarWidgetColor: Colors.white,
//            initAspectRatio: CropAspectRatioPreset.original,
//            lockAspectRatio: false),
//        iosUiSettings: IOSUiSettings(
//          title: 'Cropper',
//        ));
//    if (croppedFile != null) {
//      setState(() {
//        _imageFile = croppedFile;
//      });
//      readText();
//    }
//  }

  _readText(File image) async {
    if (image != null) {
      setState(() {
        _processImage = true;
      });
      FirebaseVisionImage ourImage = FirebaseVisionImage.fromFile(image);
      TextRecognizer recognizeText = FirebaseVision.instance.textRecognizer();
      VisionText readText = await recognizeText.processImage(ourImage);

      List<String> _dataOcr = [];

      for (TextBlock block in readText.blocks) {
        for (TextLine line in block.lines) {
          print(line.text);
          _dataOcr.add(line.text);
        }
      }
      recognizeText.close();
      _prosesDataOcr(_dataOcr);
    }
  }

  _prosesDataOcr(List<String> data) {
    List<String> _dataSortir = [];
    for (int i = 0; i < data.length; i++) {
      if (data[i].length > 14 && data[i].length < 19) {
        _dataSortir.add(data[i]);
      }
    }
    _processCheckNumericCharacter(_dataSortir);
  }

  int getCount(String item) {
    int count = 0;
    item.runes.forEach((int rune) {
      var character = new String.fromCharCode(rune);
      for (int j = 0; j < character.length; j++) {
        if (isNumeric(character[j])) {
          count++;
        }
      }
    });
    return count;
  }

  bool isNumeric(String s) {
    if (s == null) {
      return false;
    }
    return double.tryParse(s) != null;
  }

  void _processCheckNumericCharacter(List<String> data) {
    var biggestIndex = 0;
    var biggestCount = 0;

    for (int i = 0; i < data.length; i++) {
      var count = getCount(data[i]);

      if (count > biggestCount) {
        biggestIndex = i;
        biggestCount = count;
      }
    }

    print('index array dengan angka paling banyak');
    print(biggestIndex);

    print('data dengan angka terbanyak');
    print(data[biggestIndex]);

    print('Jumlah angka');
    print(biggestCount);

    setState(() {
      _nik = _optimasiCharacter(data[biggestIndex]);
      _processImage = false;
    });
  }

  String _optimasiCharacter(String data) {
    String _characterB = data.replaceAll("b", "6");
    String _characterO = _characterB.replaceAll("O", "0");
    String _characterL = _characterO.replaceAll("L", "1");
    _characterL = _characterL.replaceAll("U", "1");
    _characterL = _characterL.replaceAll("n", "0");
    _characterL = _characterL.replaceAll("o", "0");
    _characterL = _characterL.replaceAll("D", "0");
    _characterL = _characterL.replaceAll("d", "0");
    _characterL = _characterL.replaceAll("e", "2");
    _characterL = _characterL.replaceAll("B", "3");
    return _characterL;
  }

  @override
  Widget build(BuildContext context) {
    _screenHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        title: Text('Preview',style: TextStyle(color: Colors.black)),
        backgroundColor: myPrimaryColor,
        leading: IconButton(
            onPressed: (){
              Navigator.pop(context, {"status":false,"value":null});
            },icon: Icon(Icons.arrow_back,color: Colors.black),
        ),
      ),
      body: Container(
          child: Center(
              child: _processImage
                  ?
              CircularProgressIndicator()
                  :
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: Center(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          ClipRRect(
                              borderRadius: BorderRadius.circular(8),
                              child: Image.file(_imageFile)),
                          SizedBox(height: 16),
                          Row(
                            children: <Widget>[Text("NIK : "), Text("$_nik")],
                          )
                        ]
                    )
                ),
              )
          )
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.check,color: Colors.black),
        backgroundColor: myPrimaryColor,
        onPressed: () {
          if(_nik != "") Navigator.pop(context,{"status":true,"value":_nik});
        },
      ),
    );
  }

//  Future<ByteData> getBytesFromFile() async {
//    Uint8List bytes = File(widget.imagePath).readAsBytesSync() as Uint8List;
//    return ByteData.view(bytes.buffer);
//  }

  _resizePhoto(String filePath) async {
    setState(() {
      _processImage = true;
    });

    final _myImage =
    await FlutterExifRotation.rotateAndSaveImage(path: filePath);
    List<int> _myImageBytes = await _myImage.readAsBytes();

    final originalImage = IMG.decodeImage(_myImageBytes);

    var cropSize = min(originalImage.width, originalImage.height);
    print("cropsize $cropSize");

    int offsetX = (originalImage.width -
        min(originalImage.width, originalImage.height)) ~/
        2;
    print(offsetX);
    int offsetY = (originalImage.height -
        min(originalImage.width, originalImage.height)) ~/
        2;
    print(originalImage.height);

    IMG.Image destImage = IMG.copyCrop(
        originalImage, offsetX, offsetY, cropSize - 120, cropSize - 480);
    final _fixedFile = await _myImage.writeAsBytes(IMG.encodeJpg(destImage));

    setState(() {
      _imageFile = _fixedFile;
      _processImage = false;
    });
    _readText(_imageFile);

//    final originalFile = File(filePath);
//    List<int> imageBytes = await originalFile.readAsBytes();
//
//    final originalImage = IMG.decodeImage(imageBytes);
//
//    final height = originalImage.height;
//    final width = originalImage.width;
//    print("height ${originalImage.height}");
//    print("width ${originalImage.width}");
//
//    var cropSize = min(originalImage.width, originalImage.height);
//    print("cropsize $cropSize");
//    print("screen height $_screenHeight");
//    if(_screenHeight < 700){
//      if (height > width) {
//        var cropSize = min(originalImage.width, originalImage.height);
//        print("cropsize $cropSize");
//
//        int offsetX = (originalImage.width -
//            min(originalImage.width, originalImage.height)) ~/
//            2;
//        print(offsetX);
//        int offsetY = (originalImage.height -
//            min(originalImage.width, originalImage.height)) ~/
//            2;
//        print(originalImage.height);
//
//        IMG.Image destImage = IMG.copyCrop(
//            originalImage, offsetX, offsetY, cropSize-120, cropSize - 480);
//        final _fixedFile =
//        await originalFile.writeAsBytes(IMG.encodeJpg(destImage));
//
//        setState(() {
//          _imageFile = _fixedFile;
//          _processImage = false;
//        });
//      }
//      else {
//        print("landscape");
//        final _fixedImage = IMG.copyRotate(originalImage, 90);
//        final _fixedFile =
//        await originalFile.writeAsBytes(IMG.encodeJpg(_fixedImage));
//        List<int> imageBytes = await _fixedFile.readAsBytes();
//
//        final _newOriginalImage = IMG.decodeImage(imageBytes);
//
//        var cropSize = min(_newOriginalImage.width, _newOriginalImage.height);
//        print("cropsize $cropSize");
//
//        int offsetX = (_newOriginalImage.width -
//            min(_newOriginalImage.width, _newOriginalImage.height)) ~/
//            2;
//        print(offsetX);
//        int offsetY = (_newOriginalImage.height -
//            min(_newOriginalImage.width, _newOriginalImage.height)) ~/
//            2;
//        print(originalImage.height);
//
//        IMG.Image destImage = IMG.copyCrop(
//            _newOriginalImage, offsetX, offsetY, cropSize, cropSize - 480);
//        final _newFixedFile =
//        await originalFile.writeAsBytes(IMG.encodeJpg(destImage));
//
//        setState(() {
//          _imageFile = _newFixedFile;
//          _processImage = false;
//          _height = height.toString();
//          _width = width.toString();
//        });
//      }
//    }
//    else{
//      int offsetX = (originalImage.height -
//          min(originalImage.width, originalImage.height)) ~/
//          2;
//      int offsetY = (originalImage.width -
//          max(originalImage.width, originalImage.height)) ~/
//          2;
//      print("Cek setelah proses width $offsetX");
//      print("Cek setelah proses height $offsetY");
//
//      IMG.Image destImage = IMG.copyCrop(
//          originalImage, offsetX, offsetY, cropSize, cropSize);
//      final _fixedFile =
//      await originalFile.writeAsBytes(IMG.encodeJpg(destImage));
//
//      setState(() {
//        _imageFile = _fixedFile;
//        _processImage = false;
//        _height = height.toString();
//        _width = width.toString();
//      });
//    }
    // We'll use the exif package to read exif data
    // This is map of several exif properties
    // Let's check 'Image Orientation'
//    final exifData = await readExifFromBytes(imageBytes);
//
//    IMG.Image fixedImage;
//
//    if (height < width) {
//      // rotate
//      if (exifData['Image Orientation'].printable.contains('Horizontal')) {
//        fixedImage = IMG.copyRotate(originalImage, 90);
//      } else if (exifData['Image Orientation'].printable.contains('180')) {
//        fixedImage = IMG.copyRotate(originalImage, -90);
//      } else if (exifData['Image Orientation'].printable.contains('CCW')) {
//        fixedImage = IMG.copyRotate(originalImage, 180);
//      } else {
//        fixedImage = IMG.copyRotate(originalImage, 0);
//      }
//    }
//
//    // Here you can select whether you'd like to save it as png
//    // or jpg with some compression
//    // I choose jpg with 100% quality
//    final fixedFile = await originalFile.writeAsBytes(IMG.encodeJpg(fixedImage));

//
//    var bytes = await File(filePath).readAsBytes();
//    IMG.Image src = IMG.decodeImage(bytes);
//    print("${src.height} ${src.width}");
//    var cropSize = min(src.width, src.height);
//    print("cropsize $cropSize");
//
//    int offsetX = (src.width - min(src.width, src.height)) ~/ 2;
//    int offsetY = (src.height - min(src.width, src.height)) ~/ 2;
//
//    IMG.Image destImage = IMG.copyCrop(src, offsetX, offsetY, cropSize, cropSize);
//    Uint8List jpg = IMG.encodeJpg(destImage);
//    setState(() {
//      _imageFile = fixedFile;
//      _processImage = false;
//    });
  }
}

//import 'dart:io';
//import 'dart:math';
//import 'dart:typed_data';
////import 'package:esys_flutter_share/esys_flutter_share.dart';
//import 'package:firebase_ml_vision/firebase_ml_vision.dart';
//import 'package:flutter/material.dart';
//import 'package:image_cropper/image_cropper.dart';
//import 'package:nyobak_camera/previewscreen/crop_image.dart';
//import 'package:path/path.dart';
//import 'package:image/image.dart' as IMG;
//
//class PreviewImageScreen extends StatefulWidget {
//  final String imagePath;
//
//  PreviewImageScreen({this.imagePath});
//
//  @override
//  _PreviewImageScreenState createState() => _PreviewImageScreenState();
//}
//
//class _PreviewImageScreenState extends State<PreviewImageScreen> {
//  File _imageFile;
//  Uint8List _image;
//  bool _processImage = false;
//
//  @override
//  void initState() {
//    super.initState();
//    _resizePhoto(widget.imagePath);
////    _cropImage();
//  }
//  Future<Null> _cropImage() async {
//    File croppedFile = await ImageCropper.cropImage(
//        sourcePath: widget.imagePath,
//        aspectRatioPresets: Platform.isAndroid
//            ? [
//          CropAspectRatioPreset.square,
//          CropAspectRatioPreset.ratio3x2,
//          CropAspectRatioPreset.original,
//          CropAspectRatioPreset.ratio4x3,
//          CropAspectRatioPreset.ratio16x9
//        ]
//            : [
//          CropAspectRatioPreset.original,
//          CropAspectRatioPreset.square,
//          CropAspectRatioPreset.ratio3x2,
//          CropAspectRatioPreset.ratio4x3,
//          CropAspectRatioPreset.ratio5x3,
//          CropAspectRatioPreset.ratio5x4,
//          CropAspectRatioPreset.ratio7x5,
//          CropAspectRatioPreset.ratio16x9
//        ],
//        androidUiSettings: AndroidUiSettings(
//            toolbarTitle: 'Cropper',
//            toolbarColor: Colors.deepOrange,
//            toolbarWidgetColor: Colors.white,
//            initAspectRatio: CropAspectRatioPreset.original,
//            lockAspectRatio: false),
//        iosUiSettings: IOSUiSettings(
//          title: 'Cropper',
//        )
//    );
//    if (croppedFile != null) {
//      setState(() {
//        _imageFile = croppedFile;
//      });
//      readText();
//    }
//  }
//
//  Future readText() async {
//    FirebaseVisionImage ourImage = FirebaseVisionImage.fromFile(_imageFile);
//    TextRecognizer recognizeText = FirebaseVision.instance.textRecognizer();
//    VisionText readText = await recognizeText.processImage(ourImage);
//
//    List<String> _myText = [];
//    for (TextBlock block in readText.blocks) {
//      for (TextLine line in block.lines) {
//        print(line.text);
//      }
//    }
//    recognizeText.close();
//  }
//  @override
//  Widget build(BuildContext context) {
//    return Scaffold(
//      appBar: AppBar(
//        title: Text('Preview'),
//        backgroundColor: Colors.blueGrey,
//      ),
//      body: Container(
//          child: Image.file(File(widget.imagePath))
////          _processImage ? SizedBox() : Image.memory(_image)
////          _imageFile != null
////              ?
////          Center(child: Image.file(_imageFile,))
////              :
////          SizedBox(height: 0.0,width: 0.0)
//      )
//    );
//  }
//
////  Future<ByteData> getBytesFromFile() async {
////    Uint8List bytes = File(widget.imagePath).readAsBytesSync() as Uint8List;
////    return ByteData.view(bytes.buffer);
////  }
//
//  _resizePhoto(String filePath) async {
//    setState(() {
//      _processImage = true;
//    });
//    var bytes = await File(filePath).readAsBytes();
//    IMG.Image src = IMG.decodeImage(bytes);
//    print("${src.height} ${src.width}");
//    var cropSize = min(src.width, src.height);
//    print("cropsize $cropSize");
//
//    int offsetX = (src.width - min(src.width, src.height)) ~/ 2;
//    int offsetY = (src.height - min(src.width, src.height)) ~/ 2;
//
//    IMG.Image destImage = IMG.copyCrop(src, offsetX, offsetY, cropSize, cropSize);
//    Uint8List jpg = IMG.encodeJpg(destImage);
//    setState(() {
//      _image = jpg;
//      _processImage = false;
//    });
//  }
//}
