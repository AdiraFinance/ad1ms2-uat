import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/shared/search_source_order_name_dsa_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SearchSourceOrderNameDSA extends StatefulWidget {
  @override
  _SearchSourceOrderNameDSAState createState() => _SearchSourceOrderNameDSAState();
}

class _SearchSourceOrderNameDSAState extends State<SearchSourceOrderNameDSA> {
  @override
  void initState() {
    super.initState();
    Provider.of<SearchSourceOrderNameDSAChangeNotifier>(context, listen: false).getSourceOrderNameDSA();
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
          primaryColor: Colors.black,
          accentColor: myPrimaryColor,
          fontFamily: "NunitoSans",
          primarySwatch: primaryOrange
      ),
      child: Scaffold(
        key: Provider.of<SearchSourceOrderNameDSAChangeNotifier>(context,listen: false).scaffoldKey,
        appBar: AppBar(
          title: Consumer<SearchSourceOrderNameDSAChangeNotifier>(
            builder: (context, searchSourceOrderNameDSAChangeNotifier, _) {
              return TextFormField(
                controller: searchSourceOrderNameDSAChangeNotifier.controllerSearch,
                style: TextStyle(color: Colors.black),
                textInputAction: TextInputAction.search,
                onFieldSubmitted: (query) {
                  // if(query.length >= 3) {
                    searchSourceOrderNameDSAChangeNotifier.searchSourceOrderNameDSA(query);
                  // } else {
                  //   searchSourceOrderNameDSAChangeNotifier.showSnackBar("Input minimal 3 karakter");
                  // }
                },
                onChanged: (e) {
                  searchSourceOrderNameDSAChangeNotifier.changeAction(e);
                },
                textCapitalization: TextCapitalization.characters,
                cursorColor: Colors.black,
                decoration: new InputDecoration(
                  hintText: "Cari Nama Sumber Order (minimal 3 karakter)",
                  hintStyle: TextStyle(color: Colors.black),
                  enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: myPrimaryColor),
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: myPrimaryColor),
                  ),
                ),
                autofocus: true,
              );
            },
          ),
          backgroundColor: myPrimaryColor,
          iconTheme: IconThemeData(color: Colors.black),
          actions: <Widget>[
            Provider.of<SearchSourceOrderNameDSAChangeNotifier>(context,
                        listen: true)
                    .showClear
                ? IconButton(
                    icon: Icon(Icons.clear),
                    onPressed: () {
                      Provider.of<SearchSourceOrderNameDSAChangeNotifier>(context,
                          listen: false).clearSearchTemp();
                      Provider.of<SearchSourceOrderNameDSAChangeNotifier>(context,
                              listen: false)
                          .controllerSearch
                          .clear();
                      Provider.of<SearchSourceOrderNameDSAChangeNotifier>(context,
                              listen: false)
                          .changeAction(
                              Provider.of<SearchSourceOrderNameDSAChangeNotifier>(
                                      context,
                                      listen: false)
                                  .controllerSearch
                                  .text);
                    })
                : SizedBox(
                    width: 0.0,
                    height: 0.0,
                  )
          ],
        ),
        body: Consumer<SearchSourceOrderNameDSAChangeNotifier>(
          builder: (context, searchSourceOrderNameDSAChangeNotifier, _) {
            return searchSourceOrderNameDSAChangeNotifier.loadData
                ?
            Center(
              child: CircularProgressIndicator(),
            )
                :
            searchSourceOrderNameDSAChangeNotifier.listSourceOrderNameDSATemp.isNotEmpty
                ?
            ListView.separated(
              padding: EdgeInsets.symmetric(
                  vertical: MediaQuery.of(context).size.height / 57,
                  horizontal: MediaQuery.of(context).size.width / 27),
              itemCount:
              searchSourceOrderNameDSAChangeNotifier.listSourceOrderNameDSATemp.length,
              itemBuilder: (listContext, index) {
                return InkWell(
                  onTap: () {
                    Navigator.pop(
                        context,
                        searchSourceOrderNameDSAChangeNotifier
                            .listSourceOrderNameDSATemp[index]);
                  },
                  child: Container(
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Text(
                          "${searchSourceOrderNameDSAChangeNotifier.listSourceOrderNameDSATemp[index].kode} - "
                              "${searchSourceOrderNameDSAChangeNotifier.listSourceOrderNameDSATemp[index].deskripsi}",
                          style: TextStyle(fontSize: 16),
                        )
                      ],
                    ),
                  ),
                );
              },
              separatorBuilder: (context, index) {
                return Divider();
              },
            )
                :
            searchSourceOrderNameDSAChangeNotifier.listSourceOrderNameDSA.isNotEmpty
                ?
            ListView.separated(
              padding: EdgeInsets.symmetric(
                  vertical: MediaQuery.of(context).size.height / 57,
                  horizontal: MediaQuery.of(context).size.width / 27),
              itemCount:
                  searchSourceOrderNameDSAChangeNotifier.listSourceOrderNameDSA.length,
              itemBuilder: (listContext, index) {
                return InkWell(
                  onTap: () {
                    Navigator.pop(
                        context,
                        searchSourceOrderNameDSAChangeNotifier
                            .listSourceOrderNameDSA[index]);
                  },
                  child: Container(
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Text(
                          "${searchSourceOrderNameDSAChangeNotifier
                              .listSourceOrderNameDSA[index].kode} - "
                              "${searchSourceOrderNameDSAChangeNotifier
                              .listSourceOrderNameDSA[index].deskripsi}",
                          style: TextStyle(fontSize: 16),
                        )
                      ],
                    ),
                  ),
                );
              },
              separatorBuilder: (context, index) {
                return Divider();
              },
            )
                :
            Center(
              child: Text("Data tidak ditemukan"),
            );
          },
        ),
      ),
    );
  }
}
