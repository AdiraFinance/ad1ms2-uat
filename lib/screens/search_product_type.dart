import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/shared/search_product_type_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SearchProductType extends StatefulWidget {
  final String flag;
  final String kodeGroupObject;
  final String kodeObject;

  const SearchProductType({Key key, this.flag, this.kodeGroupObject, this.kodeObject}) : super(key: key);
  @override
  _SearchProductTypeState createState() => _SearchProductTypeState();
}

class _SearchProductTypeState extends State<SearchProductType> {
  @override
  void initState() {
    super.initState();
    Provider.of<SearchProductTypeChangeNotifier>(context,listen: false).getProductType(context, widget.flag, widget.kodeGroupObject, widget.kodeObject);
  }
  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
          primaryColor: Colors.black,
          accentColor: myPrimaryColor,
          fontFamily: "NunitoSans",
          primarySwatch: primaryOrange
      ),
      child: Scaffold(
        key: Provider.of<SearchProductTypeChangeNotifier>(context,listen: false).scaffoldKey,
        appBar: AppBar(
          title: Consumer<SearchProductTypeChangeNotifier>(
            builder: (context, searchProductTypeChangeNotifier, _) {
              return TextFormField(
                controller: searchProductTypeChangeNotifier.controllerSearch,
                style: TextStyle(color: Colors.black),
                textInputAction: TextInputAction.search,
                onFieldSubmitted: (query) {
                  searchProductTypeChangeNotifier.searchProductType(query);
                },
                onChanged: (e) {
                  searchProductTypeChangeNotifier.changeAction(e);
                },
                textCapitalization: TextCapitalization.characters,
                cursorColor: Colors.black,
                decoration: new InputDecoration(
                  hintText: "Cari Jenis Produk (minimal 3 karakter)",
                  hintStyle: TextStyle(color: Colors.black),
                  enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: myPrimaryColor),
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: myPrimaryColor),
                  ),
                ),
                autofocus: true,
              );
            },
          ),
          backgroundColor: myPrimaryColor,
          iconTheme: IconThemeData(color: Colors.black),
          actions: <Widget>[
            Provider.of<SearchProductTypeChangeNotifier>(context, listen: true)
                    .showClear
                ? IconButton(
                    icon: Icon(Icons.clear),
                    onPressed: () {
                      Provider.of<SearchProductTypeChangeNotifier>(context,
                          listen: false).clearSearchTemp();
                      Provider.of<SearchProductTypeChangeNotifier>(context,
                              listen: false)
                          .controllerSearch
                          .clear();
                      Provider.of<SearchProductTypeChangeNotifier>(context,
                              listen: false)
                          .changeAction(
                              Provider.of<SearchProductTypeChangeNotifier>(
                                      context,
                                      listen: false)
                                  .controllerSearch
                                  .text);
                    })
                : SizedBox(
                    width: 0.0,
                    height: 0.0,
                  )
          ],
        ),
        body: Consumer<SearchProductTypeChangeNotifier>(
          builder: (context, searchProductTypeChangeNotifier, _) {
            return searchProductTypeChangeNotifier.loadData
                ?
            Center(child: CircularProgressIndicator())
                :
            searchProductTypeChangeNotifier.listProductTypeTemp.isNotEmpty
                ?
            ListView.separated(
              padding: EdgeInsets.symmetric(
                  vertical: MediaQuery.of(context).size.height / 57,
                  horizontal: MediaQuery.of(context).size.width / 27),
              itemCount: searchProductTypeChangeNotifier.listProductTypeTemp.length,
              itemBuilder: (listContext, index) {
                return InkWell(
                  onTap: () {
                    Navigator.pop(context,
                        searchProductTypeChangeNotifier.listProductTypeTemp[index]);
                  },
                  child: Container(
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Text(
                          "${searchProductTypeChangeNotifier.listProductTypeTemp[index].id} - "
                              "${searchProductTypeChangeNotifier.listProductTypeTemp[index].name} ",
                          style: TextStyle(fontSize: 16),
                        )
                      ],
                    ),
                  ),
                );
              },
              separatorBuilder: (context, index) {
                return Divider();
              },
            )
                :
            ListView.separated(
              padding: EdgeInsets.symmetric(
                  vertical: MediaQuery.of(context).size.height / 57,
                  horizontal: MediaQuery.of(context).size.width / 27),
              itemCount: searchProductTypeChangeNotifier.listProductType.length,
              itemBuilder: (listContext, index) {
                return InkWell(
                  onTap: () {
                    Navigator.pop(context,
                        searchProductTypeChangeNotifier.listProductType[index]);
                  },
                  child: Container(
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Text(
                          "${searchProductTypeChangeNotifier.listProductType[index].id} - "
                          "${searchProductTypeChangeNotifier.listProductType[index].name} ",
                          style: TextStyle(fontSize: 16),
                        )
                      ],
                    ),
                  ),
                );
              },
              separatorBuilder: (context, index) {
                return Divider();
              },
            );
          },
        ),
      ),
    );
  }
}
