import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/shared/search_object_usage_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SearchUsageObject extends StatefulWidget {
  @override
  _SearchUsageObjectState createState() => _SearchUsageObjectState();
}

class _SearchUsageObjectState extends State<SearchUsageObject> {
  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
          primaryColor: Colors.black,
          accentColor: myPrimaryColor,
          fontFamily: "NunitoSans",
          primarySwatch: primaryOrange
      ),
      child: Scaffold(
        appBar: AppBar(
          title: Consumer<SearchObjectUsageChangeNotifier>(
            builder: (context, searchObjectUsageChangeNotifier, _) {
              return TextFormField(
                controller: searchObjectUsageChangeNotifier.controllerSearch,
                style: TextStyle(color: Colors.black),
                textInputAction: TextInputAction.search,
                onFieldSubmitted: (e) {
                // _getCustomer(e);
                },
                onChanged: (e) {
                  searchObjectUsageChangeNotifier.changeAction(e);
                },
                cursorColor: Colors.black,
                decoration: new InputDecoration(
                  hintText: "Cari Pemakaian Objek (minimal 3 karakter)",
                  hintStyle: TextStyle(color: Colors.black),
                  enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: myPrimaryColor),
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: myPrimaryColor),
                  ),
                ),
                autofocus: true,
                textCapitalization: TextCapitalization.characters,
              );
            },
          ),
          backgroundColor: myPrimaryColor,
          iconTheme: IconThemeData(color: Colors.black),
          actions: <Widget>[
            Provider.of<SearchObjectUsageChangeNotifier>(context, listen: true)
                .showClear
                ? IconButton(
                icon: Icon(Icons.clear),
                onPressed: () {
                  Provider.of<SearchObjectUsageChangeNotifier>(context,
                      listen: false)
                      .controllerSearch
                      .clear();
                  Provider.of<SearchObjectUsageChangeNotifier>(context,
                      listen: false)
                      .changeAction(
                      Provider.of<SearchObjectUsageChangeNotifier>(
                          context,
                          listen: false)
                          .controllerSearch
                          .text);
                })
                : SizedBox(
              width: 0.0,
              height: 0.0,
            )
          ],
        ),
        body: Consumer<SearchObjectUsageChangeNotifier>(
          builder: (context, searchObjectUsageChangeNotifier, _) {
            return ListView.separated(
              padding: EdgeInsets.symmetric(
                  vertical: MediaQuery.of(context).size.height / 57,
                  horizontal: MediaQuery.of(context).size.width / 27),
              itemCount:
              searchObjectUsageChangeNotifier.listObjectUsage.length,
              itemBuilder: (listContext, index) {
                return InkWell(
                  onTap: () {
                    Navigator.pop(
                        context,
                        searchObjectUsageChangeNotifier
                            .listObjectUsage[index]);
                  },
                  child: Container(
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Text(
                          "${searchObjectUsageChangeNotifier.listObjectUsage[index].id} - "
                              "${searchObjectUsageChangeNotifier.listObjectUsage[index].name} ",
                          style: TextStyle(fontSize: 16),
                        )
                      ],
                    ),
                  ),
                );
              },
              separatorBuilder: (context, index) {
                return Divider();
              },
            );
          },
        ),
      ),
    );
  }
}
