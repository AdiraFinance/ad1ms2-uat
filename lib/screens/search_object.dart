import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/shared/search_object_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SearchObject extends StatefulWidget {
  final String kodeObject;
  final String unitColla;

  const SearchObject({this.kodeObject, this.unitColla});
  @override
  _SearchObjectState createState() => _SearchObjectState();
}

class _SearchObjectState extends State<SearchObject> {
  @override
  void initState() {
    super.initState();
    Provider.of<SearchObjectChangeNotifier>(context, listen: false).getObject(context, widget.kodeObject, widget.unitColla);
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
          primaryColor: Colors.black,
          accentColor: myPrimaryColor,
          fontFamily: "NunitoSans",
          primarySwatch: primaryOrange
      ),
      child: Scaffold(
        key: Provider.of<SearchObjectChangeNotifier>(context, listen: false).scaffoldKey,
        appBar: AppBar(
          title: Consumer<SearchObjectChangeNotifier>(
            builder: (context, searchObjectChangeNotifier, _) {
              return TextFormField(
                controller: searchObjectChangeNotifier.controllerSearch,
                style: TextStyle(color: Colors.black),
                textInputAction: TextInputAction.search,
                onFieldSubmitted: (query) {
                  searchObjectChangeNotifier.searchObject(query);
                },
                onChanged: (e) {
                  searchObjectChangeNotifier.changeAction(e);
                },
                textCapitalization: TextCapitalization.characters,
                cursorColor: Colors.black,
                decoration: new InputDecoration(
                  hintText: "Cari Objek (minimal 3 karakter)",
                  hintStyle: TextStyle(color: Colors.black),
                  enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: myPrimaryColor),
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: myPrimaryColor),
                  ),
                ),
                autofocus: true,
              );
            },
          ),
          backgroundColor: myPrimaryColor,
          iconTheme: IconThemeData(color: Colors.black),
          actions: <Widget>[
            Provider.of<SearchObjectChangeNotifier>(context, listen: true)
                    .showClear
                ? IconButton(
                    icon: Icon(Icons.clear),
                    onPressed: () {
                      Provider.of<SearchObjectChangeNotifier>(context,
                          listen: false).clearSearchTemp();
                      Provider.of<SearchObjectChangeNotifier>(context,
                              listen: false)
                          .controllerSearch
                          .clear();
                      Provider.of<SearchObjectChangeNotifier>(context,
                              listen: false)
                          .changeAction(Provider.of<SearchObjectChangeNotifier>(
                                  context,
                                  listen: false)
                              .controllerSearch
                              .text);
                    })
                : SizedBox(
                    width: 0.0,
                    height: 0.0,
                  )
          ],
        ),
        body: Consumer<SearchObjectChangeNotifier>(
          builder: (context, searchObjectChangeNotifier, _) {
            return searchObjectChangeNotifier.loadData
                ?
            Center(
              child: CircularProgressIndicator(),
            )
                :
            searchObjectChangeNotifier.listObjectTemp.isNotEmpty
                ?
            ListView.separated(
              padding: EdgeInsets.symmetric(
                  vertical: MediaQuery.of(context).size.height / 57,
                  horizontal: MediaQuery.of(context).size.width / 27),
              itemCount: searchObjectChangeNotifier.listObjectTemp.length,
              itemBuilder: (listContext, index) {
                return InkWell(
                  onTap: () {
                    Navigator.pop(context,
                        searchObjectChangeNotifier.listObjectTemp[index]);
                  },
                  child: Container(
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Text(
                          "${searchObjectChangeNotifier.listObjectTemp[index].id} - "
                              "${searchObjectChangeNotifier.listObjectTemp[index].name} ",
                          style: TextStyle(fontSize: 16),
                        )
                      ],
                    ),
                  ),
                );
              },
              separatorBuilder: (context, index) {
                return Divider();
              },
            )
                :
            ListView.separated(
              padding: EdgeInsets.symmetric(
                  vertical: MediaQuery.of(context).size.height / 57,
                  horizontal: MediaQuery.of(context).size.width / 27),
              itemCount: searchObjectChangeNotifier.listObjectModel.length,
              itemBuilder: (listContext, index) {
                return InkWell(
                  onTap: () {
                    Navigator.pop(context,
                        searchObjectChangeNotifier.listObjectModel[index]);
                  },
                  child: Container(
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Text(
                          "${searchObjectChangeNotifier.listObjectModel[index].id} - "
                          "${searchObjectChangeNotifier.listObjectModel[index].name} ",
                          style: TextStyle(fontSize: 16),
                        )
                      ],
                    ),
                  ),
                );
              },
              separatorBuilder: (context, index) {
                return Divider();
              },
            );
          },
        ),
      ),
    );
  }
}
