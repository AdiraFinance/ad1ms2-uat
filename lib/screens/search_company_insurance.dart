import 'package:ad1ms2_dev/shared/search_company_insurance_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../main.dart';

class SearchCompanyInsurance extends StatefulWidget {
    final String flag;

    SearchCompanyInsurance(this.flag);
  @override
    _SearchCompanyInsuranceState createState() => _SearchCompanyInsuranceState();
}

class _SearchCompanyInsuranceState extends State<SearchCompanyInsurance> {

    @override
  void initState() {
    super.initState();
    Provider.of<SearchCompanyInsuranceChangeNotifier>(context,listen: false).getCompanyTypeInsurance(context, widget.flag);
  }
    @override
    Widget build(BuildContext context) {
        return Theme(
          data: ThemeData(
              primaryColor: Colors.black,
              fontFamily: "NunitoSans",
              accentColor: myPrimaryColor,
              primarySwatch: primaryOrange
          ),
          child: Scaffold(
              key: Provider.of<SearchCompanyInsuranceChangeNotifier>(context,listen: false).scaffoldKey,
              appBar: AppBar(
                  title: Consumer<SearchCompanyInsuranceChangeNotifier>(
                      builder: (context, searchCompanyChangeNotifier, _) {
                          return TextFormField(
                              controller: searchCompanyChangeNotifier.controllerSearch,
                              style: TextStyle(color: Colors.black),
                              textInputAction: TextInputAction.search,
                              onFieldSubmitted: (query) {
                                  searchCompanyChangeNotifier.searchCompanyTypeInsurance(query);
                              },
                              onChanged: (e) {
                                  searchCompanyChangeNotifier.changeAction(e);
                              },
                              textCapitalization: TextCapitalization.characters,
                              cursorColor: Colors.black,
                              decoration: new InputDecoration(
                                  hintText: "Cari Perusahaan (minimal 3 karakter)",
                                  hintStyle: TextStyle(color: Colors.black),
                                  enabledBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(color: myPrimaryColor),
                                  ),
                                  focusedBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(color: myPrimaryColor),
                                  ),
                              ),
                              autofocus: true,
                          );
                      },
                  ),
                  backgroundColor: myPrimaryColor,
                  iconTheme: IconThemeData(color: Colors.black),
                  actions: <Widget>[
                      Provider.of<SearchCompanyInsuranceChangeNotifier>(context, listen: true)
                          .showClear
                          ? IconButton(
                          icon: Icon(Icons.clear),
                          onPressed: () {
                              Provider.of<SearchCompanyInsuranceChangeNotifier>(context,
                                  listen: false).clearSearchInsuranceTemp();
                              Provider.of<SearchCompanyInsuranceChangeNotifier>(context,
                                  listen: false)
                                  .controllerSearch
                                  .clear();
                              Provider.of<SearchCompanyInsuranceChangeNotifier>(context,
                                  listen: false)
                                  .changeAction(Provider.of<SearchCompanyInsuranceChangeNotifier>(
                                  context,
                                  listen: false)
                                  .controllerSearch
                                  .text);
                          })
                          : SizedBox(
                          width: 0.0,
                          height: 0.0,
                      )
                  ],
              ),
              body: Consumer<SearchCompanyInsuranceChangeNotifier>(
                  builder: (context, searchCompanyChangeNotifier, _) {
                      return searchCompanyChangeNotifier.loadData
                          ?
                      Center(child: CircularProgressIndicator())
                          :
                      searchCompanyChangeNotifier.listCompanyInsuranceTemp.isNotEmpty
                          ?
                      ListView.separated(
                          padding: EdgeInsets.symmetric(
                              vertical: MediaQuery.of(context).size.height / 57,
                              horizontal: MediaQuery.of(context).size.width / 27),
                          itemCount: searchCompanyChangeNotifier.listCompanyInsuranceTemp.length,
                          itemBuilder: (listContext, index) {
                              return InkWell(
                                  onTap: () {
                                      Navigator.pop(context,
                                          searchCompanyChangeNotifier.listCompanyInsuranceTemp[index]);
                                  },
                                  child: Container(
                                      child: Row(
                                          mainAxisSize: MainAxisSize.max,
                                          children: [
                                              Text(
                                                  "${searchCompanyChangeNotifier.listCompanyInsuranceTemp[index].KODE} - "
                                                      "${searchCompanyChangeNotifier.listCompanyInsuranceTemp[index].DESKRIPSI} ",
                                                  style: TextStyle(fontSize: 16),
                                              )
                                          ],
                                      ),
                                  ),
                              );
                          },
                          separatorBuilder: (context, index) {
                              return Divider();
                          },
                      )
                          :
                      ListView.separated(
                          padding: EdgeInsets.symmetric(
                              vertical: MediaQuery.of(context).size.height / 57,
                              horizontal: MediaQuery.of(context).size.width / 27),
                          itemCount: searchCompanyChangeNotifier.listCompanyInsuranceModel.length,
                          itemBuilder: (listContext, index) {
                              return InkWell(
                                  onTap: () {
                                      Navigator.pop(context,
                                          searchCompanyChangeNotifier.listCompanyInsuranceModel[index]);
                                  },
                                  child: Container(
                                      child: Row(
                                          mainAxisSize: MainAxisSize.max,
                                          children: [
                                              Text(
                                                  "${searchCompanyChangeNotifier.listCompanyInsuranceModel[index].KODE} - "
                                                      "${searchCompanyChangeNotifier.listCompanyInsuranceModel[index].DESKRIPSI} ",
                                                  style: TextStyle(fontSize: 16),
                                              )
                                          ],
                                      ),
                                  ),
                              );
                          },
                          separatorBuilder: (context, index) {
                              return Divider();
                          },
                      );
                  },
              ),
          ),
        );
    }
}
