import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/shared/search_birth_place_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_document_type_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_reference_number_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SearchDocumentType extends StatefulWidget {
  final int flag;

  const SearchDocumentType(this.flag);
  @override
  _SearchDocumentTypeState createState() => _SearchDocumentTypeState();
}

class _SearchDocumentTypeState extends State<SearchDocumentType> {
  @override
  void initState() {
    super.initState();
    Provider.of<SearchDocumentTypeChangeNotifier>(context, listen: false).getDocumentType(context,widget.flag);
  }
  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
        fontFamily: 'NunitoSans',
        accentColor: myPrimaryColor
      ),
      child: Scaffold(
        key: Provider.of<SearchDocumentTypeChangeNotifier>(context,listen: false).scaffoldKey,
        appBar: AppBar(
          title: Consumer<SearchDocumentTypeChangeNotifier>(
            builder: (context, searchDocumentTypeChangeNotifier, _) {
              return TextFormField(
                controller: searchDocumentTypeChangeNotifier.controllerSearch,
                style: TextStyle(color: Colors.black),
                textInputAction: TextInputAction.search,
                onFieldSubmitted: (e) {
                  searchDocumentTypeChangeNotifier.searchDocumentType(e);
                },
                onChanged: (e) {
                  searchDocumentTypeChangeNotifier.changeAction(e);
                },
                cursorColor: Colors.black,
                decoration: new InputDecoration(
                  hintText: "Cari Tipe Dokumen (minimal 3 karakter)",
                  hintStyle: TextStyle(color: Colors.black),
                  enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: myPrimaryColor),
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: myPrimaryColor),
                  ),
                ),
                textCapitalization: TextCapitalization.characters,
                autofocus: true,
              );
            },
          ),
          backgroundColor: myPrimaryColor,
          iconTheme: IconThemeData(color: Colors.black),
          actions: <Widget>[
            Provider.of<SearchDocumentTypeChangeNotifier>(context, listen: true)
                .showClear
                ? IconButton(
                icon: Icon(Icons.clear),
                onPressed: () {
                  Provider.of<SearchDocumentTypeChangeNotifier>(context, listen: false).clearSearchTemp();
                  Provider.of<SearchDocumentTypeChangeNotifier>(context, listen: false).controllerSearch.clear();
                  Provider.of<SearchDocumentTypeChangeNotifier>(context, listen: false)
                      .changeAction(Provider.of<SearchDocumentTypeChangeNotifier>(context, listen: false)
                      .controllerSearch.text);
                })
                : SizedBox(
              width: 0.0,
              height: 0.0,
            )
          ],
        ),
        body: Consumer<SearchDocumentTypeChangeNotifier>(
          builder: (context, searchDocumentTypeChangeNotifier, _) {
            return searchDocumentTypeChangeNotifier.loadData
                ?
            Center(child: CircularProgressIndicator())
                :
            searchDocumentTypeChangeNotifier.listDocumentTypeTemp.isNotEmpty
                ?
            ListView.separated(
              padding: EdgeInsets.symmetric(
                  vertical: MediaQuery.of(context).size.height / 57,
                  horizontal: MediaQuery.of(context).size.width / 27),
              itemCount: searchDocumentTypeChangeNotifier.listDocumentTypeTemp.length,
              itemBuilder: (listContext, index) {
                return InkWell(
                  onTap: () {
                    Navigator.pop(context,
                        searchDocumentTypeChangeNotifier.listDocumentTypeTemp[index]);
                  },
                  child: Container(
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Expanded(
                          flex: 0,
                          child: Text(
                            "${searchDocumentTypeChangeNotifier.listDocumentTypeTemp[index].docTypeId}",
                            style: TextStyle(fontSize: 16),
                          ),
                        ),
                        Text(" - "),
                        Expanded(
                          flex: 8,
                          child: Text(
                            "${searchDocumentTypeChangeNotifier.listDocumentTypeTemp[index].docTypeName}",
                            style: TextStyle(fontSize: 16),
                          ),
                        )
                      ],
                    ),
                  ),
                );
              },
              separatorBuilder: (context, index) {
                return Divider();
              },
            )
                :
            ListView.separated(
              padding: EdgeInsets.symmetric(
                  vertical: MediaQuery.of(context).size.height / 57,
                  horizontal: MediaQuery.of(context).size.width / 27),
              itemCount: searchDocumentTypeChangeNotifier.listDocumentType.length,
              itemBuilder: (listContext, index) {
                return InkWell(
                  onTap: () {
                    Navigator.pop(context,
                        searchDocumentTypeChangeNotifier.listDocumentType[index]);
                  },
                  child: Container(
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Expanded(
                          flex: 0,
                          child: Text(
                            "${searchDocumentTypeChangeNotifier.listDocumentType[index].docTypeId}",
                            style: TextStyle(fontSize: 16),
                          ),
                        ),
                        Text(" - "),
                        Expanded(
                          flex: 8,
                          child: Text(
                            "${searchDocumentTypeChangeNotifier.listDocumentType[index].docTypeName}",
                            style: TextStyle(fontSize: 16),
                          ),
                        )
                      ],
                    ),
                  ),
                );
              },
              separatorBuilder: (context, index) {
                return Divider();
              },
            );
          },
        ),
      ),
    );
  }
}
