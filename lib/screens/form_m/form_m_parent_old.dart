import 'dart:io';

import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/screens/form_m/form_m_guarantor.dart';
import 'package:ad1ms2_dev/screens/form_m/form_m_informasi_keluarga(ibu).dart';
import 'package:ad1ms2_dev/screens/form_m/form_m_informasi_keluarga.dart';
import 'package:ad1ms2_dev/screens/form_m/form_m_pekerjaan.dart';
import 'package:ad1ms2_dev/shared/document_unit_model.dart';
import 'package:ad1ms2_dev/shared/form_m_parent_change_notifier_old.dart';
import 'package:ad1ms2_dev/shared/group_unit_object_model.dart';
import 'package:ad1ms2_dev/shared/responsive_screen.dart';
import 'package:fa_stepper/fa_stepper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:path_provider/path_provider.dart' as path_provider;
import 'package:provider/provider.dart';

import 'form_m_foto.dart';
import 'form_m_income.dart';
import 'form_m_info_nasabah.dart';
import 'form_m_informasi_alamat.dart';

class FormMParentOld extends StatefulWidget {
  @override
  _FormMParentOldState createState() => _FormMParentOldState();
}

class _FormMParentOldState extends State<FormMParentOld> {
  int _currentStep = 0;
  VoidCallback _onStepContinue, _onStepCancel;
  bool _validateFotoTempatTinggal = false;
  bool _validateFotoTempatUsaha = false;
  bool _validateGroupObjectUnit = false;
  bool _validateDocumentObjectUnit = false;
  String _occupationCodeSelected,
      _kegiatanUsahaSelected,
      _jenisKegiatanUsahaSelected,
      _jenisKonsepSelected;
  Screen _size;
  List<Map> _fileImageTmptTinggal = [];
  List<Map> _fileImageTmptUsaha = [];
  List<Map> _currentTotalWidget = [];
  List<GroupUnitObjectModel> _groupObjectUnit = [];
  List<DocumentUnitModel> _listDocumentUnit = [];
  bool _hideTittle = false;

  @override
  Widget build(BuildContext context) {
    _size = Screen(MediaQuery.of(context).size);
    return Theme(
      data: ThemeData(
        fontFamily: "NunitoSans",
        accentColor: myPrimaryColor,
      ),
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: Text("Form M", style: TextStyle(color: Colors.black)),
          backgroundColor: myPrimaryColor,
          iconTheme: IconThemeData(color: Colors.black),
          actions: [
            IconButton(
                icon: Icon(Icons.slideshow),
                onPressed: () {
                  if (_hideTittle) {
                    setState(() {
                      _hideTittle = !_hideTittle;
                    });
                  } else {
                    setState(() {
                      _hideTittle = true;
                    });
                  }
                })
          ],
        ),
        body: Consumer<FormMParentChangeNotifierOld>(
          builder: (context, formMParentChangeNotif, _) {
            return Stack(
              children: [
                FAStepper(
                    titleHeight: _hideTittle ? 0 : 70,
                    currentStep: formMParentChangeNotif.currentStep,
                    type: FAStepperType.vertical,
                    stepNumberColor: myPrimaryColor,
                    onStepTapped: (step) {
                      formMParentChangeNotif.onTapCheck(step, context);
                    },
                    controlsBuilder: (BuildContext context,
                        {VoidCallback onStepContinue,
                        VoidCallback onStepCancel}) {
                      formMParentChangeNotif.onStepContinue = onStepContinue;
                      formMParentChangeNotif.onStepCancel = onStepCancel;
                      return SizedBox(
                        width: 0.0,
                        height: 0.0,
                      );
                    },
                    onStepCancel: formMParentChangeNotif.currentStep > 0
                        ? () => setState(
                            () => formMParentChangeNotif.currentStep -= 1)
                        : null,
                    onStepContinue: () {
                      setState(() => formMParentChangeNotif.currentStep += 1);
                    },
                    steps: [
                      FAStep(
                        title:
                            Text("Foto", style: TextStyle(color: Colors.black)),
//                        Container(
//                            margin: EdgeInsets.only(top: 0.0),
//                            child:
//                            formMParentChangeNotif.currentStep == 0
//                                ? Text("Foto",
//                                    style: TextStyle(color: Colors.black))
//                                : Text("")),
                        content: Form(
                            key: formMParentChangeNotif.formKeys[0],
                            child: FormMFoto()),
                        state: formMParentChangeNotif.flagStatusForm1
                            ? FAStepstate.complete
                            : FAStepstate.disabled,
                        isActive: true,
                      ),
                      FAStep(
                          title: Text("Informasi Nasabah",
                              style: TextStyle(color: Colors.black)),
//                          formMParentChangeNotif.currentStep == 1
//                              ? Text("Informasi Nasabah",
//                                  style: TextStyle(color: Colors.black))
//                              : Text(""),
                          content: Form(
                              key: formMParentChangeNotif.formKeys[1],
                              child: FormMInfoNasabah()),
                          state: formMParentChangeNotif.flagStatusForm2
                              ? FAStepstate.complete
                              : FAStepstate.indexed,
                          isActive: formMParentChangeNotif.enableTapForm2),
                      FAStep(
                          title: Text("Informasi Keluarga(Ibu)",
                              style: TextStyle(color: Colors.black)),
//                          formMParentChangeNotif.currentStep == 2
//                              ? Text("Informasi Alamat",
//                                  style: TextStyle(color: Colors.black))
//                              : Text(""),
                          content: Form(
                              key: formMParentChangeNotif.formKeys[2],
                              child: FormMInformasiAlamat()),
                          state: formMParentChangeNotif.flagStatusForm3
                              ? FAStepstate.complete
                              : FAStepstate.indexed,
                          isActive: formMParentChangeNotif.enableTapForm3),
                      FAStep(
                          title: Text("Informasi Keluarga(Ibu)",
                              style: TextStyle(color: Colors.black)),
//                          formMParentChangeNotif.currentStep == 3
//                              ? Text("Informasi Keluarga(Ibu)",
//                                  style: TextStyle(color: Colors.black))
//                              : Text(""),
                          content: Form(
                            child: FormMInformasiKeluargaIBU(),
                            key: formMParentChangeNotif.formKeys[3],
                          ),
                          state: formMParentChangeNotif.flagStatusForm4
                              ? FAStepstate.complete
                              : FAStepstate.indexed,
                          isActive: formMParentChangeNotif.enableTapForm4),
                      FAStep(
                          title: formMParentChangeNotif.currentStep == 4
                              ? Text("Informasi Keluarga",
                                  style: TextStyle(color: Colors.black))
                              : Text(""),
                          content: Form(
                              child: FormMInfomasiKeluarga(),
                              key: formMParentChangeNotif.formKeys[4]),
                          state: formMParentChangeNotif.flagStatusForm5
                              ? FAStepstate.complete
                              : FAStepstate.indexed,
                          isActive: formMParentChangeNotif.enableTapForm5),
                      FAStep(
                          title: formMParentChangeNotif.currentStep == 5
                              ? Text("Pekerjaan",
                                  style: TextStyle(color: Colors.black))
                              : Text(""),
                          content: Form(
                            key: formMParentChangeNotif.formKeys[5],
                            child: FormMPekerjaan(),
                          ),
                          state: formMParentChangeNotif.flagStatusForm6
                              ? FAStepstate.complete
                              : FAStepstate.indexed,
                          isActive: formMParentChangeNotif.enableTapForm6),
                      FAStep(
                          title: formMParentChangeNotif.currentStep == 6
                              ? Text("Pendapatan",
                                  style: TextStyle(color: Colors.black))
                              : Text(""),
                          content: Form(
                              key: formMParentChangeNotif.formKeys[6],
                              child: FormMIncome()),
                          state: formMParentChangeNotif.flagStatusForm7
                              ? FAStepstate.complete
                              : FAStepstate.indexed,
                          isActive: formMParentChangeNotif.enableTapForm7),
                      FAStep(
                          title: formMParentChangeNotif.currentStep == 7
                              ? Text("Penjamin",
                                  style: TextStyle(color: Colors.black))
                              : Text(""),
                          content: FormMGuarantor(),
                          state: formMParentChangeNotif.flagStatusForm8
                              ? FAStepstate.complete
                              : FAStepstate.indexed,
                          isActive: formMParentChangeNotif.enableTapForm8),
                      FAStep(
                          title: Text(""),
                          content: Text("Testing9"),
                          state: _currentStep > 7
                              ? FAStepstate.complete
                              : FAStepstate.disabled,
                          isActive: _currentStep > 7),
                      FAStep(
                          title: Text(""),
                          content: Text("Testing10"),
                          state: _currentStep > 8
                              ? FAStepstate.complete
                              : FAStepstate.disabled,
                          isActive: _currentStep > 8),
                      FAStep(
                          title: Text(""),
                          content: Text("Testing11"),
                          state: _currentStep > 9
                              ? FAStepstate.complete
                              : FAStepstate.disabled,
                          isActive: _currentStep > 9),
                    ]),
                _hideTittle
                    ? Container(
                        width: MediaQuery.of(context).size.width,
                        height: MediaQuery.of(context).size.height / 47,
                        color: Colors.white,
                      )
                    : SizedBox()
              ],
            );
          },
        ),
        bottomNavigationBar: BottomAppBar(
          elevation: 0.0,
          child: Consumer<FormMParentChangeNotifierOld>(
            builder: (context, formMParentChangeNotif, _) {
              return Container(
                margin: EdgeInsets.symmetric(
                    horizontal: _size.wp(2), vertical: _size.hp(1.5)),
                child: formMParentChangeNotif.currentStep == 0
                    ? RaisedButton(
                        padding: EdgeInsets.only(
                            top: _size.hp(1.5), bottom: _size.hp(1.5)),
                        onPressed: () {
                          formMParentChangeNotif.check(context);
                        },
                        shape: RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(8.0)),
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text('NEXT',
                                style: TextStyle(
                                    fontFamily: "NunitoSans",
                                    color: Colors.black,
                                    fontSize: 14,
                                    fontWeight: FontWeight.w500,
                                    letterSpacing: 1.25))
                          ],
                        ),
                        color: myPrimaryColor,
                      )
                    : Row(
                        children: <Widget>[
                          Expanded(
                            flex: 5,
                            child: RaisedButton(
                                padding: EdgeInsets.only(
                                    top: _size.hp(1.5), bottom: _size.hp(1.5)),
                                onPressed: () {
                                  formMParentChangeNotif.onStepCancel();
                                },
                                shape: RoundedRectangleBorder(
                                    borderRadius:
                                        new BorderRadius.circular(8.0)),
                                child: Text('BACK',
                                    style: TextStyle(
                                        fontFamily: "NunitoSans",
                                        color: Colors.black,
                                        fontSize: 14,
                                        fontWeight: FontWeight.w500,
                                        letterSpacing: 1.25))),
                          ),
                          SizedBox(width: _size.wp(1.5)),
                          Expanded(
                            flex: 5,
                            child: RaisedButton(
                              padding: EdgeInsets.only(
                                  top: _size.hp(1.5), bottom: _size.hp(1.5)),
                              onPressed: () {
                                formMParentChangeNotif.check(context);
                              },
                              shape: RoundedRectangleBorder(
                                  borderRadius: new BorderRadius.circular(8.0)),
                              child: Text('NEXT',
                                  style: TextStyle(
                                      fontFamily: "NunitoSans",
                                      color: Colors.black,
                                      fontSize: 14,
                                      fontWeight: FontWeight.w500,
                                      letterSpacing: 1.25)),
                              color: myPrimaryColor,
                            ),
                          )
                        ],
                      ),
              );
            },
          ),
        ),
      ),
    );
  }
}
