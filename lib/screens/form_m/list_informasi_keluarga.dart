import 'package:ad1ms2_dev/screens/form_m/add_info_keluarga.dart';
import 'package:ad1ms2_dev/shared/add_info_keluarga_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_info_keluarga_change_notif.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../main.dart';

class ListInfoKeluarga extends StatefulWidget {
  @override
  _ListInfoKeluargaState createState() => _ListInfoKeluargaState();
}

class _ListInfoKeluargaState extends State<ListInfoKeluarga> {

  @override
  void initState() {
    super.initState();
    Provider.of<FormMInfoKeluargaChangeNotif>(context,listen: false).checkWarning(context);
  }
  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
        fontFamily: "NunitoSans",
        accentColor: myPrimaryColor
      ),
      child: Scaffold(
        appBar: AppBar(
          title:
              Text("List Informasi Keluarga", style: TextStyle(color: Colors.black)),
          centerTitle: true,
          backgroundColor: myPrimaryColor,
          iconTheme: IconThemeData(color: Colors.black),
        ),
        body: Consumer<FormMInfoKeluargaChangeNotif>(
          builder: (context, formMInfoKelChangeNotif, _) {
            return Form(
              onWillPop: formMInfoKelChangeNotif.onBackPress,
              child: formMInfoKelChangeNotif.listFormInfoKel.isEmpty
              ? Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    // Image.asset("img/alamat_kosong.png", height: MediaQuery.of(context).size.height / 9,),
                    // SizedBox(height: MediaQuery.of(context).size.height / 47,),
                    Text("Tambah Informasi Keluarga", style: TextStyle(color: Colors.grey, fontSize: 16),)
                  ],
                ),
              )
              : ListView.builder(
                  padding: EdgeInsets.symmetric(
                      vertical: MediaQuery.of(context).size.height / 47,
                      horizontal: MediaQuery.of(context).size.width / 37),
                  itemBuilder: (context, index) {
                    String _genderText = "";
                    formMInfoKelChangeNotif.listFormInfoKel[index].gender == "01"
                        ? _genderText = "Laki-Laki"
                        : _genderText = "Perempuan";
                    return InkWell(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ChangeNotifierProvider(
                                    create: (context) =>
                                        AddInfoKeluargaChangeNotif(),
                                    child: AddInfoKeluaraga(
                                      flag: 1,
                                      formMInfoKelModel: formMInfoKelChangeNotif
                                          .listFormInfoKel[index],
                                      index: index,
                                    ))));
                      },
                      child: Card(shadowColor: formMInfoKelChangeNotif.isInfoKeluargaCardChanges ? Colors.purple : Colors.black,
                          elevation: formMInfoKelChangeNotif.isInfoKeluargaCardChanges ? 5.0 : 3.3,
                          child: Stack(
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(12.0),
                                child: Column(
                                  children: [
                                    Row(
                                      children: [
                                        Expanded(
                                            child:
                                                Text("Status hubungan keluarga"),
                                            flex: 5),
                                        Text(" : "),
                                        Expanded(
                                            child: formMInfoKelChangeNotif.listFormInfoKel[index].relationshipStatusModel != null
                                                ?
                                            Text("${formMInfoKelChangeNotif.listFormInfoKel[index].relationshipStatusModel.PARA_FAMILY_TYPE_NAME}") :Text(""),
                                            flex: 5)
                                      ],
                                    ),
                                    SizedBox(
                                        height:
                                            MediaQuery.of(context).size.height /
                                                77),
                                    Row(
                                      children: [
                                        Expanded(
                                            child: Text(
                                                "Nama lengkap sesuai identitas"),
                                            flex: 5),
                                        Text(" : "),
                                        Expanded(
                                            child: Text(
                                                "${formMInfoKelChangeNotif.listFormInfoKel[index].namaLengkapSesuaiIdentitas}"),
                                            flex: 5)
                                      ],
                                    ),
                                    SizedBox(
                                        height:
                                            MediaQuery.of(context).size.height /
                                                77),
                                    Row(
                                      children: [
                                        Expanded(
                                            child: Text("Nama lengkap"), flex: 5),
                                        Text(" : "),
                                        Expanded(
                                            child: Text(
                                                "${formMInfoKelChangeNotif.listFormInfoKel[index].namaLengkap}"),
                                            flex: 5)
                                      ],
                                    ),
                                    SizedBox(
                                        height:
                                            MediaQuery.of(context).size.height /
                                                77),
                                    Row(
                                      children: [
                                        Expanded(
                                            child: Text("Jenis kelamin"),
                                            flex: 5),
                                        Text(" : "),
                                        Expanded(
                                            child: Text(
                                                "$_genderText"),
                                            flex: 5)
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                              Align(
                                alignment: Alignment.topRight,
                                child: IconButton(
                                    icon: Icon(Icons.delete, color: Colors.red),
                                    onPressed: () {
                                      formMInfoKelChangeNotif
                                          .deleteListInfoKel(context, index);
                                    }),
                              ),
                            ],
                          )),
                    );
                  },
                  itemCount: formMInfoKelChangeNotif.listFormInfoKel.length),
            );
          },
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => ChangeNotifierProvider(
                        create: (context) => AddInfoKeluargaChangeNotif(),
                        child: AddInfoKeluaraga(
                          flag: 0,
                          formMInfoKelModel: null,
                          index: null,
                        ))));
          },
          child: Icon(Icons.add, color: Colors.black),
          backgroundColor: myPrimaryColor,
        ),
      ),
    );
  }
}
