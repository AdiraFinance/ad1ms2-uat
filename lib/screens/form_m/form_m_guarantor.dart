import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/screens/form_m/list_guarantor_company.dart';
import 'package:ad1ms2_dev/screens/form_m/list_guarantor_individu.dart';
import 'package:ad1ms2_dev/shared/form_m_add_guarantor_individual_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_company_parent_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_guarantor_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_individu_change_notifier/form_m_parent_individu_change_notifier.dart';
import 'package:ad1ms2_dev/shared/list_oid_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:ad1ms2_dev/shared/form_m_info_nasabah_change_notif.dart';

class FormMGuarantor extends StatefulWidget {
  @override
  _FormMGuarantorState createState() => _FormMGuarantorState();
}

class _FormMGuarantorState extends State<FormMGuarantor> {
  Future<void> _setCustType;

  @override
  void initState() {
    super.initState();
    _setCustType = Provider.of<FormMGuarantorChangeNotifier>(context,listen: false).setCustType(context);
  }
  @override
  Widget build(BuildContext context) {
    // var _custType = "PER";
    //     // Provider.of<ListOidChangeNotifier>(context,listen: false).customerType;
    // if(_custType != "PER"){
    //   Provider.of<FormMGuarantorChangeNotifier>(context,listen: false).radioValueIsWithGuarantor = 0;
    // }
    // else{
    //   Provider.of<FormMGuarantorChangeNotifier>(context,listen: false).radioValueIsWithGuarantor = 0;
    // }
    // Provider.of<FormMGuarantorChangeNotifier>(context,listen: false).setDataFromSQLite();
    return Theme(
      data: ThemeData(
          primaryColor: Colors.black,
          accentColor: myPrimaryColor,
          fontFamily: "NunitoSans",
          primarySwatch: primaryOrange
      ),
      child: FutureBuilder(
        future: _setCustType,
        builder: (context, snapshot) {
          return Consumer<FormMGuarantorChangeNotifier>(
            builder: (context, formMGuarantorChangeNotif, _) {
              return SingleChildScrollView(
                padding: EdgeInsets.symmetric(
                  horizontal: MediaQuery.of(context).size.width/37,
                  vertical: MediaQuery.of(context).size.height/57,
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Ada Penjamin ?",
                      style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w700,
                          letterSpacing: 0.15),
                    ),
                    // SizedBox(height: MediaQuery.of(context).size.height / 47),
                    Row(
                      children: [
                        Row(
                          children: [
                            Radio(
                                activeColor: primaryOrange,
                                value: 0,
                                groupValue: formMGuarantorChangeNotif.radioValueIsWithGuarantor,
                                onChanged: (value) {
                                  formMGuarantorChangeNotif.isDisablePACIAAOSCONA
                                      ? null
                                      : formMGuarantorChangeNotif.radioValueIsWithGuarantor = value;
                                }
                            ),
                            Text("Ya")
                          ],
                        ),
                        SizedBox(height: MediaQuery.of(context).size.height / 47),
                        Row(
                          children: [
                            Radio(
                                activeColor: primaryOrange,
                                value: 1,
                                groupValue: formMGuarantorChangeNotif.radioValueIsWithGuarantor,
                                onChanged:  (value) {
                                  Provider.of<FormMInfoNasabahChangeNotif>(context, listen: false).isGuarantorVisible && formMGuarantorChangeNotif.validationRadio
                                      ?
                                  null
                                      :
                                  formMGuarantorChangeNotif.isDisablePACIAAOSCONA
                                      ?
                                  null
                                      :
                                  formMGuarantorChangeNotif.radioValueIsWithGuarantor = value;
                                }
                            ),
                            Text("Tidak")
                          ],
                        ),
                      ],
                    ),
                    formMGuarantorChangeNotif.radioValueIsWithGuarantor != 0
                        ? SizedBox()
                        : Column(
                      children: [
                        InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        ListGuarantorIndividual()));
                          },
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Card(
                                elevation: 3.3,
                                child: Padding(
                                  padding: const EdgeInsets.all(16.0),
                                  child: Row(
                                    mainAxisSize: MainAxisSize.max,
                                    mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                          "Jumlah data penjamin (Pribadi) ${formMGuarantorChangeNotif.listGuarantorIndividual.length}"),
                                      Icon(Icons.add_circle_outline, color: primaryOrange)
                                    ],
                                  ),
                                ),
                              ),
                              formMGuarantorChangeNotif.autoValidate
                                  ? Container(
                                margin: EdgeInsets.only(
                                    left: MediaQuery.of(context).size.width / 37),
                                child: Text("Tidak boleh kosong",
                                    style: TextStyle(color: Colors.red, fontSize: 12)),
                              )
                                  : SizedBox(height: MediaQuery.of(context).size.height / 87),
                            ],
                          ),
                        ),
                        // SizedBox(height: MediaQuery.of(context).size.height / 47),
                        InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        ListGuarantorCompany()));
                          },
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Card(
                                elevation: 3.3,
                                child: Padding(
                                  padding: const EdgeInsets.all(16.0),
                                  child: Row(
                                    mainAxisSize: MainAxisSize.max,
                                    mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                          "Jumlah data penjamin (Kelembagaan) ${formMGuarantorChangeNotif.listGuarantorCompany.length}"),
                                      Icon(Icons.add_circle_outline, color: primaryOrange)
                                    ],
                                  ),
                                ),
                              ),
                              formMGuarantorChangeNotif.autoValidate
                                  ?
                              Container(
                                  margin: EdgeInsets.only(
                                      left: MediaQuery.of(context).size.width / 37
                                  ),
                                  child: Text(
                                      "Tidak boleh kosong",
                                      style: TextStyle(color: Colors.red, fontSize: 12)
                                  )
                              )
                                  :
                              SizedBox(height:0.0),
                            ],
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              );
            },
          );
        }
      )
    );
  }
}
