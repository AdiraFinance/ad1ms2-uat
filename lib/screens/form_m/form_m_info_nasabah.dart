import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/form_m_informasi_nasabah_model.dart';
import 'package:ad1ms2_dev/shared/form_m_info_nasabah_change_notif.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class FormMInfoNasabah extends StatefulWidget {
  @override
  _FormMInfoNasabahState createState() => _FormMInfoNasabahState();
}

class _FormMInfoNasabahState extends State<FormMInfoNasabah> {
  Future<void> _setPreference;

  @override
  void initState() {
    super.initState();
    // Provider.of<FormMInfoNasabahChangeNotif>(context,listen: false).setDataSQLite();
    // _setGetData =  Provider.of<FormMInfoNasabahChangeNotif>(context,listen: false).getInfoNasabahShowHide();
    _setPreference = Provider.of<FormMInfoNasabahChangeNotif>(context, listen: false).setPreference();
  }

  @override
  Widget build(BuildContext context) {
    var _size = MediaQuery.of(context).size;
    return Theme(
        data: ThemeData(
            primaryColor: Colors.black,
            accentColor: myPrimaryColor,
            fontFamily: "NunitoSans",
            primarySwatch: primaryOrange),
        child: Scaffold(
          key: Provider.of<FormMInfoNasabahChangeNotif>(context, listen: false).scaffoldKey,
          appBar: AppBar(
            title:
            Text("Informasi Nasabah", style: TextStyle(color: Colors.black)),
            centerTitle: true,
            backgroundColor: myPrimaryColor,
            iconTheme: IconThemeData(color: Colors.black),
          ),
          body:
          // Provider.of<FormMInfoNasabahChangeNotif>(context,listen: false).setupData
          //     ?
          // Center(child: CircularProgressIndicator())
          //     :
          FutureBuilder(
            future: _setPreference,
            builder: (context, snapshot){
              return Consumer<FormMInfoNasabahChangeNotif>(
                builder: (context, formMInfoNasabahChangeNotif, _) {
                  formMInfoNasabahChangeNotif.setDefaultValue();
                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  }
                  return Form(
                    onWillPop: formMInfoNasabahChangeNotif.onBackPress,
                    key: formMInfoNasabahChangeNotif.keyForm,
                    child: ListView(
                      padding: EdgeInsets.symmetric(
                          vertical: MediaQuery.of(context).size.height/57,
                          horizontal: MediaQuery.of(context).size.width/37
                      ),
                      children: [
                        Visibility(visible: formMInfoNasabahChangeNotif.isGCVisible,
                          child: IgnorePointer(
                            ignoring: formMInfoNasabahChangeNotif.isDisablePACIAAOSCONA,
                            child: DropdownButtonFormField<GCModel>(
                              value: formMInfoNasabahChangeNotif.gcModel,
                              autovalidate: formMInfoNasabahChangeNotif.autoValidate,
                              validator: (e) {
                                if (e == null && formMInfoNasabahChangeNotif.isGenderMandatory) {
                                  return "Tidak boleh kosong";
                                } else {
                                  return null;
                                }
                              },
                              decoration: InputDecoration(
                                labelText: "Group Customer",
                                filled: formMInfoNasabahChangeNotif.isDisablePACIAAOSCONA,
                                fillColor: Colors.black12,
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(8)),
                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: formMInfoNasabahChangeNotif.isGCChanges ? Colors.purple : Colors.grey)),
                                disabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: formMInfoNasabahChangeNotif.isGCChanges ? Colors.purple : Colors.grey)),
                                contentPadding: EdgeInsets.symmetric(horizontal: 10)),
                              items: formMInfoNasabahChangeNotif.itemsGC.map((data) {
                                return DropdownMenuItem<GCModel>(
                                  value: data,
                                  child: Text(
                                    data.name,
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                );
                              }).toList(),
                              onChanged: (newVal) {
                                formMInfoNasabahChangeNotif.gcModel = newVal;
                              },
                              onTap: () {
                                FocusManager.instance.primaryFocus.unfocus();
                              },
                            ),
                          ),
                        ),
                        Visibility(visible: formMInfoNasabahChangeNotif.isGCVisible,
                            child: SizedBox(height: _size.height / 47)),
                        Visibility(visible: formMInfoNasabahChangeNotif.isIdentitasModelVisible,
                          child: IgnorePointer(
                            ignoring: formMInfoNasabahChangeNotif.isDisablePACIAAOSCONA || formMInfoNasabahChangeNotif.disableJenisPenawaran ? true : false,
                            child: DropdownButtonFormField<IdentityModel>(
                              value: formMInfoNasabahChangeNotif.identitasModel,
                              autovalidate: formMInfoNasabahChangeNotif.autoValidate,
                              validator: (e) {
                                if (e == null && formMInfoNasabahChangeNotif.isIdentitasModelMandatory) {
                                  return "Tidak boleh kosong";
                                } else {
                                  return null;
                                }
                              },
                              disabledHint: Text(formMInfoNasabahChangeNotif.identitasModel.name),
                              decoration: InputDecoration(
                                  labelText: "Jenis Identitas",
                                  filled: formMInfoNasabahChangeNotif.isDisablePACIAAOSCONA || formMInfoNasabahChangeNotif.disableJenisPenawaran ? true : false,
                                  fillColor: Colors.black12,
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8)),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: formMInfoNasabahChangeNotif.isidentitasModelChanges ? Colors.purple : Colors.grey)),
                                  disabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: formMInfoNasabahChangeNotif.isidentitasModelChanges ? Colors.purple : Colors.grey)),
                                  contentPadding: EdgeInsets.symmetric(horizontal: 10)),
                              items: formMInfoNasabahChangeNotif.items.map((data) {
                                return DropdownMenuItem<IdentityModel>(
                                  value: data,
                                  child: Text(
                                    data.name,
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                );
                              }).toList(),
                              onChanged: null,
                              // (newVal) {
                              //   formMInfoNasabahChangeNotif.identitasModel = newVal;
                              // },
                              onTap: () {
                                FocusManager.instance.primaryFocus.unfocus();
                              },
                            ),
                          ),
                        ),
                        Visibility(visible: formMInfoNasabahChangeNotif.isIdentitasModelVisible,
                            child: SizedBox(height: _size.height / 47)),
                        Visibility(visible: formMInfoNasabahChangeNotif.isNoIdentitasVisible,
                          child: IgnorePointer(
                            ignoring: formMInfoNasabahChangeNotif.disableJenisPenawaran || formMInfoNasabahChangeNotif.isDisablePACIAAOSCONA || formMInfoNasabahChangeNotif.enableDataDedup,
                            child: TextFormField(
                                // enabled: formMInfoNasabahChangeNotif.enableDataDedup,
                                controller: formMInfoNasabahChangeNotif.controllerNoIdentitas,
                                autovalidate: formMInfoNasabahChangeNotif.autoValidate,
                                validator: (e) {
                                  if (e.isEmpty && formMInfoNasabahChangeNotif.isNoIdentitasMandatory) {
                                    return "Tidak boleh kosong";
                                  } else {
                                    return null;
                                  }
                                },
                                style: TextStyle(color: Colors.black),
                                decoration: InputDecoration(
                                    labelText: 'Nomor Identitas',
                                    labelStyle: TextStyle(color: Colors.black),
                                    filled: formMInfoNasabahChangeNotif.disableJenisPenawaran || formMInfoNasabahChangeNotif.isDisablePACIAAOSCONA || formMInfoNasabahChangeNotif.enableDataDedup,
                                    fillColor: Colors.black12,
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(8)),
                                    enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: formMInfoNasabahChangeNotif.isNoIdentitasChanges ? Colors.purple : Colors.grey)),
                                    disabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: formMInfoNasabahChangeNotif.isNoIdentitasChanges ? Colors.purple : Colors.grey)),
                                    contentPadding: EdgeInsets.symmetric(horizontal: 10)),
                                keyboardType: formMInfoNasabahChangeNotif.identitasModel != null
                                    ? formMInfoNasabahChangeNotif.identitasModel.id != "03"
                                    ? TextInputType.number
                                    : TextInputType.text
                                    : TextInputType.number,
                                textCapitalization: TextCapitalization.characters,
                                inputFormatters: formMInfoNasabahChangeNotif.identitasModel != null
                                    ? formMInfoNasabahChangeNotif.identitasModel.id != "03"
                                    ? [WhitelistingTextInputFormatter.digitsOnly]
                                    : null
                                    : [WhitelistingTextInputFormatter.digitsOnly]
                            ),
                          ),
                        ),
                        Visibility(visible: formMInfoNasabahChangeNotif.isNoIdentitasVisible,
                            child: SizedBox(height: _size.height / 47)),
                        FocusScope(
                            node: FocusScopeNode(),
                            child: Visibility(visible: formMInfoNasabahChangeNotif.isTglIdentitasVisible,
                              child: TextFormField(
                                autovalidate: formMInfoNasabahChangeNotif.autoValidate,
                                validator: (e) {
                                  if (e.isEmpty && formMInfoNasabahChangeNotif.isTglIdentitasMandatory) {
                                    return "Tidak boleh kosong";
                                  } else {
                                    return null;
                                  }
                                },
                                onTap: () {
                                  FocusManager.instance.primaryFocus.unfocus();
                                  formMInfoNasabahChangeNotif.selectTglIdentitas(context);
                                },
                                enabled: formMInfoNasabahChangeNotif.isDisablePACIAAOSCONA ? false : true,
                                controller:
                                formMInfoNasabahChangeNotif.controllerTglIdentitas,
                                style: new TextStyle(color: Colors.black),
                                decoration: new InputDecoration(
                                    labelText: 'Tanggal Identitas',
                                    labelStyle: TextStyle(color: Colors.black),
                                    filled: formMInfoNasabahChangeNotif.isDisablePACIAAOSCONA,
                                    fillColor: Colors.black12,
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(8)),
                                    enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: formMInfoNasabahChangeNotif.isTglIdentitasChanges ? Colors.purple : Colors.grey)),
                                    disabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: formMInfoNasabahChangeNotif.isTglIdentitasChanges ? Colors.purple : Colors.grey)),
                                    contentPadding: EdgeInsets.symmetric(horizontal: 10)),
                                readOnly: true,
                              ),
                            )),
                        formMInfoNasabahChangeNotif.identitasModel != null
                            ? formMInfoNasabahChangeNotif.identitasModel.id != "01"
                            ? Visibility(visible: formMInfoNasabahChangeNotif.isTglIdentitasVisible, child: SizedBox(height: _size.height / 47))
                            : Visibility(visible: formMInfoNasabahChangeNotif.isidentitasModelVisible,
                            child: Row(
                              children: [
                                Text("KTP berlaku seumur hidup?", style: TextStyle(fontWeight: FontWeight.bold,
                                color: formMInfoNasabahChangeNotif.isKTPBerlakuSeumurHidupChanges ? Colors.purple : Colors.black)),
                                Checkbox(
                                    activeColor: primaryOrange,
                                    value:
                                    formMInfoNasabahChangeNotif.valueCheckBox,
                                    onChanged: (value) {
                                      formMInfoNasabahChangeNotif.valueCheckBox =
                                          value;
                                    }),
                                Text("Ya", style: TextStyle(fontWeight: FontWeight.bold)),
                              ],
                            ),
                        )
                            : Visibility(visible: formMInfoNasabahChangeNotif.isidentitasModelVisible, child: SizedBox(height: _size.height / 47)),
                        !formMInfoNasabahChangeNotif.valueCheckBox
                            ? FocusScope(
                            node: FocusScopeNode(),
                            child: Padding(
                              padding: EdgeInsets.only(bottom: _size.height / 47),
                              child: Visibility(visible: formMInfoNasabahChangeNotif.isIdentitasBerlakuSampaiVisible,
                                child: TextFormField(
                                  enabled: formMInfoNasabahChangeNotif.isDisablePACIAAOSCONA ? false : !formMInfoNasabahChangeNotif.valueCheckBox,
                                  autovalidate: formMInfoNasabahChangeNotif.autoValidate,
                                  validator: (e) {
                                    if (e.isEmpty && formMInfoNasabahChangeNotif.isIdentitasBerlakuSampaiMandatory) {
                                      return "Tidak boleh kosong";
                                    } else {
                                      return null;
                                    }
                                  },
                                  onTap: () {
                                    FocusManager.instance.primaryFocus.unfocus();
                                    formMInfoNasabahChangeNotif
                                        .selectTglIdentitasBerlakuSampai(context);
                                  },
                                  controller: formMInfoNasabahChangeNotif.controllerIdentitasBerlakuSampai,
                                  style: TextStyle(color: Colors.black),
                                  decoration: InputDecoration(
                                      labelText: 'Identitas Berlaku Sampai',
                                      labelStyle: TextStyle(color: Colors.black),
                                    filled: formMInfoNasabahChangeNotif.isDisablePACIAAOSCONA,
                                    fillColor: Colors.black12,
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(8)),
                                      enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: formMInfoNasabahChangeNotif.isIdentitasBerlakuSampaiChanges ? Colors.purple : Colors.grey)),
                                      disabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: formMInfoNasabahChangeNotif.isIdentitasBerlakuSampaiChanges ? Colors.purple : Colors.grey)),
                                      contentPadding: EdgeInsets.symmetric(horizontal: 10)
                                  ),
                                  readOnly: true,
                                ),
                              ),
                            ))
                            : Visibility(visible: formMInfoNasabahChangeNotif.isIdentitasBerlakuSampaiVisible, child: SizedBox(height: _size.height / 47)),
                        Visibility(visible: formMInfoNasabahChangeNotif.isNamaLengkapSesuaiIdentitasVisible,
                          child: IgnorePointer(
                            ignoring: formMInfoNasabahChangeNotif.isDisablePACIAAOSCONA || formMInfoNasabahChangeNotif.disableJenisPenawaran,
                            child: TextFormField(
                              controller: formMInfoNasabahChangeNotif.controllerNamaLengkapSesuaiIdentitas,
                              style: TextStyle(color: Colors.black),
                              decoration: InputDecoration(
                                  labelText: 'Nama Lengkap Sesuai Identitas',
                                  labelStyle: TextStyle(color: Colors.black),
                                  filled: formMInfoNasabahChangeNotif.isDisablePACIAAOSCONA || formMInfoNasabahChangeNotif.disableJenisPenawaran,
                                  fillColor: Colors.black12,
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8)),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: formMInfoNasabahChangeNotif.isNamaLengkapSesuaiIdentitasChanges ? Colors.purple : Colors.grey)),
                                  disabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: formMInfoNasabahChangeNotif.isNamaLengkapSesuaiIdentitasChanges ? Colors.purple : Colors.grey)),
                                  contentPadding: EdgeInsets.symmetric(horizontal: 10)),
                              keyboardType: TextInputType.text,
                              textCapitalization: TextCapitalization.characters,
                              autovalidate: formMInfoNasabahChangeNotif.autoValidate,
                              validator: (e) {
                                if (e.isEmpty && formMInfoNasabahChangeNotif.isNamaLengkapSesuaiIdentitasMandatory) {
                                  return "Tidak boleh kosong";
                                } else {
                                  return null;
                                }
                              },
                              // enabled: formMInfoNasabahChangeNotif.isDisablePACIAAOSCONA ? false : true,
                              inputFormatters: [
                                FilteringTextInputFormatter.allow(RegExp('[a-zA-Z  \`]')),
                              ],
                            ),
                          ),
                        ),
                        Visibility(visible: formMInfoNasabahChangeNotif.isNamaLengkapSesuaiIdentitasVisible,
                            child: SizedBox(height: _size.height / 47)),
                        Visibility(visible: formMInfoNasabahChangeNotif.isNamaLengkapVisible,
                          child: IgnorePointer(
                            ignoring: formMInfoNasabahChangeNotif.disableJenisPenawaran || formMInfoNasabahChangeNotif.isDisablePACIAAOSCONA || formMInfoNasabahChangeNotif.enableDataDedup,
                            child: TextFormField(
                              controller: formMInfoNasabahChangeNotif.controllerNamaLengkap,
                              style: TextStyle(color: Colors.black),
                              // enabled: formMInfoNasabahChangeNotif.enableDataDedup,
                              decoration: InputDecoration(
                                  labelText: 'Nama Lengkap',
                                  labelStyle: TextStyle(color: Colors.black),
                                  filled: formMInfoNasabahChangeNotif.disableJenisPenawaran || formMInfoNasabahChangeNotif.isDisablePACIAAOSCONA || formMInfoNasabahChangeNotif.enableDataDedup,
                                  fillColor: Colors.black12,
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8)),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: formMInfoNasabahChangeNotif.isNamaLengkapChanges ? Colors.purple : Colors.grey)),
                                  disabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: formMInfoNasabahChangeNotif.isNamaLengkapChanges ? Colors.purple : Colors.grey)),
                                  contentPadding: EdgeInsets.symmetric(horizontal: 10)),
                              keyboardType: TextInputType.text,
                              textCapitalization: TextCapitalization.characters,
                              autovalidate: formMInfoNasabahChangeNotif.autoValidate,
                              validator: (e) {
                                if (e.isEmpty && formMInfoNasabahChangeNotif.isNamaLengkapMandatory) {
                                  return "Tidak boleh kosong";
                                } else {
                                  return null;
                                }
                              },
                            ),
                          ),
                        ),
                        Visibility(visible: formMInfoNasabahChangeNotif.isNamaLengkapVisible,
                            child: SizedBox(height: _size.height / 47)),
                        Visibility(visible: formMInfoNasabahChangeNotif.isTglLahirVisible,
                          child: IgnorePointer(
                            ignoring: formMInfoNasabahChangeNotif.disableJenisPenawaran || formMInfoNasabahChangeNotif.isDisablePACIAAOSCONA || formMInfoNasabahChangeNotif.enableDataDedup,
                            child: TextFormField(
                              onTap: formMInfoNasabahChangeNotif.isEnableFieldBirthDate ? () {
                                FocusManager.instance.primaryFocus.unfocus();
                                formMInfoNasabahChangeNotif.selectBirthDate(context);
                              }:null,
                              controller: formMInfoNasabahChangeNotif.controllerTglLahir,
                              style: TextStyle(color: Colors.black),
                              decoration: InputDecoration(
                                  labelText: 'Tanggal Lahir',
                                  labelStyle: TextStyle(color: Colors.black),
                                  filled: formMInfoNasabahChangeNotif.disableJenisPenawaran || formMInfoNasabahChangeNotif.isDisablePACIAAOSCONA || formMInfoNasabahChangeNotif.enableDataDedup,
                                  fillColor: Colors.black12,
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8)),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: formMInfoNasabahChangeNotif.isTglLahirChanges ? Colors.purple : Colors.grey)),
                                  disabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: formMInfoNasabahChangeNotif.isTglLahirChanges ? Colors.purple : Colors.grey)),
                                  contentPadding: EdgeInsets.symmetric(horizontal: 10)),
                              autovalidate: formMInfoNasabahChangeNotif.autoValidate,
                              validator: (e) {
                                if (e.isEmpty && formMInfoNasabahChangeNotif.isTglLahirMandatory) {
                                  return "Tidak boleh kosong";
                                } else {
                                  return null;
                                }
                              },
                              // enabled: formMInfoNasabahChangeNotif.enableDataDedup,
                              readOnly: true,
                            ),
                          ),
                        ),
                        Visibility(visible: formMInfoNasabahChangeNotif.isTglLahirVisible,
                            child: SizedBox(height: _size.height / 47)),
                        Visibility(
                          visible: formMInfoNasabahChangeNotif.isTempatLahirSesuaiIdentitasVisible,
                          child: IgnorePointer(
                            ignoring: formMInfoNasabahChangeNotif.isDisablePACIAAOSCONA,
                            child: IgnorePointer(
                              ignoring: formMInfoNasabahChangeNotif.isDisablePACIAAOSCONA || formMInfoNasabahChangeNotif.enableDataDedup,
                              child: TextFormField(
                                // enabled: formMInfoNasabahChangeNotif.enableDataDedup,
                                controller: formMInfoNasabahChangeNotif.controllerTempatLahirSesuaiIdentitas,
                                style: TextStyle(color: Colors.black),
                                decoration: InputDecoration(
                                    labelText: 'Tempat Lahir Sesuai Identitas',
                                    labelStyle: TextStyle(color: Colors.black),
                                    filled: formMInfoNasabahChangeNotif.isDisablePACIAAOSCONA || formMInfoNasabahChangeNotif.enableDataDedup,
                                    fillColor: Colors.black12,
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(8)),
                                    enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: formMInfoNasabahChangeNotif.isTempatLahirSesuaiIdentitasChanges ? Colors.purple : Colors.grey)),
                                    disabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: formMInfoNasabahChangeNotif.isTempatLahirSesuaiIdentitasChanges ? Colors.purple : Colors.grey)),
                                    contentPadding: EdgeInsets.symmetric(horizontal: 10)),
                                keyboardType: TextInputType.text,
                                textCapitalization: TextCapitalization.characters,
                                autovalidate: formMInfoNasabahChangeNotif.autoValidate,
                                inputFormatters: [
                                  FilteringTextInputFormatter.allow(RegExp('[a-zA-Z ]')),
                                ],
                                validator: (e) {
                                  if (e.isEmpty && formMInfoNasabahChangeNotif.isTempatLahirSesuaiIdentitasMandatory) {
                                    return "Tidak boleh kosong";
                                  } else {
                                    return null;
                                  }
                                },
                              ),
                            ),
                          ),
                        ),
                        Visibility(visible: formMInfoNasabahChangeNotif.isTempatLahirSesuaiIdentitasVisible,
                            child: SizedBox(height: _size.height / 47)),
                        Visibility(
                          visible: formMInfoNasabahChangeNotif.isTempatLahirSesuaiIdentitasLOVVisible,
                          child: TextFormField(
                            readOnly: true,
                            onTap: (){
                              formMInfoNasabahChangeNotif.searchBirthPlace(context);
                            },
                            enabled: formMInfoNasabahChangeNotif.isDisablePACIAAOSCONA ? false : true,
                            controller: formMInfoNasabahChangeNotif.controllerTempatLahirSesuaiIdentitasLOV,
                            style: TextStyle(color: Colors.black),
                            decoration: InputDecoration(
                                labelText: 'Tempat Lahir Sesuai Identitas',
                                labelStyle: TextStyle(color: Colors.black),
                                filled: formMInfoNasabahChangeNotif.isDisablePACIAAOSCONA,
                                fillColor: Colors.black12,
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(8)),
                                enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: formMInfoNasabahChangeNotif.isTempatLahirSesuaiIdentitasLOVChanges
                                    ? Colors.purple : Colors.grey))),
                            keyboardType: TextInputType.text,
                            textCapitalization: TextCapitalization.characters,
                            autovalidate: formMInfoNasabahChangeNotif.autoValidate,
                            validator: (e) {
                              if (e.isEmpty && formMInfoNasabahChangeNotif.isTempatLahirSesuaiIdentitasLOVMandatory) {
                                return "Tidak boleh kosong";
                              } else {
                                return null;
                              }
                            },
                          ),
                        ),
                        Visibility(visible: formMInfoNasabahChangeNotif.isTempatLahirSesuaiIdentitasLOVVisible,
                            child: SizedBox(height: _size.height / 47)),
                        Visibility(visible: formMInfoNasabahChangeNotif.isGenderVisible,
                          child: Container(
                            child: Column(crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Jenis Kelamin",
                                  style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.w700,
                                      letterSpacing: 0.15,
                                  color: formMInfoNasabahChangeNotif.isGenderChanges ? Colors.purple : Colors.black),
                                ),
                                Row(
                                  children: [
                                    Row(
                                      children: [
                                        Radio(
                                            activeColor: primaryOrange,
                                            value: "01",
                                            groupValue: formMInfoNasabahChangeNotif.radioValueGender,
                                            onChanged: (value) {
                                              formMInfoNasabahChangeNotif.isDisablePACIAAOSCONA
                                              ? null
                                              : formMInfoNasabahChangeNotif.radioValueGender = value;
                                            }),
                                        Text("Laki Laki")
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        Radio(
                                            activeColor: primaryOrange,
                                            value: "02",
                                            groupValue: formMInfoNasabahChangeNotif.radioValueGender,
                                            onChanged: (value) {
                                              formMInfoNasabahChangeNotif.isDisablePACIAAOSCONA
                                              ? null
                                              : formMInfoNasabahChangeNotif.radioValueGender = value;
                                            }),
                                        Text("Perempuan")
                                      ],
                                    ),
                                  ],
                                ),
                                formMInfoNasabahChangeNotif.autoValidate ?
                                formMInfoNasabahChangeNotif.isGenderMandatory ?
                                Container(
                                  margin: EdgeInsets.only(
                                      left: MediaQuery.of(context).size.width / 37),
                                  child:
                                  formMInfoNasabahChangeNotif.isGenderMandatoryShowError ? Text("Tidak boleh kosong", style: TextStyle(color: Colors.red, fontSize: 12)) : null,
                                ) : SizedBox()
                                    : SizedBox(),
                              ],
                            ),
                          ),
                        ),
                        Visibility(visible: formMInfoNasabahChangeNotif.isGenderVisible,
                            child: SizedBox(height: _size.height / 47)),
                        Visibility(visible: formMInfoNasabahChangeNotif.isReligionVisible,
                          child: DropdownButtonFormField<ReligionModel>(
                            value: formMInfoNasabahChangeNotif.religionSelected,
                            autovalidate: formMInfoNasabahChangeNotif.autoValidate,
                            validator: (e) {
                              if (e == null && formMInfoNasabahChangeNotif.isReligionMandatory) {
                                return "Tidak boleh kosong";
                              } else {
                                return null;
                              }
                            },
                            decoration: InputDecoration(
                              labelText: "Agama",
                              border: OutlineInputBorder(),
                                enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: formMInfoNasabahChangeNotif.isReligionChanges
                                    ? Colors.purple : Colors.grey)),
                              contentPadding: EdgeInsets.symmetric(horizontal: 10),
                            ),
                            items: formMInfoNasabahChangeNotif.itemsReligion.map((data) {
                              return DropdownMenuItem<ReligionModel>(
                                value: data,
                                child: Text(
                                  data.text,
                                  overflow: TextOverflow.ellipsis,
                                ),
                              );
                            }).toList(),
                            onChanged: (newVal) {
                              formMInfoNasabahChangeNotif.religionSelected = newVal;
                            },
                            onTap: () {
                              FocusManager.instance.primaryFocus.unfocus();
                            },
                          ),
                        ),
                        Visibility(visible: formMInfoNasabahChangeNotif.isReligionVisible,
                            child: SizedBox(height: _size.height / 47)),
                        Visibility(visible: formMInfoNasabahChangeNotif.isEducationSelectedVisible,
                          child: IgnorePointer(
                            ignoring: formMInfoNasabahChangeNotif.isDisablePACIAAOSCONA,
                            child: DropdownButtonFormField<EducationModel>(
                              value: formMInfoNasabahChangeNotif.educationSelected,
                              autovalidate: formMInfoNasabahChangeNotif.autoValidate,
                              validator: (e) {
                                if (e == null && formMInfoNasabahChangeNotif.isEducationSelectedMandatory) {
                                  return "Tidak boleh kosong";
                                } else {
                                  return null;
                                }
                              },
                              decoration: InputDecoration(
                                labelText: "Pendidikan",
                                filled: formMInfoNasabahChangeNotif.isDisablePACIAAOSCONA,
                                fillColor: Colors.black12,
                                border: OutlineInputBorder(),
                                  enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: formMInfoNasabahChangeNotif.isEducationSelectedChanges
                                      ? Colors.purple : Colors.grey)),
                                contentPadding: EdgeInsets.symmetric(horizontal: 10),
                              ),
                              items: formMInfoNasabahChangeNotif.listEducation.map((data) {
                                return DropdownMenuItem<EducationModel>(
                                  value: data,
                                  child: Text(
                                    data.text,
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                );
                              }).toList(),
                              onChanged: (newVal) {
                                formMInfoNasabahChangeNotif.educationSelected = newVal;
                              },
                              onTap: () {
                                FocusManager.instance.primaryFocus.unfocus();
                              },
                            ),
                          ),
                        ),
                        Visibility(visible: formMInfoNasabahChangeNotif.isEducationSelectedVisible,
                            child: SizedBox(height: _size.height / 47)),
                        Visibility(visible: formMInfoNasabahChangeNotif.isJumlahTanggunganVisible,
                          child: TextFormField(
                            controller:
                            formMInfoNasabahChangeNotif.controllerJumlahTanggungan,
                            style: TextStyle(color: Colors.black),
                            decoration: InputDecoration(
                                labelText: 'Jumlah Tanggungan',
                                labelStyle: TextStyle(color: Colors.black),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(8)),
                                enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: formMInfoNasabahChangeNotif.isJumlahTanggunganChanges
                                    ? Colors.purple : Colors.grey))),
                            keyboardType: TextInputType.number,
                            inputFormatters: [
                              WhitelistingTextInputFormatter.digitsOnly,
                              // LengthLimitingTextInputFormatter(10),
                            ],
                            autovalidate: formMInfoNasabahChangeNotif.autoValidate,
                            validator: (e) {
                              if (e.isEmpty && formMInfoNasabahChangeNotif.isJumlahTanggunganMandatory) {
                                return "Tidak boleh kosong";
                              } else {
                                return null;
                              }
                            },
                          ),
                        ),
                        Visibility(visible: formMInfoNasabahChangeNotif.isJumlahTanggunganVisible,
                            child: SizedBox(height: _size.height / 47)),
                        Visibility(visible: formMInfoNasabahChangeNotif.isMaritalStatusSelectedVisible,
                          child: IgnorePointer(
                            ignoring: formMInfoNasabahChangeNotif.isDisablePACIAAOSCONA,
                            child: DropdownButtonFormField<MaritalStatusModel>(
                              value: formMInfoNasabahChangeNotif.maritalStatusSelected,
                              autovalidate: formMInfoNasabahChangeNotif.autoValidate,
                              validator: (e) {
                                if (e == null && formMInfoNasabahChangeNotif.isMaritalStatusSelectedMandatory) {
                                  return "Tidak boleh kosong";
                                } else {
                                  return null;
                                }
                              },
                              decoration: InputDecoration(
                                labelText: "Status Pernikahan",
                                filled: formMInfoNasabahChangeNotif.isDisablePACIAAOSCONA,
                                fillColor: Colors.black12,
                                border: OutlineInputBorder(),
                                  enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: formMInfoNasabahChangeNotif.isMaritalStatusSelectedChanges
                                      ? Colors.purple : Colors.grey)),
                                contentPadding: EdgeInsets.symmetric(horizontal: 10),
                              ),
                              items:
                              formMInfoNasabahChangeNotif.listMaritalStatus.map((data) {
                                return DropdownMenuItem<MaritalStatusModel>(
                                  value: data,
                                  child: Text(
                                    data.text,
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                );
                              }).toList(),
                              onChanged: (newVal) {
                                formMInfoNasabahChangeNotif.maritalStatusSelected = newVal;
                              },
                              onTap: () {
                                FocusManager.instance.primaryFocus.unfocus();
                              },
                            ),
                          ),
                        ),
                        Visibility(visible: formMInfoNasabahChangeNotif.isMaritalStatusSelectedVisible,
                            child: SizedBox(height: _size.height / 47)),
                        Visibility(visible: formMInfoNasabahChangeNotif.isEmailVisible,
                          child: TextFormField(
                            controller: formMInfoNasabahChangeNotif.controllerEmail,
                            style: TextStyle(color: Colors.black),
                            decoration: InputDecoration(
                                labelText: 'Email',
                                labelStyle: TextStyle(color: Colors.black),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(8)),
                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: formMInfoNasabahChangeNotif.isEmailChanges ? Colors.purple : Colors.grey)),
                                disabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: formMInfoNasabahChangeNotif.isEmailChanges ? Colors.purple : Colors.grey)),
                                contentPadding: EdgeInsets.symmetric(horizontal: 10)),
                            keyboardType: TextInputType.text,
                            textCapitalization: TextCapitalization.characters,
                            autovalidate: formMInfoNasabahChangeNotif.autoValidate,
                            validator: formMInfoNasabahChangeNotif.validateEmail,
                          ),
                        ),
                        Visibility(visible: formMInfoNasabahChangeNotif.isEmailVisible,
                            child: SizedBox(height: _size.height / 47)),
                        Row(
                          children: [
                            Visibility(visible: formMInfoNasabahChangeNotif.isNoHpVisible,
                              child: Expanded(
                                flex: 7,
                                child: IgnorePointer(
                                  ignoring: formMInfoNasabahChangeNotif.disableJenisPenawaran ? true : false,
                                  child: TextFormField(
                                    controller: formMInfoNasabahChangeNotif.controllerNoHp,
                                    style: TextStyle(color: Colors.black),
                                    decoration: InputDecoration(
                                        labelText: 'No Handphone 1',
                                        labelStyle: TextStyle(color: Colors.black),
                                        filled: formMInfoNasabahChangeNotif.disableJenisPenawaran ? true : false,
                                        fillColor: Colors.black12,
                                        prefixText: "08",
                                        border: OutlineInputBorder(
                                            borderRadius: BorderRadius.circular(8)),
                                        enabledBorder: OutlineInputBorder(
                                            borderSide: BorderSide(color: formMInfoNasabahChangeNotif.isNoHpChanges ? Colors.purple : Colors.grey)),
                                        disabledBorder: OutlineInputBorder(
                                            borderSide: BorderSide(color: formMInfoNasabahChangeNotif.isNoHpChanges ? Colors.purple : Colors.grey)),
                                        contentPadding: EdgeInsets.symmetric(horizontal: 10)),
                                    keyboardType: TextInputType.number,
                                    autovalidate: formMInfoNasabahChangeNotif.autoValidate,
                                    validator: (e) {
                                      if (e.isEmpty && formMInfoNasabahChangeNotif.isNoHpMandatory) {
                                        return "Tidak boleh kosong";
                                      } else {
                                        return null;
                                      }
                                    },
                                    onChanged: (value){
                                      formMInfoNasabahChangeNotif.checkValidNoHP(value);
                                    },
                                    inputFormatters: [
                                      LengthLimitingTextInputFormatter(14),
                                      FilteringTextInputFormatter.allow(RegExp('[0-9]')),
                                      // WhitelistingTextInputFormatter(RegExp(r"^[1-9][0-9]*$"))
                                    ],
                                  ),
                                ),
                              ),
                            ), Visibility(visible: formMInfoNasabahChangeNotif.isNoHp1WAVisible,
                              child: Expanded(
                                  flex: 3,
                                  child: Row(
                                    children: [
                                      Checkbox(
                                          activeColor: primaryOrange,
                                          value: formMInfoNasabahChangeNotif.isNoHp1WA,
                                          onChanged: (value) {
                                            formMInfoNasabahChangeNotif.isNoHp1WA = value;
                                          }),
                                      Text(
                                        "WA",
                                        style: TextStyle(
                                            fontSize: 16, fontWeight: FontWeight.bold,
                                        color: formMInfoNasabahChangeNotif.isNoHp1WAChanges ? Colors.purple : Colors.black),
                                      )
                                    ],
                                  )),
                            )
                          ],
                        ),
                        Visibility(visible: formMInfoNasabahChangeNotif.isNoHpVisible,
                            child: SizedBox(height: _size.height / 47)),
                        Row(
                          children: [
                            Expanded(
                              flex: 7,
                              child: Visibility(visible: formMInfoNasabahChangeNotif.isNoHp2Visible,
                                child: TextFormField(
                                  controller: formMInfoNasabahChangeNotif.controllerNoHp2,
                                  style: TextStyle(color: Colors.black),
                                  decoration: InputDecoration(
                                      labelText: 'No Handphone 2',
                                      labelStyle: TextStyle(color: Colors.black),
                                      prefixText: "08",
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(8)),
                                      enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: formMInfoNasabahChangeNotif.isNoHp2Changes ? Colors.purple : Colors.grey)),
                                      disabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: formMInfoNasabahChangeNotif.isNoHp2Changes ? Colors.purple : Colors.grey)),
                                      contentPadding: EdgeInsets.symmetric(horizontal: 10)),
                                  keyboardType: TextInputType.number,
                                  inputFormatters: [
                                    WhitelistingTextInputFormatter(RegExp(r"^[1-9][0-9]*$"))
                                  ],
                                  autovalidate: formMInfoNasabahChangeNotif.autoValidate,
                                  validator: (e) {
                                    if (e.isEmpty && formMInfoNasabahChangeNotif.isNoHp2Mandatory) {
                                      return "Tidak boleh kosong";
                                    } else {
                                      return null;
                                    }
                                  },
                                ),
                              ),
                            ),
                            Visibility(visible: formMInfoNasabahChangeNotif.isNoHp2WAVisible,
                              child: Expanded(
                                  flex: 3,
                                  child: Row(
                                    children: [
                                      Checkbox(
                                          activeColor: primaryOrange,
                                          value: formMInfoNasabahChangeNotif.isNoHp2WA,
                                          onChanged: (value) {
                                            formMInfoNasabahChangeNotif.isNoHp2WA = value;
                                          }),
                                      Text("WA",
                                          style: TextStyle(
                                              fontSize: 16, fontWeight: FontWeight.bold,
                                          color: formMInfoNasabahChangeNotif.isNoHp2WAChanges ? Colors.purple : Colors.black)),
                                    ],
                                  )),
                            )
                          ],
                        ),
                        Visibility(visible: formMInfoNasabahChangeNotif.isNoHp2Visible,
                            child: SizedBox(height: _size.height / 47)),
                        Row(
                          children: [
                            Expanded(
                              flex: 7,
                              child: Visibility(visible: formMInfoNasabahChangeNotif.isNoHp3Visible,
                                child: TextFormField(
                                  controller: formMInfoNasabahChangeNotif.controllerNoHp3,
                                  style: TextStyle(color: Colors.black),
                                  decoration: InputDecoration(
                                      labelText: 'No Handphone 3',
                                      labelStyle: TextStyle(color: Colors.black),
                                      prefixText: "08",
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(8)),
                                      enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: formMInfoNasabahChangeNotif.isNoHp3Changes ? Colors.purple : Colors.grey)),
                                      disabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: formMInfoNasabahChangeNotif.isNoHp3Changes ? Colors.purple : Colors.grey)),
                                      contentPadding: EdgeInsets.symmetric(horizontal: 10)),
                                  keyboardType: TextInputType.number,
                                  inputFormatters: [
                                    WhitelistingTextInputFormatter(RegExp(r"^[1-9][0-9]*$"))
                                  ],
                                  autovalidate: formMInfoNasabahChangeNotif.autoValidate,
                                  validator: (e) {
                                    if (e.isEmpty && formMInfoNasabahChangeNotif.isNoHp3Mandatory) {
                                      return "Tidak boleh kosong";
                                    } else {
                                      return null;
                                    }
                                  },
                                ),
                              ),
                            ),
                            Visibility(visible: formMInfoNasabahChangeNotif.isNoHp3WAVisible,
                              child: Expanded(
                                  flex: 3,
                                  child: Row(
                                    children: [
                                      Checkbox(
                                          activeColor: primaryOrange,
                                          value: formMInfoNasabahChangeNotif.isNoHp3WA,
                                          onChanged: (value) {
                                            formMInfoNasabahChangeNotif.isNoHp3WA = value;
                                          }),
                                      Text("WA",
                                          style: TextStyle(
                                              fontSize: 16, fontWeight: FontWeight.bold,
                                          color: formMInfoNasabahChangeNotif.isNoHp3WAChanges ? Colors.purple : Colors.black)),
                                    ],
                                  )),
                            )
                          ],
                        ),
                        Visibility(visible: formMInfoNasabahChangeNotif.isNoHp3Visible,
                            child: SizedBox(height: _size.height / 47)),
                        Visibility(visible: formMInfoNasabahChangeNotif.isHaveNPWPVisible,
                          child: Column(crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Punya NPWP ?",
                                style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w700,
                                    letterSpacing: 0.15,
                                color: formMInfoNasabahChangeNotif.isHaveNPWPChanges ? Colors.purple : Colors.black),
                              ),
                              Row(
                                children: [
                                  Row(
                                    children: [
                                      Radio(
                                          activeColor: primaryOrange,
                                          value: 1,
                                          groupValue: formMInfoNasabahChangeNotif.radioValueIsHaveNPWP,
                                          onChanged: (value) {
                                            formMInfoNasabahChangeNotif.radioValueIsHaveNPWP = value;
                                          }),
                                      Text("Ya")
                                    ],
                                  ),
                                  Row(
                                    children: [
                                      Radio(
                                          activeColor: primaryOrange,
                                          value: 0,
                                          groupValue:
                                          formMInfoNasabahChangeNotif.radioValueIsHaveNPWP,
                                          onChanged: (value) {
                                            formMInfoNasabahChangeNotif.clearDataNPWP();
                                            formMInfoNasabahChangeNotif.radioValueIsHaveNPWP = value;
                                          }),
                                      Text("Tidak")
                                    ],
                                  ),
                                ],
                              ),
                              formMInfoNasabahChangeNotif.isHaveNPWPMandatory &&  formMInfoNasabahChangeNotif.radioValueIsHaveNPWP == 0
                                  ?
                              Container(
                                margin: EdgeInsets.only(
                                    left: MediaQuery.of(context).size.width / 37),
                                child:
                                formMInfoNasabahChangeNotif.isHaveNPWPMandatoryShowError ? Text("Harus pilih Ya", style: TextStyle(color: Colors.red, fontSize: 12)) : null,
                              )
                                  : SizedBox(),
                            ],
                          ),
                        ),
                        // SizedBox(height: _size.height / 47),
                        Visibility(visible: formMInfoNasabahChangeNotif.isHaveNPWPVisible,
                            child: SizedBox(height: _size.height / 47)),
                        formMInfoNasabahChangeNotif.radioValueIsHaveNPWP == 1
                            ? Column(
                          children: [
                            Visibility(visible: formMInfoNasabahChangeNotif.isNoNPWPVisible,
                              child: TextFormField(
                                inputFormatters: [
                                  WhitelistingTextInputFormatter.digitsOnly,
                                  LengthLimitingTextInputFormatter(15),
                                ],
                                controller:
                                formMInfoNasabahChangeNotif.controllerNoNPWP,
                                style: TextStyle(color: Colors.black),
                                decoration: InputDecoration(
                                    labelText: 'Nomor NPWP',
                                    labelStyle: TextStyle(color: Colors.black),
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(8)),
                                    enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: formMInfoNasabahChangeNotif.isNoNPWPChanges ? Colors.purple : Colors.grey)),
                                    disabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: formMInfoNasabahChangeNotif.isNoNPWPChanges ? Colors.purple : Colors.grey)),
                                    contentPadding: EdgeInsets.symmetric(horizontal: 10)),
                                keyboardType: TextInputType.number,
                                autovalidate: formMInfoNasabahChangeNotif.radioValueIsHaveNPWP == 1 ? formMInfoNasabahChangeNotif.autoValidate : null,
                                validator: (e) {
                                  if (e.isEmpty && formMInfoNasabahChangeNotif.isNoNPWPMandatory) {
                                    return "Tidak boleh kosong";
                                  } else {
                                    return null;
                                  }
                                },
                                onTap: (){
                                  formMInfoNasabahChangeNotif.validateNameOrNPWP(context, "NPWP");
                                },
                                onFieldSubmitted: (val){
                                  formMInfoNasabahChangeNotif.validateNameOrNPWP(context, "NPWP");
                                },
                              ),
                            ),
                            Visibility(visible: formMInfoNasabahChangeNotif.isNoNPWPVisible,
                                child: SizedBox(height: _size.height / 47)),
                            Visibility(visible: formMInfoNasabahChangeNotif.isNamaSesuaiNPWPVisible,
                              child: TextFormField(
                                controller: formMInfoNasabahChangeNotif.controllerNamaSesuaiNPWP,
                                style: TextStyle(color: Colors.black),
                                decoration: InputDecoration(
                                    labelText: 'Nama Sesuai NPWP',
                                    labelStyle: TextStyle(color: Colors.black),
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(8)),
                                    enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: formMInfoNasabahChangeNotif.isNamaSesuaiNPWPChanges ? Colors.purple : Colors.grey)),
                                    disabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: formMInfoNasabahChangeNotif.isNamaSesuaiNPWPChanges ? Colors.purple : Colors.grey)),
                                    contentPadding: EdgeInsets.symmetric(horizontal: 10)),
                                keyboardType: TextInputType.text,
                                textCapitalization: TextCapitalization.characters,
                                autovalidate:
                                formMInfoNasabahChangeNotif.autoValidate,
                                validator: (e) {
                                  if (e.isEmpty && formMInfoNasabahChangeNotif.isNamaSesuaiNPWPMandatory) {
                                    return "Tidak boleh kosong";
                                  } else {
                                    return null;
                                  }
                                },
                                onTap: (){
                                  formMInfoNasabahChangeNotif.validateNameOrNPWP(context, "NPWP");
                                },
                              ),
                            ),
                            Visibility(visible: formMInfoNasabahChangeNotif.isNamaSesuaiNPWPVisible,
                                child: SizedBox(height: _size.height / 47)),
                            Visibility(visible: formMInfoNasabahChangeNotif.isJenisNPWPVisible,
                              child: DropdownButtonFormField<JenisNPWPModel>(
                                value: formMInfoNasabahChangeNotif.jenisNPWPSelected,
                                autovalidate:
                                formMInfoNasabahChangeNotif.autoValidate,
                                validator: (e) {
                                  if (e == null && formMInfoNasabahChangeNotif.isJenisNPWPMandatory) {
                                    return "Tidak boleh kosong";
                                  } else {
                                    return null;
                                  }
                                },
                                decoration: InputDecoration(
                                  labelText: "Jenis NPWP",
                                  border: OutlineInputBorder(),
                                    enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: formMInfoNasabahChangeNotif.isJenisNPWPChanges ? Colors.purple : Colors.grey)),
                                    disabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: formMInfoNasabahChangeNotif.isJenisNPWPChanges ? Colors.purple : Colors.grey)),
                                    contentPadding: EdgeInsets.symmetric(horizontal: 10)
                                ),
                                items: formMInfoNasabahChangeNotif.listJenisNPWP.map((data) {
                                  return DropdownMenuItem<JenisNPWPModel>(
                                    value: data,
                                    child: Text(
                                      data.text,
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                  );
                                }).toList(),
                                onChanged: (newVal) {
                                  formMInfoNasabahChangeNotif.jenisNPWPSelected = newVal;
                                },
                                onTap: () {
                                  FocusManager.instance.primaryFocus.unfocus();
                                },
                              ),
                            ),
                            Visibility(visible: formMInfoNasabahChangeNotif.isJenisNPWPVisible,
                                child: SizedBox(height: _size.height / 47)),
                            Visibility(visible: formMInfoNasabahChangeNotif.isTandaPKPVisible,
                              child: DropdownButtonFormField<TandaPKPModel>(
                                value: formMInfoNasabahChangeNotif.tandaPKPSelected,
                                autovalidate:
                                formMInfoNasabahChangeNotif.autoValidate,
                                validator: (e) {
                                  if (e == null && formMInfoNasabahChangeNotif.isTandaPKPMandatory) {
                                    return "Tidak boleh kosong";
                                  } else {
                                    return null;
                                  }
                                },
                                decoration: InputDecoration(
                                  labelText: "Tanda PKP",
                                  border: OutlineInputBorder(),
                                    enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: formMInfoNasabahChangeNotif.isTandaPKPChanges ? Colors.purple : Colors.grey)),
                                    disabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: formMInfoNasabahChangeNotif.isTandaPKPChanges ? Colors.purple : Colors.grey)),
                                    contentPadding: EdgeInsets.symmetric(horizontal: 10)
                                ),
                                items: formMInfoNasabahChangeNotif.listTandaPKP.map((data) {
                                  return DropdownMenuItem<TandaPKPModel>(
                                    value: data,
                                    child: Text(
                                      data.text,
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                  );
                                }).toList(),
                                onChanged: (newVal) {
                                  formMInfoNasabahChangeNotif.tandaPKPSelected =
                                      newVal;
                                },
                                onTap: () {
                                  FocusManager.instance.primaryFocus.unfocus();
                                },
                              ),
                            ),
                            Visibility(visible: formMInfoNasabahChangeNotif.isTandaPKPVisible,
                                child: SizedBox(height: _size.height / 47)),
                            Visibility(visible: formMInfoNasabahChangeNotif.isAlamatSesuaiNPWPVisible,
                              child: TextFormField(
                                controller: formMInfoNasabahChangeNotif.controllerAlamatSesuaiNPWP,
                                style: TextStyle(color: Colors.black),
                                decoration: InputDecoration(
                                    labelText: 'Alamat Sesuai NPWP',
                                    labelStyle: TextStyle(color: Colors.black),
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(8)),
                                    enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: formMInfoNasabahChangeNotif.isAlamatSesuaiNPWPChanges ? Colors.purple : Colors.grey)),
                                    disabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: formMInfoNasabahChangeNotif.isAlamatSesuaiNPWPChanges ? Colors.purple : Colors.grey)),
                                    contentPadding: EdgeInsets.symmetric(horizontal: 10)),
                                keyboardType: TextInputType.text,
                                textCapitalization: TextCapitalization.characters,
                                autovalidate: formMInfoNasabahChangeNotif.radioValueIsHaveNPWP == 1 ? formMInfoNasabahChangeNotif.autoValidate : null,
                                validator: true ? (e) {
                                  if (e.isEmpty && formMInfoNasabahChangeNotif.isAlamatSesuaiNPWPMandatory) {
                                    return "Tidak boleh kosong";
                                  } else {
                                    return null;
                                  }
                                } : null,
                                onTap: (){
                                  formMInfoNasabahChangeNotif.validateNameOrNPWP(context, "NPWP");
                                },
                              ),
                            ),
                          ],
                        )
                            : SizedBox()
                      ],
                    ),
                  );
                },
              );
            },
          ),
          bottomNavigationBar:
          BottomAppBar(
            elevation: 0.0,
            child: Padding(
                padding: EdgeInsets.all(8.0),
                child: Consumer<FormMInfoNasabahChangeNotif>(
                  builder: (context, formMInfoNasabahChangeNotif, _) {
                      return formMInfoNasabahChangeNotif.loadData
                        ?
                      Center(child: CircularProgressIndicator())
                        :
                      RaisedButton(
                        shape: RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(8.0)),
                        color: myPrimaryColor,
                        onPressed: () {
                          formMInfoNasabahChangeNotif.check(context);
                        },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text("DONE",
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 14,
                                    fontWeight: FontWeight.w500,
                                    letterSpacing: 1.25))
                          ],
                        ));
                  },
                )),
          ),
        )
    );
  }
}
