import 'package:ad1ms2_dev/screens/app/info_credit_income.dart';
import 'package:ad1ms2_dev/screens/app/info_credit_structure.dart';
import 'package:ad1ms2_dev/screens/app/info_credit_structure_type_installment.dart';
import 'package:ad1ms2_dev/screens/app/info_wmp.dart';
import 'package:ad1ms2_dev/screens/app/list_additional_insurance.dart';
import 'package:ad1ms2_dev/screens/app/list_major_insurance.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_additional_insurance_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_credit_income_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_credit_structure_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_credit_structure_type_installment_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_credit_subsidy_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_major_insurance_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_company_parent_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_individu_change_notifier/form_m_parent_individu_change_notifier.dart';
import 'package:ad1ms2_dev/shared/info_wmp_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../main.dart';

class MenuDetailLoan extends StatefulWidget {
  @override
  MenuDetailLoanState createState() => MenuDetailLoanState();
}

class MenuDetailLoanState extends State<MenuDetailLoan> {
    @override
    void initState() {
      super.initState();
      Provider.of<InfoMajorInsuranceChangeNotifier>(context, listen: false).checkMandatory(context);
    }

    @override
    Widget build(BuildContext context) {
        var _providerCreditStructure = Provider.of<InfoCreditStructureChangeNotifier>(context, listen: true);
        // var _providerCreditStructureTypeInstallment = Provider.of<InfoCreditStructureTypeInstallmentChangeNotifier>(context, listen: true);
        var _providerMajorInsurance = Provider.of<InfoMajorInsuranceChangeNotifier>(context, listen: true);
        var _providerAdditionalInsurance = Provider.of<InfoAdditionalInsuranceChangeNotifier>(context, listen: true);
        var _providerWmp = Provider.of<InfoWMPChangeNotifier>(context, listen: true);
        var _providerCreditIncome = Provider.of<InfoCreditIncomeChangeNotifier>(context, listen: true);

        return Padding(
            padding: EdgeInsets.symmetric(
                horizontal: MediaQuery.of(context).size.width/37,
                vertical: MediaQuery.of(context).size.height/57,
            ),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                    InkWell(
                        onTap: () {
                            Navigator.push(context, MaterialPageRoute(
                                    builder: (context) => InfoCreditStructure()
                                )
                            );
                        },
                        child: Card(
                            elevation: 3.3,
                            child: Padding(
                                padding: const EdgeInsets.all(16.0),
                                child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                        Text("Informasi Struktur Kredit"),
                                        _providerCreditStructure.flag
                                            ? Icon(Icons.check, color: primaryOrange)
                                            : SizedBox()
                                    ],
                                ),
                            ),
                        ),
                    ),
                    _providerCreditStructure.autoValidate
                    ? Container(
                        margin: EdgeInsets.only(left: MediaQuery.of(context).size.width / 37),
                        child: Text(_providerCreditStructure.messageError == '' ? "Data belum lengkap" : _providerCreditStructure.messageError,style: TextStyle(color: Colors.red, fontSize: 12)),
                    )
                    : SizedBox(height: MediaQuery.of(context).size.height / 87),
                    Visibility(visible: _providerCreditStructure.isVisible,
                        child: SizedBox(height: MediaQuery.of(context).size.height / 87)
                    ),
                    Visibility(
                      visible: _providerCreditStructure.isVisible,
                      child: InkWell(
                          onTap: () {
                              Navigator.push(context, MaterialPageRoute(
                                      builder: (context) => InfoCreditStructureTypeInstallment())
                              );
                          },
                          child: Card(
                              elevation: 3.3,
                              child: Padding(
                                  padding: const EdgeInsets.all(16.0),
                                  child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                          Text("Info Struktur Kredit Jenis Angsuran"),
                                          _providerCreditStructure.flag
                                              ? Icon(Icons.check, color: primaryOrange)
                                              : SizedBox()
                                      ],
                                  ),
                              ),
                          ),
                      ),
                    ),
                    Visibility(
                      visible: _providerCreditStructure.isVisible,
                      child: _providerCreditStructure.autoValidate
                        ? Container(
                          margin: EdgeInsets.only(left: MediaQuery.of(context).size.width / 37),
                          child: Text("Data belum lengkap",style: TextStyle(color: Colors.red, fontSize: 12)),
                        )
                        : SizedBox(height: MediaQuery.of(context).size.height / 87),
                    ),
                    Visibility(visible: _providerCreditStructure.isVisible,
                        child: SizedBox(height: MediaQuery.of(context).size.height / 87)
                    ),
                    InkWell(
                        onTap: () {
                            Navigator.push(context, MaterialPageRoute(
                                    builder: (context) => ListMajorInsurance())
                            );
                        },
                        child: Card(
                            elevation: 3.3,
                            child: Padding(
                                padding: const EdgeInsets.all(16.0),
                                child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                        Text("Informasi Asuransi Utama"),
                                        _providerMajorInsurance.listFormMajorInsurance.length != 0 && _providerMajorInsurance.isCheckData
                                            ? Icon(Icons.check, color: primaryOrange)
                                            : SizedBox()
                                    ],
                                ),
                            ),
                        ),
                    ),
                    _providerMajorInsurance.flag
                    ?
                    Container(
                        margin: EdgeInsets.only(left: MediaQuery.of(context).size.width / 37),
                        child: Text(_providerMajorInsurance.messageError == '' ? "Data belum lengkap" : _providerMajorInsurance.messageError,
                            style: TextStyle(color: Colors.red, fontSize: 12)
                        ),
                    )
                    : SizedBox(height: MediaQuery.of(context).size.height / 87),
                    InkWell(
                        onTap: () {
                            Navigator.push(context, MaterialPageRoute(
                                    builder: (context) => ListAdditionalInsurance())
                            );
                        },
                        child: Card(
                            elevation: 3.3,
                            child: Padding(
                                padding: const EdgeInsets.all(16.0),
                                child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                        Text("Informasi Asuransi Tambahan"),
                                        _providerAdditionalInsurance.listFormAdditionalInsurance.length != 0 && _providerAdditionalInsurance.isCheckData
                                            ? Icon(Icons.check, color: primaryOrange)
                                            : SizedBox()
                                    ],
                                ),
                            ),
                        ),
                    ),
                    _providerAdditionalInsurance.flag
                        ?
                    Container(
                        margin: EdgeInsets.only(left: MediaQuery.of(context).size.width / 37),
                        child: Text("Asuransi tambahan wajib diisi", style: TextStyle(color: Colors.red, fontSize: 12)),
                    )
                        :
                    SizedBox(),
                    SizedBox(height: MediaQuery.of(context).size.height / 87),
                    InkWell(
                        onTap: () {
                            Navigator.push(context, MaterialPageRoute(
                                    builder: (context) => InfoWMP()
                                )
                            );
                        },
                        child: Card(
                            elevation: 3.3,
                            child: Padding(
                                padding: const EdgeInsets.all(16.0),
                                child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                        Text("Informasi WMP"),
                                        _providerWmp.listWMP.isNotEmpty && _providerWmp.isCheckData
                                            ? Icon(Icons.check, color: primaryOrange)
                                            : SizedBox()
                                    ],
                                ),
                            ),
                        ),
                    ),
                    _providerWmp.flag
                        ? Container(
                        margin: EdgeInsets.only(left: MediaQuery.of(context).size.width / 37),
                        child: Text("Data belum lengkap",style: TextStyle(color: Colors.red, fontSize: 12)),
                    )
                        : SizedBox(height: MediaQuery.of(context).size.height / 87),
                    InkWell(
                        onTap: () {
                            Navigator.push(context, MaterialPageRoute(
                                    builder: (context) => InfoCreditIncome()
                                )
                            );
                        },
                        child: Card(
                            elevation: 3.3,
                            child: Padding(
                                padding: const EdgeInsets.all(16.0),
                                child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                        Text("Informasi Kredit Income"),
                                        _providerCreditIncome.flag
                                            ? Icon(Icons.check, color: primaryOrange)
                                            : SizedBox()
                                    ],
                                ),
                            ),
                        ),
                    ),
                   _providerCreditIncome.autoValidate
                       ? Container(
                       margin: EdgeInsets.only(left: MediaQuery.of(context).size.width / 37),
                       child: Text("Data belum lengkap",style: TextStyle(color: Colors.red, fontSize: 12)),
                   )
                       : SizedBox(height: MediaQuery.of(context).size.height / 87),
                ],
            ),
        );
    }

    Future<void> setNextState(BuildContext context,int index) async{
        SharedPreferences _preferences = await SharedPreferences.getInstance();
        var _providerCreditStructure = Provider.of<InfoCreditStructureChangeNotifier>(context, listen: true);
        var _providerInfoStructureCreditTypeInstallment = Provider.of<InfoCreditStructureTypeInstallmentChangeNotifier>(context,listen: false);
        var _providerCreditSubsidy = Provider.of<InfoCreditSubsidyChangeNotifier>(context, listen: true);
        var _providerMajorInsurance = Provider.of<InfoMajorInsuranceChangeNotifier>(context, listen: true);
        var _providerAdditionalInsurance = Provider.of<InfoAdditionalInsuranceChangeNotifier>(context, listen: true);
        var _providerWMP = Provider.of<InfoWMPChangeNotifier>(context, listen: true);
        var _providerCreditIncome = Provider.of<InfoCreditIncomeChangeNotifier>(context, listen: true);

        await _providerCreditStructure.setDataFromSQLite(context);
        await _providerCreditStructure.setDataFromSQLiteFeeCreditStructure();
        await _providerMajorInsurance.setDataFromSQLite(context);
        await _providerAdditionalInsurance.setDataFromSQLite(context);
        await _providerWMP.setDataFromSQLite(context);
        await _providerCreditIncome.setDataFromSQLite(context);

        if(_preferences.getString("last_known_state") == "IDE" && index != null){
            if(index != 8){
                _providerCreditStructure.flag = true;
                // _providerInfoStructureCreditTypeInstallment.flag = true;
                _providerMajorInsurance.isCheckData = true;
                _providerAdditionalInsurance.isCheckData = true;
                _providerWMP.isCheckData = true;
                _providerCreditIncome.flag = true;
                if(_preferences.getString("cust_type") == "PER"){
                    Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).isMenuDetailLoanDone = true;
                    Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).selectedIndex += 1;
                }
                else {
                    Provider.of<FormMParentCompanyChangeNotifier>(context,listen: false).isMenuDetailLoanDone = true;
                    Provider.of<FormMParentCompanyChangeNotifier>(context,listen: false).selectedIndex += 1;
                }
                await _providerCreditSubsidy.setDataFromSQLite(context, index);
            }
            else{
                if(_preferences.getString("cust_type") == "PER"){
                    Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).selectedIndex = 8;
                } else {
                    Provider.of<FormMParentCompanyChangeNotifier>(context,listen: false).selectedIndex = 8;
                }
            }
        }
    }
}
