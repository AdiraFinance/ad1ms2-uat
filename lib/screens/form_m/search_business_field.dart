import 'package:ad1ms2_dev/shared/form_m_company_rincian_change_notif.dart';
import 'package:ad1ms2_dev/shared/search_business_field_change_notif.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../main.dart';

class SearchBusinessField extends StatefulWidget {
  final String id;

  const SearchBusinessField({Key key, this.id}) : super(key: key);

  @override
  _SearchBusinessFieldState createState() => _SearchBusinessFieldState();
}

class _SearchBusinessFieldState extends State<SearchBusinessField> {
  @override
  void initState() {
    // TODO: implement initState
    Provider.of<SearchBusinessFieldChangeNotif>(context, listen: false).getBusinessField(widget.id);
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
          primaryColor: Colors.black,
          fontFamily: "NunitoSans",
          accentColor: myPrimaryColor,
          primarySwatch: primaryOrange
      ),
      child: Scaffold(
          key: Provider.of<SearchBusinessFieldChangeNotif>(context, listen: false).scaffoldKey,
          appBar: AppBar(
            title: Consumer<SearchBusinessFieldChangeNotif>(
              builder: (context, searchBusinessChangeNotif, _) {
                return TextFormField(
                  controller: searchBusinessChangeNotif.controllerSearch,
                  style: TextStyle(color: Colors.black),
                  textInputAction: TextInputAction.search,
                  onFieldSubmitted: (e) {
                    searchBusinessChangeNotif.searchBusinessField(e);
                  },
                  onChanged: (e) {
                    searchBusinessChangeNotif.changeAction(e);
                  },
                  cursorColor: Colors.black,
                  textCapitalization: TextCapitalization.characters,
                  decoration: new InputDecoration(
                    hintText: "Cari Lapangan Usaha (minimal 3 karakter)",
                    hintStyle: TextStyle(color: Colors.black),
                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: myPrimaryColor),
                      //  when the TextFormField in unfocused
                    ),
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: myPrimaryColor),
                      //  when the TextFormField in focused
                    ),
                  ),
                  autofocus: true,
                );
              },
            ),
            backgroundColor: myPrimaryColor,
            iconTheme: IconThemeData(color: Colors.black),
            actions: <Widget>[
              Provider.of<SearchBusinessFieldChangeNotif>(context, listen: true)
                      .showClear
                  ? IconButton(
                      icon: Icon(Icons.clear),
                      onPressed: () {
                        Provider.of<SearchBusinessFieldChangeNotif>(context,
                            listen: false).clearSearchTemp();
                        Provider.of<SearchBusinessFieldChangeNotif>(context,
                                listen: false)
                            .controllerSearch
                            .clear();
                        Provider.of<SearchBusinessFieldChangeNotif>(context,
                                listen: false)
                            .changeAction(
                                Provider.of<SearchBusinessFieldChangeNotif>(
                                        context,
                                        listen: false)
                                    .controllerSearch
                                    .text);
                      })
                  : SizedBox(
                      width: 0.0,
                      height: 0.0,
                    )
            ],
          ),
          body: Consumer<SearchBusinessFieldChangeNotif>(
            builder: (context, searchBusinessChangeNotif, _) {
              return searchBusinessChangeNotif.listBusinessFieldTemp.isNotEmpty
                ? ListView.separated(
                  padding: EdgeInsets.symmetric(
                      vertical: MediaQuery.of(context).size.height / 57,
                      horizontal: MediaQuery.of(context).size.width / 27),
                  itemCount: searchBusinessChangeNotif.listBusinessFieldTemp.length,
                  itemBuilder: (listContext, index) {
                    return InkWell(
                      onTap: () {
                        Navigator.pop(context,
                            searchBusinessChangeNotif.listBusinessFieldTemp[index]);
                      },
                      child: Container(
                        child: Row(
                          children: [
                            Expanded(
                                child: Text(
                              "${searchBusinessChangeNotif.listBusinessFieldTemp[index].KODE} - "
                              "${searchBusinessChangeNotif.listBusinessFieldTemp[index].DESKRIPSI}",
                              style: TextStyle(fontSize: 16),
                            ))
                          ],
                        ),
                      ),
                    );
                  },
                  separatorBuilder: (context, index) {
                    return Divider();
                  },
                )
                : ListView.separated(
                  padding: EdgeInsets.symmetric(
                      vertical: MediaQuery.of(context).size.height / 57,
                      horizontal: MediaQuery.of(context).size.width / 27),
                  itemCount: searchBusinessChangeNotif.listBusinessField.length,
                  itemBuilder: (listContext, index) {
                    return InkWell(
                      onTap: () {
                        Navigator.pop(context,
                            searchBusinessChangeNotif.listBusinessField[index]);
                      },
                      child: Container(
                        child: Row(
                          children: [
                            Expanded(
                                child: Text(
                                  "${searchBusinessChangeNotif.listBusinessField[index].KODE} - "
                                      "${searchBusinessChangeNotif.listBusinessField[index].DESKRIPSI}",
                                  style: TextStyle(fontSize: 16),
                                ))
                          ],
                        ),
                      ),
                    );
                  },
                  separatorBuilder: (context, index) {
                    return Divider();
                  },
                );
            },
          )),
    );
  }
}
