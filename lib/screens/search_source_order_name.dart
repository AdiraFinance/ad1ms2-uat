import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/shared/search_source_order_name_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SearchSourceOrderName extends StatefulWidget {
  @override
  _SearchSourceOrderNameState createState() => _SearchSourceOrderNameState();
}

class _SearchSourceOrderNameState extends State<SearchSourceOrderName> {
  Future<void> _getSourceOrderName;

  @override
  void initState() {
    super.initState();
    _getSourceOrderName = Provider.of<SearchSourceOrderNameChangeNotifier>(context,listen: false).getSourceOrderName(context);
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
          primaryColor: Colors.black,
          accentColor: myPrimaryColor,
          fontFamily: "NunitoSans",
          primarySwatch: primaryOrange
      ),
      child: Scaffold(
        key: Provider.of<SearchSourceOrderNameChangeNotifier>(context,listen: false).scaffoldKey,
        appBar: AppBar(
          title: Consumer<SearchSourceOrderNameChangeNotifier>(
            builder: (context, searchSourceOrderNameChangeNotifier, _) {
              return TextFormField(
                controller: searchSourceOrderNameChangeNotifier.controllerSearch,
                style: TextStyle(color: Colors.black),
                textInputAction: TextInputAction.search,
                onFieldSubmitted: (query) {
                  // if(query.length >= 3) {
                  //   searchSourceOrderNameChangeNotifier.getSourceOrderName(context, query);
                  // } else {
                  //   searchSourceOrderNameChangeNotifier.showSnackBar("Input minimal 3 karakter");
                  // }
                  searchSourceOrderNameChangeNotifier.searchSourceOrderName(query);
                },
                onChanged: (e) {
                  searchSourceOrderNameChangeNotifier.changeAction(e);
                },
                textCapitalization: TextCapitalization.characters,
                cursorColor: Colors.black,
                decoration: new InputDecoration(
                  hintText: "Cari Nama Sumber Order (minimal 3 karakter)",
                  hintStyle: TextStyle(color: Colors.black),
                  enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: myPrimaryColor),
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: myPrimaryColor),
                  ),
                ),
                autofocus: true,
              );
            },
          ),
          backgroundColor: myPrimaryColor,
          iconTheme: IconThemeData(color: Colors.black),
          actions: <Widget>[
            Provider.of<SearchSourceOrderNameChangeNotifier>(context,
                        listen: true)
                    .showClear
                ? IconButton(
                    icon: Icon(Icons.clear),
                    onPressed: () {
                      Provider.of<SearchSourceOrderNameChangeNotifier>(context,
                          listen: false).clearSearchTemp();
                      Provider.of<SearchSourceOrderNameChangeNotifier>(context,
                              listen: false)
                          .controllerSearch
                          .clear();
                      Provider.of<SearchSourceOrderNameChangeNotifier>(context,
                              listen: false)
                          .changeAction(
                              Provider.of<SearchSourceOrderNameChangeNotifier>(
                                      context,
                                      listen: false)
                                  .controllerSearch
                                  .text);
                    })
                : SizedBox(
                    width: 0.0,
                    height: 0.0,
                  )
          ],
        ),
        body: FutureBuilder(
          future: _getSourceOrderName,
          builder: (context,snapshot){
            if(snapshot.connectionState == ConnectionState.waiting){
              return Center(
                child: CircularProgressIndicator(),
              );
            }
            return Consumer<SearchSourceOrderNameChangeNotifier>(
              builder: (context, searchSourceOrderNameChangeNotifier, _) {
                return
                  //   searchSourceOrderNameChangeNotifier.loadData
                  //     ?
                  // Center(
                  //   child: CircularProgressIndicator(),
                  // )
                  //     :
                  searchSourceOrderNameChangeNotifier.listSourceOrderNameTemp.isNotEmpty
                      ?
                  ListView.separated(
                    padding: EdgeInsets.symmetric(
                        vertical: MediaQuery.of(context).size.height / 57,
                        horizontal: MediaQuery.of(context).size.width / 27),
                    itemCount:
                    searchSourceOrderNameChangeNotifier.listSourceOrderNameTemp.length,
                    itemBuilder: (listContext, index) {
                      return InkWell(
                        onTap: () {
                          Navigator.pop(
                              context,
                              searchSourceOrderNameChangeNotifier
                                  .listSourceOrderNameTemp[index]);
                        },
                        child: Container(
                          child: Row(
                            mainAxisSize: MainAxisSize.max,
                            children: [
                              Text(
                                "${searchSourceOrderNameChangeNotifier.listSourceOrderNameTemp[index].kode} - "
                                    "${searchSourceOrderNameChangeNotifier.listSourceOrderNameTemp[index].deskripsi}",
                                style: TextStyle(fontSize: 16),
                              )
                            ],
                          ),
                        ),
                      );
                    },
                    separatorBuilder: (context, index) {
                      return Divider();
                    },
                  )
                      :
                  ListView.separated(
                    padding: EdgeInsets.symmetric(
                        vertical: MediaQuery.of(context).size.height / 57,
                        horizontal: MediaQuery.of(context).size.width / 27),
                    itemCount:
                    searchSourceOrderNameChangeNotifier.listSourceOrderName.length,
                    itemBuilder: (listContext, index) {
                      return InkWell(
                        onTap: () {
                          Navigator.pop(
                              context,
                              searchSourceOrderNameChangeNotifier
                                  .listSourceOrderName[index]);
                        },
                        child: Container(
                          child: Row(
                            mainAxisSize: MainAxisSize.max,
                            children: [
                              Text(
                                "${searchSourceOrderNameChangeNotifier.listSourceOrderName[index].kode} - "
                                    "${searchSourceOrderNameChangeNotifier.listSourceOrderName[index].deskripsi}",
                                style: TextStyle(fontSize: 16),
                              )
                            ],
                          ),
                        ),
                      );
                    },
                    separatorBuilder: (context, index) {
                      return Divider();
                    },
                  );
              },
            );
          },
        ),
      ),
    );
  }
}
