import 'package:ad1ms2_dev/shared/search_coverage2_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../main.dart';

class SearchCoverage2 extends StatefulWidget {
    @override
    _SearchCoverage2State createState() => _SearchCoverage2State();
}

class _SearchCoverage2State extends State<SearchCoverage2> {
    @override
    Widget build(BuildContext context) {
        return Scaffold(
            appBar: AppBar(
                title: Consumer<SearchCoverage2ChangeNotifier>(
                    builder: (context, searchCoverageChangeNotifier, _) {
                        return TextFormField(
                            controller: searchCoverageChangeNotifier.controllerSearch,
                            style: TextStyle(color: Colors.black),
                            textInputAction: TextInputAction.search,
                            onFieldSubmitted: (e) {
//            _getCustomer(e);
                            },
                            onChanged: (e) {
                                searchCoverageChangeNotifier.changeAction(e);
                            },
                            cursorColor: Colors.black,
                            decoration: new InputDecoration(
                                hintText: "Cari Coverage 2",
                                hintStyle: TextStyle(color: Colors.black),
                                enabledBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(color: myPrimaryColor),
                                ),
                                focusedBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(color: myPrimaryColor),
                                ),
                            ),
                            autofocus: true,
                        );
                    },
                ),
                backgroundColor: myPrimaryColor,
                iconTheme: IconThemeData(color: Colors.black),
                actions: <Widget>[
                    Provider.of<SearchCoverage2ChangeNotifier>(context, listen: true)
                        .showClear
                        ? IconButton(
                        icon: Icon(Icons.clear),
                        onPressed: () {
                            Provider.of<SearchCoverage2ChangeNotifier>(context,
                                listen: false)
                                .controllerSearch
                                .clear();
                            Provider.of<SearchCoverage2ChangeNotifier>(context,
                                listen: false)
                                .changeAction(Provider.of<SearchCoverage2ChangeNotifier>(
                                context,
                                listen: false)
                                .controllerSearch
                                .text);
                        })
                        : SizedBox(
                        width: 0.0,
                        height: 0.0,
                    )
                ],
            ),
            body: Consumer<SearchCoverage2ChangeNotifier>(
                builder: (context, searchCoverage2ChangeNotifier, _) {
                    return ListView.separated(
                        padding: EdgeInsets.symmetric(
                            vertical: MediaQuery.of(context).size.height / 57,
                            horizontal: MediaQuery.of(context).size.width / 27),
                        itemCount: searchCoverage2ChangeNotifier.listCoverage2Model.length,
                        itemBuilder: (listContext, index) {
                            return InkWell(
                                onTap: () {
                                    Navigator.pop(context,
                                        searchCoverage2ChangeNotifier.listCoverage2Model[index]);
                                },
                                child: Container(
                                    child: Row(
                                        mainAxisSize: MainAxisSize.max,
                                        children: [
                                            Text(
                                                "${searchCoverage2ChangeNotifier.listCoverage2Model[index].id} - "
                                                    "${searchCoverage2ChangeNotifier.listCoverage2Model[index].text} ",
                                                style: TextStyle(fontSize: 16),
                                            )
                                        ],
                                    ),
                                ),
                            );
                        },
                        separatorBuilder: (context, index) {
                            return Divider();
                        },
                    );
                },
            ),
        );
    }
}
