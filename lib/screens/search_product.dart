import 'package:ad1ms2_dev/shared/search_company_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_product_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../main.dart';

class SearchProduct extends StatefulWidget {
    final String flag;
    final String company;
    const SearchProduct({this.flag, this.company});

    @override
    _SearchProductState createState() => _SearchProductState();
}

class _SearchProductState extends State<SearchProduct> {
    @override
    void initState() {
        super.initState();
        Provider.of<SearchProductChangeNotifier>(context, listen: false).getProduct(context, widget.flag, widget.company);
    }

    @override
    Widget build(BuildContext context) {
        return Scaffold(
            key: Provider.of<SearchProductChangeNotifier>(context, listen: false).scaffoldKey,
            appBar: AppBar(
                title: Consumer<SearchProductChangeNotifier>(
                    builder: (context, searchProductChangeNotifier, _) {
                        return TextFormField(
                            controller: searchProductChangeNotifier.controllerSearch,
                            style: TextStyle(color: Colors.black),
                            textInputAction: TextInputAction.search,
                            onFieldSubmitted: (e) {
                                searchProductChangeNotifier.searchProduct(e);
                            },
                            onChanged: (e) {
                                searchProductChangeNotifier.changeAction(e);
                            },
                            cursorColor: Colors.black,
                            decoration: new InputDecoration(
                                hintText: "Cari Produk (minimal 3 karakter)",
                                hintStyle: TextStyle(color: Colors.black),
                                enabledBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(color: myPrimaryColor),
                                ),
                                focusedBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(color: myPrimaryColor),
                                ),
                            ),
                            autofocus: true,
                        );
                    },
                ),
                backgroundColor: myPrimaryColor,
                iconTheme: IconThemeData(color: Colors.black),
                actions: <Widget>[
                    Provider.of<SearchProductChangeNotifier>(context, listen: true)
                        .showClear
                        ? IconButton(
                        icon: Icon(Icons.clear),
                        onPressed: () {
                            Provider.of<SearchProductChangeNotifier>(context,
                                listen: false)
                                .controllerSearch
                                .clear();
                            Provider.of<SearchProductChangeNotifier>(context,
                                listen: false)
                                .changeAction(Provider.of<SearchProductChangeNotifier>(
                                context,
                                listen: false)
                                .controllerSearch
                                .text);
                        })
                        : SizedBox(
                        width: 0.0,
                        height: 0.0,
                    )
                ],
            ),
            body: Consumer<SearchProductChangeNotifier>(
                builder: (context, searchProductChangeNotifier, _) {
                    return searchProductChangeNotifier.loadData
                        ?
                    Center(child: CircularProgressIndicator())
                        :
                    ListView.separated(
                        padding: EdgeInsets.symmetric(
                            vertical: MediaQuery.of(context).size.height / 57,
                            horizontal: MediaQuery.of(context).size.width / 27),
                        itemCount: searchProductChangeNotifier.listProductModel.length,
                        itemBuilder: (listContext, index) {
                            return InkWell(
                                onTap: () {
                                    Navigator.pop(context,
                                        searchProductChangeNotifier.listProductModel[index]);
                                },
                                child: Container(
                                    child: Row(
                                        mainAxisSize: MainAxisSize.max,
                                        children: [
                                            Text(
                                                "${searchProductChangeNotifier.listProductModel[index].KODE} - "
                                                    "${searchProductChangeNotifier.listProductModel[index].DESKRIPSI} ",
                                                style: TextStyle(fontSize: 16),
                                            )
                                        ],
                                    ),
                                ),
                            );
                        },
                        separatorBuilder: (context, index) {
                            return Divider();
                        },
                    );
                },
            ),
        );
    }
}
