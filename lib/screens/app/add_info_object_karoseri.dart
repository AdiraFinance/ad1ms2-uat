import 'package:ad1ms2_dev/models/karoseri_object_model.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/add_info_object_karoseri_change_notifier.dart';
import 'package:ad1ms2_dev/shared/decimal_text_input_formatter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

import '../../main.dart';

class AddInfoObjectKaroseri extends StatefulWidget {
  final int flag;
  final KaroseriObjectModel formMInfoObjectKaroseriModel;
  final int index;
  const AddInfoObjectKaroseri({Key key, this.flag, this.formMInfoObjectKaroseriModel, this.index}) : super(key: key);

  @override
  _AddInfoObjectKaroseriState createState() => _AddInfoObjectKaroseriState();
}

class _AddInfoObjectKaroseriState extends State<AddInfoObjectKaroseri> {
  Future<void> _loadData;
  @override
  void initState() {
      if (widget.flag != 0) {
          _loadData =
              Provider.of<AddInfoObjectKaroseriChangeNotifier>(context, listen: false).setValueForEditInfoObjKaroseri(widget.formMInfoObjectKaroseriModel, widget.index,context);
      } else {
          _loadData = Provider.of<AddInfoObjectKaroseriChangeNotifier>(context,listen: false).getDataFromDashboard(context);
      }
      Provider.of<AddInfoObjectKaroseriChangeNotifier>(context, listen: false).setPreference();
      // Provider.of<AddInfoObjectKaroseriChangeNotifier>(context,listen: false).getDataFromDashboard(context);
      super.initState();
  }
  @override
  Widget build(BuildContext context) {
    var _size = MediaQuery.of(context).size;
    return Theme(
        data: ThemeData(
            primaryColor: Colors.black,
            accentColor: myPrimaryColor,
            fontFamily: "NunitoSans",
            primarySwatch: primaryOrange),
        child: Scaffold(
            key: Provider.of<AddInfoObjectKaroseriChangeNotifier>(context, listen: false).scaffoldKey,
            appBar: AppBar(
                backgroundColor: myPrimaryColor,
                title: Text(
                    widget.flag == 0 ? "Tambah Karoseri" : "Edit Karoseri",
                    style: TextStyle(color: Colors.black)),
                centerTitle: true,
                iconTheme: IconThemeData(
                    color: Colors.black,
                ),
            ),
            body: SingleChildScrollView(
                padding: EdgeInsets.symmetric(
                    horizontal: MediaQuery.of(context).size.width / 27,
                    vertical: MediaQuery.of(context).size.height / 57),
                child:
//                 widget.flag == 0
//                     ?
//                 Consumer<AddInfoObjectKaroseriChangeNotifier>(
//                     builder: (consumerContext, addInfoObjKaroseriChangeNotif, _) {
//                         return Form(
//                             onWillPop: _onWillPop,
//                             key: addInfoObjKaroseriChangeNotif.key,
//                             child: Column(
//                                 crossAxisAlignment: CrossAxisAlignment.start,
//                                 children: [
//                                     Text(
//                                         "PKS Karoseri",
//                                         style: TextStyle(
//                                             fontSize: 16,
//                                             fontWeight: FontWeight.w700,
//                                             letterSpacing: 0.15),
//                                     ),
//                                     Row(
//                                         children: [
//                                             Row(
//                                                 children: [
//                                                     Radio(
//                                                         activeColor: primaryOrange,
//                                                         value: 1,
//                                                         groupValue: addInfoObjKaroseriChangeNotif
//                                                             .radioValuePKSKaroseri,
//                                                         onChanged: (value) {
//                                                             addInfoObjKaroseriChangeNotif
//                                                                 .radioValuePKSKaroseri = value;
//                                                         }),
//                                                     Text("Ya")
//                                                 ],
//                                             ),
//                                             Row(
//                                                 children: [
//                                                     Radio(
//                                                         activeColor: primaryOrange,
//                                                         value: 2,
//                                                         groupValue: addInfoObjKaroseriChangeNotif
//                                                             .radioValuePKSKaroseri,
//                                                         onChanged: (value) {
//                                                             addInfoObjKaroseriChangeNotif
//                                                                 .radioValuePKSKaroseri = value;
//                                                         }),
//                                                     Text("Tidak")
//                                                 ],
//                                             ),
//                                         ],
//                                     ),
//                                     SizedBox(height: _size.height / 47),
//                                     TextFormField(
//                                         readOnly: true,
//                                         onTap:() {
//                                             addInfoObjKaroseriChangeNotif.searchCompany(context);
//                                         },
//                                         controller: addInfoObjKaroseriChangeNotif
//                                             .controllerCompany,
//                                         style: new TextStyle(color: Colors.black),
//                                         decoration: new InputDecoration(
//                                             labelText: 'Perusahaan',
//                                             labelStyle: TextStyle(color: Colors.black),
//                                             border: OutlineInputBorder(
//                                                 borderRadius: BorderRadius.circular(8))),
//                                         keyboardType: TextInputType.text,
//                                         textCapitalization: TextCapitalization.characters,
//                                         autovalidate:
//                                         addInfoObjKaroseriChangeNotif.autoValidate,
//                                         validator: (e) {
//                                             if (e.isEmpty) {
//                                                 return "Tidak boleh kosong";
//                                             } else {
//                                                 return null;
//                                             }
//                                         },
//                                     ),
//                                     SizedBox(height: _size.height / 47),
//                                     TextFormField(
//                                         readOnly: true,
//                                         onTap:() {
//                                             addInfoObjKaroseriChangeNotif.searchKaroseri(context);
//                                         },
//                                         controller: addInfoObjKaroseriChangeNotif
//                                             .controllerKaroseri,
//                                         style: new TextStyle(color: Colors.black),
//                                         decoration: new InputDecoration(
//                                             labelText: 'Karoseri',
//                                             labelStyle: TextStyle(color: Colors.black),
//                                             border: OutlineInputBorder(
//                                                 borderRadius: BorderRadius.circular(8))),
//                                         keyboardType: TextInputType.text,
//                                         textCapitalization: TextCapitalization.characters,
//                                         autovalidate:
//                                         addInfoObjKaroseriChangeNotif.autoValidate,
//                                         validator: (e) {
//                                             if (e.isEmpty) {
//                                                 return "Tidak boleh kosong";
//                                             } else {
//                                                 return null;
//                                             }
//                                         },
//                                     ),
//                                     SizedBox(height: _size.height / 47),
//                                     TextFormField(
//                                         autovalidate: addInfoObjKaroseriChangeNotif.autoValidate,
//                                         validator: (e) {
//                                             if (e.isEmpty) {
//                                                 return "Tidak boleh kosong";
//                                             } else {
//                                                 return null;
//                                             }
//                                         },
//                                         controller: addInfoObjKaroseriChangeNotif.controllerJumlahKaroseri,
//                                         onTap: () {
//                                             addInfoObjKaroseriChangeNotif.calculatedTotalPrice();
//                                         },
//                                         onFieldSubmitted: (value) {
//                                             addInfoObjKaroseriChangeNotif.calculatedTotalPrice();
//                                         },
//                                         style: new TextStyle(color: Colors.black),
//                                         decoration: new InputDecoration(
//                                             labelText: 'Jumlah Karoseri',
//                                             labelStyle: TextStyle(color: Colors.black),
//                                             border: OutlineInputBorder(
//                                                 borderRadius: BorderRadius.circular(8))),
//                                         keyboardType: TextInputType.number,
//                                         textCapitalization: TextCapitalization.characters,
//                                         inputFormatters: [
//                                             WhitelistingTextInputFormatter.digitsOnly
//                                         ],
//                                     ),
//                                     SizedBox(height: _size.height / 47),
//                                     TextFormField(
//                                         autovalidate: addInfoObjKaroseriChangeNotif.autoValidate,
//                                         validator: (e) {
//                                             if (e.isEmpty) {
//                                                 return "Tidak boleh kosong";
//                                             } else {
//                                                 return null;
//                                             }
//                                         },
//                                         controller: addInfoObjKaroseriChangeNotif.controllerPrice,
//                                         style: new TextStyle(color: Colors.black),
//                                         decoration: new InputDecoration(
//                                             labelText: 'Harga',
//                                             labelStyle: TextStyle(color: Colors.black),
//                                             border: OutlineInputBorder(
//                                                 borderRadius: BorderRadius.circular(8))),
//                                         textAlign: TextAlign.end,
//                                         inputFormatters: [
//                                             DecimalTextInputFormatter(decimalRange: 2),
// //                                            addInfoObjKaroseriChangeNotif.amountValidator
//                                         ],
//                                         keyboardType: TextInputType.number,
//                                         onTap: () {
//                                             addInfoObjKaroseriChangeNotif.calculatedTotalPrice();
//                                         },
//                                         onFieldSubmitted: (value){
//                                             addInfoObjKaroseriChangeNotif.calculatedTotalPrice();
//                                             addInfoObjKaroseriChangeNotif.controllerPrice.text = addInfoObjKaroseriChangeNotif.formatCurrency.formatCurrency(value);
//                                         },
//                                     ),
//                                     SizedBox(height: _size.height / 47),
//                                     TextFormField(
//                                         // autovalidate: addInfoObjKaroseriChangeNotif.autoValidate,
//                                         // validator: (e) {
//                                         //     if (e.isEmpty) {
//                                         //         return "Tidak boleh kosong";
//                                         //     } else {
//                                         //         return null;
//                                         //     }
//                                         // },
//                                         enabled: false,
//                                         controller: addInfoObjKaroseriChangeNotif.controllerTotalPrice,
//                                         style: new TextStyle(color: Colors.black),
//                                         decoration: new InputDecoration(
//                                             labelText: 'Total Harga',
//                                             labelStyle: TextStyle(color: Colors.black),
//                                             border: OutlineInputBorder(
//                                                 borderRadius: BorderRadius.circular(8))),
//                                         keyboardType: TextInputType.number,
//                                         textCapitalization: TextCapitalization.characters,
//                                     ),
//                                     SizedBox(height: _size.height / 47),
//                                 ],
//                             ),
//                         );
//                     },
//                 )
//                     :
                FutureBuilder(
                    future: _loadData,
                    builder: (context, snapshot) {
                        if (snapshot.connectionState == ConnectionState.waiting) {
                            return Center(
                                child: CircularProgressIndicator(),
                            );
                        }
                        return Consumer<AddInfoObjectKaroseriChangeNotifier>(
                            builder:
                                (consumerContext, addInfoObjKaroseriChangeNotif, _) {
                                    return Form(
                                        key: addInfoObjKaroseriChangeNotif.key,
                                        onWillPop: _onWillPop,
                                        child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                                Visibility(
                                                    visible: addInfoObjKaroseriChangeNotif.isRadioValuePKSKaroseriVisible(),
                                                  child: Text(
                                                      "PKS Karoseri",
                                                      style: TextStyle(
                                                          fontSize: 16,
                                                          fontWeight: FontWeight.w700,
                                                          letterSpacing: 0.15,
                                                          color: addInfoObjKaroseriChangeNotif.isRadioValuePKSKaroseriChanges ? Colors.purple : Colors.black
                                                      ),
                                                  ),
                                                ),
                                                Visibility(
                                                    visible: addInfoObjKaroseriChangeNotif.isRadioValuePKSKaroseriVisible(),
                                                    child: IgnorePointer(
                                                        ignoring: addInfoObjKaroseriChangeNotif.isDisablePACIAAOSCONA,
                                                      child: Row(
                                                          children: [
                                                              Row(
                                                                  children: [
                                                                      Radio(
                                                                          activeColor: primaryOrange,
                                                                          value: 1,
                                                                          groupValue: addInfoObjKaroseriChangeNotif.radioValuePKSKaroseri,
                                                                          onChanged: (value) {
                                                                              addInfoObjKaroseriChangeNotif.isDisablePACIAAOSCONA ? null : addInfoObjKaroseriChangeNotif.radioValuePKSKaroseri = value;
                                                                          }),
                                                                      Text("Ya")
                                                                  ],
                                                              ),
                                                              Row(
                                                                  children: [
                                                                      Radio(
                                                                          activeColor: primaryOrange,
                                                                          value: 2,
                                                                          groupValue: addInfoObjKaroseriChangeNotif.radioValuePKSKaroseri,
                                                                          onChanged: (value) {
                                                                              addInfoObjKaroseriChangeNotif.isDisablePACIAAOSCONA ? null : addInfoObjKaroseriChangeNotif.radioValuePKSKaroseri = value;
                                                                          }),
                                                                      Text("Tidak")
                                                                  ],
                                                              ),
                                                          ],
                                                      ),
                                                    ),
                                                ),
                                                SizedBox(height: _size.height / 47),
                                                Visibility(
                                                  visible: addInfoObjKaroseriChangeNotif.isCompanyVisible(),
                                                  child: IgnorePointer(
                                                    ignoring: addInfoObjKaroseriChangeNotif.isDisablePACIAAOSCONA,
                                                    child: TextFormField(
                                                        autovalidate: addInfoObjKaroseriChangeNotif.autoValidate,
                                                        validator: (e) {
                                                            if (e.isEmpty && addInfoObjKaroseriChangeNotif.isCompanyMandatory()) {
                                                                return "Tidak boleh kosong";
                                                            } else {
                                                                return null;
                                                            }
                                                        },
                                                        readOnly: true,
                                                        onTap:() {
                                                            addInfoObjKaroseriChangeNotif.searchCompany(context);
                                                        },
                                                        enabled: addInfoObjKaroseriChangeNotif.isDisablePACIAAOSCONA ? false : addInfoObjKaroseriChangeNotif.radioValuePKSKaroseri == 2 ? false : true,
                                                        controller: addInfoObjKaroseriChangeNotif.controllerCompany,
                                                        style: new TextStyle(color: Colors.black),
                                                        decoration: new InputDecoration(
                                                            labelText: 'Perusahaan',
                                                            labelStyle: TextStyle(color: Colors.black),
                                                            filled: addInfoObjKaroseriChangeNotif.isDisablePACIAAOSCONA ? true : addInfoObjKaroseriChangeNotif.radioValuePKSKaroseri == 2 ? true : false,
                                                            fillColor: Colors.black12,
                                                            border: OutlineInputBorder(
                                                                borderRadius: BorderRadius.circular(8)),
                                                            enabledBorder: OutlineInputBorder(
                                                                borderSide: BorderSide(color: addInfoObjKaroseriChangeNotif.isCompanyChanges ? Colors.purple : Colors.grey)),
                                                            disabledBorder: OutlineInputBorder(
                                                                borderSide: BorderSide(color: addInfoObjKaroseriChangeNotif.isCompanyChanges ? Colors.purple : Colors.grey)),
                                                            contentPadding: EdgeInsets.symmetric(horizontal: 10),),
                                                        keyboardType: TextInputType.text,
                                                        textCapitalization: TextCapitalization.characters,
                                                    ),
                                                  ),
                                                ),
                                                SizedBox(height: _size.height / 47),
                                                Visibility(
                                                    visible: addInfoObjKaroseriChangeNotif.isKaroseriVisible(),
                                                  child: IgnorePointer(
                                                    ignoring: addInfoObjKaroseriChangeNotif.isDisablePACIAAOSCONA,
                                                    child: TextFormField(
                                                        readOnly: true,
                                                        onTap:() {
                                                            addInfoObjKaroseriChangeNotif.searchKaroseri(context, addInfoObjKaroseriChangeNotif.radioValuePKSKaroseri);
                                                        },
                                                        controller: addInfoObjKaroseriChangeNotif.controllerKaroseri,
                                                        style: new TextStyle(color: Colors.black),
                                                        decoration: new InputDecoration(
                                                            labelText: 'Karoseri',
                                                            labelStyle: TextStyle(color: Colors.black),
                                                            filled: addInfoObjKaroseriChangeNotif.isDisablePACIAAOSCONA,
                                                            fillColor: Colors.black12,
                                                            border: OutlineInputBorder(
                                                                borderRadius: BorderRadius.circular(8)),
                                                            enabledBorder: OutlineInputBorder(
                                                                borderSide: BorderSide(color: addInfoObjKaroseriChangeNotif.isKaroseriChanges ? Colors.purple : Colors.grey)),
                                                            disabledBorder: OutlineInputBorder(
                                                                borderSide: BorderSide(color: addInfoObjKaroseriChangeNotif.isKaroseriChanges ? Colors.purple : Colors.grey)),
                                                            contentPadding: EdgeInsets.symmetric(horizontal: 10),),
                                                        keyboardType: TextInputType.text,
                                                        textCapitalization: TextCapitalization.characters,
                                                        autovalidate: addInfoObjKaroseriChangeNotif.autoValidate,
                                                        validator: (e) {
                                                            if (e.isEmpty && addInfoObjKaroseriChangeNotif.isKaroseriMandatory()) {
                                                                return "Tidak boleh kosong";
                                                            } else {
                                                                return null;
                                                            }
                                                        },
                                                    ),
                                                  ),
                                                ),
                                                SizedBox(height: _size.height / 47),
                                                Visibility(
                                                    visible: addInfoObjKaroseriChangeNotif.isJumlahKaroseriVisible(),
                                                  child: IgnorePointer(
                                                    ignoring: addInfoObjKaroseriChangeNotif.isDisablePACIAAOSCONA,
                                                    child: TextFormField(
                                                        controller: addInfoObjKaroseriChangeNotif.controllerJumlahKaroseri,
                                                        style: new TextStyle(color: Colors.black),
                                                        decoration: new InputDecoration(
                                                            labelText: 'Jumlah Karoseri',
                                                            labelStyle: TextStyle(color: Colors.black),
                                                            filled: true,
                                                            fillColor: Colors.black12,
                                                            border: OutlineInputBorder(
                                                                borderRadius: BorderRadius.circular(8)),
                                                            enabledBorder: OutlineInputBorder(
                                                                borderSide: BorderSide(color: addInfoObjKaroseriChangeNotif.isJumlahKaroseriChanges ? Colors.purple : Colors.grey)),
                                                            disabledBorder: OutlineInputBorder(
                                                                borderSide: BorderSide(color: addInfoObjKaroseriChangeNotif.isJumlahKaroseriChanges ? Colors.purple : Colors.grey)),
                                                            contentPadding: EdgeInsets.symmetric(horizontal: 10),),
                                                        keyboardType: TextInputType.number,
                                                        textCapitalization: TextCapitalization.characters,
                                                        inputFormatters: [
                                                            WhitelistingTextInputFormatter.digitsOnly
                                                        ],
                                                        onTap: () {
                                                            addInfoObjKaroseriChangeNotif.calculatedTotalPrice();
                                                        },
                                                        onFieldSubmitted: (value){
                                                            addInfoObjKaroseriChangeNotif.calculatedTotalPrice();
                                                        },
                                                        enabled: false,
                                                        autovalidate: addInfoObjKaroseriChangeNotif.autoValidate,
                                                        validator: (e) {
                                                            if (e.isEmpty && addInfoObjKaroseriChangeNotif.isJumlahKaroseriMandatory()) {
                                                                return "Tidak boleh kosong";
                                                            } else {
                                                                return null;
                                                            }
                                                        },
                                                    ),
                                                  ),
                                                ),
                                                SizedBox(height: _size.height / 47),
                                                Visibility(
                                                    visible: addInfoObjKaroseriChangeNotif.isPriceVisible(),
                                                  child: IgnorePointer(
                                                    ignoring: addInfoObjKaroseriChangeNotif.isDisablePACIAAOSCONA,
                                                    child: TextFormField(
                                                        autovalidate: addInfoObjKaroseriChangeNotif.autoValidate,
                                                        validator: (e) {
                                                            if (e.isEmpty && addInfoObjKaroseriChangeNotif.isPriceMandatory()) {
                                                                return "Tidak boleh kosong";
                                                            } else {
                                                                return null;
                                                            }
                                                        },
                                                        controller: addInfoObjKaroseriChangeNotif.controllerPrice,
                                                        onTap: () {
                                                            addInfoObjKaroseriChangeNotif.calculatedTotalPrice();
                                                        },
                                                        onFieldSubmitted: (value){
                                                            addInfoObjKaroseriChangeNotif.calculatedTotalPrice();
                                                            addInfoObjKaroseriChangeNotif.controllerPrice.text = addInfoObjKaroseriChangeNotif.formatCurrency.formatCurrency(value);
                                                        },
                                                        style: new TextStyle(color: Colors.black),
                                                        decoration: new InputDecoration(
                                                            labelText: 'Harga',
                                                            labelStyle: TextStyle(color: Colors.black),
                                                            filled: addInfoObjKaroseriChangeNotif.isDisablePACIAAOSCONA,
                                                            fillColor: Colors.black12,
                                                            border: OutlineInputBorder(
                                                                borderRadius: BorderRadius.circular(8)),
                                                            enabledBorder: OutlineInputBorder(
                                                                borderSide: BorderSide(color: addInfoObjKaroseriChangeNotif.isPriceChanges ? Colors.purple : Colors.grey)),
                                                            disabledBorder: OutlineInputBorder(
                                                                borderSide: BorderSide(color: addInfoObjKaroseriChangeNotif.isPriceChanges ? Colors.purple : Colors.grey)),
                                                            contentPadding: EdgeInsets.symmetric(horizontal: 10),),
                                                        keyboardType: TextInputType.number,
                                                        textAlign: TextAlign.end,
                                                        inputFormatters: [
                                                            WhitelistingTextInputFormatter.digitsOnly
                                                        ],
                                                        textCapitalization: TextCapitalization.characters,
                                                    ),
                                                  ),
                                                ),
                                                SizedBox(height: _size.height / 47),
                                                Visibility(
                                                  visible: addInfoObjKaroseriChangeNotif.isTotalPriceVisible(),
                                                  child: IgnorePointer(
                                                    ignoring: addInfoObjKaroseriChangeNotif.isDisablePACIAAOSCONA,
                                                    child: TextFormField(
                                                        autovalidate: addInfoObjKaroseriChangeNotif.autoValidate,
                                                        validator: (e) {
                                                            if (e.isEmpty && addInfoObjKaroseriChangeNotif.isTotalPriceMandatory()) {
                                                                return "Tidak boleh kosong";
                                                            } else {
                                                                return null;
                                                            }
                                                        },
                                                        enabled: false,
                                                        controller: addInfoObjKaroseriChangeNotif.controllerTotalPrice,
                                                        style: new TextStyle(color: Colors.black),
                                                        decoration: new InputDecoration(
                                                            labelText: 'Total Harga',
                                                            labelStyle: TextStyle(color: Colors.black),
                                                            filled: true,
                                                            fillColor: Colors.black12,
                                                            border: OutlineInputBorder(
                                                                borderRadius: BorderRadius.circular(8)),
                                                            enabledBorder: OutlineInputBorder(
                                                                borderSide: BorderSide(color: addInfoObjKaroseriChangeNotif.isTotalPriceChanges ? Colors.purple : Colors.grey)),
                                                            disabledBorder: OutlineInputBorder(
                                                                borderSide: BorderSide(color: addInfoObjKaroseriChangeNotif.isTotalPriceChanges ? Colors.purple : Colors.grey)),
                                                            contentPadding: EdgeInsets.symmetric(horizontal: 10),),
                                                        textAlign: TextAlign.end,
                                                        keyboardType: TextInputType.number,
                                                        textCapitalization: TextCapitalization.characters,
                                                    ),
                                                  ),
                                                ),
                                                SizedBox(height: _size.height / 47),
                                            ],
                                        ),
                                    );
                            },
                        );
                    }),
            ),
            bottomNavigationBar: BottomAppBar(
                elevation: 0.0,
                child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Consumer<AddInfoObjectKaroseriChangeNotifier>(
                        builder: (context, addInfoObjKaroseriChangeNotif, _) {
                            return RaisedButton(
                                shape: RoundedRectangleBorder(
                                    borderRadius: new BorderRadius.circular(8.0)),
                                color: myPrimaryColor,
                                onPressed: () {
                                    if (widget.flag == 0) {
                                        addInfoObjKaroseriChangeNotif.check(context, widget.flag, null);
                                    } else {
                                        addInfoObjKaroseriChangeNotif.check(context, widget.flag, widget.index);
                                    }
                                },
                                child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                        Text(widget.flag == 0 ? "SAVE" : "UPDATE",
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 14,
                                                fontWeight: FontWeight.w500,
                                                letterSpacing: 1.25))
                                    ],
                                ));
                        },
                    )),
            ),
        ),
    );
  }

  Future<bool> _onWillPop() async {
      var _provider = Provider.of<AddInfoObjectKaroseriChangeNotifier>(context, listen: false);
      if (widget.flag == 0) {
          if (_provider.radioValuePKSKaroseri != 0 ||
              _provider.controllerCompany.text != "" ||
              _provider.controllerKaroseri.text != "" ||
              _provider.controllerJumlahKaroseri.text != "" ||
              _provider.controllerPrice.text != "") {
              return (await showDialog(
                  context: context,
                  builder: (myContext) => AlertDialog(
                      title: new Text('Warning'),
                      content: new Text('Simpan perubahan?'),
                      actions: <Widget>[
                          new FlatButton(
                              onPressed: () {
                                  widget.flag == 0
                                      ?
                                  _provider.check(context, widget.flag, null)
                                      :
                                  _provider.check(context, widget.flag, widget.index);
                                  Navigator.pop(context);
                              },
                              child: new Text('Ya', style: TextStyle(color: Colors.grey)),
                          ),
                          new FlatButton(
                              onPressed: () => Navigator.of(context).pop(true),
                              child: new Text('Tidak'),
                          ),
                      ],
                  ),
              )) ??
                  false;
          } else {
              return true;
          }
      } else {
          if (widget.formMInfoObjectKaroseriModel.PKSKaroseri != _provider.radioValuePKSKaroseri ||
              widget.formMInfoObjectKaroseriModel.company.id != _provider.companyModelSelected.id ||
              widget.formMInfoObjectKaroseriModel.karoseri.kode != _provider.karoseriSelected.deskripsi ||
              widget.formMInfoObjectKaroseriModel.jumlahKaroseri != _provider.controllerJumlahKaroseri.text ||
              widget.formMInfoObjectKaroseriModel.price != _provider.controllerPrice.text) {
              return (await showDialog(
                  context: context,
                  builder: (myContext) => AlertDialog(
                      title: new Text('Warning'),
                      content: new Text('Simpan perubahan?'),
                      actions: <Widget>[
                          new FlatButton(
                              onPressed: () {
                                  _provider.check(context, widget.flag, widget.index);
                                  Navigator.pop(context);
                              },
                              child: new Text('Ya', style: TextStyle(color: Colors.grey)),
                          ),
                          new FlatButton(
                              onPressed: () => Navigator.of(context).pop(true),
                              child: new Text('Tidak'),
                          ),
                      ],
                  ),
              )) ??
                  false;
          } else {
              return true;
          }
      }
  }
}
