import 'package:ad1ms2_dev/models/credit_subsidy_model.dart';
import 'package:ad1ms2_dev/models/cutting_method_model.dart';
import 'package:ad1ms2_dev/models/installment_index_model.dart';
import 'package:ad1ms2_dev/models/subsidy_type_model.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/add_credit_subsidy_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_credit_structure_change_notifier.dart';
import 'package:ad1ms2_dev/shared/decimal_text_input_formatter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

import '../../main.dart';

class AddInfoCreditSubsidy extends StatefulWidget {
    final int flag;
    final CreditSubsidyModel formMInfoCreditSubsidyModel;
    final int index;
    const AddInfoCreditSubsidy({Key key, this.flag, this.formMInfoCreditSubsidyModel, this.index}) : super(key: key);

    @override
    _AddInfoCreditSubsidyState createState() => _AddInfoCreditSubsidyState();
}

class _AddInfoCreditSubsidyState extends State<AddInfoCreditSubsidy> {
    Future<void> _loadData;
    @override
    void initState() {
        super.initState();
        // int _periodTime = int.parse(Provider.of<InfoCreditStructureChangeNotifier>(context, listen: false).periodOfTimeSelected) - 1;
        _loadData = Provider.of<AddCreditSubsidyChangeNotifier>(context, listen: false)
            .setValueForEditInfoCreditSubsidy(context, widget.formMInfoCreditSubsidyModel, widget.flag, 5, widget.index);
    }
    @override
    Widget build(BuildContext context) {
        var _size = MediaQuery.of(context).size;
        return Theme(
            data: ThemeData(
                primaryColor: Colors.black,
                accentColor: myPrimaryColor,
                fontFamily: "NunitoSans",
                primarySwatch: Colors.yellow),
            child: Scaffold(
                key: Provider.of<AddCreditSubsidyChangeNotifier>(context, listen: false).scaffoldKey,
                appBar: AppBar(
                    backgroundColor: myPrimaryColor,
                    title: Text(
                        widget.flag == 0 ? "Tambah Info Kredit Subsidi" : "Edit Info Kredit Subsidi",
                        style: TextStyle(color: Colors.black)),
                    centerTitle: true,
                    iconTheme: IconThemeData(
                        color: Colors.black,
                    ),
                ),
                body: Provider.of<AddCreditSubsidyChangeNotifier>(context, listen: true).loadData
                    ?
                Center(
                    child: CircularProgressIndicator(),
                )
                    :
                SingleChildScrollView(
                    padding: EdgeInsets.symmetric(
                        horizontal: MediaQuery.of(context).size.width / 27,
                        vertical: MediaQuery.of(context).size.height / 57),
                    child: FutureBuilder(
                        future: _loadData,
                        builder: (context, snapshot) {
                            if (snapshot.connectionState == ConnectionState.waiting) {
                                return Center(
                                    child: CircularProgressIndicator(),
                                );
                            }
                            return Consumer<AddCreditSubsidyChangeNotifier>(
                                builder:
                                    (consumerContext, addCreditSubsidyChangeNotifier, _) {
                                        return Form(
                                            onWillPop: _onWillPop,
                                            key: addCreditSubsidyChangeNotifier.key,
                                            child: Column(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                    Visibility(
                                                      visible: addCreditSubsidyChangeNotifier.isRadioValueGiverVisible(),
                                                      child: Text(
                                                          "Pemberi",
                                                          style: TextStyle(
                                                              color : addCreditSubsidyChangeNotifier.giverDakor ? Colors.purple : Colors.grey,
                                                              fontSize: 16,
                                                              fontWeight: FontWeight.w700,
                                                              letterSpacing: 0.15),
                                                      ),
                                                    ),
                                                    Visibility(
                                                      visible: addCreditSubsidyChangeNotifier.isRadioValueGiverVisible(),
                                                      child: Row(
                                                          children: [
                                                              Radio(
                                                                  activeColor: primaryOrange,
                                                                  value: "0",
                                                                  groupValue: addCreditSubsidyChangeNotifier.radioValueGiver,
                                                                  onChanged: (value) {
                                                                      addCreditSubsidyChangeNotifier.radioValueGiver = value;
                                                                      addCreditSubsidyChangeNotifier.getListType(context, null, 0, null);
                                                                  }),
                                                              Text("Adira")
                                                          ],
                                                      ),
                                                    ),
                                                    Visibility(
                                                      visible: addCreditSubsidyChangeNotifier.isRadioValueGiverVisible(),
                                                      child: Row(
                                                          children: [
                                                              Radio(
                                                                  activeColor: primaryOrange,
                                                                  value: "1",
                                                                  groupValue: addCreditSubsidyChangeNotifier.radioValueGiver,
                                                                  onChanged: (value) {
                                                                      addCreditSubsidyChangeNotifier.radioValueGiver = value;
                                                                      addCreditSubsidyChangeNotifier.getListType(context, null, 0, null);
                                                                  }),
                                                              Text("Dealer/Merchant/AXI/Pemasok")
                                                          ],
                                                      ),
                                                    ),
                                                    Visibility(
                                                        visible: addCreditSubsidyChangeNotifier.isRadioValueGiverVisible(),
                                                        child: SizedBox(height: _size.height / 47)
                                                    ),
                                                    Visibility(
                                                      visible: addCreditSubsidyChangeNotifier.isTypeSelectedVisible(),
                                                      child: IgnorePointer(
                                                        ignoring: addCreditSubsidyChangeNotifier.isDisablePACIAAOSCONA,
                                                        child: DropdownButtonFormField<SubsidyTypeModel>(
                                                            autovalidate: addCreditSubsidyChangeNotifier.autoValidate,
                                                            validator: (e) {
                                                                if (e == null && addCreditSubsidyChangeNotifier.isTypeSelectedMandatory()) {
                                                                    return "Tidak boleh kosong";
                                                                } else {
                                                                    return null;
                                                                }
                                                            },
                                                            value: addCreditSubsidyChangeNotifier.typeSelected,
                                                            onChanged: (value) {
                                                                addCreditSubsidyChangeNotifier.typeSelected = value;
                                                            },
                                                            decoration: InputDecoration(
                                                                labelText: "Jenis",
                                                                border: OutlineInputBorder(),
                                                                filled: addCreditSubsidyChangeNotifier.isDisablePACIAAOSCONA,
                                                                fillColor: Colors.black12,
                                                                enabledBorder: OutlineInputBorder(
                                                                    borderSide: BorderSide(color: addCreditSubsidyChangeNotifier.typeDakor ? Colors.purple : Colors.grey)),
                                                                disabledBorder: OutlineInputBorder(
                                                                    borderSide: BorderSide(color: addCreditSubsidyChangeNotifier.typeDakor ? Colors.purple : Colors.grey)),
                                                                contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                                            ),
                                                            items: addCreditSubsidyChangeNotifier.listType.map((value) {
                                                                return DropdownMenuItem<SubsidyTypeModel>(
                                                                    value: value,
                                                                    child: Text(
                                                                        value.text,
                                                                        overflow: TextOverflow.ellipsis,
                                                                    ),
                                                                );
                                                            }).toList(),
                                                        ),
                                                      ),
                                                    ),
                                                    Visibility(
                                                        visible: addCreditSubsidyChangeNotifier.isTypeSelectedVisible(),
                                                        child: SizedBox(height: _size.height / 47)
                                                    ),
                                                    Visibility(
                                                      visible: addCreditSubsidyChangeNotifier.isCuttingMethodSelectedVisible(),
                                                      child: Visibility(
                                                        visible: addCreditSubsidyChangeNotifier.typeSelected == null ? false : addCreditSubsidyChangeNotifier.radioValueGiver == "0" ? false : addCreditSubsidyChangeNotifier.typeSelected == null ? true : addCreditSubsidyChangeNotifier.typeSelected.id == "06" ? false : true,
                                                        child: IgnorePointer(
                                                          ignoring: addCreditSubsidyChangeNotifier.isDisablePACIAAOSCONA,
                                                          child: DropdownButtonFormField<CuttingMethodModel>(
                                                              autovalidate: addCreditSubsidyChangeNotifier.autoValidate,
                                                              validator: (e) {
                                                                  if (e == null && addCreditSubsidyChangeNotifier.isCuttingMethodSelectedMandatory()) {
                                                                      return "Tidak boleh kosong";
                                                                  } else {
                                                                      return null;
                                                                  }
                                                              },
                                                              value: addCreditSubsidyChangeNotifier.cuttingMethodSelected,
                                                              onChanged: (value) {
                                                                  addCreditSubsidyChangeNotifier.cuttingMethodSelected = value;
                                                              },
                                                              decoration: InputDecoration(
                                                                  labelText: "Metode Pemotongan",
                                                                  filled: addCreditSubsidyChangeNotifier.isDisablePACIAAOSCONA,
                                                                  fillColor: Colors.black12,
                                                                  border: OutlineInputBorder(),
                                                                  enabledBorder: OutlineInputBorder(
                                                                      borderSide: BorderSide(color: addCreditSubsidyChangeNotifier.cuttingMethodDakor ? Colors.purple : Colors.grey)),
                                                                  disabledBorder: OutlineInputBorder(
                                                                      borderSide: BorderSide(color: addCreditSubsidyChangeNotifier.cuttingMethodDakor ? Colors.purple : Colors.grey)),
                                                                  contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                                              ),
                                                              items: addCreditSubsidyChangeNotifier.listCuttingMethod.map((value) {
                                                                  return DropdownMenuItem<CuttingMethodModel>(
                                                                      value: value,
                                                                      child: Text(
                                                                          "${value.id} - ${value.text}",
                                                                          overflow: TextOverflow.ellipsis,
                                                                      ),
                                                                  );
                                                              }).toList(),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                    Visibility(
                                                      visible: addCreditSubsidyChangeNotifier.isCuttingMethodSelectedVisible(),
                                                      child: Visibility(
                                                          visible: addCreditSubsidyChangeNotifier.typeSelected == null ? false : addCreditSubsidyChangeNotifier.radioValueGiver == "0" ? false : addCreditSubsidyChangeNotifier.typeSelected == null ? true : addCreditSubsidyChangeNotifier.typeSelected.id == "06" ? false : true,
                                                          child: SizedBox(height: _size.height / 47)
                                                      ),
                                                    ),
                                                    Visibility(
                                                      visible: addCreditSubsidyChangeNotifier.isValueVisible(),
                                                      child: Visibility(
                                                        visible: addCreditSubsidyChangeNotifier.typeSelected == null ? false : addCreditSubsidyChangeNotifier.radioValueGiver == "0" ? false : addCreditSubsidyChangeNotifier.typeSelected == null ? true : addCreditSubsidyChangeNotifier.typeSelected.id == "06" ? false : true,
                                                        child: IgnorePointer(
                                                          ignoring: addCreditSubsidyChangeNotifier.isDisablePACIAAOSCONA,
                                                          child: TextFormField(
                                                              controller: addCreditSubsidyChangeNotifier.controllerValue,
                                                              style: TextStyle(color: Colors.black),
                                                              enabled: addCreditSubsidyChangeNotifier.cuttingMethodSelected == null ? false : addCreditSubsidyChangeNotifier.cuttingMethodSelected.id == "01" || addCreditSubsidyChangeNotifier.cuttingMethodSelected.id == "03" ? true : false,
                                                              decoration: InputDecoration(
                                                                  filled: addCreditSubsidyChangeNotifier.isDisablePACIAAOSCONA ? true : addCreditSubsidyChangeNotifier.cuttingMethodSelected == null ? true : addCreditSubsidyChangeNotifier.cuttingMethodSelected.id == "01" || addCreditSubsidyChangeNotifier.cuttingMethodSelected.id == "03" ? false : true,
                                                                  fillColor: addCreditSubsidyChangeNotifier.cuttingMethodSelected == null ? Colors.black12 : addCreditSubsidyChangeNotifier.cuttingMethodSelected.id == "01" || addCreditSubsidyChangeNotifier.cuttingMethodSelected.id == "03" ? null : Colors.black12,
                                                                  labelText: 'Nilai Potongan Pencairan',
                                                                  labelStyle: TextStyle(color: Colors.black),
                                                                  border: OutlineInputBorder(
                                                                      borderRadius: BorderRadius.circular(8)),
                                                                  enabledBorder: OutlineInputBorder(
                                                                      borderSide: BorderSide(color: addCreditSubsidyChangeNotifier.cuttingValueDakor ? Colors.purple : Colors.grey)),
                                                                  disabledBorder: OutlineInputBorder(
                                                                      borderSide: BorderSide(color: addCreditSubsidyChangeNotifier.cuttingValueDakor ? Colors.purple : Colors.grey)),
                                                                  contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                                              ),
                                                              keyboardType: TextInputType.number,
                                                              textAlign: TextAlign.end,
                                                              inputFormatters: [
                                                                  DecimalTextInputFormatter(decimalRange: 2),
                                                                  addCreditSubsidyChangeNotifier.amountValidator
                                                              ],
                                                              onFieldSubmitted: (value){
                                                                  addCreditSubsidyChangeNotifier.formattingPrice();
                                                                  addCreditSubsidyChangeNotifier.getEffFlatIntereset(context);
                                                              },
                                                              onTap: (){
                                                                  addCreditSubsidyChangeNotifier.formattingPrice();
                                                              },
                                                              autovalidate: addCreditSubsidyChangeNotifier.autoValidate,
                                                              validator: (e) {
                                                                  if (e.isEmpty && addCreditSubsidyChangeNotifier.isValueMandatory()) {
                                                                      return "Tidak boleh kosong";
                                                                  } else {
                                                                      return null;
                                                                  }
                                                              },
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                    Visibility(
                                                      visible: addCreditSubsidyChangeNotifier.isValueVisible(),
                                                      child: Visibility(
                                                          visible: addCreditSubsidyChangeNotifier.typeSelected == null ? false : addCreditSubsidyChangeNotifier.radioValueGiver == "0" ? false : addCreditSubsidyChangeNotifier.typeSelected == null ? true : addCreditSubsidyChangeNotifier.typeSelected.id == "06" ? false : true,
                                                          child: SizedBox(height: _size.height / 47)
                                                      ),
                                                    ),
                                                    Visibility(
                                                      visible: addCreditSubsidyChangeNotifier.isClaimValueVisible(),
                                                      child: Visibility(
                                                          visible: addCreditSubsidyChangeNotifier.typeSelected == null ? false : true,
                                                          child: IgnorePointer(
                                                            ignoring: addCreditSubsidyChangeNotifier.isDisablePACIAAOSCONA,
                                                            child: TextFormField(
                                                                enabled: addCreditSubsidyChangeNotifier.radioValueGiver == "0" ?
                                                                true : addCreditSubsidyChangeNotifier.typeSelected == null ? false : addCreditSubsidyChangeNotifier.typeSelected.id == "06" ?
                                                                true : addCreditSubsidyChangeNotifier.cuttingMethodSelected == null ?
                                                                false : addCreditSubsidyChangeNotifier.cuttingMethodSelected.id == "02" || addCreditSubsidyChangeNotifier.cuttingMethodSelected.id == "03" ?
                                                                true : false,
                                                                controller: addCreditSubsidyChangeNotifier.controllerClaimValue,
                                                                style: TextStyle(color: Colors.black),
                                                                decoration: InputDecoration(
                                                                    labelText: 'Nilai Klaim',
                                                                    labelStyle: TextStyle(color: Colors.black),
                                                                    filled: addCreditSubsidyChangeNotifier.isDisablePACIAAOSCONA ? true : addCreditSubsidyChangeNotifier.radioValueGiver == "0" ?
                                                                    false : addCreditSubsidyChangeNotifier.typeSelected == null ? false : addCreditSubsidyChangeNotifier.typeSelected.id == "06" ?
                                                                    false : addCreditSubsidyChangeNotifier.cuttingMethodSelected == null ?
                                                                    true : addCreditSubsidyChangeNotifier.cuttingMethodSelected.id == "02" || addCreditSubsidyChangeNotifier.cuttingMethodSelected.id == "03" ?
                                                                    false : true,
                                                                    fillColor: addCreditSubsidyChangeNotifier.radioValueGiver == "0" ?
                                                                    null : addCreditSubsidyChangeNotifier.typeSelected == null ? Colors.black12 : addCreditSubsidyChangeNotifier.typeSelected.id == "06" ?
                                                                    null : addCreditSubsidyChangeNotifier.cuttingMethodSelected == null ?
                                                                    Colors.black12 : addCreditSubsidyChangeNotifier.cuttingMethodSelected.id == "02" || addCreditSubsidyChangeNotifier.cuttingMethodSelected.id == "03" ?
                                                                    null : Colors.black12,
                                                                    border: OutlineInputBorder(
                                                                        borderRadius: BorderRadius.circular(8)),
                                                                    enabledBorder: OutlineInputBorder(
                                                                        borderSide: BorderSide(color: addCreditSubsidyChangeNotifier.claimValueDakor ? Colors.purple : Colors.grey)),
                                                                    disabledBorder: OutlineInputBorder(
                                                                        borderSide: BorderSide(color: addCreditSubsidyChangeNotifier.claimValueDakor ? Colors.purple : Colors.grey)),
                                                                    contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                                                ),
                                                                keyboardType: TextInputType.number,
                                                                textAlign: TextAlign.end,
                                                                inputFormatters: [
                                                                    DecimalTextInputFormatter(decimalRange: 2),
                                                                    addCreditSubsidyChangeNotifier.amountValidator
                                                                ],
                                                                onFieldSubmitted: (value){
                                                                    addCreditSubsidyChangeNotifier.formattingPrice();
                                                                    addCreditSubsidyChangeNotifier.getEffFlatIntereset(context);
                                                                },
                                                                onTap: (){
                                                                    addCreditSubsidyChangeNotifier.formattingPrice();
                                                                },
                                                                 autovalidate: addCreditSubsidyChangeNotifier.autoValidate,
                                                                 validator: (e) {
                                                                     if (e.isEmpty && addCreditSubsidyChangeNotifier.isClaimValueMandatory()) {
                                                                         return "Tidak boleh kosong";
                                                                     } else {
                                                                         return null;
                                                                     }
                                                                 },
                                                            ),
                                                          ),
                                                      ),
                                                    ),
                                                    Visibility(
                                                        visible: addCreditSubsidyChangeNotifier.typeSelected == null ? false : true,
                                                        child: SizedBox(height: _size.height / 47)
                                                    ),
                                                    Visibility(
                                                      visible: addCreditSubsidyChangeNotifier.isRateBeforeEffVisible(),
                                                      child: Visibility(
                                                          visible: addCreditSubsidyChangeNotifier.typeSelected == null ? false : addCreditSubsidyChangeNotifier.radioValueGiver == "0" ? false : addCreditSubsidyChangeNotifier.typeSelected == null ? true : addCreditSubsidyChangeNotifier.typeSelected.id == "06" ? false : true,
                                                          child: IgnorePointer(
                                                            ignoring: addCreditSubsidyChangeNotifier.isDisablePACIAAOSCONA,
                                                            child: TextFormField(
                                                                enabled: false,
                                                                controller: addCreditSubsidyChangeNotifier.controllerRateBeforeEff,
                                                                style: TextStyle(color: Colors.black),
                                                                decoration: InputDecoration(
                                                                    labelText: 'Suku Bunga Sebelum (Eff)',
                                                                    labelStyle: TextStyle(color: Colors.black),
                                                                    filled: true,
                                                                    fillColor: Colors.black12,
                                                                    border: OutlineInputBorder(
                                                                        borderRadius: BorderRadius.circular(8)),
                                                                    enabledBorder: OutlineInputBorder(
                                                                        borderSide: BorderSide(color: addCreditSubsidyChangeNotifier.rateBeforeEffDakor ? Colors.purple : Colors.grey)),
                                                                    disabledBorder: OutlineInputBorder(
                                                                        borderSide: BorderSide(color: addCreditSubsidyChangeNotifier.rateBeforeEffDakor ? Colors.purple : Colors.grey)),
                                                                    contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                                                ),
                                                                keyboardType: TextInputType.number,
                                                                inputFormatters: [
                                                                    WhitelistingTextInputFormatter.digitsOnly
                                                                ],
                                                                onTap: (){
                                                                    addCreditSubsidyChangeNotifier.formattingPrice();
                                                                },
                                                                textCapitalization: TextCapitalization.characters,
                                                                autovalidate: addCreditSubsidyChangeNotifier.autoValidate,
                                                                validator: (e) {
                                                                    if (e.isEmpty && addCreditSubsidyChangeNotifier.isRateBeforeEffMandatory()) {
                                                                        return "Tidak boleh kosong";
                                                                    } else {
                                                                        return null;
                                                                    }
                                                                },
                                                            ),
                                                          ),
                                                      ),
                                                    ),
                                                    Visibility(
                                                      visible: addCreditSubsidyChangeNotifier.isRateBeforeEffVisible(),
                                                      child: Visibility(
                                                          visible: addCreditSubsidyChangeNotifier.typeSelected == null ? false : addCreditSubsidyChangeNotifier.radioValueGiver == "0" ? false : addCreditSubsidyChangeNotifier.typeSelected == null ? true : addCreditSubsidyChangeNotifier.typeSelected.id == "06" ? false : true,
                                                          child: SizedBox(height: _size.height / 47)
                                                      ),
                                                    ),
                                                    Visibility(
                                                      visible: addCreditSubsidyChangeNotifier.isRateBeforeFlatVisible(),
                                                      child: Visibility(
                                                          visible: addCreditSubsidyChangeNotifier.typeSelected == null ? false : addCreditSubsidyChangeNotifier.radioValueGiver == "0" ? false : addCreditSubsidyChangeNotifier.typeSelected == null ? true : addCreditSubsidyChangeNotifier.typeSelected.id == "06" ? false : true,
                                                          child: IgnorePointer(
                                                            ignoring: addCreditSubsidyChangeNotifier.isDisablePACIAAOSCONA,
                                                            child: TextFormField(
                                                                enabled: false,
                                                                controller: addCreditSubsidyChangeNotifier.controllerRateBeforeFlat,
                                                                style: TextStyle(color: Colors.black),
                                                                decoration: InputDecoration(
                                                                    labelText: 'Suku Bunga Sebelum (Flat)',
                                                                    labelStyle: TextStyle(color: Colors.black),
                                                                    filled: true,
                                                                    fillColor: Colors.black12,
                                                                    border: OutlineInputBorder(
                                                                        borderRadius: BorderRadius.circular(8)),
                                                                    enabledBorder: OutlineInputBorder(
                                                                        borderSide: BorderSide(color: addCreditSubsidyChangeNotifier.rateBeforeFlatDakor ? Colors.purple : Colors.grey)),
                                                                    disabledBorder: OutlineInputBorder(
                                                                        borderSide: BorderSide(color: addCreditSubsidyChangeNotifier.rateBeforeFlatDakor ? Colors.purple : Colors.grey)),
                                                                    contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                                                ),
                                                                keyboardType: TextInputType.number,
                                                                inputFormatters: [
                                                                    WhitelistingTextInputFormatter.digitsOnly
                                                                ],
                                                                textCapitalization: TextCapitalization.characters,
                                                                autovalidate:
                                                                addCreditSubsidyChangeNotifier.autoValidate,
                                                                validator: (e) {
                                                                    if (e.isEmpty && addCreditSubsidyChangeNotifier.isRateBeforeFlatMandatory()) {
                                                                        return "Tidak boleh kosong";
                                                                    } else {
                                                                        return null;
                                                                    }
                                                                },
                                                            ),
                                                          ),
                                                      ),
                                                    ),
                                                    Visibility(
                                                      visible: addCreditSubsidyChangeNotifier.isRateBeforeFlatVisible(),
                                                      child: Visibility(
                                                          visible: addCreditSubsidyChangeNotifier.typeSelected == null ? false : addCreditSubsidyChangeNotifier.radioValueGiver == "0" ? false : addCreditSubsidyChangeNotifier.typeSelected == null ? true : addCreditSubsidyChangeNotifier.typeSelected.id == "06" ? false : true,
                                                          child: SizedBox(height: _size.height / 47)
                                                      ),
                                                    ),
                                                    Visibility(
                                                      visible: addCreditSubsidyChangeNotifier.isTotalInstallmentVisible(),
                                                      child: Visibility(
                                                          visible: addCreditSubsidyChangeNotifier.typeSelected == null ? false : addCreditSubsidyChangeNotifier.typeSelected.id == "05" ? true : false,
                                                          child: IgnorePointer(
                                                            ignoring: addCreditSubsidyChangeNotifier.isDisablePACIAAOSCONA,
                                                            child: TextFormField(
                                                                controller: addCreditSubsidyChangeNotifier.controllerTotalInstallment,
                                                                style: TextStyle(color: Colors.black),
                                                                decoration: InputDecoration(
                                                                    labelText: 'Jumlah Angsuran',
                                                                    labelStyle: TextStyle(color: Colors.black),
                                                                    filled: addCreditSubsidyChangeNotifier.isDisablePACIAAOSCONA,
                                                                    fillColor: Colors.black12,
                                                                    border: OutlineInputBorder(
                                                                        borderRadius: BorderRadius.circular(8)),
                                                                    enabledBorder: OutlineInputBorder(
                                                                        borderSide: BorderSide(color: addCreditSubsidyChangeNotifier.totalInstallmentDakor ? Colors.purple : Colors.grey)),
                                                                    disabledBorder: OutlineInputBorder(
                                                                        borderSide: BorderSide(color: addCreditSubsidyChangeNotifier.totalInstallmentDakor ? Colors.purple : Colors.grey)),
                                                                    contentPadding: EdgeInsets.symmetric(horizontal: 10),),
                                                                keyboardType: TextInputType.number,
                                                                textAlign: TextAlign.end,
                                                                inputFormatters: [
                                                                    DecimalTextInputFormatter(decimalRange: 2),
                                                                    addCreditSubsidyChangeNotifier.amountValidator
                                                                ],
                                                                onFieldSubmitted: (value){
                                                                    addCreditSubsidyChangeNotifier.formattingPrice();
                                                                },
                                                                onTap: (){
                                                                    addCreditSubsidyChangeNotifier.formattingPrice();
                                                                },
                                                                autovalidate: addCreditSubsidyChangeNotifier.autoValidate,
                                                                validator: (e) {
                                                                    if (e.isEmpty && addCreditSubsidyChangeNotifier.isTotalInstallmentMandatory()) {
                                                                        return "Tidak boleh kosong";
                                                                    } else {
                                                                        return null;
                                                                    }
                                                                },
                                                            ),
                                                          ),
                                                      ),
                                                    ),
                                                    Visibility(
                                                        visible: addCreditSubsidyChangeNotifier.typeSelected == null ? false : addCreditSubsidyChangeNotifier.typeSelected.id == "05" ? true : false,
                                                        child: SizedBox(height: _size.height / 47)
                                                    ),
                                                    Visibility(
                                                        visible: addCreditSubsidyChangeNotifier.typeSelected == null ? false : addCreditSubsidyChangeNotifier.typeSelected.id == "05" ? true : false,
                                                        child: Row(
                                                            children: [
                                                                Expanded(
                                                                    flex: 4,
                                                                    child: Visibility(
                                                                      visible: addCreditSubsidyChangeNotifier.isInstallmentIndexSelectedVisible(),
                                                                      child: IgnorePointer(
                                                                        ignoring: addCreditSubsidyChangeNotifier.isDisablePACIAAOSCONA,
                                                                        child: DropdownButtonFormField<String>(
                                                                            autovalidate: addCreditSubsidyChangeNotifier.autoValidate,
                                                                            validator: (e) {
                                                                                if (e == null && addCreditSubsidyChangeNotifier.isInstallmentIndexSelectedMandatory()) {
                                                                                    return "Tidak boleh kosong";
                                                                                } else {
                                                                                    return null;
                                                                                }
                                                                            },
                                                                            value: addCreditSubsidyChangeNotifier.installmentIndexSelected,
                                                                            onChanged: (value) {
                                                                                addCreditSubsidyChangeNotifier.installmentIndexSelected = value;
                                                                            },
                                                                            onTap: (){
                                                                                addCreditSubsidyChangeNotifier.formattingPrice();
                                                                            },
                                                                            decoration: InputDecoration(
                                                                                labelText: "Angsuran Ke",
                                                                                filled: addCreditSubsidyChangeNotifier.isDisablePACIAAOSCONA,
                                                                                fillColor: Colors.black12,
                                                                                border: OutlineInputBorder(),
                                                                                contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                                                            ),
                                                                            items: addCreditSubsidyChangeNotifier.listInstallmentIndexSelected.map((value) {
                                                                                return DropdownMenuItem<String>(
                                                                                    value: value,
                                                                                    child: Text(
                                                                                        value,
                                                                                        overflow: TextOverflow.ellipsis,
                                                                                    ),
                                                                                );
                                                                            }).toList(),
                                                                        ),
                                                                      ),
                                                                    ),
                                                                ),
                                                                SizedBox(width: _size.height / 47),
                                                                Expanded(
                                                                    flex: 1,
                                                                    child: RaisedButton(
                                                                        onPressed: (){
                                                                            addCreditSubsidyChangeNotifier.isDisablePACIAAOSCONA
                                                                            ? null
                                                                            : addCreditSubsidyChangeNotifier.addDetailInstallment(context);
                                                                        },
                                                                        shape: RoundedRectangleBorder(
                                                                            borderRadius: new BorderRadius.circular(8.0)),
                                                                        color: myPrimaryColor,
                                                                        child: Text("Add"),
                                                                    ),
                                                                )
                                                            ],
                                                        ),
                                                    ),
                                                    Visibility(
                                                        visible: addCreditSubsidyChangeNotifier.typeSelected == null ? false : addCreditSubsidyChangeNotifier.typeSelected.id == "05" ? true : false,
                                                        child: SizedBox(height: _size.height / 47)
                                                    ),
                                                    Visibility(
                                                        visible: addCreditSubsidyChangeNotifier.typeSelected == null ? false : addCreditSubsidyChangeNotifier.typeSelected.id == "05" ? true : false,
                                                        child: Row(
                                                            children: [
                                                                Expanded(
                                                                    flex: 2,
                                                                    child: Text("Angsuran Ke "),
                                                                ),
                                                                SizedBox(width: _size.height / 47),
                                                                Expanded(
                                                                    flex: 2,
                                                                    child: Text("Subsidi Angsuran "),
                                                                ),
                                                                SizedBox(width: _size.height / 47),
                                                                Expanded(
                                                                    flex: 1,
                                                                    child: Text(""),
                                                                ),
                                                            ],
                                                        ),
                                                    ),
                                                    Visibility(
                                                        visible: addCreditSubsidyChangeNotifier.typeSelected == null ? false : addCreditSubsidyChangeNotifier.typeSelected.id == "05" ? true : false,
                                                        child: Divider(color: Colors.black)
                                                    ),
                                                    Visibility(
                                                        visible: addCreditSubsidyChangeNotifier.typeSelected == null ? false : addCreditSubsidyChangeNotifier.typeSelected.id == "05" ? true : false,
                                                        child: Container(
                                                            margin: EdgeInsets.only(
                                                                bottom: MediaQuery.of(context).size.height/77),
                                                            child: Column(
                                                                crossAxisAlignment:CrossAxisAlignment.center,
                                                                children: listDetail()),
                                                        ),
                                                    ),
                                                ],
                                            ),
                                        );
                                },
                            );
                        }),
                ),
                bottomNavigationBar: BottomAppBar(
                    elevation: 0.0,
                    child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Consumer<AddCreditSubsidyChangeNotifier>(
                            builder: (context, addCreditSubsidyChangeNotifier, _) {
                                return RaisedButton(
                                    shape: RoundedRectangleBorder(
                                        borderRadius: new BorderRadius.circular(8.0)),
                                    color: myPrimaryColor,
                                    onPressed: () {
                                        if (widget.flag == 0) {
                                            addCreditSubsidyChangeNotifier.check(
                                                context, widget.flag, null);
                                        } else {
                                            addCreditSubsidyChangeNotifier.check(
                                                context, widget.flag, widget.index);
                                        }
                                    },
                                    child: Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: <Widget>[
                                            Text(widget.flag == 0 ? "SAVE" : "UPDATE",
                                                style: TextStyle(
                                                    color: Colors.black,
                                                    fontSize: 14,
                                                    fontWeight: FontWeight.w500,
                                                    letterSpacing: 1.25))
                                        ],
                                    ));
                            },
                        )),
                ),
            ),
        );
    }

    List<Widget> listDetail(){
        var _size = MediaQuery.of(context).size;
        List<Widget> _temp = [];
        var _providerCreditSubsidy = Provider.of<AddCreditSubsidyChangeNotifier>(context, listen: false);
        print("list detail ${_providerCreditSubsidy.listDetail.length}");
        if(_providerCreditSubsidy.listDetail.length != 0) {
            for(var i=0; i<_providerCreditSubsidy.listDetail.length; i++){
                _temp.add(
                    Column(
                        children: [
                            Row(
                                children: [
                                    Expanded(
                                        flex: 2,
                                        child: Text(_providerCreditSubsidy.listDetail[i].installmentIndex),
                                    ),
                                    SizedBox(width: _size.height / 47),
                                    Expanded(
                                        flex: 2,
                                        child: Text(_providerCreditSubsidy.formatCurrency.formatCurrency(_providerCreditSubsidy.listDetail[i].installmentSubsidy)),
                                    ),
                                    SizedBox(width: _size.height / 47),
                                    Expanded(
                                        flex: 1,
                                        child: IconButton(
                                            icon: Icon(Icons.delete, color: Colors.redAccent),
                                            onPressed: () {
                                                _providerCreditSubsidy.deleteIndex(context, i);
                                            }),
                                    ),
                                ],
                            ),
                            Divider(color: Colors.black)
                        ],
                    ),
                );
            }
        }
        return _temp;
    }

    Future<bool> _onWillPop() async {
        var _provider = Provider.of<AddCreditSubsidyChangeNotifier>(context, listen: false);
        if (widget.flag == 0) {
            if (_provider.radioValueGiver != "0" ||
                _provider.typeSelected != null ||
                _provider.cuttingMethodSelected != null ||
                _provider.controllerValue.text != "") {
                return (await showDialog(
                    context: context,
                    builder: (myContext) => AlertDialog(
                        title: new Text('Warning'),
                        content: new Text('Simpan perubahan?'),
                        actions: <Widget>[
                            new FlatButton(
                                onPressed: () {
                                    Navigator.pop(context);
                                    widget.flag == 0
                                        ?
                                    _provider.check(context, widget.flag, null)
                                        :
                                    _provider.check(context, widget.flag, widget.index);
                                },
                                child: new Text('Ya',
                                    style: TextStyle(
                                        color: Colors.grey,
                                        fontSize: 14,
                                        fontWeight: FontWeight.w500,
                                        letterSpacing: 1.25
                                    )),
                            ),
                            new FlatButton(
                                onPressed: () => Navigator.of(context).pop(true),
                                child: new Text('Tidak'),
                            ),
                        ],
                    ),
                )) ??
                    false;
            } else {
                return true;
            }
        }
        else {
            bool _type = widget.formMInfoCreditSubsidyModel.type != null ? widget.formMInfoCreditSubsidyModel.type.id != _provider.typeSelected.id : false;
            bool _cuttingMethod = widget.formMInfoCreditSubsidyModel.cuttingMethod != null ? widget.formMInfoCreditSubsidyModel.cuttingMethod.id != _provider.cuttingMethodSelected.id : false;

            if (widget.formMInfoCreditSubsidyModel.giver != _provider.radioValueGiver ||
                _type || _cuttingMethod ||
                widget.formMInfoCreditSubsidyModel.value != _provider.controllerValue.text ||
                widget.formMInfoCreditSubsidyModel.claimValue != _provider.controllerClaimValue.text) {
                return (await showDialog(
                    context: context,
                    builder: (myContext) => AlertDialog(
                        title: new Text('Warning'),
                        content: new Text('Simpan perubahan?'),
                        actions: <Widget>[
                            new FlatButton(
                                onPressed: () {
                                    Navigator.pop(context);
                                    _provider.check(context, widget.flag, widget.index);
                                },
                                child: new Text('Ya',
                                    style: TextStyle(
                                        color: Colors.grey,
                                        fontSize: 14,
                                        fontWeight: FontWeight.w500,
                                        letterSpacing: 1.25
                                    )),
                            ),
                            new FlatButton(
                                onPressed: () => Navigator.of(context).pop(true),
                                child: new Text('Tidak'),
                            ),
                        ],
                    ),
                )) ??
                    false;
            } else {
                return true;
            }
        }
    }
}
