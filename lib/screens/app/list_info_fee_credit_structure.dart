import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/screens/app/add_info_fee_credit_structure.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/add_info_fee_credit_stucture_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_credit_structure_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ListInfoFeeCreditStructure extends StatefulWidget {
  @override
  _ListInfoFeeCreditStructureState createState() => _ListInfoFeeCreditStructureState();
}

class _ListInfoFeeCreditStructureState extends State<ListInfoFeeCreditStructure> {
  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
        fontFamily: "NunitoSans",
        primaryColor: Colors.black,
        accentColor: myPrimaryColor,
      ),
      child: Scaffold(
        appBar: AppBar(
          title:
          Text("List Info Struktur Kredit Biaya", style: TextStyle(color: Colors.black)),
          centerTitle: true,
          backgroundColor: myPrimaryColor,
          iconTheme: IconThemeData(color: Colors.black),
        ),
        body: Consumer<InfoCreditStructureChangeNotifier>(
          builder: (context,infoCreditStructureChangeNotifier,_){
            return infoCreditStructureChangeNotifier.listInfoFeeCreditStructure.isEmpty
            ? Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  // Image.asset("img/alamat_kosong.png", height: MediaQuery.of(context).size.height / 9,),
                  // SizedBox(height: MediaQuery.of(context).size.height / 47,),
                  Text("Tambah Info Struktur Kredit Biaya", style: TextStyle(color: Colors.grey, fontSize: 16),)
                ],
              ),
            )
            : ListView.builder(
              padding: EdgeInsets.symmetric(
                horizontal: MediaQuery.of(context).size.width/37,
                vertical: MediaQuery.of(context).size.height/57,
              ),
              itemBuilder: (context, index) =>
                  InkWell(
                      onTap: (){
                        Navigator.push(
                            context, MaterialPageRoute(
                            builder: (context) => ChangeNotifierProvider(
                              create:  (context) => AddInfoFeeCreditStructureChangeNotifier(),
                              child: AddInfoFeeCreditStructure(
                                  flag: 1,
                                  index: index,
                                  model: infoCreditStructureChangeNotifier.listInfoFeeCreditStructure[index]
                              ),
                            ))
                        );
                      },
                      child: Card(
                        // shape: infoCreditSubsidyChangeNotifier.listInfoCreditSubsidy[index].isEditDakor
                        //     ? RoundedRectangleBorder(
                        //     side: BorderSide(color: Colors.purple, width: 2),
                        //     borderRadius: BorderRadius.all(Radius.circular(4)))
                        //     : null,
                        elevation: 3.3,
                        child: Stack(
                          children: [
                            Padding(
                              padding: EdgeInsets.all(13.0),
                              child: Column(
                                children: [
                                  Row(
                                    children: [
                                      Expanded(
                                          flex: 4,
                                          child: Text("Jenis Biaya")
                                      ),
                                      Text(" :  "),
                                      Expanded(
                                          flex: 6,
                                          child: Text(
                                              "${infoCreditStructureChangeNotifier.listInfoFeeCreditStructure[index].feeTypeModel.name}"
                                          )
                                      ),
                                    ],
                                  ),
                                  SizedBox(height: MediaQuery.of(context).size.height / 47),
                                  Row(
                                    children: [
                                      Expanded(
                                          flex: 4,
                                          child: Text("Biaya Tunai")
                                      ),
                                      Text(" :  "),
                                      Expanded(
                                          flex: 6,
                                          child: Text(infoCreditStructureChangeNotifier.listInfoFeeCreditStructure[index].cashCost != null ?
                                          infoCreditStructureChangeNotifier.formatCurrency.formatCurrency(infoCreditStructureChangeNotifier.listInfoFeeCreditStructure[index].cashCost) : "0.00")
                                      ),
                                    ],
                                  ),
                                  SizedBox(height: MediaQuery.of(context).size.height / 47),
                                  Row(
                                    children: [
                                      Expanded(
                                          flex: 4,
                                          child: Text("Biaya Kredit")
                                      ),
                                      Text(" :  "),
                                      Expanded(
                                          flex: 6,
                                          child: Text(infoCreditStructureChangeNotifier.listInfoFeeCreditStructure[index].creditCost != null ?
                                          infoCreditStructureChangeNotifier.formatCurrency.formatCurrency(infoCreditStructureChangeNotifier.listInfoFeeCreditStructure[index].creditCost) : "0.00")
                                      ),
                                    ],
                                  ),
                                  SizedBox(height: MediaQuery.of(context).size.height / 47),
                                  Row(
                                    children: [
                                      Expanded(
                                          flex: 4,
                                          child: Text("Total Biaya")
                                      ),
                                      Text(" :  "),
                                      Expanded(
                                          flex: 6,
                                          child: Text(infoCreditStructureChangeNotifier.listInfoFeeCreditStructure[index].totalCost != null ?
                                          infoCreditStructureChangeNotifier.formatCurrency.formatCurrency(infoCreditStructureChangeNotifier.listInfoFeeCreditStructure[index].totalCost) : "0.00")
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                            infoCreditStructureChangeNotifier.lastKnownState == "IDE" ?
                            Align(
                              alignment: Alignment.topRight,
                              child: IconButton(
                                  icon: Icon(Icons.delete, color: Colors.red),
                                  onPressed: () {
                                    infoCreditStructureChangeNotifier.deleteListInfoFeeCreditStructure(context, index);
                                  }),
                            )
                            :
                            SizedBox(),
                          ],
                        ),
                      )
                  ),
              itemCount: infoCreditStructureChangeNotifier.listInfoFeeCreditStructure.length,
            );
          },
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: (){
            Navigator.push(
                context, MaterialPageRoute(
                builder: (context) => ChangeNotifierProvider(
                    create:  (context) => AddInfoFeeCreditStructureChangeNotifier(),
                  child: AddInfoFeeCreditStructure(flag: 0,index: null),
                ))
            );
          },
          child: Icon(Icons.add),
          backgroundColor: myPrimaryColor,
        ),
      ),
    );
  }
}
