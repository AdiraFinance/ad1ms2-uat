import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/form_m_foto_model.dart';
import 'package:ad1ms2_dev/models/type_of_financing_model.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_object_unit_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_foto_change_notif.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class InformationObjectUnit extends StatefulWidget {
  final String flag;

  const InformationObjectUnit({this.flag});
  @override
  _InformationObjectUnitState createState() => _InformationObjectUnitState();
}

class _InformationObjectUnitState extends State<InformationObjectUnit> {

  Future<void> _setPreference;

  @override
  void initState() {
    super.initState();
    // if(widget.flag == "COM"){
    //     if(Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false).listBusinessActivities.isEmpty){
    //         Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false).getBusinessActivities(widget.flag,context);
    //     }
    // }
    _setPreference = Provider.of<InformationObjectUnitChangeNotifier>(context, listen: false).setPreference(context);
    // Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false).getDataFromDashboard(context);
    // Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false).businessActivitiesModelSelected = null;
  }
  @override
  Widget build(BuildContext context) {
    print("cek matrix isVisible ${Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false).isMatriksDealerVisible()}");
    return Theme(
      data: ThemeData(
        fontFamily: "NunitoSans",
        primaryColor: Colors.black,
        primarySwatch: primaryOrange,
        accentColor: myPrimaryColor,
      ),
      child: Scaffold(
        key: Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false).scaffoldKey,
        appBar: AppBar(
            title:
            Text("Informasi Unit Detail/Objek", style: TextStyle(color: Colors.black)),
            centerTitle: true,
            backgroundColor: myPrimaryColor,
            iconTheme: IconThemeData(color: Colors.black),
        ),
        backgroundColor: Colors.white,
        body: FutureBuilder(
          future: _setPreference,
            builder: (context,snapshot){
              if(snapshot.connectionState == ConnectionState.waiting){
                return Center(
                  child: CircularProgressIndicator(),
                );
              }
              return Consumer<InformationObjectUnitChangeNotifier>(
                builder: (context, infoObjectUnit, _) {
                  return
                    //   infoObjectUnit.loadData
                    //     ?
                    // Center(child: CircularProgressIndicator(),)
                    //     :
                    Form(
                      key: infoObjectUnit.keyForm,
                      onWillPop: infoObjectUnit.onBackPress,
                      child: SingleChildScrollView(
                        padding: EdgeInsets.symmetric(
                            horizontal: MediaQuery.of(context).size.width / 27,
                            vertical: MediaQuery.of(context).size.height / 47
                        ),
                        child: Column(
                          children: [
                            Visibility(
                              visible: infoObjectUnit.isTypeOfFinancingModelVisible(),
                              child: IgnorePointer(
                                ignoring: infoObjectUnit.isDisablePACIAAOSCONA,
                                child: Visibility(
                                  visible: widget.flag == "COM",
                                  child: DropdownButtonFormField<FinancingTypeModel>(
                                    isExpanded: true,
                                    autovalidate: infoObjectUnit.autoValidate,
                                    validator: (e) {
                                      if (e == null && infoObjectUnit.isTypeOfFinancingModelMandatory()) {
                                        return "Tidak boleh kosong";
                                      } else {
                                        return null;
                                      }
                                    },
                                    value: infoObjectUnit.typeOfFinancingModelSelected,
                                    onChanged: (value) {
                                      infoObjectUnit.typeOfFinancingModelSelected = value;
                                      infoObjectUnit.businessActivitiesModelSelected = null;
                                      infoObjectUnit.getBusinessActivities(widget.flag, context);
                                    },
                                    onTap: () {
                                      FocusManager.instance.primaryFocus.unfocus();
                                    },
                                    decoration: InputDecoration(
                                      labelText: "Jenis Pembiayaan",
                                      labelStyle: TextStyle(color: Colors.black),
                                      filled: infoObjectUnit.isDisablePACIAAOSCONA,
                                      fillColor: Colors.black12,
                                      border: OutlineInputBorder(),
                                      enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: infoObjectUnit.isTypeOfFinancingModelChanges ? Colors.purple : Colors.grey)),
                                      disabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: infoObjectUnit.isTypeOfFinancingModelChanges ? Colors.purple : Colors.grey)),
                                      contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                    ),
                                    items: infoObjectUnit.listTypeOfFinancing.map((value) {
                                      return DropdownMenuItem<FinancingTypeModel>(
                                        value: value,
                                        child: Text(
                                          value.financingTypeName,
                                          overflow: TextOverflow.ellipsis,
                                        ),
                                      );
                                    }).toList(),
                                  ),
                                ),
                              ),
                            ),
                            Visibility(
                                visible: widget.flag == "COM",
                                child: SizedBox(height: MediaQuery.of(context).size.height / 47)
                            ),
                            Visibility(
                              visible: infoObjectUnit.isBusinessActivitiesModelVisible(),
                              child: Visibility(
                                visible: widget.flag == "COM",
                                child: IgnorePointer(
                                  ignoring: infoObjectUnit.isDisablePACIAAOSCONA,
                                  child: DropdownButtonFormField<KegiatanUsahaModel>(
                                    autovalidate: infoObjectUnit.autoValidate,
                                    validator: (e) {
                                      if (e == null && infoObjectUnit.isBusinessActivitiesModelMandatory()) {
                                        return "Tidak boleh kosong";
                                      } else {
                                        return null;
                                      }
                                    },
                                    value: infoObjectUnit.businessActivitiesModelSelected,
                                    onChanged: (value) {
                                      infoObjectUnit.businessActivitiesModelSelected = value;
                                      infoObjectUnit.getListBusinessActivitiesType(widget.flag, context);
                                    },
                                    onTap: () {
                                      FocusManager.instance.primaryFocus.unfocus();
                                    },
                                    decoration: InputDecoration(
                                      labelText: "Kegiatan Usaha",
                                      filled: infoObjectUnit.isDisablePACIAAOSCONA,
                                      fillColor: Colors.black12,
                                      border: OutlineInputBorder(),
                                      enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: infoObjectUnit.isBusinessActivitiesModelChanges ? Colors.purple : Colors.grey)),
                                      disabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: infoObjectUnit.isBusinessActivitiesModelChanges ? Colors.purple : Colors.grey)),
                                      contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                    ),
                                    items: infoObjectUnit.listBusinessActivities.map((value) {
                                      return DropdownMenuItem<KegiatanUsahaModel>(
                                        value: value,
                                        child: Text(
                                          value.text,
                                          overflow: TextOverflow.ellipsis,
                                        ),
                                      );
                                    }).toList(),
                                  ),
                                ),
                              ),
                            ),
                            Visibility(child: SizedBox(height: MediaQuery.of(context).size.height / 47),visible: widget.flag == "COM" ? true : false,),
                            Visibility(
                              visible: infoObjectUnit.isBusinessActivitiesTypeModelVisible(),
                              child: Visibility(
                                visible: widget.flag == "COM",
                                child: IgnorePointer(
                                  ignoring: infoObjectUnit.isDisablePACIAAOSCONA,
                                  child: DropdownButtonFormField<JenisKegiatanUsahaModel>(
                                    autovalidate: infoObjectUnit.autoValidate,
                                    isExpanded: true,
                                    validator: (e) {
                                      if (e == null && infoObjectUnit.isBusinessActivitiesTypeModelMandatory()) {
                                        return "Tidak boleh kosong";
                                      } else {
                                        return null;
                                      }
                                    },
                                    value: infoObjectUnit.businessActivitiesTypeModelSelected,
                                    onChanged: (value) {
                                      infoObjectUnit.businessActivitiesTypeModelSelected = value;
                                    },
                                    onTap: () {
                                      FocusManager.instance.primaryFocus.unfocus();
                                    },
                                    decoration: InputDecoration(
                                      labelText: "Jenis Kegiatan Usaha",
                                      filled: infoObjectUnit.isDisablePACIAAOSCONA,
                                      fillColor: Colors.black12,
                                      border: OutlineInputBorder(),
                                      enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: infoObjectUnit.isBusinessActivitiesTypeModelChanges ? Colors.purple : Colors.grey)),
                                      disabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: infoObjectUnit.isBusinessActivitiesTypeModelChanges ? Colors.purple : Colors.grey)),
                                      contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                    ),
                                    items: infoObjectUnit.listBusinessActivitiesType
                                        .map((value) {
                                      return DropdownMenuItem<JenisKegiatanUsahaModel>(
                                        value: value,
                                        child: Text(
                                          value.text,
                                          overflow: TextOverflow.ellipsis,
                                        ),
                                      );
                                    }).toList(),
                                  ),
                                ),
                              ),
                            ),
                            Visibility(child: SizedBox(height: MediaQuery.of(context).size.height / 47),visible: widget.flag == "COM" ? true : false,),
                            Visibility(
                              visible: infoObjectUnit.isGroupObjectVisible(),
                              child: IgnorePointer(
                                ignoring: infoObjectUnit.isDisablePACIAAOSCONA,
                                child: TextFormField(
                                    validator: (e) {
                                      if (e.isEmpty && infoObjectUnit.isGroupObjectMandatory()) {
                                        return "Tidak boleh kosong";
                                      } else {
                                        return null;
                                      }
                                    },
                                    readOnly: true,
                                    onTap: () {
                                      if(widget.flag == "COM"){
                                        if(infoObjectUnit.typeOfFinancingModelSelected != null){
                                          infoObjectUnit.searchGroupObject(context,widget.flag);
                                        }
                                      }
                                      else{
                                        infoObjectUnit.searchGroupObject(context,"PER");
                                      }
                                    },
                                    // enabled: widget.flag == "COM" ? infoObjectUnit.typeOfFinancingModelSelected != null ? true : false : true,
                                    controller: infoObjectUnit.controllerGroupObject,
                                    autovalidate: infoObjectUnit.autoValidate,
                                    decoration: new InputDecoration(
                                      labelText: 'Grup Objek',
                                      labelStyle: TextStyle(color: Colors.black),
                                      filled: infoObjectUnit.isDisablePACIAAOSCONA,
                                      fillColor: Colors.black12,
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(8)
                                      ),
                                      enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: infoObjectUnit.isGroupObjectChanges ? Colors.purple : Colors.grey)),
                                      disabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: infoObjectUnit.isGroupObjectChanges ? Colors.purple : Colors.grey)),
                                      contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                    )
                                ),
                              ),
                            ),
                            Visibility(
                                visible: infoObjectUnit.isGroupObjectVisible(),
                                child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                            Visibility(
                              visible: infoObjectUnit.isObjectVisible(),
                              child: IgnorePointer(
                                ignoring: infoObjectUnit.isDisablePACIAAOSCONA,
                                child: TextFormField(
                                    validator: (e) {
                                      if (e.isEmpty && infoObjectUnit.isObjectMandatory()) {
                                        return "Tidak boleh kosong";
                                      } else {
                                        return null;
                                      }
                                    },
                                    readOnly: true,
                                    onTap: infoObjectUnit.controllerGroupObject.text != ""
                                        ? () {
                                      infoObjectUnit.searchObject(context);
                                    }
                                        : null,
                                    controller: infoObjectUnit.controllerObject,
                                    autovalidate: infoObjectUnit.autoValidate,
                                    decoration: new InputDecoration(
                                      labelText: 'Objek',
                                      labelStyle: TextStyle(color: Colors.black),
                                      filled: infoObjectUnit.isDisablePACIAAOSCONA,
                                      fillColor: Colors.black12,
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(8)),
                                      enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: infoObjectUnit.isObjectChanges ? Colors.purple : Colors.grey)),
                                      disabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: infoObjectUnit.isObjectChanges ? Colors.purple : Colors.grey)),
                                      contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                    )
                                ),
                              ),
                            ),
                            Visibility(
                                visible: infoObjectUnit.isObjectVisible(),
                                child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                            Visibility(
                              visible: infoObjectUnit.isTypeProductVisible(),
                              child: IgnorePointer(
                                ignoring: infoObjectUnit.isDisablePACIAAOSCONA,
                                child: TextFormField(
                                    validator: (e) {
                                      if (e.isEmpty && infoObjectUnit.isTypeProductMandatory()) {
                                        return "Tidak boleh kosong";
                                      } else {
                                        return null;
                                      }
                                    },
                                    readOnly: true,
                                    onTap: infoObjectUnit.controllerGroupObject.text != ""
                                        ? (){infoObjectUnit.searchProductType(context,widget.flag);}
                                        : null,
                                    controller: infoObjectUnit.controllerTypeProduct,
                                    autovalidate: infoObjectUnit.autoValidate,
                                    decoration: new InputDecoration(
                                      labelText: 'Jenis Produk',
                                      labelStyle: TextStyle(color: Colors.black),
                                      filled: infoObjectUnit.isDisablePACIAAOSCONA,
                                      fillColor: Colors.black12,
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(8)),
                                      enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: infoObjectUnit.isTypeProductChanges ? Colors.purple : Colors.grey)),
                                      disabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: infoObjectUnit.isTypeProductChanges ? Colors.purple : Colors.grey)),
                                      contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                    )
                                ),
                              ),
                            ),
                            Visibility(
                                visible: infoObjectUnit.isTypeProductVisible(),
                                child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                            Visibility(
                              visible: infoObjectUnit.isBrandObjectVisible(),
                              child: IgnorePointer(
                                ignoring: infoObjectUnit.isDisablePACIAAOSCONA,
                                child: TextFormField(
                                    validator: (e) {
                                      if (e.isEmpty && infoObjectUnit.isBrandObjectMandatory()) {
                                        return "Tidak boleh kosong";
                                      } else {
                                        return null;
                                      }
                                    },
                                    readOnly: true,
                                    onTap: infoObjectUnit.controllerTypeProduct.text != ""
                                        ? () {infoObjectUnit.searchBrandObject(context,widget.flag,1);}
                                        : null,
                                    controller: infoObjectUnit.controllerBrandObject,
                                    autovalidate: infoObjectUnit.autoValidate,
                                    decoration: new InputDecoration(
                                      labelText: 'Merk Objek',
                                      labelStyle: TextStyle(color: Colors.black),
                                      filled: infoObjectUnit.isDisablePACIAAOSCONA,
                                      fillColor: Colors.black12,
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(8)),
                                      enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: infoObjectUnit.isBrandObjectChanges ? Colors.purple : Colors.grey)),
                                      disabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: infoObjectUnit.isBrandObjectChanges ? Colors.purple : Colors.grey)),
                                      contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                    )
                                ),
                              ),
                            ),
                            Visibility(
                                visible: infoObjectUnit.isBrandObjectVisible(),
                                child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                            Visibility(
                              visible: infoObjectUnit.isObjectTypeVisible(),
                              child: IgnorePointer(
                                ignoring: infoObjectUnit.isDisablePACIAAOSCONA,
                                child: TextFormField(
                                    validator: (e) {
                                      if (e.isEmpty && infoObjectUnit.isObjectTypeMandatory()) {
                                        return "Tidak boleh kosong";
                                      } else {
                                        return null;
                                      }
                                    },
                                    readOnly: true,
                                    onTap: () {
                                      infoObjectUnit.searchObjectType(context,widget.flag,2);
                                    },
                                    controller: infoObjectUnit.controllerObjectType,
                                    autovalidate: infoObjectUnit.autoValidate,
                                    decoration: new InputDecoration(
                                      labelText: 'Jenis Objek',
                                      labelStyle: TextStyle(color: Colors.black),
                                      filled: infoObjectUnit.isDisablePACIAAOSCONA,
                                      fillColor: Colors.black12,
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(8)),
                                      enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: infoObjectUnit.isObjectTypeChanges ? Colors.purple : Colors.grey)),
                                      disabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: infoObjectUnit.isObjectTypeChanges ? Colors.purple : Colors.grey)),
                                      contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                    )
                                ),
                              ),
                            ),
                            Visibility(
                                visible: infoObjectUnit.isObjectTypeVisible(),
                                child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                            Visibility(
                              visible: infoObjectUnit.isModelObjectVisible(),
                              child: IgnorePointer(
                                ignoring: infoObjectUnit.isDisablePACIAAOSCONA,
                                child: TextFormField(
                                    validator: (e) {
                                      if (e.isEmpty && infoObjectUnit.isModelObjectMandatory()) {
                                        return "Tidak boleh kosong";
                                      } else {
                                        return null;
                                      }
                                    },
                                    readOnly: true,
                                    onTap: () {
                                      infoObjectUnit.searchModelObject(context,widget.flag,3);
                                    },
                                    controller: infoObjectUnit.controllerModelObject,
                                    autovalidate: infoObjectUnit.autoValidate,
                                    decoration: new InputDecoration(
                                      labelText: 'Model Objek',
                                      labelStyle: TextStyle(color: Colors.black),
                                      filled: infoObjectUnit.isDisablePACIAAOSCONA,
                                      fillColor: Colors.black12,
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(8)),
                                      enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: infoObjectUnit.isModelObjectChanges ? Colors.purple : Colors.grey)),
                                      disabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: infoObjectUnit.isModelObjectChanges ? Colors.purple : Colors.grey)),
                                      contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                    )
                                ),
                              ),
                            ),
                            Visibility(
                                visible: infoObjectUnit.isModelObjectVisible(),
                                child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                            Visibility(
                              visible: infoObjectUnit.isDetailModelVisible(),
                              child: IgnorePointer(
                                ignoring: infoObjectUnit.isDisablePACIAAOSCONA,
                                child: TextFormField(
                                    autovalidate: infoObjectUnit.autoValidate,
                                    validator: (e) {
                                      if(e.isEmpty && infoObjectUnit.isDetailModelMandatory() && (infoObjectUnit.groupObjectSelected != null
                                      ? infoObjectUnit.groupObjectSelected.KODE == "003" : false)) {
                                        return "Tidak boleh kosong";
                                      } else {
                                        return null;
                                      }
                                    },
                                    enabled: infoObjectUnit.groupObjectSelected != null
                                    ? infoObjectUnit.groupObjectSelected.KODE == "003"
                                      ? true
                                      : false
                                    : false,
                                    controller: infoObjectUnit.controllerDetailModel,
                                    decoration: new InputDecoration(
                                        labelText: 'Model Detail',
                                        labelStyle: TextStyle(color: Colors.black),
                                        filled: infoObjectUnit.isDisablePACIAAOSCONA
                                            ? true
                                            : infoObjectUnit.groupObjectSelected != null ? infoObjectUnit.groupObjectSelected.KODE == "003" ? false : true : true,
                                        fillColor: Colors.black12,
                                        errorStyle: TextStyle(
                                          color: Theme.of(context).errorColor, // or any other color
                                        ),
                                        border: OutlineInputBorder(
                                            borderRadius: BorderRadius.circular(8)),
                                        enabledBorder: OutlineInputBorder(
                                            borderSide: BorderSide(color: infoObjectUnit.isDetailModelChanges ? Colors.purple : Colors.grey)),
                                        disabledBorder: OutlineInputBorder(
                                            borderSide: BorderSide(color: infoObjectUnit.isDetailModelChanges ? Colors.purple : Colors.grey)),
                                        contentPadding: EdgeInsets.symmetric(horizontal: 10),),
                                    keyboardType: TextInputType.text,
                                    textCapitalization: TextCapitalization.characters,
                                    inputFormatters: [
                                      FilteringTextInputFormatter.allow(RegExp('[a-zA-Z0-9 /]')),
                                    ],
                                ),
                              ),
                            ),
                            Visibility(
                                visible: infoObjectUnit.isDetailModelVisible(),
                                child: SizedBox(height: MediaQuery.of(context).size.height / 47)
                            ),
                            Visibility(
                              visible: infoObjectUnit.isUsageObjectModelVisible(),
                              child: IgnorePointer(
                                ignoring: infoObjectUnit.isDisablePACIAAOSCONA,
                                child: TextFormField(
                                    validator: (e) {
                                      if (e.isEmpty && infoObjectUnit.isUsageObjectModelMandatory()) {
                                        return "Tidak boleh kosong";
                                      } else {
                                        return null;
                                      }
                                    },
                                    readOnly: true,
                                    onTap: (){
                                      infoObjectUnit.searchUsageObject(context);
                                    },
                                    enabled: false,
                                    controller: infoObjectUnit.controllerUsageObjectModel,
                                    autovalidate: infoObjectUnit.autoValidate,
                                    decoration: InputDecoration(
                                      labelText: 'Pemakaian Objek',
                                      labelStyle: TextStyle(color: Colors.black),
                                      filled: true,
                                      fillColor: Colors.black12,
                                      errorStyle: TextStyle(
                                        color: Theme.of(context).errorColor, // or any other color
                                      ),
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(8)),
                                      enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: infoObjectUnit.isUsageObjectModelChanges ? Colors.purple : Colors.grey)),
                                      disabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: infoObjectUnit.isUsageObjectModelChanges ? Colors.purple : Colors.grey)),
                                      contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                    )
                                ),
                              ),
                            ),
                            Visibility(
                                visible: infoObjectUnit.isUsageObjectModelVisible(),
                                child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                            Visibility(
                              visible: infoObjectUnit.isObjectPurposeVisible(),
                              child: IgnorePointer(
                                ignoring: infoObjectUnit.isDisablePACIAAOSCONA,
                                child: TextFormField(
                                    validator: (e) {
                                      if (e.isEmpty && infoObjectUnit.isObjectPurposeMandatory()) {
                                        return "Tidak boleh kosong";
                                      } else {
                                        return null;
                                      }
                                    },
                                    readOnly: true,
                                    onTap: () {
                                      infoObjectUnit.searchObjectPurpose(widget.flag, context);
                                    },
                                    controller: infoObjectUnit.controllerObjectPurpose,
                                    autovalidate: infoObjectUnit.autoValidate,
                                    decoration: new InputDecoration(
                                      labelText: 'Tujuan Penggunaan Objek',
                                      labelStyle: TextStyle(color: Colors.black),
                                      filled: infoObjectUnit.isDisablePACIAAOSCONA,
                                      fillColor: Colors.black12,
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(8)),
                                      enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: infoObjectUnit.isObjectPurposeChanges ? Colors.purple : Colors.grey)),
                                      disabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: infoObjectUnit.isObjectPurposeChanges ? Colors.purple : Colors.grey)),
                                      contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                    )
                                ),
                              ),
                            ),
                            Visibility(
                                visible: infoObjectUnit.isObjectPurposeVisible(),
                                child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                            Visibility(
                              visible: infoObjectUnit.isGroupSalesVisible(),
                              child: IgnorePointer(
                                ignoring: infoObjectUnit.isDisablePACIAAOSCONA,
                                child: TextFormField(
                                    autovalidate: infoObjectUnit.autoValidate,
                                    validator: (e) {
                                      if (e.isEmpty && infoObjectUnit.isGroupSalesMandatory()) {
                                        return "Tidak boleh kosong";
                                      } else {
                                        return null;
                                      }
                                    },
                                    readOnly: true,
                                    onTap: () {
                                      infoObjectUnit.searchGroupSales(context);
                                    },
                                    controller: infoObjectUnit.controllerGroupSales,
                                    decoration: new InputDecoration(
                                      labelText: 'Grup Sales',
                                      labelStyle: TextStyle(color: Colors.black),
                                      filled: infoObjectUnit.isDisablePACIAAOSCONA,
                                      fillColor: Colors.black12,
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(8)),
                                      enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: infoObjectUnit.isGroupSalesChanges ? Colors.purple : Colors.grey)),
                                      disabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: infoObjectUnit.isGroupSalesChanges ? Colors.purple : Colors.grey)),
                                      contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                    )
                                ),
                              ),
                            ),
                            Visibility(
                                visible: infoObjectUnit.isGroupSalesVisible(),
                                child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                            Visibility(
                              visible: infoObjectUnit.isGrupIdVisible(),
                              child: IgnorePointer(
                                ignoring: infoObjectUnit.isDisablePACIAAOSCONA,
                                child: TextFormField(
                                    autovalidate: infoObjectUnit.autoValidate,
                                    validator: (e) {
                                      if (e.isEmpty && infoObjectUnit.isGrupIdMandatory()) {
                                        if(infoObjectUnit.groupSalesSelected != null) {
                                          if(infoObjectUnit.groupSalesSelected.kode == "0000Y") {
                                            return"Tidak boleh kosong";
                                          } else {
                                            return null;
                                          }
                                        } else {
                                          return null;
                                        }
                                      } else {
                                        return null;
                                      }
                                    },
                                    enabled: infoObjectUnit.groupSalesSelected != null ? infoObjectUnit.groupSalesSelected.kode == "0000Y" ? true : false : false,
                                    readOnly: true,
                                    onTap: () {
                                      infoObjectUnit.searchGrupId(context);
                                    },
                                    controller: infoObjectUnit.controllerGrupId,
                                    decoration: new InputDecoration(
                                      labelText: 'Grup ID',
                                      labelStyle: TextStyle(color: Colors.black),
                                      filled: infoObjectUnit.isDisablePACIAAOSCONA ? true : infoObjectUnit.groupSalesSelected != null ? infoObjectUnit.groupSalesSelected.kode == "0000Y" ? false : true : true,
                                      fillColor: Colors.black12,
                                      errorStyle: TextStyle(
                                        color: Theme.of(context).errorColor, // or any other color
                                      ),
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(8)),
                                      enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: infoObjectUnit.isGrupIdChanges ? Colors.purple : Colors.grey)),
                                      disabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: infoObjectUnit.isGrupIdChanges ? Colors.purple : Colors.grey)),
                                      contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                    )
                                ),
                              ),
                            ),
                            Visibility(
                                visible: infoObjectUnit.isGrupIdVisible(),
                                child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                            Visibility(
                              visible: infoObjectUnit.isSourceOrderVisible(),
                              child: IgnorePointer(
                                ignoring: infoObjectUnit.isDisablePACIAAOSCONA,
                                child: TextFormField(
                                    autovalidate: infoObjectUnit.autoValidate,
                                    validator: (e) {
                                      if (e.isEmpty && infoObjectUnit.isSourceOrderMandatory()) {
                                        return "Tidak boleh kosong";
                                      } else {
                                        return null;
                                      }
                                    },
                                    readOnly: true,
                                    onTap: () {
                                      infoObjectUnit.searchSourceOrder(context);
                                    },
                                    controller: infoObjectUnit.controllerSourceOrder,
                                    decoration: new InputDecoration(
                                      labelText: 'Sumber Order',
                                      labelStyle: TextStyle(color: Colors.black),
                                      filled: infoObjectUnit.isDisablePACIAAOSCONA,
                                      fillColor: Colors.black12,
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(8)),
                                      enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: infoObjectUnit.isSourceOrderChanges ? Colors.purple : Colors.grey)),
                                      disabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: infoObjectUnit.isSourceOrderChanges ? Colors.purple : Colors.grey)),
                                      contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                    )
                                ),
                              ),
                            ),
                            Visibility(
                                visible: infoObjectUnit.isSourceOrderVisible(),
                                child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                            Visibility(
                              visible: infoObjectUnit.isSourceOrderNameVisible(),
                              child: IgnorePointer(
                                ignoring: infoObjectUnit.isDisablePACIAAOSCONA,
                                child: TextFormField(
                                    validator: (e) {
                                      if (e.isEmpty && infoObjectUnit.isSourceOrderNameMandatory()) {
                                        return "Tidak boleh kosong";
                                      } else {
                                        return null;
                                      }
                                    },
                                    readOnly: true,
                                    onTap: () {
                                      infoObjectUnit.searchSourceOrderName(context);
                                    },
                                    controller: infoObjectUnit.controllerSourceOrderName,
                                    autovalidate: infoObjectUnit.autoValidate,
                                    decoration: new InputDecoration(
                                      labelText: 'Nama Sumber Order',
                                      labelStyle: TextStyle(color: Colors.black),
                                      filled: infoObjectUnit.isDisablePACIAAOSCONA,
                                      fillColor: Colors.black12,
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(8)),
                                      enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: infoObjectUnit.isSourceOrderNameChanges ? Colors.purple : Colors.grey)),
                                      disabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: infoObjectUnit.isSourceOrderNameChanges ? Colors.purple : Colors.grey)),
                                      contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                    )
                                ),
                              ),
                            ),
                            Visibility(
                                visible: infoObjectUnit.isSourceOrderNameVisible(),
                                child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                            Visibility(
                              visible: infoObjectUnit.isThirdPartyTypeVisible(),
                              child: IgnorePointer(
                                ignoring: infoObjectUnit.isDisablePACIAAOSCONA,
                                child: TextFormField(
                                    validator: (e) {
                                      if (e.isEmpty && infoObjectUnit.isThirdPartyTypeMandatory()) {
                                        return "Tidak boleh kosong";
                                      } else {
                                        return null;
                                      }
                                    },
                                    readOnly: true,
                                    onTap: () {
                                      infoObjectUnit.searchThirdPartyType(context);
                                    },
                                    controller: infoObjectUnit.controllerThirdPartyType,
                                    autovalidate: infoObjectUnit.autoValidate,
                                    decoration: new InputDecoration(
                                      labelText: 'Jenis Pihak Ketiga',
                                      labelStyle: TextStyle(color: Colors.black),
                                      filled: infoObjectUnit.isDisablePACIAAOSCONA,
                                      fillColor: Colors.black12,
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(8)),
                                      enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: infoObjectUnit.isThirdPartyTypeChanges ? Colors.purple : Colors.grey)),
                                      disabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: infoObjectUnit.isThirdPartyTypeChanges ? Colors.purple : Colors.grey)),
                                      contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                    )
                                ),
                              ),
                            ),
                            Visibility(
                                visible: infoObjectUnit.isThirdPartyTypeVisible(),
                                child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                            Visibility(
                              visible: infoObjectUnit.isThirdPartyVisible(),
                              child: IgnorePointer(
                                ignoring: infoObjectUnit.isDisablePACIAAOSCONA,
                                child: TextFormField(
                                    validator: (e) {
                                      if (e.isEmpty && infoObjectUnit.isThirdPartyMandatory()) {
                                        return "Tidak boleh kosong";
                                      } else {
                                        return null;
                                      }
                                    },
                                    readOnly: true,
                                    onTap: () {
                                      infoObjectUnit.searchThirdParty(widget.flag, context);
                                    },
                                    controller: infoObjectUnit.controllerThirdParty,
                                    autovalidate: infoObjectUnit.autoValidate,
                                    decoration: new InputDecoration(
                                      labelText: 'Pihak Ketiga',
                                      labelStyle: TextStyle(color: Colors.black),
                                      filled: infoObjectUnit.isDisablePACIAAOSCONA,
                                      fillColor: Colors.black12,
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(8)),
                                      enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: infoObjectUnit.isThirdPartyChanges ? Colors.purple : Colors.grey)),
                                      disabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: infoObjectUnit.isThirdPartyChanges ? Colors.purple : Colors.grey)),
                                      contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                    )
                                ),
                              ),
                            ),
                            Visibility(
                                visible: infoObjectUnit.isThirdPartyVisible(),
                                child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                            Visibility(
                              visible: infoObjectUnit.isMatriksDealerVisible(),
                              child: IgnorePointer(
                                ignoring: infoObjectUnit.isDisablePACIAAOSCONA,
                                child: TextFormField(
                                    autovalidate: infoObjectUnit.autoValidate,
                                    validator: (e) {
                                      if (e.isEmpty && infoObjectUnit.isMatriksDealerMandatory()) {
                                        return "Tidak boleh kosong";
                                      } else {
                                        return null;
                                      }
                                    },
                                    enabled: false,
                                    // onTap: () {
                                    //   infoObjectUnit.searchMatriksDealer(context);
                                    // },
                                    controller: infoObjectUnit.controllerMatriksDealer,
                                    decoration: new InputDecoration(
                                      labelText: 'Matriks Dealer',
                                      labelStyle: TextStyle(color: Colors.black),
                                      filled: true,
                                      fillColor: Colors.black12,
                                      errorStyle: TextStyle(
                                        color: Theme.of(context).errorColor, // or any other color
                                      ),
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(8)),
                                      enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: infoObjectUnit.isMatriksDealerChanges ? Colors.purple : Colors.grey)),
                                      disabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: infoObjectUnit.isMatriksDealerChanges ? Colors.purple : Colors.grey)),
                                      contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                    )
                                ),
                              ),
                            ),
                            Visibility(
                                visible: infoObjectUnit.isMatriksDealerVisible(),
                                child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                            Visibility(
                              visible: infoObjectUnit.isKegiatanVisible(),
                              child: IgnorePointer(
                                ignoring: infoObjectUnit.isDisablePACIAAOSCONA,
                                child: TextFormField(
                                    validator: (e) {
                                      if (e.isEmpty && infoObjectUnit.isKegiatanMandatory()) {
                                        return "Tidak boleh kosong";
                                      } else {
                                        return null;
                                      }
                                    },
                                    readOnly: true,
                                    onTap: infoObjectUnit.sourceOrderNameSelected != null ? () {
                                      infoObjectUnit.searchActivities(context);
                                    }: null,
                                    controller: infoObjectUnit.controllerKegiatan,
                                    autovalidate: infoObjectUnit.autoValidate,
                                    decoration: new InputDecoration(
                                      labelText: 'Kegiatan',
                                      labelStyle: TextStyle(color: Colors.black),
                                      filled: infoObjectUnit.isDisablePACIAAOSCONA,
                                      fillColor: Colors.black12,
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(8)),
                                      enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: infoObjectUnit.isKegiatanChanges ? Colors.purple : Colors.grey)),
                                      disabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: infoObjectUnit.isKegiatanChanges ? Colors.purple : Colors.grey)),
                                      contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                    )
                                ),
                              ),
                            ),
                            Visibility(
                                visible: infoObjectUnit.isKegiatanVisible(),
                                child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                            Visibility(
                              visible: infoObjectUnit.isSentraDVisible(),
                              child: IgnorePointer(
                                ignoring: infoObjectUnit.isDisablePACIAAOSCONA,
                                child: TextFormField(
                                    autovalidate: infoObjectUnit.autoValidate,
                                    validator: (e) {
                                      if (e.isEmpty && infoObjectUnit.isSentraDMandatory()) {
                                        return "Tidak boleh kosong";
                                      } else {
                                        return null;
                                      }
                                    },
                                    readOnly: true,
                                    onTap: () {},
                                    controller: infoObjectUnit.controllerSentraD,
                                    decoration: new InputDecoration(
                                      labelText: 'Sentra D',
                                      labelStyle: TextStyle(color: Colors.black),
                                      filled: infoObjectUnit.isDisablePACIAAOSCONA,
                                      fillColor: Colors.black12,
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(8)),
                                      enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: infoObjectUnit.isSentraDChanges ? Colors.purple : Colors.grey)),
                                      disabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: infoObjectUnit.isSentraDChanges ? Colors.purple : Colors.grey)),
                                      contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                    )
                                ),
                              ),
                            ),
                            Visibility(
                                visible: infoObjectUnit.isSentraDVisible(),
                                child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                            Visibility(
                              visible: infoObjectUnit.isUnitDVisible(),
                              child: IgnorePointer(
                                ignoring: infoObjectUnit.isDisablePACIAAOSCONA,
                                child: TextFormField(
                                    validator: (e) {
                                      if (e.isEmpty && infoObjectUnit.isUnitDMandatory()) {
                                        return "Tidak boleh kosong";
                                      } else {
                                        return null;
                                      }
                                    },
                                    readOnly: true,
                                    onTap: () {},
                                    controller: infoObjectUnit.controllerUnitD,
                                    autovalidate: infoObjectUnit.autoValidate,
                                    decoration: new InputDecoration(
                                      labelText: 'Unit D',
                                      labelStyle: TextStyle(color: Colors.black),
                                      filled: infoObjectUnit.isDisablePACIAAOSCONA,
                                      fillColor: Colors.black12,
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(8)),
                                      enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: infoObjectUnit.isUnitDChanges ? Colors.purple : Colors.grey)),
                                      disabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: infoObjectUnit.isUnitDChanges ? Colors.purple : Colors.grey)),
                                      contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                    )
                                ),
                              ),
                            ),
                            Visibility(
                                visible: infoObjectUnit.isUnitDVisible(),
                                child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                            Visibility(
                              visible: infoObjectUnit.isProgramVisible(),
                              child: IgnorePointer(
                                ignoring: infoObjectUnit.isDisablePACIAAOSCONA,
                                child: TextFormField(
                                    validator: (e) {
                                      if (e.isEmpty && infoObjectUnit.isProgramMandatory()) {
                                        return "Tidak boleh kosong";
                                      } else {
                                        return null;
                                      }
                                    },
                                    readOnly: true,
                                    onTap: () {
                                      infoObjectUnit.searchProgram(context);
                                    },
                                    controller: infoObjectUnit.controllerProgram,
                                    autovalidate: infoObjectUnit.autoValidate,
                                    decoration: new InputDecoration(
                                      labelText: 'Program',
                                      labelStyle: TextStyle(color: Colors.black),
                                      filled: infoObjectUnit.isDisablePACIAAOSCONA,
                                      fillColor: Colors.black12,
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(8)),
                                      enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: infoObjectUnit.isProgramChanges ? Colors.purple : Colors.grey)),
                                      disabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: infoObjectUnit.isProgramChanges ? Colors.purple : Colors.grey)),
                                      contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                    )
                                ),
                              ),
                            ),
                            Visibility(
                                visible: infoObjectUnit.isProgramVisible(),
                                child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                            Visibility(
                              visible: infoObjectUnit.isRehabTypeVisible(),
                              child: IgnorePointer(
                                ignoring: infoObjectUnit.isDisablePACIAAOSCONA,
                                child: TextFormField(
                                    validator: (e) {
                                      if (e.isEmpty && infoObjectUnit.isRehabTypeMandatory()) {
                                        return "Tidak boleh kosong";
                                      } else {
                                        return null;
                                      }
                                    },
                                    readOnly: true,
                                    onTap: () {
                                      infoObjectUnit.searchRehabType(context);
                                    },
                                    controller: infoObjectUnit.controllerRehabType,
                                    autovalidate: infoObjectUnit.autoValidate,
                                    decoration: new InputDecoration(
                                      labelText: 'Jenis Rehab',
                                      labelStyle: TextStyle(color: Colors.black),
                                      filled: infoObjectUnit.isDisablePACIAAOSCONA,
                                      fillColor: Colors.black12,
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(8)),
                                      enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: infoObjectUnit.isRehabTypeChanges ? Colors.purple : Colors.grey)),
                                      disabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: infoObjectUnit.isRehabTypeChanges ? Colors.purple : Colors.grey)),
                                      contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                    )
                                ),
                              ),
                            ),
                            Visibility(
                                visible: infoObjectUnit.isRehabTypeVisible(),
                                child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                            Visibility(
                              visible: infoObjectUnit.isReferenceNumberVisible(),
                              child: IgnorePointer(
                                ignoring: infoObjectUnit.isDisablePACIAAOSCONA,
                                child: TextFormField(
                                    validator: (e) {
                                      if ((e.isEmpty && infoObjectUnit.isReferenceNumberMandatory()) || ((e.isEmpty && infoObjectUnit.controllerRehabType.text.isNotEmpty) && (infoObjectUnit.rehabTypeSelected.id != "002"))) {
                                        return "Tidak boleh kosong";
                                      } else {
                                        return null;
                                      }
                                    },
                                    readOnly: true,
                                    onTap: () {
                                      infoObjectUnit.searchReferenceNumber(context);
                                    },
                                    controller: infoObjectUnit.controllerReferenceNumber,
                                    autovalidate: infoObjectUnit.autoValidate,
                                    decoration: new InputDecoration(
                                      labelText: 'No Reference',
                                      labelStyle: TextStyle(color: Colors.black),
                                      filled: infoObjectUnit.isDisablePACIAAOSCONA,
                                      fillColor: Colors.black12,
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(8)),
                                      enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: infoObjectUnit.isReferenceNumberChanges ? Colors.purple : Colors.grey)),
                                      disabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: infoObjectUnit.isReferenceNumberChanges ? Colors.purple : Colors.grey)),
                                      contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                    )
                                ),
                              ),
                            ),
                            Visibility(
                                visible: infoObjectUnit.isReferenceNumberVisible(),
                                child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
//                    TextFormField(
////                validator: (e) {
////                  if (e.isEmpty) {
////                    return "Tidak boleh kosong";
////                  } else {
////                    return null;
////                  }
////                },
//                        readOnly: true,
//                        onTap: () {},
//                        controller: infoObjectUnit.controllerWMP,
//                        autovalidate: infoObjectUnit.autoValidate,
//                        decoration: new InputDecoration(
//                            labelText: 'WMP',
//                            labelStyle: TextStyle(color: Colors.black),
//                            border: OutlineInputBorder(
//                                borderRadius: BorderRadius.circular(8)))),
//                     SizedBox(height: MediaQuery.of(context).size.height / 47),
                          ],
                        ),
                      ),
                    );
                },
              );
            }
        ),
        bottomNavigationBar: BottomAppBar(
              elevation: 0.0,
              child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Consumer<InformationObjectUnitChangeNotifier>(
                      builder: (context, informationObjectUnitChangeNotifier, _) {
                          return RaisedButton(
                              shape: RoundedRectangleBorder(
                                  borderRadius: new BorderRadius.circular(8.0)),
                              color: myPrimaryColor,
                              onPressed: () {
                                  informationObjectUnitChangeNotifier.check(context);
                              },
                              child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                      Text("DONE",
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 14,
                                              fontWeight: FontWeight.w500,
                                              letterSpacing: 1.25))
                                  ],
                              ));
                      },
                  )),
          ),
      ),
    );
  }
}
