import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/insurance_type_model.dart';
import 'package:ad1ms2_dev/models/major_insurance_model.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/form_m_add_major_insurance_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_credit_structure_change_notifier.dart';
import 'package:ad1ms2_dev/shared/decimal_text_input_formatter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';


class AddMajorInsurance extends StatefulWidget {
    final int flag;
    final MajorInsuranceModel formMMajorInsurance;
    final int index;

    const AddMajorInsurance({Key key, this.flag, this.formMMajorInsurance, this.index});
  @override
  _AddMajorInsuranceState createState() => _AddMajorInsuranceState();
}

class _AddMajorInsuranceState extends State<AddMajorInsurance> {
    Future<void> _loadData;
    @override
    void initState() {
      // Provider.of<FormMAddMajorInsuranceChangeNotifier>(context, listen: false).setPreference();
      // Provider.of<FormMAddMajorInsuranceChangeNotifier>(context,listen: false).getDataFromDashboard(context);

      // if (widget.flag != 0) {
        _loadData = Provider.of<FormMAddMajorInsuranceChangeNotifier>(context, listen: false).addData(context,widget.formMMajorInsurance, widget.flag, widget.index);
      // }
      // else{
      //   _loadData = Provider.of<FormMAddMajorInsuranceChangeNotifier>(context, listen: false).addData(context,null,0, widget.index);
      // }
      // Provider.of<FormMAddMajorInsuranceChangeNotifier>(context, listen: false).setDefault();
      super.initState();
      Provider.of<FormMAddMajorInsuranceChangeNotifier>(context, listen: false).setPreference();
    }
    @override
    Widget build(BuildContext context) {
      var _data = Provider.of<FormMAddMajorInsuranceChangeNotifier>(context, listen: false);
      _data.getDataFromDashboard(context);
      return Theme(
          data: ThemeData(
            fontFamily: "NunitoSans",
            primaryColor: Colors.black,
            accentColor: myPrimaryColor,
            primarySwatch: primaryOrange,
          ),
          child: Consumer<FormMAddMajorInsuranceChangeNotifier>(
            builder: (context, formMAddMajorInsuranceChangeNotifier, _) {
              return Scaffold(
                key:formMAddMajorInsuranceChangeNotifier.scaffoldKey,
                appBar: AppBar(
                  backgroundColor: myPrimaryColor,
                  title: Text(
                      widget.flag == 0 ? "Tambah Asuransi Utama" : formMAddMajorInsuranceChangeNotifier.isDisablePACIAAOSCONA ? "Detail Asuransi Utama" : "Edit Asuransi Utama",
                      style: TextStyle(color: Colors.black)),
                  centerTitle: true,
                  iconTheme: IconThemeData(
                    color: Colors.black,
                  ),
                ),
                body: FutureBuilder(
                    future: _loadData,
                    builder: (context, snapshot) {
                      if (snapshot.connectionState == ConnectionState.waiting) {
                        return Center(
                          child: CircularProgressIndicator(),
                        );
                      }
                      return Form(
                        onWillPop: _onWillPop,
                        key: formMAddMajorInsuranceChangeNotifier.key,
                        child: SingleChildScrollView(
                          padding: EdgeInsets.symmetric(
                              horizontal: MediaQuery.of(context).size.width / 27,
                              vertical: MediaQuery.of(context).size.height / 57),
                          child: Column(
                            children: [
                              Visibility(
                                visible: _data.isInsuranceTypeVisible(),
                                child: IgnorePointer(
                                  ignoring: formMAddMajorInsuranceChangeNotifier.isDisablePACIAAOSCONA || formMAddMajorInsuranceChangeNotifier.disableJenisPenawaran ? true : false,
                                  child: DropdownButtonFormField<InsuranceTypeModel>(
                                    autovalidate: formMAddMajorInsuranceChangeNotifier.autoValidate,
                                    validator: (e) {
                                      if (e == null && _data.isInsuranceTypeMandatory()) {
                                        return "Tidak boleh kosong";
                                      }
                                      else {
                                        return null;
                                      }
                                    },
                                    value: formMAddMajorInsuranceChangeNotifier.insuranceTypeSelected,
                                    onChanged: (value) {
                                      formMAddMajorInsuranceChangeNotifier.insuranceTypeSelected = value;
                                      formMAddMajorInsuranceChangeNotifier.getJenisPeriode(context, Provider.of<InfoCreditStructureChangeNotifier>(context,listen: false).periodOfTimeSelected);
                                    },
                                    decoration: InputDecoration(
                                      labelText: "Tipe Asuransi",
                                      border: OutlineInputBorder(),
                                      filled: formMAddMajorInsuranceChangeNotifier.isDisablePACIAAOSCONA || formMAddMajorInsuranceChangeNotifier.disableJenisPenawaran ? true : false,
                                      fillColor: Colors.black12,
                                      enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: formMAddMajorInsuranceChangeNotifier.insuranceTypeDakor ? Colors.purple : Colors.grey)),
                                      disabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: formMAddMajorInsuranceChangeNotifier.insuranceTypeDakor ? Colors.purple : Colors.grey)),
                                      contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                    ),
                                    items: formMAddMajorInsuranceChangeNotifier.listInsurance.map((value) {
                                      return DropdownMenuItem<InsuranceTypeModel>(
                                        value: value,
                                        child: Text(
                                          "${value.KODE} - ${value.DESKRIPSI}",
                                          overflow: TextOverflow.ellipsis,
                                        ),
                                      );
                                    }).toList(),
                                  ),
                                ),
                              ),
                              Visibility(
                                  visible: _data.isInsuranceTypeVisible(),
                                  child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                              Visibility(
                                visible: _data.isInstallmentNoVisible(),
                                child: IgnorePointer(
                                  ignoring: formMAddMajorInsuranceChangeNotifier.isDisablePACIAAOSCONA || formMAddMajorInsuranceChangeNotifier.disableJenisPenawaran ? true : false,
                                  child: DropdownButtonFormField<String>(
                                    autovalidate: formMAddMajorInsuranceChangeNotifier.autoValidate,
                                    validator: (e) {
                                      if (e == null && _data.isInstallmentNoMandatory()) {
                                        return "Tidak boleh kosong";
                                      }
                                      else {
                                        return null;
                                      }
                                    },
                                    value: formMAddMajorInsuranceChangeNotifier.numberOfCollateral,
                                    onChanged: (value) {
                                      formMAddMajorInsuranceChangeNotifier.numberOfCollateral = value;
                                    },
                                    decoration: InputDecoration(
                                      labelText: "Jaminan Ke",
                                      border: OutlineInputBorder(),
                                      filled: formMAddMajorInsuranceChangeNotifier.isDisablePACIAAOSCONA || formMAddMajorInsuranceChangeNotifier.disableJenisPenawaran ? true : false,
                                      fillColor: Colors.black12,
                                      enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: formMAddMajorInsuranceChangeNotifier.installmentNoDakor ? Colors.purple : Colors.grey)),
                                      disabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: formMAddMajorInsuranceChangeNotifier.installmentNoDakor ? Colors.purple : Colors.grey)),
                                      contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                    ),
                                    items: formMAddMajorInsuranceChangeNotifier.listNumberOfColla.map((value) {
                                      return DropdownMenuItem<String>(
                                        value: value,
                                        child: Text(
                                          value,
                                          overflow: TextOverflow.ellipsis,
                                        ),
                                      );
                                    }).toList(),
                                  ),
                                ),
                              ),
                              Visibility(
                                  visible: _data.isInstallmentNoVisible(),
                                  child: SizedBox(height: MediaQuery.of(context).size.height / 47)
                              ),
                              Visibility(
                                visible: _data.isCompanyVisible(),
                                child: IgnorePointer(
                                  ignoring: formMAddMajorInsuranceChangeNotifier.isDisablePACIAAOSCONA || formMAddMajorInsuranceChangeNotifier.disableJenisPenawaran ? true : false,
                                  child: TextFormField(
                                    readOnly: true,
                                    onTap:() {
                                      formMAddMajorInsuranceChangeNotifier.searchCompany(context);
                                    },
                                    controller: formMAddMajorInsuranceChangeNotifier.controllerCompany,
                                    style: new TextStyle(color: Colors.black),
                                    decoration: new InputDecoration(
                                      labelText: 'Perusahaan',
                                      labelStyle: TextStyle(color: Colors.black),
                                      filled: formMAddMajorInsuranceChangeNotifier.isDisablePACIAAOSCONA || formMAddMajorInsuranceChangeNotifier.disableJenisPenawaran ? true : false,
                                      fillColor: Colors.black12,
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(8)),
                                      enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: formMAddMajorInsuranceChangeNotifier.companyDakor ? Colors.purple : Colors.grey)),
                                      disabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: formMAddMajorInsuranceChangeNotifier.companyDakor ? Colors.purple : Colors.grey)),
                                      contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                    ),
                                    keyboardType: TextInputType.text,
                                    textCapitalization: TextCapitalization.characters,
                                    autovalidate: formMAddMajorInsuranceChangeNotifier.autoValidate,
                                    validator: (e) {
                                      if (e.isEmpty && _data.isCompanyMandatory()) {
                                        return "Tidak boleh kosong";
                                      } else {
                                        return null;
                                      }
                                    },
                                  ),
                                ),
                              ),
                              Visibility(
                                  visible: _data.isCompanyVisible(),
                                  child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                              Visibility(
                                visible: _data.isProductVisible(),
                                child: IgnorePointer(
                                  ignoring: formMAddMajorInsuranceChangeNotifier.isDisablePACIAAOSCONA || formMAddMajorInsuranceChangeNotifier.disableJenisPenawaran ? true : false,
                                  child: TextFormField(
                                    readOnly: true,
                                    onTap:() {
                                      formMAddMajorInsuranceChangeNotifier.searchProduct(context);
                                    },
                                    controller: formMAddMajorInsuranceChangeNotifier.controllerProduct,
                                    style: new TextStyle(color: Colors.black),
                                    decoration: new InputDecoration(
                                      labelText: 'Produk',
                                      labelStyle: TextStyle(color: Colors.black),
                                      filled: formMAddMajorInsuranceChangeNotifier.isDisablePACIAAOSCONA || formMAddMajorInsuranceChangeNotifier.disableJenisPenawaran ? true : false,
                                      fillColor: Colors.black12,
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(8)),
                                      enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: formMAddMajorInsuranceChangeNotifier.productDakor ? Colors.purple : Colors.grey)),
                                      disabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: formMAddMajorInsuranceChangeNotifier.productDakor ? Colors.purple : Colors.grey)),
                                      contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                    ),
                                    keyboardType: TextInputType.text,
                                    textCapitalization: TextCapitalization.characters,
                                    autovalidate: formMAddMajorInsuranceChangeNotifier.autoValidate,
                                    validator: (e) {
                                      if (e.isEmpty && _data.isProductMandatory()) {
                                        return "Tidak boleh kosong";
                                      } else {
                                        return null;
                                      }
                                    },
                                  ),
                                ),
                              ),
                              Visibility(
                                  visible: _data.isProductVisible(),
                                  child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                              Visibility(
                                visible: _data.isPeriodVisible(),
                                child: IgnorePointer(
                                  ignoring: formMAddMajorInsuranceChangeNotifier.disableJenisPenawaran,
                                  child: TextFormField(
                                    enabled: false,
                                    controller: formMAddMajorInsuranceChangeNotifier.controllerPeriodType,
                                    style: new TextStyle(color: Colors.black),
                                    decoration: new InputDecoration(
                                      filled: true,
                                      fillColor: Colors.black12,
                                      labelText: 'Periode',
                                      labelStyle: TextStyle(color: Colors.black),
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(8)),
                                      enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: formMAddMajorInsuranceChangeNotifier.periodDakor ? Colors.purple : Colors.grey)),
                                      disabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: formMAddMajorInsuranceChangeNotifier.periodDakor ? Colors.purple : Colors.grey)),
                                      contentPadding: EdgeInsets.symmetric(horizontal: 10),),
                                    keyboardType: TextInputType.number,
                                    textCapitalization: TextCapitalization.characters,
                                    inputFormatters: [
                                      WhitelistingTextInputFormatter.digitsOnly
                                    ],
                                    autovalidate: formMAddMajorInsuranceChangeNotifier.autoValidate,
                                    validator: (e) {
                                      if (e.isEmpty && _data.isPeriodMandatory()) {
                                        return "Tidak boleh kosong";
                                      } else {
                                        return null;
                                      }
                                    },
                                    // onChanged: (value){
                                    //   formMAddMajorInsuranceChangeNotifier.getType(int.parse(value));
                                    // },
                                  ),
                                ),
                              ),
                              Visibility(
                                  visible: _data.isPeriodVisible(),
                                  child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                              // formMAddMajorInsuranceChangeNotifier.switchType
                              //     ?
                              // Visibility(
                              //   visible: _data.isTypeVisible(),
                              //   child: TextFormField(
                              //     controller: formMAddMajorInsuranceChangeNotifier.controllerType,
                              //     style: new TextStyle(color: Colors.black),
                              //     decoration: new InputDecoration(
                              //         labelText: 'Jenis',
                              //         labelStyle: TextStyle(color: Colors.black),
                              //         border: OutlineInputBorder(
                              //             borderRadius: BorderRadius.circular(8)),
                              //       enabledBorder: OutlineInputBorder(
                              //           borderSide: BorderSide(color: formMAddMajorInsuranceChangeNotifier.typeDakor ? Colors.purple : Colors.grey)),
                              //       disabledBorder: OutlineInputBorder(
                              //           borderSide: BorderSide(color: formMAddMajorInsuranceChangeNotifier.typeDakor ? Colors.purple : Colors.grey)),),
                              //     keyboardType: TextInputType.number,
                              //     inputFormatters: [
                              //       WhitelistingTextInputFormatter.digitsOnly,
                              //     ],
                              //     autovalidate: formMAddMajorInsuranceChangeNotifier.autoValidate,
                              //     validator: (e) {
                              //       if (e.isEmpty && _data.isTypeMandatory()) {
                              //         return "Tidak boleh kosong";
                              //       } else {
                              //         return null;
                              //       }
                              //     },
                              //     onFieldSubmitted: (value){
                              //       formMAddMajorInsuranceChangeNotifier.calculatePeriod(0,null);
                              //     },
                              //   ),
                              // )
                              //     :
                              Visibility(
                                visible: _data.isTypeVisible(),
                                child: IgnorePointer(
                                  ignoring: formMAddMajorInsuranceChangeNotifier.isDisablePACIAAOSCONA || formMAddMajorInsuranceChangeNotifier.disableJenisPenawaran ? true : false,
                                  child: DropdownButtonFormField<String>(
                                    autovalidate: formMAddMajorInsuranceChangeNotifier.autoValidate,
                                    validator: (e) {
                                      if (e == null && _data.isTypeMandatory()) {
                                        return "Tidak boleh kosong";
                                      } else {
                                        return null;
                                      }
                                    },
                                    value: formMAddMajorInsuranceChangeNotifier.typeSelected,
                                    onChanged: (value) {
                                      formMAddMajorInsuranceChangeNotifier.typeSelected = value;
                                      // formMAddMajorInsuranceChangeNotifier.calculatePeriod(0,null);
                                    },
                                    decoration: InputDecoration(
                                      labelText: "Jenis",
                                      border: OutlineInputBorder(),
                                      filled: formMAddMajorInsuranceChangeNotifier.isDisablePACIAAOSCONA || formMAddMajorInsuranceChangeNotifier.disableJenisPenawaran ? true : false,
                                      fillColor: Colors.black12,
                                      enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: formMAddMajorInsuranceChangeNotifier.typeDakor ? Colors.purple : Colors.grey)),
                                      disabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: formMAddMajorInsuranceChangeNotifier.typeDakor ? Colors.purple : Colors.grey)),
                                      contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                    ),
                                    items: formMAddMajorInsuranceChangeNotifier.listType.map((value) {
                                      return DropdownMenuItem<String>(
                                        value: value,
                                        child: Text(
                                          value,
                                          overflow: TextOverflow.ellipsis,
                                        ),
                                      );
                                    }).toList(),
                                  ),
                                ),
                              ),
                              Visibility(visible: _data.isTypeVisible(),
                                  child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                              Visibility(
                                visible: _data.isCoverage1Visible(),
                                child: IgnorePointer(
                                  ignoring: formMAddMajorInsuranceChangeNotifier.isDisablePACIAAOSCONA || formMAddMajorInsuranceChangeNotifier.disableJenisPenawaran ? true : false,
                                  child: TextFormField(
                                    readOnly: true,
                                    onTap:() {
                                      formMAddMajorInsuranceChangeNotifier.searchCoverage(context,1);
                                    },
                                    controller: formMAddMajorInsuranceChangeNotifier.controllerCoverage1,
                                    style: new TextStyle(color: Colors.black),
                                    decoration: new InputDecoration(
                                      labelText: 'Coverage 1',
                                      labelStyle: TextStyle(color: Colors.black),
                                      filled: formMAddMajorInsuranceChangeNotifier.isDisablePACIAAOSCONA || formMAddMajorInsuranceChangeNotifier.disableJenisPenawaran ? true : false,
                                      fillColor: Colors.black12,
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(8)),
                                      enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: formMAddMajorInsuranceChangeNotifier.coverage1Dakor ? Colors.purple : Colors.grey)),
                                      disabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: formMAddMajorInsuranceChangeNotifier.coverage1Dakor ? Colors.purple : Colors.grey)),
                                      contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                    ),
                                    keyboardType: TextInputType.text,
                                    textCapitalization: TextCapitalization.characters,
                                    autovalidate: formMAddMajorInsuranceChangeNotifier.autoValidate,
                                    validator: (e) {
                                      if (e.isEmpty && _data.isCoverage1Mandatory()) {
                                        return "Tidak boleh kosong";
                                      } else {
                                        return null;
                                      }
                                    },
                                  ),
                                ),
                              ),
                              Visibility(visible: _data.isCoverage1Visible(),
                                  child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                              !_data.isAsuransiPerluasanType
                                ?
                              Column(
                                children: [
                                  Visibility(
                                    visible: _data.isCoverage2Visible(),
                                    child: IgnorePointer(
                                      ignoring: formMAddMajorInsuranceChangeNotifier.isDisablePACIAAOSCONA || formMAddMajorInsuranceChangeNotifier.disableJenisPenawaran ? true : false,
                                      child: TextFormField(
                                        readOnly: true,
                                        onTap:() {
                                          formMAddMajorInsuranceChangeNotifier.searchCoverage(context,2);
                                        },
                                        enabled: formMAddMajorInsuranceChangeNotifier.controllerPeriodType.text == formMAddMajorInsuranceChangeNotifier.typeSelected || formMAddMajorInsuranceChangeNotifier.typeSelected == null ? false : true,
                                        controller: formMAddMajorInsuranceChangeNotifier.controllerCoverage2,
                                        style: new TextStyle(color: Colors.black),
                                        decoration: new InputDecoration(
                                          filled: formMAddMajorInsuranceChangeNotifier.controllerPeriodType.text == formMAddMajorInsuranceChangeNotifier.typeSelected || formMAddMajorInsuranceChangeNotifier.typeSelected == null ? true : false,
                                          fillColor: formMAddMajorInsuranceChangeNotifier.controllerPeriodType.text == formMAddMajorInsuranceChangeNotifier.typeSelected || formMAddMajorInsuranceChangeNotifier.typeSelected == null ? Colors.black12 : null,
                                          labelText: 'Coverage 2',
                                          labelStyle: TextStyle(color: Colors.black),
                                          border: OutlineInputBorder(
                                              borderRadius: BorderRadius.circular(8)),
                                          enabledBorder: OutlineInputBorder(
                                              borderSide: BorderSide(color: formMAddMajorInsuranceChangeNotifier.coverage2Dakor ? Colors.purple : Colors.grey)),
                                          disabledBorder: OutlineInputBorder(
                                              borderSide: BorderSide(color: formMAddMajorInsuranceChangeNotifier.coverage2Dakor ? Colors.purple : Colors.grey)),
                                          contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                        ),
                                        keyboardType: TextInputType.text,
                                        textCapitalization: TextCapitalization.characters,
                                        autovalidate: formMAddMajorInsuranceChangeNotifier.autoValidate,
                                        validator: (e) {
                                          if (e.isEmpty && _data.isCoverage2Mandatory()) {
                                            return "Tidak boleh kosong";
                                          } else {
                                            return null;
                                          }
                                        },
                                      ),
                                    ),
                                  ),
                                  Visibility(visible: _data.isCoverage2Visible(),
                                      child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                                ],
                              )
                                :
                              Column(
                                children: [
                                  Visibility(
                                    visible: _data.isSubTypeVisible(),
                                    child: IgnorePointer(
                                      ignoring: formMAddMajorInsuranceChangeNotifier.isDisablePACIAAOSCONA || formMAddMajorInsuranceChangeNotifier.disableJenisPenawaran ? true : false,
                                      child: TextFormField(
                                        readOnly: true,
                                        onTap:() {
                                          formMAddMajorInsuranceChangeNotifier.searchSubType(context);
                                        },
                                        controller: formMAddMajorInsuranceChangeNotifier.controllerSubType,
                                        style: TextStyle(color: Colors.black),
                                        decoration: InputDecoration(
                                          labelText: 'Sub Type',
                                          labelStyle: TextStyle(color: Colors.black),
                                          filled: formMAddMajorInsuranceChangeNotifier.isDisablePACIAAOSCONA || formMAddMajorInsuranceChangeNotifier.disableJenisPenawaran ? true : false,
                                          fillColor: Colors.black12,
                                          border: OutlineInputBorder(
                                              borderRadius: BorderRadius.circular(8)),
                                          enabledBorder: OutlineInputBorder(
                                              borderSide: BorderSide(color: formMAddMajorInsuranceChangeNotifier.subTypeDakor ? Colors.purple : Colors.grey)),
                                          disabledBorder: OutlineInputBorder(
                                              borderSide: BorderSide(color: formMAddMajorInsuranceChangeNotifier.subTypeDakor ? Colors.purple : Colors.grey)),
                                          contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                        ),
                                        keyboardType: TextInputType.text,
                                        textCapitalization: TextCapitalization.characters,
                                        autovalidate: formMAddMajorInsuranceChangeNotifier.autoValidate,
                                        validator: (e) {
                                          if (e.isEmpty) {
                                            return "Tidak boleh kosong";
                                          } else {
                                            return null;
                                          }
                                        },
                                      ),
                                    ),
                                  ),
                                  Visibility(
                                      visible: _data.isSubTypeVisible(),
                                      child: SizedBox(height: MediaQuery.of(context).size.height / 47)
                                  ),
                                ],
                              ),
                              // Visibility(
                              //   visible: _data.isCoverage2Visible(),
                              //   child: TextFormField(
                              //     readOnly: true,
                              //     onTap:() {
                              //       formMAddMajorInsuranceChangeNotifier.searchCoverage(context,2);
                              //     },
                              //     controller: formMAddMajorInsuranceChangeNotifier.controllerCoverage2,
                              //     style: new TextStyle(color: Colors.black),
                              //     decoration: new InputDecoration(
                              //       labelText: 'Coverage 2',
                              //       labelStyle: TextStyle(color: Colors.black),
                              //       border: OutlineInputBorder(
                              //           borderRadius: BorderRadius.circular(8)),
                              //       enabledBorder: OutlineInputBorder(
                              //           borderSide: BorderSide(color: formMAddMajorInsuranceChangeNotifier.coverage2Dakor ? Colors.purple : Colors.grey)),
                              //       disabledBorder: OutlineInputBorder(
                              //           borderSide: BorderSide(color: formMAddMajorInsuranceChangeNotifier.coverage2Dakor ? Colors.purple : Colors.grey)),),
                              //     keyboardType: TextInputType.text,
                              //     textCapitalization: TextCapitalization.characters,
                              //     autovalidate: formMAddMajorInsuranceChangeNotifier.autoValidate,
                              //     validator: (e) {
                              //       if (e.isEmpty && _data.isCoverage2Mandatory()) {
                              //         return "Tidak boleh kosong";
                              //       } else {
                              //         return null;
                              //       }
                              //     },
                              //   ),
                              // ),
                              // Visibility(visible: _data.isCoverage2Visible(),
                              //     child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                              // Visibility(
                              //   visible: _data.isSubTypeVisible(),
                              //   child: TextFormField(
                              //     readOnly: true,
                              //     onTap:() {
                              //       formMAddMajorInsuranceChangeNotifier.searchSubType(context);
                              //     },
                              //     controller: formMAddMajorInsuranceChangeNotifier.controllerSubType,
                              //     style: TextStyle(color: Colors.black),
                              //     decoration: InputDecoration(
                              //       labelText: 'Sub Type',
                              //       labelStyle: TextStyle(color: Colors.black),
                              //       border: OutlineInputBorder(
                              //           borderRadius: BorderRadius.circular(8)),
                              //       // enabledBorder: OutlineInputBorder(
                              //       //     borderSide: BorderSide(color: formMAddMajorInsuranceChangeNotifier.coverage2Dakor ? Colors.purple : Colors.grey)),
                              //       // disabledBorder: OutlineInputBorder(
                              //       //     borderSide: BorderSide(color: formMAddMajorInsuranceChangeNotifier.coverage2Dakor ? Colors.purple : Colors.grey)),
                              //     ),
                              //     keyboardType: TextInputType.text,
                              //     textCapitalization: TextCapitalization.characters,
                              //     autovalidate: formMAddMajorInsuranceChangeNotifier.autoValidate,
                              //     validator: (e) {
                              //       if (e.isEmpty) {
                              //         return "Tidak boleh kosong";
                              //       } else {
                              //         return null;
                              //       }
                              //     },
                              //   ),
                              // ),
                              // Visibility(
                              //     visible: _data.isSubTypeVisible(),
                              //     child: SizedBox(height: MediaQuery.of(context).size.height / 47)
                              // ),
                              Visibility(
                                visible: _data.isCoverageTypeVisible(),
                                child: IgnorePointer(
                                  ignoring: formMAddMajorInsuranceChangeNotifier.disableJenisPenawaran,
                                  child: TextFormField(
                                    // onTap: (){
                                    //   formMAddMajorInsuranceChangeNotifier.searchCoverageType(context);
                                    // },
                                    // readOnly: true,
                                    enabled: false,
                                    controller: formMAddMajorInsuranceChangeNotifier.controllerCoverageType,
                                    style: new TextStyle(color: Colors.black),
                                    decoration: new InputDecoration(
                                      filled: true,
                                      fillColor: Colors.black12,
                                      labelText: 'Jenis Pertanggungan',
                                      labelStyle: TextStyle(color: Colors.black),
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(8)),
                                      enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: formMAddMajorInsuranceChangeNotifier.coverageTypeDakor ? Colors.purple : Colors.grey)),
                                      disabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: formMAddMajorInsuranceChangeNotifier.coverageTypeDakor ? Colors.purple : Colors.grey)),
                                      contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                    ),
                                    keyboardType: TextInputType.text,
                                    textCapitalization: TextCapitalization.characters,
                                    autovalidate: formMAddMajorInsuranceChangeNotifier.autoValidate,
                                    validator: (e) {
                                      if (e.isEmpty && _data.isCoverageTypeMandatory()) {
                                        return "Tidak boleh kosong";
                                      } else {
                                        return null;
                                      }
                                    },
                                  ),
                                ),
                              ),
                              Visibility(visible: _data.isCoverageTypeVisible(),
                                  child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                              Visibility(
                                visible: _data.isCoverageValueVisible(),
                                child: IgnorePointer(
                                  ignoring: formMAddMajorInsuranceChangeNotifier.disableJenisPenawaran,
                                  child: TextFormField(
                                    enabled: formMAddMajorInsuranceChangeNotifier.flagDisableNilaiPertanggungan ? false : true,
                                    controller: formMAddMajorInsuranceChangeNotifier.controllerCoverageValue,
                                    style: new TextStyle(color: Colors.black),
                                    decoration: new InputDecoration(
                                      labelText: 'Nilai Pertanggungan',
                                      labelStyle: TextStyle(color: Colors.black),
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(8)),
                                      filled: formMAddMajorInsuranceChangeNotifier.flagDisableNilaiPertanggungan ? true : false,
                                      fillColor: formMAddMajorInsuranceChangeNotifier.flagDisableNilaiPertanggungan ? Colors.black12 : null,
                                      enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: formMAddMajorInsuranceChangeNotifier.coverageValueDakor ? Colors.purple : Colors.grey)),
                                      disabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: formMAddMajorInsuranceChangeNotifier.coverageValueDakor ? Colors.purple : Colors.grey)),
                                      contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                    ),
                                    keyboardType: TextInputType.number,
                                    textCapitalization: TextCapitalization.characters,
                                    onFieldSubmitted: (value) {
                                      formMAddMajorInsuranceChangeNotifier.controllerCoverageValue.text = formMAddMajorInsuranceChangeNotifier.formatCurrency.formatCurrency(value);
                                      formMAddMajorInsuranceChangeNotifier.calculateUpperLowerLimit();
                                    },
                                    inputFormatters: [
                                      WhitelistingTextInputFormatter.digitsOnly
                                    ],
                                    textAlign: TextAlign.end,
                                    autovalidate: formMAddMajorInsuranceChangeNotifier.autoValidate,
                                    validator: (e) {
                                      if (e.isEmpty && _data.isCoverageValueMandatory()) {
                                        return "Tidak boleh kosong";
                                      } else {
                                        return null;
                                      }
                                    },
                                  ),
                                ),
                              ),
                              Visibility(visible: _data.isCoverageValueVisible(),
                                  child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                              Row(
                                children: [
                                  Visibility(
                                    visible: _data.isUpperLimitRateVisible(),
                                    child: Expanded(
                                      flex: 2,
                                      child: IgnorePointer(
                                        ignoring: formMAddMajorInsuranceChangeNotifier.disableJenisPenawaran,
                                        child: TextFormField(
                                          enabled: false,
                                          controller: formMAddMajorInsuranceChangeNotifier.controllerUpperLimitRate,
                                          style: new TextStyle(color: Colors.black),
                                          decoration: new InputDecoration(
                                            labelText: 'Batas Atas %',
                                            labelStyle: TextStyle(color: Colors.black),
                                            border: OutlineInputBorder(
                                                borderRadius: BorderRadius.circular(8)),
                                            filled: true,
                                            fillColor: Colors.black12,
                                            enabledBorder: OutlineInputBorder(
                                                borderSide: BorderSide(color: formMAddMajorInsuranceChangeNotifier.upperLimitRateDakor ? Colors.purple : Colors.grey)),
                                            disabledBorder: OutlineInputBorder(
                                                borderSide: BorderSide(color: formMAddMajorInsuranceChangeNotifier.upperLimitRateDakor ? Colors.purple : Colors.grey)),
                                            contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                          ),
                                          keyboardType: TextInputType.number,
                                          textCapitalization: TextCapitalization.characters,
                                          inputFormatters: [
                                            WhitelistingTextInputFormatter.digitsOnly
                                          ],
                                          autovalidate: formMAddMajorInsuranceChangeNotifier.autoValidate,
                                          validator: (e) {
                                            if (e.isEmpty && _data.isUpperLimitRateMandatory()) {
                                              return "Tidak boleh kosong";
                                            } else {
                                              return null;
                                            }
                                          },
                                        ),
                                      ),
                                    ),
                                  ),
                                  Visibility(visible: _data.isUpperLimitRateVisible(),
                                      child: SizedBox(width: MediaQuery.of(context).size.width / 47)),
                                  Visibility(
                                    visible: _data.isUpperLimitValueVisible(),
                                    child: Expanded(
                                      flex: 3,
                                      child: IgnorePointer(
                                        ignoring: formMAddMajorInsuranceChangeNotifier.disableJenisPenawaran,
                                        child: TextFormField(
                                          enabled: false,
                                          controller: formMAddMajorInsuranceChangeNotifier.controllerUpperLimit,
                                          style: new TextStyle(color: Colors.black),
                                          decoration: new InputDecoration(
                                            labelText: 'Batas Atas',
                                            labelStyle: TextStyle(color: Colors.black),
                                            border: OutlineInputBorder(
                                                borderRadius: BorderRadius.circular(8)),
                                            filled: true,
                                            fillColor: Colors.black12,
                                            enabledBorder: OutlineInputBorder(
                                                borderSide: BorderSide(color: formMAddMajorInsuranceChangeNotifier.upperLimitValueDakor ? Colors.purple : Colors.grey)),
                                            disabledBorder: OutlineInputBorder(
                                                borderSide: BorderSide(color: formMAddMajorInsuranceChangeNotifier.upperLimitValueDakor ? Colors.purple : Colors.grey)),
                                            contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                          ),
                                          textAlign: TextAlign.end,
                                          keyboardType: TextInputType.number,
                                          textCapitalization: TextCapitalization.characters,
                                          inputFormatters: [
                                            WhitelistingTextInputFormatter.digitsOnly
                                          ],
                                          autovalidate: formMAddMajorInsuranceChangeNotifier.autoValidate,
                                          validator: (e) {
                                            if (e.isEmpty && _data.isUpperLimitValueMandatory()) {
                                              return "Tidak boleh kosong";
                                            } else {
                                              return null;
                                            }
                                          },
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              Visibility(visible: _data.isUpperLimitValueVisible() || _data.isUpperLimitRateVisible(),
                                  child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                              Row(
                                children: [
                                  Visibility(
                                    visible: _data.isLowerLimitRateVisible(),
                                    child: Expanded(
                                      flex: 2,
                                      child: IgnorePointer(
                                        ignoring: formMAddMajorInsuranceChangeNotifier.disableJenisPenawaran,
                                        child: TextFormField(
                                          enabled: false,
                                          controller: formMAddMajorInsuranceChangeNotifier.controllerLowerLimitRate,
                                          style: new TextStyle(color: Colors.black),
                                          decoration: new InputDecoration(
                                            labelText: 'Batas Bawah %',
                                            labelStyle: TextStyle(color: Colors.black),
                                            border: OutlineInputBorder(
                                                borderRadius: BorderRadius.circular(8)),
                                            filled: true,
                                            fillColor: Colors.black12,
                                            enabledBorder: OutlineInputBorder(
                                                borderSide: BorderSide(color: formMAddMajorInsuranceChangeNotifier.lowerLimitRateDakor ? Colors.purple : Colors.grey)),
                                            disabledBorder: OutlineInputBorder(
                                                borderSide: BorderSide(color: formMAddMajorInsuranceChangeNotifier.lowerLimitRateDakor ? Colors.purple : Colors.grey)),
                                            contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                          ),
                                          keyboardType: TextInputType.number,
                                          textCapitalization: TextCapitalization.characters,
                                          inputFormatters: [
                                            WhitelistingTextInputFormatter.digitsOnly
                                          ],
                                          autovalidate: formMAddMajorInsuranceChangeNotifier.autoValidate,
                                          validator: (e) {
                                            if (e.isEmpty && _data.isLowerLimitRateMandatory()) {
                                              return "Tidak boleh kosong";
                                            } else {
                                              return null;
                                            }
                                          },
                                        ),
                                      ),
                                    ),
                                  ),
                                  Visibility(visible: _data.isLowerLimitRateVisible(),
                                      child: SizedBox(width: MediaQuery.of(context).size.width / 47)),
                                  Visibility(
                                    visible: _data.isLowerLimitValueVisible(),
                                    child: Expanded(
                                      flex: 3,
                                      child: IgnorePointer(
                                        ignoring: formMAddMajorInsuranceChangeNotifier.disableJenisPenawaran,
                                        child: TextFormField(
                                          enabled: false,
                                          controller: formMAddMajorInsuranceChangeNotifier.controllerLowerLimit,
                                          style: new TextStyle(color: Colors.black),
                                          decoration: new InputDecoration(
                                            labelText: 'Batas Bawah',
                                            labelStyle: TextStyle(color: Colors.black),
                                            border: OutlineInputBorder(
                                                borderRadius: BorderRadius.circular(8)),
                                            filled: true,
                                            fillColor: Colors.black12,
                                            enabledBorder: OutlineInputBorder(
                                                borderSide: BorderSide(color: formMAddMajorInsuranceChangeNotifier.lowerLimitValueDakor ? Colors.purple : Colors.grey)),
                                            disabledBorder: OutlineInputBorder(
                                                borderSide: BorderSide(color: formMAddMajorInsuranceChangeNotifier.lowerLimitValueDakor ? Colors.purple : Colors.grey)),
                                            contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                          ),
                                          textAlign: TextAlign.end,
                                          keyboardType: TextInputType.number,
                                          textCapitalization: TextCapitalization.characters,
                                          inputFormatters: [
                                            WhitelistingTextInputFormatter.digitsOnly
                                          ],
                                          autovalidate: formMAddMajorInsuranceChangeNotifier.autoValidate,
                                          validator: (e) {
                                            if (e.isEmpty && _data.isLowerLimitValueMandatory()) {
                                              return "Tidak boleh kosong";
                                            } else {
                                              return null;
                                            }
                                          },
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              Visibility(visible: _data.isLowerLimitValueVisible() || _data.isLowerLimitRateVisible(),
                                  child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                              Visibility(
                                visible: _data.isCashVisible(),
                                child: IgnorePointer(
                                  ignoring: formMAddMajorInsuranceChangeNotifier.isDisablePACIAAOSCONA || formMAddMajorInsuranceChangeNotifier.disableJenisPenawaran ? true : false,
                                  child: TextFormField(
                                    controller: formMAddMajorInsuranceChangeNotifier.controllerPriceCash,
                                    style: new TextStyle(color: Colors.black),
                                    decoration: new InputDecoration(
                                      labelText: 'Biaya Tunai',
                                      labelStyle: TextStyle(color: Colors.black),
                                      filled: formMAddMajorInsuranceChangeNotifier.isDisablePACIAAOSCONA || formMAddMajorInsuranceChangeNotifier.disableJenisPenawaran ? true : false,
                                      fillColor: Colors.black12,
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(8)),
                                      enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: formMAddMajorInsuranceChangeNotifier.cashDakor ? Colors.purple : Colors.grey)),
                                      disabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: formMAddMajorInsuranceChangeNotifier.cashDakor ? Colors.purple : Colors.grey)),
                                      contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                    ),
                                    keyboardType: TextInputType.number,
                                    textAlign: TextAlign.end,
                                    onFieldSubmitted: (value) {
                                      formMAddMajorInsuranceChangeNotifier.controllerPriceCash.text = formMAddMajorInsuranceChangeNotifier.formatCurrency.formatCurrency(value);
                                      formMAddMajorInsuranceChangeNotifier.calculateTotalCost();
                                      formMAddMajorInsuranceChangeNotifier.calculateUpperLowerLimit();
                                    },
                                    onTap: () {
                                      formMAddMajorInsuranceChangeNotifier.formatting();
                                      formMAddMajorInsuranceChangeNotifier.calculateTotalCost();
                                      formMAddMajorInsuranceChangeNotifier.calculateUpperLowerLimit();
                                    },
                                    textInputAction: TextInputAction.done,
                                    inputFormatters: [
                                      DecimalTextInputFormatter(decimalRange: 2),
                                      formMAddMajorInsuranceChangeNotifier.amountValidator
                                    ],
                                    autovalidate: formMAddMajorInsuranceChangeNotifier.autoValidate,
                                    validator: (e) {
                                      if (e.isEmpty && _data.isCashMandatory()) {
                                        return "Tidak boleh kosong";
                                      } else {
                                        return null;
                                      }
                                    },
                                  ),
                                ),
                              ),
                              Visibility(visible: _data.isCashVisible(),
                                  child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                              Visibility(
                                visible: _data.isCreditVisible(),
                                child: IgnorePointer(
                                  ignoring: formMAddMajorInsuranceChangeNotifier.isDisablePACIAAOSCONA || formMAddMajorInsuranceChangeNotifier.disableJenisPenawaran ? true : false,
                                  child: TextFormField(
                                    controller: formMAddMajorInsuranceChangeNotifier.controllerPriceCredit,
                                    style: new TextStyle(color: Colors.black),
                                    decoration: new InputDecoration(
                                      labelText: 'Biaya Kredit',
                                      labelStyle: TextStyle(color: Colors.black),
                                      filled: formMAddMajorInsuranceChangeNotifier.isDisablePACIAAOSCONA || formMAddMajorInsuranceChangeNotifier.disableJenisPenawaran ? true : false,
                                      fillColor: Colors.black12,
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(8)),
                                      enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: formMAddMajorInsuranceChangeNotifier.creditDakor ? Colors.purple : Colors.grey)),
                                      disabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: formMAddMajorInsuranceChangeNotifier.creditDakor ? Colors.purple : Colors.grey)),
                                      contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                    ),
                                    keyboardType: TextInputType.number,
                                    textAlign: TextAlign.end,
                                    onFieldSubmitted: (value) {
                                      formMAddMajorInsuranceChangeNotifier.controllerPriceCredit.text = formMAddMajorInsuranceChangeNotifier.formatCurrency.formatCurrency(value);
                                      formMAddMajorInsuranceChangeNotifier.calculateTotalCost();
                                      formMAddMajorInsuranceChangeNotifier.calculateUpperLowerLimit();
                                    },
                                    onTap: () {
                                      formMAddMajorInsuranceChangeNotifier.formatting();
                                      formMAddMajorInsuranceChangeNotifier.calculateTotalCost();
                                      formMAddMajorInsuranceChangeNotifier.calculateUpperLowerLimit();
                                    },
                                    textInputAction: TextInputAction.done,
                                    inputFormatters: [
                                      DecimalTextInputFormatter(decimalRange: 2),
                                      formMAddMajorInsuranceChangeNotifier.amountValidator
                                    ],
                                    autovalidate: formMAddMajorInsuranceChangeNotifier.autoValidate,
                                    validator: (e) {
                                      if (e.isEmpty && _data.isCreditMandatory()) {
                                        return "Tidak boleh kosong";
                                      } else {
                                        return null;
                                      }
                                    },
                                  ),
                                ),
                              ),
                              Visibility(visible: _data.isCreditVisible(),
                                  child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                              Visibility(
                                visible: _data.isTotalPriceSplitVisible(),
                                child: IgnorePointer(
                                  ignoring: formMAddMajorInsuranceChangeNotifier.disableJenisPenawaran,
                                  child: TextFormField(
                                    enabled: false,
                                    controller: formMAddMajorInsuranceChangeNotifier.controllerTotalPriceSplit,
                                    style: new TextStyle(color: Colors.black),
                                    decoration: new InputDecoration(
                                      labelText: 'Total Biaya Split',
                                      labelStyle: TextStyle(color: Colors.black),
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(8)),
                                      filled: true,
                                      fillColor: Colors.black12,
                                      enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: formMAddMajorInsuranceChangeNotifier.totalPriceSplitDakor ? Colors.purple : Colors.grey)),
                                      disabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: formMAddMajorInsuranceChangeNotifier.totalPriceSplitDakor ? Colors.purple : Colors.grey)),
                                      contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                    ),
                                    keyboardType: TextInputType.number,
                                    textCapitalization: TextCapitalization.characters,
                                    textAlign: TextAlign.end,
                                    inputFormatters: [
                                      WhitelistingTextInputFormatter.digitsOnly
                                    ],
                                    autovalidate: formMAddMajorInsuranceChangeNotifier.autoValidate,
                                    validator: (e) {
                                      if (e.isEmpty && _data.isTotalPriceSplitMandatory()) {
                                        return "Tidak boleh kosong";
                                      } else {
                                        return null;
                                      }
                                    },
                                  ),
                                ),
                              ),
                              Visibility(visible: _data.isTotalPriceSplitVisible(),
                                  child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                              Row(
                                children: [
                                  Visibility(
                                    visible: _data.isTotalPriceRateVisible(),
                                    child: Expanded(
                                      flex: 2,
                                      child: IgnorePointer(
                                        ignoring: formMAddMajorInsuranceChangeNotifier.disableJenisPenawaran,
                                        child: TextFormField(
                                          enabled: false,
                                          controller: formMAddMajorInsuranceChangeNotifier.controllerTotalPriceRate,
                                          style: new TextStyle(color: Colors.black),
                                          decoration: new InputDecoration(
                                            labelText: 'Total Biaya %',
                                            labelStyle: TextStyle(color: Colors.black),
                                            border: OutlineInputBorder(
                                                borderRadius: BorderRadius.circular(8)),
                                            filled: true,
                                            fillColor: Colors.black12,
                                            enabledBorder: OutlineInputBorder(
                                                borderSide: BorderSide(color: formMAddMajorInsuranceChangeNotifier.totalPriceRateDakor ? Colors.purple : Colors.grey)),
                                            disabledBorder: OutlineInputBorder(
                                                borderSide: BorderSide(color: formMAddMajorInsuranceChangeNotifier.totalPriceRateDakor ? Colors.purple : Colors.grey)),
                                            contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                          ),
                                          keyboardType: TextInputType.number,
                                          textCapitalization: TextCapitalization.characters,
                                          inputFormatters: [
                                            WhitelistingTextInputFormatter.digitsOnly
                                          ],
                                          autovalidate: formMAddMajorInsuranceChangeNotifier.autoValidate,
                                          validator: (e) {
                                            if (e.isEmpty && _data.isTotalPriceRateMandatory()) {
                                              return "Tidak boleh kosong";
                                            } else {
                                              return null;
                                            }
                                          },
                                        ),
                                      ),
                                    ),
                                  ),
                                  Visibility(visible: _data.isTotalPriceRateVisible(),
                                      child: SizedBox(width: MediaQuery.of(context).size.width / 47)),
                                  Visibility(
                                    visible: _data.isTotalPriceVisible(),
                                    child: Expanded(
                                      flex: 3,
                                      child: IgnorePointer(
                                        ignoring: formMAddMajorInsuranceChangeNotifier.disableJenisPenawaran,
                                        child: TextFormField(
                                          enabled: false,
                                          textAlign: TextAlign.end,
                                          controller: formMAddMajorInsuranceChangeNotifier.controllerTotalPrice,
                                          style: new TextStyle(color: Colors.black),
                                          decoration: new InputDecoration(
                                            labelText: 'Total Biaya',
                                            labelStyle: TextStyle(color: Colors.black),
                                            border: OutlineInputBorder(
                                                borderRadius: BorderRadius.circular(8)),
                                            filled: true,
                                            fillColor: Colors.black12,
                                            enabledBorder: OutlineInputBorder(
                                                borderSide: BorderSide(color: formMAddMajorInsuranceChangeNotifier.totalPriceDakor ? Colors.purple : Colors.grey)),
                                            disabledBorder: OutlineInputBorder(
                                                borderSide: BorderSide(color: formMAddMajorInsuranceChangeNotifier.totalPriceDakor ? Colors.purple : Colors.grey)),
                                            contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                          ),
                                          keyboardType: TextInputType.number,
                                          textCapitalization: TextCapitalization.characters,
                                          inputFormatters: [
                                            WhitelistingTextInputFormatter.digitsOnly
                                          ],
                                          autovalidate: formMAddMajorInsuranceChangeNotifier.autoValidate,
                                          validator: (e) {
                                            if (e.isEmpty && _data.isTotalPriceMandatory()) {
                                              return "Tidak boleh kosong";
                                            } else {
                                              return null;
                                            }
                                          },
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      );
                    }),
                bottomNavigationBar: BottomAppBar(
                    elevation: 0.0,
                    child: formMAddMajorInsuranceChangeNotifier.isDisablePACIAAOSCONA
                    ? SizedBox()
                    : Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Consumer<FormMAddMajorInsuranceChangeNotifier>(
                            builder: (context, formMAddMajorInsuranceChangeNotifier, _) {
                                return RaisedButton(
                                    shape: RoundedRectangleBorder(
                                        borderRadius: new BorderRadius.circular(8.0)),
                                    color: myPrimaryColor,
                                    onPressed: () {
                                        if (widget.flag == 0) {
                                            formMAddMajorInsuranceChangeNotifier.check(context, widget.flag, null);
                                        } else {
                                            formMAddMajorInsuranceChangeNotifier.check(context, widget.flag, widget.index);
                                        }
                                    },
                                    child: Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: <Widget>[
                                            Text(widget.flag == 0 ? "SAVE" : "UPDATE",
                                                style: TextStyle(
                                                    color: Colors.black,
                                                    fontSize: 14,
                                                    fontWeight: FontWeight.w500,
                                                    letterSpacing: 1.25))
                                        ],
                                    ));
                            },
                        )),
                ),
              );
              },
          )
      );

    }

    Future<bool> _onWillPop() async {
      var _provider = Provider.of<FormMAddMajorInsuranceChangeNotifier>(context, listen: false);
      if (widget.flag == 0) {
        if (_provider.insuranceTypeSelected != null ||
            _provider.controllerAssurance.text != "" ||
            _provider.controllerCompany.text != "" ||
            _provider.controllerProduct.text != "" ||
            _provider.typeSelected != null ||
            _provider.controllerCoverage1.text != "" ||
            _provider.controllerCoverage2.text != "" ||
            _provider.controllerCoverageType.text != "" ||
            _provider.controllerPriceCredit.text != "" ||
            _provider.controllerPriceCash.text != "") {
            return (await showDialog(
                context: context,
                builder: (myContext) => AlertDialog(
                    title: new Text('Warning'),
                    content: new Text('Keluar dengan simpan perubahan?'),
                    actions: <Widget>[
                        new FlatButton(
                            onPressed: () {
                                widget.flag == 0
                                    ?
                                _provider.check(context, widget.flag, null)
                                    :
                                _provider.check(context, widget.flag, widget.index);
                                Navigator.pop(context);
                            },
                            child: new Text('Ya', style: TextStyle(color: Colors.grey),),
                        ),
                        new FlatButton(
                            onPressed: () => Navigator.of(context).pop(true),
                            child: new Text('Tidak'),
                        ),
                    ],
                ),
            )) ??
                false;
        } else {
            return true;
        }
      } else {
          if (widget.formMMajorInsurance.insuranceType.KODE != _provider.insuranceTypeSelected.KODE ||
              widget.formMMajorInsurance.numberOfColla != _provider.numberOfCollateral ||
              widget.formMMajorInsurance.company.KODE != _provider.companySelected.KODE ||
              widget.formMMajorInsurance.product.KODE != _provider.productSelected.KODE ||
              widget.formMMajorInsurance.type != _provider.typeSelected ||
              widget.formMMajorInsurance.coverage1.KODE != _provider.coverage1Selected.KODE ||
              // widget.formMMajorInsurance.coverage2.PARENT_KODE != _provider.coverage2Selected.PARENT_KODE ||
              // widget.formMMajorInsurance.coverageType != _provider.controllerCoverageType.text ||
              widget.formMMajorInsurance.priceCredit != _provider.controllerPriceCredit.text ||
              widget.formMMajorInsurance.priceCash != _provider.controllerPriceCash.text){
              return (await showDialog(
                  context: context,
                  builder: (myContext) => AlertDialog(
                      title: new Text('Warning'),
                      content: new Text('Keluar dengan simpan perubahan?'),
                      actions: <Widget>[
                        Provider.of<FormMAddMajorInsuranceChangeNotifier>(context, listen: false).lastKnownState == "PAC" || Provider.of<FormMAddMajorInsuranceChangeNotifier>(context, listen: false).lastKnownState == "IA"
                        ? null
                        : new FlatButton(
                              onPressed: () {
                                  _provider.check(context, widget.flag, widget.index);
                                  Navigator.pop(context);
                              },
                              child: new Text('Ya', style: TextStyle(color: Colors.grey),),
                          ),
                          new FlatButton(
                              onPressed: () => Navigator.of(context).pop(true),
                              child: new Text('Tidak'),
                          ),
                      ],
                  ),
              )) ??
                  false;
          } else {
              return true;
          }
      }
    }
}
