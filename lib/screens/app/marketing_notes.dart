import 'package:ad1ms2_dev/shared/change_notifier_app/marketing_notes_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class MarketingNotes extends StatefulWidget {
  @override
  _MarketingNotesState createState() => _MarketingNotesState();
}

class _MarketingNotesState extends State<MarketingNotes> {

  Future<void> _setPreference;

  @override
  void initState() {
    _setPreference = Provider.of<MarketingNotesChangeNotifier>(context,listen: false).setPreference(context);
    // Provider.of<MarketingNotesChangeNotifier>(context,listen: false).getDataFromDashboard(context);
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: Provider.of<MarketingNotesChangeNotifier>(context,listen: false).scaffoldKey,
      body: FutureBuilder(
          future: _setPreference,
          builder: (context,snapshot){
            if(snapshot.connectionState == ConnectionState.waiting){
              return Center(
                child: CircularProgressIndicator(),
              );
            }
            return Consumer<MarketingNotesChangeNotifier>(
              builder: (context, value, child) {
                return SingleChildScrollView(
                  padding: EdgeInsets.symmetric(
                      vertical: MediaQuery.of(context).size.height / 37,
                      horizontal: MediaQuery.of(context).size.width / 27),
                  child: Column(
                    children: [
                      Visibility(
                        visible: value.isMarketingNotesVisible(),
                        child: IgnorePointer(
                          ignoring: value.isDisablePACIAAOSCONA,
                          child: TextFormField(
                            autovalidate: value.autoValidate,
                            validator: (e) {
                              if (e.isEmpty && value.isMarketingNotesMandatory()) {
                                return "Tidak boleh kosong";
                              } else {
                                return null;
                              }
                            },
                            keyboardType: TextInputType.text,
                            textCapitalization: TextCapitalization.characters,
                            maxLines: 3,
                            controller: value.controllerMarketingNotes,
                            decoration: InputDecoration(
                              labelText: 'Marketing Notes',
                              labelStyle: TextStyle(color: Colors.black),
                              filled: value.isDisablePACIAAOSCONA,
                              fillColor: Colors.black12,
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(8)
                              ),
                              enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: value.isMarketingNotesDakor ? Colors.purple : Colors.grey)),
                              disabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: value.isMarketingNotesDakor ? Colors.purple : Colors.grey)),
                            ),
                            inputFormatters: [
                              FilteringTextInputFormatter.allow(RegExp('[a-zA-Z 0-9]')),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                );
              },
            );
          }
      ),
    );
  }
}
