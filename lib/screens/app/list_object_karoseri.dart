import 'package:ad1ms2_dev/screens/app/add_info_object_karoseri.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/add_info_object_karoseri_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_object_karoseri_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../main.dart';

class ListObjectKaroseri extends StatefulWidget {
  @override
  _ListObjectKaroseriState createState() => _ListObjectKaroseriState();
}

class _ListObjectKaroseriState extends State<ListObjectKaroseri> {
  @override
  void initState() {
    super.initState();
    Provider.of<InfoObjectKaroseriChangeNotifier>(context, listen: false).setPreference();
  }

  @override
  Widget build(BuildContext context) {
    // Provider.of<InfoObjectKaroseriChangeNotifier>(context,listen: false).deleteSQLite();
    return Theme(
        data: ThemeData(
            fontFamily: "NunitoSans",
            accentColor: myPrimaryColor
        ),
        child: Scaffold(
//            appBar: AppBar(
//                title: Text("List Info Objek Karoseri", style: TextStyle(color: Colors.black)),
//                centerTitle: true,
//                backgroundColor: myPrimaryColor,
//                iconTheme: IconThemeData(color: Colors.black),
//            ),
            body: Consumer<InfoObjectKaroseriChangeNotifier>(
                builder: (context, formMInfoObjKaroseriChangeNotif, _) {
                    return formMInfoObjKaroseriChangeNotif.listFormKaroseriObject.isEmpty
                        ? Center(
                        child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                                // Image.asset("img/alamat_kosong.png", height: MediaQuery.of(context).size.height / 9,),
                                // SizedBox(height: MediaQuery.of(context).size.height / 47,),
                                Text("Tambah Karoseri", style: TextStyle(color: Colors.grey, fontSize: 16),)
                            ],
                        ),
                    )
                    : ListView.builder(
                        padding: EdgeInsets.symmetric(
                            vertical: MediaQuery.of(context).size.height / 47,
                            horizontal: MediaQuery.of(context).size.width / 37),
                        itemBuilder: (context, index) {
                            return InkWell(
                                onTap: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) => ChangeNotifierProvider(
                                                create: (context) => AddInfoObjectKaroseriChangeNotifier(),
                                                child: AddInfoObjectKaroseri(
                                                    flag: 1,
                                                    formMInfoObjectKaroseriModel: formMInfoObjKaroseriChangeNotif.listFormKaroseriObject[index],
                                                    index: index,
                                                ))));
                                },
                                child: Card(
                                    shape: formMInfoObjKaroseriChangeNotif.listFormKaroseriObject[index].isEdit
                                        ? RoundedRectangleBorder(
                                        side: BorderSide(color: Colors.purple, width: 2),
                                        borderRadius: BorderRadius.all(Radius.circular(4)))
                                        : null,
                                    elevation: 3.3,
                                    child: Stack(
                                        children: [
                                            Padding(
                                                padding: const EdgeInsets.all(12.0),
                                                child: Column(
                                                    children: [
                                                        Row(
                                                            children: [
                                                                Expanded(
                                                                    child:
                                                                    Text("PKS Karoseri"),
                                                                    flex: 5),
                                                                Text(" : "),
                                                                Expanded(
                                                                    child: Text(
                                                                        formMInfoObjKaroseriChangeNotif.listFormKaroseriObject[index].PKSKaroseri == 1 ? "Ya" : "Tidak"),
                                                                    flex: 5)
                                                            ],
                                                          ),
                                                        SizedBox(height:MediaQuery.of(context).size.height / 77),
                                                        Row(
                                                            children: [
                                                                Expanded(
                                                                    child: Text(
                                                                        "Perusahaan"),
                                                                    flex: 5),
                                                                Text(" : "),
                                                                Expanded(
                                                                    child: Text(formMInfoObjKaroseriChangeNotif.listFormKaroseriObject[index].company != null ?
                                                                        "${formMInfoObjKaroseriChangeNotif.listFormKaroseriObject[index].company.desc}" : ""),
                                                                    flex: 5)
                                                            ],
                                                        ),
                                                        SizedBox(height:MediaQuery.of(context).size.height / 77),
                                                        Row(
                                                            children: [
                                                                Expanded(
                                                                    child: Text("Karoseri"), flex: 5),
                                                                Text(" : "),
                                                                Expanded(
                                                                    child: Text(
                                                                        "${formMInfoObjKaroseriChangeNotif.listFormKaroseriObject[index].karoseri.deskripsi}"),
                                                                    flex: 5)
                                                            ],
                                                        ),
                                                        SizedBox(height:MediaQuery.of(context).size.height / 77),
                                                        Row(
                                                            children: [
                                                                Expanded(
                                                                    child: Text("Jumlah Karoseri"),
                                                                    flex: 5),
                                                                Text(" : "),
                                                                Expanded(
                                                                    child: Text(
                                                                        "${formMInfoObjKaroseriChangeNotif.listFormKaroseriObject[index].jumlahKaroseri}"),
                                                                    flex: 5)
                                                            ],
                                                        ),
                                                        SizedBox(height:MediaQuery.of(context).size.height / 77),
                                                        Row(
                                                            children: [
                                                                Expanded(
                                                                    child: Text("Harga"),
                                                                    flex: 5),
                                                                Text(" : "),
                                                                Expanded(
                                                                    child: Text(
                                                                        "${formMInfoObjKaroseriChangeNotif.formatCurrency.formatCurrency(formMInfoObjKaroseriChangeNotif.listFormKaroseriObject[index].price)}"),
                                                                    flex: 5)
                                                            ],
                                                        ),
                                                        SizedBox(
                                                            height:
                                                            MediaQuery.of(context).size.height /
                                                                77),
                                                        Row(
                                                            children: [
                                                                Expanded(
                                                                    child: Text("Total Harga"),
                                                                    flex: 5),
                                                                Text(" : "),
                                                                Expanded(
                                                                    child: Text(
                                                                        "${formMInfoObjKaroseriChangeNotif.formatCurrency.formatCurrency(formMInfoObjKaroseriChangeNotif.listFormKaroseriObject[index].totalPrice)}"),
                                                                    flex: 5)
                                                            ],
                                                        ),
                                                    ],
                                                ),
                                            ),
                                            Align(
                                                alignment: Alignment.topRight,
                                                child: IconButton(
                                                    icon: Icon(Icons.delete, color: Colors.red),
                                                    onPressed: () {
                                                        formMInfoObjKaroseriChangeNotif.deleteListInfoObjKaroseri(context, index);
                                                    }),
                                            ),
                                        ],
                                    )),
                            );
                        },
                        itemCount: formMInfoObjKaroseriChangeNotif.listFormKaroseriObject.length);
                },
            ),
            floatingActionButton: Provider.of<InfoObjectKaroseriChangeNotifier>(context, listen: false).isDisablePACIAAOSCONA
                ?
            null
                :
            FloatingActionButton(
                onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ChangeNotifierProvider(
                                create: (context) => AddInfoObjectKaroseriChangeNotifier(),
                                child: AddInfoObjectKaroseri(
                                    flag: 0,
                                    formMInfoObjectKaroseriModel: null,
                                    index: null,
                                ))));
                },
                child: Icon(Icons.add, color: Colors.black),
                backgroundColor: myPrimaryColor,
            ),
        ),
    );
  }
}

