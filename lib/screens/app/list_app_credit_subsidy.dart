import 'package:ad1ms2_dev/screens/app/add_credit_subsidy.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/add_credit_subsidy_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_credit_subsidy_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../main.dart';

class ListAppCreditSubsidy extends StatefulWidget {
  @override
  _ListAppCreditSubsidyState createState() => _ListAppCreditSubsidyState();
}

class _ListAppCreditSubsidyState extends State<ListAppCreditSubsidy> {
    @override
    void initState() {
        super.initState();
        Provider.of<InfoCreditSubsidyChangeNotifier>(context, listen: false).setPreference();
    }

    @override
    Widget build(BuildContext context) {
        return Theme(
            data: ThemeData(
                fontFamily: "NunitoSans",
                accentColor: myPrimaryColor
            ),
            child: Scaffold(
                body: Consumer<InfoCreditSubsidyChangeNotifier>(
                    builder: (context, infoCreditSubsidyChangeNotifier, _) {
                        return infoCreditSubsidyChangeNotifier.listInfoCreditSubsidy.isEmpty
                            ? Center(
                            child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                    // Image.asset("img/alamat_kosong.png", height: MediaQuery.of(context).size.height / 9,),
                                    // SizedBox(height: MediaQuery.of(context).size.height / 47,),
                                    Text("Tambah Subsidi", style: TextStyle(color: Colors.grey, fontSize: 16),)
                                ],
                            ),
                        )
                            :
                        ListView.builder(
                            padding: EdgeInsets.symmetric(
                                vertical: MediaQuery.of(context).size.height / 47,
                                horizontal: MediaQuery.of(context).size.width / 37),
                            itemBuilder: (context, index) {
                                return InkWell(
                                    onTap: () {
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) => ChangeNotifierProvider(
                                                    create: (context) => AddCreditSubsidyChangeNotifier(),
                                                    child: AddInfoCreditSubsidy(
                                                        flag: 1,
                                                        formMInfoCreditSubsidyModel: infoCreditSubsidyChangeNotifier.listInfoCreditSubsidy[index],
                                                        index: index,
                                                    ))));
                                    },
                                    child: Card(
                                        shape: infoCreditSubsidyChangeNotifier.listInfoCreditSubsidy[index].isEditDakor
                                            ? RoundedRectangleBorder(
                                            side: BorderSide(color: Colors.purple, width: 2),
                                            borderRadius: BorderRadius.all(Radius.circular(4)))
                                            : null,
                                        elevation: 3.3,
                                        child: Stack(
                                            children: [
                                                Padding(
                                                    padding: const EdgeInsets.all(12.0),
                                                    child: Column(
                                                        children: [
                                                            Row(
                                                                children: [
                                                                    Expanded(
                                                                        child: Text(
                                                                            "Pemberi"),
                                                                        flex: 5),
                                                                    Text(" : "),
                                                                    Expanded(
                                                                        child: Text(
                                                                            infoCreditSubsidyChangeNotifier.listInfoCreditSubsidy[index].giver == "0" ? "Adira" : "Dealer/Merchant/AXI/Pemasok"),
                                                                        flex: 5)
                                                                ],
                                                            ),
                                                            SizedBox(
                                                                height:
                                                                MediaQuery.of(context).size.height /
                                                                    77),
                                                            Row(
                                                                children: [
                                                                    Expanded(
                                                                        child: Text("Jenis"), flex: 5),
                                                                    Text(" : "),
                                                                    Expanded(
                                                                        child: Text(
                                                                            infoCreditSubsidyChangeNotifier.listInfoCreditSubsidy[index].type.text),
                                                                        flex: 5)
                                                                ],
                                                            ),
                                                            SizedBox(
                                                                height:
                                                                MediaQuery.of(context).size.height /
                                                                    77),
                                                            Row(
                                                                children: [
                                                                    Expanded(
                                                                        child: Text("Metode Pemotongan"),
                                                                        flex: 5),
                                                                    Text(" : "),
                                                                    Expanded(
                                                                        child: Text(infoCreditSubsidyChangeNotifier.listInfoCreditSubsidy[index].cuttingMethod != null ?
                                                                            infoCreditSubsidyChangeNotifier.listInfoCreditSubsidy[index].cuttingMethod.text : "-"),
                                                                        flex: 5)
                                                                ],
                                                            ),
                                                            SizedBox(
                                                                height:
                                                                MediaQuery.of(context).size.height /
                                                                    77),
                                                            Row(
                                                                children: [
                                                                    Expanded(
                                                                        child: Text("Nilai Potongan"),
                                                                        flex: 5),
                                                                    Text(" : "),
                                                                    Expanded(
                                                                        child: Text(infoCreditSubsidyChangeNotifier.listInfoCreditSubsidy[index].value != null ?
                                                                        infoCreditSubsidyChangeNotifier.formatCurrency.formatCurrency(infoCreditSubsidyChangeNotifier.listInfoCreditSubsidy[index].value) : "0.00"),
                                                                        flex: 5)
                                                                ],
                                                            ),
                                                            SizedBox(
                                                                height:
                                                                MediaQuery.of(context).size.height /
                                                                    77),
                                                            Row(
                                                                children: [
                                                                    Expanded(
                                                                        child: Text("Nilai Klaim"),
                                                                        flex: 5),
                                                                    Text(" : "),
                                                                    Expanded(
                                                                        child: Text(infoCreditSubsidyChangeNotifier.listInfoCreditSubsidy[index].claimValue != null ?
                                                                        infoCreditSubsidyChangeNotifier.formatCurrency.formatCurrency(infoCreditSubsidyChangeNotifier.listInfoCreditSubsidy[index].claimValue) : "0.00"),
                                                                        flex: 5)
                                                                ],
                                                            ),
                                                        ],
                                                    ),
                                                ),
                                                Align(
                                                    alignment: Alignment.topRight,
                                                    child: IconButton(
                                                        icon: Icon(Icons.delete, color: Colors.red),
                                                        onPressed: () {
                                                            infoCreditSubsidyChangeNotifier
                                                                .deleteListInfoCreditSubsidy(context, index);
                                                        }),
                                                ),
                                            ],
                                        )),
                                );
                            },
                            itemCount: infoCreditSubsidyChangeNotifier.listInfoCreditSubsidy.length);
                    },
                ),
                floatingActionButton: Provider.of<InfoCreditSubsidyChangeNotifier>(context, listen: false).isDisablePACIAAOSCONA
                    || Provider.of<InfoCreditSubsidyChangeNotifier>(context, listen: true).listInfoCreditSubsidy.length > 0
                    ?
                null
                    :
                FloatingActionButton(
                    onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ChangeNotifierProvider(
                                    create: (context) => AddCreditSubsidyChangeNotifier(),
                                    child: AddInfoCreditSubsidy(
                                        flag: 0,
                                        formMInfoCreditSubsidyModel: null,
                                        index: null,
                                    ))));
                    },
                    child: Icon(Icons.add, color: Colors.black),
                    backgroundColor: myPrimaryColor,
                ),
            ),
        );
    }
}

