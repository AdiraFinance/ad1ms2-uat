import 'package:ad1ms2_dev/screens/app/add_major_insurence.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/form_m_add_major_insurance_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_major_insurance_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_object_karoseri_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../main.dart';

class ListMajorInsurance extends StatefulWidget {
  @override
  _ListMajorInsuranceState createState() => _ListMajorInsuranceState();
}

class _ListMajorInsuranceState extends State<ListMajorInsurance> {
    @override
    void initState() {
        super.initState();
        Provider.of<InfoMajorInsuranceChangeNotifier>(context, listen: false).getDataColla(context);
        Provider.of<InfoMajorInsuranceChangeNotifier>(context, listen: false).setPreference();
    }

    @override
    Widget build(BuildContext context) {
        return Theme(
            data: ThemeData(
                fontFamily: "NunitoSans",
                accentColor: myPrimaryColor
            ),
            child: Scaffold(
                appBar: AppBar(
                    title:
                    Text("List Asuransi Utama", style: TextStyle(color: Colors.black)),
                    centerTitle: true,
                    backgroundColor: myPrimaryColor,
                    iconTheme: IconThemeData(color: Colors.black),
                ),
                body: Consumer<InfoMajorInsuranceChangeNotifier>(
                    builder: (context, infoMajorInsuranceChangeNotifier, _) {
                        return Form(
                          onWillPop: infoMajorInsuranceChangeNotifier.onBackPress,
                          child: infoMajorInsuranceChangeNotifier.listFormMajorInsurance.isEmpty
                              ? Center(
                              child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                      // Image.asset("img/alamat_kosong.png", height: MediaQuery.of(context).size.height / 9,),
                                      // SizedBox(height: MediaQuery.of(context).size.height / 47,),
                                      Text("Tambah Asuransi Utama", style: TextStyle(color: Colors.grey, fontSize: 16),)
                                  ],
                              ),
                          )
                          : ListView.builder(
                              padding: EdgeInsets.symmetric(
                                  vertical: MediaQuery.of(context).size.height / 47,
                                  horizontal: MediaQuery.of(context).size.width / 37),
                              itemBuilder: (context, index) {
                                  return InkWell(
                                      onTap: () {
                                          Navigator.push(context, MaterialPageRoute(
                                                  builder: (context) => ChangeNotifierProvider(
                                                      create: (context) => FormMAddMajorInsuranceChangeNotifier(),
                                                      child: AddMajorInsurance(
                                                          flag: 1,
                                                          formMMajorInsurance: infoMajorInsuranceChangeNotifier.listFormMajorInsurance[index],
                                                          index: index,
                                                      ))));
                                      },
                                      child: Card(
                                          shape: infoMajorInsuranceChangeNotifier.listFormMajorInsurance[index].isEdit
                                              ? RoundedRectangleBorder(
                                              side: BorderSide(color: Colors.purple, width: 2),
                                              borderRadius: BorderRadius.all(Radius.circular(4)))
                                              : null,
                                          elevation: 3.3,
                                          child: Stack(
                                              children: [
                                                  Padding(
                                                      padding: const EdgeInsets.all(12.0),
                                                      child: Column(
                                                          children: [
                                                              Row(
                                                                  children: [
                                                                      Expanded(child: Text("Jaminan Ke"), flex: 5),
                                                                      Text(" : "),
                                                                      Expanded(
                                                                          child: Text("${infoMajorInsuranceChangeNotifier.listFormMajorInsurance[index].numberOfColla}"),
                                                                          flex: 5
                                                                      )
                                                                  ],
                                                              ),
                                                              SizedBox(height: MediaQuery.of(context).size.height / 77),
                                                              Row(
                                                                  children: [
                                                                      Expanded(child: Text("Tipe Asuransi"), flex: 5),
                                                                      Text(" : "),
                                                                      Expanded(
                                                                          child: Text(infoMajorInsuranceChangeNotifier.listFormMajorInsurance[index].insuranceType != null ? "${infoMajorInsuranceChangeNotifier.listFormMajorInsurance[index].insuranceType.DESKRIPSI}" : ""),
                                                                          flex: 5
                                                                      )
                                                                  ],
                                                              ),
                                                              SizedBox(height: MediaQuery.of(context).size.height / 77),
                                                              Row(
                                                                  children: [
                                                                      Expanded(child: Text("Perusahaan"), flex: 5),
                                                                      Text(" : "),
                                                                      Expanded(
                                                                          child: Text(infoMajorInsuranceChangeNotifier.listFormMajorInsurance[index].company != null ? "${infoMajorInsuranceChangeNotifier.listFormMajorInsurance[index].company.DESKRIPSI}" : ""),
                                                                          flex: 5)
                                                                  ],
                                                              ),
                                                              SizedBox(height:MediaQuery.of(context).size.height / 77),
                                                              Row(
                                                                  children: [
                                                                      Expanded(child: Text("Produk"), flex: 5),
                                                                      Text(" : "),
                                                                      Expanded(
                                                                          child: Text(infoMajorInsuranceChangeNotifier.listFormMajorInsurance[index].product != null ? "${infoMajorInsuranceChangeNotifier.listFormMajorInsurance[index].product.DESKRIPSI}" : ""),
                                                                          flex: 5)
                                                                  ],
                                                              ),
                                                              SizedBox(height: MediaQuery.of(context).size.height / 77),
                                                              Row(
                                                                  children: [
                                                                      Expanded(child: Text("Biaya Tunai"), flex: 5),
                                                                      Text(" : "),
                                                                      Expanded(
                                                                          child: Text(infoMajorInsuranceChangeNotifier.listFormMajorInsurance[index].priceCash != null ?
                                                                          infoMajorInsuranceChangeNotifier.formatCurrency.formatCurrency(infoMajorInsuranceChangeNotifier.listFormMajorInsurance[index].priceCash) : "0.00"),
                                                                          flex: 5)
                                                                  ],
                                                              ),
                                                              SizedBox(height: MediaQuery.of(context).size.height / 77),
                                                              Row(
                                                                  children: [
                                                                      Expanded(child: Text("Biaya Kredit"), flex: 5),
                                                                      Text(" : "),
                                                                      Expanded(
                                                                          child: Text(infoMajorInsuranceChangeNotifier.listFormMajorInsurance[index].priceCredit != null ?
                                                                          infoMajorInsuranceChangeNotifier.formatCurrency.formatCurrency(infoMajorInsuranceChangeNotifier.listFormMajorInsurance[index].priceCredit) : "0.00"),
                                                                          flex: 5)
                                                                  ],
                                                              ),
                                                              SizedBox(height: MediaQuery.of(context).size.height / 77),
                                                              Row(
                                                                  children: [
                                                                      Expanded(child: Text("Total Biaya"), flex: 5),
                                                                      Text(" : "),
                                                                      Expanded(
                                                                          child: Text(infoMajorInsuranceChangeNotifier.listFormMajorInsurance[index].totalPrice != null ?
                                                                          infoMajorInsuranceChangeNotifier.formatCurrency.formatCurrency(infoMajorInsuranceChangeNotifier.listFormMajorInsurance[index].totalPrice) : "0.00"),
                                                                          flex: 5)
                                                                  ],
                                                              ),
                                                          ],
                                                      ),
                                                  ),
                                                  Align(
                                                      alignment: Alignment.topRight,
                                                      child: Provider.of<InfoMajorInsuranceChangeNotifier>(context, listen: false).lastKnownState == "PAC" || Provider.of<InfoMajorInsuranceChangeNotifier>(context, listen: false).lastKnownState == "IA" || Provider.of<InfoMajorInsuranceChangeNotifier>(context, listen: false).lastKnownState == "AOS" || Provider.of<InfoMajorInsuranceChangeNotifier>(context, listen: false).lastKnownState == "CONA"
                                                      ? SizedBox()
                                                      : IconButton(
                                                          icon: Icon(Icons.delete, color: Colors.red),
                                                          onPressed: () {
                                                              infoMajorInsuranceChangeNotifier.deleteListInfoMajorInsurance(context, index);
                                                          }),
                                                  ),
                                              ],
                                          )),
                                  );
                              },
                              itemCount: infoMajorInsuranceChangeNotifier.listFormMajorInsurance.length),
                        );
                    },
                ),
                floatingActionButton: Provider.of<InfoMajorInsuranceChangeNotifier>(context, listen: false).lastKnownState == "PAC" || Provider.of<InfoMajorInsuranceChangeNotifier>(context, listen: false).lastKnownState == "IA" || Provider.of<InfoMajorInsuranceChangeNotifier>(context, listen: false).lastKnownState == "AOS" || Provider.of<InfoMajorInsuranceChangeNotifier>(context, listen: false).lastKnownState == "CONA"
                ? null
                : FloatingActionButton(
                    onPressed: () {
                        Provider.of<FormMAddMajorInsuranceChangeNotifier>(context, listen: false).clearData();
                        Navigator.push(context, MaterialPageRoute(
                                builder: (context) => ChangeNotifierProvider(
                                    create: (context) => FormMAddMajorInsuranceChangeNotifier(),
                                    child: AddMajorInsurance(
                                        flag: 0,
                                        formMMajorInsurance: null,
                                        index: null,
                                    ))));
                    },
                    child: Icon(Icons.add, color: Colors.black),
                    backgroundColor: myPrimaryColor,
                ),
            ),
        );
    }
}

