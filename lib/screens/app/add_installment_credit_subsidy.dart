import 'package:ad1ms2_dev/models/add_detail_installment_credit_subsidy.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/add_installment_credit_subsidy_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

import '../../main.dart';

class AddInstallment extends StatefulWidget {
  final ValueChanged<AddInstallmentDetailModel> addItems;

  const AddInstallment({Key key, this.addItems});

  @override
  _AddInstallmentState createState() => _AddInstallmentState();
}

class _AddInstallmentState extends State<AddInstallment> {
  @override
  Widget build(BuildContext context) {
    var _size = MediaQuery.of(context).size;
    return Theme(
        data: ThemeData(
            primaryColor: Colors.black,
            accentColor: myPrimaryColor,
            fontFamily: "NunitoSans",
            primarySwatch: Colors.yellow),
        child: Scaffold(
            appBar: AppBar(
                backgroundColor: myPrimaryColor,
                title: Text("Add Angsuran Ke ", style: TextStyle(color: Colors.black)),
                centerTitle: true,
                iconTheme: IconThemeData(
                    color: Colors.black,
                ),
            ),
            body: Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: MediaQuery.of(context).size.width / 27,
                  vertical: MediaQuery.of(context).size.height / 57),
              child: Consumer<AddInstallmentCreditSubsidyChangeNotifier>(
                  builder: (consumerContext, addInstallment, _){
                      return Form(
                          key: addInstallment.key,
                          child: Column(
                              children: [
                                  TextFormField(
                                      controller: addInstallment.controllerInstallmentIndex,
                                      style: new TextStyle(color: Colors.black),
                                      decoration: new InputDecoration(
                                          labelText: 'Angsuran Ke',
                                          labelStyle: TextStyle(color: Colors.black),
                                          border: OutlineInputBorder(
                                              borderRadius: BorderRadius.circular(8))),
                                      keyboardType: TextInputType.number,
                                      textCapitalization: TextCapitalization.characters,
                                      inputFormatters: [
                                          WhitelistingTextInputFormatter.digitsOnly
                                      ],
                                      autovalidate: addInstallment.autoValidate,
                                      validator: (e) {
                                          if (e.isEmpty) {
                                              return "Tidak boleh kosong";
                                          } else {
                                              return null;
                                          }
                                      },
                                  ),
                                  SizedBox(height: _size.height / 47),
                                  TextFormField(
                                      controller: addInstallment.controllerInstallmentSubsidy,
                                      style: new TextStyle(color: Colors.black),
                                      decoration: new InputDecoration(
                                          labelText: 'Subsidi Angsuran',
                                          labelStyle: TextStyle(color: Colors.black),
                                          border: OutlineInputBorder(
                                              borderRadius: BorderRadius.circular(8))),
                                      keyboardType: TextInputType.number,
                                      textCapitalization: TextCapitalization.characters,
                                      inputFormatters: [
                                          WhitelistingTextInputFormatter.digitsOnly
                                      ],
                                      autovalidate: addInstallment.autoValidate,
                                      validator: (e) {
                                          if (e.isEmpty) {
                                              return "Tidak boleh kosong";
                                          } else {
                                              return null;
                                          }
                                      },
                                  ),
                              ],
                          ),
                      );
                  },
              )
            ),
            bottomNavigationBar: BottomAppBar(
                elevation: 0.0,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: RaisedButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(8.0)),
                      color: myPrimaryColor,
                      onPressed: () {
                          Provider.of<AddInstallmentCreditSubsidyChangeNotifier>(context, listen: false).addDetail(context);
                      },
                      child: Text("Add"),
                  )
                ),
            ),
        ),
    );
  }

}
