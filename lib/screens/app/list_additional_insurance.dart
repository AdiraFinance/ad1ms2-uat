
import 'package:ad1ms2_dev/shared/change_notifier_app/form_m_add_additional_insurance_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_additional_insurance_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_major_insurance_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_object_karoseri_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../main.dart';
import 'add_additional_insurence.dart';

class ListAdditionalInsurance extends StatefulWidget {
  @override
  _ListAdditionalInsuranceState createState() => _ListAdditionalInsuranceState();
}

class _ListAdditionalInsuranceState extends State<ListAdditionalInsurance> {
    @override
    void initState() {
      super.initState();
      // Provider.of<InfoAdditionalInsuranceChangeNotifier>(context, listen: false).getDataColla(context);
      Provider.of<InfoAdditionalInsuranceChangeNotifier>(context, listen: false).getMandatory(context);
      Provider.of<InfoAdditionalInsuranceChangeNotifier>(context, listen: false).setPreference();
    }

    @override
    Widget build(BuildContext context) {
        return Theme(
            data: ThemeData(
                fontFamily: "NunitoSans",
                accentColor: myPrimaryColor
            ),
            child: Scaffold(
                appBar: AppBar(
                    title:
                    Text("List Asuransi Tambahan", style: TextStyle(color: Colors.black)),
                    centerTitle: true,
                    backgroundColor: myPrimaryColor,
                    iconTheme: IconThemeData(color: Colors.black),
                ),
                body: Consumer<InfoAdditionalInsuranceChangeNotifier>(
                    builder: (context, infoAdditionalInsuranceChangeNotifier, _) {
                        return Form(
                          onWillPop: infoAdditionalInsuranceChangeNotifier.onBackPress,
                          child: infoAdditionalInsuranceChangeNotifier.listFormAdditionalInsurance.isEmpty
                          ? Center(
                              child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                      // Image.asset("img/alamat_kosong.png", height: MediaQuery.of(context).size.height / 9,),
                                      // SizedBox(height: MediaQuery.of(context).size.height / 47,),
                                      Text("Tambah Asuransi Tambahan", style: TextStyle(color: Colors.grey, fontSize: 16),)
                                  ],
                              ),
                          )
                          : ListView.builder(
                              padding: EdgeInsets.symmetric(
                                  vertical: MediaQuery.of(context).size.height / 47,
                                  horizontal: MediaQuery.of(context).size.width / 37),
                              itemBuilder: (context, index) {
                                  return InkWell(
                                      onTap: () {
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) => ChangeNotifierProvider(
                                                      create: (context) => FormMAddAdditionalInsuranceChangeNotifier(),
                                                      child: AddAdditionalInsurance(
                                                          flag: 1,
                                                          formMAdditionalInsurance: infoAdditionalInsuranceChangeNotifier.listFormAdditionalInsurance[index],
                                                          index: index,
                                                      ))));
                                      },
                                      child: Card(
                                          shape: infoAdditionalInsuranceChangeNotifier.listFormAdditionalInsurance[index].isEdit
                                              ? RoundedRectangleBorder(
                                              side: BorderSide(color: Colors.purple, width: 2),
                                              borderRadius: BorderRadius.all(Radius.circular(4)))
                                              : null,
                                          elevation: 3.3,
                                          child: Stack(
                                              children: [
                                                  Padding(
                                                      padding: const EdgeInsets.all(12.0),
                                                      child: Column(
                                                          children: [
                                                              Row(
                                                                  children: [
                                                                      Expanded(
                                                                          child: Text(
                                                                              "Tipe Asuransi"),
                                                                          flex: 5),
                                                                      Text(" : "),
                                                                      Expanded(
                                                                          child: Text(
                                                                              infoAdditionalInsuranceChangeNotifier.listFormAdditionalInsurance[index].insuranceType.DESKRIPSI),
                                                                          flex: 5)
                                                                  ],
                                                              ),
                                                              SizedBox(
                                                                  height:
                                                                  MediaQuery.of(context).size.height /
                                                                      77),
                                                              Row(
                                                                  children: [
                                                                      Expanded(
                                                                          child: Text("Perusahaan"), flex: 5),
                                                                      Text(" : "),
                                                                      Expanded(
                                                                          child: Text(
                                                                              infoAdditionalInsuranceChangeNotifier.listFormAdditionalInsurance[index].company != null ?
                                                                              infoAdditionalInsuranceChangeNotifier.listFormAdditionalInsurance[index].company.DESKRIPSI
                                                                                  :
                                                                               ""),
                                                                          flex: 5)
                                                                  ],
                                                              ),
                                                              SizedBox(
                                                                  height:
                                                                  MediaQuery.of(context).size.height /
                                                                      77),
                                                              Row(
                                                                  children: [
                                                                      Expanded(
                                                                          child: Text("Produk"),
                                                                          flex: 5),
                                                                      Text(" : "),
                                                                      Expanded(
                                                                          child: Text(
                                                                              infoAdditionalInsuranceChangeNotifier.listFormAdditionalInsurance[index].product.DESKRIPSI),
                                                                          flex: 5)
                                                                  ],
                                                              ),
                                                              SizedBox(
                                                                  height:
                                                                  MediaQuery.of(context).size.height /
                                                                      77),
                                                              Row(
                                                                  children: [
                                                                      Expanded(
                                                                          child: Text("Biaya Tunai"),
                                                                          flex: 5),
                                                                      Text(" : "),
                                                                      Expanded(
                                                                          child: Text(infoAdditionalInsuranceChangeNotifier.listFormAdditionalInsurance[index].priceCash != null ?
                                                                          infoAdditionalInsuranceChangeNotifier.formatCurrency.formatCurrency(infoAdditionalInsuranceChangeNotifier.listFormAdditionalInsurance[index].priceCash) : "0.00"),
                                                                          flex: 5)
                                                                  ],
                                                              ),
                                                              SizedBox(
                                                                  height:
                                                                  MediaQuery.of(context).size.height /
                                                                      77),
                                                              Row(
                                                                  children: [
                                                                      Expanded(
                                                                          child: Text("Biaya Kredit"),
                                                                          flex: 5),
                                                                      Text(" : "),
                                                                      Expanded(
                                                                          child: Text(infoAdditionalInsuranceChangeNotifier.listFormAdditionalInsurance[index].priceCredit != null ?
                                                                          infoAdditionalInsuranceChangeNotifier.formatCurrency.formatCurrency(infoAdditionalInsuranceChangeNotifier.listFormAdditionalInsurance[index].priceCredit) : "0.00"),
                                                                          flex: 5)
                                                                  ],
                                                              ),
                                                              SizedBox(
                                                                  height:
                                                                  MediaQuery.of(context).size.height /
                                                                      77),
                                                              Row(
                                                                  children: [
                                                                      Expanded(
                                                                          child: Text("Total Biaya"),
                                                                          flex: 5),
                                                                      Text(" : "),
                                                                      Expanded(
                                                                          child: Text(infoAdditionalInsuranceChangeNotifier.listFormAdditionalInsurance[index].totalPrice != null ?
                                                                          infoAdditionalInsuranceChangeNotifier.formatCurrency.formatCurrency(infoAdditionalInsuranceChangeNotifier.listFormAdditionalInsurance[index].totalPrice) : "0.00"),
                                                                          flex: 5)
                                                                  ],
                                                              ),
                                                          ],
                                                      ),
                                                  ),
                                                  Align(
                                                      alignment: Alignment.topRight,
                                                      child: Provider.of<InfoAdditionalInsuranceChangeNotifier>(context, listen: false).lastKnownState == "PAC" || Provider.of<InfoAdditionalInsuranceChangeNotifier>(context, listen: false).lastKnownState == "IA" || Provider.of<InfoAdditionalInsuranceChangeNotifier>(context, listen: false).lastKnownState == "AOS" || Provider.of<InfoAdditionalInsuranceChangeNotifier>(context, listen: false).lastKnownState == "CONA"
                                                      ? SizedBox()
                                                      : IconButton(
                                                          icon: Icon(Icons.delete, color: Colors.red),
                                                          onPressed: () {
                                                              infoAdditionalInsuranceChangeNotifier.deleteListInfoAdditionalInsurance(context, index);
                                                          }),
                                                  ),
                                              ],
                                          )),
                                  );
                              },
                              itemCount: infoAdditionalInsuranceChangeNotifier.listFormAdditionalInsurance.length),
                        );
                    },
                ),
                floatingActionButton: Provider.of<InfoAdditionalInsuranceChangeNotifier>(context, listen: false).lastKnownState == "PAC" || Provider.of<InfoAdditionalInsuranceChangeNotifier>(context, listen: false).lastKnownState == "IA" || Provider.of<InfoAdditionalInsuranceChangeNotifier>(context, listen: false).lastKnownState == "AOS" || Provider.of<InfoAdditionalInsuranceChangeNotifier>(context, listen: false).lastKnownState == "CONA"
                ? null
                : FloatingActionButton(
                    onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ChangeNotifierProvider(
                                    create: (context) => FormMAddAdditionalInsuranceChangeNotifier(),
                                    child: AddAdditionalInsurance(
                                        flag: 0,
                                        formMAdditionalInsurance: null,
                                        index: null,
                                    ))));
                    },
                    child: Icon(Icons.add, color: Colors.black),
                    backgroundColor: myPrimaryColor,
                ),
            ),
        );
    }
}

