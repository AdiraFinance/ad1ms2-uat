import 'package:ad1ms2_dev/screens/form_m/form_m_info_nasabah.dart';
import 'package:ad1ms2_dev/screens/form_m/form_m_informasi_alamat.dart';
import 'package:ad1ms2_dev/screens/form_m/form_m_informasi_keluarga(ibu).dart';
import 'package:ad1ms2_dev/screens/form_m/list_informasi_keluarga.dart';
import 'package:ad1ms2_dev/shared/form_m_add_address_individu_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_individu_change_notifier/form_m_parent_individu_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_info_keluarga_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_info_nasabah_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_informasi_alamat_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_informasi_keluarga(ibu)_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_occupation_change_notif.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../main.dart';

class MenuCustomerDetail extends StatefulWidget {
  @override
  MenuCustomerDetailState createState() => MenuCustomerDetailState();
}

class MenuCustomerDetailState extends State<MenuCustomerDetail> {

    @override
    void initState() {
        Provider.of<FormMInfoNasabahChangeNotif>(context, listen: false).setDataSQLite();
        Provider.of<FormMInfoAlamatChangeNotif>(context, listen: false).setDataFromSQLite("5");
        Provider.of<FormMInformasiKeluargaIBUChangeNotif>(context, listen: false).setDataSQLite();
        Provider.of<FormMInfoKeluargaChangeNotif>(context, listen: false).setDataFromSQLite();
        super.initState();
    }

    @override
    Widget build(BuildContext context) {
        var _providerInfoNasabah = Provider.of<FormMInfoNasabahChangeNotif>(context, listen: true);
        var _providerInfoAlamat = Provider.of<FormMInfoAlamatChangeNotif>(context, listen: true);
        var _providerInfoKeluargaIbu = Provider.of<FormMInformasiKeluargaIBUChangeNotif>(context, listen: true);
        var _providerInfoKeluarga = Provider.of<FormMInfoKeluargaChangeNotif>(context, listen: true);

        return Padding(
            padding: EdgeInsets.symmetric(
                horizontal: MediaQuery.of(context).size.width/37,
                vertical: MediaQuery.of(context).size.height/57,
            ),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                    InkWell(
                        onTap: () async{
                           // _providerInfoNasabah.getInfoNasabahShowHide();
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => FormMInfoNasabah()
                                )
                            );
                        },
                        child: Card(
                            elevation: 3.3,
                            child: Padding(
                                padding: const EdgeInsets.all(16.0),
                                child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                        Text(
                                            "Informasi Nasabah"),
                                            _providerInfoNasabah.flag
                                            ? Icon(Icons.check, color: primaryOrange)
                                            : SizedBox()
                                    ],
                                ),
                            ),
                        ),
                    ),
                    _providerInfoNasabah.autoValidate
                    ? Container(
                        margin: EdgeInsets.only(left: MediaQuery.of(context).size.width / 37),
                        child: Text("Data belum lengkap",style: TextStyle(color: Colors.red, fontSize: 12)),
                    )
                    : SizedBox(height: MediaQuery.of(context).size.height / 87),
                    SizedBox(height: MediaQuery.of(context).size.height / 87),
                    InkWell(
                        onTap: () {
                            Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setDataAddressIndividu(context);
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => FormMInformasiAlamat()));
                        },
                        child: Card(
                            elevation: 3.3,
                            child: Padding(
                                padding: const EdgeInsets.all(16.0),
                                child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                        Text(
                                            "Informasi Alamat"),
                                        _providerInfoAlamat.flag
                                            ? Icon(Icons.check, color: primaryOrange)
                                            : SizedBox()
                                    ],
                                ),
                            ),
                        ),
                    ),
                    _providerInfoAlamat.autoValidate
                    ? Container(
                        margin: EdgeInsets.only(left: MediaQuery.of(context).size.width / 37),
                        child: Text("Data belum lengkap",style: TextStyle(color: Colors.red, fontSize: 12)),
                    )
                    : SizedBox(height: MediaQuery.of(context).size.height / 87),
                    SizedBox(height: MediaQuery.of(context).size.height / 87),
                    InkWell(
                        onTap: () {
                            // _providerInfoKeluargaIbu.deleteSQLite();
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => FormMInformasiKeluargaIBU()));
                        },
                        child: Card(
                            elevation: 3.3,
                            child: Padding(
                                padding: const EdgeInsets.all(16.0),
                                child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                        Text(
                                            "Informasi Keluarga (Ibu)"),
                                        _providerInfoKeluargaIbu.flag
                                            ? Icon(Icons.check, color: primaryOrange)
                                            : SizedBox()
                                    ],
                                ),
                            ),
                        ),
                    ),
                    _providerInfoKeluargaIbu.autoValidate
                    ? Container(
                        margin: EdgeInsets.only(left: MediaQuery.of(context).size.width / 37),
                        child: Text("Data belum lengkap",style: TextStyle(color: Colors.red, fontSize: 12)),
                    )
                    : SizedBox(height: MediaQuery.of(context).size.height / 87),
                    SizedBox(height: MediaQuery.of(context).size.height / 87),
                    InkWell(
                        onTap: () {
                            // _providerInfoKeluarga.deleteSQLite();
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => ListInfoKeluarga()));
                        },
                        child: Card(
                            elevation: 3.3,
                            child: Padding(
                                padding: const EdgeInsets.all(16.0),
                                child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                        Text(
                                            "Informasi Keluarga"),
                                        _providerInfoKeluarga.listFormInfoKel.length != 0 && _providerInfoKeluarga.isCheckData
                                            ? Icon(Icons.check, color: primaryOrange)
                                            : SizedBox()
                                    ],
                                ),
                            ),
                        ),
                    ),
                    _providerInfoKeluarga.isPasanganExist
                            ? Container(
                               margin: EdgeInsets.only(left: MediaQuery.of(context).size.width / 37),
                               child: Text("Harap isi data pasangan",style: TextStyle(color: Colors.red, fontSize: 12)),
                            )
                            : SizedBox(height: MediaQuery.of(context).size.height / 87)
                ],
            ),
        );
    }

    Future<void> setNextState(BuildContext context,int index) async{
        SharedPreferences _preferences = await SharedPreferences.getInstance();
        var _providerFormMCustomerInfoChangeNotifier = Provider.of<FormMInfoNasabahChangeNotif>(context, listen: false);
        var _providerInfoAlamat = Provider.of<FormMInfoAlamatChangeNotif>(context, listen: false);
        var _providerInfoKeluargaIbu = Provider.of<FormMInformasiKeluargaIBUChangeNotif>(context, listen: false);
        var _providerInfoKeluarga = Provider.of<FormMInfoKeluargaChangeNotif>(context, listen: false);
        var _providerOccupationChangeNotif = Provider.of<FormMOccupationChangeNotif>(context, listen: false);

        await _providerFormMCustomerInfoChangeNotifier.setDataSQLite();
        await _providerInfoAlamat.setDataFromSQLite("5");
        await _providerInfoKeluargaIbu.setDataSQLite();
        await _providerInfoKeluarga.setDataFromSQLite();
        if(_preferences.getString("last_known_state") == "IDE" && index != null){
            if(index != 1){
                _providerFormMCustomerInfoChangeNotifier.flag = true;
                _providerInfoAlamat.flag = true;
                _providerInfoKeluargaIbu.flag = true;
                _providerInfoKeluarga.isCheckData = true;

                Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).isMenuCustomerDetailDone = true;
                Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).selectedIndex += 1;
                await _providerOccupationChangeNotif.setDataFromSQLite(context,"4",index);
            }
            else{
                Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).selectedIndex = 1;
            }
        }
    }
}
