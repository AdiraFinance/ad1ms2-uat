import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/form_m_foto_model.dart';
import 'package:ad1ms2_dev/models/proportional_type_of_insurance_model.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_application_change_notifier.dart';
import 'package:ad1ms2_dev/shared/list_oid_change_notifier.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class InfoApplication extends StatefulWidget {
  @override
  _InfoApplicationState createState() => _InfoApplicationState();
}

class _InfoApplicationState extends State<InfoApplication> {
  Future<void> _setDefaultValue;
  @override
  void initState() {
    super.initState();
    // _setGetData =  Provider.of<InfoAppChangeNotifier>(context,listen: false).getInfoAppShowHide();
    _setDefaultValue = Provider.of<InfoAppChangeNotifier>(context,listen: false).setDefaultValue(context);
  }

  @override
  Widget build(BuildContext context) {
    // Provider.of<InfoAppChangeNotifier>(context,listen: false).getDataFromDashboard(context);
    // Provider.of<InfoAppChangeNotifier>(context,listen: false).setDataFromSQLite();
    // Provider.of<InfoAppChangeNotifier>(context,listen: false).deleteSQLite();
    return Theme(
      data: ThemeData(
          primaryColor: Colors.black,
          accentColor: myPrimaryColor,
          fontFamily: "NunitoSans",
          primarySwatch: primaryOrange),
      child: FutureBuilder(
          future: _setDefaultValue,
          builder: (context, snapshot){
            if (snapshot.connectionState == ConnectionState.waiting) {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
            return Consumer<InfoAppChangeNotifier>(
              builder: (context, infoAppChangeNotifier, _) {
                return SingleChildScrollView(
                  padding: EdgeInsets.symmetric(
                      horizontal: MediaQuery.of(context).size.width / 27,
                      vertical: MediaQuery.of(context).size.height / 57),
                  child: Column(
                    children: [
                      Visibility(
                        visible: infoAppChangeNotifier.isOrderDateVisibleIde(),
                        child: IgnorePointer(
                          ignoring: infoAppChangeNotifier.isDisablePACIAAOSCONA,
                          child: TextFormField(
                              autovalidate: infoAppChangeNotifier.autoValidate,
                              validator: (e) {
                                if (e.isEmpty && infoAppChangeNotifier.isOrderDateMandatoryIde()) {
                                  return "Tidak boleh kosong";
                                } else {
                                  return null;
                                }
                              },
                              controller: infoAppChangeNotifier.controllerOrderDate,
                              readOnly: true,
                              onTap: () {
                                FocusManager.instance.primaryFocus.unfocus();
                                infoAppChangeNotifier.selectDateOrder(context);
                              },
                              decoration: new InputDecoration(
                                  labelText: 'Tanggal Pemesanan',
                                  labelStyle: TextStyle(color: Colors.black),
                                  filled: infoAppChangeNotifier.isDisablePACIAAOSCONA,
                                  fillColor: Colors.black12,
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8)),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: infoAppChangeNotifier.isOrderDateChanges ? Colors.purple : Colors.grey)),
                                  disabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: infoAppChangeNotifier.isOrderDateChanges ? Colors.purple : Colors.grey)),
                                  contentPadding: EdgeInsets.symmetric(horizontal: 10))
                          ),
                        ),
                      ),
                      SizedBox(height: MediaQuery.of(context).size.height / 47),
                      Visibility(
                        visible: infoAppChangeNotifier.isSurveyAppointmentDateVisibleIde(),
                        child: Row(
                          children: [
                            Expanded(
                              flex:7,
                              child: IgnorePointer(
                                ignoring: infoAppChangeNotifier.isDisablePACIAAOSCONA,
                                child: TextFormField(
                                    validator: (e) {
                                      if (e.isEmpty && infoAppChangeNotifier.isSurveyAppointmentDateMandatoryIde()) {
                                        return "Tidak boleh kosong";
                                      } else {
                                        return null;
                                      }
                                    },
                                    autovalidate: infoAppChangeNotifier.autoValidate,
                                    controller: infoAppChangeNotifier.controllerSurveyAppointmentDate,
                                    readOnly: true,
                                    onTap: () {
                                      FocusManager.instance.primaryFocus.unfocus();
                                      infoAppChangeNotifier.selectSurveyDate(context);
                                    },
                                    decoration: new InputDecoration(
                                        labelText: 'Tanggal Janji Survey',
                                        labelStyle: TextStyle(color: Colors.black),
                                        filled: infoAppChangeNotifier.isDisablePACIAAOSCONA,
                                        fillColor: Colors.black12,
                                        border: OutlineInputBorder(
                                            borderRadius: BorderRadius.circular(8)),
                                        enabledBorder: OutlineInputBorder(
                                            borderSide: BorderSide(color: infoAppChangeNotifier.isSurveyAppointmentDateChanges ? Colors.purple : Colors.grey)),
                                        disabledBorder: OutlineInputBorder(
                                            borderSide: BorderSide(color: infoAppChangeNotifier.isSurveyAppointmentDateChanges ? Colors.purple : Colors.grey)),
                                        contentPadding: EdgeInsets.symmetric(horizontal: 10))
                                ),
                              ),
                            ),
                            SizedBox(width: 8),
                            Expanded(
                              flex: 4,
                              child: IgnorePointer(
                                ignoring: infoAppChangeNotifier.isDisablePACIAAOSCONA,
                                child: TextFormField(
                                    validator: (e) {
                                      if (e.isEmpty && infoAppChangeNotifier.isSurveyAppointmentDateMandatoryIde()) {
                                        return "Tidak boleh kosong";
                                      } else {
                                        return null;
                                      }
                                    },
                                    autovalidate: infoAppChangeNotifier.autoValidate,
                                    controller: infoAppChangeNotifier.controllerSurveyAppointmentTime,
                                    readOnly: true,
                                    onTap: () {
                                      FocusManager.instance.primaryFocus.unfocus();
                                      infoAppChangeNotifier.selectTimeSurvey(context);
                                    },
                                    decoration: new InputDecoration(
                                        labelText: 'Jam Janji Survey',
                                        labelStyle: TextStyle(color: Colors.black),
                                        filled: infoAppChangeNotifier.isDisablePACIAAOSCONA,
                                        fillColor: Colors.black12,
                                        border: OutlineInputBorder(
                                            borderRadius: BorderRadius.circular(8)),
                                        enabledBorder: OutlineInputBorder(
                                            borderSide: BorderSide(color: infoAppChangeNotifier.isSurveyAppointmentDateChanges ? Colors.purple : Colors.grey)),
                                        disabledBorder: OutlineInputBorder(
                                            borderSide: BorderSide(color: infoAppChangeNotifier.isSurveyAppointmentDateChanges ? Colors.purple : Colors.grey)),
                                        contentPadding: EdgeInsets.symmetric(horizontal: 10))
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                      Visibility(
                        visible: infoAppChangeNotifier.isIsSignedPKVisibleIde(),
                        child: Row(
                          children: [
                            Text("PK telah di tanda tangani",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: infoAppChangeNotifier.isIsSignedPKChanges ? Colors.purple : Colors.black
                              ),),
                            Checkbox(
                              onChanged: (bool value) {
                                infoAppChangeNotifier.lastKnownState == "IA" ? null : infoAppChangeNotifier.isSignedPK = value;
                              },
                              value: infoAppChangeNotifier.isSignedPK,
                              activeColor: primaryOrange,
                            )
                          ],
                        ),
                      ),
                      Visibility(
                          visible: infoAppChangeNotifier.custType == "COM",
                          child: SizedBox(height: MediaQuery.of(context).size.height / 47)
                      ),
                      Visibility(
                        visible: infoAppChangeNotifier.isConceptTypeModelVisibleIde(),
                        child: Visibility(
                          visible: infoAppChangeNotifier.custType == "COM",
                          child: IgnorePointer(
                            ignoring: infoAppChangeNotifier.isDisablePACIAAOSCONA,
                            child: DropdownButtonFormField<JenisKonsepModel>(
                              isExpanded: true,
                              autovalidate: infoAppChangeNotifier.autoValidate,
                              validator: (e) {
                                if (e == null && infoAppChangeNotifier.isConceptTypeModelMandatoryIde()) {
                                  return "Tidak boleh kosong";
                                } else {
                                  return null;
                                }
                              },
                              value: infoAppChangeNotifier.conceptTypeModelSelected,
                              onChanged: (value) {
                                infoAppChangeNotifier.conceptTypeModelSelected = value;
                                // infoAppChangeNotifier.setDefaultValue(context);
                              },
                              onTap: () {
                                FocusManager.instance.primaryFocus.unfocus();
                              },
                              decoration: InputDecoration(
                                labelText: "Jenis Konsep",
                                border: OutlineInputBorder(),
                                filled: infoAppChangeNotifier.isDisablePACIAAOSCONA,
                                fillColor: Colors.black12,
                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: infoAppChangeNotifier.isConceptTypeModelChanges ? Colors.purple : Colors.grey)),
                                disabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: infoAppChangeNotifier.isConceptTypeModelChanges ? Colors.purple : Colors.grey)),
                                contentPadding: EdgeInsets.symmetric(horizontal: 10),
                              ),
                              items: infoAppChangeNotifier.listConceptType.map((value) {
                                return DropdownMenuItem<JenisKonsepModel>(
                                  value: value,
                                  child: Text(
                                    value.text,
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                );
                              }).toList(),
                            ),
                          )
                        ),
                      ),
                      SizedBox(height: MediaQuery.of(context).size.height / 47),
                      Visibility(
                        visible: infoAppChangeNotifier.isTotalObjectVisibleIde(),
                        child: IgnorePointer(
                          ignoring: infoAppChangeNotifier.isDisablePACIAAOSCONA,
                          child: TextFormField(
                              inputFormatters: [
                                WhitelistingTextInputFormatter.digitsOnly,
                              ],
                              validator: (e) {
                                if (e.isEmpty && infoAppChangeNotifier.isTotalObjectMandatoryIde()) {
                                  return "Tidak boleh kosong";
                                } else {
                                  return null;
                                }
                              },
                              onChanged: (value) => infoAppChangeNotifier.addNumberOfUnitList(context, 99),
                              keyboardType: TextInputType.number,
                              controller: infoAppChangeNotifier.controllerTotalObject,
                              autovalidate: infoAppChangeNotifier.autoValidate,
                              decoration: new InputDecoration(
                                  labelText: 'Jumlah Objek',
                                  labelStyle: TextStyle(color: Colors.black),
                                  filled: infoAppChangeNotifier.isDisablePACIAAOSCONA,
                                  fillColor: Colors.black12,
                                  border: OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: infoAppChangeNotifier.isTotalObjectChanges ? Colors.purple : Colors.grey
                                      )
                                  ),
                                  disabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: infoAppChangeNotifier.isTotalObjectChanges ? Colors.purple : Colors.grey
                                      )
                                  ),
                                  contentPadding: EdgeInsets.symmetric(horizontal: 10)
                              )
                          ),
                        ),
                      ),
                      SizedBox(height: MediaQuery.of(context).size.height / 47),
                      Visibility(
                        visible: infoAppChangeNotifier.isPropotionalObjectVisibleIde(),
                        child: IgnorePointer(
                          ignoring: infoAppChangeNotifier.isDisablePACIAAOSCONA,
                          child: DropdownButtonFormField<ProportionalTypeOfInsuranceModel>(
                            isExpanded: true,
                            autovalidate: infoAppChangeNotifier.autoValidate,
                            validator: (e) {
                              if (e == null && infoAppChangeNotifier.isPropotionalObjectMandatoryIde()) {
                                return "Tidak boleh kosong";
                              } else {
                                return null;
                              }
                            },
                            disabledHint: infoAppChangeNotifier.proportionalTypeOfInsuranceModelSelected != null ?
                            Text(infoAppChangeNotifier.proportionalTypeOfInsuranceModelSelected.description) : Text("Jenis proporsional asuransi"),
                            value: infoAppChangeNotifier.proportionalTypeOfInsuranceModelSelected,
                            // onChanged: infoAppChangeNotifier.isProporsionalEnable ?(value) {
                            //   infoAppChangeNotifier.proportionalTypeOfInsuranceModelSelected = value;
                            // } : null,
                            onChanged: (value) {
                              infoAppChangeNotifier.proportionalTypeOfInsuranceModelSelected = value;
                            },
                            onTap: () {
                              FocusManager.instance.primaryFocus.unfocus();
                            },
                            decoration: InputDecoration(
                              labelText: "Jenis proporsional asuransi",
                              border: OutlineInputBorder(),
                              filled: infoAppChangeNotifier.isDisablePACIAAOSCONA,
                              fillColor: Colors.black12,
                              enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: infoAppChangeNotifier.isPropotionalObjectChanges ? Colors.purple : Colors.grey)),
                              disabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: infoAppChangeNotifier.isPropotionalObjectChanges ? Colors.purple : Colors.grey)),
                              contentPadding: EdgeInsets.symmetric(horizontal: 10),
                            ),
                            items: infoAppChangeNotifier.listProportionalTypeOfInsurance
                                .map((value) {
                              return DropdownMenuItem<ProportionalTypeOfInsuranceModel>(
                                value: value,
                                child: Text(
                                  value.description,
                                  overflow: TextOverflow.ellipsis,
                                ),
                              );
                            }).toList(),
                          ),
                        ),
                      ),
                      SizedBox(height: MediaQuery.of(context).size.height / 47),
                      Visibility(
                        visible: infoAppChangeNotifier.isNumberOfUnitVisibleIde(),
                        child: IgnorePointer(
                          ignoring: infoAppChangeNotifier.isDisablePACIAAOSCONA,
                          child: DropdownButtonFormField<String>(
                            isExpanded: true,
                            autovalidate: infoAppChangeNotifier.autoValidate,
                            validator: (e) {
                              if (e == null && infoAppChangeNotifier.isNumberOfUnitMandatoryIde()) {
                                return "Tidak boleh kosong";
                              } else {
                                return null;
                              }
                            },
                            value: infoAppChangeNotifier.numberOfUnitSelected,
                            onChanged: (value) {
                              infoAppChangeNotifier.numberOfUnitSelected = value;
                            },
                            onTap: () {
                              FocusManager.instance.primaryFocus.unfocus();
                            },
                            decoration: InputDecoration(
                              labelText: "Unit Ke",
                              border: OutlineInputBorder(),
                              filled: infoAppChangeNotifier.isDisablePACIAAOSCONA,
                              fillColor: Colors.black12,
                              enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: infoAppChangeNotifier.isNumberOfUnitChanges ? Colors.purple : Colors.grey)),
                              disabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: infoAppChangeNotifier.isNumberOfUnitChanges ? Colors.purple : Colors.grey)),
                              contentPadding: EdgeInsets.symmetric(horizontal: 10),
                            ),
                            items: infoAppChangeNotifier.listNumberUnit.map((value) {
                              return DropdownMenuItem<String>(
                                value: value,
                                child: Text(
                                  value,
                                  overflow: TextOverflow.ellipsis,
                                ),
                              );
                            }).toList(),
                          ),
                        ),
                      ),
                    ],
                  ),
                );
              },
            );
          }
      ),
    );
  }
}