import 'package:ad1ms2_dev/shared/search_third_party_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../main.dart';

class SearchThirdParty extends StatefulWidget {
  final String flag;

  const SearchThirdParty(this.flag);
  @override
  _SearchThirdPartyState createState() => _SearchThirdPartyState();
}

class _SearchThirdPartyState extends State<SearchThirdParty> {

  @override
  void initState() {
    super.initState();
     // Provider.of<SearchThirdPartyChangeNotifier>(context,listen: false).getThirdParty(widget.flag, context);
  }
  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
          primaryColor: Colors.black,
          accentColor: myPrimaryColor,
          fontFamily: "NunitoSans",
          primarySwatch: primaryOrange
      ),
      child: Scaffold(
        key: Provider.of<SearchThirdPartyChangeNotifier>(context,listen: false).scaffoldKey,
          appBar: AppBar(
//          elevation: 0.0,
            title: Consumer<SearchThirdPartyChangeNotifier>(
              builder: (context, searchThirdPartyChangeNotifier, _) {
                return TextFormField(
                  controller: searchThirdPartyChangeNotifier.controllerSearch,
                  style: TextStyle(color: Colors.black),
                  textInputAction: TextInputAction.search,
                  onFieldSubmitted: (e) {
                    if(e.length >= 3) {
                      searchThirdPartyChangeNotifier.getThirdParty(widget.flag, context, e);
                    } else {
                      searchThirdPartyChangeNotifier.showSnackBar("Input minimal 3 karakter");
                    }
                  },
                  onChanged: (e) {
                    searchThirdPartyChangeNotifier.changeAction(e);
                  },
                  cursorColor: Colors.black,
                  decoration: new InputDecoration(
                    hintText: "Cari Pihak Ketiga (minimal 3 karakter)",
                    hintStyle: TextStyle(color: Colors.black),
                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: myPrimaryColor),
                    ),
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: myPrimaryColor),
                    ),
                  ),
                  autofocus: true,
                  textCapitalization: TextCapitalization.characters,
                );
              },
            ),
            backgroundColor: myPrimaryColor,
            iconTheme: IconThemeData(color: Colors.black),
            actions: <Widget>[
              Provider.of<SearchThirdPartyChangeNotifier>(context, listen: true)
                      .showClear
                  ? IconButton(
                      icon: Icon(Icons.clear),
                      onPressed: () {
                        Provider.of<SearchThirdPartyChangeNotifier>(context,
                                listen: false)
                            .controllerSearch
                            .clear();
                        Provider.of<SearchThirdPartyChangeNotifier>(context,
                                listen: false)
                            .changeAction(
                                Provider.of<SearchThirdPartyChangeNotifier>(
                                        context,
                                        listen: false)
                                    .controllerSearch
                                    .text);
                      })
                  : SizedBox(
                      width: 0.0,
                      height: 0.0,
                    )
            ],
          ),
          body: Column(
            children: [
              // SizedBox(
              //   height: 60,
              //   child: GestureDetector(
              //     onTap: () {
              //       _showBottomSheetDialog();
              //     },
              //     child: Row(
              //       mainAxisSize: MainAxisSize.min,
              //       mainAxisAlignment: MainAxisAlignment.center,
              //       children: [
              //         Icon(Icons.filter_list),
              //         SizedBox(width: 16),
              //         Text(
              //           "FILTER",
              //           style: TextStyle(
              //               fontSize: 14,
              //               fontWeight: FontWeight.w500,
              //               letterSpacing: 1.25),
              //         )
              //       ],
              //     ),
              //   ),
              // ),
              Expanded(
                child: Consumer<SearchThirdPartyChangeNotifier>(
                  builder: (context, searchThirdPartyChangeNotifier, _) {
                    return
                    //   searchThirdPartyChangeNotifier.loadData
                    //     ?
                    // Center(
                    //   child: CircularProgressIndicator(),
                    // )
                    //     :
                    ListView.separated(
                      padding: EdgeInsets.symmetric(
                          vertical: MediaQuery.of(context).size.height / 57,
                          horizontal: MediaQuery.of(context).size.width / 27),
                      itemCount:
                          searchThirdPartyChangeNotifier.listThirdParty.length,
                      itemBuilder: (listContext, index) {
                        return InkWell(
                          onTap: () {
                            Navigator.pop(
                                context,
                                searchThirdPartyChangeNotifier
                                    .listThirdParty[index]);
                          },
                          child: Container(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  children: [
                                    Text(
                                      "${searchThirdPartyChangeNotifier.listThirdParty[index].kode}",
                                      style: TextStyle(
                                          fontSize: 16,
                                          fontWeight: FontWeight.w700),
                                    ),
                                    Text(" - "),
                                    Text(
                                      "${searchThirdPartyChangeNotifier.listThirdParty[index].deskripsi}",
                                      style: TextStyle(
                                          fontSize: 16,
                                          fontWeight: FontWeight.w700),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 13,
                                ),
                                Text(
                                  "${searchThirdPartyChangeNotifier.listThirdParty[index].sentraName}",
                                  style: TextStyle(fontSize: 16),
                                ),
                                SizedBox(
                                  height: 13,
                                ),
                                Text(
                                  "${searchThirdPartyChangeNotifier.listThirdParty[index].unitName}",
                                  style: TextStyle(fontSize: 16),
                                ),
                                SizedBox(
                                  height: 13,
                                ),
                                Text(
                                  "${searchThirdPartyChangeNotifier.listThirdParty[index].kabKotName}",
                                  style: TextStyle(fontSize: 16),
                                ),
                                SizedBox(
                                  height: 13,
                                ),
                                Text(
                                  "${searchThirdPartyChangeNotifier.listThirdParty[index].address}",
                                  style: TextStyle(fontSize: 16),
                                ),
                              ],
                            ),
                          ),
                        );
                      },
                      separatorBuilder: (context, index) {
                        return Divider();
                      },
                    );
                  },
                ),
              ),
            ],
          )),
    );
  }

  // void _showBottomSheetDialog() {
  //   var _provider =
  //       Provider.of<SearchThirdPartyChangeNotifier>(context, listen: false);
  //   showModalBottomSheet(
  //       context: context,
  //       builder: (_) {
  //         return Theme(
  //           data: ThemeData(
  //               accentColor: primaryOrange, primaryColor: myPrimaryColor, primarySwatch: primaryOrange),
  //           child: Container(child: StatefulBuilder(
  //               builder: (BuildContext context, StateSetter stateSetter) {
  //             return Wrap(
  //               children: [
  //                 Row(
  //                   children: [
  //                     Checkbox(
  //                         value: _provider.isAllSentra,
  //                         onChanged: (value) {
  //                           stateSetter(() {
  //                             return _provider.isAllSentra = value;
  //                           });
  //                         }),
  //                     SizedBox(width: 16),
  //                     Text("All Sentra")
  //                   ],
  //                 ),
  //                 Row(
  //                   children: [
  //                     Row(
  //                       children: [
  //                         Radio(
  //                             value: 0,
  //                             groupValue: _provider.radioValueNameOrCityFilter,
  //                             onChanged: (value) {
  //                               stateSetter(() {
  //                                 return _provider.radioValueNameOrCityFilter =
  //                                     value;
  //                               });
  //                             }),
  //                         SizedBox(width: 16),
  //                         Text("Nama/Kode")
  //                       ],
  //                     ),
  //                     Row(
  //                       children: [
  //                         Radio(
  //                             value: 1,
  //                             groupValue: _provider.radioValueNameOrCityFilter,
  //                             onChanged: (value) {
  //                               stateSetter(() {
  //                                 return _provider.radioValueNameOrCityFilter =
  //                                     value;
  //                               });
  //                             }),
  //                         SizedBox(width: 16),
  //                         Text("Kota")
  //                       ],
  //                     ),
  //                   ],
  //                 ),
  //                 Padding(
  //                   padding: const EdgeInsets.all(8.0),
  //                   child: RaisedButton(
  //                       shape: RoundedRectangleBorder(
  //                           borderRadius: BorderRadius.circular(8)),
  //                       color: myPrimaryColor,
  //                       onPressed: () {
  //                         Navigator.pop(context);
  //                       },
  //                       child: Row(
  //                         mainAxisAlignment: MainAxisAlignment.center,
  //                         children: [
  //                           Text("APPLY"),
  //                         ],
  //                       )),
  //                 )
  //               ],
  //             );
  //           })),
  //         );
  //       });
  // }
}
