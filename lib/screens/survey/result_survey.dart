import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/recomm_model.dart';
import 'package:ad1ms2_dev/shared/survey/list_survey_photo_change_notifier.dart';
import 'package:ad1ms2_dev/shared/survey/result_survey_change_notifier.dart';
import 'package:ad1ms2_dev/widgets/widget_result_survey_create_edit_asset.dart';
import 'package:ad1ms2_dev/widgets/widget_result_survey_create_edit_survey_detail.dart';
import 'package:ad1ms2_dev/widgets/widget_result_survey_group_location.dart';
import 'package:ad1ms2_dev/widgets/widget_result_survey_group_note.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'list_survey_photo.dart';

class ResultSurvey extends StatefulWidget {
  @override
  _ResultSurveyState createState() => _ResultSurveyState();

  Future<void> menuSetDataFromSQLite(BuildContext context, int index) async{
    // per tanggal 25.05.2021 sengaja tidak dibuat set data untuk form survey di semua tipe form
    var _providerSurvey = Provider.of<ResultSurveyChangeNotifier>(context, listen: true);
    var _providerPhotoSurvey = Provider.of<ListSurveyPhotoChangeNotifier>(context, listen: true);
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    if(_preferences.getString("last_known_state") == "DKR"){
      await _providerSurvey.selectDataMS2SvyAssgnmt();
      await _providerSurvey.selectDataSurveyAsset();
      await _providerSurvey.selectDataSurveyDetail();
      await _providerPhotoSurvey.setDataSQLite();
    }else if(_preferences.getString("last_known_state") == "PAC"){
      await _providerSurvey.selectDataMS2SvyAssgnmt();
      await _providerSurvey.selectDataSurveyAsset();
      await _providerSurvey.selectDataSurveyDetail();
      await _providerPhotoSurvey.setDataSQLite();
    }else if( index == 13){
      await _providerSurvey.selectDataMS2SvyAssgnmt();
      await _providerSurvey.selectDataSurveyAsset();
      await _providerSurvey.selectDataSurveyDetail();
      await _providerPhotoSurvey.setDataSQLite();
    }
  }
}

class _ResultSurveyState extends State<ResultSurvey> {
  Future<void> _setPreference;

  @override
  void initState() {
    super.initState();
    _setPreference = Provider.of<ResultSurveyChangeNotifier>(context,listen: false).setShowMandatoryResultEnabledGroupNote(context);
  }

  @override
  Widget build(BuildContext context) {
    var _listPhotoSurvey = Provider.of<ListSurveyPhotoChangeNotifier>(context, listen: true);
    return Theme(
      data: ThemeData(
          fontFamily: "NunitoSans",
          accentColor: myPrimaryColor,
          primaryColor: Colors.black,
          primarySwatch: primaryOrange
      ),
      child: Scaffold(
        key: Provider.of<ResultSurveyChangeNotifier>(context,listen: false).scaffoldKeySurvey,
        body: FutureBuilder(
          future: _setPreference,
          builder: (context, snapshot){
            return Consumer<ResultSurveyChangeNotifier>(
              builder: (context, _provider, _) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                }
                return Padding(
                  padding: EdgeInsets.symmetric(
                    horizontal: MediaQuery.of(context).size.width / 37,
                    vertical: MediaQuery.of(context).size.height / 57,
                  ),
                  child: CustomScrollView(
                    slivers: [
                      SliverList(
                          delegate: SliverChildListDelegate(
                            [
                              InkWell(
                                onTap: () {
                                  Navigator.push(context,
                                      MaterialPageRoute(
                                          builder: (context) => WidgetResultGroupSurveyNotes()));
                                },
                                child: Card(
                                  elevation: 3.3,
                                  child: Padding(
                                    padding: EdgeInsets.all(16.0),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Text("Catatan"),
                                        _provider.flagResultSurveyGroupNotes
                                            ? Icon(Icons.check, color: primaryOrange)
                                            : SizedBox()
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                              _provider.autoValidateResultSurveyGroupNote
                                  ?
                              Container(
                                margin: EdgeInsets.only(left: MediaQuery.of(context).size.width / 37),
                                child: Text("Data belum lengkap",
                                    style: TextStyle(color: Colors.red, fontSize: 12)),
                              )
                                  :
                              SizedBox(),
                              SizedBox(height: MediaQuery.of(context).size.height / 87),
                              InkWell(
                                onTap: () {
                                  Navigator.push(context,
                                      MaterialPageRoute(
                                          builder: (context) => WidgetResultSurveyGroupLocation()));
                                },
                                child: Card(
                                  elevation: 3.3,
                                  child: Padding(
                                    padding: EdgeInsets.all(16.0),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Text("Lokasi"),
                                        _provider.flagResultSurveyGroupLocation
                                            ? Icon(Icons.check, color: primaryOrange)
                                            : SizedBox()
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                              _provider.autoValidateResultSurveyGroupLocation
                                  ?
                              Container(
                                margin: EdgeInsets.only(left: MediaQuery.of(context).size.width / 37),
                                child: Text("Data belum lengkap",
                                    style: TextStyle(color: Colors.red, fontSize: 12)),
                              )
                                  :
                              SizedBox(),
                              SizedBox(height: MediaQuery.of(context).size.height / 87),
                              InkWell(
                                onTap: () {
                                  Navigator.push(context,
                                      MaterialPageRoute(
                                          builder: (context) => WidgetResultSurveyCreateNewEditSurveyDetail()));
                                },
                                child: Card(
                                  elevation: 3.3,
                                  child: Padding(
                                    padding: EdgeInsets.all(16.0),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Text("Survey Detail"),
                                        _provider.mandatoryFieldResultSurveyBerhasil
                                        ? _provider.listResultSurveyDetailSurveyModel.isNotEmpty && _provider.flagResultSurveyCreateEditDetailSurvey
                                            ? Icon(Icons.check, color: primaryOrange)
                                            : SizedBox()
                                        : _provider.flagResultSurveyCreateEditDetailSurvey
                                          ? Icon(Icons.check, color: primaryOrange)
                                          : SizedBox()
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                              _provider.checkDataResultSurveyCreateEditDetailSurvey
                              ? Container(
                                  margin: EdgeInsets.only(
                                      left: MediaQuery.of(context).size.width / 37),
                                  child: Text("Data belum lengkap",
                                      style: TextStyle(color: Colors.red, fontSize: 12)),
                                )
                              : SizedBox(),
                              SizedBox(height: MediaQuery.of(context).size.height / 87),
                              InkWell(
                                onTap: () {
                                  Navigator.push(context,
                                      MaterialPageRoute(
                                          builder: (context) => WidgetResultSurveyCreateEditAsset()));
                                },
                                child: Card(
                                  elevation: 3.3,
                                  child: Padding(
                                    padding: EdgeInsets.all(16.0),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Text("Aset"),
                                        _provider.mandatoryFieldResultSurveyBerhasil
                                        ? _provider.listResultSurveyAssetModel.isNotEmpty && _provider.flagResultSurveyAsset
                                            ? Icon(Icons.check, color: primaryOrange)
                                            : SizedBox()
                                        : _provider.flagResultSurveyAsset
                                            ? Icon(Icons.check, color: primaryOrange)
                                            : SizedBox()
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                              _provider.checkDataResultSurveyAsset
                              ? Container(
                                margin: EdgeInsets.only(
                                    left: MediaQuery.of(context).size.width / 37),
                                child: Text(_provider.messageErrorAsset == "" ? "Asset Property wajib ditambahkan." : _provider.messageErrorAsset,
                                    style: TextStyle(color: Colors.red, fontSize: 12)),
                              )
                              : SizedBox(),
                              SizedBox(height: MediaQuery.of(context).size.height / 87),
                              InkWell(
                                onTap: () {
                                  Navigator.push(context,
                                      MaterialPageRoute(
                                          builder: (context) => ListSurveyPhoto()));
                                },
                                child: Card(
                                  elevation: 3.3,
                                  child: Padding(
                                    padding: EdgeInsets.all(16.0),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Text("Foto Survey"),
                                        _provider.mandatoryFieldResultSurveyBerhasil
                                        ? _listPhotoSurvey.listSurveyPhoto.isNotEmpty && _listPhotoSurvey.isFlagSurveyPhoto
                                            ? Icon(Icons.check, color: primaryOrange)
                                            : SizedBox()
                                        : _listPhotoSurvey.isFlagSurveyPhoto
                                            ? Icon(Icons.check, color: primaryOrange)
                                            : SizedBox()
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                              _listPhotoSurvey.checkDataSurveyPhoto
                                  ?
                              Container(
                                margin: EdgeInsets.only(
                                    left: MediaQuery.of(context).size.width / 37),
                                child: Text(_listPhotoSurvey.messageError == "" ? "Foto Selfie wajib ditambahkan." : _listPhotoSurvey.messageError,
                                    style: TextStyle(color: Colors.red, fontSize: 12)),
                              )
                                  :
                              SizedBox(),
                              SizedBox(height: MediaQuery.of(context).size.height / 87),
                              Visibility(
                                visible: _provider.typeForm == "AOS" || _provider.typeForm == "CONA" ? true : false,
                                child: InkWell(
                                  onTap: () {
                                    _provider.saveToSQLitePhotoSurvey();
                                  },
                                  child: Card(
                                    elevation: 3.3,
                                    child: Padding(
                                      padding: EdgeInsets.all(16.0),
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Text("Approve", style: TextStyle(
                                              fontSize: 16,
                                              fontWeight: FontWeight.w700,
                                              letterSpacing: 0.15),),
                                          Row(
                                            children: [
                                              Row(
                                                children: [
                                                  Radio(
                                                      activeColor: primaryOrange,
                                                      value: '1',
                                                      groupValue: _provider.radioValueApproved,
                                                      onChanged: (data) {
                                                        _provider.radioValueApproved = data;
                                                      }),
                                                  Text("Ya")
                                                ],
                                              ),
                                              SizedBox(height: MediaQuery.of(context).size.height / 47),
                                              Row(
                                                children: [
                                                  Radio(
                                                      activeColor: primaryOrange,
                                                      value: '0',
                                                      groupValue: _provider.radioValueApproved,
                                                      onChanged: (data) {
                                                        _provider.radioValueApproved = data;
                                                      }),
                                                  Text("Tidak")
                                                ],
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ]
                          )
                      ),
                      SliverList(
                          delegate: SliverChildListDelegate(
                            [
                              Card(
                                elevation:3.3,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Text(
                                          "Rekomendasi",
                                          style: TextStyle(
                                            fontSize: 22,
                                          )
                                      ),
                                    ),
                                    _provider.listRecomm.isNotEmpty
                                        ?
                                    Column(
                                      children: _listRekom(_provider.listRecomm),
                                    )
                                        :
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Center(
                                        child: Text(
                                          "Tidak Ada Rekomendasi",
                                          style: TextStyle(
                                            fontSize: 18,
                                            color: Colors.grey
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              )
                            ]
                          )
                      )
                    ],
                  ),
                );
              },
            );
          },
        ),
        // Padding(
        //   padding: EdgeInsets.symmetric(
        //     horizontal: MediaQuery.of(context).size.width / 37,
        //     vertical: MediaQuery.of(context).size.height / 57,
        //   ),
        //   child: SingleChildScrollView(
        //     child: Column(
        //       crossAxisAlignment: CrossAxisAlignment.start,
        //       children: [
        //         InkWell(
        //           onTap: () {
        //             Navigator.push(
        //                 context,
        //                 MaterialPageRoute(
        //                     builder: (context) => WidgetResultGroupSurveyNotes()));
        //           },
        //           child: Card(
        //             elevation: 3.3,
        //             child: Padding(
        //               padding: EdgeInsets.all(16.0),
        //               child: Row(
        //                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
        //                 children: <Widget>[
        //                   Text("Catatan"),
        //                   _provider.flagResultSurveyGroupNotes
        //                       ? Icon(Icons.check, color: primaryOrange)
        //                       : SizedBox()
        //                 ],
        //               ),
        //             ),
        //           ),
        //         ),
        //         _provider.autoValidateResultSurveyGroupNote
        //             ?
        //         Container(
        //           margin: EdgeInsets.only(left: MediaQuery.of(context).size.width / 37),
        //           child: Text("Data belum lengkap",
        //               style: TextStyle(color: Colors.red, fontSize: 12)),
        //         )
        //             :
        //         SizedBox(),
        //         SizedBox(height: MediaQuery.of(context).size.height / 87),
        //         InkWell(
        //           onTap: () {
        //             Navigator.push(
        //                 context,
        //                 MaterialPageRoute(
        //                     builder: (context) => WidgetResultSurveyGroupLocation()));
        //           },
        //           child: Card(
        //             elevation: 3.3,
        //             child: Padding(
        //               padding: EdgeInsets.all(16.0),
        //               child: Row(
        //                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
        //                 children: <Widget>[
        //                   Text("Lokasi"),
        //                   _provider.flagResultSurveyGroupLocation
        //                       ? Icon(Icons.check, color: primaryOrange)
        //                       : SizedBox()
        //                 ],
        //               ),
        //             ),
        //           ),
        //         ),
        //         _provider.autoValidateResultSurveyGroupLocation
        //             ?
        //         Container(
        //           margin: EdgeInsets.only(left: MediaQuery.of(context).size.width / 37),
        //           child: Text("Data belum lengkap",
        //               style: TextStyle(color: Colors.red, fontSize: 12)),
        //         )
        //             :
        //         SizedBox(),
        //         SizedBox(height: MediaQuery.of(context).size.height / 87),
        //         InkWell(
        //           onTap: () {
        //             Navigator.push(
        //                 context,
        //                 MaterialPageRoute(
        //                     builder: (context) => WidgetResultSurveyCreateNewEditSurveyDetail()));
        //           },
        //           child: Card(
        //             elevation: 3.3,
        //             child: Padding(
        //               padding: EdgeInsets.all(16.0),
        //               child: Row(
        //                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
        //                 children: <Widget>[
        //                   Text("Survey Detail"),
        //                   _provider.listResultSurveyDetailSurveyModel.isNotEmpty
        //                       ? Icon(Icons.check, color: primaryOrange)
        //                       : SizedBox()
        //                 ],
        //               ),
        //             ),
        //           ),
        //         ),
        //         // _provider.autoValidateResultSurveyCreateEditDetailSurvey
        //         //     ? Container(
        //         //         margin: EdgeInsets.only(
        //         //             left: MediaQuery.of(context).size.width / 37),
        //         //         child: Text("Data belum lengkap",
        //         //             style: TextStyle(color: Colors.red, fontSize: 12)),
        //         //       )
        //         //     : SizedBox(),
        //         SizedBox(height: MediaQuery.of(context).size.height / 87),
        //         InkWell(
        //           onTap: () {
        //             Navigator.push(
        //                 context,
        //                 MaterialPageRoute(
        //                     builder: (context) => WidgetResultSurveyCreateEditAsset()));
        //           },
        //           child: Card(
        //             elevation: 3.3,
        //             child: Padding(
        //               padding: EdgeInsets.all(16.0),
        //               child: Row(
        //                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
        //                 children: <Widget>[
        //                   Text("Aset"),
        //                   _provider.listResultSurveyAssetModel.isNotEmpty
        //                       ? Icon(Icons.check, color: primaryOrange)
        //                       : SizedBox()
        //                 ],
        //               ),
        //             ),
        //           ),
        //         ),
        //         // _provider.autoValidateResultSurveyAssetModel
        //         //     ?
        //         // Container(
        //         //   margin: EdgeInsets.only(
        //         //       left: MediaQuery.of(context).size.width / 37),
        //         //   child: Text("Data belum lengkap",
        //         //       style: TextStyle(color: Colors.red, fontSize: 12)),
        //         // )
        //         //     :
        //         // SizedBox(),
        //         SizedBox(height: MediaQuery.of(context).size.height / 87),
        //         InkWell(
        //           onTap: () {
        //             Navigator.push(context,
        //                 MaterialPageRoute(
        //                     builder: (context) => ListSurveyPhoto()));
        //           },
        //           child: Card(
        //             elevation: 3.3,
        //             child: Padding(
        //               padding: EdgeInsets.all(16.0),
        //               child: Row(
        //                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
        //                 children: <Widget>[
        //                   Text("Foto Survey"),
        //                   _listPhotoSurvey.listSurveyPhoto.isNotEmpty
        //                       ? Icon(Icons.check, color: primaryOrange)
        //                       : SizedBox()
        //                 ],
        //               ),
        //             ),
        //           ),
        //         ),
        //         // _provider.autoValidateResultSurveyPhoto
        //         //     ?
        //         // Container(
        //         //   margin: EdgeInsets.only(
        //         //       left: MediaQuery.of(context).size.width / 37),
        //         //   child: Text("Data belum lengkap",
        //         //       style: TextStyle(color: Colors.red, fontSize: 12)),
        //         // )
        //         //     :
        //         // SizedBox(height: MediaQuery.of(context).size.height / 87),
        //         // InkWell(
        //         //   onTap: () {
        //         //     _provider.saveToSQLitePhotoSurvey();
        //         //   },
        //         //   child: Card(
        //         //     elevation: 3.3,
        //         //     child: Padding(
        //         //       padding: EdgeInsets.all(16.0),
        //         //       child: Row(
        //         //         mainAxisAlignment: MainAxisAlignment.spaceBetween,
        //         //         children: <Widget>[
        //         //           Text("Foto Survey"),
        //         //           _provider.flagResultSurveyAsset
        //         //               ? Icon(Icons.check, color: primaryOrange)
        //         //               : SizedBox()
        //         //         ],
        //         //       ),
        //         //     ),
        //         //   ),
        //         // ),
        //       ],
        //     ),
        //   ),
        // ),
      ),
    );
  }

  List<Widget> _listRekom(List<RecommModel> _listRecomm){
    List<Widget> _temp = [];
    for(int i=0; i < _listRecomm.length; i++){
      _temp.add(
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              children: [
                Row(
                  children: [
                    Expanded(child: Text("LEVEL KOMITE"),flex: 5),
                    Text(" : "),
                    Expanded(child: Text("${_listRecomm[i].levelKomite}"),flex: 5)
                  ],
                ),
                Row(
                  children: [
                    Expanded(child: Text("REKOMENDASI"),flex: 5),
                    Text(" : "),
                    Expanded(child: Text("${_listRecomm[i].rekomendasi}"),flex: 5)
                  ],
                ),
                Row(
                  children: [
                    Expanded(child: Text("FIELD"),flex: 5),
                    Text(" : "),
                    Expanded(child: Text("${_listRecomm[i].field}"),flex: 5)
                  ],
                ),
                Row(
                  children: [
                    Expanded(child: Text("KETERANGAN"),flex: 5),
                    Text(" : "),
                    Expanded(child: Text("${_listRecomm[i].keterangan}"),flex: 5)
                  ],
                ),
                Row(
                  children: [
                    Expanded(child: Text("CREATED_BY"),flex: 5),
                    Text(" : "),
                    Expanded(child: Text("-"),flex: 5)
                  ],
                ),
                SizedBox(height: 6),
                Divider(color: Colors.black,height: 2)
              ],
            ),
          )
      );
    }
    return _temp;
  }
}
