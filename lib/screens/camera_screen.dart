import 'dart:async';

import 'package:ad1ms2_dev/screens/preview_screen.dart';

import '../main.dart';
import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sensors/sensors.dart';

class CameraScreen extends StatefulWidget {
  @override
  _CameraScreenState createState() {
    return _CameraScreenState();
  }
}

class _CameraScreenState extends State {
  CameraController controller;
  List cameras;
  int selectedCameraIdx;
  String imagePath;
  List<double> _gyroscopeValues;
  List<StreamSubscription<dynamic>> _streamSubscriptions = <StreamSubscription<dynamic>>[];

  @override
  void initState() {
    super.initState();
    availableCameras().then((availableCameras) {
      cameras = availableCameras;

      if (cameras.length > 0) {
        setState(() {
          selectedCameraIdx = 0;
        });

        _initCameraController(cameras[selectedCameraIdx]).then((void v) {});
      } else {
        print("No camera available");
      }
    }).catchError((err) {
      print('Error: $err.code\nError Message: $err.message');
    });
    _streamSubscriptions.add(gyroscopeEvents.listen((GyroscopeEvent event) {
      setState(() {
        _gyroscopeValues = <double>[event.x, event.y, event.z];
      });
    }));
  }

  @override
  void dispose() {
    super.dispose();
    for (StreamSubscription<dynamic> subscription in _streamSubscriptions) {
      subscription.cancel();
    }
  }

  Future _initCameraController(CameraDescription cameraDescription) async {
    if (controller != null) {
      await controller.dispose();
    }

    controller = CameraController(cameraDescription, ResolutionPreset.max);

    // If the controller is updated then update the UI.
    controller.addListener(() {
      if (mounted) {
        setState(() {});
      }

      if (controller.value.hasError) {
        print('Camera error ${controller.value.errorDescription}');
      }
    });

    try {
      await controller.initialize();
    } on CameraException catch (e) {
      _showCameraException(e);
    }

    if (mounted) {
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    final List<String> gyroscope = _gyroscopeValues?.map((double v) => v.toStringAsFixed(1))?.toList();
    print("gyroscope $gyroscope");
    return Scaffold(
      appBar: AppBar(
        title: const Text('Capture ID',style: TextStyle(color: Colors.black)),
        backgroundColor: myPrimaryColor,
      ),
      body: Container(
        child: SafeArea(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Expanded(
                flex: 1,
                child: Stack(
                  children: <Widget>[
                    Positioned.fill(child: _cameraPreviewWidget()
//                        NativeDeviceOrientationReader(
//                            builder: (context) {
//                              NativeDeviceOrientation orientation = NativeDeviceOrientationReader.orientation(context);
//                              print("cek orientation $orientation");
//                              int turns;
//                              switch (orientation) {
//                                case NativeDeviceOrientation.landscapeLeft: turns = -1; break;
//                                case NativeDeviceOrientation.landscapeRight: turns = 1; break;
//                                case NativeDeviceOrientation.portraitDown: turns = 2; break;
//                                default: turns = 0; break;
//                              }
//
//                              return RotatedBox(
//                                  quarterTurns: turns,
//                                  child: _cameraPreviewWidget()
//                              );
//                            }
//                        )
                    ),
                    Positioned.fill(
                        child: ClipPath(
                          child: Container(
                            color: Colors.black.withOpacity(0.6),
                          ),
                          clipper: _SquareModePhoto(),
                        ))
                  ],
                ),
              ),
              SizedBox(height: 10.0),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  _cameraTogglesRowWidget(),
                  _captureControlRowWidget(context),
                  Spacer()
                ],
              ),
              SizedBox(height: 20.0)
            ],
          ),
        ),
      ),
    );
  }

  /// Display Camera preview.
  Widget _cameraPreviewWidget() {
    if (controller == null || !controller.value.isInitialized) {
      return const Text(
        'Loading',
        style: TextStyle(
          color: Colors.white,
          fontSize: 20.0,
          fontWeight: FontWeight.w900,
        ),
      );
    }

    return AspectRatio(
      aspectRatio: controller.value.aspectRatio,
      child: CameraPreview(controller),
    );
  }

  /// Display the control bar with buttons to take pictures
  Widget _captureControlRowWidget(context) {
    return Expanded(
      child: Align(
        alignment: Alignment.center,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          mainAxisSize: MainAxisSize.max,
          children: [
            FloatingActionButton(
                child: Icon(Icons.camera),
                backgroundColor: Colors.blueGrey,
                onPressed: () {
                  _onCapturePressed(context);
                })
          ],
        ),
      ),
    );
  }

  /// Display a row of toggle to select the camera (or a message if no camera is available).
  Widget _cameraTogglesRowWidget() {
    if (cameras == null || cameras.isEmpty) {
      return Spacer();
    }

    CameraDescription selectedCamera = cameras[selectedCameraIdx];
    CameraLensDirection lensDirection = selectedCamera.lensDirection;

    return Expanded(
      child: Align(
        alignment: Alignment.centerLeft,
        child: FlatButton.icon(
            onPressed: _onSwitchCamera,
            icon: Icon(_getCameraLensIcon(lensDirection)),
            label: Text(
                "${lensDirection.toString().substring(lensDirection.toString().indexOf('.') + 1)}")),
      ),
    );
  }

  IconData _getCameraLensIcon(CameraLensDirection direction) {
    switch (direction) {
      case CameraLensDirection.back:
        return Icons.camera_rear;
      case CameraLensDirection.front:
        return Icons.camera_front;
      case CameraLensDirection.external:
        return Icons.camera;
      default:
        return Icons.device_unknown;
    }
  }

  void _onSwitchCamera() {
    selectedCameraIdx = selectedCameraIdx < cameras.length - 1 ? selectedCameraIdx + 1 : 0;
    CameraDescription selectedCamera = cameras[selectedCameraIdx];
    _initCameraController(selectedCamera);
  }

  void _onCapturePressed(context) async {
    // Take the Picture in a try / catch block. If anything goes wrong,
    // catch the error.
    try {
      // Attempt to take a picture and log where it's been saved
      final path = join(
        // In this example, store the picture in the temp directory. Find
        // the temp directory using the `path_provider` plugin.
        (await getTemporaryDirectory()).path, '${DateTime.now()}.png',
      );
      print(path);
      await controller.takePicture(path);

      // If the picture was taken, display it on a new screen
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => PreviewImageScreen(imagePath: path),
        ),
      ).then((value) => value != null ? value['status'] ? Navigator.pop(context,value['value']) : null : null);
    } catch (e) {
      // If an error occurs, log the error to the console.
      print(e);
    }
  }

  void _showCameraException(CameraException e) {
    String errorText = 'Error: ${e.code}\nError Message: ${e.description}';
    print(errorText);

    print('Error: ${e.code}\n${e.description}');
  }
}

class _SquareModePhoto extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    print(size.width / 11);
    var path = Path();
    var reactPath = Path();

//    reactPath.moveTo(size.width/11, size.height/4);
//    reactPath.lineTo(size.width/11, size.height/1.8);
//    reactPath.lineTo(size.width/1.09, size.height/1.8);
//    reactPath.lineTo(size.width/1.09, size.height/4);

    reactPath.moveTo(size.width / 11, size.height / 2.05);
    reactPath.quadraticBezierTo(size.width / 10.59, size.height / 1.890,
        size.width / 6.6, size.height / 1.87);
    reactPath.lineTo(size.width / 1.13, size.height / 1.87);
    reactPath.quadraticBezierTo(size.width / 1.06789, size.height / 1.890,
        size.width / 1.07, size.height / 2.05);
    reactPath.lineTo(size.width / 1.07, size.height / 5.234);
    reactPath.quadraticBezierTo(size.width / 1.06789, size.height / 6.91,
        size.width / 1.13, size.height / 7);
    reactPath.lineTo(size.width / 7.1, size.height / 7);
    reactPath.quadraticBezierTo(size.width / 10.59, size.height / 6.91,
        size.width / 11, size.height / 5.234);

//    reactPath.moveTo(size.width/11, size.height/9);
//    reactPath.quadraticBezierTo(size.width/11, size.height, 20.0, size.height);
//    reactPath.lineTo(size.width/11, size.height/1.9);
//    reactPath.lineTo(size.width/1.1, size.height/1.9);
//    reactPath.lineTo(size.width/1.1, size.height/9);

    path.addPath(reactPath, Offset(0, 0));
    path.addRect(Rect.fromLTWH(0.0, 0.0, size.width, size.height));
    path.fillType = PathFillType.evenOdd;
/*
    path.moveTo(size.width/4, size.height/4);
    path.lineTo(size.width/4, size.height*3/4);
    path.lineTo(size.width*3/4, size.height*3/4);
    path.lineTo(size.width*3/4, size.height/4);
*/
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return true;
  }
}