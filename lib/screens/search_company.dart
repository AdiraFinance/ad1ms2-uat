import 'package:ad1ms2_dev/shared/search_company_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../main.dart';

class SearchCompany extends StatefulWidget {
    @override
    _SearchCompanyState createState() => _SearchCompanyState();
}

class _SearchCompanyState extends State<SearchCompany> {

    @override
  void initState() {
    super.initState();
    Provider.of<SearchCompanyChangeNotifier>(context,listen: false).getCompanyType(context);
  }
    @override
    Widget build(BuildContext context) {
        return Theme(
          data: ThemeData(
              primaryColor: Colors.black,
              fontFamily: "NunitoSans",
              accentColor: myPrimaryColor,
              primarySwatch: primaryOrange
          ),
          child: Scaffold(
              key: Provider.of<SearchCompanyChangeNotifier>(context,listen: false).scaffoldKey,
              appBar: AppBar(
                  title: Consumer<SearchCompanyChangeNotifier>(
                      builder: (context, searchCompanyChangeNotifier, _) {
                          return TextFormField(
                              controller: searchCompanyChangeNotifier.controllerSearch,
                              style: TextStyle(color: Colors.black),
                              textInputAction: TextInputAction.search,
                              onFieldSubmitted: (query) {
                                  searchCompanyChangeNotifier.searchCompanyType(query);
                              },
                              onChanged: (e) {
                                  searchCompanyChangeNotifier.changeAction(e);
                              },
                              textCapitalization: TextCapitalization.characters,
                              cursorColor: Colors.black,
                              decoration: new InputDecoration(
                                  hintText: "Cari Perusahaan (minimal 3 karakter)",
                                  hintStyle: TextStyle(color: Colors.black),
                                  enabledBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(color: myPrimaryColor),
                                  ),
                                  focusedBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(color: myPrimaryColor),
                                  ),
                              ),
                              autofocus: true,
                          );
                      },
                  ),
                  backgroundColor: myPrimaryColor,
                  iconTheme: IconThemeData(color: Colors.black),
                  actions: <Widget>[
                      Provider.of<SearchCompanyChangeNotifier>(context, listen: true)
                          .showClear
                          ? IconButton(
                          icon: Icon(Icons.clear),
                          onPressed: () {
                              Provider.of<SearchCompanyChangeNotifier>(context,
                                  listen: false).clearSearchTemp();
                              Provider.of<SearchCompanyChangeNotifier>(context,
                                  listen: false)
                                  .controllerSearch
                                  .clear();
                              Provider.of<SearchCompanyChangeNotifier>(context,
                                  listen: false)
                                  .changeAction(Provider.of<SearchCompanyChangeNotifier>(
                                  context,
                                  listen: false)
                                  .controllerSearch
                                  .text);
                          })
                          : SizedBox(
                          width: 0.0,
                          height: 0.0,
                      )
                  ],
              ),
              body: Consumer<SearchCompanyChangeNotifier>(
                  builder: (context, searchCompanyChangeNotifier, _) {
                      return searchCompanyChangeNotifier.loadData
                          ?
                      Center(child: CircularProgressIndicator())
                          :
                      searchCompanyChangeNotifier.listCompanyTemp.isNotEmpty
                          ?
                      ListView.separated(
                          padding: EdgeInsets.symmetric(
                              vertical: MediaQuery.of(context).size.height / 57,
                              horizontal: MediaQuery.of(context).size.width / 27),
                          itemCount: searchCompanyChangeNotifier.listCompanyTemp.length,
                          itemBuilder: (listContext, index) {
                              return InkWell(
                                  onTap: () {
                                      Navigator.pop(context,
                                          searchCompanyChangeNotifier.listCompanyTemp[index]);
                                  },
                                  child: Container(
                                      child: Row(
                                          mainAxisSize: MainAxisSize.max,
                                          children: [
                                              Text(
                                                  "${searchCompanyChangeNotifier.listCompanyTemp[index].id} - "
                                                      "${searchCompanyChangeNotifier.listCompanyTemp[index].desc} ",
                                                  style: TextStyle(fontSize: 16),
                                              )
                                          ],
                                      ),
                                  ),
                              );
                          },
                          separatorBuilder: (context, index) {
                              return Divider();
                          },
                      )
                          :
                      ListView.separated(
                          padding: EdgeInsets.symmetric(
                              vertical: MediaQuery.of(context).size.height / 57,
                              horizontal: MediaQuery.of(context).size.width / 27),
                          itemCount: searchCompanyChangeNotifier.listCompanyModel.length,
                          itemBuilder: (listContext, index) {
                              return InkWell(
                                  onTap: () {
                                      Navigator.pop(context,
                                          searchCompanyChangeNotifier.listCompanyModel[index]);
                                  },
                                  child: Container(
                                      child: Row(
                                          mainAxisSize: MainAxisSize.max,
                                          children: [
                                              Text(
                                                  "${searchCompanyChangeNotifier.listCompanyModel[index].id} - "
                                                      "${searchCompanyChangeNotifier.listCompanyModel[index].desc} ",
                                                  style: TextStyle(fontSize: 16),
                                              )
                                          ],
                                      ),
                                  ),
                              );
                          },
                          separatorBuilder: (context, index) {
                              return Divider();
                          },
                      );
                  },
              ),
          ),
        );
    }
}
