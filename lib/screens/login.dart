import 'package:ad1ms2_dev/screens/dashboard.dart';
import 'package:ad1ms2_dev/shared/login_change_notifier.dart';
import 'package:ad1ms2_dev/shared/responsive_screen.dart';
import 'package:ad1ms2_dev/shared/upper_clipper.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:random_string/random_string.dart';

import '../main.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  void initState() {
    super.initState();
    Provider.of<LoginChangeNotifier>(context, listen: false).setRandomText(0);
    Provider.of<LoginChangeNotifier>(context, listen: false).checkLogin(context);
  }

  @override
  Widget build(BuildContext context) {
    var _size = Screen(MediaQuery.of(context).size);
    return Consumer<LoginChangeNotifier>(
      builder: (context, login, _){
          return Scaffold(
            key: login.scaffoldKey,
            backgroundColor: Colors.black,
            body: Container(
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      login.upperPart(context),
//                  _upperPart(),
                      SizedBox(height: _size.hp(2)),
                      Form(
                        key: login.key,
                        child: Container(
                          margin: EdgeInsets.symmetric(horizontal: _size.wp(8)),
                          child: Column(
                            children: <Widget>[
                              TextFormField(
                                controller: login.controllerEmail,
                                style: new TextStyle(color: Colors.white),
                                decoration: new InputDecoration(
                                  labelText: 'NIK',
                                  labelStyle: TextStyle(
                                      fontFamily: "NunitoSans", color: Colors.white),
                                  enabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8.0),
                                      borderSide: BorderSide(color: Colors.white)),
                                  focusedBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8.0),
                                      borderSide: BorderSide(color: Colors.white)),
                                  errorBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8.0),
                                      borderSide: BorderSide(color: Colors.red)),
                                ),
                                keyboardType: TextInputType.emailAddress,
                                autovalidate: login.autoValidate,
                                validator: (e) {
                                  if (e.isEmpty) {
                                    return "Tidak boleh kosong";
                                  } else if(e.length > 8){
                                    return "Maksimal 8 digit";
                                  }
                                  else {
                                    return null;
                                  }
                                },
                                inputFormatters: [
                                  FilteringTextInputFormatter.allow(RegExp('[0-9]')),
                                ],
                              ),
                              SizedBox(height: 16),
                              TextFormField(
                                controller: login.controllerPassword,
                                style: new TextStyle(color: Colors.white),
                                decoration: new InputDecoration(
                                  labelText: 'Password',
                                  labelStyle: TextStyle(
                                      fontFamily: "NunitoSans", color: Colors.white),
                                  enabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8.0),
                                      borderSide: BorderSide(color: Colors.white)),
                                  focusedBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8.0),
                                      borderSide: BorderSide(color: Colors.white)),
                                  errorBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8.0),
                                      borderSide: BorderSide(color: Colors.red)),
                                  suffixIcon: IconButton(
                                      icon: Icon(
                                        login.secureText
                                            ? Icons.visibility_off
                                            : Icons.visibility,
                                        color: Colors.white,
                                      ),
                                      onPressed: login.showHidePass),
                                ),
                                keyboardType: TextInputType.emailAddress,
                                obscureText: login.secureText,
                                autovalidate: login.autoValidate,
                                validator: (e) {
                                  if (e.isEmpty) {
                                    return "Tidak boleh kosong";
                                  } else {
                                    return null;
                                  }
                                },
                              ),
                              SizedBox(height: 16),
                              Row(
                                children: <Widget>[
                                  Expanded(
                                    flex: 1,
                                    child: RaisedButton(
                                        onPressed: () {
                                          login.setRandomText(1);
                                        },
                                        padding: EdgeInsets.only(
                                            top: _size.hp(2.37),
                                            bottom: _size.hp(2.37)),
                                        child: Icon(Icons.refresh),
                                        shape: RoundedRectangleBorder(
                                            borderRadius: BorderRadius.only(
                                                topLeft: Radius.circular(8),
                                                bottomLeft: Radius.circular(8),
                                                topRight: Radius.circular(0),
                                                bottomRight: Radius.circular(0)))),
                                  ),
                                  Expanded(
                                    flex: 4,
                                    child: RaisedButton(
                                        onPressed: () {},
                                        padding: EdgeInsets.only(
                                            top: _size.hp(2.37),
                                            bottom: _size.hp(2.37)),
                                        child: Text(
                                          "${login.randomText}",
                                          style: TextStyle(
                                            fontFamily: "NunitoSans",
                                            fontSize: 18,
                                            fontStyle: FontStyle.italic,
                                          ),
                                        ),
                                        shape: RoundedRectangleBorder(
                                            borderRadius: BorderRadius.only(
                                                topLeft: Radius.circular(0),
                                                bottomLeft: Radius.circular(0),
                                                topRight: Radius.circular(8),
                                                bottomRight: Radius.circular(8)))),
//                            TextFormField(
//                              style: new TextStyle(
//                                  color: Colors.white,
//                                  wordSpacing: 32,
//                                  fontFamily: "NunitoSans",
//                                  fontSize: 21,
//                                  fontStyle: FontStyle.italic,
//                                  letterSpacing: 6),
//                              enabled: false,
//                              decoration: new InputDecoration(
//                                fillColor: Colors.grey,
//                                filled: true,
//                                contentPadding: EdgeInsets.only(
//                                    top: _size.hp(1.9), bottom: _size.hp(1.9)),
//                                disabledBorder: OutlineInputBorder(
//                                    borderRadius: BorderRadius.only(
//                                        bottomRight: Radius.circular(8),
//                                        topRight: Radius.circular(8),
//                                        bottomLeft: Radius.circular(0),
//                                        topLeft: Radius.circular(0)),
//                                    borderSide:
//                                        BorderSide(color: Colors.white)),
//                              ),
//                            ),
                                  ),
                                  SizedBox(width: 8),
                                  Expanded(
                                    flex: 4,
                                    child: TextFormField(
                                      controller: login.valueRandomText,
                                      style: new TextStyle(color: Colors.white),
                                      autovalidate: login.autoValidate,
                                      validator: (e) {
                                        if (e.isEmpty) {
                                          return "Tidak boleh kosong";
                                        } else if(e != login.randomText){
                                          return "Random text not match";
                                        }
                                        else {
                                          return null;
                                        }
                                      },
                                      decoration: new InputDecoration(
                                        labelText: 'Captcha',
                                        labelStyle: TextStyle(
                                            fontFamily: "NunitoSans",
                                            color: Colors.white),
                                        contentPadding: EdgeInsets.only(
                                            top: _size.hp(1.9),
                                            bottom: _size.hp(1.9),
                                            left: _size.wp(2),
                                            right: _size.wp(2)),
                                        enabledBorder: OutlineInputBorder(
                                            borderRadius: BorderRadius.only(
                                                bottomRight: Radius.circular(8),
                                                topRight: Radius.circular(8),
                                                bottomLeft: Radius.circular(8),
                                                topLeft: Radius.circular(8)),
                                            borderSide:
                                            BorderSide(color: Colors.white)),
                                        focusedBorder: OutlineInputBorder(
                                            borderRadius: BorderRadius.only(
                                                bottomRight: Radius.circular(8),
                                                topRight: Radius.circular(8),
                                                bottomLeft: Radius.circular(8),
                                                topLeft: Radius.circular(8)),
                                            borderSide:
                                            BorderSide(color: Colors.white)),
                                        errorBorder: OutlineInputBorder(
                                            borderRadius: BorderRadius.only(
                                                bottomRight: Radius.circular(8),
                                                topRight: Radius.circular(8),
                                                bottomLeft: Radius.circular(8),
                                                topLeft: Radius.circular(8)),
                                            borderSide: BorderSide(color: Colors.red)
                                        ),
//                                 errorText: login.errorRandomNotmatch
//                                     ? "Random text not match"
//                                     : null
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 24),
                              login.loginProcess
                                  ?
                              Center(child: CircularProgressIndicator())
                                  :
                              RaisedButton(
                                color: myPrimaryColor,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(8)),
                                child: Row(
                                  mainAxisSize: MainAxisSize.max,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Text("Login",
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontFamily: "NunitoSansBold")),
                                  ],
                                ),
                                padding: EdgeInsets.only(top: 16.0, bottom: 16.0),
                                onPressed: () {
                                  login.check(context);
                                },
                              )
                            ],
                          ),
                        ),
                      ),
                      SizedBox(height: _size.hp(3)),
                      InkWell(
                        onTap: () {
                          login.dialogAbout(context);
                        },
                        child: Container(
                          child: Text("About",
                              style: TextStyle(
                                fontFamily: "NunitoSansSemiBold",
                                color: Colors.white,
                                decoration: TextDecoration.underline,
                              )),
                        ),
                      ),
                    ],
                  ),
                )
            ),
          );
      },
    );
  }

//  Widget _upperPart() {
//    return Stack(
//      children: <Widget>[
//        ClipPath(
//          clipper: UpperClipper(),
//          child: Container(
//            height: _size.hp(35),
//            decoration: BoxDecoration(color: myPrimaryColor),
//          ),
//        ),
//        Container(
//            margin: EdgeInsets.only(top: _size.hp(4), left: _size.wp(8)),
//            child: Image.asset('img/logo_adira.png', height: 150, width: 150))
//      ],
//    );
//  }

//  _showHidePass() {
//    setState(() {
//      _secureText = !_secureText;
//    });
//  }

}

// import 'package:connectivity/connectivity.dart';
//import 'package:ad1ms2_dev/shared/constants.dart';
//import 'package:flutter/material.dart';
//import 'package:hexcolor/hexcolor.dart';
//import 'package:random_string/random_string.dart';
//
//import 'home.dart';
//
//class Login extends StatefulWidget {
//  final Function toggleView;
//
//  Login({this.toggleView});
//
//  @override
//  _LoginState createState() => _LoginState();
//}
//
//class _LoginState extends State<Login> {
//  final _formKey = GlobalKey<FormState>();
//  bool loading = false;
//
//  //Text Field State
//  String email = '';
//  String password = '';
//  String error = '';
//  String inpCaptcha = '';
//  String captcha = '';
//
//  final String orange = '#f7931e';
//
//  bool _pass = true;
//
//  void _passView() {
//    setState(() {
//      _pass = !_pass;
//    });
//  }
//
//  void _setCaptcha() {
//    setState(() {
//      captcha = randomAlpha(5);
//    });
//  }
//
//  @override
//  void initState() {
//    super.initState();
//    _setCaptcha();
//  }
//
//  Widget _iconMS2() {
//    return Container(
//      height: 200,
//      width: 200,
//      decoration: BoxDecoration(
//          borderRadius: BorderRadius.all(Radius.circular(100)),
//          border: Border.all(
//              width: 3, color: Colors.white, style: BorderStyle.solid)),
//      child: Center(
//          child: Text(
//        "MS2",
//        style: TextStyle(
//            fontSize: 50, fontWeight: FontWeight.bold, color: Colors.white),
//      )),
//    );
//  }
//
//  Widget _buildUsername() {
//    return TextFormField(
//      decoration: textInputDecoration.copyWith(
//          prefixIcon: Icon(Icons.person), hintText: 'NIK', labelText: 'NIK'),
//      validator: (val) => val.isEmpty ? 'Input NIK!' : null,
//      onChanged: (val) {
//        setState(() => email = val);
//      },
//    );
//  }
//
//  Widget _buildPassword() {
//    return TextFormField(
//      decoration: textInputDecoration.copyWith(
//          prefixIcon: Icon(Icons.vpn_key),
//          hintText: 'Password',
//          labelText: 'Password',
//          suffixIcon: IconButton(
//            icon: Icon(Icons.remove_red_eye),
//            onPressed: _passView,
//          )),
//      obscureText: _pass,
//      validator: (val) =>
//          val.length < 6 ? 'Minimum password is 6 chars long' : null,
//      onChanged: (val) {
//        setState(() => password = val);
//      },
//    );
//  }
//
//  Widget _buildCaptcha() {
//    return Row(
//      children: <Widget>[
//        FlatButton(
//          onPressed: () {
//            _setCaptcha();
//          },
//          child: Icon(Icons.refresh),
//          shape: RoundedRectangleBorder(
//            borderRadius: BorderRadius.only(
//                topLeft: Radius.circular(8),
//                bottomLeft: Radius.circular(8),
//                topRight: Radius.circular(0),
//                bottomRight: Radius.circular(0)),
//          ),
//          splashColor: Hexcolor(orange),
//        ),
//        Container(
//          child: Align(
//            alignment: Alignment.center,
//            child: Text(
//              captcha,
//              style: TextStyle(
//                fontSize: 25,
//              ),
//            ),
//          ),
//          color: Hexcolor('#FFFFFF'),
//          width: MediaQuery.of(context).size.width / 3,
//          height: 50,
//        ),
//        SizedBox(
//          width: 8,
//        ),
//        SizedBox(
//          width: 8,
//        ),
//      ],
//    );
//  }
//
//  Widget _buildInputCaptcha() {
//    return TextFormField(
//      decoration: InputDecoration(
//          hintText: 'Input String',
//          errorText: _errCaptcha ? "inputan captcha salah" : null),
//      onChanged: (val) {
//        setState(() => inpCaptcha = val);
//      },
//    );
//  }
//
//  // _checkConnection() async {
//  //   var result = await Connectivity().checkConnectivity();
//  //   switch (result) {
//  //     case ConnectivityResult.none:
//  //       _showDialog('None', 'No Signal');
//  //       break;
//  //     case ConnectivityResult.mobile:
//  //       _showDialog('Mobile', 'Mobile Signal');
//  //       break;
//  //     case ConnectivityResult.wifi:
//  //       _showDialog('Wifi', 'Wifi Signal');
//  //       break;
//  //     default:
//  //   }
//  // }
//
//  // _showDialog(String title, String content) {
//  //   showDialog(
//  //       context: context,
//  //       builder: (context) {
//  //         return AlertDialog(
//  //           title: Text(title),
//  //           content: Text(content),
//  //           actions: <Widget>[
//  //             FlatButton(
//  //               child: Text('OK'),
//  //               onPressed: () {
//  //                 Navigator.of(context).pop();
//  //               },
//  //             )
//  //           ],
//  //         );
//  //       });
//  // }
//
//  bool _errCaptcha = false;
//  _validateCaptcha() {
//    if (captcha.toUpperCase() != inpCaptcha.toUpperCase()) {
//      _errCaptcha = true;
//      _resetForm();
//      _setCaptcha();
//    } else {
//      setState(() {
//        _errCaptcha = false;
//      });
//      // login proses
//      _login();
//    }
//  }
//
//  void _resetForm() {
//    email = '';
//    password = '';
//    inpCaptcha = '';
//  }
//
//  _login() {
//    //placeholder method for login check
//    Navigator.push(
//      context,
//      MaterialPageRoute(builder: (context) => Home()),
//    );
//  }
//
//  @override
//  Widget build(BuildContext context) {
//    return SafeArea(
//      child: Scaffold(
//        backgroundColor: Hexcolor(orange),
//        body: Container(
//            padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 50.0),
//            child: Form(
//              key: _formKey,
//              child: Column(
//                children: <Widget>[
//                  _iconMS2(),
//                  SizedBox(
//                    height: 30.0,
//                  ),
//                  _buildUsername(),
//                  _buildPassword(),
//                  SizedBox(
//                    height: 8,
//                  ),
//                  _buildCaptcha(),
//                  _buildInputCaptcha(),
//                  SizedBox(
//                    height: 12.0,
//                  ),
//                  SizedBox(
//                    height: 40.0,
//                    width: 150.0,
//                    child: RaisedButton(
//                      color: Colors.white70,
//                      child: Text(
//                        'Sign In',
//                        style: TextStyle(
//                          color: Hexcolor(orange),
//                        ),
//                      ),
//                      onPressed: () async {
//                        // Navigator.push(
//                        //   context,
//                        //   MaterialPageRoute(builder: (context) => Home()),
//                        // );
//                        _validateCaptcha();
////                    if (_formKey.currentState.validate()) {
////                      setState(() {
////                        loading = true;
////                      });
//////                      dynamic result = await _auth.signInWithEmailAndPassword(email, password);
////                      if (result == null) {
////                        setState(() {
////                          error = 'please suply valid email';
////                          loading = false;
////                        });
////                      }
////                    }
//                      },
//                    ),
//                  ),
//                  // SizedBox(
//                  //   height: 12.0,
//                  // ),
//                  // SizedBox(
//                  //   height: 20.0,
//                  //   child: RaisedButton(
//                  //     color: Colors.white70,
//                  //     child: Text(
//                  //       'Check Signal',
//                  //       style: TextStyle(
//                  //         color: Hexcolor(orange),
//                  //       ),
//                  //     ),
//                  //     onPressed: _checkConnection,
//                  //   ),
//                  // ),
//                  Text(
//                    error,
//                    style: TextStyle(color: Colors.red, fontSize: 14.0),
//                  ),
//                ],
//              ),
//            )),
//      ),
//    );
//  }
//}
