import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/shared/search_birth_place_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SearchBirthPlace extends StatefulWidget {
  @override
  _SearchBirthPlaceState createState() => _SearchBirthPlaceState();
}

class _SearchBirthPlaceState extends State<SearchBirthPlace> {
  @override
  void initState() {
    super.initState();
    Provider.of<SearchBirthPlaceChangeNotifier>(context, listen: false).getBirthPlace();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: Provider.of<SearchBirthPlaceChangeNotifier>(context,listen: false).scaffoldKey,
      appBar: AppBar(
        title: Consumer<SearchBirthPlaceChangeNotifier>(
          builder: (context, searchBirthPlaceChangeNotifier, _) {
            return TextFormField(
              controller: searchBirthPlaceChangeNotifier.controllerSearch,
              style: TextStyle(color: Colors.black),
              textInputAction: TextInputAction.search,
              onFieldSubmitted: (e) {
                searchBirthPlaceChangeNotifier.searchBirthPlace(e);
              },
              onChanged: (e) {
                searchBirthPlaceChangeNotifier.changeAction(e);
              },
              cursorColor: Colors.black,
              decoration: new InputDecoration(
                hintText: "Cari Kabupaten/Kota (minimal 3 karakter)",
                hintStyle: TextStyle(color: Colors.black),
                enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: myPrimaryColor),
                ),
                focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: myPrimaryColor),
                ),
              ),
              textCapitalization: TextCapitalization.characters,
              autofocus: true,
            );
          },
        ),
        backgroundColor: myPrimaryColor,
        iconTheme: IconThemeData(color: Colors.black),
        actions: <Widget>[
          Provider.of<SearchBirthPlaceChangeNotifier>(context, listen: true)
              .showClear
              ? IconButton(
              icon: Icon(Icons.clear),
              onPressed: () {
                Provider.of<SearchBirthPlaceChangeNotifier>(context,
                    listen: false)
                    .controllerSearch
                    .clear();
                Provider.of<SearchBirthPlaceChangeNotifier>(context,
                    listen: false)
                    .changeAction(Provider.of<SearchBirthPlaceChangeNotifier>(
                    context,
                    listen: false)
                    .controllerSearch
                    .text);
              })
              : SizedBox(
            width: 0.0,
            height: 0.0,
          )
        ],
      ),
      body: Consumer<SearchBirthPlaceChangeNotifier>(
        builder: (context, searchBirthPlaceChangeNotifier, _) {
          if (searchBirthPlaceChangeNotifier.loadData == true) {
            return Center(
              child: CircularProgressIndicator(
                valueColor: new AlwaysStoppedAnimation<Color>(primaryOrange),
              ),
            );
          }
          else{
            return searchBirthPlaceChangeNotifier.loadData
                ?
            Center(child: CircularProgressIndicator())
                :
            searchBirthPlaceChangeNotifier.listBirthPlaceTemp.isNotEmpty
                ?
            ListView.separated(
            padding: EdgeInsets.symmetric(
                vertical: MediaQuery.of(context).size.height / 57,
                horizontal: MediaQuery.of(context).size.width / 27),
            itemCount: searchBirthPlaceChangeNotifier.listBirthPlaceTemp.length,
            itemBuilder: (listContext, index) {
              return InkWell(
                onTap: () {
                  Navigator.pop(context,
                      searchBirthPlaceChangeNotifier.listBirthPlaceTemp[index]);
                },
                child: Container(
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      Text(
                        "${searchBirthPlaceChangeNotifier.listBirthPlaceTemp[index].KABKOT_ID} - "
                            "${searchBirthPlaceChangeNotifier.listBirthPlaceTemp[index].KABKOT_NAME} ",
                        style: TextStyle(fontSize: 16),
                      )
                    ],
                  ),
                ),
              );
            },
            separatorBuilder: (context, index) {
              return Divider();
            },
          )
                :
            ListView.separated(
              padding: EdgeInsets.symmetric(
                  vertical: MediaQuery.of(context).size.height / 57,
                  horizontal: MediaQuery.of(context).size.width / 27),
              itemCount: searchBirthPlaceChangeNotifier.listBirthPlace.length,
              itemBuilder: (listContext, index) {
                return InkWell(
                  onTap: () {
                    Navigator.pop(context,
                        searchBirthPlaceChangeNotifier.listBirthPlace[index]);
                  },
                  child: Container(
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Text(
                          "${searchBirthPlaceChangeNotifier.listBirthPlace[index].KABKOT_ID} - "
                              "${searchBirthPlaceChangeNotifier.listBirthPlace[index].KABKOT_NAME} ",
                          style: TextStyle(fontSize: 16),
                        )
                      ],
                    ),
                  ),
                );
              },
              separatorBuilder: (context, index) {
                return Divider();
              },
            );
          }
        },
      ),
    );
  }
}
