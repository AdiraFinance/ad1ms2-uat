import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/shared/search_activity_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SearchActivity extends StatefulWidget {
  @override
  _SearchActivityState createState() => _SearchActivityState();
}

class _SearchActivityState extends State<SearchActivity> {

  @override
  void initState() {
    super.initState();
    Provider.of<SearchActivityChangeNotifier>(context,listen: false).getActivities(context);
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
          primaryColor: Colors.black,
          accentColor: myPrimaryColor,
          fontFamily: "NunitoSans",
          primarySwatch: primaryOrange
      ),
      child: Scaffold(
        key: Provider.of<SearchActivityChangeNotifier>(context,listen: false).scaffoldKey,
        appBar: AppBar(
          title: Consumer<SearchActivityChangeNotifier>(
            builder: (context, searchObjectPurposeChangeNotifier, _) {
              return TextFormField(
                controller: searchObjectPurposeChangeNotifier.controllerSearch,
                style: TextStyle(color: Colors.black),
                textInputAction: TextInputAction.search,
                onFieldSubmitted: (query) {
               searchObjectPurposeChangeNotifier.searchActivity(query);
                },
                onChanged: (e) {
                  searchObjectPurposeChangeNotifier.changeAction(e);
                },
                textCapitalization: TextCapitalization.characters,
                cursorColor: Colors.black,
                decoration: new InputDecoration(
                  hintText: "Cari Kegiatan (minimal 3 karakter)",
                  hintStyle: TextStyle(color: Colors.black),
                  enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: myPrimaryColor),
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: myPrimaryColor),
                  ),
                ),
                autofocus: true,
              );
            },
          ),
          backgroundColor: myPrimaryColor,
          iconTheme: IconThemeData(color: Colors.black),
          actions: <Widget>[
            Provider.of<SearchActivityChangeNotifier>(context, listen: true)
                .showClear
                ? IconButton(
                icon: Icon(Icons.clear),
                onPressed: () {
                  Provider.of<SearchActivityChangeNotifier>(context,
                      listen: false).clearSearchTemp();
                  Provider.of<SearchActivityChangeNotifier>(context,
                      listen: false)
                      .controllerSearch
                      .clear();
                  Provider.of<SearchActivityChangeNotifier>(context,
                      listen: false)
                      .changeAction(
                      Provider.of<SearchActivityChangeNotifier>(
                          context,
                          listen: false)
                          .controllerSearch
                          .text);
                })
                : SizedBox(
              width: 0.0,
              height: 0.0,
            )
          ],
        ),
        body: Consumer<SearchActivityChangeNotifier>(
          builder: (context, searchObjectPurposeChangeNotifier, _) {
            return searchObjectPurposeChangeNotifier.loadData
                ?
            Center(child: CircularProgressIndicator(),)
                :
            searchObjectPurposeChangeNotifier.listActivitiesTemp.isNotEmpty
                ?
            ListView.separated(
              padding: EdgeInsets.symmetric(
                  vertical: MediaQuery.of(context).size.height / 57,
                  horizontal: MediaQuery.of(context).size.width / 27),
              itemCount:
              searchObjectPurposeChangeNotifier.listActivitiesTemp.length,
              itemBuilder: (listContext, index) {
                return InkWell(
                  onTap: () {
                    Navigator.pop(
                        context,
                        searchObjectPurposeChangeNotifier
                            .listActivitiesTemp[index]);
                  },
                  child: Container(
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Text(
                          "${searchObjectPurposeChangeNotifier.listActivitiesTemp[index].kode} - "
                              "${searchObjectPurposeChangeNotifier.listActivitiesTemp[index].deskripsi} ",
                          style: TextStyle(fontSize: 16),
                        )
                      ],
                    ),
                  ),
                );
              },
              separatorBuilder: (context, index) {
                return Divider();
              },
            )
                :
            ListView.separated(
              padding: EdgeInsets.symmetric(
                  vertical: MediaQuery.of(context).size.height / 57,
                  horizontal: MediaQuery.of(context).size.width / 27),
              itemCount:
              searchObjectPurposeChangeNotifier.listActivities.length,
              itemBuilder: (listContext, index) {
                return InkWell(
                  onTap: () {
                    Navigator.pop(
                        context,
                        searchObjectPurposeChangeNotifier
                            .listActivities[index]);
                  },
                  child: Container(
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Text(
                          "${searchObjectPurposeChangeNotifier.listActivities[index].kode} - "
                              "${searchObjectPurposeChangeNotifier.listActivities[index].deskripsi} ",
                          style: TextStyle(fontSize: 16),
                        )
                      ],
                    ),
                  ),
                );
              },
              separatorBuilder: (context, index) {
                return Divider();
              },
            );
          },
        ),
      ),
    );
  }
}
