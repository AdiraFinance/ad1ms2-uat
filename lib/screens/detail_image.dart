import 'dart:io';

import 'package:ad1ms2_dev/main.dart';
import 'package:flutter/material.dart';

class DetailImage extends StatefulWidget {
  final File imageFile;

  const DetailImage({this.imageFile});
  @override
  _DetailImageState createState() => _DetailImageState();
}

class _DetailImageState extends State<DetailImage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: myPrimaryColor,
        iconTheme: IconThemeData(
          color: Colors.black
        ),
        title: Text(
            "Detail Image",
          style: TextStyle(
            color: Colors.black
          ),
        ),
      ),
      body: Center(
        child: Image.file(widget.imageFile),
      ),
    );
  }
}
