import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/shared/search_profesi_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SearchProfesi extends StatefulWidget {
  @override
  _SearchProfesiState createState() => _SearchProfesiState();
}

class _SearchProfesiState extends State<SearchProfesi> {
  @override
  void initState() {
    super.initState();
    Provider.of<SearchProfesiChangeNotifier>(context, listen: false).getDataProfessionType();
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
          primaryColor: Colors.black,
          accentColor: myPrimaryColor,
          fontFamily: "NunitoSans",
          primarySwatch: primaryOrange
      ),
      child: Scaffold(
        key: Provider.of<SearchProfesiChangeNotifier>(context, listen: false).scaffoldKey,
        appBar: AppBar(
          title: Consumer<SearchProfesiChangeNotifier>(
            builder: (context, searchProfesiChangeNotifier, _) {
              return TextFormField(
                controller: searchProfesiChangeNotifier.controllerSearch,
                style: TextStyle(color: Colors.black),
                textInputAction: TextInputAction.search,
                onFieldSubmitted: (query) {
                  // searchProgramChangeNotifier.se(query);
                },
                onChanged: (e) {
                  searchProfesiChangeNotifier.changeAction(e);
                },
                cursorColor: Colors.black,
                decoration: new InputDecoration(
                  hintText: "Cari Profesi (minimal 3 karakter)",
                  hintStyle: TextStyle(color: Colors.black),
                  enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: myPrimaryColor),
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: myPrimaryColor),
                  ),
                ),
                autofocus: true,
                textCapitalization: TextCapitalization.characters,
              );
            },
          ),
          backgroundColor: myPrimaryColor,
          iconTheme: IconThemeData(color: Colors.black),
          actions: <Widget>[
            Provider.of<SearchProfesiChangeNotifier>(context, listen: true)
                .showClear
                ? IconButton(
                icon: Icon(Icons.clear),
                onPressed: () {
                  Provider.of<SearchProfesiChangeNotifier>(context,
                      listen: false).clearSearchTemp();
                  Provider.of<SearchProfesiChangeNotifier>(context,
                      listen: false)
                      .controllerSearch
                      .clear();
                  Provider.of<SearchProfesiChangeNotifier>(context,
                      listen: false)
                      .changeAction(Provider.of<SearchProfesiChangeNotifier>(
                      context,
                      listen: false)
                      .controllerSearch
                      .text);
                })
                : SizedBox(
              width: 0.0,
              height: 0.0,
            )
          ],
        ),
        body: Consumer<SearchProfesiChangeNotifier>(
          builder: (context, searchProfesiChangeNotifier, _) {
            return searchProfesiChangeNotifier.loadData
                ?
            Center(child: CircularProgressIndicator())
                :
            searchProfesiChangeNotifier.listProfessionTypesTemp.isNotEmpty
                ?
            ListView.separated(
              padding: EdgeInsets.symmetric(
                  vertical: MediaQuery.of(context).size.height / 57,
                  horizontal: MediaQuery.of(context).size.width / 27),
              itemCount: searchProfesiChangeNotifier.listProfessionTypesTemp.length,
              itemBuilder: (listContext, index) {
                return InkWell(
                  onTap: () {
                    Navigator.pop(context,
                        searchProfesiChangeNotifier.listProfessionTypesTemp[index]);
                  },
                  child: Container(
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Text(
                          "${searchProfesiChangeNotifier.listProfessionTypesTemp[index].id} - "
                              "${searchProfesiChangeNotifier.listProfessionTypesTemp[index].desc} ",
                          style: TextStyle(fontSize: 16),
                        )
                      ],
                    ),
                  ),
                );
              },
              separatorBuilder: (context, index) {
                return Divider();
              },
            )
                :
            ListView.separated(
              padding: EdgeInsets.symmetric(
                  vertical: MediaQuery.of(context).size.height / 57,
                  horizontal: MediaQuery.of(context).size.width / 27),
              itemCount: searchProfesiChangeNotifier.listProfessionTypes.length,
              itemBuilder: (listContext, index) {
                return InkWell(
                  onTap: () {
                    Navigator.pop(context,
                        searchProfesiChangeNotifier.listProfessionTypes[index]);
                  },
                  child: Container(
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Text(
                          "${searchProfesiChangeNotifier.listProfessionTypes[index].id} - "
                              "${searchProfesiChangeNotifier.listProfessionTypes[index].desc} ",
                          style: TextStyle(fontSize: 16),
                        )
                      ],
                    ),
                  ),
                );
              },
              separatorBuilder: (context, index) {
                return Divider();
              },
            );
          },
        ),
      ),
    );
  }
}
