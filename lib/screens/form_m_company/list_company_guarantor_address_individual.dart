import 'package:ad1ms2_dev/screens/form_m/add_address_individu.dart';
import 'package:ad1ms2_dev/screens/form_m_company/company_add_guarantor_address_individual.dart';
import 'package:ad1ms2_dev/shared/form_m_company_add_address_guarantor_individual_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_company_add_guarantor_individual_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../main.dart';

class ListCompanyGuarantorAddressIndividual extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Theme(
        data: ThemeData(
            primaryColor: Colors.black,
            accentColor: myPrimaryColor,
            fontFamily: "NunitoSans",
          primarySwatch: primaryOrange
        ),
        child: Scaffold(
          appBar: AppBar(
            title: Text("List Alamat Penjamin Individu",
                style: TextStyle(color: Colors.black)),
            centerTitle: true,
            backgroundColor: myPrimaryColor,
            iconTheme: IconThemeData(color: Colors.black),
            actions: [
              // IconButton(
              //     icon: Icon(Icons.info_outline),
              //     onPressed: (){
              //       Provider.of<FormMCompanyAddGuarantorIndividualChangeNotifier>(context, listen: true).iconShowDialog(context);
              //     })
            ],
          ),
          body: Consumer<FormMCompanyAddGuarantorIndividualChangeNotifier>(
            builder: (context, formMAddGuarantorChangeNotifier, _) {
              return formMAddGuarantorChangeNotifier.listGuarantorAddress.isEmpty
              ? Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset("img/alamat_kosong.png", height: MediaQuery.of(context).size.height / 9,),
                    SizedBox(height: MediaQuery.of(context).size.height / 47,),
                    Text("Tambah Alamat", style: TextStyle(color: Colors.grey, fontSize: 16),)
                  ],
                ),
              )
              : ListView.builder(
                  padding: EdgeInsets.symmetric(
                      vertical: MediaQuery.of(context).size.height / 77,
                      horizontal: MediaQuery.of(context).size.width / 47),
                  itemBuilder: (context, index) {
                    return InkWell(
                      onLongPress: () {
                        formMAddGuarantorChangeNotifier.selectedIndex = index;
                        formMAddGuarantorChangeNotifier
                            .setCorrespondenceAddress(
                                formMAddGuarantorChangeNotifier
                                    .listGuarantorAddress[index],
                                index);
                      },
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ChangeNotifierProvider(
                                    create: (context) =>
                                        FormMCompanyAddAddressGuarantorIndividualChangeNotifier(),
                                    child: AddAddressIndividu(
                                        flag: 1,
                                        index: index,
                                        addressModel:
                                            formMAddGuarantorChangeNotifier
                                                    .listGuarantorAddress[
                                                index]))));
                      },
                      child: Card(
                        shape: formMAddGuarantorChangeNotifier.selectedIndex ==
                                index
                            ? RoundedRectangleBorder(
                                side:
                                    BorderSide(color: primaryOrange, width: 2),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(4)))
                            : null,
                        elevation: 3.3,
                        child: Padding(
                            padding: const EdgeInsets.all(16),
                            child: Stack(
                              children: [
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text(formMAddGuarantorChangeNotifier.listGuarantorAddress[index].jenisAlamatModel.DESKRIPSI, style: TextStyle(fontWeight: FontWeight.bold),),
                                    SizedBox(
                                      height:
                                      MediaQuery.of(context).size.height /
                                          97,
                                    ),
                                    Text("${formMAddGuarantorChangeNotifier.listGuarantorAddress[index].address}",
                                      style: TextStyle(color: Colors.grey, fontSize: 13),
                                    ),
                                    Text("${formMAddGuarantorChangeNotifier.listGuarantorAddress[index].kelurahanModel.KEC_NAME}, ${formMAddGuarantorChangeNotifier.listGuarantorAddress[index].kelurahanModel.KABKOT_NAME}, ${formMAddGuarantorChangeNotifier.listGuarantorAddress[index].kelurahanModel.PROV_NAME}",
                                      style: TextStyle(color: Colors.grey, fontSize: 13),
                                    ),
                                    Text("${formMAddGuarantorChangeNotifier.listGuarantorAddress[index].kelurahanModel.ZIPCODE}",
                                      style: TextStyle(color: Colors.grey, fontSize: 13),),
                                    SizedBox(
                                      height:
                                      MediaQuery.of(context).size.height /
                                          97,
                                    ),
                                    Text("${formMAddGuarantorChangeNotifier.listGuarantorAddress[index].areaCode} ${formMAddGuarantorChangeNotifier.listGuarantorAddress[index].phone}",
                                      style: TextStyle(color: Colors.grey, fontSize: 13),)
                                  ],
                                ),
                                Align(
                                  alignment: Alignment.topRight,
                                  child: GestureDetector(
                                    // onTap: () {formMinfoAlamat.deleteAlamatKorespondensi(context, index);},
                                      onTap: () {formMAddGuarantorChangeNotifier.moreDialog(context, index);},
                                      child: Icon(Icons.more_vert, color: Colors.grey)
                                  )
                                  // formMAddGuarantorChangeNotifier.listGuarantorAddress[index].jenisAlamatModel.KODE != "03"
                                  //     ? formMAddGuarantorChangeNotifier.listGuarantorAddress[index].isSameWithIdentity
                                  //     ? SizedBox()
                                  //     : GestureDetector(
                                  //     onTap: () {formMAddGuarantorChangeNotifier.deleteListGuarantorAddress(context, index);},
                                  //     child: Icon(Icons.delete, color: Colors.red)
                                  // )
                                  // IconButton(icon: Icon(Icons.delete, color: Colors.red),
                                  //     onPressed: () {
                                  //       // formMinfoAlamat.deleteAlamatKorespondensi(index);
                                  //     })
                                  //     : SizedBox(),
                                )
                              ],
                            )
                        ),
                      ),
                    );
                  },
                  itemCount: formMAddGuarantorChangeNotifier
                      .listGuarantorAddress.length);
            },
          ),
          floatingActionButton: FloatingActionButton(
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => ChangeNotifierProvider(
                            create: (context) =>
                                FormMCompanyAddAddressGuarantorIndividualChangeNotifier(),
                            child: CompanyAddGuarantorAddressIndividual(
                              flag: 0,
                              index: null,
                              addressModel: null,
                            )
                        )
                    )
                );
                // .then((value) => Provider.of<FormMCompanyAddGuarantorIndividualChangeNotifier>(context, listen: false).isShowDialog(context));
              },
              child: Icon(Icons.add),
              backgroundColor: myPrimaryColor),
        )
    );
  }
}
