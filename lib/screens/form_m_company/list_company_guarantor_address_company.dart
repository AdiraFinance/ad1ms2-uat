import 'package:ad1ms2_dev/screens/form_m_company/company_add_guarantor_address_company.dart';
import 'package:ad1ms2_dev/shared/form_m_company_add_guarantor_company_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../main.dart';
import 'form_m_company_add_guarantor_address_company_change_notifier.dart';

//ga kepake
class ListCompanyGuarantorAddressCompany extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Theme(
        data: ThemeData(
            primaryColor: Colors.black,
            accentColor: myPrimaryColor,
            fontFamily: "NunitoSans"),
        child: Scaffold(
          appBar: AppBar(
            title: Text("List Alamat Penjamin Kelembagaan",
                style: TextStyle(color: Colors.black)),
            centerTitle: true,
            backgroundColor: myPrimaryColor,
            iconTheme: IconThemeData(color: Colors.black),
            actions: [
              // IconButton(
              //     icon: Icon(Icons.info_outline),
              //     onPressed: (){
              //       Provider.of<FormMCompanyAddGuarantorCompanyChangeNotifier>(context, listen: true).iconShowDialog(context);
              //     })
            ],
          ),
          body: Consumer<FormMCompanyAddGuarantorCompanyChangeNotifier>(
            builder: (context, formMAddGuarantorChangeNotifier, _) {
              return formMAddGuarantorChangeNotifier.listGuarantorAddress.isEmpty
              ? Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset("img/alamat_kosong.png", height: MediaQuery.of(context).size.height / 9,),
                    SizedBox(height: MediaQuery.of(context).size.height / 47,),
                    Text("Tambah Alamat", style: TextStyle(color: Colors.grey, fontSize: 16),)
                  ],
                ),
              )
              : ListView.builder(
                  padding: EdgeInsets.symmetric(
                      vertical: MediaQuery.of(context).size.height / 77,
                      horizontal: MediaQuery.of(context).size.width / 47),
                  itemBuilder: (context, index) {
                    return InkWell(
                      onLongPress: () {
                        formMAddGuarantorChangeNotifier.selectedIndex = index;
                        formMAddGuarantorChangeNotifier.setCorrespondenceAddress(formMAddGuarantorChangeNotifier.listGuarantorAddress[index], index);
                      },
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ChangeNotifierProvider(
                                    create: (context) =>
                                        FormMCompanyAddGuarantorAddressCompanyChangeNotifier(),
                                    child: CompanyAddGuarantorAddressCompany(
                                        flag: 1,
                                        index: index,
                                        addressModel:
                                            formMAddGuarantorChangeNotifier
                                                    .listGuarantorAddress[
                                                index]
                                    )
                                )
                            )
                        ).then((value) => formMAddGuarantorChangeNotifier.listGuarantorAddress[index].isCorrespondence ? formMAddGuarantorChangeNotifier.setCorrespondenceAddress(formMAddGuarantorChangeNotifier.listGuarantorAddress[index], index) : null );
                      },
                      child: Card(
                        shape: formMAddGuarantorChangeNotifier.selectedIndex ==
                                index
                            ? RoundedRectangleBorder(
                                side:
                                    BorderSide(color: primaryOrange, width: 2),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(4)))
                            : null,
                        elevation: 3.3,
                        child: Padding(
                            padding: const EdgeInsets.all(16),
                            child: Stack(
                              children: [
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text(formMAddGuarantorChangeNotifier.listGuarantorAddress[index].jenisAlamatModel.DESKRIPSI, style: TextStyle(fontWeight: FontWeight.bold),),
                                    SizedBox(
                                      height:
                                      MediaQuery.of(context).size.height /
                                          97,
                                    ),
                                    Text("${formMAddGuarantorChangeNotifier.listGuarantorAddress[index].address}",
                                      style: TextStyle(color: Colors.grey, fontSize: 13),
                                    ),
                                    Text("${formMAddGuarantorChangeNotifier.listGuarantorAddress[index].kelurahanModel.KEC_NAME}, ${formMAddGuarantorChangeNotifier.listGuarantorAddress[index].kelurahanModel.KABKOT_NAME}, ${formMAddGuarantorChangeNotifier.listGuarantorAddress[index].kelurahanModel.PROV_NAME}",
                                      style: TextStyle(color: Colors.grey, fontSize: 13),
                                    ),
                                    Text("${formMAddGuarantorChangeNotifier.listGuarantorAddress[index].kelurahanModel.ZIPCODE}",
                                      style: TextStyle(color: Colors.grey, fontSize: 13),),
                                    SizedBox(
                                      height:
                                      MediaQuery.of(context).size.height /
                                          97,
                                    ),
                                    Text("${formMAddGuarantorChangeNotifier.listGuarantorAddress[index].phoneArea1} ${formMAddGuarantorChangeNotifier.listGuarantorAddress[index].phone1}",
                                      style: TextStyle(color: Colors.grey, fontSize: 13),)
                                  ],
                                ),
                                Align(
                                  alignment: Alignment.topRight,
                                  child: formMAddGuarantorChangeNotifier.listGuarantorAddress[index].jenisAlamatModel.KODE != "03"
                                      ? formMAddGuarantorChangeNotifier.listGuarantorAddress[index].isSameWithIdentity
                                      ? SizedBox()
                                      : GestureDetector(
                                      // onTap: () {formMAddGuarantorChangeNotifier.deleteListGuarantorAddress(context, index);},
                                      onTap: () {formMAddGuarantorChangeNotifier.moreDialog(context, index);},
                                      child: Icon(Icons.delete, color: Colors.red)
                                  )
                                  // IconButton(icon: Icon(Icons.delete, color: Colors.red),
                                  //     onPressed: () {
                                  //       // formMinfoAlamat.deleteAlamatKorespondensi(index);
                                  //     })
                                      : SizedBox(),
                                )
                              ],
                            )
                        ),
                      ),
                    );
                  },
                  itemCount: formMAddGuarantorChangeNotifier
                      .listGuarantorAddress.length);
            },
          ),
          floatingActionButton: FloatingActionButton(
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => ChangeNotifierProvider(
                            create: (context) =>
                                FormMCompanyAddGuarantorAddressCompanyChangeNotifier(),
                            child: CompanyAddGuarantorAddressCompany(
                              flag: 0,
                              index: null,
                              addressModel: null,
                            )
                        )
                    )
                );
                // .then((value) => Provider.of<FormMCompanyAddGuarantorCompanyChangeNotifier>(context, listen: false).isShowDialog(context));
              },
              child: Icon(Icons.add),
              backgroundColor: myPrimaryColor),
        ));
  }
}
