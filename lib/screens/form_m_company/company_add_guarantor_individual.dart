import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/form_m_informasi_nasabah_model.dart';
import 'package:ad1ms2_dev/models/guarantor_idividual_model.dart';
import 'package:ad1ms2_dev/screens/form_m_company/list_company_guarantor_address_individual.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/form_m_company_add_guarantor_individual_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class CompanyAddGuarantorIndividual extends StatefulWidget {
  final int flag;
  final int index;
  final GuarantorIndividualModel model;
  const CompanyAddGuarantorIndividual({this.flag, this.index, this.model});
  @override
  _CompanyAddGuarantorIndividualState createState() =>
      _CompanyAddGuarantorIndividualState();
}

class _CompanyAddGuarantorIndividualState
    extends State<CompanyAddGuarantorIndividual> {
  Future<void> _setValueForEdit;

  @override
  void initState() {
    super.initState();
    if (widget.flag != 0) {
      _setValueForEdit =
          Provider.of<FormMCompanyAddGuarantorIndividualChangeNotifier>(context,
                  listen: false)
              .setValueForEdit(widget.model);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
          primaryColor: Colors.black,
          accentColor: myPrimaryColor,
          fontFamily: "NunitoSans",
          primarySwatch: primaryOrange),
      child: Scaffold(
        appBar: AppBar(
          title: Text(
              widget.flag == 0
                  ? "Add Penjamin Pribadi"
                  : "Edit Penjamin Pribadi",
              style: TextStyle(color: Colors.black)),
          centerTitle: true,
          backgroundColor: myPrimaryColor,
          iconTheme: IconThemeData(color: Colors.black),
        ),
        body: SingleChildScrollView(
            padding: EdgeInsets.symmetric(
                horizontal: MediaQuery.of(context).size.width / 27,
                vertical: MediaQuery.of(context).size.height / 57),
            child: widget.flag == 0
                ? Consumer<FormMCompanyAddGuarantorIndividualChangeNotifier>(
                    builder: (context,
                        formMCompanyAddGuarantorIndividualChangeNotifier, _) {
                      return Form(
                        key: formMCompanyAddGuarantorIndividualChangeNotifier
                            .key,
                        onWillPop: _onWillPop,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            DropdownButtonFormField<RelationshipStatusModel>(
                                autovalidate:
                                    formMCompanyAddGuarantorIndividualChangeNotifier
                                        .autoValidate,
                                validator: (e) {
                                  if (e == null) {
                                    return "Silahkan pilih status hubungan";
                                  } else {
                                    return null;
                                  }
                                },
                                value:
                                    formMCompanyAddGuarantorIndividualChangeNotifier
                                        .relationshipStatusModelSelected,
                                onChanged: (value) {
                                  formMCompanyAddGuarantorIndividualChangeNotifier
                                      .relationshipStatusModelSelected = value;
                                },
                                onTap: () {
                                  FocusManager.instance.primaryFocus.unfocus();
                                },
                                decoration: InputDecoration(
                                  labelText: "Status Hubungan ",
                                  border: OutlineInputBorder(),
                                  contentPadding:
                                      EdgeInsets.symmetric(horizontal: 10),
                                ),
                                items:
                                    formMCompanyAddGuarantorIndividualChangeNotifier
                                        .listRelationShipStatus
                                        .map((value) {
                                  return DropdownMenuItem<
                                      RelationshipStatusModel>(
                                    value: value,
                                    child: Text(
                                      value.PARA_FAMILY_TYPE_NAME,
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                  );
                                }).toList()),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height / 47),
                            DropdownButtonFormField<IdentityModel>(
                                autovalidate:
                                    formMCompanyAddGuarantorIndividualChangeNotifier
                                        .autoValidate,
                                validator: (e) {
                                  if (e == null) {
                                    return "Silahkan pilih jenis identitas";
                                  } else {
                                    return null;
                                  }
                                },
                                value:
                                    formMCompanyAddGuarantorIndividualChangeNotifier
                                        .identityModelSelected,
                                onChanged: (value) {
                                  formMCompanyAddGuarantorIndividualChangeNotifier
                                      .identityModelSelected = value;
                                },
                                onTap: () {
                                  FocusManager.instance.primaryFocus.unfocus();
                                },
                                decoration: InputDecoration(
                                  labelText: "Jenis Identitas",
                                  border: OutlineInputBorder(),
                                  contentPadding:
                                      EdgeInsets.symmetric(horizontal: 10),
                                ),
                                items:
                                    formMCompanyAddGuarantorIndividualChangeNotifier
                                        .lisIdentityModel
                                        .map((value) {
                                  return DropdownMenuItem<IdentityModel>(
                                    value: value,
                                    child: Text(
                                      value.name,
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                  );
                                }).toList()),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height / 47),
                            TextFormField(
                              controller:
                                  formMCompanyAddGuarantorIndividualChangeNotifier
                                      .controllerIdentityNumber,
                              autovalidate:
                                  formMCompanyAddGuarantorIndividualChangeNotifier
                                      .autoValidate,
                              validator: (e) {
                                if (e.isEmpty) {
                                  return "Tidak boleh kosong";
                                } else {
                                  return null;
                                }
                              },
                              style: new TextStyle(color: Colors.black),
                              decoration: new InputDecoration(
                                  labelText: 'Nomor Identitas',
                                  labelStyle: TextStyle(color: Colors.black),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8))),
                              inputFormatters: [
                                WhitelistingTextInputFormatter.digitsOnly,
                                // LengthLimitingTextInputFormatter(10),
                              ],
                              keyboardType: TextInputType.number,
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height / 47),
                            TextFormField(
                              controller:
                                  formMCompanyAddGuarantorIndividualChangeNotifier
                                      .controllerFullNameIdentity,
                              style: new TextStyle(color: Colors.black),
                              decoration: new InputDecoration(
                                  labelText: 'Nama Lengkap Sesuai Identitas',
                                  labelStyle: TextStyle(color: Colors.black),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8))),
                              keyboardType: TextInputType.text,
                              textCapitalization: TextCapitalization.characters,
                              autovalidate:
                                  formMCompanyAddGuarantorIndividualChangeNotifier
                                      .autoValidate,
                              validator: (e) {
                                if (e.isEmpty) {
                                  return "Tidak boleh kosong";
                                } else {
                                  return null;
                                }
                              },
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height / 47),
                            TextFormField(
                              controller:
                                  formMCompanyAddGuarantorIndividualChangeNotifier
                                      .controllerFullName,
                              style: new TextStyle(color: Colors.black),
                              decoration: new InputDecoration(
                                  labelText: 'Nama Lengkap',
                                  labelStyle: TextStyle(color: Colors.black),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8))),
                              keyboardType: TextInputType.text,
                              textCapitalization: TextCapitalization.characters,
                              autovalidate:
                                  formMCompanyAddGuarantorIndividualChangeNotifier
                                      .autoValidate,
                              validator: (e) {
                                if (e.isEmpty) {
                                  return "Tidak boleh kosong";
                                } else {
                                  return null;
                                }
                              },
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height / 47),
                            TextFormField(
                              onTap: () {
                                formMCompanyAddGuarantorIndividualChangeNotifier
                                    .selectBirthDate(context);
                              },
                              controller:
                                  formMCompanyAddGuarantorIndividualChangeNotifier
                                      .controllerBirthDate,
                              style: new TextStyle(color: Colors.black),
                              decoration: new InputDecoration(
                                  labelText: 'Tanggal Lahir',
                                  labelStyle: TextStyle(color: Colors.black),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8))),
                              autovalidate:
                                  formMCompanyAddGuarantorIndividualChangeNotifier
                                      .autoValidate,
                              validator: (e) {
                                if (e.isEmpty) {
                                  return "Tidak boleh kosong";
                                } else {
                                  return null;
                                }
                              },
                              readOnly: true,
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height / 47),
                            TextFormField(
                              controller:
                                  formMCompanyAddGuarantorIndividualChangeNotifier
                                      .controllerBirthPlaceIdentity,
                              style: new TextStyle(color: Colors.black),
                              decoration: new InputDecoration(
                                  labelText: 'Tempat Lahir Sesuai Identitas',
                                  labelStyle: TextStyle(color: Colors.black),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8))),
                              keyboardType: TextInputType.text,
                              textCapitalization: TextCapitalization.characters,
                              autovalidate:
                                  formMCompanyAddGuarantorIndividualChangeNotifier
                                      .autoValidate,
                              validator: (e) {
                                if (e.isEmpty) {
                                  return "Tidak boleh kosong";
                                } else {
                                  return null;
                                }
                              },
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height / 47),
                            TextFormField(
                              controller:
                                  formMCompanyAddGuarantorIndividualChangeNotifier
                                      .controllerBirthPlaceIdentity,
                              style: new TextStyle(color: Colors.black),
                              decoration: new InputDecoration(
                                  labelText: 'Tempat Lahir Sesuai Identitas',
                                  labelStyle: TextStyle(color: Colors.black),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8))),
                              keyboardType: TextInputType.text,
                              textCapitalization: TextCapitalization.characters,
                              autovalidate:
                                  formMCompanyAddGuarantorIndividualChangeNotifier
                                      .autoValidate,
                              validator: (e) {
                                if (e.isEmpty) {
                                  return "Tidak boleh kosong";
                                } else {
                                  return null;
                                }
                              },
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height / 47),
                            Text(
                              "Jenis Kelamin",
                              style: TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.w700,
                                  letterSpacing: 0.15),
                            ),
                            Row(
                              children: [
                                Row(
                                  children: [
                                    Radio(
                                        activeColor: primaryOrange,
                                        value: 1,
                                        groupValue:
                                        formMCompanyAddGuarantorIndividualChangeNotifier
                                            .radioValueGender,
                                        onChanged: (value) {
                                          formMCompanyAddGuarantorIndividualChangeNotifier
                                              .radioValueGender = value;
                                        }),
                                    Text("Laki Laki")
                                  ],
                                ),
                                Row(
                                  children: [
                                    Radio(
                                        activeColor: primaryOrange,
                                        value: 2,
                                        groupValue:
                                        formMCompanyAddGuarantorIndividualChangeNotifier
                                            .radioValueGender,
                                        onChanged: (value) {
                                          formMCompanyAddGuarantorIndividualChangeNotifier
                                              .radioValueGender = value;
                                        }),
                                    Text("Perempuan")
                                  ],
                                ),
                              ],
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height / 47),
                            TextFormField(
                              controller:
                                  formMCompanyAddGuarantorIndividualChangeNotifier
                                      .controllerCellPhoneNumber,
                              style: new TextStyle(color: Colors.black),
                              decoration: new InputDecoration(
                                  labelText: 'Nomor Handphone',
                                  labelStyle: TextStyle(color: Colors.black),
                                  prefixText: "08",
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8))),
                              inputFormatters: [
                                WhitelistingTextInputFormatter.digitsOnly,
                                // LengthLimitingTextInputFormatter(10),
                              ],
                              keyboardType: TextInputType.number,
                              autovalidate:
                                  formMCompanyAddGuarantorIndividualChangeNotifier
                                      .autoValidate,
                              validator: (e) {
                                if (e.isEmpty) {
                                  return "Tidak boleh kosong";
                                } else {
                                  return null;
                                }
                              },
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height / 47),
                            TextFormField(
                              autovalidate:
                                  formMCompanyAddGuarantorIndividualChangeNotifier
                                      .autoValidate,
                              validator: (e) {
                                if (e.isEmpty) {
                                  return "Tidak boleh kosong";
                                } else {
                                  return null;
                                }
                              },
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            ListCompanyGuarantorAddressIndividual()
//                                  ChangeNotifierProvider(
//                                      create: (context) =>
//                                          FormMAddGuarantorIndividualChangeNotifier(),
//                                      child:
//                                          ListGuarantorAddressIndividual())
                                        ));
                              },
                              controller:
                                  formMCompanyAddGuarantorIndividualChangeNotifier
                                      .controllerAddress,
                              style: TextStyle(color: Colors.black),
                              decoration: new InputDecoration(
                                  labelText: 'Alamat',
                                  labelStyle: TextStyle(color: Colors.black),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8))),
                              readOnly: true,
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height / 47),
                            TextFormField(
                              readOnly: true,
                              autovalidate:
                                  formMCompanyAddGuarantorIndividualChangeNotifier
                                      .autoValidate,
                              validator: (e) {
                                if (e.isEmpty) {
                                  return "Tidak boleh kosong";
                                } else {
                                  return null;
                                }
                              },
                              controller:
                                  formMCompanyAddGuarantorIndividualChangeNotifier
                                      .controllerAddressType,
                              style: new TextStyle(color: Colors.black),
                              decoration: new InputDecoration(
                                  filled: true,
                                  fillColor: Colors.black12,
                                  labelText: 'Jenis Alamat',
                                  labelStyle: TextStyle(color: Colors.black),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8))),
                              enabled: false,
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height / 47),
                            Row(
                              children: [
                                Expanded(
                                  flex: 5,
                                  child: TextFormField(
                                    readOnly: true,
                                    autovalidate:
                                        formMCompanyAddGuarantorIndividualChangeNotifier
                                            .autoValidate,
                                    validator: (e) {
                                      if (e.isEmpty) {
                                        return "Tidak boleh kosong";
                                      } else {
                                        return null;
                                      }
                                    },
                                    controller:
                                        formMCompanyAddGuarantorIndividualChangeNotifier
                                            .controllerRT,
                                    style: new TextStyle(color: Colors.black),
                                    decoration: new InputDecoration(
                                        filled: true,
                                        fillColor: Colors.black12,
                                        labelText: 'RT',
                                        labelStyle:
                                            TextStyle(color: Colors.black),
                                        border: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(8))),
                                    enabled: false,
                                  ),
                                ),
                                SizedBox(width: 8),
                                Expanded(
                                    flex: 5,
                                    child: TextFormField(
                                      readOnly: true,
                                      autovalidate:
                                          formMCompanyAddGuarantorIndividualChangeNotifier
                                              .autoValidate,
                                      validator: (e) {
                                        if (e.isEmpty) {
                                          return "Tidak boleh kosong";
                                        } else {
                                          return null;
                                        }
                                      },
                                      controller:
                                          formMCompanyAddGuarantorIndividualChangeNotifier
                                              .controllerRW,
                                      style: new TextStyle(color: Colors.black),
                                      decoration: new InputDecoration(
                                          filled: true,
                                          fillColor: Colors.black12,
                                          labelText: 'RW',
                                          labelStyle:
                                              TextStyle(color: Colors.black),
                                          border: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(8))),
                                      enabled: false,
                                    ))
                              ],
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height / 47),
                            TextFormField(
                              readOnly: true,
                              autovalidate:
                                  formMCompanyAddGuarantorIndividualChangeNotifier
                                      .autoValidate,
                              validator: (e) {
                                if (e.isEmpty) {
                                  return "Tidak boleh kosong";
                                } else {
                                  return null;
                                }
                              },
                              controller:
                                  formMCompanyAddGuarantorIndividualChangeNotifier
                                      .controllerKelurahan,
                              style: new TextStyle(color: Colors.black),
                              decoration: new InputDecoration(
                                  filled: true,
                                  fillColor: Colors.black12,
                                  labelText: 'Kelurahan',
                                  labelStyle: TextStyle(color: Colors.black),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8))),
                              enabled: false,
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height / 47),
                            TextFormField(
                              readOnly: true,
                              autovalidate:
                                  formMCompanyAddGuarantorIndividualChangeNotifier
                                      .autoValidate,
                              validator: (e) {
                                if (e.isEmpty) {
                                  return "Tidak boleh kosong";
                                } else {
                                  return null;
                                }
                              },
                              controller:
                                  formMCompanyAddGuarantorIndividualChangeNotifier
                                      .controllerKecamatan,
                              style: new TextStyle(color: Colors.black),
                              decoration: new InputDecoration(
                                  filled: true,
                                  fillColor: Colors.black12,
                                  labelText: 'Kecamatan',
                                  labelStyle: TextStyle(color: Colors.black),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8))),
                              enabled: false,
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height / 47),
                            TextFormField(
                              readOnly: true,
                              autovalidate:
                                  formMCompanyAddGuarantorIndividualChangeNotifier
                                      .autoValidate,
                              validator: (e) {
                                if (e.isEmpty) {
                                  return "Tidak boleh kosong";
                                } else {
                                  return null;
                                }
                              },
                              controller:
                                  formMCompanyAddGuarantorIndividualChangeNotifier
                                      .controllerKota,
                              style: TextStyle(color: Colors.black),
                              decoration: InputDecoration(
                                  filled: true,
                                  fillColor: Colors.black12,
                                  labelText: 'Kabupaten/Kota',
                                  labelStyle: TextStyle(color: Colors.black),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8))),
                              enabled: false,
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height / 47),
                            Row(
                              children: [
                                Expanded(
                                  flex: 7,
                                  child: TextFormField(
                                    readOnly: true,
                                    autovalidate:
                                        formMCompanyAddGuarantorIndividualChangeNotifier
                                            .autoValidate,
                                    validator: (e) {
                                      if (e.isEmpty) {
                                        return "Tidak boleh kosong";
                                      } else {
                                        return null;
                                      }
                                    },
                                    controller:
                                        formMCompanyAddGuarantorIndividualChangeNotifier
                                            .controllerProvinsi,
                                    style: new TextStyle(color: Colors.black),
                                    decoration: new InputDecoration(
                                        filled: true,
                                        fillColor: Colors.black12,
                                        labelText: 'Provinsi',
                                        labelStyle:
                                            TextStyle(color: Colors.black),
                                        border: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(8))),
                                    enabled: false,
                                  ),
                                ),
                                SizedBox(width: 8),
                                Expanded(
                                  flex: 3,
                                  child: TextFormField(
                                    readOnly: true,
                                    autovalidate:
                                        formMCompanyAddGuarantorIndividualChangeNotifier
                                            .autoValidate,
                                    validator: (e) {
                                      if (e.isEmpty) {
                                        return "Tidak boleh kosong";
                                      } else {
                                        return null;
                                      }
                                    },
                                    controller:
                                        formMCompanyAddGuarantorIndividualChangeNotifier
                                            .controllerPostalCode,
                                    style: new TextStyle(color: Colors.black),
                                    decoration: new InputDecoration(
                                        filled: true,
                                        fillColor: Colors.black12,
                                        labelText: 'Kode Pos',
                                        labelStyle:
                                            TextStyle(color: Colors.black),
                                        border: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(8))),
                                    enabled: false,
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height / 47),
                            Row(
                              children: [
                                Expanded(
                                  flex: 4,
                                  child: TextFormField(
                                    readOnly: true,
                                    autovalidate:
                                        formMCompanyAddGuarantorIndividualChangeNotifier
                                            .autoValidate,
                                    validator: (e) {
                                      if (e.isEmpty) {
                                        return "Tidak boleh kosong";
                                      } else {
                                        return null;
                                      }
                                    },
                                    keyboardType: TextInputType.number,
                                    controller:
                                        formMCompanyAddGuarantorIndividualChangeNotifier
                                            .controllerKodeArea,
                                    style: new TextStyle(color: Colors.black),
                                    decoration: new InputDecoration(
                                        filled: true,
                                        fillColor: Colors.black12,
                                        labelText: 'Telepon (Area)',
                                        labelStyle:
                                            TextStyle(color: Colors.black),
                                        border: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(8))),
                                    enabled: false,
                                  ),
                                ),
                                SizedBox(
                                    width:
                                        MediaQuery.of(context).size.width / 37),
                                Expanded(
                                  flex: 6,
                                  child: TextFormField(
                                    readOnly: true,
                                    autovalidate:
                                        formMCompanyAddGuarantorIndividualChangeNotifier
                                            .autoValidate,
                                    validator: (e) {
                                      if (e.isEmpty) {
                                        return "Tidak boleh kosong";
                                      } else {
                                        return null;
                                      }
                                    },
                                    keyboardType: TextInputType.number,
                                    inputFormatters: [
                                      WhitelistingTextInputFormatter.digitsOnly,
                                      LengthLimitingTextInputFormatter(10),
                                    ],
                                    controller:
                                        formMCompanyAddGuarantorIndividualChangeNotifier
                                            .controllerTlpn,
                                    style: new TextStyle(color: Colors.black),
                                    decoration: new InputDecoration(
                                        filled: true,
                                        fillColor: Colors.black12,
                                        labelText: 'Telepon',
                                        labelStyle:
                                            TextStyle(color: Colors.black),
                                        border: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(8))),
                                    enabled: false,
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      );
                    },
                  )
                : FutureBuilder(
                    future: _setValueForEdit,
                    builder: (context, snapshot) {
                      if (snapshot.connectionState == ConnectionState.waiting) {
                        return Center(
                          child: CircularProgressIndicator(),
                        );
                      }
                      return Consumer<
                          FormMCompanyAddGuarantorIndividualChangeNotifier>(
                        builder: (context,
                            formMCompanyAddGuarantorIndividualChangeNotifier,
                            _) {
                          return Form(
                            key:
                                formMCompanyAddGuarantorIndividualChangeNotifier
                                    .key,
                            onWillPop: _onWillPop,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                DropdownButtonFormField<
                                        RelationshipStatusModel>(
                                    autovalidate:
                                        formMCompanyAddGuarantorIndividualChangeNotifier
                                            .autoValidate,
                                    validator: (e) {
                                      if (e == null) {
                                        return "Silahkan pilih status hubungan";
                                      } else {
                                        return null;
                                      }
                                    },
                                    value:
                                        formMCompanyAddGuarantorIndividualChangeNotifier
                                            .relationshipStatusModelSelected,
                                    onChanged: (value) {
                                      formMCompanyAddGuarantorIndividualChangeNotifier
                                              .relationshipStatusModelSelected =
                                          value;
                                    },
                                    onTap: () {
                                      FocusManager.instance.primaryFocus.unfocus();
                                    },
                                    decoration: InputDecoration(
                                      labelText: "Status Hubungan ",
                                      border: OutlineInputBorder(),
                                      contentPadding:
                                          EdgeInsets.symmetric(horizontal: 10),
                                    ),
                                    items:
                                        formMCompanyAddGuarantorIndividualChangeNotifier
                                            .listRelationShipStatus
                                            .map((value) {
                                      return DropdownMenuItem<
                                          RelationshipStatusModel>(
                                        value: value,
                                        child: Text(
                                          value.PARA_FAMILY_TYPE_NAME,
                                          overflow: TextOverflow.ellipsis,
                                        ),
                                      );
                                    }).toList()),
                                SizedBox(
                                    height: MediaQuery.of(context).size.height /
                                        47),
                                DropdownButtonFormField<IdentityModel>(
                                    autovalidate:
                                        formMCompanyAddGuarantorIndividualChangeNotifier
                                            .autoValidate,
                                    validator: (e) {
                                      if (e == null) {
                                        return "Silahkan pilih jenis identitas";
                                      } else {
                                        return null;
                                      }
                                    },
                                    value:
                                        formMCompanyAddGuarantorIndividualChangeNotifier
                                            .identityModelSelected,
                                    onChanged: (value) {
                                      formMCompanyAddGuarantorIndividualChangeNotifier
                                          .identityModelSelected = value;
                                    },
                                    onTap: () {
                                      FocusManager.instance.primaryFocus.unfocus();
                                    },
                                    decoration: InputDecoration(
                                      labelText: "Jenis Identitas",
                                      border: OutlineInputBorder(),
                                      contentPadding:
                                          EdgeInsets.symmetric(horizontal: 10),
                                    ),
                                    items:
                                        formMCompanyAddGuarantorIndividualChangeNotifier
                                            .lisIdentityModel
                                            .map((value) {
                                      return DropdownMenuItem<IdentityModel>(
                                        value: value,
                                        child: Text(
                                          value.name,
                                          overflow: TextOverflow.ellipsis,
                                        ),
                                      );
                                    }).toList()),
                                SizedBox(
                                    height: MediaQuery.of(context).size.height /
                                        47),
                                TextFormField(
                                  controller:
                                      formMCompanyAddGuarantorIndividualChangeNotifier
                                          .controllerIdentityNumber,
                                  autovalidate:
                                      formMCompanyAddGuarantorIndividualChangeNotifier
                                          .autoValidate,
                                  validator: (e) {
                                    if (e.isEmpty) {
                                      return "Tidak boleh kosong";
                                    } else {
                                      return null;
                                    }
                                  },
                                  style: new TextStyle(color: Colors.black),
                                  decoration: new InputDecoration(
                                      labelText: 'Nomor Identitas',
                                      labelStyle:
                                          TextStyle(color: Colors.black),
                                      border: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(8))),
                                  inputFormatters: [
                                    WhitelistingTextInputFormatter.digitsOnly,
                                    // LengthLimitingTextInputFormatter(10),
                                  ],
                                  keyboardType: TextInputType.number,
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.height /
                                        47),
                                TextFormField(
                                  controller:
                                      formMCompanyAddGuarantorIndividualChangeNotifier
                                          .controllerFullNameIdentity,
                                  style: new TextStyle(color: Colors.black),
                                  decoration: new InputDecoration(
                                      labelText:
                                          'Nama Lengkap Sesuai Identitas',
                                      labelStyle:
                                          TextStyle(color: Colors.black),
                                      border: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(8))),
                                  keyboardType: TextInputType.text,
                                  textCapitalization:
                                      TextCapitalization.characters,
                                  autovalidate:
                                      formMCompanyAddGuarantorIndividualChangeNotifier
                                          .autoValidate,
                                  validator: (e) {
                                    if (e.isEmpty) {
                                      return "Tidak boleh kosong";
                                    } else {
                                      return null;
                                    }
                                  },
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.height /
                                        47),
                                TextFormField(
                                  controller:
                                      formMCompanyAddGuarantorIndividualChangeNotifier
                                          .controllerFullName,
                                  style: new TextStyle(color: Colors.black),
                                  decoration: new InputDecoration(
                                      labelText: 'Nama Lengkap',
                                      labelStyle:
                                          TextStyle(color: Colors.black),
                                      border: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(8))),
                                  keyboardType: TextInputType.text,
                                  textCapitalization:
                                      TextCapitalization.characters,
                                  autovalidate:
                                      formMCompanyAddGuarantorIndividualChangeNotifier
                                          .autoValidate,
                                  validator: (e) {
                                    if (e.isEmpty) {
                                      return "Tidak boleh kosong";
                                    } else {
                                      return null;
                                    }
                                  },
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.height /
                                        47),
                                TextFormField(
                                  onTap: () {
                                    FocusManager.instance.primaryFocus.unfocus();
                                    formMCompanyAddGuarantorIndividualChangeNotifier
                                        .selectBirthDate(context);
                                  },
                                  controller:
                                      formMCompanyAddGuarantorIndividualChangeNotifier
                                          .controllerBirthDate,
                                  style: new TextStyle(color: Colors.black),
                                  decoration: new InputDecoration(
                                      labelText: 'Tanggal Lahir',
                                      labelStyle:
                                          TextStyle(color: Colors.black),
                                      border: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(8))),
                                  autovalidate:
                                      formMCompanyAddGuarantorIndividualChangeNotifier
                                          .autoValidate,
                                  validator: (e) {
                                    if (e.isEmpty) {
                                      return "Tidak boleh kosong";
                                    } else {
                                      return null;
                                    }
                                  },
                                  readOnly: true,
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.height /
                                        47),
                                TextFormField(
                                  controller:
                                      formMCompanyAddGuarantorIndividualChangeNotifier
                                          .controllerBirthPlaceIdentity,
                                  style: new TextStyle(color: Colors.black),
                                  decoration: new InputDecoration(
                                      labelText:
                                          'Tempat Lahir Sesuai Identitas',
                                      labelStyle:
                                          TextStyle(color: Colors.black),
                                      border: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(8))),
                                  keyboardType: TextInputType.text,
                                  textCapitalization:
                                      TextCapitalization.characters,
                                  autovalidate:
                                      formMCompanyAddGuarantorIndividualChangeNotifier
                                          .autoValidate,
                                  validator: (e) {
                                    if (e.isEmpty) {
                                      return "Tidak boleh kosong";
                                    } else {
                                      return null;
                                    }
                                  },
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.height /
                                        47),
                                TextFormField(
                                  controller:
                                      formMCompanyAddGuarantorIndividualChangeNotifier
                                          .controllerBirthPlaceIdentity,
                                  style: new TextStyle(color: Colors.black),
                                  decoration: new InputDecoration(
                                      labelText:
                                          'Tempat Lahir Sesuai Identitas',
                                      labelStyle:
                                          TextStyle(color: Colors.black),
                                      border: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(8))),
                                  keyboardType: TextInputType.text,
                                  textCapitalization:
                                      TextCapitalization.characters,
                                  autovalidate:
                                      formMCompanyAddGuarantorIndividualChangeNotifier
                                          .autoValidate,
                                  validator: (e) {
                                    if (e.isEmpty) {
                                      return "Tidak boleh kosong";
                                    } else {
                                      return null;
                                    }
                                  },
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.height /
                                        47),
                                Text(
                                  "Jenis Kelamin",
                                  style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.w700,
                                      letterSpacing: 0.15),
                                ),
                                Row(
                                  children: [
                                    Row(
                                      children: [
                                        Radio(
                                            activeColor: primaryOrange,
                                            value: 1,
                                            groupValue:
                                            formMCompanyAddGuarantorIndividualChangeNotifier
                                                .radioValueGender,
                                            onChanged: (value) {
                                              formMCompanyAddGuarantorIndividualChangeNotifier
                                                  .radioValueGender = value;
                                            }),
                                        Text("Laki Laki")
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        Radio(
                                            activeColor: primaryOrange,
                                            value: 2,
                                            groupValue:
                                            formMCompanyAddGuarantorIndividualChangeNotifier
                                                .radioValueGender,
                                            onChanged: (value) {
                                              formMCompanyAddGuarantorIndividualChangeNotifier
                                                  .radioValueGender = value;
                                            }),
                                        Text("Perempuan")
                                      ],
                                    ),
                                  ],
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.height /
                                        47),
                                TextFormField(
                                  controller:
                                      formMCompanyAddGuarantorIndividualChangeNotifier
                                          .controllerCellPhoneNumber,
                                  style: new TextStyle(color: Colors.black),
                                  decoration: new InputDecoration(
                                      labelText: 'Nomor Handphone',
                                      labelStyle:
                                          TextStyle(color: Colors.black),
                                      prefixText: "08",
                                      border: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(8))),
                                  inputFormatters: [
                                    WhitelistingTextInputFormatter.digitsOnly,
                                    // LengthLimitingTextInputFormatter(10),
                                  ],
                                  keyboardType: TextInputType.number,
                                  textCapitalization:
                                      TextCapitalization.characters,
                                  autovalidate:
                                      formMCompanyAddGuarantorIndividualChangeNotifier
                                          .autoValidate,
                                  validator: (e) {
                                    if (e.isEmpty) {
                                      return "Tidak boleh kosong";
                                    } else {
                                      return null;
                                    }
                                  },
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.height /
                                        47),
                                TextFormField(
                                  autovalidate:
                                      formMCompanyAddGuarantorIndividualChangeNotifier
                                          .autoValidate,
                                  validator: (e) {
                                    if (e.isEmpty) {
                                      return "Tidak boleh kosong";
                                    } else {
                                      return null;
                                    }
                                  },
                                  onTap: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                ListCompanyGuarantorAddressIndividual()));
                                  },
                                  controller:
                                      formMCompanyAddGuarantorIndividualChangeNotifier
                                          .controllerAddress,
                                  style: TextStyle(color: Colors.black),
                                  decoration: new InputDecoration(
                                      labelText: 'Alamat',
                                      labelStyle:
                                          TextStyle(color: Colors.black),
                                      border: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(8))),
                                  readOnly: true,
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.height /
                                        47),
                                TextFormField(
                                  readOnly: true,
                                  autovalidate:
                                      formMCompanyAddGuarantorIndividualChangeNotifier
                                          .autoValidate,
                                  validator: (e) {
                                    if (e.isEmpty) {
                                      return "Tidak boleh kosong";
                                    } else {
                                      return null;
                                    }
                                  },
                                  controller:
                                      formMCompanyAddGuarantorIndividualChangeNotifier
                                          .controllerAddressType,
                                  style: new TextStyle(color: Colors.black),
                                  decoration: new InputDecoration(
                                      filled: true,
                                      fillColor: Colors.black12,
                                      labelText: 'Jenis Alamat',
                                      labelStyle:
                                          TextStyle(color: Colors.black),
                                      border: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(8))),
                                  enabled: false,
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.height /
                                        47),
                                Row(
                                  children: [
                                    Expanded(
                                      flex: 5,
                                      child: TextFormField(
                                        readOnly: true,
                                        autovalidate:
                                            formMCompanyAddGuarantorIndividualChangeNotifier
                                                .autoValidate,
                                        validator: (e) {
                                          if (e.isEmpty) {
                                            return "Tidak boleh kosong";
                                          } else {
                                            return null;
                                          }
                                        },
                                        controller:
                                            formMCompanyAddGuarantorIndividualChangeNotifier
                                                .controllerRT,
                                        style:
                                            new TextStyle(color: Colors.black),
                                        decoration: new InputDecoration(
                                            filled: true,
                                            fillColor: Colors.black12,
                                            labelText: 'RT',
                                            labelStyle:
                                                TextStyle(color: Colors.black),
                                            border: OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(8))),
                                        enabled: false,
                                      ),
                                    ),
                                    SizedBox(width: 8),
                                    Expanded(
                                        flex: 5,
                                        child: TextFormField(
                                          readOnly: true,
                                          autovalidate:
                                              formMCompanyAddGuarantorIndividualChangeNotifier
                                                  .autoValidate,
                                          validator: (e) {
                                            if (e.isEmpty) {
                                              return "Tidak boleh kosong";
                                            } else {
                                              return null;
                                            }
                                          },
                                          controller:
                                              formMCompanyAddGuarantorIndividualChangeNotifier
                                                  .controllerRW,
                                          style: new TextStyle(
                                              color: Colors.black),
                                          decoration: new InputDecoration(
                                              filled: true,
                                              fillColor: Colors.black12,
                                              labelText: 'RW',
                                              labelStyle: TextStyle(
                                                  color: Colors.black),
                                              border: OutlineInputBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          8))),
                                          enabled: false,
                                        ))
                                  ],
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.height /
                                        47),
                                TextFormField(
                                  readOnly: true,
                                  autovalidate:
                                      formMCompanyAddGuarantorIndividualChangeNotifier
                                          .autoValidate,
                                  validator: (e) {
                                    if (e.isEmpty) {
                                      return "Tidak boleh kosong";
                                    } else {
                                      return null;
                                    }
                                  },
                                  controller:
                                      formMCompanyAddGuarantorIndividualChangeNotifier
                                          .controllerKelurahan,
                                  style: new TextStyle(color: Colors.black),
                                  decoration: new InputDecoration(
                                      filled: true,
                                      fillColor: Colors.black12,
                                      labelText: 'Kelurahan',
                                      labelStyle:
                                          TextStyle(color: Colors.black),
                                      border: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(8))),
                                  enabled: false,
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.height /
                                        47),
                                TextFormField(
                                  readOnly: true,
                                  autovalidate:
                                      formMCompanyAddGuarantorIndividualChangeNotifier
                                          .autoValidate,
                                  validator: (e) {
                                    if (e.isEmpty) {
                                      return "Tidak boleh kosong";
                                    } else {
                                      return null;
                                    }
                                  },
                                  controller:
                                      formMCompanyAddGuarantorIndividualChangeNotifier
                                          .controllerKecamatan,
                                  style: new TextStyle(color: Colors.black),
                                  decoration: new InputDecoration(
                                      filled: true,
                                      fillColor: Colors.black12,
                                      labelText: 'Kecamatan',
                                      labelStyle:
                                          TextStyle(color: Colors.black),
                                      border: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(8))),
                                  enabled: false,
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.height /
                                        47),
                                TextFormField(
                                  readOnly: true,
                                  autovalidate:
                                      formMCompanyAddGuarantorIndividualChangeNotifier
                                          .autoValidate,
                                  validator: (e) {
                                    if (e.isEmpty) {
                                      return "Tidak boleh kosong";
                                    } else {
                                      return null;
                                    }
                                  },
                                  controller:
                                      formMCompanyAddGuarantorIndividualChangeNotifier
                                          .controllerKota,
                                  style: TextStyle(color: Colors.black),
                                  decoration: InputDecoration(
                                      filled: true,
                                      fillColor: Colors.black12,
                                      labelText: 'Kabupaten/Kota',
                                      labelStyle:
                                          TextStyle(color: Colors.black),
                                      border: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(8))),
                                  enabled: false,
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.height /
                                        47),
                                Row(
                                  children: [
                                    Expanded(
                                      flex: 7,
                                      child: TextFormField(
                                        readOnly: true,
                                        autovalidate:
                                            formMCompanyAddGuarantorIndividualChangeNotifier
                                                .autoValidate,
                                        validator: (e) {
                                          if (e.isEmpty) {
                                            return "Tidak boleh kosong";
                                          } else {
                                            return null;
                                          }
                                        },
                                        controller:
                                            formMCompanyAddGuarantorIndividualChangeNotifier
                                                .controllerProvinsi,
                                        style:
                                            new TextStyle(color: Colors.black),
                                        decoration: new InputDecoration(
                                            filled: true,
                                            fillColor: Colors.black12,
                                            labelText: 'Provinsi',
                                            labelStyle:
                                                TextStyle(color: Colors.black),
                                            border: OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(8))),
                                        enabled: false,
                                      ),
                                    ),
                                    SizedBox(width: 8),
                                    Expanded(
                                      flex: 3,
                                      child: TextFormField(
                                        readOnly: true,
                                        autovalidate:
                                            formMCompanyAddGuarantorIndividualChangeNotifier
                                                .autoValidate,
                                        validator: (e) {
                                          if (e.isEmpty) {
                                            return "Tidak boleh kosong";
                                          } else {
                                            return null;
                                          }
                                        },
                                        controller:
                                            formMCompanyAddGuarantorIndividualChangeNotifier
                                                .controllerPostalCode,
                                        style:
                                            new TextStyle(color: Colors.black),
                                        decoration: new InputDecoration(
                                            filled: true,
                                            fillColor: Colors.black12,
                                            labelText: 'Kode Pos',
                                            labelStyle:
                                                TextStyle(color: Colors.black),
                                            border: OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(8))),
                                        enabled: false,
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.height /
                                        47),
                                Row(
                                  children: [
                                    Expanded(
                                      flex: 4,
                                      child: TextFormField(
                                        readOnly: true,
                                        autovalidate:
                                            formMCompanyAddGuarantorIndividualChangeNotifier
                                                .autoValidate,
                                        validator: (e) {
                                          if (e.isEmpty) {
                                            return "Tidak boleh kosong";
                                          } else {
                                            return null;
                                          }
                                        },
                                        keyboardType: TextInputType.number,
                                        controller:
                                            formMCompanyAddGuarantorIndividualChangeNotifier
                                                .controllerKodeArea,
                                        style:
                                            new TextStyle(color: Colors.black),
                                        decoration: new InputDecoration(
                                            filled: true,
                                            fillColor: Colors.black12,
                                            labelText: 'Telepon (Area)',
                                            labelStyle:
                                                TextStyle(color: Colors.black),
                                            border: OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(8))),
                                        enabled: false,
                                      ),
                                    ),
                                    SizedBox(
                                        width:
                                            MediaQuery.of(context).size.width /
                                                37),
                                    Expanded(
                                      flex: 6,
                                      child: TextFormField(
                                        readOnly: true,
                                        autovalidate:
                                            formMCompanyAddGuarantorIndividualChangeNotifier
                                                .autoValidate,
                                        validator: (e) {
                                          if (e.isEmpty) {
                                            return "Tidak boleh kosong";
                                          } else {
                                            return null;
                                          }
                                        },
                                        keyboardType: TextInputType.number,
                                        inputFormatters: [
                                          WhitelistingTextInputFormatter
                                              .digitsOnly,
                                          LengthLimitingTextInputFormatter(10),
                                        ],
                                        controller:
                                            formMCompanyAddGuarantorIndividualChangeNotifier
                                                .controllerTlpn,
                                        style:
                                            new TextStyle(color: Colors.black),
                                        decoration: new InputDecoration(
                                            filled: true,
                                            fillColor: Colors.black12,
                                            labelText: 'Telepon',
                                            labelStyle:
                                                TextStyle(color: Colors.black),
                                            border: OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(8))),
                                        enabled: false,
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          );
                        },
                      );
                    })),
        bottomNavigationBar: BottomAppBar(
          elevation: 0.0,
          child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: RaisedButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(8.0)),
                  color: myPrimaryColor,
                  onPressed: () {
                    if (widget.flag == 0) {
                      Provider.of<FormMCompanyAddGuarantorIndividualChangeNotifier>(
                              context,
                              listen: false)
                          .check(context, widget.flag, null);
                    } else {
                      Provider.of<FormMCompanyAddGuarantorIndividualChangeNotifier>(
                              context,
                              listen: false)
                          .check(context, widget.flag, widget.index);
                    }
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(widget.flag == 0 ? "SAVE" : "UPDATE",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 14,
                              fontWeight: FontWeight.w500,
                              letterSpacing: 1.25))
                    ],
                  ))),
        ),
      ),
    );
  }

  Future<bool> _onWillPop() async {
    var _provider =
        Provider.of<FormMCompanyAddGuarantorIndividualChangeNotifier>(context,
            listen: false);
    if (widget.flag == 0) {
      if (_provider.relationshipStatusModelSelected != null ||
          _provider.listGuarantorAddress.isNotEmpty ||
          _provider.identityModelSelected != null ||
          _provider.controllerIdentityNumber.text != "" ||
          _provider.controllerFullNameIdentity.text != "" ||
          _provider.controllerFullName.text != "" ||
          _provider.controllerBirthDate.text != "" ||
          _provider.controllerBirthPlaceIdentity.text != "") {
        return (await showDialog(
              context: context,
              builder: (myContext) => AlertDialog(
                title: new Text('Warning'),
                content: new Text('Simpan perubahan?'),
                actions: <Widget>[
                  new FlatButton(
                    onPressed: () {
                      _provider.check(context, widget.flag, null);
                      Navigator.pop(context, true);
                    },
                    child: new Text('Ya', style: TextStyle(fontSize: 12, color: Colors.grey),),
                  ),
                  new FlatButton(
                    onPressed: () {
                      _provider.clearData();
                      Navigator.of(context).pop(true);
                    },
                    child: new Text('Tidak'),
                  ),
                ],
              ),
            )) ??
            false;
      } else {
        return true;
      }
    } else {
      if (_provider.relationshipStatusModelTemp.PARA_FAMILY_TYPE_ID !=
              _provider.relationshipStatusModelSelected.PARA_FAMILY_TYPE_ID ||
          _provider.identityModelTemp.id !=
              _provider.identityModelSelected.id ||
          _provider.identiyNumberTemp !=
              _provider.controllerIdentityNumber.text ||
          _provider.fullNameIdentityTemp !=
              _provider.controllerFullNameIdentity.text ||
          _provider.fullNameTemp != _provider.controllerFullName.text ||
          _provider.birthDateTemp != _provider.controllerBirthDate.text ||
          _provider.birthPlaceTemp !=
              _provider.controllerBirthPlaceIdentity.text ||
          _provider.addressTemp != _provider.controllerAddress.text ||
          _provider.addressTypeTemp != _provider.controllerAddressType.text ||
          _provider.rtTemp != _provider.controllerRT.text ||
          _provider.rwTemp != _provider.controllerRW.text ||
          _provider.kelurahanTemp != _provider.controllerKelurahan.text ||
          _provider.kecamatanTemp != _provider.controllerKecamatan.text ||
          _provider.kotaTemp != _provider.controllerKota.text ||
          _provider.provTemp != _provider.controllerProvinsi.text ||
          _provider.postalCodeTemp != _provider.controllerPostalCode.text ||
          _provider.areaCodeTemp != _provider.controllerKodeArea.text ||
          _provider.phoneTemp != _provider.controllerTlpn.text ||
          _provider.sizeList != _provider.listGuarantorAddress.length) {
        return (await showDialog(
              context: context,
              builder: (myContext) => AlertDialog(
                title: new Text('Warning'),
                content: new Text('Simpan perubahan?'),
                actions: <Widget>[
                  FlatButton(
                    onPressed: () {
                      _provider.check(context, widget.flag, widget.index);
                      Navigator.pop(context);
                    },
                    child: new Text('Ya', style: TextStyle(fontSize: 12, color: Colors.grey),),
                  ),
                  FlatButton(
                    onPressed: () {
                      _provider.clearData();
                      Navigator.of(context).pop(true);
                    },
                    child: new Text('Tidak'),
                  ),
                ],
              ),
            )) ??
            false;
      } else {
        return (await showDialog(
              context: context,
              builder: (myContext) => AlertDialog(
                title: new Text('Warning'),
                content: new Text('Keluar dari edit data?'),
                actions: <Widget>[
                  new FlatButton(
                    onPressed: () {
                      _provider.clearData();
                      Navigator.pop(context, true);
                    },
                    child: new Text('Ya', style: TextStyle(fontSize: 12, color: Colors.grey),),
                  ),
                  new FlatButton(
                    onPressed: () => Navigator.of(context).pop(false),
                    child: new Text('Tidak'),
                  ),
                ],
              ),
            )) ??
            false;
      }
    }
  }
}
