import 'package:ad1ms2_dev/screens/form_m_company/list_pemegang_saham_pribadi.dart';
import 'package:ad1ms2_dev/shared/pemegang_saham_lembaga_change_notif.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_application_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_add_address_company_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_add_address_individu_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_company_parent_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_company_pemegang_saham_kelembagaan_alamat_change_notif.dart';
import 'package:ad1ms2_dev/shared/add_pemegang_saham_lembaga_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_company_pemegang_saham_pribadi_alamat_change_notif.dart';
import 'package:ad1ms2_dev/shared/pemegang_saham_pribadi_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../main.dart';
import 'form_m_pemegang_saham_kelembagaan_alamat.dart';
import 'form_m_pemegang_saham_pribadi_alamat.dart';
import 'list_pemegang_saham_lembaga.dart';

class MenuPemegangSaham extends StatelessWidget {
    @override
    Widget build(BuildContext context) {
        var _providerSahamPribadi = Provider.of<PemegangSahamPribadiChangeNotifier>(context, listen: true);
        var _providerSahamLembaga = Provider.of<PemegangSahamLembagaChangeNotifier>(context, listen: true);

        return Padding(
            padding: EdgeInsets.symmetric(
                horizontal: MediaQuery.of(context).size.width/37,
                vertical: MediaQuery.of(context).size.height/57,
            ),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                    InkWell(
                        onTap: () {
                            Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setDataAddressIndividu(context);
                            Navigator.push(context, MaterialPageRoute(
                                    builder: (context) => ListPemegangSahamPribadi()
                                )
                            );
                        },
                        child: Card(
                            elevation: 3.3,
                            child: Padding(
                                padding: EdgeInsets.all(16.0),
                                child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                        Text("Pemegang Saham Pribadi"),
                                        _providerSahamPribadi.flag
                                            ? Icon(Icons.check, color: primaryOrange)
                                            : SizedBox()
                                    ],
                                ),
                            ),
                        ),
                    ),
                    Visibility(
                      visible: _providerSahamPribadi.autoValidate,
                      child: Container(
                          margin: EdgeInsets.only(left: MediaQuery.of(context).size.width / 37),
                          child: Text("Data belum lengkap",style: TextStyle(color: Colors.red, fontSize: 12)),
                      ),
                    ),
                    SizedBox(height: MediaQuery.of(context).size.height / 87),
                    InkWell(
                       onTap: () {
                           Provider.of<FormMAddAddressCompanyChangeNotifier>(context, listen: false).getDataFromDashboard(context);
                           Navigator.push(context, MaterialPageRoute(
                                   builder: (context) => ListPemegangSahamLembaga()
                               )
                           );
                       },
                       child: Card(
                           elevation: 3.3,
                           child: Padding(
                               padding: EdgeInsets.all(16.0),
                               child: Row(
                                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                   children: <Widget>[
                                       Text("Pemegang Saham Kelembagaan"),
                                       _providerSahamLembaga.flag
                                           ? Icon(Icons.check, color: primaryOrange)
                                           : SizedBox()
                                   ],
                               ),
                           ),
                       ),
                   ),
                    _providerSahamLembaga.autoValidate
                        ?
                    Container(
                        margin: EdgeInsets.only(left: MediaQuery.of(context).size.width / 37),
                        child: Text("Data belum lengkap",style: TextStyle(color: Colors.red, fontSize: 12)),
                    )
                        :
                    SizedBox(height: MediaQuery.of(context).size.height / 87),
                ],
            ),
        );
    }

    Future<void> setNextState(BuildContext context,int index) async{
        SharedPreferences _preferences = await SharedPreferences.getInstance();
        var _providerSahamPribadi = Provider.of<PemegangSahamPribadiChangeNotifier>(context, listen: true);
        var _providerSahamLembaga = Provider.of<PemegangSahamLembagaChangeNotifier>(context, listen: true);
        var _providerApp = Provider.of<InfoAppChangeNotifier>(context, listen: true);

        _providerSahamPribadi.setDataFromSQLite();
        _providerSahamLembaga.setDataFromSQLite();

        if(_preferences.getString("last_known_state") == "IDE" && index != null){
            if(index != 4){
                Provider.of<FormMParentCompanyChangeNotifier>(context,listen: false).isMenuPemegangSaham = true;
                Provider.of<FormMParentCompanyChangeNotifier>(context,listen: false).selectedIndex += 1;
                _providerSahamPribadi.flag = true;
                _providerSahamLembaga.flag = true;
                _providerApp.addNumberOfUnitList(context, index);
            }
            else{
                Provider.of<FormMParentCompanyChangeNotifier>(context,listen: false).selectedIndex = 4;
            }
        }
    }

}
