import 'dart:io';

import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/shared/decimal_text_input_formatter.dart';
import 'package:ad1ms2_dev/shared/form_m_company_pendapatan_change_notif.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class FormMCompanyPendapatanProvider extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var _data = Provider.of<FormMCompanyPendapatanChangeNotifier>(context, listen: false);
    _data.getDataFromDashboard(context);
    return Theme(
      data: ThemeData(
        fontFamily: "NunitoSans",
        primaryColor: Colors.black,
        accentColor: myPrimaryColor,
        primarySwatch: primaryOrange,
      ),
      child: Consumer<FormMCompanyPendapatanChangeNotifier>(
        builder: (context, formMCompanyPendapatanChangeNotif, _) {
          return Scaffold(
            body: ListView(
              padding: EdgeInsets.symmetric(
                  vertical: MediaQuery.of(context).size.height/57,
                  horizontal: MediaQuery.of(context).size.width/37
              ),
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Visibility(
                      visible: _data.isMonthIncomeVisible(),
                      child: TextFormField(
                        autovalidate: formMCompanyPendapatanChangeNotif.autoValidate,
                        validator: (e) {
                          if (e.isEmpty && _data.isMonthIncomeMandatory()) {
                            return "Tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                        controller: formMCompanyPendapatanChangeNotif.controllerMonthlyIncome,
                        style: new TextStyle(color: Colors.black),
                        decoration: new InputDecoration(
                            labelText: 'Pendapatan Perbulan',
                            labelStyle: TextStyle(color: Colors.black),
                            border: OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
                            enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: formMCompanyPendapatanChangeNotif.monthIncomeDakor ? Colors.purple : Colors.grey)),
                            disabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: formMCompanyPendapatanChangeNotif.monthIncomeDakor ? Colors.purple : Colors.grey)),
                        ),
                        keyboardType: TextInputType.number,
                        textAlign: TextAlign.end,
                        onFieldSubmitted: (value) {
                          formMCompanyPendapatanChangeNotif.controllerMonthlyIncome.text = formMCompanyPendapatanChangeNotif.formatCurrency.formatCurrency(value);
                          formMCompanyPendapatanChangeNotif.calculateTotalIncome();
                          formMCompanyPendapatanChangeNotif.calculateGrossProfit();
                          formMCompanyPendapatanChangeNotif.calculateNetProfitBeforeTax();
                          formMCompanyPendapatanChangeNotif.calculateNetProfitAfterTax();
                        },
                        onTap: () {
                          formMCompanyPendapatanChangeNotif.formattingCompany();
                          formMCompanyPendapatanChangeNotif.calculateTotalIncome();
                          formMCompanyPendapatanChangeNotif.calculateGrossProfit();
                          formMCompanyPendapatanChangeNotif.calculateNetProfitBeforeTax();
                          formMCompanyPendapatanChangeNotif.calculateNetProfitAfterTax();
                        },
                        textInputAction: TextInputAction.done,
                        inputFormatters: [
                          DecimalTextInputFormatter(decimalRange: 2),
                          formMCompanyPendapatanChangeNotif.amountValidator
                        ],
                      ),
                    ),
                    Visibility(visible: _data.isMonthIncomeVisible(),child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                    Visibility(
                      visible: _data.isOtherIncomeVisible(),
                      child: TextFormField(
                        autovalidate: formMCompanyPendapatanChangeNotif.autoValidate,
                        validator: (e) {
                          if (e.isEmpty && _data.isOtherIncomeMandatory()) {
                            return "Tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                        controller: formMCompanyPendapatanChangeNotif.controllerOtherIncome,
                        style: new TextStyle(color: Colors.black),
                        decoration: new InputDecoration(
                            labelText: 'Pendapatan Lainnya',
                            labelStyle: TextStyle(color: Colors.black),
                            border: OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
                            enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: formMCompanyPendapatanChangeNotif.otherIncomeDakor ? Colors.purple : Colors.grey)),
                            disabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: formMCompanyPendapatanChangeNotif.otherIncomeDakor ? Colors.purple : Colors.grey)),
                        ),
                        keyboardType: TextInputType.number,
                        textAlign: TextAlign.end,
                        onFieldSubmitted: (value) {
                          formMCompanyPendapatanChangeNotif.controllerOtherIncome.text = formMCompanyPendapatanChangeNotif.formatCurrency.formatCurrency(value);
                          formMCompanyPendapatanChangeNotif.calculateTotalIncome();
                          formMCompanyPendapatanChangeNotif.calculateGrossProfit();
                          formMCompanyPendapatanChangeNotif.calculateNetProfitBeforeTax();
                          formMCompanyPendapatanChangeNotif.calculateNetProfitAfterTax();
                        },
                        onTap: () {
                          formMCompanyPendapatanChangeNotif.formattingCompany();
                          formMCompanyPendapatanChangeNotif.calculateTotalIncome();
                          formMCompanyPendapatanChangeNotif.calculateGrossProfit();
                          formMCompanyPendapatanChangeNotif.calculateNetProfitBeforeTax();
                          formMCompanyPendapatanChangeNotif.calculateNetProfitAfterTax();
                        },
                        textInputAction: TextInputAction.done,
                        inputFormatters: [
                          DecimalTextInputFormatter(decimalRange: 2),
                          formMCompanyPendapatanChangeNotif.amountValidator
                        ],
                      ),
                    ),
                    Visibility(visible: _data.isOtherIncomeVisible(),child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                    Visibility(
                      visible: _data.isTotalIncomeVisible(),
                      child: TextFormField(
                        enabled: false,
                        autovalidate: formMCompanyPendapatanChangeNotif.autoValidate,
                        validator: (e) {
                          if (e.isEmpty && _data.isTotalIncomeMandatory()) {
                            return "Tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                        controller: formMCompanyPendapatanChangeNotif.controllerTotalIncome,
                        style: new TextStyle(color: Colors.black),
                        textAlign: TextAlign.end,
                        decoration: new InputDecoration(
                            labelText: 'Total Pendapatan',
                            labelStyle: TextStyle(color: Colors.black),
                            border: OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
                            filled: true,
                            fillColor: Colors.black12,
                            enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: formMCompanyPendapatanChangeNotif.totalIncomeDakor ? Colors.purple : Colors.grey)),
                            disabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: formMCompanyPendapatanChangeNotif.totalIncomeDakor ? Colors.purple : Colors.grey)),
                        ),
                        keyboardType: TextInputType.number,
                        textCapitalization: TextCapitalization.characters,
                      ),
                    ),
                    Visibility(visible: _data.isTotalIncomeVisible(),child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                    Visibility(
                      visible: _data.isHppIncomeVisible(),
                      child: TextFormField(
                        autovalidate: formMCompanyPendapatanChangeNotif.autoValidate,
                        validator: (e) {
                          if (e.isEmpty && _data.isHppIncomeMandatory()) {
                            return "Tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                        controller: formMCompanyPendapatanChangeNotif.controllerCostOfRevenue,
                        style: new TextStyle(color: Colors.black),
                        decoration: new InputDecoration(
                            labelText: 'Harga Pokok Penjualan',
                            labelStyle: TextStyle(color: Colors.black),
                            border: OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
                            enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: formMCompanyPendapatanChangeNotif.hppIncomeDakor ? Colors.purple : Colors.grey)),
                            disabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: formMCompanyPendapatanChangeNotif.hppIncomeDakor ? Colors.purple : Colors.grey)),
                        ),
                        keyboardType: TextInputType.number,
                        textAlign: TextAlign.end,
                        onFieldSubmitted: (value) {
                          formMCompanyPendapatanChangeNotif.controllerCostOfRevenue.text = formMCompanyPendapatanChangeNotif.formatCurrency.formatCurrency(value);
                          formMCompanyPendapatanChangeNotif.calculateTotalIncome();
                          formMCompanyPendapatanChangeNotif.calculateGrossProfit();
                          formMCompanyPendapatanChangeNotif.calculateNetProfitBeforeTax();
                          formMCompanyPendapatanChangeNotif.calculateNetProfitAfterTax();
                        },
                        onTap: () {
                          formMCompanyPendapatanChangeNotif.formattingCompany();
                          formMCompanyPendapatanChangeNotif.calculateTotalIncome();
                          formMCompanyPendapatanChangeNotif.calculateGrossProfit();
                          formMCompanyPendapatanChangeNotif.calculateNetProfitBeforeTax();
                          formMCompanyPendapatanChangeNotif.calculateNetProfitAfterTax();
                        },
                        textInputAction: TextInputAction.done,
                        inputFormatters: [
                          DecimalTextInputFormatter(decimalRange: 2),
                          formMCompanyPendapatanChangeNotif.amountValidator
                        ],
                      ),
                    ),
                    Visibility(visible: _data.isHppIncomeVisible(),child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                    Visibility(
                      visible: _data.isLabaKotorVisible(),
                      child: TextFormField(
                        enabled: false,
                        autovalidate: formMCompanyPendapatanChangeNotif.autoValidate,
                        validator: (e) {
                          if (e.isEmpty && _data.isLabaKotorMandatory()) {
                            return "Tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                        controller: formMCompanyPendapatanChangeNotif.controllerGrossProfit,
                        style: new TextStyle(color: Colors.black),
                        textAlign: TextAlign.end,
                        decoration: new InputDecoration(
                            labelText: 'Laba Kotor',
                            labelStyle: TextStyle(color: Colors.black),
                            border: OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
                            filled: true,
                            fillColor: Colors.black12,
                            enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: formMCompanyPendapatanChangeNotif.labaKotorDakor ? Colors.purple : Colors.grey)),
                            disabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: formMCompanyPendapatanChangeNotif.labaKotorDakor ? Colors.purple : Colors.grey)),
                        ),
                        keyboardType: TextInputType.number,
                        textCapitalization: TextCapitalization.characters,
                      ),
                    ),
                    Visibility(visible: _data.isLabaKotorVisible(),child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                    Visibility(
                      visible: _data.isOperationalCostVisible(),
                      child: TextFormField(
                        autovalidate: formMCompanyPendapatanChangeNotif.autoValidate,
                        validator: (e) {
                          if (e.isEmpty && _data.isOperationalCostMandatory()) {
                            return "Tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                        controller: formMCompanyPendapatanChangeNotif.controllerOperatingCosts,
                        style: new TextStyle(color: Colors.black),
                        decoration: new InputDecoration(
                            labelText: 'Biaya Operasional',
                            labelStyle: TextStyle(color: Colors.black),
                            border: OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
                            enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: formMCompanyPendapatanChangeNotif.operationalCostDakor ? Colors.purple : Colors.grey)),
                            disabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: formMCompanyPendapatanChangeNotif.operationalCostDakor ? Colors.purple : Colors.grey)),
                        ),
                        keyboardType: TextInputType.number,
                        textAlign: TextAlign.end,
                        onFieldSubmitted: (value) {
                          formMCompanyPendapatanChangeNotif.controllerOperatingCosts.text = formMCompanyPendapatanChangeNotif.formatCurrency.formatCurrency(value);
                          formMCompanyPendapatanChangeNotif.calculateTotalIncome();
                          formMCompanyPendapatanChangeNotif.calculateGrossProfit();
                          formMCompanyPendapatanChangeNotif.calculateNetProfitBeforeTax();
                          formMCompanyPendapatanChangeNotif.calculateNetProfitAfterTax();
                        },
                        onTap: () {
                          formMCompanyPendapatanChangeNotif.formattingCompany();
                          formMCompanyPendapatanChangeNotif.calculateTotalIncome();
                          formMCompanyPendapatanChangeNotif.calculateGrossProfit();
                          formMCompanyPendapatanChangeNotif.calculateNetProfitBeforeTax();
                          formMCompanyPendapatanChangeNotif.calculateNetProfitAfterTax();
                        },
                        textInputAction: TextInputAction.done,
                        inputFormatters: [
                          DecimalTextInputFormatter(decimalRange: 2),
                          formMCompanyPendapatanChangeNotif.amountValidator
                        ],
                      ),
                    ),
                    Visibility(visible: _data.isOperationalCostVisible(),child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                    Visibility(
                      visible: _data.isOtherCostVisible(),
                      child: TextFormField(
                        autovalidate: formMCompanyPendapatanChangeNotif.autoValidate,
                        validator: (e) {
                          if (e.isEmpty && _data.isOtherCostMandatory()) {
                            return "Tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                        controller: formMCompanyPendapatanChangeNotif.controllerOtherCosts,
                        style:  TextStyle(color: Colors.black),
                        decoration: new InputDecoration(
                            labelText: 'Biaya Lainnya',
                            labelStyle: TextStyle(color: Colors.black),
                            border: OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
                            enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: formMCompanyPendapatanChangeNotif.otherCostDakor ? Colors.purple : Colors.grey)),
                            disabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: formMCompanyPendapatanChangeNotif.otherCostDakor ? Colors.purple : Colors.grey)),
                        ),
                        keyboardType: TextInputType.number,
                        textAlign: TextAlign.end,
                        onFieldSubmitted: (value) {
                          formMCompanyPendapatanChangeNotif.controllerOtherCosts.text = formMCompanyPendapatanChangeNotif.formatCurrency.formatCurrency(value);
                          formMCompanyPendapatanChangeNotif.calculateTotalIncome();
                          formMCompanyPendapatanChangeNotif.calculateGrossProfit();
                          formMCompanyPendapatanChangeNotif.calculateNetProfitBeforeTax();
                          formMCompanyPendapatanChangeNotif.calculateNetProfitAfterTax();
                        },
                        onTap: () {
                          formMCompanyPendapatanChangeNotif.formattingCompany();
                          formMCompanyPendapatanChangeNotif.calculateTotalIncome();
                          formMCompanyPendapatanChangeNotif.calculateGrossProfit();
                          formMCompanyPendapatanChangeNotif.calculateNetProfitBeforeTax();
                          formMCompanyPendapatanChangeNotif.calculateNetProfitAfterTax();
                        },
                        textInputAction: TextInputAction.done,
                        inputFormatters: [
                          DecimalTextInputFormatter(decimalRange: 2),
                          formMCompanyPendapatanChangeNotif.amountValidator
                        ],
                      ),
                    ),
                    Visibility(visible: _data.isOtherCostVisible(),child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                    Visibility(
                      visible: _data.isLabaBeforeTaxVisible(),
                      child: TextFormField(
                        enabled: false,
                        autovalidate: formMCompanyPendapatanChangeNotif.autoValidate,
                        validator: (e) {
                          if (e.isEmpty && _data.isLabaBeforeTaxMandatory()) {
                            return "Tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                        controller: formMCompanyPendapatanChangeNotif.controllerNetProfitBeforeTax,
                        style: new TextStyle(color: Colors.black),
                        textAlign: TextAlign.end,
                        decoration: new InputDecoration(
                            labelText: 'Laba Bersih Sebelum Pajak',
                            labelStyle: TextStyle(color: Colors.black),
                            border: OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
                            filled: true,
                            fillColor: Colors.black12,
                            enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: formMCompanyPendapatanChangeNotif.labaBeforeTaxDakor ? Colors.purple : Colors.grey)),
                            disabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: formMCompanyPendapatanChangeNotif.labaBeforeTaxDakor ? Colors.purple : Colors.grey)),
                        ),
                        keyboardType: TextInputType.number,
                        textCapitalization: TextCapitalization.characters,
                      ),
                    ),
                    Visibility(visible: _data.isLabaBeforeTaxVisible(),child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                    Visibility(
                      visible: _data.isTaxVisible(),
                      child: TextFormField(
                        autovalidate: formMCompanyPendapatanChangeNotif.autoValidate,
                        validator: (e) {
                          if (e.isEmpty && _data.isTaxMandatory()) {
                            return "Tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                        controller: formMCompanyPendapatanChangeNotif.controllerTax,
                        style: new TextStyle(color: Colors.black),
                        decoration: new InputDecoration(
                            labelText: 'Pajak',
                            labelStyle: TextStyle(color: Colors.black),
                            border: OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
                            enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: formMCompanyPendapatanChangeNotif.taxDakor ? Colors.purple : Colors.grey)),
                            disabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: formMCompanyPendapatanChangeNotif.taxDakor ? Colors.purple : Colors.grey)),
                        ),
                        keyboardType: TextInputType.number,
                        textAlign: TextAlign.end,
                        onFieldSubmitted: (value) {
                          formMCompanyPendapatanChangeNotif.controllerTax.text = formMCompanyPendapatanChangeNotif.formatCurrency.formatCurrency(value);
                          formMCompanyPendapatanChangeNotif.calculateTotalIncome();
                          formMCompanyPendapatanChangeNotif.calculateGrossProfit();
                          formMCompanyPendapatanChangeNotif.calculateNetProfitBeforeTax();
                          formMCompanyPendapatanChangeNotif.calculateNetProfitAfterTax();
                        },
                        onTap: () {
                          formMCompanyPendapatanChangeNotif.formattingCompany();
                          formMCompanyPendapatanChangeNotif.calculateTotalIncome();
                          formMCompanyPendapatanChangeNotif.calculateGrossProfit();
                          formMCompanyPendapatanChangeNotif.calculateNetProfitBeforeTax();
                          formMCompanyPendapatanChangeNotif.calculateNetProfitAfterTax();
                        },
                        textInputAction: TextInputAction.done,
                        inputFormatters: [
                          DecimalTextInputFormatter(decimalRange: 2),
                          formMCompanyPendapatanChangeNotif.amountValidator
                        ],
                      ),
                    ),
                    Visibility(visible: _data.isTaxVisible(),child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                    Visibility(
                      visible: _data.isLabaAfterTaxVisible(),
                      child: TextFormField(
                        enabled: false,
                        autovalidate: formMCompanyPendapatanChangeNotif.autoValidate,
                        validator: (e) {
                          if (e.isEmpty && _data.isLabaAfterTaxMandatory()) {
                            return "Tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                        controller: formMCompanyPendapatanChangeNotif.controllerNetProfitAfterTax,
                        style: new TextStyle(color: Colors.black),
                        textAlign: TextAlign.end,
                        decoration: new InputDecoration(
                            labelText: 'Laba Bersih Setelah Pajak',
                            labelStyle: TextStyle(color: Colors.black),
                            border: OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
                            filled: true,
                            fillColor: Colors.black12,
                            enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: formMCompanyPendapatanChangeNotif.labaAfterTaxDakor ? Colors.purple : Colors.grey)),
                            disabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: formMCompanyPendapatanChangeNotif.labaAfterTaxDakor ? Colors.purple : Colors.grey)),
                        ),
                        keyboardType: TextInputType.number,
                        textCapitalization: TextCapitalization.characters,
                      ),
                    ),
                    Visibility(visible: _data.isLabaAfterTaxVisible(),child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                    Visibility(
                      visible: _data.isOtherInstallmentVisible(),
                      child: TextFormField(
                        autovalidate: formMCompanyPendapatanChangeNotif.autoValidate,
                        validator: (e) {
                          if (e.isEmpty && _data.isOtherInstallmentMandatory()) {
                            return "Tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                        controller: formMCompanyPendapatanChangeNotif.controllerOtherInstallments,
                        style: new TextStyle(color: Colors.black),
                        decoration: new InputDecoration(
                            labelText: 'Angsuran Lainnya',
                            labelStyle: TextStyle(color: Colors.black),
                            border: OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
                            enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: formMCompanyPendapatanChangeNotif.otherInstallmentDakor ? Colors.purple : Colors.grey)),
                            disabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: formMCompanyPendapatanChangeNotif.otherInstallmentDakor ? Colors.purple : Colors.grey)),
                        ),
                        keyboardType: TextInputType.number,
                        textAlign: TextAlign.end,
                        onFieldSubmitted: (value) {
                          formMCompanyPendapatanChangeNotif.controllerOtherInstallments.text = formMCompanyPendapatanChangeNotif.formatCurrency.formatCurrency(value);
                          formMCompanyPendapatanChangeNotif.calculateTotalIncome();
                          formMCompanyPendapatanChangeNotif.calculateGrossProfit();
                          formMCompanyPendapatanChangeNotif.calculateNetProfitBeforeTax();
                          formMCompanyPendapatanChangeNotif.calculateNetProfitAfterTax();
                        },
                        onTap: () {
                          formMCompanyPendapatanChangeNotif.formattingCompany();
                          formMCompanyPendapatanChangeNotif.calculateTotalIncome();
                          formMCompanyPendapatanChangeNotif.calculateGrossProfit();
                          formMCompanyPendapatanChangeNotif.calculateNetProfitBeforeTax();
                          formMCompanyPendapatanChangeNotif.calculateNetProfitAfterTax();
                        },
                        textInputAction: TextInputAction.done,
                        inputFormatters: [
                          DecimalTextInputFormatter(decimalRange: 2),
                          formMCompanyPendapatanChangeNotif.amountValidator
                        ],
                      ),
                    ),
                  ],
                )
              ],
            ),
          );
        },
      )
    );
  }
}
