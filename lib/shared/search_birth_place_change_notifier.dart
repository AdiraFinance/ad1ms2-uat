import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/birth_place_model.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/io_client.dart';

class SearchBirthPlaceChangeNotifier with ChangeNotifier {
  bool _showClear = false;
  bool _loadData = false;
  TextEditingController _controllerSearch = TextEditingController();
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  var storage = FlutterSecureStorage();
  List<BirthPlaceModel> _listBirthPlace = [];
  List<BirthPlaceModel> _listBirthPlaceTemp = [];

  TextEditingController get controllerSearch => _controllerSearch;

  bool get showClear => _showClear;

  set showClear(bool value) {
    this._showClear = value;
    notifyListeners();
  }

  void changeAction(String value) {
    if (value != "") {
      showClear = true;
    } else {
      showClear = false;
    }
  }

  UnmodifiableListView<BirthPlaceModel> get listBirthPlace {
    return UnmodifiableListView(this._listBirthPlace);
  }

  UnmodifiableListView<BirthPlaceModel> get listBirthPlaceTemp {
    return UnmodifiableListView(this._listBirthPlaceTemp);
  }

  bool get loadData => _loadData;

  set loadData(bool value) {
    this._loadData = value;
    notifyListeners();
  }

  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  // void getBirthPlace(String query) async{
  //   if(query.length < 3) {
  //     showSnackBar("Input minimal 3 karakter");
  //   } else {
  //     this._listBirthPlace.clear();
  //     loadData = true;
  //     final ioc = new HttpClient();
  //     ioc.badCertificateCallback =
  //         (X509Certificate cert, String host, int port) => true;
  //
  //     final _http = IOClient(ioc);
  //
  //     var _body = jsonEncode({
  //       "P_FLAG":"2",
  //       "P_SEARCH": query
  //     });
  //
  //     String _fieldTempatLahirSesuaiIdentitas = await storage.read(key: "FieldTempatLahirSesuaiIdentitas");
  //     final _response = await _http.post(
  //       "${BaseUrl.urlGeneral}$_fieldTempatLahirSesuaiIdentitas",
  //         // "${urlPublic}api/address/get-alamat-full",
  //         body: _body,
  //         headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
  //     );
  //
  //     List<BirthPlaceModel> _listTemp = [];
  //     _listTemp.clear();
  //
  //     if(_response.statusCode == 200){
  //       final _result = jsonDecode(_response.body);
  //       final _data = _result['data'];
  //       if(_data.isNotEmpty){
  //         for(int i = 0; i < _data.length; i++){
  //           _listTemp.add(BirthPlaceModel(_data[i]['KABKOT_ID'], _data[i]['KABKOT_NAME']));
  //         }
  //         var _uniqueKode = _listTemp.map((e) => e.KABKOT_ID.trim()).toSet().toList();
  //         var _uniqueDescription = _listTemp.map((e) => e.KABKOT_NAME.trim()).toSet().toList();
  //
  //         for(var i =0; i<_uniqueKode.length; i++){
  //           BirthPlaceModel _myData = BirthPlaceModel(_uniqueKode[i], _uniqueDescription[i]);
  //           this._listBirthPlace.add(_myData);
  //         }
  //         loadData = false;
  //       }
  //       else{
  //         showSnackBar("Kota tidak ditemukan");
  //         loadData = false;
  //       }
  //     }
  //     else{
  //       showSnackBar("Error get kota response ${_response.statusCode}");
  //       loadData = false;
  //     }
  //   }
  // }
  void getBirthPlace() async{
    // if(query.length < 3) {
    //   showSnackBar("Input minimal 3 karakter");
    // } else {
      this._listBirthPlace.clear();
      this._loadData = true;
      final ioc = new HttpClient();
      ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;

      final _http = IOClient(ioc);

      // var _body = jsonEncode({
      //   "P_FLAG":"2",
      //   "P_SEARCH": query
      // });

      String _fieldTempatLahirSesuaiIdentitas = await storage.read(key: "FieldTempatLahirSesuaiIdentitasLOV");
      final _response = await _http.get(
          "${BaseUrl.urlGeneral}$_fieldTempatLahirSesuaiIdentitas",
          // "${urlPublic}api/address/get-alamat-full",
          // body: _body,
          headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
      );

      // List<BirthPlaceModel> _listTemp = [];
      // _listTemp.clear();

      if(_response.statusCode == 200){
        final _result = jsonDecode(_response.body);
        final _data = _result['data'];
        if(_data.isNotEmpty){
          for(int i = 0; i < _data.length; i++){
            this._listBirthPlace.add(BirthPlaceModel(_data[i]['KODE'], _data[i]['DESKRIPSI']));
          }
          // var _uniqueKode = _listTemp.map((e) => e.KABKOT_ID.trim()).toSet().toList();
          // var _uniqueDescription = _listTemp.map((e) => e.KABKOT_NAME.trim()).toSet().toList();

          // for(var i =0; i<_uniqueKode.length; i++){
          //   BirthPlaceModel _myData = BirthPlaceModel(_uniqueKode[i], _uniqueDescription[i]);
          //   this._listBirthPlace.add(_myData);
          // }
          this._loadData = false;
        }
        else{
          showSnackBar("Kota tidak ditemukan");
          this._loadData = false;
        }
      }
      else{
        showSnackBar("Error get kota response ${_response.statusCode}");
        this._loadData = false;
      }
      notifyListeners();
    // }
  }

  void searchBirthPlace(String query) {
    if(query.length < 3) {
      showSnackBar("Input minimal 3 karakter");
    } else {
      _listBirthPlaceTemp.clear();
      if (query.isEmpty) {
        return;
      }

      _listBirthPlace.forEach((dataKabKotAll) {
        if (dataKabKotAll.KABKOT_ID.contains(query) || dataKabKotAll.KABKOT_NAME.contains(query)) {
          _listBirthPlaceTemp.add(dataKabKotAll);
        }
      });
    }
    notifyListeners();
  }

  void showSnackBar(String text){
    this._scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor));
  }
}
