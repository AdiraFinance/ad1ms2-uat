import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';

import 'change_notifier_app/info_app_document_change_notifier.dart';
import 'constants.dart';
import 'form_m_foto_change_notif.dart';

class ChooseImagePicker{
  final _picker = ImagePicker();

  Future<Map> imageFromCamera(BuildContext context) async{
    String _date = formatDateListOID(DateTime.now()).replaceAll("-", "");
    String _time = formatTime(DateTime.now()).replaceAll(":", "");
    var _providerDocument = Provider.of<FormMFotoChangeNotifier>(context, listen: false);
    var _image = await _picker.getImage(source: ImageSource.camera, maxHeight: 480.0, maxWidth: 640.0,imageQuality: 100);
    if(_image!= null){
      File _imagePicked = File(_image.path);
      //resize img
      final filePath = _imagePicked.absolute.path;
      final lastIndex = filePath.lastIndexOf(new RegExp(r'.jp'));
      final splitted = filePath.substring(0, (lastIndex));
      final outPath = "${splitted}_out${filePath.substring(lastIndex)}";
      File _result = await FlutterImageCompress.compressAndGetFile(
        _imagePicked.absolute.path, outPath,
        quality: 50,
      );
      // List _splitFileName = _image.path.split("/");
      // int _idFileName = _splitFileName.length - 1;
      var _data = {
        "file":_result,
        "file_name": "FOTO_DOKUMEN_UNIT_${_providerDocument.listDocument.length+1}_${_date}_$_time.JPG"
      };
      return _data;
    }
    else{
      return null;
    }
  }

  Future<Map> imageFromCameraInfoDocument(BuildContext context) async{
    String _date = formatDateListOID(DateTime.now()).replaceAll("-", "");
    String _time = formatTime(DateTime.now()).replaceAll(":", "");
    var _providerDocument = Provider.of<InfoDocumentChangeNotifier>(context, listen: false);
    var _image = await _picker.getImage(source: ImageSource.camera, maxHeight: 480.0, maxWidth: 640.0,imageQuality: 100);
    if(_image!= null){
      File _imagePicked = File(_image.path);
      //resize img
      final filePath = _imagePicked.absolute.path;
      final lastIndex = filePath.lastIndexOf(new RegExp(r'.jp'));
      final splitted = filePath.substring(0, (lastIndex));
      final outPath = "${splitted}_out${filePath.substring(lastIndex)}";
      File _result = await FlutterImageCompress.compressAndGetFile(
        _imagePicked.absolute.path, outPath,
        quality: 50,
      );
      // List _splitFileName = _image.path.split("/");
      // int _idFileName = _splitFileName.length - 1;
      var _data = {
        "file":_result,
        "file_name": "FOTO_DOKUMEN_${_providerDocument.listInfoDocument.length+1}_${_date}_$_time.JPG"//_splitFileName[_idFileName],
      };
      return _data;
    }
    else{
      return null;
    }
  }

  Future<Map> fileFromGallery(BuildContext context) async{
    String _date = formatDateListOID(DateTime.now()).replaceAll("-", "");
    String _time = formatTime(DateTime.now()).replaceAll(":", "");
    var _providerDocument = Provider.of<FormMFotoChangeNotifier>(context, listen: false);
    File _file = await FilePicker.getFile(type: FileType.custom,allowedExtensions: ['pdf','docx','xlsx','jpg','png','jpeg']);

    //resize img
    final filePath = _file.absolute.path;
    final lastIndex = filePath.lastIndexOf(new RegExp(r'.jp'));
    final splitted = filePath.substring(0, (lastIndex));
    final outPath = "${splitted}_out${filePath.substring(lastIndex)}";
    File _result = await FlutterImageCompress.compressAndGetFile(
      _file.absolute.path, outPath,
      quality: 50,
    );
    if(_result != null){
      // List _splitFileName = _file.path.split("/");
      // int _idFileName = _splitFileName.length - 1;
      var _dataFile = {
        "file":_result,
        "file_name": "FOTO_DOKUMEN_UNIT_${_providerDocument.listDocument.length+1}_${_date}_$_time.JPG"//_splitFileName[_idFileName],
      };
      return _dataFile;
    }
    else{
      return null;
    }
  }

  Future<Map> fileFromGalleryInfoDocument(BuildContext context) async{
    String _date = formatDateListOID(DateTime.now()).replaceAll("-", "");
    String _time = formatTime(DateTime.now()).replaceAll(":", "");
    var _providerDocument = Provider.of<InfoDocumentChangeNotifier>(context, listen: false);
    File _file = await FilePicker.getFile(type: FileType.custom,allowedExtensions: ['pdf','docx','xlsx','jpg','png','jpeg']);

    //resize img
    final filePath = _file.absolute.path;
    final lastIndex = filePath.lastIndexOf(new RegExp(r'.jp'));
    final splitted = filePath.substring(0, (lastIndex));
    final outPath = "${splitted}_out${filePath.substring(lastIndex)}";
    File _result = await FlutterImageCompress.compressAndGetFile(
      _file.absolute.path, outPath,
      quality: 50,
    );
    if(_result != null){
      // List _splitFileName = _result.path.split("/");
      // int _idFileName = _splitFileName.length - 1;
      var _dataFile = {
        "file":_result,
        "file_name": "FOTO_DOKUMEN_${_providerDocument.listInfoDocument.length+1}_${_date}_$_time.JPG"//_splitFileName[_idFileName],
      };
      return _dataFile;
    }
    else{
      return null;
    }
  }

}