import 'package:ad1ms2_dev/models/guarantor_idividual_model.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import '../main.dart';

class FormMCompanyGuarantorChangeNotifier with ChangeNotifier {
  int _radioValueIsWithGuarantor = 0;
  List<GuarantorIndividualModel> _listGuarantorIndividual = [];

  List<GuarantorCompanyModel> _listGuarantorCompany = [];

  int get radioValueIsWithGuarantor => _radioValueIsWithGuarantor;

  set radioValueIsWithGuarantor(int value) {
    this._radioValueIsWithGuarantor = value;
    notifyListeners();
  }

  List<GuarantorIndividualModel> get listGuarantorIndividual =>
      _listGuarantorIndividual;

  List<GuarantorCompanyModel> get listGuarantorCompany => _listGuarantorCompany;

  void addGuarantorIndividual(GuarantorIndividualModel model) {
    this._listGuarantorIndividual.add(model);
    notifyListeners();
  }

  void addGuarantorCompany(GuarantorCompanyModel model) {
    this._listGuarantorCompany.add(model);
    notifyListeners();
  }

  void deleteListGuarantorIndividual(BuildContext context, int index) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context){
          return Theme(
            data: ThemeData(
                fontFamily: "NunitoSans",
                primaryColor: Colors.black,
                primarySwatch: primaryOrange,
                accentColor: myPrimaryColor
            ),
            child: AlertDialog(
              title: Text("Warning", style: TextStyle(fontWeight: FontWeight.bold)),
              content: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text("Apakah kamu yakin menghapus alamat ini?",),
                ],
              ),
              actions: <Widget>[
                new FlatButton(
                  onPressed: () {
                    this._listGuarantorIndividual.removeAt(index);
                    notifyListeners();
                    Navigator.pop(context);
                  },
                  child: new Text('Ya', style: TextStyle(color: Colors.grey),),
                ),
                new FlatButton(
                  onPressed: () => Navigator.of(context).pop(true),
                  child: new Text('Tidak'),
                ),
              ],
            ),
          );
        }
    );
  }

  void updateListGuarantorIndividual(
      GuarantorIndividualModel value, int index) {
    this._listGuarantorIndividual[index] = value;
    notifyListeners();
  }

  void updateListGuarantorCompany(GuarantorCompanyModel value, int index) {
    this._listGuarantorCompany[index] = value;
    notifyListeners();
  }
}
