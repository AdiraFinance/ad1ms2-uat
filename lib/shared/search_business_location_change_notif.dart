import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/models/form_m_pekerjaan_model.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/io_client.dart';

import '../main.dart';
import 'constants.dart';

class SearchBusinessLocationChangeNotif with ChangeNotifier {
    bool _showClear = false;
    bool _loadData = false;
    GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
    TextEditingController _controllerSearch = TextEditingController();

    List<BusinessLocationModel> _listBusinessLocationField = [];
    List<BusinessLocationModel> _listBusinessLocationFieldTemp = [];

    TextEditingController get controllerSearch => _controllerSearch;

    bool get showClear => _showClear;

    set showClear(bool value) {
        this._showClear = value;
        notifyListeners();
    }

    void changeAction(String value) {
        if (value != "") {
            showClear = true;
        } else {
            showClear = false;
        }
    }

    UnmodifiableListView<BusinessLocationModel> get listBusinessField {
        return UnmodifiableListView(this._listBusinessLocationField);
    }

    UnmodifiableListView<BusinessLocationModel> get listBusinessFieldTemp {
        return UnmodifiableListView(this._listBusinessLocationFieldTemp);
    }

    bool get loadData => _loadData;

    set loadData(bool value) {
        this._loadData = value;
        notifyListeners();
    }

    GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

    Future<void> getBusinessLocationField() async{
        this._listBusinessLocationField.clear();
        this._loadData = true;
        final ioc = new HttpClient();
        ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;

        final _http = IOClient(ioc);

        var storage = FlutterSecureStorage();
        String _fieldBusinessLocation = await storage.read(key: "FieldLokasiUsaha");
        final _response = await _http.get(
            "${BaseUrl.urlGeneral}$_fieldBusinessLocation",
            headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
            // "${urlPublic}api/occupation/get-lapangan-usaha",
        );

        if(_response.statusCode == 200){
            final _result = jsonDecode(_response.body);
            if(_result['data'].isNotEmpty){
                for(int i = 0 ; i < _result['data'].length ; i++) {
                    _listBusinessLocationField.add(BusinessLocationModel(_result['data'][i]['kode'], _result['data'][i]['deskripsi']));
                }
            }
            this._loadData = false;
        } else {
            showSnackBar("Error response status ${_response.statusCode}");
            this._loadData = false;
        }
        notifyListeners();
    }

    void searchBusinessLocationField(String query) {
        if(query.length < 3) {
            showSnackBar("Input minimal 3 karakter");
        } else {
            _listBusinessLocationFieldTemp.clear();
            if (query.isEmpty) {
                return;
            }
            _listBusinessLocationField.forEach((dataBusinessField) {
                if (dataBusinessField.id.contains(query) || dataBusinessField.desc.contains(query)) {
                    _listBusinessLocationFieldTemp.add(dataBusinessField);
                }
            });
        }
        notifyListeners();
    }

    void clearSearchTemp() {
        _listBusinessLocationFieldTemp.clear();
    }

    void showSnackBar(String text){
        this._scaffoldKey.currentState.showSnackBar(new SnackBar(
            content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor));
    }
}
