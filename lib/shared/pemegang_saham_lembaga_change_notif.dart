import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/address_guarantor_model.dart';
import 'package:ad1ms2_dev/models/form_m_informasi_alamat_model.dart';
import 'package:ad1ms2_dev/models/ms2_cust_addr_model.dart';
import 'package:ad1ms2_dev/models/ms2_cust_shrhldr_com_model.dart';
import 'package:ad1ms2_dev/models/pemegang_saham_model.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'constants.dart';

class PemegangSahamLembagaChangeNotifier with ChangeNotifier {
  List<PemegangSahamLembagaModel> _listPemegangSahamLembaga = [];
  bool _autoValidate = false;
  bool _flag = false;
  DbHelper _dbHelper = DbHelper();

  List<PemegangSahamLembagaModel> get listPemegangSahamLembaga => _listPemegangSahamLembaga;

  void addPemegangSahamLembaga(PemegangSahamLembagaModel value) {
    this._listPemegangSahamLembaga.add(value);
    notifyListeners();
  }

  void deleteListPemegangSahamLembaga(BuildContext context, int index) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return Theme(
            data: ThemeData(
                fontFamily: "NunitoSans",
                primaryColor: Colors.black,
                primarySwatch: primaryOrange,
                accentColor: myPrimaryColor
            ),
            child: AlertDialog(
              title: Text(
                  "Warning", style: TextStyle(fontWeight: FontWeight.bold)),
              content: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text("Apakah kamu yakin menghapus data ini?",),
                ],
              ),
              actions: <Widget>[
                new FlatButton(
                  onPressed: () {
                    this._listPemegangSahamLembaga.removeAt(index);
                    notifyListeners();
                    Navigator.pop(context);
                  },
                  child: new Text('Ya', style: TextStyle(color: Colors.grey),),
                ),
                new FlatButton(
                  onPressed: () => Navigator.of(context).pop(true),
                  child: new Text('Tidak'),
                ),
              ],
            ),
          );
        }
    );
  }

  void updateListPemegangSahamLembaga(int index, PemegangSahamLembagaModel value) {
    this._listPemegangSahamLembaga[index] = value;
    notifyListeners();
  }

  void clearDataPemegangSahamLembaga() {
    this._autoValidate = false;
    this._flag = false;
    this._listPemegangSahamLembaga.clear();
  }

  bool get autoValidate => _autoValidate;

  set autoValidate(bool value) {
    _autoValidate = value;
    notifyListeners();
  }

  bool get flag => _flag;

  set flag(bool value) {
    _flag = value;
    notifyListeners();
  }

  Future<bool> onBackPress() async {
    // if (_listPemegangSahamLembaga.isNotEmpty) {
      autoValidate = false;
      flag = true;
    // } else {
    //   autoValidate = true;
    //   flag = false;
    // }
    return true;
  }

  void saveToSQLite() async {
    print("save sqlite saham lembaga");
    List<MS2CustShrhldrComModel> _listData = [];
    for (int i = 0; i < _listPemegangSahamLembaga.length; i++) {
      _listData.add(MS2CustShrhldrComModel(
        "",
        this._listPemegangSahamLembaga[i].shareholdersCorporateID,
        this._listPemegangSahamLembaga[i].typeInstitutionModel,
        null,
        null,
        this._listPemegangSahamLembaga[i].institutionName,
        null,
        null,
        null,
        null,
        null,
        null,
        this._listPemegangSahamLembaga[i].initialDateForDateOfEstablishment,
        0,
        null,
        null,
        null,
        null,
        this._listPemegangSahamLembaga[i].npwp,
        null,
        null,
        this._listPemegangSahamLembaga[i].npwp != "" ? 1 : 0,
        // _provider.signPKPSelected,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        this._listPemegangSahamLembaga[i].percentShare.isEmpty ? 0 : int.parse(this._listPemegangSahamLembaga[i].percentShare),
        null,
        DateTime.now().toString(),
        null,
        null,
        null,
        1,
        this._listPemegangSahamLembaga[i].isTypeInstitutionModelChange ? "1" : "0",
        this._listPemegangSahamLembaga[i].isInstitutionNameChange ? "1" : "0",
        this._listPemegangSahamLembaga[i].isInitialDateForDateOfEstablishmentChange ? "1" : "0",
        this._listPemegangSahamLembaga[i].isNpwpChange ? "1" : "0",
        this._listPemegangSahamLembaga[i].isPercentShareChange ? "1" : "0",
      ));
    }
    _dbHelper.insertMS2CustShrhldrCom(_listData);

    //alamat
    List<MS2CustAddrModel> _listAddress = [];
    for(int i=0; i<_listPemegangSahamLembaga.length; i++){
      for(int j=0; j<_listPemegangSahamLembaga[i].listAddress.length; j++){
        _listAddress.add(MS2CustAddrModel(
          "123",
          _listPemegangSahamLembaga[i].listAddress[j].addressID,
          _listPemegangSahamLembaga[i].listAddress[j].foreignBusinessID,
          _listPemegangSahamLembaga[i].npwp,
          _listPemegangSahamLembaga[i].listAddress[j].isCorrespondence ? "1" : "0",
          "12",
          _listPemegangSahamLembaga[i].listAddress[j].address,
          _listPemegangSahamLembaga[i].listAddress[j].rt,
          null,
          _listPemegangSahamLembaga[i].listAddress[j].rw,
          null,
          _listPemegangSahamLembaga[i].listAddress[j].kelurahanModel.PROV_ID,
          _listPemegangSahamLembaga[i].listAddress[j].kelurahanModel.PROV_NAME,
          _listPemegangSahamLembaga[i].listAddress[j].kelurahanModel.KABKOT_ID,
          _listPemegangSahamLembaga[i].listAddress[j].kelurahanModel.KABKOT_NAME,
          _listPemegangSahamLembaga[i].listAddress[j].kelurahanModel.KEC_ID,
          _listPemegangSahamLembaga[i].listAddress[j].kelurahanModel.KEC_NAME,
          _listPemegangSahamLembaga[i].listAddress[j].kelurahanModel.KEL_ID,
          _listPemegangSahamLembaga[i].listAddress[j].kelurahanModel.KEL_NAME,
          _listPemegangSahamLembaga[i].listAddress[j].kelurahanModel.ZIPCODE,
          null,
          _listPemegangSahamLembaga[i].listAddress[j].phone1,
          _listPemegangSahamLembaga[i].listAddress[j].phoneArea1,
          _listPemegangSahamLembaga[i].listAddress[j].phone2,
          _listPemegangSahamLembaga[i].listAddress[j].phoneArea2,
          _listPemegangSahamLembaga[i].listAddress[j].fax,
          _listPemegangSahamLembaga[i].listAddress[j].faxArea,
          _listPemegangSahamLembaga[i].listAddress[j].jenisAlamatModel.KODE,
          _listPemegangSahamLembaga[i].listAddress[j].jenisAlamatModel.DESKRIPSI,
          _listPemegangSahamLembaga[i].listAddress[j].addressLatLong != null ?_listPemegangSahamLembaga[i].listAddress[j].addressLatLong['latitude'].toString() : null,
          _listPemegangSahamLembaga[i].listAddress[j].addressLatLong != null ?_listPemegangSahamLembaga[i].listAddress[j].addressLatLong['longitude'].toString() : null,
          _listPemegangSahamLembaga[i].listAddress[j].addressLatLong != null ?_listPemegangSahamLembaga[i].listAddress[j].addressLatLong['address'].toString() : null,
          null,
          null,
          null,
          null,
          _listPemegangSahamLembaga[i].listAddress[j].active,
          _listPemegangSahamLembaga[i].listAddress[j].isAddressTypeDakor ? "1" : "0",
          _listPemegangSahamLembaga[i].listAddress[j].isAddressDakor ? "1" : "0",
          _listPemegangSahamLembaga[i].listAddress[j].isRTDakor ? "1" : "0",
          _listPemegangSahamLembaga[i].listAddress[j].isRWDakor ? "1" : "0",
          _listPemegangSahamLembaga[i].listAddress[j].isKelurahanDakor ? "1" : "0",
          _listPemegangSahamLembaga[i].listAddress[j].isKecamatanDakor ? "1" : "0",
          _listPemegangSahamLembaga[i].listAddress[j].isKabKotDakor ? "1" : "0",
          _listPemegangSahamLembaga[i].listAddress[j].isProvinsiDakor ? "1" : "0",
          _listPemegangSahamLembaga[i].listAddress[j].isKodePosDakor ? "1" : "0",
          _listPemegangSahamLembaga[i].listAddress[j].isTelp1AreaDakor ? "1" : "0",
          _listPemegangSahamLembaga[i].listAddress[j].isTelp1Dakor ? "1" : "0",
          _listPemegangSahamLembaga[i].listAddress[j].isTelp2AreaDakor ? "1" : "0",
          _listPemegangSahamLembaga[i].listAddress[j].isTelp2Dakor ? "1" : "0",
          _listPemegangSahamLembaga[i].listAddress[j].isFaxAreaDakor ? "1" : "0",
          _listPemegangSahamLembaga[i].listAddress[j].isFaxDakor ? "1" : "0",
          _listPemegangSahamLembaga[i].listAddress[j].isDataLatLongDakor ? "1" : "0",
        ));
      }
    }
    _dbHelper.insertMS2CustAddr(_listAddress);
  }

  Future<bool> deleteSQLite() async {
    if(await _dbHelper.deleteMS2CustShrhldrCom() && await _dbHelper.deleteMS2CustAddr("12")){
      return true;
    }
    else{
      return false;
    }
  }

  Future<void> setDataFromSQLite() async {
    List _dataCustomerCompanyShrhldrCom = await _dbHelper.selectMS2CustShrhldrCom();
    List _addressCompany = await _dbHelper.selectMS2CustAddr("12");
    List<AddressModelCompany> _listAddressCom = [];

    if(_addressCompany.isNotEmpty){
      for(int i=0; i<_addressCompany.length; i++){
        var jenisAlamatModel = JenisAlamatModel(_addressCompany[i]['addr_type'], _addressCompany[i]['addr_desc']);
        var kelurahanModel = KelurahanModel(_addressCompany[i]['kelurahan'], _addressCompany[i]['kelurahan_desc'],
            _addressCompany[i]['kecamatan_desc'], _addressCompany[i]['kabkot_desc'], _addressCompany[i]['provinsi_desc'], _addressCompany[i]['zip_code'],
            _addressCompany[i]['kecamatan'], _addressCompany[i]['kabkot'], _addressCompany[i]['provinsi']);
        var _addressFromMap = {
          "address": _addressCompany[i]['address_from_map'] != "" && _addressCompany[i]['address_from_map'] != "null" ? _addressCompany[i]['address_from_map'] : '',
          "latitude": _addressCompany[i]['latitude'],
          "longitude": _addressCompany[i]['longitude']
        };
        var _koresponden = _addressCompany[i]['koresponden'].toString().toLowerCase();
        _listAddressCom.add(AddressModelCompany(
          jenisAlamatModel,
          _addressCompany[i]['addressID'],
          _addressCompany[i]['foreignBusinessID'],
          _addressCompany[i]['id_no'],
          kelurahanModel,
          _addressCompany[i]['address'].toString(),
          _addressCompany[i]['rt'].toString(),
          _addressCompany[i]['rw'].toString(),
          _addressCompany[i]['phone1_area'].toString(),
          _addressCompany[i]['phone1'].toString(),
          _addressCompany[i]['phone_2_area'].toString(),
          _addressCompany[i]['phone_2'].toString(),
          _addressCompany[i]['fax_area'].toString(),
          _addressCompany[i]['fax'].toString(),
          false,
          _addressFromMap,
          _koresponden == '1' ? true : false,
          _addressCompany[i]['active'],
          _addressCompany[i]['edit_addr_type'] == "1" ||
              _addressCompany[i]['edit_address'] == "1" ||
              _addressCompany[i]['edit_rt'] == "1" ||
              _addressCompany[i]['edit_rw'] == "1" ||
              _addressCompany[i]['edit_kelurahan'] == "1" ||
              _addressCompany[i]['edit_kecamatan'] == "1" ||
              _addressCompany[i]['edit_kabkot'] == "1" ||
              _addressCompany[i]['edit_provinsi'] == "1" ||
              _addressCompany[i]['edit_zip_code'] == "1" ||
              _addressCompany[i]['edit_phone1_area'] == "1" ||
              _addressCompany[i]['edit_phone1'] == "1" ||
              _addressCompany[i]['edit_phone_2_area'] == "1" ||
              _addressCompany[i]['edit_phone_2'] == "1" ||
              _addressCompany[i]['edit_fax_area'] == "1" ||
              _addressCompany[i]['edit_fax'] == "1" ||
              _addressCompany[i]['edit_address_from_map'] == "1" ? true : false,
          _addressCompany[i]['edit_addr_type'] == "1",
          _addressCompany[i]['edit_address'] == "1",
          _addressCompany[i]['edit_rt'] == "1",
          _addressCompany[i]['edit_rw'] == "1",
          _addressCompany[i]['edit_kelurahan'] == "1",
          _addressCompany[i]['edit_kecamatan'] == "1",
          _addressCompany[i]['edit_kabkot'] == "1",
          _addressCompany[i]['edit_provinsi'] == "1",
          _addressCompany[i]['edit_zip_code'] == "1",
          _addressCompany[i]['edit_phone1_area'] == "1",
          _addressCompany[i]['edit_phone1'] == "1",
          _addressCompany[i]['edit_phone_2_area'] == "1",
          _addressCompany[i]['edit_phone_2'] == "1",
          _addressCompany[i]['edit_fax_area'] == "1",
          _addressCompany[i]['edit_fax'] == "1",
          _addressCompany[i]['edit_address_from_map'] == "1",
        )
        );
      }
    }
    if(_dataCustomerCompanyShrhldrCom.isNotEmpty){
      for (int i = 0; i < _dataCustomerCompanyShrhldrCom.length; i++) {
        List<AddressModelCompany> _listAddressShareholder = [];
        for(int j=0; j<_listAddressCom.length; j++){
          if(_dataCustomerCompanyShrhldrCom[i]['npwp_no'] == _listAddressCom[j].numberID){
            _listAddressShareholder.add(_listAddressCom[j]);
          }
        }
        this._listPemegangSahamLembaga.add(PemegangSahamLembagaModel(
          TypeInstitutionModel(_dataCustomerCompanyShrhldrCom[i]['comp_type'], _dataCustomerCompanyShrhldrCom[i]['comp_type_desc']),
          _dataCustomerCompanyShrhldrCom[i]['comp_name'],
          _dataCustomerCompanyShrhldrCom[i]['establish_date'],
          _dataCustomerCompanyShrhldrCom[i]['npwp_no'],
          _dataCustomerCompanyShrhldrCom[i]['shrldng_percent'].toString(),
          _listAddressShareholder, //nanti alamat
          _dataCustomerCompanyShrhldrCom[i]['edit_comp_type'] == "1" || _dataCustomerCompanyShrhldrCom[i]['edit_comp_name'] == "1" ||
          _dataCustomerCompanyShrhldrCom[i]['edit_establish_date'] == "1" || _dataCustomerCompanyShrhldrCom[i]['edit_npwp_no'] == "1" ||
          _dataCustomerCompanyShrhldrCom[i]['edit_shrldng_percent'] == "1",
          _dataCustomerCompanyShrhldrCom[i]['edit_comp_type'] == "1",
          _dataCustomerCompanyShrhldrCom[i]['edit_comp_name'] == "1",
          _dataCustomerCompanyShrhldrCom[i]['edit_establish_date'] == "1",
          _dataCustomerCompanyShrhldrCom[i]['edit_npwp_no'] == "1",
          _dataCustomerCompanyShrhldrCom[i]['edit_shrldng_percent'] == "1",
            _dataCustomerCompanyShrhldrCom[i]['shareholdersCorporateID']
        ));
      }
    }
    notifyListeners();
  }

}
