import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/screens/list_oid.dart';
import 'package:ad1ms2_dev/shared/resource/get_set_vme.dart';
import 'package:ad1ms2_dev/shared/resource/validate_data.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import '../constants.dart';

class DedupIndiChangeNotifier with ChangeNotifier{

  TextEditingController _controllerNomorIdentitas = TextEditingController();
  TextEditingController _controllerNamaLengkap = TextEditingController();
  TextEditingController _controllerTempatLahir = TextEditingController();
  TextEditingController _controllerNamaGadisIbuKandung = TextEditingController();
  TextEditingController _controllerAlamatIdentitas = TextEditingController();
  TextEditingController _controllerBirthDate = TextEditingController();
  DateTime _initialDate;
  bool _processSaveDedupData = false;
  final _key = new GlobalKey<FormState>();
  bool _autoValidate = false;
  bool _loadData = false;
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  ValidateData _validateData = ValidateData();

  bool _isIdentityNoVisible = false;
  bool _isFullNameVisible = false;
  bool _isBirthDateVisible = false;
  bool _isBirthPlaceVisible = false;
  bool _isMotherNameVisible = false;
  bool _isAddressIdentityVisible = false;

  bool _isIdentityNoMandatory = false;
  bool _isFullNameMandatory = false;
  bool _isBirthDateMandatory = false;
  bool _isBirthPlaceMandatory = false;
  bool _isMotherNameMandatory = false;
  bool _isAddressIdentityMandatory = false;

  bool get isIdentityNoVisible => _isIdentityNoVisible;

  bool get isFullNameVisible => _isFullNameVisible;

  bool get isBirthDateVisible => _isBirthDateVisible;

  bool get isBirthPlaceVisible => _isBirthPlaceVisible;

  bool get isAddressIdentityMandatory => _isAddressIdentityMandatory;

  bool get isMotherNameMandatory => _isMotherNameMandatory;

  bool get isBirthPlaceMandatory => _isBirthPlaceMandatory;

  bool get isBirthDateMandatory => _isBirthDateMandatory;

  bool get isFullNameMandatory => _isFullNameMandatory;

  bool get isIdentityNoMandatory => _isIdentityNoMandatory;

  bool get isAddressIdentityVisible => _isAddressIdentityVisible;

  bool get isMotherNameVisible => _isMotherNameVisible;

  set isAddressIdentityMandatory(bool value) {
    this._isAddressIdentityMandatory = value;
  }

  set isMotherNameMandatory(bool value) {
    this._isMotherNameMandatory = value;
  }

  set isBirthPlaceMandatory(bool value) {
    this._isBirthPlaceMandatory = value;
  }

  set isBirthDateMandatory(bool value) {
    this._isBirthDateMandatory = value;
  }

  set isFullNameMandatory(bool value) {
    this._isFullNameMandatory = value;
  }

  set isIdentityNoMandatory(bool value) {
    this._isIdentityNoMandatory = value;
  }

  set isAddressIdentityVisible(bool value) {
    this._isAddressIdentityVisible = value;
  }

  set isMotherNameVisible(bool value) {
    this._isMotherNameVisible = value;
  }

  set isBirthPlaceVisible(bool value) {
    this._isBirthPlaceVisible = value;
  }

  set isBirthDateVisible(bool value) {
    this._isBirthDateVisible = value;
  }

  set isFullNameVisible(bool value) {
    this._isFullNameVisible = value;
  }

  set isIdentityNoVisible(bool value) {
    this._isIdentityNoVisible = value;
  }

  bool get autoValidate => _autoValidate;

  set autoValidate(bool value) {
    this._autoValidate = value;
    notifyListeners();
  }

  bool get processSaveDedupData => _processSaveDedupData;

  set processSaveDedupData(bool value) {
    this._processSaveDedupData = value;
    notifyListeners();
  }

  bool get loadData => _loadData;

  set loadData(bool value) {
    _loadData = value;
    notifyListeners();
  }

  get key => _key;

  DateTime get initialDate => _initialDate;

  TextEditingController get controllerBirthDate => _controllerBirthDate;

  TextEditingController get controllerAlamatIdentitas =>
      _controllerAlamatIdentitas;

  TextEditingController get controllerNamaGadisIbuKandung =>
      _controllerNamaGadisIbuKandung;

  TextEditingController get controllerTempatLahir => _controllerTempatLahir;

  TextEditingController get controllerNamaLengkap => _controllerNamaLengkap;

  TextEditingController get controllerNomorIdentitas =>
      _controllerNomorIdentitas;

  showDatePicker(BuildContext context) async {
    // DatePickerShared _datePicker = DatePickerShared();
    // var _dateSelected = await _datePicker.selectStartDate(context, _initialDate,
    //     canAccessNextDay: false);
    // if (_dateSelected != null) {
    //   setState(() {
    //     _initialDate = _dateSelected;
    //     _controllerBirthDate.text = dateFormat.format(_dateSelected);
    //   });
    // } else {
    //   return;
    // }
    print("cek date $_initialDate");
    var _dateSelected = await selectDateLast10Year(context, _initialDate);
    if (_dateSelected != null) {
        _initialDate = _dateSelected;
        _controllerBirthDate.text = dateFormat.format(_dateSelected);
        validateAgeOrName(context,"AGE");
    } else {
      return;
    }
  }

  check(BuildContext context) {
    final _form = _key.currentState;
    if (_form.validate()) {
      validateAgeOrName(context,"NAME & AGE");
    } else {
      this._autoValidate = true;
    }
  }

  Future<void> setValueMandatoryVisible() async{
    _initialDate = DateTime(DateTime.now().year-10,12,31);
    this._autoValidate = false;
    _controllerNomorIdentitas.clear();
    _controllerNamaLengkap.clear();
    _controllerTempatLahir.clear();
    _controllerNamaGadisIbuKandung.clear();
    _controllerAlamatIdentitas.clear();
    _controllerBirthDate.clear();
    try{
      List _dataMandatory = await getMandatoryDedupIndi();
      print(_dataMandatory[0]);
      List _dataVisible = await getVisibleDedupIndi();
      print(_dataVisible[0]);
      isIdentityNoMandatory = _dataMandatory[0]['noIdentitas'] == "1";
      isFullNameMandatory = _dataMandatory[0]['namaLengkap'] == "1";
      isBirthDateMandatory = _dataMandatory[0]['tglLahir'] == "1";
      isBirthPlaceMandatory = _dataMandatory[0]['tempatLahir'] == "1";
      isMotherNameMandatory = _dataMandatory[0]['namaGadis'] == "1";
      isAddressIdentityMandatory = _dataMandatory[0]['alamatIdentitas'] == "1";
      isIdentityNoVisible = _dataVisible[0]['noIdentitas'] == "1";
      isFullNameVisible = _dataVisible[0]['namaLengkap'] == "1";
      isBirthDateVisible = _dataVisible[0]['tglLahir'] == "1";
      isBirthPlaceVisible = _dataVisible[0]['tempatLahir'] == "1";
      isMotherNameVisible = _dataVisible[0]['namaGadis'] == "1";
      isAddressIdentityVisible = _dataVisible[0]['alamatIdentitas'] == "1";
    }
    catch(e){
      print("${e.toString()}");
    }
    notifyListeners();
  }


  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  validateAgeOrName(BuildContext context,String typeValidate) async{
    try{
      if(typeValidate == "AGE"){
        if(await _validateData.validateAge(this._controllerBirthDate.text) != "SUCCESS"){
          _showSnackBar(await _validateData.validateAge(this._controllerBirthDate.text));
        }
      }
      else if(typeValidate == "NAME"){
        print("NAME");
        if(this._controllerNamaLengkap.text != ""){
          if(await _validateData.validateName(this._controllerNamaLengkap.text,"PER") != "SUCCESS"){
            _showSnackBar("${await _validateData.validateAge(this._controllerNamaLengkap.text)} untuk nama");
          }
        }
      }
      else if(typeValidate == "MOTHER_NAME"){
        print("MOTHER_NAME");
        if(this._controllerNamaGadisIbuKandung.text != ""){
          if(await _validateData.validateName(this._controllerNamaGadisIbuKandung.text,"PER") != "SUCCESS"){
            _showSnackBar("${await _validateData.validateAge(this._controllerNamaGadisIbuKandung.text)} untuk nama ibu");
          }
        }
      }
      else{
        print("ELSE");
        loadData = true;
        if(await _validateData.validateAge(this._controllerBirthDate.text) != "SUCCESS"){
          _showSnackBar(await _validateData.validateAge(this._controllerBirthDate.text));
        }
        else if(await _validateData.validateName(this._controllerNamaGadisIbuKandung.text,"PER") != "SUCCESS"){
          _showSnackBar("${await _validateData.validateAge(this._controllerNamaGadisIbuKandung.text)} untuk nama ibu");
        }
        else if(await _validateData.validateName(this._controllerNamaLengkap.text,"PER") != "SUCCESS"){
          _showSnackBar("${await _validateData.validateAge(this._controllerNamaLengkap.text)} untuk nama");
        }
        else{
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => ListOid(
                    flag: "PER",
                    identityNumber: this._controllerNomorIdentitas.text,
                    fullname: this._controllerNamaLengkap.text,
                    birthDate: this._controllerBirthDate.text,
                    birthPlace: this._controllerTempatLahir.text,
                    motherName: this._controllerNamaGadisIbuKandung.text,
                    identityAddress: this._controllerAlamatIdentitas.text,
                    initialDateBirthDate: this._initialDate,
                  )
              )
          );
        }
        loadData = false;
      }
    }
    catch(e){
      _showSnackBar(e);
    }
  }

  void _showSnackBar(String text){
    this._scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: Text("$text"),
        behavior: SnackBarBehavior.floating,backgroundColor: snackbarColor, duration: Duration(seconds: 4)));
  }
}