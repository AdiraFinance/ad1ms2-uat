import 'dart:collection';
import 'dart:io';
import 'dart:typed_data';

import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/models/info_document_type_model.dart';
import 'package:ad1ms2_dev/screens/search_document_type.dart';
import 'package:ad1ms2_dev/shared/search_document_type_change_notifier.dart';
import 'package:file_utils/file_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart' as handler;
import 'package:provider/provider.dart';
import 'package:ad1ms2_dev/shared/resource/get_list_meta_data.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../main.dart';
import 'choose_image_picker.dart';
import 'constants.dart';
import 'date_picker.dart';
import 'document_unit_model.dart';
import 'form_m_foto_change_notif.dart';

class FormMFotoAddDocumentObjectUnit with ChangeNotifier {
  TextEditingController _controllerReceiveDocumentDate = TextEditingController();
  bool _autoValidate = false;
  bool _autoValidateFile = false;
  DateTime _initialDate = DateTime(dateNow.year, dateNow.month, dateNow.day);
  String _tglTerimaDocumentTemp;
  String _fileName;
  String _pathFile;
  GlobalKey<FormState> _key = GlobalKey<FormState>();
  ChooseImagePicker _chooseImagePicker = ChooseImagePicker();
  Map _fileDocumentUnitObject, _fileDocumentUnitObjectTemp;
  TextEditingController _controllerDocumentType = TextEditingController();
  InfoDocumentTypeModel _documentTypeSelected;
  String _orderSupportingDocumentID = "NEW";

  DbHelper _dbHelper = DbHelper();
  bool _isEdit = false;
  bool _isEditJenisDocument = false;
  bool _isEditDateTime = false;
  bool _isEditFileDocumentUnitObject = false;

  bool get isEdit => _isEdit;
  bool get isEditJenisDocument => _isEditJenisDocument;
  bool get isEditDateTime => _isEditDateTime;
  bool get isEditFileDocumentUnitObject => _isEditFileDocumentUnitObject;

  set isEdit(bool value) {
    _isEdit = value;
    notifyListeners();
  }
  set isEditJenisDocument(bool value) {
    _isEditJenisDocument = value;
    notifyListeners();
  }
  set isEditDateTime(bool value) {
    _isEditDateTime = value;
    notifyListeners();
  }
  set isEditFileDocumentUnitObject(bool value) {
    _isEditFileDocumentUnitObject = value;
    notifyListeners();
  }


  DateTime get initialDate => _initialDate;
  String _symbolName;

  set initialDate(DateTime value) {
    this._initialDate = value;
  }

  bool get autoValidate => _autoValidate;

  set autoValidate(bool value) {
    this._autoValidate = value;
    notifyListeners();
  }

  bool get autoValidateFile => _autoValidateFile;

  set autoValidateFile(bool value) {
    this._autoValidateFile = value;
    notifyListeners();
  }

  GlobalKey<FormState> get key => _key;

  TextEditingController get controllerReceiveDocumentDate => _controllerReceiveDocumentDate;

  void showDatePicker(BuildContext context) async {
    // DatePickerShared _datePicker = DatePickerShared();
    // var _dateSelected = await _datePicker.selectStartDate(context, _initialDate,
    //     canAccessNextDay: true);
    // if (_dateSelected != null) {
    //   _initialDate = _dateSelected;
    //   _controllerReceiveDocumentDate.text = dateFormat.format(_dateSelected);
    //   notifyListeners();
    // } else {
    //   return;
    // }
    var _dateSelected = await selectDateLastToday(context, _initialDate);
    if (_dateSelected != null) {
      _initialDate = _dateSelected;
      _controllerReceiveDocumentDate.text = dateFormat.format(_dateSelected);
      notifyListeners();
    } else {
      return;
    }
  }

  void showBottomSheetChooseFile(context) {
    showModalBottomSheet(context: context, builder: (BuildContext bc) {
          return Theme(
            data: ThemeData(fontFamily: "NunitoSans"),
            child: Container(
              child: Wrap(
                children: <Widget>[
                  FlatButton(
                      onPressed: () {
                        _imageFromCamera(context);
                        Navigator.pop(context);
                      },
                      child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Icon(
                              Icons.photo_camera,
                              color: myPrimaryColor,
                              size: 22.0,
                            ),
                            SizedBox(width: 12.0),
                            Text("Camera",
                              style: TextStyle(fontSize: 18.0),
                            )
                          ])),
                  FlatButton(
                      onPressed: () {
                        _fileFormGallery(context);
                        Navigator.pop(context);
                      },
                      child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Icon(
                              Icons.photo,
                              color: myPrimaryColor,
                              size: 22.0,
                            ),
                            SizedBox(width: 12.0),
                            Text("Gallery",
                              style: TextStyle(fontSize: 18.0),
                            )
                          ])),
                ],
              ),
            ),
          );
        });
  }

  Map get fileDocumentUnitObject => _fileDocumentUnitObject;

  set fileDocumentUnitObject(Map value) {
    this._fileDocumentUnitObject = value;
    notifyListeners();
  }

  void _imageFromCamera(BuildContext context) async {
    Map _imagePicker = await _chooseImagePicker.imageFromCamera(context);
    if (_imagePicker != null) {
      _fileDocumentUnitObject = _imagePicker;
      if (this._autoValidateFile) this._autoValidateFile = false;
      notifyListeners();
    } else {
      return;
    }
  }

  void _fileFormGallery(BuildContext context) async {
    Map _file = await _chooseImagePicker.fileFromGallery(context);
    if (_file != null) {
      _fileDocumentUnitObject = _file;
      if (this._autoValidateFile) this._autoValidateFile = false;
      notifyListeners();
    } else {
      return;
    }
  }

  void check(BuildContext context, int flag, int index) {
    final _form = this._key.currentState;
    if (flag == 0) {
      if (!_form.validate()) {
        this._autoValidate = true;
        notifyListeners();
      } else {
        if (this._fileDocumentUnitObject == null) {
          this._autoValidateFile = true;
          notifyListeners();
        } else {
          saveData(context, flag, index);
        }
      }
    } else {
      if (!_form.validate()) {
        this._autoValidate = true;
        notifyListeners();
      } else {
        if (this._fileDocumentUnitObject == null) {
          this._autoValidateFile = true;
          notifyListeners();
        } else {
          saveData(context, flag, index);
        }
      }
    }
  }

  Future<void> setValueForEditDocument(DocumentUnitModel documentUnitModel,int flag, int index) async {
    if(flag != 0){
      this._documentTypeSelected = documentUnitModel.jenisDocument;
      this._orderSupportingDocumentID = documentUnitModel.orderSupportingDocumentID;
      this._controllerDocumentType.text = "${documentUnitModel.jenisDocument.docTypeId} - ${documentUnitModel.jenisDocument.docTypeName}";
      this._initialDate = documentUnitModel.dateTime;
      this._controllerReceiveDocumentDate.text = dateFormat.format(documentUnitModel.dateTime);
      this._tglTerimaDocumentTemp = this._controllerReceiveDocumentDate.text;
      this._fileDocumentUnitObject = documentUnitModel.fileDocumentUnitObject;
      this._fileDocumentUnitObjectTemp = this._fileDocumentUnitObject;
      this._fileName = this._fileDocumentUnitObject['file_name'];
      this._pathFile = documentUnitModel.path;
      await checkDataDakor(index);
    }
  }

  String get fileName => _fileName;

  String get pathFile => _pathFile;

  String get tglTerimaDocumentTemp => _tglTerimaDocumentTemp;

  get fileDocumentUnitObjectTemp => _fileDocumentUnitObjectTemp;

  String get orderSupportingDocumentID => _orderSupportingDocumentID;

  set orderSupportingDocumentID(String value) {
    this._orderSupportingDocumentID = value;
  }

  void searchDocumentType(BuildContext context,int flag) async{
    InfoDocumentTypeModel data = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => SearchDocumentTypeChangeNotifier(),
                child: SearchDocumentType(flag))));
    if (data != null) {
      print(data.docTypeName);
      this._documentTypeSelected = data;
      this._controllerDocumentType.text = "${data.docTypeId} - ${data.docTypeName}";
      getListMeta(data.docTypeId);
      notifyListeners();
    } else {
      return;
    }
  }

  InfoDocumentTypeModel get documentTypeSelected => _documentTypeSelected;

  set documentTypeSelected(InfoDocumentTypeModel value) {
    this._documentTypeSelected = value;
    notifyListeners();
  }

  TextEditingController get controllerDocumentType => _controllerDocumentType;

 void saveData(BuildContext context, int flag, int index) async{
   try{
     File _myFile = this._fileDocumentUnitObject['file'];
     File _path = await _myFile.copy("$globalPath/${this._fileDocumentUnitObject['file_name']}");
     _pathFile = _path.path;
     Position _position = await Geolocator().getCurrentPosition(desiredAccuracy: LocationAccuracy.bestForNavigation);
     if(flag == 0){
       Provider.of<FormMFotoChangeNotifier>(context, listen: false).addListDocument(
           DocumentUnitModel(this._documentTypeSelected, this._orderSupportingDocumentID,
               this._initialDate, this._fileDocumentUnitObject, this._pathFile,
               _position.longitude, _position.longitude,_symbolName, "",
               _isEdit,_isEditJenisDocument,_isEditDateTime,_isEditFileDocumentUnitObject),
           context);
     } else {
       await checkDataDakor(index);
       Provider.of<FormMFotoChangeNotifier>(context, listen: false).updateListDocumentObjectUnit(
           DocumentUnitModel(this._documentTypeSelected, this._orderSupportingDocumentID, this._initialDate, this._fileDocumentUnitObject, this._pathFile,
               _position.longitude, _position.longitude,_symbolName, "", _isEdit,_isEditJenisDocument,_isEditDateTime,_isEditFileDocumentUnitObject),
           context, index);
     }
//      Uint8List bytes = this._fileDocumentUnitObject['file'].readAsBytesSync();
//      await file.writeAsBytes(bytes);
   }
   catch(e){
     print(e);
   }
 }

  Future<void> checkDataDakor(int index) async{
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    
    if(_preferences.getString("last_known_state") == "DKR"){
      List _check1 = await _dbHelper.selectMS2PhotoDetail();
      List _check = await _dbHelper.selectMS2PhotoDetail2();
      var _listFoto2Flag4 = _check.where((i) => i['flag']=="4").toList();

      if(_listFoto2Flag4.isNotEmpty){

        List _splitFileName = _listFoto2Flag4[index]['path_photo'].split("/");
        int _idFileName = _splitFileName.length - 1;

        isEditJenisDocument = _documentTypeSelected.docTypeId != _listFoto2Flag4[index]['document_type'] || _listFoto2Flag4[index]['edit_document_type'] == "1";
        // isEditDateTime = _initialDate != DateTime.parse(_listFoto2Flag4[index]['date_receive']) || _listFoto2Flag4[index]['edit_date_receive'] == "1";
        isEditFileDocumentUnitObject = _fileDocumentUnitObject['file_name'] != _splitFileName[_idFileName] || _listFoto2Flag4[index]['edit_path_photo'] == "1";

      }

      if (isEditJenisDocument || isEditDateTime || isEditFileDocumentUnitObject){
        isEdit = true;
      }else{
        isEdit = false;
      }
    }

  }

   void deleteFile() async{
     final file =  File("$globalPath/${this._fileDocumentUnitObjectTemp['file_name']}");
     await file.delete();
   }

  void getListMeta(String id) async{
    try{
      List _data = await getListMetadata(id);
      _symbolName = _data[0]['symbolName'];
    }
    catch(e){
      print(e);
    }
  }
}
