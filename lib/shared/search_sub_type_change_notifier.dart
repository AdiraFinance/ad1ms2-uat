import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/company_model.dart';
import 'package:ad1ms2_dev/models/form_m_pekerjaan_model.dart';
import 'package:ad1ms2_dev/models/sub_type_model.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_object_unit_change_notifier.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/form_m_foto_change_notif.dart';
import 'package:ad1ms2_dev/shared/list_oid_change_notifier.dart';
import 'package:ad1ms2_dev/shared/resource/get_company_type.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/io_client.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:ad1ms2_dev/shared/form_m_informasi_alamat_change_notif.dart';
import 'form_m_company_alamat_change_notif.dart';

class SearchSubTypeChangeNotifier with ChangeNotifier {
    bool _showClear = false;
    TextEditingController _controllerSearch = TextEditingController();
    bool _loadData = false;
    GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

    List<SubTypeModel> _listSubType = [];
    List<SubTypeModel> _listSubTypeTemp = [];

    TextEditingController get controllerSearch => _controllerSearch;

    bool get showClear => _showClear;

    set showClear(bool value) {
        this._showClear = value;
        notifyListeners();
    }

    void changeAction(String value) {
        if (value != "") {
            showClear = true;
        } else {
            showClear = false;
        }
    }

    UnmodifiableListView<SubTypeModel> get listSubTypeModel {
        return UnmodifiableListView(this._listSubType);
    }

    UnmodifiableListView<SubTypeModel> get listSubTypeTemp {
        return UnmodifiableListView(this._listSubTypeTemp);
    }

    bool get loadData => _loadData;

    set loadData(bool value) {
        this._loadData = value;
    }

    GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

    void getSubTypeModel(BuildContext context, String parentCode) async{
        this._listSubType.clear();
        loadData = true;

        try{
            final ioc = new HttpClient();
            ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
            final _http = IOClient(ioc);
            var _body = jsonEncode({
                "P_INSR_MAP_PROD_TYPE_ID": parentCode
            });
            print("body sub type = $_body");

            var storage = FlutterSecureStorage();
            String _subType = await storage.read(key: "SubType");
            final _response = await _http.post(
                "${BaseUrl.urlGeneral}$_subType",
                // "${urlPublic}api/asuransi/get-insurance-company",
                body: _body,
                headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
            );
            if(_response.statusCode == 200){
                var _result = jsonDecode(_response.body);
                var _data = _result['data'];
                if(_data.isNotEmpty){
                    for(int i=0 ; i<_data.length; i++) {
                        _listSubType.add(SubTypeModel(_data[i]['KODE'].toString(), _data[i]['DESKRIPSI'].toString().trim()));
                    }
                } else {
                    showSnackBar("Data sub type tidak ditemukan");
                }
                this._loadData = false;
            } else {
                showSnackBar("Error response status ${_response.statusCode}");
                this._loadData = false;
            }
        }
        catch(e){
            loadData = false;
            showSnackBar(e.toString());
        }
        notifyListeners();
    }

    void showSnackBar(String text){
        this._scaffoldKey.currentState.showSnackBar(new SnackBar(
            content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor));
    }

    void searchSubType(String query) {
        if(query.length < 3) {
            showSnackBar("Input minimal 3 karakter");
        } else {
            _listSubTypeTemp.clear();
            if (query.isEmpty) {
                return;
            }

            _listSubType.forEach((data) {
                if (data.KODE.contains(query) || data.DESKRIPSI.contains(query)) {
                    _listSubTypeTemp.add(data);
                }
            });
        }
        notifyListeners();
    }

    void clearSubTypeTemp() {
        _listSubTypeTemp.clear();
    }
}
