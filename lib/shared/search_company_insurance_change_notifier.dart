import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/company_model.dart';
import 'package:ad1ms2_dev/models/form_m_pekerjaan_model.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_collatelar_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_object_unit_change_notifier.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/form_m_foto_change_notif.dart';
import 'package:ad1ms2_dev/shared/list_oid_change_notifier.dart';
import 'package:ad1ms2_dev/shared/resource/get_company_type.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/io_client.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:ad1ms2_dev/shared/form_m_informasi_alamat_change_notif.dart';
import 'form_m_company_alamat_change_notif.dart';

class SearchCompanyInsuranceChangeNotifier with ChangeNotifier {
    bool _showClear = false;
    TextEditingController _controllerSearch = TextEditingController();
    bool _loadData = false;
    GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

    List<CompanyTypeInsuranceModel> _listCompanyInsurance = [];
    List<CompanyTypeInsuranceModel> _listCompanyInsuranceTemp = [];

    TextEditingController get controllerSearch => _controllerSearch;

    bool get showClear => _showClear;

    set showClear(bool value) {
        this._showClear = value;
        notifyListeners();
    }

    void changeAction(String value) {
        if (value != "") {
            showClear = true;
        } else {
            showClear = false;
        }
    }

    UnmodifiableListView<CompanyTypeInsuranceModel> get listCompanyInsuranceModel {
        return UnmodifiableListView(this._listCompanyInsurance);
    }

    UnmodifiableListView<CompanyTypeInsuranceModel> get listCompanyInsuranceTemp {
        return UnmodifiableListView(this._listCompanyInsuranceTemp);
    }

    bool get loadData => _loadData;

    set loadData(bool value) {
        this._loadData = value;
    }

    GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

    void getCompanyTypeInsurance(BuildContext context, String flag) async{
        this._listCompanyInsurance.clear();
        loadData = true;
        SharedPreferences _preference = await SharedPreferences.getInstance();
        var _providerRincianFoto = Provider.of<FormMFotoChangeNotifier>(context, listen: false);
        var _providerInfoObjectUnit = Provider.of<InformationObjectUnitChangeNotifier>(context, listen: false);
        var _providerColla = Provider.of<InformationCollateralChangeNotifier>(context, listen: false);

        try{
            final ioc = new HttpClient();
            ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
            final _http = IOClient(ioc);
            var _body = jsonEncode({
                "P_INSR_LEVEL": "$flag",
                "P_PARA_CP_SENTRA": _preference.getString("SentraD"),
                "P_PARA_FIN_TYPE": _preference.getString("cust_type") == "COM" ? _providerInfoObjectUnit.typeOfFinancingModelSelected.financingTypeId : _providerRincianFoto.typeOfFinancingModelSelected.financingTypeId,
                "P_PARA_OBJECT": flag == "1" ? _providerInfoObjectUnit.objectSelected != null ? _providerInfoObjectUnit.objectSelected.id : "" :  _providerColla.objectSelected != null ? _providerColla.objectSelected.id : "",
                "P_PARA_OBJECT_BRAND": flag == "1" ? _providerInfoObjectUnit.brandObjectSelected != null ? _providerInfoObjectUnit.brandObjectSelected.id : "" :  _providerColla.brandObjectSelected != null ? _providerColla.brandObjectSelected.id : "",
                "P_PARA_OBJECT_GENRE": flag == "1" ? _providerInfoObjectUnit.objectUsageModel != null ? _providerInfoObjectUnit.objectUsageModel.id : "" :  _providerColla.objectUsageSelected != null ? _providerColla.objectUsageSelected.id : "",
                "P_PARA_OBJECT_TYPE": flag == "1" ? _providerInfoObjectUnit.objectTypeSelected != null ? _providerInfoObjectUnit.objectTypeSelected.id : "" :  _providerColla.objectTypeSelected != null ? _providerColla.objectTypeSelected.id : "",
                "P_PARA_OUTLET": _providerInfoObjectUnit.sourceOrderNameSelected != null ? _providerInfoObjectUnit.sourceOrderNameSelected.kode : ""
            });
            print("body company = $_body");

            var storage = FlutterSecureStorage();
            String _fieldPerusahaan = await storage.read(key: "FieldPerusahaan");
            final _response = await _http.post(
                "${BaseUrl.urlGeneral}$_fieldPerusahaan",
                // "${urlPublic}api/asuransi/get-insurance-company",
                body: _body,
                headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
            );
            var _result = jsonDecode(_response.body);
            print("result com $_result");
            if(_response.statusCode == 200){
                var _data = _result['data'];
                if(_data.isNotEmpty){
                    for(int i=0 ; i<_data.length; i++) {
                        _listCompanyInsurance.add(CompanyTypeInsuranceModel(_data[i]['PARENT_KODE'], _data[i]['KODE'].toString(), _data[i]['DESKRIPSI'].toString().trim()));
                    }
                } else {
                    showSnackBar("Data perusahaan tidak ditemukan");
                }
                this._loadData = false;
            } else {
                showSnackBar("Error response status ${_response.statusCode}");
                this._loadData = false;
            }
        }
        catch(e){
            loadData = false;
            showSnackBar(e.toString());
        }
        notifyListeners();
    }

    void showSnackBar(String text){
        this._scaffoldKey.currentState.showSnackBar(new SnackBar(
            content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor));
    }

    void searchCompanyTypeInsurance(String query) {
        if(query.length < 3) {
            showSnackBar("Input minimal 3 karakter");
        } else {
            _listCompanyInsuranceTemp.clear();
            if (query.isEmpty) {
                return;
            }

            _listCompanyInsurance.forEach((dataCompanyTypeInsurance) {
                if (dataCompanyTypeInsurance.KODE.contains(query) || dataCompanyTypeInsurance.DESKRIPSI.contains(query)) {
                    _listCompanyInsuranceTemp.add(dataCompanyTypeInsurance);
                }
            });
        }
        notifyListeners();
    }

    void clearSearchInsuranceTemp() {
        _listCompanyInsuranceTemp.clear();
    }
}
