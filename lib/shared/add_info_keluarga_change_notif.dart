import 'dart:collection';

import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/models/birth_place_model.dart';
import 'package:ad1ms2_dev/models/form_m_info_keluarga_model.dart';
import 'package:ad1ms2_dev/models/form_m_informasi_nasabah_model.dart';
import 'package:ad1ms2_dev/models/showhide_mandatory_visibility/set_add_info_keluarga.dart';
import 'package:ad1ms2_dev/screens/form_m/form_m_informasi_keluarga.dart';
import 'package:ad1ms2_dev/screens/search_birth_place.dart';
import 'package:ad1ms2_dev/shared/form_m_info_keluarga_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_info_nasabah_change_notif.dart';
import 'package:ad1ms2_dev/shared/search_birth_place_change_notifier.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'constants.dart';
import 'dashboard/dashboard_change_notif.dart';
import 'date_picker.dart';

class AddInfoKeluargaChangeNotif with ChangeNotifier {
  GlobalKey<FormState> _key = GlobalKey<FormState>();
  List<RelationshipStatusModel> _listRelationShipStatus = [];
  List<RelationshipStatusModel> _listRelationShipStatusTemp = [];
  bool _autoValidate = false;
  RelationshipStatusModel _relationshipStatusSelected;
  RelationshipStatusModel _relationshipStatusSelectedTemp;
  IdentityModel _identitySelected;
  IdentityModel _identitySelectedTemp;
  TextEditingController _controllerNoIdentitas = TextEditingController();
  TextEditingController _controllerNamaLengkapIdentitas = TextEditingController();
  TextEditingController _controllerNamaLengkap = TextEditingController();
  TextEditingController _controllerTglLahir = TextEditingController();
  TextEditingController _controllerTempatLahirSesuaiIdentitas = TextEditingController();
  TextEditingController _controllerTempatLahirSesuaiIdentitasLOV = TextEditingController();
  TextEditingController _controllerKodeArea = TextEditingController();
  TextEditingController _controllerTlpn = TextEditingController();
  TextEditingController _controllerNoHp = TextEditingController();
  String _radioValueGender = "01";
  String _noIdentitas;
  String _namaLengkapIdentitas;
  String _namaLengkap;
  String _birthdate;
  String _birthPlace;
  String _birthPlaceLOV;
  String _areaCode;
  String _phoneNumber;
  String _cellPhoneNumber;
  String _radioValueGenderTemp = "01";
  String _familyInfoID;
  BirthPlaceModel _birthPlaceSelected;
  bool _disableJenisPenawaran = false;

  bool _isRelationshipStatusChanges = false;
  bool _isIdentityChanges = false;
  bool _isNoIdentitasChanges = false;
  bool _isNamaLengkapIdentitasChanges = false;
  bool _isNamaLengkapChanges = false;
  bool _isTglLahirChanges = false;
  bool _isTempatLahirSesuaiIdentitasLOVChanges = false;
  bool _isTempatLahirSesuaiIdentitasChanges = false;
  bool _isGenderChanges = false;
  bool _isKodeAreaChanges = false;
  bool _isTlpnChanges = false;
  bool _isNoHpChanges = false;

  bool get isRelationshipStatusChanges => _isRelationshipStatusChanges;
  bool get isIdentityChanges => _isIdentityChanges;
  bool get isNoIdentitasChanges => _isNoIdentitasChanges;
  bool get isNamaLengkapIdentitasChanges => _isNamaLengkapIdentitasChanges;
  bool get isNamaLengkapChanges => _isNamaLengkapChanges;
  bool get isTglLahirChanges => _isTglLahirChanges;
  bool get isTempatLahirSesuaiIdentitasLOVChanges => _isTempatLahirSesuaiIdentitasLOVChanges;
  bool get isTempatLahirSesuaiIdentitasChanges => _isTempatLahirSesuaiIdentitasChanges;
  bool get isGenderChanges => _isGenderChanges;
  bool get isKodeAreaChanges => _isKodeAreaChanges;
  bool get isTlpnChanges => _isTlpnChanges;
  bool get isNoHpChanges => _isNoHpChanges;

  String get familyInfoID => _familyInfoID;

  set familyInfoID(String value) {
    this._familyInfoID = value;
    notifyListeners();
  }

  set isRelationshipStatusChanges(bool value) {
    this._isRelationshipStatusChanges = value;
    notifyListeners();
  }
  set isIdentityChanges(bool value) {
    this._isIdentityChanges = value;
    notifyListeners();
  }
  set isNoIdentitasChanges(bool value) {
    this._isNoIdentitasChanges = value;
    notifyListeners();
  }
  set isNamaLengkapIdentitasChanges(bool value) {
    this._isNamaLengkapIdentitasChanges = value;
    notifyListeners();
  }
  set isNamaLengkapChanges(bool value) {
    this._isNamaLengkapChanges = value;
    notifyListeners();
  }
  set isTglLahirChanges(bool value) {
    this._isTglLahirChanges = value;
    notifyListeners();
  }
  set isTempatLahirSesuaiIdentitasLOVChanges(bool value) {
    this._isTempatLahirSesuaiIdentitasLOVChanges = value;
    notifyListeners();
  }
  set isTempatLahirSesuaiIdentitasChanges(bool value) {
    this._isTempatLahirSesuaiIdentitasChanges = value;
    notifyListeners();
  }
  set isGenderChanges(bool value) {
    this._isGenderChanges = value;
    notifyListeners();
  }
  set isKodeAreaChanges(bool value) {
    this._isKodeAreaChanges = value;
    notifyListeners();
  }
  set isTlpnChanges(bool value) {
    this._isTlpnChanges = value;
    notifyListeners();
  }
  set isNoHpChanges(bool value) {
    this._isNoHpChanges = value;
    notifyListeners();
  }



  SetAddInfoKeluargaModel _setAddInfoKeluarga;

  List<IdentityModel> _listIdentityType = IdentityType().lisIdentityModel;
//  [
//    IdentityModel("01", "KTP"),
//    IdentityModel("03", "PASSPORT"),
//    IdentityModel("04", "SIM"),
//    IdentityModel("05", "KTP Sementara"),
//    IdentityModel("06", "Resi KTP"),
//    IdentityModel("07", "Ket. Domisili"),
//  ];

  DateTime _initialDateForTglLahir =
  DateTime(dateNow.year, dateNow.month, dateNow.day);

  bool get autoValidate => _autoValidate;

  set autoValidate(bool value) {
    this._autoValidate = value;
    notifyListeners();
  }

  String get radioValueGender => _radioValueGender;

  set radioValueGender(String value) {
    this._radioValueGender = value;
    notifyListeners();
  }

  RelationshipStatusModel get relationshipStatusSelected =>
      _relationshipStatusSelected;

  set relationshipStatusSelected(RelationshipStatusModel value) {
    this._relationshipStatusSelected = value;
    if(this._relationshipStatusSelected.PARA_FAMILY_TYPE_ID == "01") {
      radioValueGender = "01";
    } else if(this._relationshipStatusSelected.PARA_FAMILY_TYPE_ID == "02") {
      radioValueGender = "02";
    } else if(this._relationshipStatusSelected.PARA_FAMILY_TYPE_ID == "04") {
      radioValueGender = "01";
    }
    notifyListeners();
  }

  IdentityModel get identitySelected => _identitySelected;

  set identitySelected(IdentityModel value) {
    this._identitySelected = value;
    this._controllerNoIdentitas.clear();
    notifyListeners();
  }

  TextEditingController get controllerNoIdentitas => _controllerNoIdentitas;

  TextEditingController get controllerTempatLahirSesuaiIdentitasLOV =>
      _controllerTempatLahirSesuaiIdentitasLOV;

  UnmodifiableListView<RelationshipStatusModel> get listRelationShipStatus {
    return UnmodifiableListView(this._listRelationShipStatus);
  }

  UnmodifiableListView<RelationshipStatusModel> get listRelationShipStatusTemp {
    return UnmodifiableListView(this._listRelationShipStatusTemp);
  }

  UnmodifiableListView<IdentityModel> get listIdentityType {
    return UnmodifiableListView(this._listIdentityType);
  }

  void selectBirthDate(BuildContext context) async {
    // DatePickerShared _datePickerShared = DatePickerShared();
    // var _datePickerSelected = await _datePickerShared.selectStartDate(
    //     context, this._initialDateForTglLahir,
    //     canAccessNextDay: false);
    // if (_datePickerSelected != null) {
    //   this._controllerTglLahir.text = dateFormat.format(_datePickerSelected);
    //   this._initialDateForTglLahir = _datePickerSelected;
    //   notifyListeners();
    // } else {
    //   return;
    // }
    var _datePickerSelected = await selectDateLast10Year(context, _initialDateForTglLahir);
    if (_datePickerSelected != null) {
      this._controllerTglLahir.text = dateFormat.format(_datePickerSelected);
      this._initialDateForTglLahir = _datePickerSelected;
      notifyListeners();
    } else {
      return;
    }
  }

  void checkValidCOdeArea(String value) {
    if (value == "08") {
      Future.delayed(Duration(milliseconds: 100), () {
        this._controllerKodeArea.clear();
      });
    } else {
      return;
    }
  }

  void checkValidNotZero(String value) {
    if (value == "08") {
      Future.delayed(Duration(milliseconds: 100), () {
        this._controllerNoHp.clear();
      });
    } else {
      return;
    }
  }

  void check(BuildContext context, int flag, int index) {
    print("cek tanggal = ${this._initialDateForTglLahir.toString()}");
    final _form = _key.currentState;
    if (flag == 0) {
      if (_form.validate()) {
        Provider.of<FormMInfoKeluargaChangeNotif>(context, listen: false)
            .addListInfoKel(FormMInfoKelModel(
          this._identitySelected,
          "NEW",
          this._relationshipStatusSelected,
          this._controllerNoIdentitas.text,
          this.controllerNamaLengkapIdentitas.text,
          this._controllerNamaLengkap.text,
          this._controllerTglLahir.text.isEmpty ? null : this._initialDateForTglLahir.toString(),
          this._radioValueGender,
          this._controllerKodeArea.text,
          this._controllerTlpn.text,
          this.controllerTempatLahirSesuaiIdentitas.text,
          this._controllerNoHp.text,
          this._birthPlaceSelected,
          this._isIdentityChanges,
          this._isRelationshipStatusChanges,
          this._isNoIdentitasChanges,
          this._isNamaLengkapIdentitasChanges,
          this._isNamaLengkapChanges,
          this._isTglLahirChanges,
          this._isGenderChanges,
          this._isKodeAreaChanges,
          this._isTlpnChanges,
          this._isTempatLahirSesuaiIdentitasChanges,
          this._isNoHpChanges,
          this._isTempatLahirSesuaiIdentitasLOVChanges,

        ));
        Navigator.pop(context);
      } else {
        autoValidate = true;
      }
    } else {
      if (_form.validate()) {
        Provider.of<FormMInfoKeluargaChangeNotif>(context, listen: false)
            .updateListInfoKel(
            FormMInfoKelModel(
              this._identitySelected,
              this._familyInfoID,
              this._relationshipStatusSelected,
              this._controllerNoIdentitas.text,
              this.controllerNamaLengkapIdentitas.text,
              this._controllerNamaLengkap.text,
              this._controllerTglLahir.text.isEmpty ? null : this._initialDateForTglLahir.toString(),
              this._radioValueGender,
              this._controllerKodeArea.text,
              this._controllerTlpn.text,
              this.controllerTempatLahirSesuaiIdentitas.text,
              this._controllerNoHp.text,
              this._birthPlaceSelected,
              this._isIdentityChanges,
              this._isRelationshipStatusChanges,
              this._isNoIdentitasChanges,
              this._isNamaLengkapIdentitasChanges,
              this._isNamaLengkapChanges,
              this._isTglLahirChanges,
              this._isGenderChanges,
              this._isKodeAreaChanges,
              this._isTlpnChanges,
              this._isTempatLahirSesuaiIdentitasChanges,
              this._isNoHpChanges,
              this._isTempatLahirSesuaiIdentitasLOVChanges,
            ),
            context,
            index);
      } else {
        autoValidate = true;
      }
    }
  }

  TextEditingController get controllerNamaLengkapIdentitas => _controllerNamaLengkapIdentitas;

  TextEditingController get controllerNamaLengkap => _controllerNamaLengkap;

  TextEditingController get controllerTglLahir => _controllerTglLahir;

  TextEditingController get controllerTempatLahirSesuaiIdentitas => _controllerTempatLahirSesuaiIdentitas;

  TextEditingController get controllerKodeArea => _controllerKodeArea;

  TextEditingController get controllerTlpn => _controllerTlpn;

  TextEditingController get controllerNoHp => _controllerNoHp;

  GlobalKey<FormState> get key => _key;

  RelationshipStatusModel get relationshipStatusSelectedTemp => _relationshipStatusSelectedTemp;

  IdentityModel get identitySelectedTemp => _identitySelectedTemp;

  String get noIdentitas => _noIdentitas;

  String get birthPlaceLOV => _birthPlaceLOV;

  get namaLengkapIdentitas => _namaLengkapIdentitas;

  get namaLengkap => _namaLengkap;

  get birthdate => _birthdate;

  get birthPlace => _birthPlace;

  get areCode => _areaCode;

  get phoneNumber => _phoneNumber;

  get cellPhoneNumber => _cellPhoneNumber;

  String get radioValueGenderTemp => _radioValueGenderTemp;

  Future<void> setValueForEditInfoKeluarga(BuildContext context, FormMInfoKelModel data,int flag, int index) async {
    relationshipFamily(context, flag);
    if(flag == 1){
      if(data.relationshipStatusModel != null){
        for (int i = 0; i < this._listRelationShipStatus.length; i++) {
          if (data.relationshipStatusModel.PARA_FAMILY_TYPE_ID == this._listRelationShipStatus[i].PARA_FAMILY_TYPE_ID) {
            this._relationshipStatusSelected = _listRelationShipStatus[i];
            this._relationshipStatusSelectedTemp = _listRelationShipStatus[i];
          }
        }
      }
      if(data.identityModel != null){
        for (int j = 0; j < this._listIdentityType.length; j++) {
          if (data.identityModel.id == _listIdentityType[j].id) {
            this._identitySelected = this._listIdentityType[j];
            this._identitySelectedTemp = this._listIdentityType[j];
          }
        }
      }
      this._familyInfoID = data.familyInfoID;
      this._controllerNoIdentitas.text = data.noIdentitas;
      this._noIdentitas = this._controllerNoIdentitas.text;
      this._controllerNamaLengkapIdentitas.text = data.namaLengkapSesuaiIdentitas;
      this._namaLengkapIdentitas = this._controllerNamaLengkapIdentitas.text;
      this._controllerNamaLengkap.text = data.namaLengkap;
      this._namaLengkap = this._controllerNamaLengkap.text;
      if(data.birthDate != null) {
        this._initialDateForTglLahir = DateTime.parse(data.birthDate);
        this._controllerTglLahir.text = dateFormat.format(this._initialDateForTglLahir);
        this._birthdate = this._controllerTglLahir.text;
      }
      this._controllerTempatLahirSesuaiIdentitas.text = data.tmptLahirSesuaiIdentitas;
      if(data.birthPlaceModel != null){
        this._birthPlaceSelected = data.birthPlaceModel;
        this._controllerTempatLahirSesuaiIdentitasLOV.text = "${data.birthPlaceModel.KABKOT_ID} -${data.birthPlaceModel.KABKOT_NAME}";
        this._birthPlaceLOV = this._controllerTempatLahirSesuaiIdentitasLOV.text;
      }
      this._birthPlace = this._controllerTempatLahirSesuaiIdentitas.text;
      this._radioValueGender = data.gender;
      this._radioValueGenderTemp = this._radioValueGender;
      this._controllerKodeArea.text = data.kodeArea;
      this._areaCode = this._controllerKodeArea.text;
      this._controllerTlpn.text = data.noTlpn;
      this._phoneNumber = this._controllerTlpn.text;
      this._controllerNoHp.text = data.noHp;
      this._cellPhoneNumber = this._controllerNoHp.text;
      checkDataDakor(data, context, index);
    }
  }

  DbHelper _dbHelper = DbHelper();
  void checkDataDakor(FormMInfoKelModel data, BuildContext context, int index) async{
    var _check = await _dbHelper.selectDataInfoKeluarga("");
    if(_check.isNotEmpty && _lastKnownState == "DKR"){
      _isRelationshipStatusChanges = _isRelationshipStatusChanges = this._relationshipStatusSelected.PARA_FAMILY_TYPE_ID != _check[index]['relation_stauts'] || _check[index]['edit_relation_stauts'] == '1';
      _isIdentityChanges = this._identitySelected.id != _check[index]['id_type'] || _check[index]['edit_id_type'] == '1';
      _isNoIdentitasChanges = this._noIdentitas.toString() != _check[index]['id_no'] || _check[index]['edit_id_no'] == '1';
      _isNamaLengkapIdentitasChanges = this._controllerNamaLengkapIdentitas.toString() != _check[index]['full_name_id'] || _check[index]['edit_full_name_id'] == '1';
      _isNamaLengkapChanges = this._controllerNamaLengkap.toString() != _check[index]['full_name'] || _check[index]['edit_full_name'] == '1';
      _isTglLahirChanges= this._controllerTglLahir.toString() != _check[index]['date_of_birth'] || _check[index]['edit_date_of_birth'] == '1';
      _isTempatLahirSesuaiIdentitasLOVChanges = this._controllerTempatLahirSesuaiIdentitasLOV.toString() != _check[index]['place_of_birth_kabkota'] || _check[index]['edit_place_of_birth_kabkota'] == '1';
      _isTempatLahirSesuaiIdentitasChanges = this._controllerTempatLahirSesuaiIdentitas.toString() != _check[index]['place_of_birth'] || _check[index]['edit_place_of_birth'] == '1';
      _isGenderChanges = this._radioValueGender.toString() != _check[index]['gender'] || _check[index]['edit_gender'] == '1';
      _isKodeAreaChanges = this._controllerKodeArea.toString() != _check[index]['phone1_area'] || _check[index]['edit_phone1_area'] == '1';
      _isTlpnChanges = this._controllerTlpn.toString() != _check[index]['phone1'] || _check[index]['edit_phone1'] == '1';
      _isNoHpChanges = this._controllerNoHp.toString() != _check[index]['handphone_no'] || _check[index]['edit_handphone_no'] == '1';
    }
    notifyListeners();
  }

  void searchBirthPlace(BuildContext context) async{
    BirthPlaceModel data = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => SearchBirthPlaceChangeNotifier(),
                child: SearchBirthPlace())));
    if (data != null) {
      this._birthPlaceSelected = data;
      this._controllerTempatLahirSesuaiIdentitasLOV.text = "${data.KABKOT_ID} - ${data.KABKOT_NAME}";
      notifyListeners();
    } else {
      return;
    }
  }

  void relationshipFamily(BuildContext context, int flag) {
    var _providerListKeluarga = Provider.of<FormMInfoKeluargaChangeNotif>(context, listen: false);
    this._relationshipStatusSelected = null;
    this._listRelationShipStatus.clear();

    // menghilangkan ibu
    for(int i = 0; i < RelationshipStatusList().relationshipStatusItems.length; i++) {
      if(RelationshipStatusList().relationshipStatusItems[i].PARA_FAMILY_TYPE_ID != "05") {
        _listRelationShipStatusTemp.add(RelationshipStatusList().relationshipStatusItems[i]);
      }
    }

    // menghilangan suami, istri apabila belum kawin
    for(int j = 0; j < _listRelationShipStatusTemp.length; j++) {
      if(Provider.of<FormMInfoNasabahChangeNotif>(context, listen: false).maritalStatusSelected.id == "01") {
        // kawin
        if(Provider.of<FormMInfoNasabahChangeNotif>(context, listen: false).radioValueGender == "01") {
          // Laki-laki
          if(_listRelationShipStatusTemp[j].PARA_FAMILY_TYPE_ID != "01") {
            _listRelationShipStatus.add(_listRelationShipStatusTemp[j]);
          }
        } else {
          // Perempuan
          if(_listRelationShipStatusTemp[j].PARA_FAMILY_TYPE_ID != "02") {
            _listRelationShipStatus.add(_listRelationShipStatusTemp[j]);
          }
        }
      } else if(Provider.of<FormMInfoNasabahChangeNotif>(context, listen: false).maritalStatusSelected.id == "04") {
        // duda/janda dengan anak
        if(_listRelationShipStatusTemp[j].PARA_FAMILY_TYPE_ID != "01" && _listRelationShipStatusTemp[j].PARA_FAMILY_TYPE_ID != "02") {
          _listRelationShipStatus.add(_listRelationShipStatusTemp[j]);
        }
      } else {
        // single
        if(_listRelationShipStatusTemp[j].PARA_FAMILY_TYPE_ID != "01" && _listRelationShipStatusTemp[j].PARA_FAMILY_TYPE_ID != "02" && _listRelationShipStatusTemp[j].PARA_FAMILY_TYPE_ID != "03") {
          _listRelationShipStatus.add(_listRelationShipStatusTemp[j]);
        }
      }
    }

    if(flag == 0){
      //menghilangkan ayah kandung
      for(int i=0; i<_providerListKeluarga.listFormInfoKel.length; i++){
        for(int j=0; j<this._listRelationShipStatus.length; j++){
          if(_providerListKeluarga.listFormInfoKel[i].relationshipStatusModel.PARA_FAMILY_TYPE_ID == this._listRelationShipStatus[j].PARA_FAMILY_TYPE_ID){
            this._listRelationShipStatus.removeAt(j);
          }
        }
      }
    }

    print('Total Items ${_listRelationShipStatus.length}');
  }

  bool isEmptyAll(){
    if(this._relationshipStatusSelected == null && this._identitySelected == null
        && this._controllerNoIdentitas.text == "" && this._controllerNamaLengkapIdentitas.text == ""
        && this._controllerNamaLengkap.text == "" && this._controllerTglLahir.text == ""
        && this._controllerTempatLahirSesuaiIdentitas.text == "" && this._controllerTempatLahirSesuaiIdentitasLOV.text == ""
        && this._controllerKodeArea.text == "" && this._controllerTlpn.text == "" && this._controllerNoHp.text == ""
    ){
      return true;
    }
    else{
      return false;
    }
  }

  String _custType;
  String _lastKnownState;

  String get custType => _custType;

  set custType(String value) {
    this._custType = value;
    notifyListeners();
  }

  String get lastKnownState => _lastKnownState;

  set lastKnownState(String value) {
    this._lastKnownState = value;
    notifyListeners();
  }

  bool get disableJenisPenawaran => _disableJenisPenawaran;

  set disableJenisPenawaran(bool value) {
    this._disableJenisPenawaran = value;
  }

  void setPreference() async {
    SharedPreferences _preference = await SharedPreferences.getInstance();
    this._custType = _preference.getString("cust_type");
    this._lastKnownState = _preference.getString("last_known_state");
    this._disableJenisPenawaran = false;
    if(_preference.getString("jenis_penawaran") == "002"){
      this._disableJenisPenawaran = true;
    }
  }

  void getDataFromDashboard(BuildContext context){
    _setAddInfoKeluarga = Provider.of<DashboardChangeNotif>(context, listen: false).setAddInfoKeluarga;
  }

  bool isRelationshipStatusVisible() => _setAddInfoKeluarga.isRelationshipStatusVisible;
  bool isIdentityVisible() => _setAddInfoKeluarga.isIdentityVisible;
  bool isNoIdentitasVisible() => _setAddInfoKeluarga.isNoIdentitasVisible;
  bool isNamaLengkapIdentitasVisible() => _setAddInfoKeluarga.isNamaLengkapIdentitasVisible;
  bool isNamaLengkapVisible() => _setAddInfoKeluarga.isNamaLengkapVisible;
  bool isTglLahirVisible() => _setAddInfoKeluarga.isTglLahirVisible;
  bool isTempatLahirSesuaiIdentitasLOVVisible() => _setAddInfoKeluarga.isTempatLahirSesuaiIdentitasLOVVisible;
  bool isTempatLahirSesuaiIdentitasVisible() => _setAddInfoKeluarga.isTempatLahirSesuaiIdentitasVisible;
  bool isGenderVisible() => _setAddInfoKeluarga.isGenderVisible;
  bool isKodeAreaVisible() => _setAddInfoKeluarga.isKodeAreaVisible;
  bool isTlpnVisible() => _setAddInfoKeluarga.isTlpnVisible;
  bool isNoHpVisible() => _setAddInfoKeluarga.isNoHpVisible;

  bool isRelationshipStatusMandatory() => _setAddInfoKeluarga.isRelationshipStatusMandatory;
  bool isIdentityMandatory() => _setAddInfoKeluarga.isIdentityMandatory;
  bool isNoIdentitasMandatory() => _setAddInfoKeluarga.isNoIdentitasMandatory;
  bool isNamaLengkapIdentitasMandatory() => _setAddInfoKeluarga.isNamaLengkapIdentitasMandatory;
  bool isNamaLengkapMandatory() => _setAddInfoKeluarga.isNamaLengkapMandatory;
  bool isTglLahirMandatory() => _setAddInfoKeluarga.isTglLahirMandatory;
  bool isTempatLahirSesuaiIdentitasLOVMandatory() => _setAddInfoKeluarga.isTempatLahirSesuaiIdentitasLOVMandatory;
  bool isTempatLahirSesuaiIdentitasMandatory() => _setAddInfoKeluarga.isTempatLahirSesuaiIdentitasMandatory;
  bool isGenderMandatory() => _setAddInfoKeluarga.isGenderMandatory;
  bool isKodeAreaMandatory() => _setAddInfoKeluarga.isKodeAreaMandatory;
  bool isTlpnMandatory() => _setAddInfoKeluarga.isTlpnMandatory;
  bool isNoHpMandatory() => _setAddInfoKeluarga.isNoHpMandatory;
}
