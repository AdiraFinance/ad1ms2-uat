import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/screens/dashboard.dart';
import 'package:ad1ms2_dev/screens/form_m/menu_detail_loan.dart';
import 'package:ad1ms2_dev/screens/form_m/menu_object_information.dart';
import 'package:ad1ms2_dev/screens/form_m_company/menu_detail_company.dart';
import 'package:ad1ms2_dev/screens/form_m_company/menu_management_pic.dart';
import 'package:ad1ms2_dev/screens/form_m_company/menu_pemegang_saham.dart';
import 'package:ad1ms2_dev/screens/survey/result_survey.dart';
import 'package:ad1ms2_dev/shared/add_pemegang_saham_lembaga_change_notif.dart';
import 'package:ad1ms2_dev/shared/add_pemegang_saham_pribadi_change_notif.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/form_m_add_additional_insurance_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/form_m_add_major_insurance_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_additional_insurance_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_app_document_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_credit_income_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_credit_structure_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_credit_structure_type_installment_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_credit_subsidy_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_major_insurance_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_company_alamat_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_company_manajemen_pic_alamat_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_company_pendapatan_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_company_rincian_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_guarantor_change_notifier.dart';
import 'package:ad1ms2_dev/shared/pemegang_saham_pribadi_change_notifier.dart';
import 'package:ad1ms2_dev/shared/resource/get_list_meta_data.dart';
import 'package:ad1ms2_dev/shared/resource/old_applicationdto_provider.dart';
import 'package:ad1ms2_dev/shared/resource/submit_data_partial.dart';
import 'package:ad1ms2_dev/shared/survey/result_survey_change_notifier.dart';
import 'package:dio/adapter.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/io_client.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../main.dart';
import 'pemegang_saham_lembaga_change_notif.dart';
import 'change_notifier_app/info_application_change_notifier.dart';
import 'change_notifier_app/info_object_karoseri_change_notifier.dart';
import 'change_notifier_app/information_collatelar_change_notifier.dart';
import 'change_notifier_app/information_object_unit_change_notifier.dart';
import 'change_notifier_app/information_salesman_change_notifier.dart';
import 'change_notifier_app/marketing_notes_change_notifier.dart';
import 'change_notifier_app/taksasi_unit_change_notifier.dart';
import 'constants.dart';
import 'form_m_company_manajemen_pic_change_notif.dart';
import 'form_m_informasi_alamat_change_notif.dart';
import 'info_wmp_change_notifier.dart';
import 'package:dio/dio.dart' as dio;
import 'package:http/http.dart' as http;

import 'survey/list_survey_photo_change_notifier.dart';

class FormMParentCompanyChangeNotifier with ChangeNotifier {
    List<GlobalKey<FormState>> formKeys = [
        GlobalKey<FormState>(),
        GlobalKey<FormState>(),
        GlobalKey<FormState>(),
        GlobalKey<FormState>(),
        GlobalKey<FormState>(),
        GlobalKey<FormState>(),
        GlobalKey<FormState>(),
        GlobalKey<FormState>(),
        GlobalKey<FormState>(),
        GlobalKey<FormState>(),
        GlobalKey<FormState>(),
        GlobalKey<FormState>(),
        GlobalKey<FormState>(),
        GlobalKey<FormState>(),
        GlobalKey<FormState>(),
        GlobalKey<FormState>(),
        GlobalKey<FormState>(),
        GlobalKey<FormState>(),
        GlobalKey<FormState>(),
        GlobalKey<FormState>(),
        GlobalKey<FormState>(),
        GlobalKey<FormState>(),
        GlobalKey<FormState>(),
        GlobalKey<FormState>(),
        GlobalKey<FormState>(),
        GlobalKey<FormState>(),
    ];

    var storage = FlutterSecureStorage();
    GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
    SubmitDataPartial _submitDataPartial = SubmitDataPartial();
    DbHelper _dbHelper = DbHelper();
    var _orderSupportingDocuments = [];
    int _lastStepIndex = 0;
    int _selectedIndex = 0;
    bool _isMenuCompany = false;
//    bool _isRincian = false;
//    bool _isAlamat = false;
    bool _isPendapatan = false;
    bool _isPenjamin = false;
    bool _isMenuManagementPIC = false;
//    bool _isManajemenPIC = false;
//    bool _isManajemenPICAlamat = false;
    bool _isMenuPemegangSaham = false;
//    bool _isPemegangSahamPribadi = false;
//    bool _isPemegangSahamPribadiAlamat = false;
//    bool _isPemegangSahamKelembagaan = false;
//    bool _isPemegangSahamKelembagaanAlamat = false;
    bool _isInfoAppDone = false;
    bool _isMenuObjectInformationDone = false;
    bool _isInfoObjKaroseriDone = false;
    bool _isMenuDetailLoanDone = false;
//    bool _isUnitObjectInfoDone = false;
//    bool _isSalesmanInfoDone = false;
//    bool _isCollateralInfoDone = false;
//    bool _isCreditStructureDone = false;
//    bool _isCreditStructureTypeInstallmentDone = false;
//    bool _isMajorInsuranceDone = false;
//    bool _isAdditionalInsuranceDone = false;
//    bool _isInfoWMPDone = false;
//    bool _isCreditIncomeDone = false;
    bool _isCreditSubsidyDone = false;
    bool _isDocumentInfoDone = false;
    bool _isTaksasiUnitDone = false;
    bool _isMarketingNotesDone = false;
    String _urlPublic;
    String _messageProcess;
    String _urlUploadFile;
    List _customerIncomes = [];
    bool _loadData = false;

    bool get loadData => _loadData;

    set loadData(bool value) {
        this._loadData = value;
        notifyListeners();
    }

    List get customerIncomes => _customerIncomes;

    String get messageProcess => _messageProcess;

    set messageProcess(String value) {
        this._messageProcess = value;
    }

    bool get isMenuDetailLoanDone => _isMenuDetailLoanDone;

    set isMenuDetailLoanDone(bool value) {
      this._isMenuDetailLoanDone = value;
      notifyListeners();
    }
    bool get isMarketingNotesDone => _isMarketingNotesDone;

    set isMarketingNotesDone(bool value) {
        this._isMarketingNotesDone = value;
        notifyListeners();
    }

    bool get isMenuObjectInformationDone => _isMenuObjectInformationDone;

    set isMenuObjectInformationDone(bool value) {
        this._isMenuObjectInformationDone = value;
        notifyListeners();
    }

    int get selectedIndex => _selectedIndex;

    set selectedIndex(int value) {
        this._selectedIndex = value;
        notifyListeners();
    }

    bool get isMenuCompany => _isMenuCompany;

    set isMenuCompany(bool value) {
        this._isMenuCompany = value;
        notifyListeners();
    }

//    bool get isRincian => _isRincian;
//
//    set isRincian(bool value) {
//        this._isRincian = value;
//        notifyListeners();
//    }
//
//    bool get isAlamat => _isAlamat;
//
//    set isAlamat(bool value) {
//        this._isAlamat = value;
//        notifyListeners();
//    }

    bool get isPendapatan => _isPendapatan;

    set isPendapatan(bool value) {
        this._isPendapatan = value;
        notifyListeners();
    }

    bool get isPenjamin => _isPenjamin;

    set isPenjamin(bool value) {
        this._isPenjamin = value;
        notifyListeners();
    }

    bool get isMenuManagementPIC => _isMenuManagementPIC;

    set isMenuManagementPIC(bool value) {
        this._isMenuManagementPIC = value;
        notifyListeners();
    }

    bool get isMenuPemegangSaham => _isMenuPemegangSaham;

    set isMenuPemegangSaham(bool value) {
        this._isMenuPemegangSaham = value;
        notifyListeners();
    }

//    bool get isManajemenPIC => _isManajemenPIC;
//
//    set isManajemenPIC(bool value) {
//        this._isManajemenPIC = value;
//        notifyListeners();
//    }
//
//    bool get isManajemenPICAlamat => _isManajemenPICAlamat;
//
//    set isManajemenPICAlamat(bool value) {
//        this._isManajemenPICAlamat = value;
//        notifyListeners();
//    }

//    bool get isPemegangSahamPribadi => _isPemegangSahamPribadi;
//
//    set isPemegangSahamPribadi(bool value) {
//        this._isPemegangSahamPribadi = value;
//        notifyListeners();
//    }
//
//    bool get isPemegangSahamPribadiAlamat => _isPemegangSahamPribadiAlamat;
//
//    set isPemegangSahamPribadiAlamat(bool value) {
//        this._isPemegangSahamPribadiAlamat = value;
//        notifyListeners();
//    }
//
//    bool get isPemegangSahamKelembagaan => _isPemegangSahamKelembagaan;
//
//    set isPemegangSahamKelembagaan(bool value) {
//        this._isPemegangSahamKelembagaan = value;
//        notifyListeners();
//    }
//
//    bool get isPemegangSahamKelembagaanAlamat => _isPemegangSahamKelembagaanAlamat;
//
//    set isPemegangSahamKelembagaanAlamat(bool value) {
//        this._isPemegangSahamKelembagaanAlamat = value;
//        notifyListeners();
//    }

    bool get isInfoAppDone => _isInfoAppDone;

    set isInfoAppDone(bool value) {
        this._isInfoAppDone = value;
        notifyListeners();
    }

//    bool get isUnitObjectInfoDone => _isUnitObjectInfoDone;
//
//    set isUnitObjectInfoDone(bool value) {
//        this._isUnitObjectInfoDone = value;
//        notifyListeners();
//    }
//
//    bool get isSalesmanInfoDone => _isSalesmanInfoDone;
//
//    set isSalesmanInfoDone(bool value) {
//        this._isSalesmanInfoDone = value;
//        notifyListeners();
//    }
//
//    bool get isCollateralInfoDone => _isCollateralInfoDone;
//
//    set isCollateralInfoDone(bool value) {
//        this._isCollateralInfoDone = value;
//        notifyListeners();
//    }

    bool get isInfoObjKaroseriDone => _isInfoObjKaroseriDone;

    set isInfoObjKaroseriDone(bool value) {
        this._isInfoObjKaroseriDone = value;
        notifyListeners();
    }

//    bool get isMajorInsuranceDone => _isMajorInsuranceDone;
//
//    set isMajorInsuranceDone(bool value) {
//        this._isMajorInsuranceDone = value;
//        notifyListeners();
//    }
//
//    bool get isAdditionalInsuranceDone => _isAdditionalInsuranceDone;
//
//    set isAdditionalInsuranceDone(bool value) {
//        this._isAdditionalInsuranceDone = value;
//        notifyListeners();
//    }


    bool get isDocumentInfoDone => _isDocumentInfoDone;

    set isDocumentInfoDone(bool value) {
        this._isDocumentInfoDone = value;
        notifyListeners();
    }

    get orderSupportingDocuments => _orderSupportingDocuments;

    set orderSupportingDocuments(value) {
        this._orderSupportingDocuments = value;
        notifyListeners();
    }

    GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

    void checkBtnNext(BuildContext context) async{
        final _form = formKeys[this._selectedIndex].currentState;
        SharedPreferences _preferences = await SharedPreferences.getInstance();
        var _providerFormMCompanyRincianChangeNotif = Provider.of<FormMCompanyRincianChangeNotifier>(context, listen: false);
        var _providerFormMCompanyAlamatChangeNotif = Provider.of<FormMCompanyAlamatChangeNotifier>(context, listen: false);
        var _providerFormMCompanyPendapatanChangeNotif = Provider.of<FormMCompanyPendapatanChangeNotifier>(context, listen: false);
        var _providerFormMCompanyPenjaminChangeNotif = Provider.of<FormMGuarantorChangeNotifier>(context, listen: false);
        var _providerFormMCompanyManajemenPICChangeNotif = Provider.of<FormMCompanyManajemenPICChangeNotifier>(context, listen: false);
        var _providerFormMCompanyManajemenPICAlamatChangeNotif = Provider.of<FormMCompanyManajemenPICAlamatChangeNotifier>(context, listen: false);
        var _providerFormMCompanyPemegangSahamPribadiChangeNotif = Provider.of<PemegangSahamPribadiChangeNotifier>(context, listen: false);
        var _providerFormMCompanyPemegangSahamKelembagaanChangeNotif = Provider.of<PemegangSahamLembagaChangeNotifier>(context, listen: false);

        var _providerInfoApp = Provider.of<InfoAppChangeNotifier>(context, listen: false);
        var _providerInfoObjectUnit = Provider.of<InformationObjectUnitChangeNotifier>(context, listen: false);
        var _providerInfoSales = Provider.of<InformationSalesmanChangeNotifier>(context, listen: false);
        var _providerKolateral = Provider.of<InformationCollateralChangeNotifier>(context, listen: false);
        var _providerKaroseri = Provider.of<InfoObjectKaroseriChangeNotifier>(context, listen: false);
        var _providerInfoCreditStructure = Provider.of<InfoCreditStructureChangeNotifier>(context, listen: false);
        var _providerMajorInsurance = Provider.of<InfoMajorInsuranceChangeNotifier>(context, listen: true);
        var _providerAddMajorInsurance = Provider.of<FormMAddMajorInsuranceChangeNotifier>(context, listen: false);
        var _providerAdditionalInsurance = Provider.of<InfoAdditionalInsuranceChangeNotifier>(context, listen: true);
        var _providerAddAdditionalInsurance = Provider.of<FormMAddAdditionalInsuranceChangeNotifier>(context, listen: false);
        var _providerWMP = Provider.of<InfoWMPChangeNotifier>(context, listen: false);
        var _providerCreditIncome = Provider.of<InfoCreditIncomeChangeNotifier>(context, listen: false);
        var _providerCreditSubsidy = Provider.of<InfoCreditSubsidyChangeNotifier>(context, listen: false);
        var _providerInfoDocument = Provider.of<InfoDocumentChangeNotifier>(context, listen: false);
        var _providerTaksasiUnit = Provider.of<TaksasiUnitChangeNotifier>(context, listen: false);
        var _providerMarketingNotes = Provider.of<MarketingNotesChangeNotifier>(context,listen: false);

        // selectedIndex+=1;
        if(this._selectedIndex == 0){
            if(!_providerFormMCompanyRincianChangeNotif.flag){
                _providerFormMCompanyRincianChangeNotif.autoValidate = true;
            }
            if(!_providerFormMCompanyAlamatChangeNotif.flag){
                _providerFormMCompanyAlamatChangeNotif.autoValidate = true;
            }

            if(_providerFormMCompanyRincianChangeNotif.flag && _providerFormMCompanyAlamatChangeNotif.flag){
                await _providerFormMCompanyRincianChangeNotif.checkDataDakor();
                if(await _providerFormMCompanyRincianChangeNotif.deleteSQLite() &&  await _providerFormMCompanyAlamatChangeNotif.deleteSQLite("10")){
                    await _setLastStep(this._selectedIndex);
                    selectedIndex+=1;
                    isMenuCompany = true;
                    _providerFormMCompanyRincianChangeNotif.saveToSQLite(context);
                    _providerFormMCompanyAlamatChangeNotif.saveToSQLite("10");
                    _providerFormMCompanyManajemenPICChangeNotif.saveToSQLite();
                }
                // if(_preferences.getString("last_known_state") != "DKR"){
                    try{
                        loadData = true;
                        await _submitDataPartial.submitCustomerCompany(context, 0);
                        loadData = false;
                    }
                    catch(e){
                        loadData = false;
                        dialogFailedSavePartial(context, e.toString());
                    }
                // }
            }
        }
        else if (this._selectedIndex == 1) {
            // var _msgMinusLabaKotor = "";
            // var _msgMinusLabaBersihSebelumPajak = "";
            // var _msgMinusLabaBersihSetelahPajak = "";
            // // minus laba kotor
            // if(double.parse(_providerFormMCompanyPendapatanChangeNotif.controllerGrossProfit.text.replaceAll(",", "")) < 0) {
            //     _providerFormMCompanyPendapatanChangeNotif.controllerCostOfRevenue.clear();
            //     _msgMinusLabaKotor = "- Laba Kotor tidak boleh minus.";
            // }
            // // minus laba bersih sebelum pajak
            // if(double.parse(_providerFormMCompanyPendapatanChangeNotif.controllerNetProfitBeforeTax.text.replaceAll(",", "")) < 0) {
            //     _providerFormMCompanyPendapatanChangeNotif.controllerOperatingCosts.clear();
            //     _providerFormMCompanyPendapatanChangeNotif.controllerOtherCosts.clear();
            //     _msgMinusLabaBersihSebelumPajak = "- Laba Bersih Sebelum Pajak tidak boleh minus.";
            // }
            // // minus laba bersih setelah pajak
            // if(double.parse(_providerFormMCompanyPendapatanChangeNotif.controllerNetProfitAfterTax.text.replaceAll(",", "")) < 0) {
            //     _providerFormMCompanyPendapatanChangeNotif.controllerTax.clear();
            //     _msgMinusLabaBersihSetelahPajak = "- Laba Bersih Setelah Pajak tidak boleh minus.";
            // }
            //
            // if(double.parse(_providerFormMCompanyPendapatanChangeNotif.controllerGrossProfit.text.replaceAll(",", "")) < 0 ||
            //     double.parse(_providerFormMCompanyPendapatanChangeNotif.controllerNetProfitBeforeTax.text.replaceAll(",", "")) < 0 ||
            //     double.parse(_providerFormMCompanyPendapatanChangeNotif.controllerNetProfitAfterTax.text.replaceAll(",", "")) < 0) {
            //     print("masuk if");
            //     if(double.parse(_providerFormMCompanyPendapatanChangeNotif.controllerGrossProfit.text.replaceAll(",", "")) < 0) {
            //         _providerFormMCompanyPendapatanChangeNotif.controllerGrossProfit.clear();
            //     }
            //     if(double.parse(_providerFormMCompanyPendapatanChangeNotif.controllerNetProfitBeforeTax.text.replaceAll(",", "")) < 0) {
            //         _providerFormMCompanyPendapatanChangeNotif.controllerNetProfitBeforeTax.clear();
            //     }
            //     if(double.parse(_providerFormMCompanyPendapatanChangeNotif.controllerNetProfitAfterTax.text.replaceAll(",", "")) < 0) {
            //         _providerFormMCompanyPendapatanChangeNotif.controllerNetProfitAfterTax.clear();
            //     }
            //     showDialog(
            //         context: context,
            //         barrierDismissible: true,
            //         builder: (BuildContext context){
            //             return Theme(
            //                 data: ThemeData(
            //                     fontFamily: "NunitoSans",
            //                     primaryColor: Colors.black,
            //                     primarySwatch: primaryOrange,
            //                     accentColor: myPrimaryColor
            //                 ),
            //                 child: AlertDialog(
            //                     title: Text("Informasi", style: TextStyle(fontWeight: FontWeight.bold)),
            //                     content: Column(
            //                         crossAxisAlignment: CrossAxisAlignment.start,
            //                         mainAxisSize: MainAxisSize.min,
            //                         children: <Widget>[
            //                             Visibility(
            //                                 visible: _msgMinusLabaKotor != "" ? true : false,
            //                                 child: Text("$_msgMinusLabaKotor",)
            //                             ),
            //                             Visibility(
            //                                 visible: _msgMinusLabaKotor != "" ? true : false,
            //                                 child: SizedBox(height: MediaQuery.of(context).size.height / 117)
            //                             ),
            //                             Visibility(
            //                                 visible: _msgMinusLabaBersihSebelumPajak != "" ? true : false,
            //                                 child: Text("$_msgMinusLabaBersihSebelumPajak",)
            //                             ),
            //                             Visibility(
            //                                 visible: _msgMinusLabaBersihSebelumPajak != "" ? true : false,
            //                                 child: SizedBox(height: MediaQuery.of(context).size.height / 117)
            //                             ),
            //                             Visibility(
            //                                 visible: _msgMinusLabaBersihSetelahPajak != "" ? true : false,
            //                                 child: Text("$_msgMinusLabaBersihSetelahPajak",)
            //                             ),
            //                         ],
            //                     ),
            //                     actions: <Widget>[
            //                         new FlatButton(
            //                             onPressed: () {
            //                                 Navigator.of(context).pop(true);
            //                             },
            //                             child: new Text('Close'),
            //                         ),
            //                     ],
            //                 ),
            //             );
            //         }
            //     );
            //     // _providerFormMCompanyPendapatanChangeNotif.incomeNotMinus = false;
            // }
            // else if(double.parse(_providerFormMCompanyPendapatanChangeNotif.controllerGrossProfit.text.replaceAll(",", "")) > 0 &&
            //     double.parse(_providerFormMCompanyPendapatanChangeNotif.controllerNetProfitBeforeTax.text.replaceAll(",", "")) > 0 &&
            //     double.parse(_providerFormMCompanyPendapatanChangeNotif.controllerNetProfitAfterTax.text.replaceAll(",", "")) > 0) {
            //     print("masuk else if");
            //     // _providerFormMCompanyPendapatanChangeNotif.incomeNotMinus = true;
            // }
            if (_form.validate()) {
                await _providerFormMCompanyPendapatanChangeNotif.checkDataDakor();
                if(await _providerFormMCompanyPendapatanChangeNotif.deleteSQLite()){
                    _providerFormMCompanyPendapatanChangeNotif.saveToSQLite(context);
                    await _setLastStep(this._selectedIndex);
                    selectedIndex += 1;
                    isPendapatan = true;
                    // _providerFormMCompanyPenjaminChangeNotif.radioValueIsWithGuarantor = 0;
                    // _providerFormMCompanyPenjaminChangeNotif.setDataFromSQLite();
                }
                // if(_preferences.getString("last_known_state") != "DKR"){
                    try{
                        loadData = true;
                        await _submitDataPartial.submitCustomerCompany(context, 0);
                        await _submitDataPartial.submitIncome(context, 1,true);
                        loadData = false;
                    }
                    catch(e){
                        loadData = false;
                        dialogFailedSavePartial(context, e.toString());
                    }
                // }
            } else {
                _providerFormMCompanyPendapatanChangeNotif.autoValidate = true;
            }
        }
        else if (this._selectedIndex == 2) {
            if(_providerFormMCompanyPenjaminChangeNotif.radioValueIsWithGuarantor == 0){
                if(_providerFormMCompanyPenjaminChangeNotif.listGuarantorIndividual.isNotEmpty || _providerFormMCompanyPenjaminChangeNotif.listGuarantorCompany.isNotEmpty){
                    if(await _providerFormMCompanyPenjaminChangeNotif.deleteSQLite()){
                        // await _provider.saveToSQLite();
                        await _providerFormMCompanyPenjaminChangeNotif.saveToSQLite(context);
                        await _setLastStep(this._selectedIndex);
                        selectedIndex += 1;
                        isPenjamin = true;
                    }
                    // if(_preferences.getString("last_known_state") != "DKR"){
                        try{
                            loadData = true;
                            await _submitDataPartial.submitCustomerCompany(context, 0);
                            await _submitDataPartial.submitIncome(context, 1,true);
                            if(_providerFormMCompanyPenjaminChangeNotif.radioValueIsWithGuarantor == 0) await _submitDataPartial.submitGuarantor(context,2);
                            loadData = false;
                        }
                        catch(e){
                            loadData = false;
                            dialogFailedSavePartial(context, e.toString());
                        }
                    // }
                }
                else{
                    _providerFormMCompanyPenjaminChangeNotif.autoValidate = true;
                }
            }
            else{
                await _setLastStep(this._selectedIndex);
                selectedIndex += 1;
                isPenjamin = true;
            }
        }
        else if(this._selectedIndex == 3){
            if(!_providerFormMCompanyManajemenPICChangeNotif.flag){
                _providerFormMCompanyManajemenPICChangeNotif.autoValidate = true;
            }
            if(!_providerFormMCompanyManajemenPICAlamatChangeNotif.flag){
                _providerFormMCompanyManajemenPICAlamatChangeNotif.autoValidate = true;
            }
            if(_providerFormMCompanyManajemenPICChangeNotif.flag && _providerFormMCompanyManajemenPICAlamatChangeNotif.flag){
                if( await _providerFormMCompanyManajemenPICAlamatChangeNotif.deleteSQLite("9")){
                    _providerFormMCompanyManajemenPICChangeNotif.saveToSQLite();
                    _providerFormMCompanyManajemenPICAlamatChangeNotif.saveToSQLite("9");
                    await _setLastStep(this._selectedIndex);
                    selectedIndex+=1;
                    isMenuManagementPIC = true;
                }
                // if(_preferences.getString("last_known_state") != "DKR"){
                    try{
                        loadData = true;
                        await _submitDataPartial.submitCustomerCompany(context, 0);
                        await _submitDataPartial.submitIncome(context, 1,true);
                        if(_providerFormMCompanyPenjaminChangeNotif.listGuarantorIndividual.isNotEmpty || _providerFormMCompanyPenjaminChangeNotif.listGuarantorCompany.isNotEmpty) await _submitDataPartial.submitGuarantor(context,2);
                        await _submitDataPartial.submitManagementPIC(3,context);
                        loadData = false;
                    }
                    catch(e){
                        loadData = false;
                        dialogFailedSavePartial(context, e.toString());
                    }
                // }
            }
        }
        else if(this._selectedIndex == 4){

            // Validasi pemegang saham pribadi
            var _providerAddShareHolderPer = Provider.of<AddPemegangSahamPribadiChangeNotifier>(context, listen: false);
            await _providerAddShareHolderPer.getDataFromDashboard(context);
            if (_providerFormMCompanyPemegangSahamPribadiChangeNotif.listPemegangSahamPribadi.isNotEmpty) {
                _providerFormMCompanyPemegangSahamPribadiChangeNotif.autoValidate = false;
                _providerFormMCompanyPemegangSahamPribadiChangeNotif.flag = true;
            } else {
              if(_providerAddShareHolderPer.checkIsMandatoryPemegangSahamPribadi()){
                  _providerFormMCompanyPemegangSahamPribadiChangeNotif.autoValidate = false;
                  _providerFormMCompanyPemegangSahamPribadiChangeNotif.flag = true;
              }else{
                  _providerFormMCompanyPemegangSahamPribadiChangeNotif.autoValidate = true;
                  _providerFormMCompanyPemegangSahamPribadiChangeNotif.flag = false;
              }
            }


            // Validasi pemegang saham lembaga
            var _providerAddShareHolderCom = Provider.of<AddPemegangSahamLembagaChangeNotif>(context, listen: false);
            await _providerAddShareHolderCom.getDataFromDashboard(context);
            if (_providerFormMCompanyPemegangSahamKelembagaanChangeNotif.listPemegangSahamLembaga.isNotEmpty) {
                _providerFormMCompanyPemegangSahamKelembagaanChangeNotif.autoValidate = false;
                _providerFormMCompanyPemegangSahamKelembagaanChangeNotif.flag = true;
            } else {
                if(_providerAddShareHolderCom.checkIsMandatoryPemegangSahamLembaga()){
                    _providerFormMCompanyPemegangSahamKelembagaanChangeNotif.autoValidate = false;
                    _providerFormMCompanyPemegangSahamKelembagaanChangeNotif.flag = true;
                }else{
                    _providerFormMCompanyPemegangSahamKelembagaanChangeNotif.autoValidate = true;
                    _providerFormMCompanyPemegangSahamKelembagaanChangeNotif.flag = false;
                }
            }

            // kodingan asli
            // if(!_providerFormMCompanyPemegangSahamPribadiChangeNotif.flag){
            //     _providerFormMCompanyPemegangSahamPribadiChangeNotif.autoValidate = true;
            // }
            if(!_providerFormMCompanyPemegangSahamKelembagaanChangeNotif.flag){
                _providerFormMCompanyPemegangSahamKelembagaanChangeNotif.autoValidate = true;
            }
            if(_providerFormMCompanyPemegangSahamPribadiChangeNotif.flag && _providerFormMCompanyPemegangSahamKelembagaanChangeNotif.flag){
                if(await _providerFormMCompanyPemegangSahamPribadiChangeNotif.deleteSQLite() && await _providerFormMCompanyPemegangSahamKelembagaanChangeNotif.deleteSQLite()){
                    _providerFormMCompanyPemegangSahamPribadiChangeNotif.saveToSQLite();
                    _providerFormMCompanyPemegangSahamKelembagaanChangeNotif.saveToSQLite();
                }
                await _setLastStep(this._selectedIndex);
                selectedIndex+=1;
                isMenuPemegangSaham = true;
                // if(_preferences.getString("last_known_state") != "DKR"){
                    try{
                        loadData = true;
                        await _submitDataPartial.submitCustomerCompany(context, 0);
                        await _submitDataPartial.submitIncome(context, 1,true);
                        if(_providerFormMCompanyPenjaminChangeNotif.listGuarantorIndividual.isNotEmpty || _providerFormMCompanyPenjaminChangeNotif.listGuarantorCompany.isNotEmpty) await _submitDataPartial.submitGuarantor(context,2);
                        await _submitDataPartial.submitManagementPIC(3,context);
                        if(_providerFormMCompanyPemegangSahamPribadiChangeNotif.listPemegangSahamPribadi.isNotEmpty || _providerFormMCompanyPemegangSahamKelembagaanChangeNotif.listPemegangSahamLembaga.isNotEmpty) await _submitDataPartial.submitShareHolder(4,context);
                        loadData = false;
                    }
                    catch(e){
                        loadData = false;
                        dialogFailedSavePartial(context, e.toString());
                    }
                // }
            }
        }
        else if(this._selectedIndex == 5){
            if (_form.validate()) {
                await _providerInfoApp.checkDataDakor();
                if(await _providerInfoApp.deleteSQLite()){
                    _providerInfoApp.saveToSQLite(context);
                }
                await _setLastStep(this._selectedIndex);
                selectedIndex += 1;
                isInfoAppDone = true;
                // if(_preferences.getString("last_known_state") != "DKR"){
                    try{
                        loadData = true;
                        await _submitDataPartial.submitCustomerCompany(context, 0);
                        await _submitDataPartial.submitIncome(context, 1,true);
                        if(_providerFormMCompanyPenjaminChangeNotif.listGuarantorIndividual.isNotEmpty || _providerFormMCompanyPenjaminChangeNotif.listGuarantorCompany.isNotEmpty) await _submitDataPartial.submitGuarantor(context,2);
                        await _submitDataPartial.submitManagementPIC(3,context);
                        if(_providerFormMCompanyPemegangSahamPribadiChangeNotif.listPemegangSahamPribadi.isNotEmpty || _providerFormMCompanyPemegangSahamKelembagaanChangeNotif.listPemegangSahamLembaga.isNotEmpty) await _submitDataPartial.submitShareHolder(4,context);
                        await _submitDataPartial.submitApplication(context,5);
                        loadData = false;
                    }
                    catch(e){
                        loadData = false;
                        dialogFailedSavePartial(context, e.toString());
                    }
                // }
            } else {
                _providerInfoApp.autoValidate = true;
            }
        }
        else if(this._selectedIndex == 6){
            // selectedIndex +=1;
            if(!_providerInfoObjectUnit.flag){
                _providerInfoObjectUnit.autoValidate = true;
            }
            if(_providerInfoSales.listInfoSales.length == 0){
                _providerInfoSales.autoValidate = true;
            }
            if(!_providerKolateral.flag){
                if(_providerKolateral.collateralTypeModel == null){
                    _providerKolateral.autoValidateAuto = true;
                    _providerKolateral.autoValidateProp = false;
                } else {
                    if(_providerKolateral.collateralTypeModel.id == "001"){
                        _providerKolateral.autoValidateAuto = true;
                        _providerKolateral.autoValidateProp = false;
                    } else if(_providerKolateral.collateralTypeModel.id == "002") {
                        _providerKolateral.autoValidateAuto = false;
                        _providerKolateral.autoValidateProp = true;
                    }
                }
            }
            // if(_providerInfoObjectUnit.flag && _providerInfoSales.listInfoSales.length != 0 && _providerKolateral.flag){
            if(await _providerInfoObjectUnit.deleteSQLite()){
                _providerInfoObjectUnit.saveToSQLite(context);
            }
            if(await _providerInfoSales.deleteSQLite()){
                _providerInfoSales.saveToSQLite(context);
            }
            if(_providerKolateral.collateralTypeModel.id == "001"){
                if(await _providerKolateral.deleteSQLiteCollaOto()){
                    _providerKolateral.saveToSQLiteOto(context);
                }
            }
            else if(_providerKolateral.collateralTypeModel.id == "002") {
                if(await _providerKolateral.deleteSQLiteCollaProp()){
                    _providerKolateral.saveToSQLiteProperti(context, "6");
                }
            }
            await _setLastStep(this._selectedIndex);
            // selectedIndex +=1;
            Provider.of<InfoObjectKaroseriChangeNotifier>(context, listen: true).isVisible ? selectedIndex +=1 : selectedIndex +=2;
            isMenuObjectInformationDone = true;
            // if(_preferences.getString("last_known_state") != "DKR"){
                try{
                    loadData = true;
                    await _submitDataPartial.submitCustomerCompany(context, 0);
                    await _submitDataPartial.submitIncome(context, 1,true);
                    if(_providerFormMCompanyPenjaminChangeNotif.listGuarantorIndividual.isNotEmpty || _providerFormMCompanyPenjaminChangeNotif.listGuarantorCompany.isNotEmpty) await _submitDataPartial.submitGuarantor(context,2);
                    await _submitDataPartial.submitManagementPIC(3,context);
                    if(_providerFormMCompanyPemegangSahamPribadiChangeNotif.listPemegangSahamPribadi.isNotEmpty || _providerFormMCompanyPemegangSahamKelembagaanChangeNotif.listPemegangSahamLembaga.isNotEmpty) await _submitDataPartial.submitShareHolder(4,context);
                    await _submitDataPartial.submitApplication(context,5);
                    await _submitDataPartial.submitColla(context,6);
                    loadData = false;
                }
                catch(e){
                    loadData = false;
                    dialogFailedSavePartial(context, e.toString());
                }
            // }
            // }
        }
        else if(this._selectedIndex == 7){
            if(await _providerKaroseri.deleteSQLite()) _providerKaroseri.saveToSQLite();
            await _setLastStep(this._selectedIndex);
            // List _step = await _dbHelper.selectLastStep();
            selectedIndex += 1;
            isInfoObjKaroseriDone = true;
            // if(_preferences.getString("last_known_state") != "DKR"){
                try{
                    loadData = true;
                    await _submitDataPartial.submitCustomerCompany(context, 0);
                    await _submitDataPartial.submitIncome(context, 1,true);
                    if(_providerFormMCompanyPenjaminChangeNotif.listGuarantorIndividual.isNotEmpty || _providerFormMCompanyPenjaminChangeNotif.listGuarantorCompany.isNotEmpty) await _submitDataPartial.submitGuarantor(context,2);
                    await _submitDataPartial.submitManagementPIC(3,context);
                    if(_providerFormMCompanyPemegangSahamPribadiChangeNotif.listPemegangSahamPribadi.isNotEmpty || _providerFormMCompanyPemegangSahamKelembagaanChangeNotif.listPemegangSahamLembaga.isNotEmpty) await _submitDataPartial.submitShareHolder(4,context);
                    await _submitDataPartial.submitApplication(context,5);
                    await _submitDataPartial.submitColla(context,6);
                    if(_providerKaroseri.listFormKaroseriObject.isNotEmpty) await _submitDataPartial.submitKaroseri(context,7);
                    loadData = false;
                }
                catch(e){
                    loadData = false;
                    dialogFailedSavePartial(context, e.toString());
                }
            // }
            // if(_providerKaroseri.listFormKaroseriObject.isNotEmpty){
            //     if(await _providerKaroseri.deleteSQLite()){
            //         _providerKaroseri.saveToSQLite();
            //     }
            //     selectedIndex += 1;
            //     isInfoObjKaroseriDone = true;
            // } else {
            //     _showSnackBar("Karoseri tidak boleh kosong");
            //     _providerKaroseri.autoValidate = true;
            // }
        }
        else if(this._selectedIndex == 8){
            if(!_providerInfoCreditStructure.flag){
                _providerInfoCreditStructure.autoValidate = true;
            }
            await _providerMajorInsurance.onBackPress();
            await _providerAdditionalInsurance.onBackPress();
            if(_preferences.getString("last_known_state") != "IDE") {
                if(_providerMajorInsurance.listFormMajorInsurance.isNotEmpty) {
                    for(int i=0; i<_providerMajorInsurance.listFormMajorInsurance.length; i++) {
                        if(_providerMajorInsurance.listFormMajorInsurance[i].insuranceType.KODE == "2") {
                            await _providerAddMajorInsurance.getBatasAtasBawahUtamaUpdated(context, _providerMajorInsurance.listFormMajorInsurance[i], i);
                        } else if(_providerMajorInsurance.listFormMajorInsurance[i].insuranceType.KODE == "3") {
                            await _providerAddMajorInsurance.getBatasAtasBawahPerluasanUpdated(context, _providerMajorInsurance.listFormMajorInsurance[i], i);
                        }
                    }
                }
                if(_providerAdditionalInsurance.listFormAdditionalInsurance.isNotEmpty) {
                    for(int i=0; i<_providerAdditionalInsurance.listFormAdditionalInsurance.length; i++) {
                        await _providerAddAdditionalInsurance.getLowerUpperLimitUpdated(context, _providerAdditionalInsurance.listFormAdditionalInsurance[i], i);
                    }
                }
            }
            // if(_providerMajorInsurance.listFormMajorInsurance.length == 0){
            //     _providerMajorInsurance.flag = true;
            // }
            // if(_providerAdditionalInsurance.listFormAdditionalInsurance.length == 0 && _providerInfoObjectUnit.groupObjectSelected.KODE == "002"){
            //     _providerAdditionalInsurance.flag = true;
            // }
            if(_providerInfoCreditStructure.flag && !_providerMajorInsurance.flag && !_providerAdditionalInsurance.flag){
                _providerInfoCreditStructure.updateMS2ApplObjectInfStrukturKredit();
                if(await _providerMajorInsurance.deleteSQLite()){
                    _providerMajorInsurance.saveToSQLite();
                }
                if(await _providerAdditionalInsurance.deleteSQLite()){
                    _providerAdditionalInsurance.saveToSQLite();
                }
                if(await _providerWMP.deleteSQLite()){
                    _providerWMP.saveToSQLite();
                }
                _providerCreditIncome.updateMS2ApplObjectInfStrukturKreditIncome();
                if(await _providerInfoCreditStructure.deleteSQLite()){
                    _providerInfoCreditStructure.saveToSQLiteInfStrukturKreditBiaya();
                }
                await _setLastStep(this._selectedIndex);
                selectedIndex += 1;
                isMenuDetailLoanDone = true;
                // if(_preferences.getString("last_known_state") != "DKR"){
                    try{
                        loadData = true;
                        await _submitDataPartial.submitCustomerCompany(context, 0);
                        await _submitDataPartial.submitIncome(context, 1,true);
                        if(_providerFormMCompanyPenjaminChangeNotif.listGuarantorIndividual.isNotEmpty || _providerFormMCompanyPenjaminChangeNotif.listGuarantorCompany.isNotEmpty) await _submitDataPartial.submitGuarantor(context,2);
                        await _submitDataPartial.submitManagementPIC(3,context);
                        if(_providerFormMCompanyPemegangSahamPribadiChangeNotif.listPemegangSahamPribadi.isNotEmpty || _providerFormMCompanyPemegangSahamKelembagaanChangeNotif.listPemegangSahamLembaga.isNotEmpty) await _submitDataPartial.submitShareHolder(4,context);
                        await _submitDataPartial.submitApplication(context,5);
                        await _submitDataPartial.submitColla(context,6);
                        if(_providerKaroseri.listFormKaroseriObject.isNotEmpty) await _submitDataPartial.submitKaroseri(context,7);
                        await _submitDataPartial.submitStructureCredit(8,context);
                        if(_providerMajorInsurance.listFormMajorInsurance.isNotEmpty
                            ||  _providerAdditionalInsurance.listFormAdditionalInsurance.isNotEmpty) await _submitDataPartial.submitInsurance(8,context);
                        if(_providerWMP.listWMP.isNotEmpty) await _submitDataPartial.submitWMP(8,context);
                        loadData = false;
                    }
                    catch(e){
                        loadData = false;
                        dialogFailedSavePartial(context, e.toString());
                    }
                // }
            }
        }
        else if(this._selectedIndex == 9){
            // if(_providerCreditSubsidy.listInfoCreditSubsidy.isNotEmpty){
                if(await _providerCreditSubsidy.deleteSQLIte()){
                    _providerCreditSubsidy.saveToSQLite();
                }
                await _setLastStep(this._selectedIndex);
                selectedIndex += 1;
                _isCreditSubsidyDone = true;
                // if(_preferences.getString("last_known_state") != "DKR"){
                    try{
                        loadData = true;
                        await _submitDataPartial.submitCustomerCompany(context, 0);
                        await _submitDataPartial.submitIncome(context, 1,true);
                        if(_providerFormMCompanyPenjaminChangeNotif.listGuarantorIndividual.isNotEmpty || _providerFormMCompanyPenjaminChangeNotif.listGuarantorCompany.isNotEmpty) await _submitDataPartial.submitGuarantor(context,2);
                        await _submitDataPartial.submitManagementPIC(3,context);
                        if(_providerFormMCompanyPemegangSahamPribadiChangeNotif.listPemegangSahamPribadi.isNotEmpty || _providerFormMCompanyPemegangSahamKelembagaanChangeNotif.listPemegangSahamLembaga.isNotEmpty) await _submitDataPartial.submitShareHolder(4,context);
                        await _submitDataPartial.submitApplication(context,5);
                        await _submitDataPartial.submitColla(context,6);
                        if(_providerKaroseri.listFormKaroseriObject.isNotEmpty) await _submitDataPartial.submitKaroseri(context,7);
                        await _submitDataPartial.submitStructureCredit(8,context);
                        if(_providerMajorInsurance.listFormMajorInsurance.isNotEmpty
                            ||  _providerAdditionalInsurance.listFormAdditionalInsurance.isNotEmpty) await _submitDataPartial.submitInsurance(8,context);
                        if(_providerWMP.listWMP.isNotEmpty) await _submitDataPartial.submitWMP(8,context);
                        if(_providerCreditSubsidy.listInfoCreditSubsidy.isNotEmpty) await _submitDataPartial.submitSubsidy(9,context);
                        loadData = false;
                    }
                    catch(e){
                        loadData = false;
                        dialogFailedSavePartial(context, e.toString());
                    }
                // }
            // } else {
            //     _showSnackBar("Info Kredit Subsidi tidak boleh kosong");
            // }
        }
        else if(this._selectedIndex == 10){
            if(_providerInfoDocument.listInfoDocument.isNotEmpty){
                if(await _providerInfoDocument.deleteSQLite()){
                    _providerInfoDocument.saveToSQLite(context);
                }
                await _setLastStep(this._selectedIndex);
                if(_providerTaksasiUnit.isVisible){
                    selectedIndex += 1;
                }
                else {
                    selectedIndex += 2;
                }
                _isDocumentInfoDone = true;
            } else {
                _showSnackBar("Info Dokumen tidak boleh kosong");
            }
        }
        else if(this._selectedIndex == 11){
            // if(_providerInfoObjectUnit.groupObjectSelected != null){
            if(_form.validate()){
                await _setLastStep(this._selectedIndex);
                selectedIndex += 1;
                isTaksasiUnitDone = true;
                if(await _providerTaksasiUnit.deleteSQLite()){
                    _providerTaksasiUnit.saveToSQLite(context);
                }
            }
            else{
                if(_providerKolateral.groupObjectSelected.KODE == "001"){
                    _providerTaksasiUnit.autoValidateMotor;
                }
                else if(_providerKolateral.groupObjectSelected.KODE == "002"){
                    _providerTaksasiUnit.autoValidateCar;
                }
            }
            // if(_providerInfoObjectUnit.groupObjectSelected.KODE == "001"){
            //
            //   _providerTaksasiUnit.autoValidateMotor;
            // }
            // else if(_providerInfoObjectUnit.groupObjectSelected.KODE == "002"){
            //   if(_form.validate()){
            //     selectedIndex += 1;
            //     isTaksasiUnitDone = true;
            //     _providerTaksasiUnit.saveToSQLite(context);
            //   }
            //   _providerTaksasiUnit.autoValidateCar;
            // }
            // }
        }
        else if(this._selectedIndex == 12){
            if(_form.validate()){
                await _setLastStep(this._selectedIndex);
                selectedIndex += 1;
                isTaksasiUnitDone = true;
            }
            else{
                _providerMarketingNotes.autoValidate = true;
            }
        }
        // _setLastStep(this._selectedIndex);
    }

    Future<void> _setLastStep(int index) async{
        if(this._lastStepIndex <= index){
            print(" masuk if last step parent company $index");
            if(await _dbHelper.deleteSaveLastStep()){
                _dbHelper.saveLastStep(this._selectedIndex);
            }
        }
    }

    //setup data from sqlite
    void _setupDataFromSQLite(BuildContext context) async{
        this._loadData = true;
        List _data = await _dbHelper.selectLastStep();
        this._lastStepIndex = int.parse(_data[0]['idx']);
        MenuDetailCompany _detailCompany = MenuDetailCompany();
        MenuManagementPIC _detailManagement = MenuManagementPIC();
        MenuPemegangSaham _detailSaham = MenuPemegangSaham();
        MenuObjectInformation _detailObject = MenuObjectInformation();
        MenuDetailLoanState _detailLoan = MenuDetailLoanState();
        ResultSurvey _survey = ResultSurvey();
        SharedPreferences _preferences = await SharedPreferences.getInstance();
        if(_preferences.getString("last_known_state") != "IDE" || _preferences.getString("status_aoro") != "0"){//yg lama "DKR"
            await _detailCompany.setNextState(context, null);
            await Provider.of<FormMCompanyPendapatanChangeNotifier>(context, listen: false).setDataFromSQLite(context, null);
            await Provider.of<FormMGuarantorChangeNotifier>(context, listen: false).setDataFromSQLite(context, null, 2);
            await _detailManagement.setNextState(context, null);
            await _detailSaham.setNextState(context, null);
            await Provider.of<InfoAppChangeNotifier>(context, listen: true).addNumberOfUnitList(context, null);
            await _detailObject.setNextState(context, null);
            await Provider.of<InfoObjectKaroseriChangeNotifier>(context, listen: false).setDataFromSQLite(context, null);
            await _detailLoan.setNextState(context, null);
            await Provider.of<InfoCreditSubsidyChangeNotifier>(context, listen: false).setDataFromSQLite(context, null);
            await Provider.of<InfoDocumentChangeNotifier>(context,listen: false).setDataFromSQLite(context, null);
            await Provider.of<TaksasiUnitChangeNotifier>(context,listen: false).getListTaksasi(context, null);
            await Provider.of<MarketingNotesChangeNotifier>(context,listen: false).setDataFromSQLite();
            await _survey.menuSetDataFromSQLite(context, _lastStepIndex);
            loadData = false;
            // _survey.menuSetDataFromSQLite(context);
        }
        else {
            List _data = await _dbHelper.selectLastStep();
            if (_data.isNotEmpty) {
                this._lastStepIndex = int.parse(_data[0]['idx']);
                if(_preferences.getString("cust_type") == "COM"){
                    await _detailCompany.setNextState(context, this._lastStepIndex);
                }
                this._loadData = false;
            }
            else {
                this._loadData = false;
            }
        }
        this._loadData = false;
        notifyListeners();
    }

    void checkTabDrawer(BuildContext context, int index) async{
        // this.selectedIndex = index;
        if(this._selectedIndex < index){
            for(int i=this._selectedIndex; i < index; i++){
                if(await isValidateTabDrawer(context, i)){
                    break;
                }
            }
        }
        else{
            this.selectedIndex = index;
        }
        notifyListeners();
    }

    void backBtn(BuildContext context) {
        if(selectedIndex != 0){
            if(this._selectedIndex == 8 && !Provider.of<InfoObjectKaroseriChangeNotifier>(context, listen: true).isVisible){
                this._selectedIndex -= 2;
            }
            else if(this._selectedIndex == 12 && !Provider.of<TaksasiUnitChangeNotifier>(context,listen: false).isVisible){
                this._selectedIndex -= 2;
            }
            else{
                this._selectedIndex -= 1;
            }
            notifyListeners();
        }
    }

    void _showSnackBar(String text){
        this._scaffoldKey.currentState.showSnackBar(new SnackBar(
            content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor));
    }

    void _showSnackBarSuccess(String text){
        this._scaffoldKey.currentState.showSnackBar(new SnackBar(
            content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: Colors.green, duration: Duration(seconds: 2)));
    }


    void dialogFailedSavePartial(BuildContext context, String message) {
        showDialog(
            context: context,
            barrierDismissible: false,
            builder: (BuildContext context){
                return Theme(
                    data: ThemeData(
                        fontFamily: "NunitoSans"
                    ),
                    child: AlertDialog(
                        title: Text("Informasi", style: TextStyle(fontWeight: FontWeight.bold)),
                        content: Text(message),
                        actions: <Widget>[
                            FlatButton(
                                onPressed: (){
                                    Navigator.pop(context);
                                    notifyListeners();
                                },
                                child: Text("OK",
                                    style: TextStyle(
                                        color: primaryOrange,
                                        fontSize: 14,
                                        fontWeight: FontWeight.w500,
                                        letterSpacing: 1.25
                                    )
                                )
                            )
                        ],
                    ),
                );
            }
        );
    }

//    bool get isCreditStructureDone => _isCreditStructureDone;
//
//    set isCreditStructureDone(bool value) {
//        this._isCreditStructureDone = value;
//        notifyListeners();
//    }

//    bool get isCreditStructureTypeInstallmentDone => _isCreditStructureTypeInstallmentDone;
//
//    set isCreditStructureTypeInstallmentDone(bool value) {
//        this._isCreditStructureTypeInstallmentDone = value;
//        notifyListeners();
//    }

    bool get isTaksasiUnitDone => _isTaksasiUnitDone;

    set isTaksasiUnitDone(bool value) {
        this._isTaksasiUnitDone = value;
        notifyListeners();
    }

    bool get isCreditSubsidyDone => _isCreditSubsidyDone;

    set isCreditSubsidyDone(bool value) {
        this._isCreditSubsidyDone = value;
        notifyListeners();
    }

    void clearData(BuildContext context) {
        debugPrint("GREEN");
        this._selectedIndex = 0;
        this._isMenuCompany = false;
        this._isPendapatan = false;
        this._isPenjamin = false;
        this._isMenuManagementPIC = false;
        this._isMenuPemegangSaham = false;

        // Form M Company
        Provider.of<FormMCompanyRincianChangeNotifier>(context, listen: false).clearDataRincian();
        Provider.of<FormMCompanyAlamatChangeNotifier>(context, listen: false).clearDataAlamat();
        Provider.of<FormMCompanyPendapatanChangeNotifier>(context, listen: false).clearDataPendapatan();
        Provider.of<FormMCompanyManajemenPICChangeNotifier>(context, listen: false).clearDataManajemenPIC();
        Provider.of<FormMCompanyManajemenPICAlamatChangeNotifier>(context, listen: false).clearDataManajemenPICAlamat();
        Provider.of<PemegangSahamPribadiChangeNotifier>(context, listen: false).clearDataPemegangSahamPribadi();
        Provider.of<PemegangSahamLembagaChangeNotifier>(context, listen: false).clearDataPemegangSahamLembaga();
        Provider.of<FormMGuarantorChangeNotifier>(context, listen: false).clearDataGuarantor();
        // Form Survey
        // Provider.of<ResultSurveyChangeNotifier>(context, listen: false).clearDataSurvey();
        // Form App
        // Provider.of<InformationObjectUnitChangeNotifier>(context, listen: false).clearDataInfoUnitObject();
        // Provider.of<InformationSalesmanChangeNotifier>(context, listen: false).clearDataInfoSales();
        // Provider.of<InformationCollateralChangeNotifier>(context, listen: false).clearDataInfoCollateral(); //diganti .clear()
        // Provider.of<InfoObjectKaroseriChangeNotifier>(context, listen: false).clearDataInfoObjectKaroseri();
        // Provider.of<InfoCreditStructureChangeNotifier>(context, listen: false).clearInfoCreditStructure();
        // Provider.of<InfoCreditStructureTypeInstallmentChangeNotifier>(context, listen: false).clearData();
        // Provider.of<InfoObjectKaroseriChangeNotifier>(context, listen: false).clearDataInfoObjectKaroseri();
        // Provider.of<InfoMajorInsuranceChangeNotifier>(context, listen: false).clearMajorInsurance();
        // Provider.of<InfoAdditionalInsuranceChangeNotifier>(context, listen: false).clearListAdditionalInsurance();
        // Provider.of<InfoCreditIncomeChangeNotifier>(context, listen: false).clearDataInfoCreditIncome();
        // Provider.of<InfoCreditSubsidyChangeNotifier>(context, listen: false).clearInfoCreditSubsidy();
        // Provider.of<InfoDocumentChangeNotifier>(context, listen: false).clearInfoDocument();
        // Provider.of<TaksasiUnitChangeNotifier>(context, listen: false).clearTaksasiUnit();
        // Provider.of<MarketingNotesChangeNotifier>(context, listen: false).clearMarketingNotes();
        Provider.of<InfoAppChangeNotifier>(context, listen: false).clearDataInfoApp();
        Provider.of<InformationObjectUnitChangeNotifier>(context, listen: false).clearDataInfoUnitObject();
        Provider.of<InformationSalesmanChangeNotifier>(context, listen: false).clearDataInfoSales();
        Provider.of<InformationCollateralChangeNotifier>(context, listen: false).clearDataInfoCollateral();
        Provider.of<InfoObjectKaroseriChangeNotifier>(context, listen: false).clearDataInfoObjectKaroseri();
        Provider.of<InfoCreditStructureChangeNotifier>(context, listen: false).clearInfoCreditStructure();
        Provider.of<InfoCreditStructureTypeInstallmentChangeNotifier>(context, listen: false).clearData();
        Provider.of<InfoMajorInsuranceChangeNotifier>(context, listen: false).clearMajorInsurance();
        Provider.of<InfoAdditionalInsuranceChangeNotifier>(context, listen: false).clearListAdditionalInsurance();
        Provider.of<InfoWMPChangeNotifier>(context, listen: false).clearDataWMP();
        Provider.of<InfoCreditIncomeChangeNotifier>(context, listen: false).clearDataInfoCreditIncome();
        Provider.of<InfoCreditSubsidyChangeNotifier>(context, listen: false).clearInfoCreditSubsidy();
        Provider.of<InfoDocumentChangeNotifier>(context, listen: false).clearInfoDocument();
        Provider.of<TaksasiUnitChangeNotifier>(context, listen: false).clearTaksasiUnit();
        Provider.of<MarketingNotesChangeNotifier>(context, listen: false).clearMarketingNotes();
        Provider.of<ListSurveyPhotoChangeNotifier>(context, listen: false).clearData();
        Provider.of<ResultSurveyChangeNotifier>(context, listen: false).clearDataSurvey();
        _setupDataFromSQLite(context);
    }

    Future<bool> isValidateTabDrawer(BuildContext context,int index) async {
        final _form = formKeys[index].currentState;
        var _providerFormMCompanyRincianChangeNotif = Provider.of<FormMCompanyRincianChangeNotifier>(context, listen: false);
        var _providerFormMCompanyAlamatChangeNotif = Provider.of<FormMCompanyAlamatChangeNotifier>(context, listen: false);
        var _providerFormMCompanyPendapatanChangeNotif = Provider.of<FormMCompanyPendapatanChangeNotifier>(context, listen: false);
        var _providerFormMCompanyPenjaminChangeNotif = Provider.of<FormMGuarantorChangeNotifier>(context, listen: false);
        var _providerFormMCompanyManajemenPICChangeNotif = Provider.of<FormMCompanyManajemenPICChangeNotifier>(context, listen: false);
        var _providerFormMCompanyManajemenPICAlamatChangeNotif = Provider.of<FormMCompanyManajemenPICAlamatChangeNotifier>(context, listen: false);
        var _providerFormMCompanyPemegangSahamPribadiChangeNotif = Provider.of<PemegangSahamPribadiChangeNotifier>(context, listen: false);
        var _providerFormMCompanyPemegangSahamKelembagaanChangeNotif = Provider.of<PemegangSahamLembagaChangeNotifier>(context, listen: false);
        var _providerInfoApp = Provider.of<InfoAppChangeNotifier>(context, listen: false);
        var _providerInfoObjectUnit = Provider.of<InformationObjectUnitChangeNotifier>(context, listen: false);
        var _providerInfoSales = Provider.of<InformationSalesmanChangeNotifier>(context, listen: false);
        var _providerKolateral = Provider.of<InformationCollateralChangeNotifier>(context, listen: false);
        var _providerKaroseri = Provider.of<InfoObjectKaroseriChangeNotifier>(context, listen: false);
        var _providerMajorInsurance = Provider.of<InfoMajorInsuranceChangeNotifier>(context, listen: true);
        var _providerAdditionalInsurance = Provider.of<InfoAdditionalInsuranceChangeNotifier>(context, listen: true);
        var _providerInfoCreditStructure = Provider.of<InfoCreditStructureChangeNotifier>(context, listen: false);
        var _providerCreditSubsidy = Provider.of<InfoCreditSubsidyChangeNotifier>(context, listen: false);
        var _providerWMP = Provider.of<InfoWMPChangeNotifier>(context, listen: false);
        var _providerInfoDocument = Provider.of<InfoDocumentChangeNotifier>(context, listen: false);
        var _providerTaksasiUnit = Provider.of<TaksasiUnitChangeNotifier>(context, listen: false);

        var _isValidate = false;


        if(index == 0){
            if(!_providerFormMCompanyRincianChangeNotif.flag){
                _providerFormMCompanyRincianChangeNotif.autoValidate = true;
            }
            if(!_providerFormMCompanyAlamatChangeNotif.flag){
                _providerFormMCompanyAlamatChangeNotif.autoValidate = true;
            }
            if(_providerFormMCompanyRincianChangeNotif.flag && _providerFormMCompanyAlamatChangeNotif.flag){
                if(await _providerFormMCompanyRincianChangeNotif.deleteSQLite() &&  await _providerFormMCompanyAlamatChangeNotif.deleteSQLite("10")){
                    selectedIndex+=1;
                    isMenuCompany = true;
                    _providerFormMCompanyRincianChangeNotif.saveToSQLite(context);
                    _providerFormMCompanyAlamatChangeNotif.saveToSQLite("10");
                }
                _isValidate = false;
            } else {
                _isValidate = true;
            }
        }
        else if (index == 1) {
            if(isPendapatan){
                if(await _providerFormMCompanyPendapatanChangeNotif.deleteSQLite()){
                    _providerFormMCompanyPendapatanChangeNotif.saveToSQLite(context);
                }
                selectedIndex += 1;
                isPendapatan = true;
                _isValidate = false;
            } else {
                _isValidate = true;
            }
        }
        else if (index == 2) {
            if(isPenjamin){
                if(await _providerFormMCompanyPenjaminChangeNotif.deleteSQLite()){
                    // _providerFormMCompanyPenjaminChangeNotif.saveToSQLite(1,2);
                    _providerFormMCompanyPenjaminChangeNotif.saveToSQLite(context);
                    // _providerFormMCompanyPenjaminChangeNotif.saveToSQLiteAddressGuarantor();
                    selectedIndex += 1;
                    isPenjamin = true;
                }
                _isValidate = false;
            } else {
                _isValidate = true;
            }
        }
        else if(index == 3){
            if(!_providerFormMCompanyManajemenPICChangeNotif.flag){
                _providerFormMCompanyManajemenPICChangeNotif.autoValidate = true;
            }
            if(!_providerFormMCompanyManajemenPICAlamatChangeNotif.flag){
                _providerFormMCompanyManajemenPICAlamatChangeNotif.autoValidate = true;
            }
            if(_providerFormMCompanyManajemenPICChangeNotif.flag && _providerFormMCompanyManajemenPICAlamatChangeNotif.flag){
                if(await _providerFormMCompanyManajemenPICAlamatChangeNotif.deleteSQLite("9")){
                    _providerFormMCompanyManajemenPICChangeNotif.saveToSQLite();
                    _providerFormMCompanyManajemenPICAlamatChangeNotif.saveToSQLite("9");
                    selectedIndex+=1;
                    isMenuManagementPIC = true;
                }
                _isValidate = false;
            } else {
                _isValidate = true;
            }
        }
        else if(index == 4){
            var _providerAddShareHolderPer = Provider.of<AddPemegangSahamPribadiChangeNotifier>(context, listen: false);
            await _providerAddShareHolderPer.getDataFromDashboard(context);
            if (_providerFormMCompanyPemegangSahamPribadiChangeNotif.listPemegangSahamPribadi.isNotEmpty) {
                _providerFormMCompanyPemegangSahamPribadiChangeNotif.autoValidate = false;
                _providerFormMCompanyPemegangSahamPribadiChangeNotif.flag = true;
            } else {
                if(_providerAddShareHolderPer.checkIsMandatoryPemegangSahamPribadi()){
                    _providerFormMCompanyPemegangSahamPribadiChangeNotif.autoValidate = false;
                    _providerFormMCompanyPemegangSahamPribadiChangeNotif.flag = true;
                    debugPrint('drawer');
                }else{
                    _providerFormMCompanyPemegangSahamPribadiChangeNotif.autoValidate = true;
                    _providerFormMCompanyPemegangSahamPribadiChangeNotif.flag = false;
                }
            }

            var _providerAddShareHolderCom = Provider.of<AddPemegangSahamLembagaChangeNotif>(context, listen: false);
            await _providerAddShareHolderCom.getDataFromDashboard(context);
            if (_providerFormMCompanyPemegangSahamKelembagaanChangeNotif.listPemegangSahamLembaga.isNotEmpty) {
                _providerFormMCompanyPemegangSahamKelembagaanChangeNotif.autoValidate = false;
                _providerFormMCompanyPemegangSahamKelembagaanChangeNotif.flag = true;
            } else {
                if(_providerAddShareHolderCom.checkIsMandatoryPemegangSahamLembaga()){
                    _providerFormMCompanyPemegangSahamKelembagaanChangeNotif.autoValidate = false;
                    _providerFormMCompanyPemegangSahamKelembagaanChangeNotif.flag = true;
                }else{
                    _providerFormMCompanyPemegangSahamKelembagaanChangeNotif.autoValidate = true;
                    _providerFormMCompanyPemegangSahamKelembagaanChangeNotif.flag = false;
                }
            }

            // kodingan asli untuk pengecekan validasi pemegang saham pribadi
            // if(!_providerFormMCompanyPemegangSahamPribadiChangeNotif.flag){
            //     _providerFormMCompanyPemegangSahamPribadiChangeNotif.autoValidate = true;
            // }

//            if(!_providerFormMCompanyPemegangSahamKelembagaanChangeNotif.flag){
//                _providerFormMCompanyPemegangSahamKelembagaanChangeNotif.autoValidate = true;
//            }
//            if(!_providerFormMCompanyPemegangSahamKelembagaanAlamatChangeNotif.flag){
//                _providerFormMCompanyPemegangSahamKelembagaanAlamatChangeNotif.autoValidate = true;
//            }
            if(_providerFormMCompanyPemegangSahamPribadiChangeNotif.flag && _providerFormMCompanyPemegangSahamKelembagaanChangeNotif.flag){
                if(await _providerFormMCompanyPemegangSahamPribadiChangeNotif.deleteSQLite()
                    && await _providerFormMCompanyPemegangSahamKelembagaanChangeNotif.deleteSQLite()){
                    _providerFormMCompanyPemegangSahamPribadiChangeNotif.saveToSQLite();
                    _providerFormMCompanyPemegangSahamKelembagaanChangeNotif.saveToSQLite();
                }
                selectedIndex+=1;
                isMenuPemegangSaham = true;
                _isValidate = false;
            } else {
                _isValidate = true;
            }
        }

        else if (index == 5){
            if(isInfoAppDone){
                await _providerInfoApp.checkDataDakor();
                if(await _providerInfoApp.deleteSQLite()){
                    _providerInfoApp.saveToSQLite(context);
                }
                selectedIndex += 1;
                isInfoAppDone = true;
                _isValidate = false;
            } else {
                _isValidate = true;
            }
        }

        else if (index == 6){
            if(!_providerInfoObjectUnit.flag){
                _providerInfoObjectUnit.autoValidate = true;
                _isValidate = true;
            }
            if(_providerInfoSales.listInfoSales.length == 0){
                _providerInfoSales.autoValidate = true;
                _isValidate = true;
            }
            if(!_providerKolateral.flag){
                if(_providerKolateral.collateralTypeModel == null){
                    _providerKolateral.autoValidateAuto = true;
                    _providerKolateral.autoValidateProp = false;
                    _isValidate = true;
                } else {
                    if(_providerKolateral.collateralTypeModel.id == "001"){
                        _providerKolateral.autoValidateAuto = true;
                        _providerKolateral.autoValidateProp = false;
                        _isValidate = true;
                    } else if(_providerKolateral.collateralTypeModel.id == "002") {
                        _providerKolateral.autoValidateAuto = false;
                        _providerKolateral.autoValidateProp = true;
                        _isValidate = true;
                    }
                }
            }
            if(_providerInfoObjectUnit.flag && _providerInfoSales.listInfoSales.length != 0 && _providerKolateral.flag){
                if(await _providerInfoObjectUnit.deleteSQLite()){
                    _providerInfoObjectUnit.saveToSQLite(context);
                }
                if(await _providerInfoSales.deleteSQLite()){
                    _providerInfoSales.saveToSQLite(context);
                }
                if(_providerKolateral.collateralTypeModel.id == "001"){
                    if(await _providerKolateral.deleteSQLiteCollaOto()){
                        _providerKolateral.saveToSQLiteOto(context);
                    }
                }
                else if(_providerKolateral.collateralTypeModel.id == "002") {
                    if(await _providerKolateral.deleteSQLiteCollaProp()){
                        _providerKolateral.saveToSQLiteProperti(context, "6");
                    }
                }
                // selectedIndex +=1;
                Provider.of<InfoObjectKaroseriChangeNotifier>(context, listen: true).isVisible ? selectedIndex +=1 : selectedIndex +=2;
                isMenuObjectInformationDone = true;
                _isValidate = false;
            }
        }
        else if (index == 7){
            // if(_providerKaroseri.listFormKaroseriObject.isNotEmpty){
                if(await _providerKaroseri.deleteSQLite()){
                    _providerKaroseri.saveToSQLite();
                }
                selectedIndex += 1;
                isInfoObjKaroseriDone = true;
                _isValidate = false;
            // } else {
            //     _showSnackBar("Karoseri tidak boleh kosong");
            //     _providerKaroseri.autoValidate = true;
            //     _isValidate = true;
            // }
        }
        else if (index == 8){
            if(!_providerInfoCreditStructure.flag){
                _providerInfoCreditStructure.autoValidate = true;
                _isValidate = true;
            }
            if(_providerMajorInsurance.listFormMajorInsurance.length == 0){
                _providerMajorInsurance.flag = true;
                _isValidate = true;
            }
            if(_providerAdditionalInsurance.listFormAdditionalInsurance.length == 0 && _providerInfoObjectUnit.groupObjectSelected.KODE == "002"){
                _providerAdditionalInsurance.flag = true;
                _isValidate = true;
            }
            if(_providerInfoCreditStructure.flag && _providerMajorInsurance.listFormMajorInsurance.length != 0){
                if(await _providerInfoCreditStructure.deleteSQLite()){
                    _providerInfoCreditStructure.saveToSQLiteInfStrukturKreditBiaya();
                }
                _providerInfoCreditStructure.updateMS2ApplObjectInfStrukturKredit();
                if(await _providerMajorInsurance.deleteSQLite()){
                    _providerMajorInsurance.saveToSQLite();
                }
                if(await _providerAdditionalInsurance.deleteSQLite()){
                    _providerAdditionalInsurance.saveToSQLite();
                }
                _providerWMP.saveToSQLite();
                selectedIndex += 1;
                isMenuDetailLoanDone = true;
                _isValidate = false;
            }
        }
        else if (index == 9){
            // if(_providerCreditSubsidy.listInfoCreditSubsidy.isNotEmpty){
                if(await _providerCreditSubsidy.deleteSQLIte()){
                    _providerCreditSubsidy.saveToSQLite();
                }
                selectedIndex += 1;
                _isCreditSubsidyDone = true;
                _isValidate = false;
            // } else {
            //     _showSnackBar("Info Kredit Subsidi tidak boleh kosong");
            //     _isValidate = true;
            // }
        }
        else if (index == 10){
            if(_providerInfoDocument.listInfoDocument.isNotEmpty){
                if(await _providerInfoDocument.deleteSQLite()){
                    _providerInfoDocument.saveToSQLite(context);
                }
                if(_providerTaksasiUnit.isVisible){
                    selectedIndex += 1;
                    _isValidate = false;
                }
                else {
                    selectedIndex += 2;
                    _isValidate = false;
                }
                _isDocumentInfoDone = true;
            } else {
                _showSnackBar("Info Dokumen tidak boleh kosong");
                _isValidate = true;
            }
        }
        else if (index == 11){
            if(_providerInfoObjectUnit.groupObjectSelected != null){
                if(_providerInfoObjectUnit.groupObjectSelected.KODE == "001"){
                    if(_form.validate()){
                        selectedIndex += 1;
                        isTaksasiUnitDone = true;
                        _isValidate = false;
                    }
                    _providerTaksasiUnit.autoValidateMotor;
                    _isValidate = true;
                }
                else if(_providerInfoObjectUnit.groupObjectSelected.KODE == "002"){
                    if(_form.validate()){
                        selectedIndex += 1;
                        isTaksasiUnitDone = true;
                        _isValidate = false;
                    }
                    _providerTaksasiUnit.autoValidateCar;
                    _isValidate = true;
                }
                if(await _providerTaksasiUnit.deleteSQLite()){
                    _providerTaksasiUnit.saveToSQLite(context);
                }
            }
        }
        return _isValidate;
    }

    void actionMoreButton(BuildContext context,String action) async {
        await _setLastStep(this._selectedIndex);
        // List _data = await _dbHelper.selectLastStep();
        var _providerInfoAlamat = Provider.of<FormMInfoAlamatChangeNotif>(context, listen: false);
        var _providerFormMCompanyRincianChangeNotif = Provider.of<FormMCompanyRincianChangeNotifier>(context, listen: false);
        var _providerFormMCompanyAlamatChangeNotif = Provider.of<FormMCompanyAlamatChangeNotifier>(context, listen: false);
        var _providerFormMCompanyPendapatanChangeNotif = Provider.of<FormMCompanyPendapatanChangeNotifier>(context, listen: false);
        var _providerFormMCompanyPenjaminChangeNotif = Provider.of<FormMGuarantorChangeNotifier>(context, listen: false);
        var _providerFormMCompanyManajemenPICChangeNotif = Provider.of<FormMCompanyManajemenPICChangeNotifier>(context, listen: false);
        var _providerFormMCompanyManajemenPICAlamatChangeNotif = Provider.of<FormMCompanyManajemenPICAlamatChangeNotifier>(context, listen: false);
        var _providerFormMCompanyPemegangSahamPribadiChangeNotif = Provider.of<PemegangSahamPribadiChangeNotifier>(context, listen: false);
        var _providerFormMCompanyPemegangSahamKelembagaanChangeNotif = Provider.of<PemegangSahamLembagaChangeNotifier>(context, listen: false);
        var _providerInfoObjectUnit = Provider.of<InformationObjectUnitChangeNotifier>(context, listen: false);
        var _providerInfoSales = Provider.of<InformationSalesmanChangeNotifier>(context, listen: false);
        var _providerKolateral = Provider.of<InformationCollateralChangeNotifier>(context, listen: false);
        var _providerKaroseri = Provider.of<InfoObjectKaroseriChangeNotifier>(context, listen: false);
        var _providerWMP = Provider.of<InfoWMPChangeNotifier>(context, listen: false);
        var _providerCreditIncome = Provider.of<InfoCreditIncomeChangeNotifier>(context, listen: false);
        var _providerMajorInsurance = Provider.of<InfoMajorInsuranceChangeNotifier>(context, listen: true);
        var _providerAdditionalInsurance = Provider.of<InfoAdditionalInsuranceChangeNotifier>(context, listen: true);
        var _providerInfoCreditStructure = Provider.of<InfoCreditStructureChangeNotifier>(context, listen: false);
        var _providerCreditSubsidy = Provider.of<InfoCreditSubsidyChangeNotifier>(context, listen: false);
        var _providerInfoDocument = Provider.of<InfoDocumentChangeNotifier>(context, listen: false);
        var _providerTaksasiUnit = Provider.of<TaksasiUnitChangeNotifier>(context, listen: false);
        var _providerInfoApp = Provider.of<InfoAppChangeNotifier>(context, listen: false);
        var _providerMarketingNotes = Provider.of<MarketingNotesChangeNotifier>(context, listen: false);
        var _providerSurvey = Provider.of<ResultSurveyChangeNotifier>(context, listen: false);
        var _providerListSurveyPhotoChangeNotifier = Provider.of<ListSurveyPhotoChangeNotifier>(context, listen: false);

        debugPrint("CEK_INDEX_SAVE_DRAFT $_selectedIndex");
        for(int i=0; i <= this._selectedIndex; i++){
            // if(action == LabelPopUpMenuButtonSaveToDraftBackToHome.SAVE_TO_DRAFT){
            if(i == 0){
                debugPrint("SAVE_DRAFT $i");
                //rincian lembaga
                if(await _providerFormMCompanyRincianChangeNotif.deleteSQLite()){
                    _providerFormMCompanyRincianChangeNotif.saveToSQLite(context);
                }
                //alamat lembaga
                if(await _providerInfoAlamat.deleteSQLite("10")){
                    _providerFormMCompanyAlamatChangeNotif.saveToSQLite("10");
                }
            }
            else if(i == 1){
                debugPrint("SAVE_DRAFT $i");
                if(await _providerFormMCompanyPendapatanChangeNotif.deleteSQLite()){
                    _providerFormMCompanyPendapatanChangeNotif.saveToSQLite(context);
                }
            }
            else if(i == 2){
                debugPrint("SAVE_DRAFT $i");
                if(await _providerFormMCompanyPenjaminChangeNotif.deleteSQLite()){
                    _providerFormMCompanyPenjaminChangeNotif.saveToSQLite(context);
                    // _providerFormMCompanyPenjaminChangeNotif.saveToSQLite();
                    // _providerFormMCompanyPenjaminChangeNotif.saveToSQLiteAddressGuarantor();
                }
            }
            else if(i == 3){
                debugPrint("SAVE_DRAFT $i");
                //manajemen pic
                _providerFormMCompanyManajemenPICChangeNotif.saveToSQLite();
                //alamat manajemen pic
                if(await _providerInfoAlamat.deleteSQLite("9")){
                    _providerFormMCompanyManajemenPICAlamatChangeNotif.saveToSQLite("9");
                }
            }
            else if(i == 4){
                debugPrint("SAVE_DRAFT $i");
                if(await _providerFormMCompanyPemegangSahamPribadiChangeNotif.deleteSQLite()
                    && await _providerFormMCompanyPemegangSahamKelembagaanChangeNotif.deleteSQLite()){
                    _providerFormMCompanyPemegangSahamPribadiChangeNotif.saveToSQLite();
                    _providerFormMCompanyPemegangSahamKelembagaanChangeNotif.saveToSQLite();
                }
            }
            else if(i == 5){
                debugPrint("SAVE_DRAFT $i");
                if(await _providerInfoApp.deleteSQLite()){
                    _providerInfoApp.saveToSQLite(context);
                }
            }
            else if(i == 6){
                debugPrint("SAVE_DRAFT $i");
                if(await _providerInfoObjectUnit.deleteSQLite()){
                    _providerInfoObjectUnit.saveToSQLite(context);
                }
                if(await _providerInfoSales.deleteSQLite()){
                    _providerInfoSales.saveToSQLite(context);
                }
                _providerKolateral.collateralTypeModel != null
                    ?
                _providerKolateral.collateralTypeModel.id == "001"
                    ?
                await _providerKolateral.deleteSQLiteCollaOto() ? _providerKolateral.saveToSQLiteOto(context) : print("")
                    :
                await _providerKolateral.deleteSQLiteCollaProp() ? _providerKolateral.saveToSQLiteProperti(context, "6") : print("ada")
                    :
                print("no colla");
                // _providerKolateral.collateralTypeModel.id == "001" ? _providerKolateral.saveToSQLiteOto() : _providerKolateral.saveToSQLiteProperti(context, "6");
                if(await _providerWMP.deleteSQLite()){
                    _providerWMP.saveToSQLite();
                }
            }
            else if(i == 7){
                debugPrint("SAVE_DRAFT $i");
                if(await _providerKaroseri.deleteSQLite()){
                    _providerKaroseri.saveToSQLite();
                }
            }
            else if(i == 8){
                debugPrint("SAVE_DRAFT $i");
                _providerInfoCreditStructure.updateMS2ApplObjectInfStrukturKredit();
                if(await _providerInfoCreditStructure.deleteSQLite()){
                    _providerInfoCreditStructure.saveToSQLiteInfStrukturKreditBiaya();
                }
                if(await _providerMajorInsurance.deleteSQLite()){
                    _providerMajorInsurance.saveToSQLite();
                }
                if(await _providerAdditionalInsurance.deleteSQLite()){
                    _providerAdditionalInsurance.saveToSQLite();
                }
                if(await _providerWMP.deleteSQLite()){
                    _providerWMP.saveToSQLite();
                }
                _providerCreditIncome.updateMS2ApplObjectInfStrukturKreditIncome();
            }
            else if(i == 9){
                if(await _providerCreditSubsidy.deleteSQLIte()){
                    _providerCreditSubsidy.saveToSQLite();
                }
            }
            else if(i == 10){
                if(await _providerInfoDocument.deleteSQLite()){
                    _providerInfoDocument.saveToSQLite(context);
                }
            }
            else if(i == 11){
                if(await _providerTaksasiUnit.deleteSQLite()){
                    _providerTaksasiUnit.saveToSQLite(context);
                }
            }
            else if(i==12){
                _providerMarketingNotes.saveToSQLite();
            }
            else if(i==13){
                _providerSurvey.insertDataSurveyAsset(context);
                _providerSurvey.insertDataSurveyDetail(context);
                _providerSurvey.insertDataMS2SvyAssgnmt(context);
                _providerListSurveyPhotoChangeNotifier.saveToSQLite(context);
            }
        }
        // Ditambahkan delay karena untuk mencegah context destroy sebelum semua fungsi dijalankan
        Future.delayed(const Duration(seconds: 5), () => Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) =>
            Dashboard(flag: 0)), (Route<dynamic> route) => false));
    }

    void isShowDialog(BuildContext context, String action) {
        showDialog(
            context: context,
            barrierDismissible: true,
            builder: (BuildContext context){
                return Theme(
                    data: ThemeData(
                        fontFamily: "NunitoSans"
                    ),
                    child: AlertDialog(
                        title: Text("Informasi", style: TextStyle(fontWeight: FontWeight.bold)),
                        content: Text("Simpan ke draft dan kembali ke home ?"),
                        actions: <Widget>[
                            FlatButton(
                                onPressed: (){
                                    actionMoreButton(context, action);
                                    // Navigator.push(context, MaterialPageRoute(builder: (context) => Dashboard()));
                                },
                                child: Text("Ya",
                                    style: TextStyle(
                                        color: primaryOrange,
                                        fontSize: 14,
                                        fontWeight: FontWeight.w500,
                                        letterSpacing: 1.25
                                    )
                                )
                            ),
                            FlatButton(
                                onPressed: (){
                                    Navigator.pop(context);
                                },
                                child: Text("Tidak",
                                    style: TextStyle(
                                        color: Colors.grey,
                                        fontSize: 14,
                                        fontWeight: FontWeight.w500,
                                        letterSpacing: 1.25
                                    )
                                )
                            )
                        ],
                    ),
                );
            }
        );
    }

    void generateAplikasiPayung(BuildContext context) async {
        debugPrint("payung dijalankan");
        loadData = true;
        messageProcess = "Proses generate payung...";
        // var _listOID = Provider.of<ListOidChangeNotifier>(context,listen: false);
        // var _providerFoto = Provider.of<FormMFotoChangeNotifier>(context,listen: false);
        // var _providerUnitObject = Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false);
        SharedPreferences _preferences = await SharedPreferences.getInstance();
        List _dataNoPayung = await _dbHelper.selectDataNoPayung();
        debugPrint("cek no payung $_dataNoPayung");
        if(_dataNoPayung.isEmpty){
            final ioc = new HttpClient();
            ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;

            final _http = IOClient(ioc);

            var _body = jsonEncode({
                "unitId" : _preferences.getString("branchid"),
                "columnName" : "AC_APPL_NO",
                "objectGroupId" : "",
                "finTypeId" : "",
                "noAppUnit" : ""
                // "unitId":_preferences.getString("branchid"),
                // "columnName":"AC_APPL_NO",
                // "objectGroupId":"",//_providerUnitObject.groupObjectSelected != null ? _providerUnitObject.groupObjectSelected.KODE : ""
                // "finTypeTid":"",//_preferences.getString("cust_type") == "PER" ? _providerFoto.typeOfFinancingModelSelected.financingTypeId : _providerUnitObject.typeOfFinancingModelSelected.financingTypeId
                // "noAppUnit":""
            });

            // String urlAcction = await storage.read(key: "urlAcction");
            String _payungDanNomerUnit = await storage.read(key: "PayungDanNomerUnit");
            // print("${urlAcction}adira-acction-prod/acction/service/order/generateID");
            final _response = await _http.post(
                // "${BaseUrl.urlAcction}adira-acction-062036/acction/service/order/generateID",
                //   "${BaseUrl.urlGeneral}adira-acction-prod/acction/service/order/generateID",
                "${BaseUrl.urlGeneral}$_payungDanNomerUnit",
                body: _body,
                headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
            );

            if(_response.statusCode == 200 || _response.statusCode == 201){
                print(_response.body);
                print("payung masuk if response");
                loadData = false;
                _showSnackBarSuccess("Berhasil generate nomer aplikasi payung");
                final _result = jsonDecode(_response.body);
                var _orderId = _result['message'];
                await _dbHelper.insertDataNoPayung(_orderId);
                generateAplikasiUnit(context, _orderId);
            }
            else{
                loadData = false;
                messageProcess = "";
                print(_response.statusCode);
                print("payung masuk else response");
                _showSnackBar("Error generate no payung${_response.statusCode}");
            }
        }
        else{
            loadData = false;
            messageProcess = "";
            print("payung masuk else");
            generateAplikasiUnit(context, _dataNoPayung[0]['no_aplikasi_payung']);
        }
    }

    void generateAplikasiUnit(BuildContext context, String orderId) async {
        loadData = true;
        messageProcess = "Proses generate no unit...";
        // var _listOID = Provider.of<ListOidChangeNotifier>(context,listen: false);
        // var _providerFoto = Provider.of<FormMFotoChangeNotifier>(context,listen: false);
        // var _providerUnitObject = Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false);

        SharedPreferences _preferences = await SharedPreferences.getInstance();
        List _dataNoUnit = await _dbHelper.selectDataNoUnit();
        debugPrint("cek no unit $_dataNoUnit");
        debugPrint("cek no unit data ${_dataNoUnit[0]['no_unit'] == null}");
        if(_dataNoUnit[0]['no_unit'] == null){
            print("unit masuk if");
            final ioc = new HttpClient();
            ioc.badCertificateCallback =
                (X509Certificate cert, String host, int port) => true;

            final _http = IOClient(ioc);

            var _body = jsonEncode({
                "unitId" : _preferences.getString("branchid"),
                "columnName" : "AC_APPL_NO",
                "objectGroupId" : "",
                "finTypeId" : "",
                "noAppUnit" : ""
                // "unitId" : _preferences.getString("branchid"),
                // "columnName" : "AC_APPL_NO",
                // "objectGroupId" : "", //_providerUnitObject.groupObjectSelected != null ? _providerUnitObject.groupObjectSelected.KODE : ""
                // "finTypeTid" : "", //_preferences.getString("cust_type") == "PER" ? _providerFoto.typeOfFinancingModelSelected.financingTypeId : _providerUnitObject.typeOfFinancingModelSelected.financingTypeId
                // "noAppUnit" : ""
            });
            // String urlAcction = await storage.read(key: "urlAcction");
            String _payungDanNomerUnit = await storage.read(key: "PayungDanNomerUnit");
            final _response = await _http.post(
                // "${BaseUrl.urlAcction}adira-acction-062036/acction/service/order/generateID",
                //   "${BaseUrl.urlGeneral}adira-acction-prod/acction/service/order/generateID",
                "${BaseUrl.urlGeneral}$_payungDanNomerUnit",
                body: _body,
                headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
            );

            debugPrint("unit ${_response.statusCode}");
            if(_response.statusCode == 200 || _response.statusCode == 201){
                debugPrint("unit ${_response.body}");
                debugPrint("unit masuk if response");
                loadData = false;
                _showSnackBarSuccess("Berhasil generate nomer aplikasi unit");
                final _result = jsonDecode(_response.body);
                var _orderProductId = _result['message'];
                await _dbHelper.updateDataNoUnit(_orderProductId);
                calculate(context, orderId, _orderProductId);
                // submitFotoToECM(context,orderId,_orderProductId);
                // validateData(context,orderId,_orderProductId);
            }
            else{
                loadData = false;
                messageProcess = "";
                // print(_response.statusCode);
                print("payung masuk else response");
                _showSnackBar("Error generate no unit ${_response.statusCode}");
            }
        }
        else{
            loadData = false;
            messageProcess = "";
            print("unit masuk else");
            // validateData(context,orderId,_dataNoUnit[0]['no_unit']);
            submitFotoToECM(context, orderId, _dataNoUnit[0]['no_unit']);
        }
    }

    void submitFotoToECM(BuildContext context, String orderId, String orderProductId) async {
        loadData = true;
        this._orderSupportingDocuments.clear();
        messageProcess = "Proses submit ECM...";
        _urlUploadFile = await storage.read(key: "UploadFile");
        SharedPreferences _preferences = await SharedPreferences.getInstance();
        String _orderNo = _preferences.getString("order_no");
        DateTime _timeStartValidate = DateTime.now();
        var _providerDocument = Provider.of<InfoDocumentChangeNotifier>(context,listen: false);
        var _providerResultSurvey = Provider.of<ListSurveyPhotoChangeNotifier>(context,listen: false);
        var _dio = dio.Dio();
        (_dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate  = (client) {
            client.badCertificateCallback=(X509Certificate cert, String host, int port){
                return true;
            };
        };
        // info document
        if(_providerDocument.listInfoDocument.isNotEmpty){
            print("masuk if info document");
            for(int i=0; i < _providerDocument.listInfoDocument.length; i++){
                print("cek docTypeId infoDocument ${_providerDocument.listInfoDocument[i].documentType.docTypeId} === ${_providerDocument.listInfoDocument[i].orderSupportingDocumentID}");
                List _data = await getListMetadata("${_providerDocument.listInfoDocument[i].documentType.docTypeId}");
                if(_data.isNotEmpty){
                    String _docTitle = _providerDocument.listInfoDocument[i].fileName.split("/").last;
                    dio.FormData formData = dio.FormData.fromMap({
                        "file" :  await dio.MultipartFile.fromFile(_providerDocument.listInfoDocument[i].documentDetail.file.path, filename: _docTitle),
                        "documentTitle" : "${DateFormat("dd-MM-yyyy HH-mm-ss").format(DateTime.now())}-$_orderNo-$_docTitle",
                        "documentType" : _data[0]['symbolName'],
                        "application" : "REVAMPMS2",
                        "objectStore" : "ADIRAOS",
                        "requestId" : "2020369"
                    });
                    var _bodyInfoDocument = jsonEncode({
                        "documentTitle" : "${DateFormat("dd-MM-yyyy HH-mm-ss").format(DateTime.now())}-$_orderNo-$_docTitle",
                        "documentType" : _data[0]['symbolName'],
                        "application" : "REVAMPMS2",
                        "objectStore" : "ADIRAOS",
                        "requestId" : "2020369"
                    });
                    print("dokumen belakang = ${_providerDocument.listInfoDocument[i].fileHeaderID} = ${_providerDocument.listInfoDocument[i].orderSupportingDocumentID}");
                    if(_providerDocument.listInfoDocument[i].orderSupportingDocumentID != "NEW"){
                        print("masuk iffffff");
                        _orderSupportingDocuments.add({
                            "orderSupportingDocumentID": _providerDocument.listInfoDocument[i].orderSupportingDocumentID,
                            "documentSpecification": {
                                "documentTypeID": _providerDocument.listInfoDocument[i].documentType.docTypeId,
                                "isMandatory": _providerDocument.listInfoDocument[i].documentType.mandatory,
                                "isDisplay": _providerDocument.listInfoDocument[i].documentType.display,
                                "isUnit": _providerDocument.listInfoDocument[i].documentType.flag_unit,
                                "receivedDate": formatDateValidateAndSubmit(DateTime.parse(_providerDocument.listInfoDocument[i].date)),//_providerDocument.listInfoDocument[i].date,
                            },
                            "fileSpecification": {
                                "fileName": _docTitle,
                                "fileHeaderID": _providerDocument.listInfoDocument[i].fileHeaderID,
                                "uploadDate": formatDateValidateAndSubmit(DateTime.now()),
                                "longitude": "${_providerDocument.listInfoDocument[i].longitude}",
                                "latitude": "${_providerDocument.listInfoDocument[i].latitude}"
                            },
                            "creationalSpecification": {
                                "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                                "createdBy": _preferences.getString("username"),
                                "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                                "modifiedBy": _preferences.getString("username"),
                            },
                            "status": "ACTIVE"
                        });
                    }
                    else{
                        try{
                            var _response = await _dio.post(
                                "${BaseUrl.urlECM}$_urlUploadFile",
                                // "http://10.50.3.123:8080/api/v1/files",
                                data: formData,
                                options: dio.Options(
                                    headers: {"x-api-key": xApiKey, "Authorization":"bearer $token"}, // M1I02YSVDX3ZCV4NF1CBUSXWSKD204
                                )
                            );
                            insertLog(context,_timeStartValidate,DateTime.now(),_bodyInfoDocument,"Start Upload Foto ${_data[0]['symbolName']}","Start Upload Foto ${_data[0]['symbolName']}");
                            if(_response.statusCode != 200){
                                loadData = false;
                                // _showSnackBar("Error ECM info document ${_response.statusCode}");
                                _showSnackBar("Problem koneksi ke ECM. Mohon ditunggu beberapa saat untuk submit kembali");
                                messageProcess = "";
                                return;
                            }
                            else{
                                // List _splitDate = _providerDocument.listInfoDocument[i].date.split("-");
                                // print("cek dateTime "+_splitDate[2]+"-"+_splitDate[1]+"-"+_splitDate[0]);
                                // print("info dokumen tgl ${formatDateValidateAndSubmit(DateTime.parse(_splitDate[2]+"-"+_splitDate[1]+"-"+_splitDate[0]))}");
                                print("result info dokumen = ${_response.data}");
                                if(_response.data['status'] == 200){
                                    _orderSupportingDocuments.add({
                                        "orderSupportingDocumentID": _providerDocument.listInfoDocument[i].orderSupportingDocumentID,
                                        "documentSpecification": {
                                            "documentTypeID": _providerDocument.listInfoDocument[i].documentType.docTypeId,
                                            "isMandatory": _providerDocument.listInfoDocument[i].documentType.mandatory,
                                            "isDisplay": _providerDocument.listInfoDocument[i].documentType.display,
                                            "isUnit": _providerDocument.listInfoDocument[i].documentType.flag_unit,
                                            "receivedDate": formatDateValidateAndSubmit(DateTime.parse(_providerDocument.listInfoDocument[i].date)),//_providerDocument.listInfoDocument[i].date,
                                        },
                                        "fileSpecification": {
                                            "fileName": _docTitle,
                                            "fileHeaderID": _response.data['objectId'].toString().replaceAll("{}", ""),
                                            "uploadDate": formatDateValidateAndSubmit(DateTime.now()),
                                            "longitude": "${_providerDocument.listInfoDocument[i].longitude}",
                                            "latitude": "${_providerDocument.listInfoDocument[i].latitude}"
                                        },
                                        "creationalSpecification": {
                                            "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                                            "createdBy": _preferences.getString("username"),
                                            "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                                            "modifiedBy": _preferences.getString("username"),
                                        },
                                        "status": "ACTIVE"
                                    });
                                }
                                else{
                                    loadData = false;
                                    // _showSnackBar("Error ECM tempat tinggal response ${_response.statusCode}");
                                    _showSnackBar("Problem koneksi ke ECM. Mohon ditunggu beberapa saat untuk submit kembali");
                                    messageProcess = "";
                                    return;
                                }
                                // _listObjectIdInfoDocument.add(_response.data['objectId'].toString().replaceAll("{}", ""));
                            }
                        }
                        catch(e){
                            loadData = false;
                            _showSnackBar("Error submit ECM info dokumen ${e.toString()}");
                            return;
                        }
                    }
                }
                else{
                    loadData = false;
                    _showSnackBar("Error get metadata info document");
                    return;
                }
            }
        }

        // photo survey
        // sementara
        if(_preferences.getString("last_known_state") == "IDE"){
            _providerResultSurvey.listSurveyPhoto.clear();
        }
        if(_providerResultSurvey.listSurveyPhoto.isNotEmpty){
            List _data = await getListMetadata("A8C");
            if(_data.isNotEmpty){
                for(int i=0; i < _providerResultSurvey.listSurveyPhoto.length; i++){
                    if(_providerResultSurvey.listSurveyPhoto[i].listImageModel.isNotEmpty){
                        for(int j=0; j<_providerResultSurvey.listSurveyPhoto[i].listImageModel.length; j++){
                            String _docTitle = _providerResultSurvey.listSurveyPhoto[i].listImageModel[j].path.split("/").last;
                            dio.FormData formData = dio.FormData.fromMap({
                                "file" :  await dio.MultipartFile.fromFile(_providerResultSurvey.listSurveyPhoto[i].listImageModel[j].path, filename: _docTitle),
                                "documentTitle" : "${DateFormat("dd-MM-yyyy HH-mm-ss").format(DateTime.now())}-$_orderNo-$_docTitle",
                                "documentType" : _data[0]['symbolName'],
                                "application" : "REVAMPMS2",
                                "objectStore" : "ADIRAOS",
                                "requestId" : "2020369"
                                // "nama" : _preferences.getString("fullname"),
                                // "region" : _preferences.getString("branchid"),
                                // "NIKPIC" : _preferences.getString("username")
                            });
                            try{
                                var _response = await _dio.post(
                                    "${BaseUrl.urlECM}$_urlUploadFile",
                                    // "http://10.50.3.123:8080/api/v1/files",
                                    data: formData,
                                    options: dio.Options(
                                        headers: {"x-api-key": xApiKey, "Authorization":"bearer $token"}, // M1I02YSVDX3ZCV4NF1CBUSXWSKD204
                                    )
                                );
                                insertLog(context,_timeStartValidate,DateTime.now(),formData.toString(),"Start Upload Foto ${_data[0]['symbolName']}","Start Upload Foto ${_data[0]['symbolName']}");
                                if(_response.statusCode != 200){
                                    loadData = false;
                                    // _showSnackBar("Error ECM photo survey ${_response.statusCode}");
                                    _showSnackBar("Problem koneksi ke ECM. Mohon ditunggu beberapa saat untuk submit kembali");
                                    messageProcess = "";
                                    return;
                                }
                                else{
                                    _orderSupportingDocuments.add({
                                        "orderSupportingDocumentID": "NEW",
                                        "documentSpecification": {
                                            "documentTypeID": "${_providerResultSurvey.listSurveyPhoto[i].photoTypeModel.id}",
                                            "isMandatory": 1, // 0 = true | 1 = false
                                            "isDisplay": 1, // 0 = true | 1 = false
                                            "isUnit": 1, // 0 = true | 1 = false
                                            "receivedDate": formatDateValidateAndSubmit(DateTime.now()),// tidak ada field tgl terima dokumen
                                        },
                                        "fileSpecification": {
                                            "fileName": _docTitle,
                                            "fileHeaderID": _response.data['objectId'].toString().replaceAll("{}", ""),
                                            "uploadDate": formatDateValidateAndSubmit(DateTime.now()),
                                            "longitude": "${_providerResultSurvey.listSurveyPhoto[i].listImageModel[j].longitude}",
                                            "latitude": "${_providerResultSurvey.listSurveyPhoto[i].listImageModel[j].latitude}"
                                        },
                                        "creationalSpecification": {
                                            "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                                            "createdBy": _preferences.getString("username"),
                                            "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                                            "modifiedBy": _preferences.getString("username"),
                                        },
                                        "status": "ACTIVE"
                                    });
                                }
                            }
                            catch(e){
                                loadData = false;
                                _showSnackBar("Error submit ECM foto survey ${e.toString()}");
                                return;
                            }
                        }
                    }
                }
            }
            else{
                loadData = false;
                _showSnackBar("Error get metadata hasil survey");
                return;
            }
            await _submitDataPartial.submitPhoto(context, "2");
        }

        loadData = false;
        validateData(context, orderId, orderProductId);
    }

    Future<void> setValueCustomerIncome(BuildContext context) async {
        this._customerIncomes.clear();
        var _providerIncome = Provider.of<FormMCompanyPendapatanChangeNotifier>(context, listen: false);
        SharedPreferences _preferences = await SharedPreferences.getInstance();
        for(int i=0; i < _providerIncome.listIncome.length; i++){
            _customerIncomes.add(
                {
                    "customerIncomeID": _preferences.getString("last_known_state") != "IDE" ? _providerIncome.listIncome[i].customerIncomeID : "NEW",
                    "incomeFrmlType": _providerIncome.listIncome[i].type_income_frml,
                    "incomeType": _providerIncome.listIncome[i].income_type,
                    "incomeValue": double.parse(_providerIncome.listIncome[i].income_value.replaceAll(",", "")),
                    "dataStatus": "ACTIVE",
                    "creationalSpecification": {
                        "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                        "createdBy": _preferences.getString("username"),
                        "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                        "modifiedBy": _preferences.getString("username"),
                    },
                }
            );
        }
    }

    void validateData(BuildContext context, String orderId, String orderProductId) async {
        print("cek validateData");
        _showSnackBarSuccess("Success submit ECM");
        loadData = true;
        messageProcess = "Proses validate data...";
        List _dataResultSurvey = await _dbHelper.selectResultSurveyMS2Assignment();
        // Dedup
        // var _listOID = Provider.of<ListOidChangeNotifier>(context,listen: false);

        // Form IDE Individu
        // var _providerFoto = Provider.of<FormMFotoChangeNotifier>(context, listen: false);
        // var _providerInfoNasabah = Provider.of<FormMInfoNasabahChangeNotif>(context, listen: false);
        // var _providerInfoAlamat = Provider.of<FormMInfoAlamatChangeNotif>(context, listen: false);
        // var _providerIbu = Provider.of<FormMInformasiKeluargaIBUChangeNotif>(context, listen: false);
        // var _providerKeluarga = Provider.of<FormMInfoKeluargaChangeNotif>(context, listen: false);
        // var _providerOccupation = Provider.of<FormMOccupationChangeNotif>(context,listen: false);
        // var _providerIncome = Provider.of<FormMIncomeChangeNotifier>(context, listen: false);
        var _providerGuarantor = Provider.of<FormMGuarantorChangeNotifier>(context, listen: false);

        // Form IDE Company
        var _providerRincian = Provider.of<FormMCompanyRincianChangeNotifier>(context, listen: false);
        var _providerInfoAlamatCompany = Provider.of<FormMCompanyAlamatChangeNotifier>(context, listen: false);
        // var _providerIncomeCompany = Provider.of<FormMCompanyPendapatanChangeNotifier>(context, listen: false);
        var _providerManajemenPIC = Provider.of<FormMCompanyManajemenPICChangeNotifier>(context, listen: false);
        var _providerAddressManajemenPIC = Provider.of<FormMCompanyManajemenPICAlamatChangeNotifier>(context,listen: false);
        // var _providerAddressSharedHolder = Provider.of<FormMCompanyPemegangSahamPribadiAlamatChangeNotifier>(context, listen: false);
        var _providerPemegangSaham = Provider.of<PemegangSahamPribadiChangeNotifier>(context, listen: false);
        var _providerPemegangSahamCompany = Provider.of<PemegangSahamLembagaChangeNotifier>(context, listen: false);
        // var _providerAddressSharedHolderCom = Provider.of<FormMCompanyPemegangSahamKelembagaanAlamatChangeNotifier>(context, listen: false);
        var _providerSurvey = Provider.of<ResultSurveyChangeNotifier>(context,listen:false);

        // Form App
        var _providerApp = Provider.of<InfoAppChangeNotifier>(context, listen: false);
        var _providerUnitObject = Provider.of<InformationObjectUnitChangeNotifier>(context, listen: false);
        var _providerKolateral = Provider.of<InformationCollateralChangeNotifier>(context, listen: false);
        var _providerSales = Provider.of<InformationSalesmanChangeNotifier>(context, listen: false);
        var _providerKaroseri = Provider.of<InfoObjectKaroseriChangeNotifier>(context, listen: false);
        var _providerCreditStructure = Provider.of<InfoCreditStructureChangeNotifier>(context, listen: false);
        var _providerCreditStructureType = Provider.of<InfoCreditStructureTypeInstallmentChangeNotifier>(context, listen: false);
        var _providerInsurance = Provider.of<InfoMajorInsuranceChangeNotifier>(context, listen: false);
        var _providerAdditionalInsurance = Provider.of<InfoAdditionalInsuranceChangeNotifier>(context, listen: true);
        var _providerCreditIncome = Provider.of<InfoCreditIncomeChangeNotifier>(context, listen: false);
        var _providerWmp = Provider.of<InfoWMPChangeNotifier>(context,listen: false);
        var _providerSubsidy = Provider.of<InfoCreditSubsidyChangeNotifier>(context,listen: false);
        // var _providerSubsidyDetail = Provider.of<AddCreditSubsidyChangeNotifier>(context, listen: false);
        // var _providerCreditStructureTypeInstallment = Provider.of<InfoCreditStructureTypeInstallmentChangeNotifier>(context,listen: false);
        // var _providerDocument = Provider.of<InfoDocumentChangeNotifier>(context,listen: false);

        // var occupation = Provider.of<FormMFotoChangeNotifier>(context, listen: false).occupationSelected.KODE;
        // var _providerInfoDocument = Provider.of<InfoDocumentChangeNotifier>(context, listen: false);

        SharedPreferences _preferences = await SharedPreferences.getInstance();
        String _npwpType = "";
        if(_providerRincian.typeNPWPSelected != null) {
            _npwpType = _providerRincian.typeNPWPSelected != null ? _providerRincian.typeNPWPSelected.id : "2";
        } else {
            _npwpType = "2";
        }

        String _npwpAddress = "";
        if(_providerRincian.radioValueIsHaveNPWP == 1){
            for(int i=0; i < _providerInfoAlamatCompany.listAlamatKorespondensi.length; i++) {
                if(_providerInfoAlamatCompany.listAlamatKorespondensi[i].jenisAlamatModel.KODE == "03") {
                    _npwpAddress = _providerInfoAlamatCompany.listAlamatKorespondensi[i].address.toString();
                }
            }
            // _npwpAddress = _providerRincian.controllerNPWPAddress.text.isNotEmpty ? _providerRincian.controllerNPWPAddress.text : _providerInfoAlamatCompany.listAlamatKorespondensi[i].address.toString();
        }
        else {
            // _npwpAddress = "INDONESIA";
            for(int i=0; i < _providerInfoAlamatCompany.listAlamatKorespondensi.length; i++) {
                if(_providerInfoAlamatCompany.listAlamatKorespondensi[i].jenisAlamatModel.KODE == "03") {
                    _npwpAddress = _providerInfoAlamatCompany.listAlamatKorespondensi[i].address.toString();
                }
            }
        }

        String _npwpFullName = "";
        if(_providerRincian.radioValueIsHaveNPWP == 1){
            _npwpFullName = _providerRincian.controllerFullNameNPWP.text;
        } else {
            _npwpFullName = _providerRincian.controllerFullNameNPWP.text;
        }

        String _npwpNumber = "";
        if(_providerRincian.radioValueIsHaveNPWP == 1){
            _npwpNumber = _providerRincian.controllerNPWP.text;
        }
        else{
            _npwpNumber = "000000000000000";
        }

        await setValueCustomerIncome(context);
        // var _customerIncomes = [];
        // for(int i=0; i < 10; i++) {
        //     _customerIncomes.add(
        //         {
        //             "customerIncomeID": _preferences.getString("last_known_state") != "IDE" ? _preferences.getString("customerIncomeID") : "NEW",
        //             "incomeFrmlType": i == 0
        //                 ? "S" : i == 1
        //                 ? "+" : i == 2
        //                 ? "=" : i == 3
        //                 ? "-" : i == 4
        //                 ? "=" : i == 5
        //                 ? "-" : i == 6
        //                 ? "-" : i == 7
        //                 ? "=" : i == 8
        //                 ? "-" : i == 9
        //                 ? "="
        //                 : "N",
        //             "incomeType": i == 0
        //                 ? "016" : i == 1
        //                 ? "002" : i == 2
        //                 ? "003" : i == 3
        //                 ? "004" : i == 4
        //                 ? "005" : i == 5
        //                 ? "006" : i == 6
        //                 ? "007" : i == 7
        //                 ? "008" : i == 8
        //                 ? "009" : i == 9
        //                 ? "010"
        //                 : "013",
        //             "incomeValue": i == 0
        //                 ? double.parse(_providerIncomeCompany.controllerMonthlyIncome.text.replaceAll(",", "")) : i == 1
        //                 ? double.parse(_providerIncomeCompany.controllerOtherIncome.text.replaceAll(",", "")) : i == 2
        //                 ? double.parse(_providerIncomeCompany.controllerTotalIncome.text.replaceAll(",", "")) : i == 3
        //                 ? double.parse(_providerIncomeCompany.controllerCostOfRevenue.text.replaceAll(",", "")) : i == 4
        //                 ? double.parse(_providerIncomeCompany.controllerGrossProfit.text.replaceAll(",", "")) : i == 5
        //                 ? double.parse(_providerIncomeCompany.controllerOperatingCosts.text.replaceAll(",", "")) : i == 6
        //                 ? double.parse(_providerIncomeCompany.controllerOtherCosts.text.replaceAll(",", "")) : i == 7
        //                 ? double.parse(_providerIncomeCompany.controllerNetProfitBeforeTax.text.replaceAll(",", "")) : i == 8
        //                 ? double.parse(_providerIncomeCompany.controllerTax.text.replaceAll(",", "")) : i == 9
        //                 ? double.parse(_providerIncomeCompany.controllerNetProfitAfterTax.text.replaceAll(",", ""))
        //                 : double.parse(_providerIncomeCompany.controllerOtherInstallments.text.replaceAll(",", "")),
        //             "dataStatus": "ACTIVE",
        //             "creationalSpecification": {
        //                 "createdAt": formatDateValidateAndSubmit(DateTime.now()),
        //                 "createdBy": _preferences.getString("username"),
        //                 "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
        //                 "modifiedBy": _preferences.getString("username"),
        //             },
        //         }
        //     );
        // }

        var _detailAddressesInfoNasabah = [];
        for (int i = 0; i < _providerInfoAlamatCompany.listAlamatKorespondensi.length; i++) {
            _detailAddressesInfoNasabah.add({
                "addressID": _preferences.getString("last_known_state") != "IDE" ? _providerInfoAlamatCompany.listAlamatKorespondensi[i].addressID != "NEW" ? _providerInfoAlamatCompany.listAlamatKorespondensi[i].addressID : "NEW" : "NEW",
                "addressSpecification": {
                    "koresponden": _providerInfoAlamatCompany.listAlamatKorespondensi[i].isCorrespondence ? "1" : "0",
                    "matrixAddr": "10",
                    "address": _providerInfoAlamatCompany.listAlamatKorespondensi[i].address,
                    "rt": _providerInfoAlamatCompany.listAlamatKorespondensi[i].rt,
                    "rw": _providerInfoAlamatCompany.listAlamatKorespondensi[i].rw,
                    "provinsiID": _providerInfoAlamatCompany.listAlamatKorespondensi[i].kelurahanModel.PROV_ID,
                    "kabkotID": _providerInfoAlamatCompany.listAlamatKorespondensi[i].kelurahanModel.KABKOT_ID,
                    "kecamatanID": _providerInfoAlamatCompany.listAlamatKorespondensi[i].kelurahanModel.KEC_ID,
                    "kelurahanID": _providerInfoAlamatCompany.listAlamatKorespondensi[i].kelurahanModel.KEL_ID,
                    "zipcode": _providerInfoAlamatCompany.listAlamatKorespondensi[i].kelurahanModel.ZIPCODE
                },
                "contactSpecification": {
                    "telephoneArea1": _providerInfoAlamatCompany.listAlamatKorespondensi[i].phoneArea1,
                    "telephone1": _providerInfoAlamatCompany.listAlamatKorespondensi[i].phone1,
                    "telephoneArea2": _providerInfoAlamatCompany.listAlamatKorespondensi[i].phoneArea2 == "" ? null : _providerInfoAlamatCompany.listAlamatKorespondensi[i].phoneArea2,
                    "telephone2": _providerInfoAlamatCompany.listAlamatKorespondensi[i].phone2,
                    "faxArea": _providerInfoAlamatCompany.listAlamatKorespondensi[i].faxArea,
                    "fax": _providerInfoAlamatCompany.listAlamatKorespondensi[i].fax,
                    "handphone": "",
                    "email": ""
                },
                "addressType": _providerInfoAlamatCompany.listAlamatKorespondensi[i].jenisAlamatModel.KODE,
                "creationalSpecification": {
                    "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                    "createdBy": _preferences.getString("username"),
                    "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                    "modifiedBy": _preferences.getString("username"),
                },
                "status": _providerInfoAlamatCompany.listAlamatKorespondensi[i].active == 0 ? "ACTIVE" : "INACTIVE",
                "foreignBusinessID": _preferences.getString("last_known_state") != "IDE" ?
                _providerInfoAlamatCompany.listAlamatKorespondensi[i].foreignBusinessID != "NEW" ?
                _providerInfoAlamatCompany.listAlamatKorespondensi[i].foreignBusinessID
                    :"CUST001" : "CUST001" // NEW
            });
        }

        //address colla
        if(_providerKolateral.collateralTypeModel.id == "002"){
            _detailAddressesInfoNasabah.add({
                    "addressID": _preferences.getString("last_known_state") != "IDE" ? _providerKolateral.addressID != "NEW" ? _providerKolateral.addressID : "NEW" : "NEW",
                    "foreignBusinessID": _preferences.getString("last_known_state") != "IDE" ? _providerKolateral.foreignBusinessID != null ? _providerKolateral.foreignBusinessID : "COLP001" : "COLP001", // NEW
                    "addressSpecification": {
                        "koresponden": _providerKolateral.isKorespondensi.toString(),
                        "matrixAddr": _preferences.getString("cust_type") == "PER" ? "6" : "13",
                        "address": _providerKolateral.controllerAddress.text,
                        "rt": _providerKolateral.controllerRT.text,
                        "rw": _providerKolateral.controllerRW.text,
                        "provinsiID": _providerKolateral.kelurahanSelected.PROV_ID,
                        "kabkotID": _providerKolateral.kelurahanSelected.KABKOT_ID,
                        "kecamatanID": _providerKolateral.kelurahanSelected.KEC_ID,
                        "kelurahanID": _providerKolateral.kelurahanSelected.KEL_ID,
                        "zipcode": _providerKolateral.kelurahanSelected.ZIPCODE
                    },
                    "contactSpecification": {
                        "telephoneArea1": null,
                        "telephone1": null,
                        "telephoneArea2": null,
                        "telephone2": null,
                        "faxArea": null,
                        "fax": null,
                        "handphone": null,
                        "email": null,
                        "noWa": null,
                        "noWa2": null,
                        "noWa3": null
                    },
                    "addressType": _providerKolateral.addressTypeSelected.KODE,
                    "creationalSpecification": {
                        "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                        "createdBy": _preferences.getString("username"),
                        "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                        "modifiedBy": _preferences.getString("username"),
                    },
                    "status": "ACTIVE"
                });
        }

        //alamat PIC customer company
        if(_providerAddressManajemenPIC.listManajemenPICAddress.isNotEmpty){
            for(int i=0; i < _providerAddressManajemenPIC.listManajemenPICAddress.length; i++){
                _detailAddressesInfoNasabah.add({
                    "addressID": _preferences.getString("last_known_state") != "IDE"
                        ? _providerAddressManajemenPIC.listManajemenPICAddress[i].addressID != "NEW"
                        ? _providerAddressManajemenPIC.listManajemenPICAddress[i].addressID : "NEW" : "NEW",
                    "addressSpecification": {
                        "koresponden": _providerAddressManajemenPIC.listManajemenPICAddress[i].isCorrespondence ? "1" : "0",
                        "matrixAddr": "9",
                        "address": _providerAddressManajemenPIC.listManajemenPICAddress[i].address,
                        "rt": _providerAddressManajemenPIC.listManajemenPICAddress[i].rt,
                        "rw": _providerAddressManajemenPIC.listManajemenPICAddress[i].rw,
                        "provinsiID": _providerAddressManajemenPIC.listManajemenPICAddress[i].kelurahanModel.PROV_ID,
                        "kabkotID": _providerAddressManajemenPIC.listManajemenPICAddress[i].kelurahanModel.KABKOT_ID,
                        "kecamatanID": _providerAddressManajemenPIC.listManajemenPICAddress[i].kelurahanModel.KEC_ID,
                        "kelurahanID": _providerAddressManajemenPIC.listManajemenPICAddress[i].kelurahanModel.KEL_ID,
                        "zipcode": _providerAddressManajemenPIC.listManajemenPICAddress[i].kelurahanModel.ZIPCODE
                    },
                    "contactSpecification": {
                        "telephoneArea1": _providerAddressManajemenPIC.listManajemenPICAddress[i].areaCode,
                        "telephone1": _providerAddressManajemenPIC.listManajemenPICAddress[i].phone,
                        "telephoneArea2": "",
                        "telephone2": "",
                        "faxArea": "",
                        "fax": "",
                        "handphone": "",
                        "email": ""
                    },
                    "addressType": _providerAddressManajemenPIC.listManajemenPICAddress[i].jenisAlamatModel.KODE,
                    "creationalSpecification": {
                        "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                        "createdBy": _preferences.getString("username"),
                        "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                        "modifiedBy": _preferences.getString("username"),
                    },
                    "status": _providerAddressManajemenPIC.listManajemenPICAddress[i].active == 0 ? "ACTIVE" : "INACTIVE",
                    "foreignBusinessID": _preferences.getString("last_known_state") != "IDE" ?
                    _providerAddressManajemenPIC.listManajemenPICAddress[i].foreignBusinessID != "NEW" ?
                    _providerAddressManajemenPIC.listManajemenPICAddress[i].foreignBusinessID
                        : "CUST001" : "CUST001" // MEW
                });
            }
        }

        var _installmentDetails = [];
        if(_providerCreditStructure.installmentTypeSelected.id == "06") {
            // Stepping
            if(_providerCreditStructureType.listInfoCreditStructureStepping.isNotEmpty) {
                for(int i=0; i < _providerCreditStructureType.listInfoCreditStructureStepping.length; i++) {
                    _installmentDetails.add({
                        "installmentID": _preferences.getString("last_known_state") != "IDE" ? null : "NEW",// nanti di bagian if di ambil dr sharedPreference
                        "installmentNumber": 0,
                        "percentage": _providerCreditStructureType.listInfoCreditStructureStepping[i].controllerPercentage.text != "" ? double.parse(_providerCreditStructureType.listInfoCreditStructureStepping[i].controllerPercentage.text.replaceAll(",", "")) : 0,
                        "amount": _providerCreditStructureType.listInfoCreditStructureStepping[i].controllerValue.text != "" ? double.parse(_providerCreditStructureType.listInfoCreditStructureStepping[i].controllerValue.text.replaceAll(",", "")) : 0,
                        "creationalSpecification": {
                            "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                            "createdBy": _preferences.getString("username"),
                            "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                            "modifiedBy": _preferences.getString("username"),
                        },
                        "status": "ACTIVE"
                    });
                }
            }
        }
        else if(_providerCreditStructure.installmentTypeSelected.id == "07") {
            // Irreguler
            if(_providerCreditStructureType.listInfoCreditStructureIrregularModel.isNotEmpty) {
                for(int i=0; i < _providerCreditStructureType.listInfoCreditStructureIrregularModel.length; i++) {
                    _installmentDetails.add({
                        "installmentID": _preferences.getString("last_known_state") != "IDE" ? null : "NEW",// nanti di bagian if di ambil dr sharedPreference
                        "installmentNumber": i+1,
                        "percentage": 0,
                        "amount": _providerCreditStructureType.listInfoCreditStructureIrregularModel[i].controller.text != "" ? double.parse(_providerCreditStructureType.listInfoCreditStructureIrregularModel[i].controller.text.replaceAll(",", "")) : 0,
                        "creationalSpecification": {
                            "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                            "createdBy": _preferences.getString("username"),
                            "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                            "modifiedBy": _preferences.getString("username"),
                        },
                        "status": "ACTIVE"
                    });
                }
            }
        }

        var _guarantorIndividuals = [];
        if(_providerGuarantor.listGuarantorIndividual.isNotEmpty) {
            for(int i=0; i < _providerGuarantor.listGuarantorIndividual.length; i++) {
                List _detailAddressGuarantorIndividu = [];
                String _foreignBKGuarantorIndividu = "IKPG";
                if(_providerGuarantor.listGuarantorIndividual.length < 10) {
                    _foreignBKGuarantorIndividu = "IKPG00" + (i+1).toString();
                } else if(_providerGuarantor.listGuarantorIndividual.length < 100) {
                    _foreignBKGuarantorIndividu = "IKPG0" + (i+1).toString();
                } else if(_providerGuarantor.listGuarantorIndividual.length < 1000) {
                    _foreignBKGuarantorIndividu = "IKPG" + (i+1).toString();
                }
                if(_providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel.isNotEmpty){
                    for(int j=0; j < _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel.length; j++){
                        _detailAddressesInfoNasabah.add({
                            "addressID": _preferences.getString("last_known_state") != "IDE"
                                ? _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].addressID != "NEW"
                                ? _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].addressID : "NEW" : "NEW",
                            "addressSpecification": {
                                "koresponden": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].isCorrespondence ? "1" : "0",
                                "matrixAddr": _preferences.getString("cust_type") == "PER" ? "1" : "7",
                                "address": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].address,
                                "rt": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].rt,
                                "rw": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].rw,
                                "provinsiID": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].kelurahanModel.PROV_ID,
                                "kabkotID": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].kelurahanModel.KABKOT_ID,
                                "kecamatanID": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].kelurahanModel.KEC_ID,
                                "kelurahanID": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].kelurahanModel.KEL_ID,
                                "zipcode": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].kelurahanModel.ZIPCODE
                            },
                            "contactSpecification": {
                                "telephoneArea1": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].areaCode,
                                "telephone1": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].phone,
                                "telephoneArea2": null,
                                "telephone2": null,
                                "faxArea": null,
                                "fax": null,
                                "handphone": "",
                                "email": ""
                            },
                            "addressType": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].jenisAlamatModel.KODE,
                            "creationalSpecification": {
                                "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                                "createdBy": _preferences.getString("username"),
                                "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                                "modifiedBy": _preferences.getString("username"),
                            },
                            "status": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].active == 0 ? "ACTIVE" : "INACTIVE",
                            "foreignBusinessID": _preferences.getString("last_known_state") != "IDE"
                                ? _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].foreignBusinessID != "NEW"
                                ? _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].foreignBusinessID
                                : _foreignBKGuarantorIndividu : _foreignBKGuarantorIndividu
                        });
                        _detailAddressGuarantorIndividu.add({
                            "addressID": _preferences.getString("last_known_state") != "IDE"
                                ? _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].addressID != "NEW"
                                ? _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].addressID : "NEW" : "NEW",
                            "addressSpecification": {
                                "koresponden": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].isCorrespondence ? "1" : "0",
                                "matrixAddr": _preferences.getString("cust_type") == "PER" ? "1" : "7",
                                "address": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].address,
                                "rt": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].rt,
                                "rw": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].rw,
                                "provinsiID": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].kelurahanModel.PROV_ID,
                                "kabkotID": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].kelurahanModel.KABKOT_ID,
                                "kecamatanID": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].kelurahanModel.KEC_ID,
                                "kelurahanID": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].kelurahanModel.KEL_ID,
                                "zipcode": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].kelurahanModel.ZIPCODE
                            },
                            "contactSpecification": {
                                "telephoneArea1": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].areaCode,
                                "telephone1": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].phone,
                                "telephoneArea2": null,
                                "telephone2": null,
                                "faxArea": null,
                                "fax": null,
                                "handphone": "",
                                "email": ""
                            },
                            "addressType": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].jenisAlamatModel.KODE,
                            "creationalSpecification": {
                                "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                                "createdBy": _preferences.getString("username"),
                                "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                                "modifiedBy": _preferences.getString("username"),
                            },
                            "status": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].active == 0 ? "ACTIVE" : "INACTIVE",
                            "foreignBusinessID": _preferences.getString("last_known_state") != "IDE"
                                ? _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].foreignBusinessID != "NEW"
                                ? _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].foreignBusinessID
                                : _foreignBKGuarantorIndividu : _foreignBKGuarantorIndividu
                        });
                    }
                }
                _guarantorIndividuals.add(
                    {
                        "guarantorIndividualID": _preferences.getString("last_known_state") != "IDE"
                            ? _providerGuarantor.listGuarantorIndividual[i].guarantorIndividualID != null
                            ? _providerGuarantor.listGuarantorIndividual[i].guarantorIndividualID : _foreignBKGuarantorIndividu : _foreignBKGuarantorIndividu,
                        "relationshipStatus":_providerGuarantor.listGuarantorIndividual[i].relationshipStatusModel != null ?
                        _providerGuarantor.listGuarantorIndividual[i].relationshipStatusModel.PARA_FAMILY_TYPE_ID : "",
                        "dedupScore": 0.0,
                        "guarantorIdentity": {
                            "identityType": _providerGuarantor.listGuarantorIndividual[i].identityModel != null ?
                            _providerGuarantor.listGuarantorIndividual[i].identityModel.id : "",
                            "identityNumber": _providerGuarantor.listGuarantorIndividual[i].identityNumber,
                            "identityName": _providerGuarantor.listGuarantorIndividual[i].fullNameIdentity,
                            "fullName": _providerGuarantor.listGuarantorIndividual[i].fullName,
                            "alias": "",
                            "title": "",
                            "dateOfBirth": _providerGuarantor.listGuarantorIndividual[i].birthDate != "" ? formatDateValidateAndSubmit(DateTime.parse(_providerGuarantor.listGuarantorIndividual[i].birthDate)) : "01-01-1900 00:00:00",
                            "placeOfBirth": _providerGuarantor.listGuarantorIndividual[i].birthPlaceIdentity1 != "" ? _providerGuarantor.listGuarantorIndividual[i].birthPlaceIdentity1 : "",
                            "placeOfBirthKabKota": _providerGuarantor.listGuarantorIndividual[i].birthPlaceIdentity2 != null ? _providerGuarantor.listGuarantorIndividual[i].birthPlaceIdentity2.KABKOT_ID : "",
                            "gender": _providerGuarantor.listGuarantorIndividual[i].gender,
                            "identityActiveStart": null,
                            "identityActiveEnd": null,
                            "isLifetime": null,
                            "religion": null,
                            "occupationID": null,
                            "positionID": null,
                            "maritalStatusID": null
                        },
                        "guarantorContact": {
                            "telephoneArea1": null,
                            "telephone1": null,
                            "telephoneArea2": null,
                            "telephone2": null,
                            "faxArea": null,
                            "fax": null,
                            "handphone": _providerGuarantor.listGuarantorIndividual[i].cellPhoneNumber != "" && _providerGuarantor.listGuarantorIndividual[i].cellPhoneNumber != null ? "08${_providerGuarantor.listGuarantorIndividual[i].cellPhoneNumber}" : "",
                            "email": null,
                            "noWa": null,
                            "noWa2": null,
                            "noWa3": null
                        },
                        "guarantorIndividualAddresses": _detailAddressGuarantorIndividu,
                        "creationalSpecification": {
                            "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                            "createdBy": _preferences.getString("username"),
                            "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                            "modifiedBy": _preferences.getString("username"),
                        },
                        "status": "ACTIVE"
                    }
                );
            }
        }

        var _guarantorCorporates = [];
        if(_providerGuarantor.listGuarantorCompany.isNotEmpty) {
            for(int i=0; i < _providerGuarantor.listGuarantorCompany.length; i++) {
                String _npwpAddressGuarantorCorporate = "";
                List _detailAddressGuarantorCompany = [];
                String _foreignBKGuarantorCorporate = "IKGK";
                if(_providerGuarantor.listGuarantorCompany.length < 10) {
                    _foreignBKGuarantorCorporate = "IKGK00" + (i+1).toString();
                } else if(_providerGuarantor.listGuarantorCompany.length < 100) {
                    _foreignBKGuarantorCorporate = "IKGK0" + (i+1).toString();
                } else if(_providerGuarantor.listGuarantorCompany.length < 1000) {
                    _foreignBKGuarantorCorporate = "IKGK" + (i+1).toString();
                }
                if(_providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel.isNotEmpty){
                    for(int j=0; j < _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel.length; j++){
                        _detailAddressesInfoNasabah.add({
                            "addressID": _preferences.getString("last_known_state") != "IDE"
                                ? _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].addressID != "NEW"
                                ? _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].addressID : "NEW" : "NEW",
                            "addressSpecification": {
                                "koresponden": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].isCorrespondence ? "1" : "0",
                                "matrixAddr": _preferences.getString("cust_type") == "PER" ? "2" : "8",
                                "address": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].address,
                                "rt": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].rt,
                                "rw": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].rw,
                                "provinsiID": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].kelurahanModel.PROV_ID,
                                "kabkotID": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].kelurahanModel.KABKOT_ID,
                                "kecamatanID": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].kelurahanModel.KEC_ID,
                                "kelurahanID": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].kelurahanModel.KEL_ID,
                                "zipcode": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].kelurahanModel.ZIPCODE
                            },
                            "contactSpecification": {
                                "telephoneArea1": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].phoneArea1,
                                "telephone1": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].phone1,
                                "telephoneArea2": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].phoneArea2,
                                "telephone2": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].phone2,
                                "faxArea": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].faxArea,
                                "fax": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].fax,
                                "handphone": "",
                                "email": ""
                            },
                            "addressType": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].jenisAlamatModel.KODE,
                            "creationalSpecification": {
                                "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                                "createdBy": _preferences.getString("username"),
                                "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                                "modifiedBy": _preferences.getString("username"),
                            },
                            "status": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].active == 0 ? "ACTIVE" : "INACTIVE",
                            "foreignBusinessID": _preferences.getString("last_known_state") != "IDE"
                                ? _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].foreignBusinessID != "NEW"
                                ? _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].foreignBusinessID
                                : _foreignBKGuarantorCorporate : _foreignBKGuarantorCorporate
                        });
                        _detailAddressGuarantorCompany.add({
                            "addressID": _preferences.getString("last_known_state") != "IDE"
                                ? _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].addressID != "NEW"
                                ? _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].addressID : "NEW" : "NEW",
                            "addressSpecification": {
                                "koresponden": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].isCorrespondence ? "1" : "0",
                                "matrixAddr": _preferences.getString("cust_type") == "PER" ? "2" : "8",
                                "address": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].address,
                                "rt": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].rt,
                                "rw": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].rw,
                                "provinsiID": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].kelurahanModel.PROV_ID,
                                "kabkotID": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].kelurahanModel.KABKOT_ID,
                                "kecamatanID": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].kelurahanModel.KEC_ID,
                                "kelurahanID": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].kelurahanModel.KEL_ID,
                                "zipcode": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].kelurahanModel.ZIPCODE
                            },
                            "contactSpecification": {
                                "telephoneArea1": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].phoneArea1,
                                "telephone1": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].phone1,
                                "telephoneArea2": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].phoneArea2,
                                "telephone2": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].phone2,
                                "faxArea": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].faxArea,
                                "fax": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].fax,
                                "handphone": "",
                                "email": ""
                            },
                            "addressType": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].jenisAlamatModel.KODE,
                            "creationalSpecification": {
                                "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                                "createdBy": _preferences.getString("username"),
                                "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                                "modifiedBy": _preferences.getString("username"),
                            },
                            "status": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].active == 0 ? "ACTIVE" : "INACTIVE",
                            "foreignBusinessID": _preferences.getString("last_known_state") != "IDE" ?
                            _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].foreignBusinessID != "NEW" ?
                            _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].foreignBusinessID
                                : _foreignBKGuarantorCorporate : _foreignBKGuarantorCorporate
                        });
                        if(_providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].jenisAlamatModel.KODE == "03") {
                            _npwpAddressGuarantorCorporate = _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].address;
                        }
                    }
                }
                _guarantorCorporates.add(
                    {
                        "guarantorCorporateID": _preferences.getString("last_known_state") != "IDE"
                            ? _providerGuarantor.listGuarantorCompany[i].guarantorCorporateID != null
                            ? _providerGuarantor.listGuarantorCompany[i].guarantorCorporateID : _foreignBKGuarantorCorporate : _foreignBKGuarantorCorporate,
                        "dedupScore": 0.0,
                        "businessPermitSpecification": {
                            "institutionType": _providerGuarantor.listGuarantorCompany[i].typeInstitutionModel.PARA_ID,
                            "profile": _preferences.getString("cust_type") == "PER" ? _providerGuarantor.listGuarantorCompany[i].profilModel.id : "",
                            "fullName": _providerGuarantor.listGuarantorCompany[i].institutionName,
                            "deedOfIncorporationNumber": null,
                            "deedEndDate": null,
                            "siupNumber": null,
                            "siupStartDate": null,
                            "tdpNumber": null,
                            "tdpStartDate": null,
                            "establishmentDate": formatDateValidateAndSubmit(DateTime.parse(_providerGuarantor.listGuarantorCompany[i].establishDate)),//"${_providerGuarantor.listGuarantorCompany[i].establishDate} 00:00:00",
                            "isCompany": false,
                            "authorizedCapital": 0,
                            "paidCapital": 0
                        },
                        "businessSpecification": {
                            "economySector": null,
                            "businessField": null,
                            "locationStatus": null,
                            "businessLocation": null,
                            "employeeTotal": 0,
                            "bussinessLengthInMonth": 0,
                            "totalBussinessLengthInMonth": 0
                        },
                        "customerNpwpSpecification": {
                            "hasNpwp": true,
                            "npwpAddress": _npwpAddressGuarantorCorporate,
                            "npwpFullname": _providerGuarantor.listGuarantorCompany[i].institutionName,
                            "npwpNumber": _providerGuarantor.listGuarantorCompany[i].npwp,
                            "npwpType": _npwpType,
                            "pkpIdentifier": _providerRincian.signPKPSelected != null ? _providerRincian.signPKPSelected.id : "2"
                        },
                        "guarantorCorporateAddresses": _detailAddressGuarantorCompany,
                        "creationalSpecification": {
                            "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                            "createdBy": _preferences.getString("username"),
                            "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                            "modifiedBy": _preferences.getString("username"),
                        },
                        "status": "ACTIVE"
                    }
                );
            }
        }

        var _orderKaroseris = [];
        if(_providerKaroseri.listFormKaroseriObject.isNotEmpty) {
            for(int i=0; i < _providerKaroseri.listFormKaroseriObject.length; i++) {
                _orderKaroseris.add({
                        "orderKaroseriID": _preferences.getString("last_known_state") != "IDE" ? _providerKaroseri.listFormKaroseriObject[i].orderKaroseriID : "NEW",
                        "isPksKaroseri": _providerKaroseri.listFormKaroseriObject[i].PKSKaroseri,
                        "karoseriCompanyID": _providerKaroseri.listFormKaroseriObject[i].company != null ? _providerKaroseri.listFormKaroseriObject[i].company.id : null,
                        "karoseriID": _providerKaroseri.listFormKaroseriObject[i].karoseri.kode,
                        "karoseriPrice": double.parse(_providerKaroseri.listFormKaroseriObject[i].price.replaceAll(",", "")),
                        "karoseriQty": _providerKaroseri.listFormKaroseriObject[i].jumlahKaroseri,
                        "karoseriTotalPrice": double.parse(_providerKaroseri.listFormKaroseriObject[i].totalPrice.replaceAll(",", "")),
                        "uangMukaKaroseri": 0,
                        "flagKaroseri": 1,
                        "creationalSpecification": {
                            "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                            "createdBy": _preferences.getString("username"),
                            "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                            "modifiedBy": _preferences.getString("username"),
                        },
                        "status": "ACTIVE"
                    });
            }
        }

        var _orderWmps = [];
        print("cek wmp ${_providerWmp.listWMP.isNotEmpty}");
        if(_providerWmp.listWMP.isNotEmpty) {
            print("masuk if");
            for(int i=0; i < _providerWmp.listWMP.length; i++) {
                _orderWmps.add(
                    {
                        "orderWmpID": _preferences.getString("last_known_state") != "IDE" ? _providerWmp.listWMP[i].orderWmpID : "NEW",
                        "wmpNumber": _providerWmp.listWMP[i].noProposal,
                        "wmpType": _providerWmp.listWMP[i].type,
                        "wmpJob": "",
                        "wmpSubsidyTypeID": _providerWmp.listWMP[i].typeSubsidi,
                        "maxRefundAmount": _providerWmp.listWMP[i].amount,
                        "status": "ACTIVE",
                        "creationalSpecification": {
                            "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                            "createdBy": _preferences.getString("username"),
                            "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                            "modifiedBy": _preferences.getString("username"),
                        },
                    },
                );
            }
        }

        var _orderSubsidies = [];
        var _orderSubsidyDetails = [];
        if(_providerSubsidy.listInfoCreditSubsidy.isNotEmpty) {
            for(int i=0; i < _providerSubsidy.listInfoCreditSubsidy.length; i++) {
                for(int j=0; j < _providerSubsidy.listInfoCreditSubsidy[i].installmentDetail.length; j++) {
                    _orderSubsidyDetails.add(
                        {
                            "orderSubsidyDetailID": _preferences.getString("last_known_state") != "IDE" ? _providerSubsidy.listInfoCreditSubsidy[i].installmentDetail[j].orderSubsidyDetailID : "NEW",
                            "installmentNumber": _providerSubsidy.listInfoCreditSubsidy[i].installmentDetail[j].installmentIndex != null ? double.parse(_providerSubsidy.listInfoCreditSubsidy[i].installmentDetail[j].installmentIndex.replaceAll(",", "")) : 0,
                            "installmentAmount": _providerSubsidy.listInfoCreditSubsidy[i].installmentDetail[j].installmentSubsidy != null ? double.parse(_providerSubsidy.listInfoCreditSubsidy[i].installmentDetail[j].installmentSubsidy.replaceAll(",", "")) : 0,
                            "creationalSpecification": {
                                "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                                "createdBy": _preferences.getString("username"),
                                "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                                "modifiedBy": _preferences.getString("username"),
                            },
                            "status": "ACTIVE"
                        }
                    );
                }
                _orderSubsidies.add(
                    {
                        "orderSubsidyID": _preferences.getString("last_known_state") != "IDE" ? _providerSubsidy.listInfoCreditSubsidy[i].orderSubsidyID : "NEW",
                        "subsidyProviderID": _providerSubsidy.listInfoCreditSubsidy[i].giver,
                        "subsidyTypeID": _providerSubsidy.listInfoCreditSubsidy[i].type != null ? _providerSubsidy.listInfoCreditSubsidy[i].type.id : "",
                        "subsidyMethodTypeID": _providerSubsidy.listInfoCreditSubsidy[i].cuttingMethod != null ? _providerSubsidy.listInfoCreditSubsidy[i].cuttingMethod.id : "02",
                        "refundAmount": _providerSubsidy.listInfoCreditSubsidy[i].value != ""
                            ? double.parse(_providerSubsidy.listInfoCreditSubsidy[i].value.replaceAll(",", "")) : 0,
                        "refundAmountKlaim": _providerSubsidy.listInfoCreditSubsidy[i].claimValue != "" ?
                        double.parse(_providerSubsidy.listInfoCreditSubsidy[i].claimValue.replaceAll(",", "")) : 0, //Nilai Klaim
                        "effectiveRate": _providerSubsidy.listInfoCreditSubsidy[i].rateBeforeEff != ""
                            ? double.parse(_providerSubsidy.listInfoCreditSubsidy[i].rateBeforeEff.replaceAll(",", "")) : 0,
                        "flatRate":_providerSubsidy.listInfoCreditSubsidy[i].rateBeforeFlat != "" ?
                        double.parse(_providerSubsidy.listInfoCreditSubsidy[i].rateBeforeFlat.replaceAll(",", "")) : 0,
                        "dpReal": 0,
                        "totalInstallment": _providerSubsidy.listInfoCreditSubsidy[i].totalInstallment != "" && _providerSubsidy.listInfoCreditSubsidy[i].totalInstallment != null
                            ? _providerSubsidy.listInfoCreditSubsidy[i].totalInstallment.replaceAll(",", "") : "",
                        "interestRate": 0,
                        "creationalSpecification": {
                            "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                            "createdBy": _preferences.getString("username"),
                            "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                            "modifiedBy": _preferences.getString("username"),
                        },
                        "status": "ACTIVE",
                        "orderSubsidyDetails": _orderSubsidyDetails
                    }
                );
            }
        }

        final ioc = new HttpClient();
        ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;

        final _http = IOClient(ioc);

        var _orderProductSaleses = [];
        if(_providerSales.listInfoSales.isNotEmpty) {
            for(int i=0; i<_providerSales.listInfoSales.length; i++){
                _orderProductSaleses.add({
                    "orderProductSalesID": _preferences.getString("last_known_state") != "IDE" ? _providerSales.listInfoSales[i].orderProductSalesID : "NEW",
                    "salesType": _providerSales.listInfoSales[i].salesmanTypeModel.id,
                    "employeeJob": _providerSales.listInfoSales[i].positionModel.id == "" ? null : _providerSales.listInfoSales[i].positionModel.id,
                    "employeeID": _providerSales.listInfoSales[i].employeeModel == null ? '' : _providerSales.listInfoSales[i].employeeModel.NIK,
                    "employeeHeadID": _providerSales.listInfoSales[i].employeeHeadModel == null ? '' : _providerSales.listInfoSales[i].employeeHeadModel.NIK,
                    "referalContractNumber": null,
                    "creationalSpecification": {
                        "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                        "createdBy": _preferences.getString("username"),
                        "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                        "modifiedBy": _preferences.getString("username"),
                    },
                    "status": "ACTIVE"
                });
            }
        }

        var _orderProductInsurances = [];
        String _collateralIDMajorInsurance = "";
        if(_providerKolateral.collateralTypeModel.id == "001") {
            _collateralIDMajorInsurance = "CAU001";
        } else if(_providerKolateral.collateralTypeModel.id == "002") {
            _collateralIDMajorInsurance = "COLP001";
        } else {
            _collateralIDMajorInsurance = "";
        }
        if(_providerInsurance.listFormMajorInsurance.isNotEmpty) {
            for(int i=0; i<_providerInsurance.listFormMajorInsurance.length; i++) {
                _orderProductInsurances.add({
                    "orderProductInsuranceID": _preferences.getString("last_known_state") != "IDE" ? _providerInsurance.listFormMajorInsurance[i].orderProductInsuranceID : "NEW",
                    "insuranceCollateralReferenceSpecification": {
                        "collateralID": _preferences.getString("last_known_state") != "IDE" ? _providerKolateral.collateralID : _collateralIDMajorInsurance, // NEW
                        "collateralTypeID": _providerKolateral.collateralTypeModel.id
                    },
                    "insuranceTypeID": _providerInsurance.listFormMajorInsurance[i].insuranceType.KODE,
                    "insuranceCompanyID": _providerInsurance.listFormMajorInsurance[i].company  == null ? "02" : _providerInsurance.listFormMajorInsurance[i].company.KODE, // field perusahaan
                    "insuranceProductID": _providerInsurance.listFormMajorInsurance[i].product != null ? _providerInsurance.listFormMajorInsurance[i].product.KODE : "", // field produk
                    "period": _providerInsurance.listFormMajorInsurance[i].periodType == "" ? null : int.parse(_providerInsurance.listFormMajorInsurance[i].periodType),
                    "type": _providerInsurance.listFormMajorInsurance[i].type,
                    "type1": _providerInsurance.listFormMajorInsurance[i].coverage1 != null ? _providerInsurance.listFormMajorInsurance[i].coverage1.KODE :"", // field coverage 1
                    "type2": _providerInsurance.listFormMajorInsurance[i].coverage2 != null ? _providerInsurance.listFormMajorInsurance[i].coverage2.KODE : "", // field coverage 2
                    "subType": _providerInsurance.listFormMajorInsurance[i].subType != null ? _providerInsurance.listFormMajorInsurance[i].subType.KODE :"", // field coverage 1
                    "subTypeCoverage": _providerInsurance.listFormMajorInsurance[i].coverageType == null ? null : _providerInsurance.listFormMajorInsurance[i].coverageType.KODE, // field sub type
                    "insuranceSourceTypeID": _providerInsurance.listFormMajorInsurance[i].coverageType != null ? _providerInsurance.listFormMajorInsurance[i].coverageType.KODE : "", // field jenis pertanggungan
                    "insuranceValue": _providerInsurance.listFormMajorInsurance[i].coverageValue.isEmpty ? 0 : double.parse(_providerInsurance.listFormMajorInsurance[i].coverageValue.replaceAll(",", "")).round(),
                    "upperLimitPercentage": _providerInsurance.listFormMajorInsurance[i].upperLimitRate == "" ? 0.0 : double.parse(_providerInsurance.listFormMajorInsurance[i].upperLimitRate),
                    "upperLimitAmount": _providerInsurance.listFormMajorInsurance[i].upperLimit == "" ? 0 : double.parse(_providerInsurance.listFormMajorInsurance[i].upperLimit.replaceAll(",", "")),
                    "lowerLimitPercentage": _providerInsurance.listFormMajorInsurance[i].lowerLimitRate == "" ? 0 : double.parse(_providerInsurance.listFormMajorInsurance[i].lowerLimitRate),
                    "lowerLimitAmount": _providerInsurance.listFormMajorInsurance[i].lowerLimit == "" ? 0 : double.parse(_providerInsurance.listFormMajorInsurance[i].lowerLimit.replaceAll(",", "")),
                    "insuranceCashFee": _providerInsurance.listFormMajorInsurance[i].priceCash == "" ? 0 : double.parse(_providerInsurance.listFormMajorInsurance[i].priceCash.replaceAll(",", "")).round(),
                    "insuranceCreditFee": _providerInsurance.listFormMajorInsurance[i].priceCredit == "" ? 0 : double.parse(_providerInsurance.listFormMajorInsurance[i].priceCredit.replaceAll(",", "")).round(),
                    "totalSplitFee": _providerInsurance.listFormMajorInsurance[i].totalPriceSplit == "" ? 0 : double.parse(_providerInsurance.listFormMajorInsurance[i].totalPriceSplit.replaceAll(",", "")),
                    "totalInsuranceFeePercentage": _providerInsurance.listFormMajorInsurance[i].totalPriceRate == "" ? 0 : double.parse(_providerInsurance.listFormMajorInsurance[i].totalPriceRate),
                    "totalInsuranceFeeAmount": _providerInsurance.listFormMajorInsurance[i].totalPrice == "" ? 0 : double.parse(_providerInsurance.listFormMajorInsurance[i].totalPrice.replaceAll(",", "")),
                    "creationalSpecification": {
                        "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                        "createdBy": _preferences.getString("username"),
                        "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                        "modifiedBy": _preferences.getString("username"),
                    },
                    "status": "ACTIVE",
                    "insurancePaymentType": null
                });
            }
        }

        if(_providerAdditionalInsurance.listFormAdditionalInsurance.isNotEmpty) {
            debugPrint("CHECK INSURANCE ${_providerAdditionalInsurance.listFormAdditionalInsurance.length}");
            for(int i=0; i<_providerAdditionalInsurance.listFormAdditionalInsurance.length; i++) {
                _orderProductInsurances.add({
                    "orderProductInsuranceID": _preferences.getString("last_known_state") != "IDE"
                        ? _providerAdditionalInsurance.listFormAdditionalInsurance[i].orderProductInsuranceID : "NEW",
                    "insuranceCollateralReferenceSpecification": {
                        "collateralID": _preferences.getString("last_known_state") != "IDE" ? _providerKolateral.collateralID : "", // NEW
                        "collateralTypeID": _providerKolateral.collateralTypeModel.id
                    },
                    "insuranceTypeID": _providerAdditionalInsurance.listFormAdditionalInsurance[i].insuranceType.KODE,
                    "insuranceCompanyID": _providerAdditionalInsurance.listFormAdditionalInsurance[i].company  == null ? "02" : _providerAdditionalInsurance.listFormAdditionalInsurance[i].company.KODE,
                    "insuranceProductID": _providerAdditionalInsurance.listFormAdditionalInsurance[i].product.KODE,
                    "period": _providerAdditionalInsurance.listFormAdditionalInsurance[i].periodType == "" ? null : int.parse(_providerAdditionalInsurance.listFormAdditionalInsurance[i].periodType),
                    "type": "",
                    "type1": "",
                    "type2": "",
                    "subType": "",
                    "subTypeCoverage": "",
                    "insuranceSourceTypeID": _providerAdditionalInsurance.listFormAdditionalInsurance[i].coverageType != null ? _providerAdditionalInsurance.listFormAdditionalInsurance[i].coverageType.KODE : "", // field jenis pertanggungan
                    "insuranceValue": _providerAdditionalInsurance.listFormAdditionalInsurance[i].coverageValue.isEmpty ? 0 : double.parse(_providerAdditionalInsurance.listFormAdditionalInsurance[i].coverageValue.replaceAll(",", "")).round(),
                    "upperLimitPercentage": _providerAdditionalInsurance.listFormAdditionalInsurance[i].upperLimitRate == "" ? 0.0 : double.parse(_providerAdditionalInsurance.listFormAdditionalInsurance[i].upperLimitRate),
                    "upperLimitAmount": _providerAdditionalInsurance.listFormAdditionalInsurance[i].upperLimit == "" ? 0 : double.parse(_providerAdditionalInsurance.listFormAdditionalInsurance[i].upperLimit.replaceAll(",", "")),
                    "lowerLimitPercentage": _providerAdditionalInsurance.listFormAdditionalInsurance[i].lowerLimitRate == "" ? 0 : double.parse(_providerAdditionalInsurance.listFormAdditionalInsurance[i].lowerLimitRate),
                    "lowerLimitAmount": _providerAdditionalInsurance.listFormAdditionalInsurance[i].lowerLimit == "" ? 0 : double.parse(_providerAdditionalInsurance.listFormAdditionalInsurance[i].lowerLimit.replaceAll(",", "")),
                    "insuranceCashFee": _providerAdditionalInsurance.listFormAdditionalInsurance[i].priceCash == "" ? 0 : double.parse(_providerAdditionalInsurance.listFormAdditionalInsurance[i].priceCash.replaceAll(",", "")).round(),
                    "insuranceCreditFee": _providerAdditionalInsurance.listFormAdditionalInsurance[i].priceCredit == "" ? 0 : double.parse(_providerAdditionalInsurance.listFormAdditionalInsurance[i].priceCredit.replaceAll(",", "")).round(),
                    "totalSplitFee": "",
                    "totalInsuranceFeePercentage": _providerAdditionalInsurance.listFormAdditionalInsurance[i].totalPriceRate == "" ? 0 : double.parse(_providerAdditionalInsurance.listFormAdditionalInsurance[i].totalPriceRate),
                    "totalInsuranceFeeAmount": _providerAdditionalInsurance.listFormAdditionalInsurance[i].totalPrice == "" ? 0 : double.parse(_providerAdditionalInsurance.listFormAdditionalInsurance[i].totalPrice.replaceAll(",", "")),
                    "creationalSpecification": {
                        "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                        "createdBy": _preferences.getString("username"),
                        "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                        "modifiedBy": _preferences.getString("username"),
                    },
                    "status": "ACTIVE",
                    "insurancePaymentType": null
                });
            }
        }

        var _orderFees = [];
        if(_providerCreditStructure.listInfoFeeCreditStructure.isNotEmpty) {
            bool _isProvisiEmpty = true;
            bool _isAdminEmpty = true;
            for (int i = 0; i <  _providerCreditStructure.listInfoFeeCreditStructure.length; i++) {
                if(_providerCreditStructure.listInfoFeeCreditStructure[i].feeTypeModel.id == "01"){
                    _isAdminEmpty = false;
                }
                if(_providerCreditStructure.listInfoFeeCreditStructure[i].feeTypeModel.id == "02"){
                    _isProvisiEmpty = false;
                }
                _orderFees.add({
                    "orderFeeID": _preferences.getString("last_known_state") != "IDE" ? _providerCreditStructure.listInfoFeeCreditStructure[i].orderFeeID : "NEW",
                    "feeTypeID": _providerCreditStructure.listInfoFeeCreditStructure[i].feeTypeModel.id,
                    "cashFee": _providerCreditStructure.listInfoFeeCreditStructure[i].cashCost == "" ? null : double.parse(_providerCreditStructure.listInfoFeeCreditStructure[i].cashCost.replaceAll(",", "")).round(),
                    "creditFee": _providerCreditStructure.listInfoFeeCreditStructure[i].creditCost == "" ? null : double.parse(_providerCreditStructure.listInfoFeeCreditStructure[i].creditCost.replaceAll(",", "")).round(),
                    "totalFee": _providerCreditStructure.listInfoFeeCreditStructure[i].totalCost == "" ? null : double.parse(_providerCreditStructure.listInfoFeeCreditStructure[i].totalCost.replaceAll(",", "")).round(),
                    "creationalSpecification": {
                        "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                        "createdBy": _preferences.getString("username"),
                        "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                        "modifiedBy": _preferences.getString("username"),
                    },
                    "status": "ACTIVE"
                });
            }

            if(_isAdminEmpty){
                _orderFees.add({
                    "orderFeeID": "NEW",
                    "feeTypeID": "01",
                    "cashFee": 0,
                    "creditFee": 0,
                    "totalFee": 0,
                    "creationalSpecification": {
                        "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                        "createdBy": _preferences.getString("username"),
                        "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                        "modifiedBy": _preferences.getString("username"),
                    },
                    "status": "ACTIVE"
                });
            }

            if(_isProvisiEmpty){
                _orderFees.add({
                    "orderFeeID": "NEW",
                    "feeTypeID": "02",
                    "cashFee": 0,
                    "creditFee": 0,
                    "totalFee": 0,
                    "creationalSpecification": {
                        "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                        "createdBy": _preferences.getString("username"),
                        "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                        "modifiedBy": _preferences.getString("username"),
                    },
                    "status": "ACTIVE"
                });
            }
        }
        else{
            _orderFees.add({
                "orderFeeID": "NEW",
                "feeTypeID": "01",
                "cashFee": 0,
                "creditFee": 0,
                "totalFee": 0,
                "creationalSpecification": {
                    "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                    "createdBy": _preferences.getString("username"),
                    "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                    "modifiedBy": _preferences.getString("username"),
                },
                "status": "ACTIVE"
            });
            _orderFees.add({
                "orderFeeID": "NEW",
                "feeTypeID": "02",
                "cashFee": 0,
                "creditFee": 0,
                "totalFee": 0,
                "creationalSpecification": {
                    "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                    "createdBy": _preferences.getString("username"),
                    "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                    "modifiedBy": _preferences.getString("username"),
                },
                "status": "ACTIVE"
            });
        }

        List _pemegangSahamPribadi = [];
        if(_providerPemegangSaham.listPemegangSahamPribadi.isNotEmpty){
            for(int i=0; i< _providerPemegangSaham.listPemegangSahamPribadi.length; i++){
                debugPrint("CEK SHARE_HOLDER_ID ${_providerPemegangSaham.listPemegangSahamPribadi[i].shareholdersIndividualID}");
                List _detailAddressShareHolderIndividu = [];
                String _foreignBKPemegangSahamPribadi = "KSP";
                if(_providerGuarantor.listGuarantorCompany.length < 10) {
                    _foreignBKPemegangSahamPribadi = "KSP00" + (i+1).toString();
                } else if(_providerGuarantor.listGuarantorCompany.length < 100) {
                    _foreignBKPemegangSahamPribadi = "KSP0" + (i+1).toString();
                } else if(_providerGuarantor.listGuarantorCompany.length < 1000) {
                    _foreignBKPemegangSahamPribadi = "KSP" + (i+1).toString();
                }
                for(int j =0; j < _providerPemegangSaham.listPemegangSahamPribadi[i].listAddress.length; j++){
                    _detailAddressShareHolderIndividu.add({
                        "addressID": _preferences.getString("last_known_state") != "IDE"
                            ? _providerPemegangSaham.listPemegangSahamPribadi[i].listAddress[j].addressID != "NEW"
                            ? _providerPemegangSaham.listPemegangSahamPribadi[i].listAddress[j].addressID : "NEW" : "NEW",
                        "addressSpecification": {
                            "koresponden": _providerPemegangSaham.listPemegangSahamPribadi[i].listAddress[j].isCorrespondence ? "1" : "0",
                            "matrixAddr": "11",
                            "address": _providerPemegangSaham.listPemegangSahamPribadi[i].listAddress[j].address,
                            "rt": _providerPemegangSaham.listPemegangSahamPribadi[i].listAddress[j].rt,
                            "rw": _providerPemegangSaham.listPemegangSahamPribadi[i].listAddress[j].rw,
                            "provinsiID": _providerPemegangSaham.listPemegangSahamPribadi[i].listAddress[j].kelurahanModel.PROV_ID,
                            "kabkotID": _providerPemegangSaham.listPemegangSahamPribadi[i].listAddress[j].kelurahanModel.KABKOT_ID,
                            "kecamatanID": _providerPemegangSaham.listPemegangSahamPribadi[i].listAddress[j].kelurahanModel.KEC_ID,
                            "kelurahanID": _providerPemegangSaham.listPemegangSahamPribadi[i].listAddress[j].kelurahanModel.KEL_ID,
                            "zipcode": _providerPemegangSaham.listPemegangSahamPribadi[i].listAddress[j].kelurahanModel.ZIPCODE
                        },
                        "contactSpecification": {
                            "telephoneArea1": _providerPemegangSaham.listPemegangSahamPribadi[i].listAddress[j].areaCode,
                            "telephone1": _providerPemegangSaham.listPemegangSahamPribadi[i].listAddress[j].phone,
                            "telephoneArea2": "",
                            "telephone2": "",
                            "faxArea": "",
                            "fax": "",
                            "handphone": "",
                            "email": ""
                        },
                        "addressType": _providerPemegangSaham.listPemegangSahamPribadi[i].listAddress[j].jenisAlamatModel.KODE,
                        "creationalSpecification": {
                            "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                            "createdBy": _preferences.getString("username"),
                            "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                            "modifiedBy": _preferences.getString("username"),
                        },
                        "status": _providerPemegangSaham.listPemegangSahamPribadi[i].listAddress[j].active == 0 ? "ACTIVE" : "INACTIVE",
                        "foreignBusinessID": _preferences.getString("last_known_state") != "IDE" ?
                        _providerPemegangSaham.listPemegangSahamPribadi[i].listAddress[j].foreignBusinessID != "NEW" ?
                        _providerPemegangSaham.listPemegangSahamPribadi[i].listAddress[j].foreignBusinessID
                            : _foreignBKPemegangSahamPribadi : _foreignBKPemegangSahamPribadi
                    });
                    _detailAddressesInfoNasabah.add({
                        "addressID": _preferences.getString("last_known_state") != "IDE"
                            ? _providerPemegangSaham.listPemegangSahamPribadi[i].listAddress[j].addressID != "NEW"
                            ? _providerPemegangSaham.listPemegangSahamPribadi[i].listAddress[j].addressID : "NEW" : "NEW",
                        "addressSpecification": {
                            "koresponden": _providerPemegangSaham.listPemegangSahamPribadi[i].listAddress[j].isCorrespondence ? "1" : "0",
                            "matrixAddr": "11",
                            "address": _providerPemegangSaham.listPemegangSahamPribadi[i].listAddress[j].address,
                            "rt": _providerPemegangSaham.listPemegangSahamPribadi[i].listAddress[j].rt,
                            "rw": _providerPemegangSaham.listPemegangSahamPribadi[i].listAddress[j].rw,
                            "provinsiID": _providerPemegangSaham.listPemegangSahamPribadi[i].listAddress[j].kelurahanModel.PROV_ID,
                            "kabkotID": _providerPemegangSaham.listPemegangSahamPribadi[i].listAddress[j].kelurahanModel.KABKOT_ID,
                            "kecamatanID": _providerPemegangSaham.listPemegangSahamPribadi[i].listAddress[j].kelurahanModel.KEC_ID,
                            "kelurahanID": _providerPemegangSaham.listPemegangSahamPribadi[i].listAddress[j].kelurahanModel.KEL_ID,
                            "zipcode": _providerPemegangSaham.listPemegangSahamPribadi[i].listAddress[j].kelurahanModel.ZIPCODE
                        },
                        "contactSpecification": {
                            "telephoneArea1": _providerPemegangSaham.listPemegangSahamPribadi[i].listAddress[j].areaCode,
                            "telephone1": _providerPemegangSaham.listPemegangSahamPribadi[i].listAddress[j].phone,
                            "telephoneArea2": "",
                            "telephone2": "",
                            "faxArea": "",
                            "fax": "",
                            "handphone": "",
                            "email": ""
                        },
                        "addressType": _providerPemegangSaham.listPemegangSahamPribadi[i].listAddress[j].jenisAlamatModel.KODE,
                        "creationalSpecification": {
                            "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                            "createdBy": _preferences.getString("username"),
                            "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                            "modifiedBy": _preferences.getString("username"),
                        },
                        "status": _providerPemegangSaham.listPemegangSahamPribadi[i].listAddress[j].active == 0 ? "ACTIVE" : "INACTIVE",
                        "foreignBusinessID": _preferences.getString("last_known_state") != "IDE" ?
                        _providerPemegangSaham.listPemegangSahamPribadi[i].listAddress[j].foreignBusinessID != "NEW" ?
                        _providerPemegangSaham.listPemegangSahamPribadi[i].listAddress[j].foreignBusinessID :
                        _foreignBKPemegangSahamPribadi : _foreignBKPemegangSahamPribadi
                    });
                }
                _pemegangSahamPribadi.add(
                    {
                        "shareholdersIndividualID": _preferences.getString("last_known_state") != "IDE"
                            ? _providerPemegangSaham.listPemegangSahamPribadi[i].shareholdersIndividualID != null
                            ? _providerPemegangSaham.listPemegangSahamPribadi[i].shareholdersIndividualID : _foreignBKPemegangSahamPribadi : _foreignBKPemegangSahamPribadi,
                        "shareholding": _providerPemegangSaham.listPemegangSahamPribadi[i].percentShare != "" ? double.parse(_providerPemegangSaham.listPemegangSahamPribadi[i].percentShare.replaceAll(",", "")) : 0,
                        "dedupScore": 0,
                        "relationshipStatus": _providerPemegangSaham.listPemegangSahamPribadi[i].relationshipStatusSelected != null ? _providerPemegangSaham.listPemegangSahamPribadi[i].relationshipStatusSelected.PARA_FAMILY_TYPE_ID : "",
                        "shareholdersIndividualIdentity": {
                            "identityType": _providerPemegangSaham.listPemegangSahamPribadi[i].typeIdentitySelected != null ? _providerPemegangSaham.listPemegangSahamPribadi[i].typeIdentitySelected.id : "",
                            "identityNumber": _providerPemegangSaham.listPemegangSahamPribadi[i].identityNumber != "" ? _providerPemegangSaham.listPemegangSahamPribadi[i].identityNumber : "",
                            "identityName": _providerPemegangSaham.listPemegangSahamPribadi[i].fullNameIdentity != "" ? _providerPemegangSaham.listPemegangSahamPribadi[i].fullNameIdentity : "",
                            "fullName": _providerPemegangSaham.listPemegangSahamPribadi[i].fullName != "" ? _providerPemegangSaham.listPemegangSahamPribadi[i].fullName : "",
                            "alias": null,
                            "title": null,
                            "dateOfBirth": _providerPemegangSaham.listPemegangSahamPribadi[i].birthOfDate != "" ? formatDateValidateAndSubmit(DateTime.parse(_providerPemegangSaham.listPemegangSahamPribadi[i].birthOfDate)) : "01-01-1900 00:00:00",
                            "placeOfBirth": _providerPemegangSaham.listPemegangSahamPribadi[i].placeOfBirthIdentity != "" ? _providerPemegangSaham.listPemegangSahamPribadi[i].placeOfBirthIdentity : "",
                            "placeOfBirthKabKota": _providerPemegangSaham.listPemegangSahamPribadi[i].birthPlaceIdentityLOV != null ? _providerPemegangSaham.listPemegangSahamPribadi[i].birthPlaceIdentityLOV.KABKOT_ID : "",
                            "gender": _providerPemegangSaham.listPemegangSahamPribadi[i].gender,
                            "identityActiveStart": null,
                            "identityActiveEnd": null,
                            "religion": null,
                            "occupationID": "0",
                            "positionID": null,
                            "maritalStatusID": ""
                        },
                        "shareholderIndividualContact": {
                            "telephoneArea1": null,
                            "telephone1": null,
                            "telephoneArea2": null,
                            "telephone2": null,
                            "faxArea": null,
                            "fax": null,
                            "handphone": null,
                            "email": null
                        },
                        "shareholderIndividualAddresses": _detailAddressShareHolderIndividu,
                        "creationalSpecification": {
                            "createdAt":  formatDateValidateAndSubmit(DateTime.now()),
                            "createdBy": _preferences.getString("username"),
                            "modifiedAt":  formatDateValidateAndSubmit(DateTime.now()),
                            "modifiedBy": _preferences.getString("username"),
                        },
                        "status": "ACTIVE"
                    }
                );
            }
        }
        debugPrint("shareholderIndividualAddresses $_pemegangSahamPribadi");

        List _shareholdersCorporates = [];
        if(_providerPemegangSahamCompany.listPemegangSahamLembaga.isNotEmpty){
            for(int i=0; i < _providerPemegangSahamCompany.listPemegangSahamLembaga.length; i++){
                debugPrint("CEK SHARE_HOLDER_ID_COM ${_providerPemegangSaham.listPemegangSahamPribadi[i].shareholdersIndividualID}");
                debugPrint("CEK_EST_DATE_SHARE_COM ${_providerPemegangSahamCompany.listPemegangSahamLembaga[i].initialDateForDateOfEstablishment}");
                List _detailAddressShareHolderCompany = [];
                String _foreignBKPemegangSahamCompany = "KSK";
                if(_providerGuarantor.listGuarantorCompany.length < 10) {
                    _foreignBKPemegangSahamCompany = "KSK00" + (i+1).toString();
                } else if(_providerGuarantor.listGuarantorCompany.length < 100) {
                    _foreignBKPemegangSahamCompany = "KSK0" + (i+1).toString();
                } else if(_providerGuarantor.listGuarantorCompany.length < 1000) {
                    _foreignBKPemegangSahamCompany = "KSK" + (i+1).toString();
                }
                for(int j = 0; j < _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress.length; j++){
                    _detailAddressShareHolderCompany.add({
                        "addressID": _preferences.getString("last_known_state") != "IDE" ? _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].addressID != "NEW" ? _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].addressID : "NEW" : "NEW",
                        "addressSpecification": {
                            "koresponden": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].isCorrespondence ? "1" : "0",
                            "matrixAddr": "12",
                            "address": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].address,
                            "rt": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].rt,
                            "rw": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].rw,
                            "provinsiID": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].kelurahanModel.PROV_ID,
                            "kabkotID": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].kelurahanModel.KABKOT_ID,
                            "kecamatanID": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].kelurahanModel.KEC_ID,
                            "kelurahanID": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].kelurahanModel.KEL_ID,
                            "zipcode": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].kelurahanModel.ZIPCODE
                        },
                        "contactSpecification": {
                            "telephoneArea1": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].phoneArea1,
                            "telephone1": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].phone1,
                            "telephoneArea2":_providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].phoneArea2,
                            "telephone2": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].phone2,
                            "faxArea": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].faxArea,
                            "fax": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].fax,
                            "handphone": "",
                            "email": ""
                        },
                        "addressType": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].jenisAlamatModel.KODE,
                        "creationalSpecification": {
                            "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                            "createdBy": _preferences.getString("username"),
                            "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                            "modifiedBy": _preferences.getString("username"),
                        },
                        "status": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].active == 0 ? "ACTIVE" : "INACTIVE",
                        "foreignBusinessID": _preferences.getString("last_known_state") != "IDE" ?
                        _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].foreignBusinessID != "NEW" ?
                        _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].foreignBusinessID
                            : _foreignBKPemegangSahamCompany : _foreignBKPemegangSahamCompany
                    });
                    _detailAddressesInfoNasabah.add({
                        "addressID": _preferences.getString("last_known_state") != "IDE" ? _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].addressID != "NEW" ? _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].addressID : "NEW" : "NEW",
                        "addressSpecification": {
                            "koresponden": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].isCorrespondence ? "1" : "0",
                            "matrixAddr": "12",
                            "address": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].address,
                            "rt": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].rt,
                            "rw": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].rw,
                            "provinsiID": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].kelurahanModel.PROV_ID,
                            "kabkotID": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].kelurahanModel.KABKOT_ID,
                            "kecamatanID": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].kelurahanModel.KEC_ID,
                            "kelurahanID": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].kelurahanModel.KEL_ID,
                            "zipcode": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].kelurahanModel.ZIPCODE
                        },
                        "contactSpecification": {
                            "telephoneArea1": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].phoneArea1,
                            "telephone1": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].phone1,
                            "telephoneArea2":_providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].phoneArea2,
                            "telephone2": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].phone1,
                            "faxArea": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].faxArea,
                            "fax": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].fax,
                            "handphone": "",
                            "email": ""
                        },
                        "addressType": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].jenisAlamatModel.KODE,
                        "creationalSpecification": {
                            "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                            "createdBy": _preferences.getString("username"),
                            "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                            "modifiedBy": _preferences.getString("username"),
                        },
                        "status": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].active == 0 ? "ACTIVE" : "INACTIVE",
                        "foreignBusinessID": _preferences.getString("last_known_state") != "IDE" ?
                        _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].foreignBusinessID != "NEW" ?
                        _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].foreignBusinessID :
                        _foreignBKPemegangSahamCompany : _foreignBKPemegangSahamCompany
                    });
                }
                _shareholdersCorporates.add(
                    {
                        "shareholdersCorporateID": _preferences.getString("last_known_state") != "IDE"
                            ? _providerPemegangSahamCompany.listPemegangSahamLembaga[i].shareholdersCorporateID != null
                            ? _providerPemegangSahamCompany.listPemegangSahamLembaga[i].shareholdersCorporateID : _foreignBKPemegangSahamCompany : _foreignBKPemegangSahamCompany,
                        "businessSpecification": {
                            "economySector": "",
                            "businessField": "",
                            "locationStatus": "",
                            "businessLocation": "",
                            "employeeTotal": 0,
                            "bussinessLengthInMonth": 0,
                            "totalBussinessLengthInMonth": 0
                        },
                        "businessPermitSpecification": {
                            "institutionType": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].typeInstitutionModel.PARA_ID,
                            "profile": null,
                            "fullName": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].institutionName,
                            "deedOfIncorporationNumber": "",
                            "deedEndDate": null,
                            "siupNumber": null,
                            "siupStartDate": null,
                            "tdpNumber": null,
                            "tdpStartDate": null,
                            "establishmentDate": formatDateValidateAndSubmit(DateTime.parse(_providerPemegangSahamCompany.listPemegangSahamLembaga[i].initialDateForDateOfEstablishment)),//"${_providerPemegangSahamCompany.listPemegangSahamLembaga[i].initialDateForDateOfEstablishment} 00:00:00",
                            "isCompany": false,
                            "authorizedCapital": 0,
                            "paidCapital": 0
                        },
                        "customerNpwpSpecification": {
                            "hasNpwp": true,
                            "npwpAddress": null,
                            "npwpFullname": null,
                            "npwpNumber": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].npwp,
                            "npwpType": null,
                            "pkpIdentifier": null
                        },
                        "positionID": null,
                        "shareholding": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].percentShare,
                        "dedupScore": 0.0,
                        "shareholdersCorporateAddresses": _detailAddressShareHolderCompany,
                        "creationalSpecification": {
                            "createdAt":  formatDateValidateAndSubmit(DateTime.now()),
                            "createdBy": _preferences.getString("username"),
                            "modifiedAt":  formatDateValidateAndSubmit(DateTime.now()),
                            "modifiedBy": _preferences.getString("username"),
                        },
                        "status": "ACTIVE"
                    }
                );
            }
        }

        var _surveyResultDetails = [];
        if(_providerSurvey.listResultSurveyDetailSurveyModel.isNotEmpty) {
            for(int i=0; i < _providerSurvey.listResultSurveyDetailSurveyModel.length; i++) {
                _surveyResultDetails.add({
                    "surveyResultDetailID": "NEW",
                    "infoEnvironment": _providerSurvey.listResultSurveyDetailSurveyModel[i].environmentalInformationModel.id,
                    "sourceInfo": _providerSurvey.listResultSurveyDetailSurveyModel[i].recourcesInfoSurveyModel.PARA_INFORMATION_SOURCE_ID,
                    "sourceNameInfo": _providerSurvey.listResultSurveyDetailSurveyModel[i].resourceInformationName,
                    "status": "ACTIVE",
                    "creationalSpecification": {
                        "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                        "createdBy": _preferences.getString("username"),
                        "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                        "modifiedBy": _preferences.getString("username"),
                    }
                });
            }
        }

        var _surveyResultAssets = [];
        if(_providerSurvey.listResultSurveyAssetModel.isNotEmpty){
            for(int i=0; i< _providerSurvey.listResultSurveyAssetModel.length; i++){
                _surveyResultAssets.add({
                    "surveyResultAssetID": "NEW",
                    "assetType": _providerSurvey.listResultSurveyAssetModel[i].assetTypeModel != null ? _providerSurvey.listResultSurveyAssetModel[i].assetTypeModel.id : null,
                    "assetAmount": _providerSurvey.listResultSurveyAssetModel[i].valueAsset != "" ? double.parse(_providerSurvey.listResultSurveyAssetModel[i].valueAsset.replaceAll(",", "")).toInt() : 0,
                    "ownStatus": _providerSurvey.listResultSurveyAssetModel[i].ownershipModel != null ? _providerSurvey.listResultSurveyAssetModel[i].ownershipModel.id : null,
                    "sizeofLand": _providerSurvey.listResultSurveyAssetModel[i].surfaceBuildingArea != "" ? _providerSurvey.listResultSurveyAssetModel[i].surfaceBuildingArea : null,
                    "streetType": _providerSurvey.listResultSurveyAssetModel[i].roadTypeModel != null ? _providerSurvey.listResultSurveyAssetModel[i].roadTypeModel.kode : null,
                    "electricityType": _providerSurvey.listResultSurveyAssetModel[i].electricityTypeModel != null ? _providerSurvey.listResultSurveyAssetModel[i].electricityTypeModel.PARA_ELECTRICITY_ID : null,
                    "electricityBill": _providerSurvey.listResultSurveyAssetModel[i].electricityBills != "" ? double.parse(_providerSurvey.listResultSurveyAssetModel[i].electricityBills.replaceAll(",", "")).toInt() : 0,
                    "expiredContractDate": _providerSurvey.listResultSurveyAssetModel[i].endDateLease != null ? formatDateValidateAndSubmit(_providerSurvey.listResultSurveyAssetModel[i].endDateLease) : null,
                    "longOfStay": _providerSurvey.listResultSurveyAssetModel[i].lengthStay != "" ? _providerSurvey.listResultSurveyAssetModel[i].lengthStay : null,
                    "status": "ACTIVE",
                    "creationalSpecification": {
                        "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                        "createdBy": _preferences.getString("username"),
                        "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                        "modifiedBy": _preferences.getString("username")
                    }
                });
            }
        }

        var _body = _preferences.getString("status_aoro") == "1" || _preferences.getString("status_aoro") == "2" ? jsonEncode({
                "creditLimitDTO": {
                    "lmeId": "",
                    "flagEligible": "",
                    "disburseType": "",
                    "productId": "",
                    "installmentAmt": "",
                    "phAmt": "",
                    "voucherCode": "",
                    "tenor": "",
                    "customerGrading": "",
                    "jenisPenawaran": "",
                    "urutanPencairan": "",
                    "jenisOpsi": "",
                    "maxPlafond": "",
                    "maxTenor": "",
                    "noReferensi": "",
                    "maxInstallment": "",
                    "portfolio": "",
                    "activationDate": "",
                },
                "applicationDTO": {
                    "customerDTO": {
                        "customerID": _preferences.getString("last_known_state") != "IDE" ? _providerRincian.customerID : "NEW",// perlu di rencanakan mau ditaruh di sqlite atau di sharedPreference
                        "customerType": _preferences.getString("cust_type") == "PER" ? "PER" : "COM",
                        "customerSpecification": {
                            "customerNPWP": {
                                "npwpAddress": _providerRincian.controllerNPWPAddress.text,//_providerInfoNasabah.radioValueIsHaveNPWP == 1 ? _providerInfoNasabah.controllerAlamatSesuaiNPWP.text : _providerInfoAlamat.listAlamatKorespondensi[0].address,
                                "npwpFullname": _npwpFullName,//_providerInfoNasabah.radioValueIsHaveNPWP == 1 ? _providerInfoNasabah.controllerNamaSesuaiNPWP.text  : _providerInfoNasabah.controllerNamaLengkapSesuaiIdentitas.text,
                                "npwpNumber": _npwpNumber,//_providerInfoNasabah.radioValueIsHaveNPWP == 1 ? _providerInfoNasabah.controllerNoNPWP.text : "00000000000000",
                                "npwpType": _npwpType,
                                "hasNpwp": _providerRincian.radioValueIsHaveNPWP == 1 ? true : false,//_providerInfoNasabah.radioValueIsHaveNPWP == 1 ? true : false,
                                "pkpIdentifier": _providerRincian.signPKPSelected != null ? _providerRincian.signPKPSelected.id : "2",
                            },
                            "detailAddresses": _detailAddressesInfoNasabah,
                            "customerGuarantor": {
                                "isGuarantor": _providerGuarantor.radioValueIsWithGuarantor == 0,
                                "guarantorCorporates": _guarantorCorporates,
                                "guarantorIndividuals": _guarantorIndividuals
                            }
                        },
                        "customerIndividualDTO": [],
                        "customerCorporateDTO": [
                            {
                                "customerCorporateID": _preferences.getString("last_known_state") != "IDE" ? _providerRincian.customerCorporateID : "CUST001",
                                "businessSpecification": {
                                    "economySector": _providerRincian.sectorEconomicModelSelected != null ? _providerRincian.sectorEconomicModelSelected.KODE : null,//_preferences.getString("cust_type") == "PER" ? "" : _providerRincian.sectorEconomicModelSelected.KODE,
                                    "businessField": _providerRincian.businessFieldModelSelected != null ? _providerRincian.businessFieldModelSelected.KODE : null ,//_preferences.getString("cust_type") == "PER" ? "" : _providerRincian.businessFieldModelSelected.KODE,
                                    "locationStatus": _providerRincian.locationStatusSelected != null ?  _providerRincian.locationStatusSelected.id : null,//_preferences.getString("cust_type") == "PER" ? "" : _providerRincian.locationStatusSelected.id,
                                    "businessLocation": _providerRincian.businessLocationSelected != null ? _providerRincian.businessLocationSelected.id : null,//_preferences.getString("cust_type") == "PER" ? "" : _providerRincian.businessLocationSelected.id,
                                    "employeeTotal": _providerRincian.controllerTotalEmployees.text != "" ? int.parse(_providerRincian.controllerTotalEmployees.text) : null,//_preferences.getString("cust_type") == "PER" ? 0 : double.parse(_providerRincian.controllerTotalEmployees.text.replaceAll(",", "")),
                                    "bussinessLengthInMonth": _providerRincian.controllerLamaUsahaBerjalan.text != "" ? double.parse(_providerRincian.controllerLamaUsahaBerjalan.text.replaceAll(",", "")) : null,//_preferences.getString("cust_type") == "PER" ? 0 : double.parse(_providerRincian.controllerLamaUsahaBerjalan.text.replaceAll(",", "")),
                                    "totalBussinessLengthInMonth": _providerRincian.controllerTotalLamaUsahaBerjalan.text != "" ? int.parse(_providerRincian.controllerTotalLamaUsahaBerjalan.text.replaceAll(",", "")) : null,//_preferences.getString("cust_type") == "PER" ? 0 : double.parse(_providerRincian.controllerTotalLamaUsahaBerjalan.text.replaceAll(",", "")),
                                },
                                "businessPermitSpecification": {
                                    "institutionType": _providerRincian.typeInstitutionSelected != null ?  _providerRincian.typeInstitutionSelected.PARA_ID : "",//_preferences.getString("cust_type") == "PER" ? "" : _providerRincian.typeInstitutionSelected.PARA_ID,
                                    "profile": _providerRincian.profilSelected != null ? _providerRincian.profilSelected.id : "",//_preferences.getString("cust_type") == "PER" ? "" : _providerRincian.profilSelected.id,
                                    "fullName": _providerRincian.controllerInstitutionName.text != "" ? _providerRincian.controllerInstitutionName.text : "",//_preferences.getString("cust_type") == "PER" ? "" : _providerRincian.controllerInstitutionName.text,
                                    "deedOfIncorporationNumber": null,
                                    "deedEndDate": null,
                                    "siupNumber": null,
                                    "siupStartDate": null,
                                    "tdpNumber": null,
                                    "tdpStartDate": null,
                                    "establishmentDate": _providerRincian.controllerDateEstablishment.text != "" ? "${_providerRincian.controllerDateEstablishment.text} 00:00:00" : "01-01-1900 00:00:00",//formatDateValidateAndSubmit(_providerRincian.initialDateForDateEstablishment),//_preferences.getString("cust_type") == "PER" ? "" : _providerRincian.controllerDateEstablishment.text,
                                    "isCompany": false,
                                    "authorizedCapital": 0,
                                    "paidCapital": 0
                                },
                                "managementPIC": {
                                    "picIdentity": {
                                        "identityType": _providerManajemenPIC.typeIdentitySelected != null ? _providerManajemenPIC.typeIdentitySelected.id : "",
                                        "identityNumber":  _providerManajemenPIC.controllerIdentityNumber.text != "" ? _providerManajemenPIC.controllerIdentityNumber.text : "",
                                        "identityName": _providerManajemenPIC.controllerFullNameIdentity.text != "" ? _providerManajemenPIC.controllerFullNameIdentity.text : "",
                                        "fullName": _providerManajemenPIC.controllerFullName.text != "" ? _providerManajemenPIC.controllerFullName.text : "",
                                        "alias": null,
                                        "title": null,
                                        "dateOfBirth": _providerManajemenPIC.controllerBirthOfDate.text != "" ? formatDateValidateAndSubmit(_providerManajemenPIC.initialDateForBirthOfDate) : "01-01-1900 00:00:00",//_preferences.getString("cust_type") == "PER" ? "" : _providerManajemenPIC.controllerBirthOfDate.text,
                                        "placeOfBirth": _providerManajemenPIC.controllerPlaceOfBirthIdentity.text != "" ? _providerManajemenPIC.controllerPlaceOfBirthIdentity.text : "",//_preferences.getString("cust_type") == "PER" ? "" : _providerManajemenPIC.controllerPlaceOfBirthIdentity.text,
                                        "placeOfBirthKabKota": _providerManajemenPIC.birthPlaceSelected != null ? _providerManajemenPIC.birthPlaceSelected.KABKOT_ID : "",//_preferences.getString("cust_type") == "PER" ? "" : _providerManajemenPIC.birthPlaceSelected.KABKOT_ID,
                                        "gender": null,
                                        "identityActiveStart": null,
                                        "identityActiveEnd": null,
                                        "isLifetime": null,
                                        "religion": null,
                                        "occupationID": null,
                                        "positionID": _providerManajemenPIC.positionSelected != null ? _providerManajemenPIC.positionSelected.PARA_FAMILY_TYPE_ID : "10", // "02"
                                        "maritalStatusID": "02"
                                    },
                                    "picContact": {
                                        "telephoneArea1": null,
                                        "telephone1": null,
                                        "telephoneArea2": null,
                                        "telephone2": null,
                                        "faxArea": null,
                                        "fax": null,
                                        "handphone": _providerManajemenPIC.controllerHandphoneNumber.text != "" ? "08${_providerManajemenPIC.controllerHandphoneNumber.text}" : "",//_preferences.getString("cust_type") == "PER" ? "" : _providerManajemenPIC.controllerHandphoneNumber.text,
                                        "email": _providerManajemenPIC.controllerEmail.text != "" ? _providerManajemenPIC.controllerEmail.text : "",//_preferences.getString("cust_type") == "PER" ? "" : _providerManajemenPIC.controllerEmail.text,
                                        "noWa": null,
                                        "noWa2": null,
                                        "noWa3": null
                                    },
                                    "picAddresses": [],//_detailAddressPIC,
                                    "shareholding": 0
                                },
                                "shareholdersCorporates": _shareholdersCorporates,
                                "shareholdersIndividuals": _pemegangSahamPribadi,
                                "dedupScore": 0.0,
                                "status": "ACTIVE",
                                "creationalSpecification": {
                                    "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                                    "createdBy": _preferences.getString("username"),
                                    "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                                    "modifiedBy": _preferences.getString("username"),
                                },
                            }
                        ],
                        "customerAdditionalInformation": null,
                        "customerProcessingCompletion": {
                            "lastKnownCustomerState": "CUSTOMER_GUARANTOR",
                            "nextKnownCustomerState": "CUSTOMER_GUARANTOR"
                        },
                        "creationalSpecification": {
                            "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                            "createdBy": _preferences.getString("username"),
                            "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                            "modifiedBy": _preferences.getString("username"),
                        },
                        "status": "ACTIVE",
                        "obligorID": _providerRincian.obligorID != "NEW" && _providerRincian.obligorID != "null" && _providerRincian.obligorID != null ? _providerRincian.obligorID : _preferences.getString("oid").toString(), // _preferences.getString("status_aoro") == "1" || _preferences.getString("status_aoro") == "2" ? _providerRincian.obligorID != "NEW" ? _providerRincian.obligorID : _preferences.getString("oid").toString() : "NEW",// _preferences.getString("last_known_state") != "IDE" ? _providerRincian.obligorID != "NEW" ? _providerRincian.obligorID : _preferences.getString("oid").toString() : "NEW",
                        "exposureInformations": null,
                        "customerIncomes": _customerIncomes
                    },
                    "orderDTO": {
                        "orderID": _preferences.getString("last_known_state") != "IDE" ? _providerApp.applNo : orderId,
                        "customerID": _preferences.getString("last_known_state") != "IDE" ? _providerRincian.customerID : "NEW",
                        "orderSpecification": {
                            "aosApprovalStatus": _preferences.getString("last_known_state") == "AOS" ? _providerSurvey.radioValueApproved : "",
                            "isMayor": false, // _preferences.getString("last_known_state") == "AOS" && _providerSurvey.radioValueApproved == "0" ? true : | _preferences.getString("is_mayor") == "0",
                            "hasFiducia": false,
                            "applicationSource": "003",
                            "orderDate": "${_providerApp.controllerOrderDate.text} 00:00:00",//di edit karena tgl orderDate lebih besar dr surveyAppointmentDate <formatDateValidateAndSubmit(_providerApp.initialDateOrder)>
                            "applicationDate": "${_providerApp.controllerOrderDate.text} 00:00:00",// di edit karena tgl applicationDate lebih besar dr surveyAppointmentDate <formatDateValidateAndSubmit(_providerApp.initialDateOrder)>
                            "surveyAppointmentDate": formatDateValidateAndSubmit(_providerApp.initialSurveyDate),
                            "orderType": _preferences.getString("status_aoro") != null ? _preferences.getString("status_aoro") : "0",//_preferences.getString("status_aoro"),
                            "isOpenAccount": true,
                            "accountFormNumber": null,
                            "sentraCID": _preferences.getString("SentraD"),
                            "unitCID": _preferences.getString("UnitD"),
                            "initialRecommendation": _providerApp.initialRecomendation,
                            "finalRecommendation": _providerApp.finalRecomendation,
                            "brmsScoring": _providerApp.brmsScoring,
                            "isInPlaceApproval": false,
                            "isPkSigned": _providerApp.isSignedPK,
                            "maxApprovalLevel": null,
                            "jenisKonsep": _providerApp.conceptTypeModelSelected != null ? _providerApp.conceptTypeModelSelected.id : "",
                            "jumlahObjek": _providerApp.controllerTotalObject.text == "" ? null : int.parse(_providerApp.controllerTotalObject.text),
                            "jenisProporsionalAsuransi": _providerApp.proportionalTypeOfInsuranceModelSelected != null ? _providerApp.proportionalTypeOfInsuranceModelSelected.kode : "",
                            "unitKe": _providerApp.numberOfUnitSelected == "" ? null : int.parse(_providerApp.numberOfUnitSelected),
                            "isWillingToAcceptInfo": false,
                            "applicationContractNumber": null,
                            "dealerNote": Provider.of<MarketingNotesChangeNotifier>(context,listen: false).controllerMarketingNotes.text,
                            "userDealer": "",
                            "orderDealer": "",
                            "flagSourceMS2": "103", // diganti dari 006 ke 103 sesuai arahan mas Oji (per tanggal 06.07.2021)
                            "orderSupportingDocuments": _orderSupportingDocuments,
                            "collateralTypeID": _providerKolateral.collateralTypeModel.id,
                            "applicationDocVerStatus": null
                        },
                        "orderProducts": [
                            {
                                "orderProductID": _preferences.getString("last_known_state") != "IDE" ? _providerUnitObject.applObjtID : orderProductId, // jika IDE ngambil dari nomer unit, selain IDE ngambil dari APPL_NO get data from DB API
                                "orderProductSpecification": {
                                    "financingTypeID": _providerUnitObject.typeOfFinancingModelSelected.financingTypeId,//_preferences.getString("cust_type") == "PER" ? _providerFoto.typeOfFinancingModelSelected.financingTypeId : _providerUnitObject.typeOfFinancingModelSelected.financingTypeId,
                                    "ojkBusinessTypeID": _providerUnitObject.businessActivitiesModelSelected.id,//_preferences.getString("cust_type") == "PER" ? _providerFoto.kegiatanUsahaSelected.id.toString() : _providerUnitObject.typeOfFinancingModelSelected.financingTypeId.toString(),
                                    "ojkBussinessDetailID": _providerUnitObject.businessActivitiesTypeModelSelected.id,//_preferences.getString("cust_type") == "PER" ? _providerFoto.jenisKegiatanUsahaSelected.id.toString() : _providerUnitObject.typeOfFinancingModelSelected.financingTypeId,
                                    "orderUnitSpecification": {
                                        "objectGroupID": _providerUnitObject.groupObjectSelected != null ? _providerUnitObject.groupObjectSelected.KODE : "",
                                        "objectID": _providerUnitObject.objectSelected != null ? _providerUnitObject.objectSelected.id : "",
                                        "productTypeID": _providerUnitObject.productTypeSelected != null ? _providerUnitObject.productTypeSelected.id : "",
                                        "objectBrandID": _providerUnitObject.brandObjectSelected != null ? _providerUnitObject.brandObjectSelected.id : "",
                                        "objectTypeID": _providerUnitObject.objectTypeSelected != null ? _providerUnitObject.objectTypeSelected.id : "",
                                        "objectModelID": _providerUnitObject.modelObjectSelected != null ? _providerUnitObject.modelObjectSelected.id : "",
                                        "objectUsageID": _providerUnitObject.objectUsageModel != null ? _providerUnitObject.objectUsageModel.id : "", // field pemakaian objek (unit)
                                        "objectPurposeID": _providerUnitObject.objectPurposeSelected != null ? _providerUnitObject.objectPurposeSelected.id : "", // field tujuan objek (unit)
                                        "modelDetail": _providerUnitObject.controllerDetailModel.text == "" ? null : _providerUnitObject.controllerDetailModel.text
                                    },
                                    "salesGroupID": _providerUnitObject.groupSalesSelected != null ? _providerUnitObject.groupSalesSelected.kode : "",
                                    "orderSourceID": _providerUnitObject.sourceOrderSelected != null ? _providerUnitObject.sourceOrderSelected.kode : "",
                                    "orderSourceName": _providerUnitObject.sourceOrderNameSelected != null ? _providerUnitObject.sourceOrderNameSelected.kode : "",
                                    "orderProductDealerSpecification": {
                                        "isThirdParty": false,
                                        "thirdPartyTypeID": _providerUnitObject.thirdPartyTypeSelected != null ? _providerUnitObject.thirdPartyTypeSelected.kode : "",
                                        "thirdPartyID": _providerUnitObject.thirdPartySelected != null ? _providerUnitObject.thirdPartySelected.kode : "",
                                        "thirdPartyActivity": _providerUnitObject.activitiesModel != null ? _providerUnitObject.activitiesModel.kode : null,
                                        "sentradID": _preferences.getString("SentraD"), //_providerUnitObject.thirdPartySelected != null ? _providerUnitObject.thirdPartySelected.sentraId : "",
                                        "unitdID": _preferences.getString("UnitD"), //_providerUnitObject.thirdPartySelected != null ? _providerUnitObject.thirdPartySelected.unitId : "",
                                        "dealerMatrix": _providerUnitObject.matriksDealerSelected != null ? _providerUnitObject.matriksDealerSelected.kode : ""
                                    },
                                    "programID": _providerUnitObject.programSelected != null ? _providerUnitObject.programSelected.kode : "",
                                    "rehabType": _providerUnitObject.rehabTypeSelected != null ? _providerUnitObject.rehabTypeSelected.id : "",
                                    "referenceNumber": _providerUnitObject.referenceNumberModel != null ? _providerUnitObject.referenceNumberModel.noPK : null,
                                    "applicationUnitContractNumber": null,
                                    "orderProductSaleses": _orderProductSaleses,
                                    "orderKaroseris": _orderKaroseris,
                                    "orderProductInsurances": _orderProductInsurances,
                                    "orderWmps": _orderWmps,
                                    "groupID": _providerUnitObject.grupIdSelected != null ? _providerUnitObject.grupIdSelected.kode : null
                                },
                                "orderCreditStructure": {
                                    "installmentTypeID": _providerCreditStructure.installmentTypeSelected != null ? _providerCreditStructure.installmentTypeSelected.id : "", // sebelumnya ngambil by kode
                                    "tenor": _providerCreditStructure.periodOfTimeSelected != null ? int.parse(_providerCreditStructure.periodOfTimeSelected) : "",
                                    "paymentMethodID": _providerCreditStructure.paymentMethodSelected != null ? _providerCreditStructure.paymentMethodSelected.id : "", // sebelumnya ngambil by kode
                                    "effectiveRate": _providerCreditStructure.controllerInterestRateEffective.text != "" ? double.parse(_providerCreditStructure.controllerInterestRateEffective.text) : "",
                                    "flatRate": _providerCreditStructure.controllerInterestRateFlat.text != "" ? double.parse(_providerCreditStructure.controllerInterestRateFlat.text) : "", //0.48501
                                    "objectPrice": _providerCreditStructure.controllerObjectPrice.text != "" ? double.parse(_providerCreditStructure.controllerObjectPrice.text.replaceAll(",", "")) : 0,
                                    "karoseriTotalPrice": _providerCreditStructure.controllerKaroseriTotalPrice.text != "" ? double.parse(_providerCreditStructure.controllerKaroseriTotalPrice.text.replaceAll(",", "")) : 0,
                                    "totalPrice": _providerCreditStructure.controllerTotalPrice.text != "" ? double.parse(_providerCreditStructure.controllerTotalPrice.text.replaceAll(",", "")) : 0,
                                    "nettDownPayment": _providerCreditStructure.controllerNetDP.text != "" ? double.parse(_providerCreditStructure.controllerNetDP.text.replaceAll(",", "")) : 0,
                                    "declineNInstallment": _providerCreditStructure.installmentDeclineN,
                                    "paymentOfYear": _providerCreditStructure.installmentTypeSelected != null ? _providerCreditStructure.installmentTypeSelected.id == "04" ? int.parse(_providerCreditStructure.controllerPaymentPerYear.text) : 1 : 1,
                                    "branchDownPayment": _providerCreditStructure.controllerBranchDP.text != "" ? double.parse(_providerCreditStructure.controllerBranchDP.text.replaceAll(",", "")) : 0,
                                    "grossDownPayment": _providerCreditStructure.controllerGrossDP.text != "" ? double.parse(_providerCreditStructure.controllerGrossDP.text.replaceAll(",", "")) : 0,
                                    "totalLoan": _providerCreditStructure.controllerTotalLoan.text != "" ? double.parse(_providerCreditStructure.controllerTotalLoan.text.replaceAll(",", "")) : 0,
                                    "installmentAmount": _providerCreditStructure.controllerInstallment.text != "" ? double.parse(_providerCreditStructure.controllerInstallment.text.replaceAll(",", "")) : 0,
                                    "ltvRatio": _providerCreditStructure.controllerLTV.text != "" ? double.parse(_providerCreditStructure.controllerLTV.text.replaceAll(",", "")) : 0.0,
                                    "pencairan": 0,
                                    "interestAmount": _providerCreditStructure.interestAmount,
                                    "gpType": null,
                                    "newTenor": 0,
                                    "totalStepping": 0,
                                    "installmentBalloonPayment": {
                                        "balloonType": _providerCreditStructureType.radioValueBalloonPHOrBalloonInstallment,
                                        "lastInstallmentPercentage": _providerCreditStructureType.controllerInstallmentPercentage.text != "" ? double.parse(_providerCreditStructureType.controllerInstallmentPercentage.text.replaceAll(",", "")) : 0,
                                        "lastInstallmentValue": _providerCreditStructureType.controllerInstallmentValue.text != "" ? double.parse(_providerCreditStructureType.controllerInstallmentValue.text.replaceAll(",", "")) : 0
                                    },
                                    "installmentSchedule": {
                                        "installmentType": _providerCreditStructure.installmentTypeSelected != null ? _providerCreditStructure.installmentTypeSelected.name : null, // OLD: ambil dari ID
                                        "paymentMethodType": _providerCreditStructure.paymentMethodSelected != null ? _providerCreditStructure.paymentMethodSelected.name : null, // OLD: ambil dari ID
                                        "installment": _providerCreditStructure.controllerInstallment.text != "" ? double.parse(_providerCreditStructure.controllerInstallment.text.replaceAll(",", "")) : 0,
                                        "installmentRound": 0,
                                        "installmentMethod": 0,
                                        "lastKnownOutstanding": 0,
                                        "minInterest": 0,
                                        "maxInterest": 0,
                                        "futureValue": 0,
                                        "isInstallment": false,
                                        "roundingValue": null,
                                        "balloonInstallment": 0,
                                        "gracePeriode": 0,
                                        "gracePeriodes": [],
                                        "seasonal": 0,
                                        "steppings": [],
                                        "installmentScheduleDetails": []
                                    },
                                    "dsr": _providerCreditIncome.controllerDebtComparison.text != "" ? double.parse(_providerCreditIncome.controllerDebtComparison.text.replaceAll(",", "")) : 0,
                                    "dir": _providerCreditIncome.controllerIncomeComparison.text != "" ? double.parse(_providerCreditIncome.controllerIncomeComparison.text.replaceAll(",", "")) : 0,
                                    "dsc": _providerCreditIncome.controllerDSC.text != "" ? double.parse(_providerCreditIncome.controllerDSC.text.replaceAll(",", "")) : 0.0,
                                    "irr": _providerCreditIncome.controllerIRR.text != "" ? double.parse(_providerCreditIncome.controllerIRR.text.replaceAll(",", "")) : 0.0,
                                    "installmentDetails": _installmentDetails,
                                    "uangMukaKaroseri": 0, // diset 0
                                    "uangMukaChasisNet": _providerCreditStructure.controllerNetDP.text != "" ? double.parse(_providerCreditStructure.controllerNetDP.text.replaceAll(",", "")) : 0, // set sama kaya nettDownPayment
                                    "uangMukaChasisGross": _providerCreditStructure.controllerGrossDP.text != "" ? double.parse(_providerCreditStructure.controllerGrossDP.text.replaceAll(",", "")) : 0, // set sama kaya grossDownPayment
                                    // [
                                    //   {
                                    //     "installmentID": null,
                                    //     "installmentNumber": 0,
                                    //     "percentage": 0,
                                    //     "amount": 0,
                                    //     "creationalSpecification": {
                                    //       "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                                    //       "createdBy": "10056030",
                                    //       "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                                    //       "modifiedBy": "10056030"
                                    //     },
                                    //     "status": "ACTIVE"
                                    //   }
                                    // ],
                                    "realDSR": 0 // di jadikan model dari get dsr
                                },
                                "orderFees": _orderFees,
                                "orderSubsidies": [],
                                "orderProductAdditionalSpecification": {
                                    "virtualAccountID": null,
                                    "virtualAccountValue": 0,
                                    "cashingPurposeID": null,
                                    "cashingTypeID": null
                                },
                                "creationalSpecification": {
                                    "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                                    "createdBy": _preferences.getString("username"),
                                    "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                                    "modifiedBy": _preferences.getString("username"),
                                },
                                "status": "ACTIVE",
                                "collateralAutomotives": _providerKolateral.collateralTypeModel.id == "001"
                                    ?
                                [
                                    {
                                        "collateralID": _preferences.getString("last_known_state") != "IDE" ? _providerKolateral.collateralID : "CAU001", // NEW | _providerKolateral.collateralTypeModel != null ? _providerKolateral.collateralTypeModel.id : "",
                                        "isCollaSameUnit": _providerKolateral.radioValueIsCollateralSameWithUnitOto, // Jika Collateral = Unit dipilih YA, maka kirim TRUE. Begitu juga sebaliknya (0 = YA/true | 1 = TIDAK/false)
                                        "isMultiUnitCollateral": _providerKolateral.radioValueForAllUnitOto, // ya = false = 0 | tidak = true = 1
                                        "isCollaNameSameWithCustomerName": _providerKolateral.radioValueIsCollaNameSameWithApplicantOto, // Jika Nama Jaminan = Pemohon dipilih YA, maka kirim TRUE. Begitu juga sebaliknya (0 = YA/true | 1 = TIDAK/false)
                                        "identitySpecification": {
                                            "identityType": _providerKolateral.identityTypeSelectedAuto != null ? _providerKolateral.identityTypeSelectedAuto.id : "",
                                            "identityNumber":  _providerKolateral.controllerIdentityNumberAuto.text,
                                            "identityName": _providerKolateral.controllerNameOnCollateralAuto.text,
                                            "fullName" : _providerKolateral.controllerNameOnCollateralAuto.text,
                                            "alias": null,
                                            "title": null,
                                            "dateOfBirth": _providerKolateral.controllerBirthDateAuto.text != "" ? _providerKolateral.controllerBirthDateAuto.text+" 00:00:00" : null,
                                            "placeOfBirth": null, //_providerKolateral.controllerBirthPlaceValidWithIdentityAuto.text != "" ? _providerKolateral.controllerBirthPlaceValidWithIdentityAuto.text : "",
                                            "placeOfBirthKabKota": null, //_providerKolateral.birthPlaceAutoSelected != null ? _providerKolateral.birthPlaceAutoSelected.KABKOT_ID : _providerInfoNasabah.birthPlaceSelected.KABKOT_ID,
                                            "gender": null,//_providerKolateral.radioValueIsCollaNameSameWithApplicantOto == 0 ? _providerInfoNasabah.radioValueGender : null,
                                            "identityActiveStart": null,
                                            "identityActiveEnd": null,
                                            "religion": null, // _providerKolateral.radioValueIsCollaNameSameWithApplicantOto == 0 ? _providerInfoNasabah.religionSelected == null ? null : _providerInfoNasabah.religionSelected.id : null
                                            "occupationID": null,//_providerFoto.occupationSelected != null ? _providerFoto.occupationSelected.KODE : "",
                                            "positionID": null
                                        },
                                        "collateralAutomotiveSpecification": {
                                            "orderUnitSpecification": {
                                                "objectGroupID": _providerKolateral.groupObjectSelected != null ? _providerKolateral.groupObjectSelected.KODE : null,
                                                "objectID": _providerKolateral.objectSelected != null ? _providerKolateral.objectSelected.id : null,
                                                "productTypeID": _providerKolateral.productTypeSelected != null ? _providerKolateral.productTypeSelected.id : null,
                                                "objectBrandID":  _providerKolateral.brandObjectSelected != null ? _providerKolateral.brandObjectSelected.id : null,
                                                "objectTypeID": _providerKolateral.objectTypeSelected != null ? _providerKolateral.objectTypeSelected.id : null,
                                                "objectModelID": _providerKolateral.modelObjectSelected != null ? _providerKolateral.modelObjectSelected.id : null,
                                                "objectUsageID":  "", // tidak dipakai
                                                "objectPurposeID": _providerKolateral.objectUsageSelected != null ? _providerKolateral.objectUsageSelected.id : null, // field tujuan penggunaan collateral
                                            },
                                            "mpAdiraUpld": _providerKolateral.controllerMPAdiraUpload.text != "" ? double.parse(_providerKolateral.controllerMPAdiraUpload.text.replaceAll(",", "")) : null,
                                            "productionYear": _providerKolateral.yearProductionSelected != null ? int.parse(_providerKolateral.yearProductionSelected) : "",
                                            "registrationYear": _providerKolateral.yearRegistrationSelected != null ? int.parse(_providerKolateral.yearRegistrationSelected) : "",
                                            "isYellowPlate": _providerKolateral.radioValueYellowPlat, // per tanggal 10.05.2021 = dirubah ga dirubah jadi true false (OLD: == 0 ? false : true)
                                            "isBuiltUp": _providerKolateral.radioValueBuiltUpNonATPM == 0 ? true : false,
                                            "utjSpecification": {
                                                "bpkbNumber": _providerKolateral.controllerBPKPNumber.text != "" ? _providerKolateral.controllerBPKPNumber.text : null,
                                                "chassisNumber":  _providerKolateral.controllerFrameNumber.text != "" ? _providerKolateral.controllerFrameNumber.text : null,
                                                "engineNumber": _providerKolateral.controllerMachineNumber.text != "" ? _providerKolateral.controllerMachineNumber.text : null,
                                                "policeNumber": _providerKolateral.controllerPoliceNumber.text != "" ? _providerKolateral.controllerPoliceNumber.text : null,
                                            },
                                            "gradeUnit": _providerKolateral.controllerGradeUnit.text != "" ? _providerKolateral.controllerGradeUnit.text : null,
                                            "utjFacility": _providerKolateral.controllerFasilitasUTJ.text != "" ? _providerKolateral.controllerFasilitasUTJ.text == "TUNAI" ? "1" : "0" : null,
                                            "bidderName": _providerKolateral.controllerNamaBidder.text != "" ? _providerKolateral.controllerNamaBidder.text : null,
                                            "showroomPrice": _providerKolateral.controllerHargaJualShowroom.text != "" ? double.parse(_providerKolateral.controllerHargaJualShowroom.text.replaceAll(",", "")).round() : "",
                                            "additionalEquipment": _providerKolateral.controllerPerlengkapanTambahan.text != "" ? _providerKolateral.controllerPerlengkapanTambahan.text : "",
                                            "mpAdira":  _providerKolateral.controllerMPAdira.text != "" ? double.parse(_providerKolateral.controllerMPAdira.text.replaceAll(",", "")) : null,
                                            "physicalRecondition": _providerKolateral.controllerRekondisiFisik.text != "" ? double.parse(_providerKolateral.controllerRekondisiFisik.text.replaceAll(",", "")) : 0,
                                            "isProper": _providerKolateral.radioValueWorthyOrUnworthy, // 0 = layak = false | 1 = tidak layak = true
                                            "ltvRatio": 0.0,
                                            "appraisalSpecification": {
                                                "collateralDp": double.parse(_providerKolateral.controllerDPJaminan.text == '' ? '0' : _providerKolateral.controllerDPJaminan.text.replaceAll(",", "")),
                                                "maximumPh": double.parse(_providerKolateral.controllerPHMaxAutomotive.text == '' ? '0' : _providerKolateral.controllerPHMaxAutomotive.text.replaceAll(",", "")),
                                                "appraisalPrice": double.parse(_providerKolateral.controllerTaksasiPriceAutomotive.text == '' ? '0' : _providerKolateral.controllerTaksasiPriceAutomotive.text.replaceAll(",", "")),
                                                "collateralUtilizationID": _providerKolateral.collateralUsageOtoSelected != null ? _providerKolateral.collateralUsageOtoSelected.id : "" // field tujuan penggunaan collateral
                                            },
                                            "collateralAutomotiveAdditionalSpecification": {
                                                "capacity": 0,
                                                "color": null,
                                                "manufacturer": null,
                                                "serialNumber": null,
                                                "invoiceNumber": null,
                                                "stnkActiveDate": null,
                                                "bpkbAddress": null,
                                                "bpkbIdentityTypeID": null
                                            },
                                        },
                                        "status": "ACTIVE",
                                        "creationalSpecification": {
                                            "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                                            "createdBy": _preferences.getString("username"),
                                            "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                                            "modifiedBy": _preferences.getString("username"),
                                        }
                                    }
                                ] : [],
                                "collateralProperties": _providerKolateral.collateralTypeModel.id == "002"
                                    ?
                                [
                                    {
                                        "isMultiUnitCollateral": true, // _providerKolateral.radioValueForAllUnitOto, // ya = false = 0 | tidak = true = 1
                                        "isCollaNameSameWithCustomerName": _providerKolateral.radioValueIsCollateralSameWithApplicantProperty == 0,
                                        "collateralID": _preferences.getString("last_known_state") != "IDE" ? _providerKolateral.collateralID : "COLP001", // NEW
                                        "identitySpecification": {
                                            "identityType": _providerKolateral.identityModel != null ? _providerKolateral.identityModel.id : "",
                                            "identityNumber": _providerKolateral.controllerIdentityNumber.text,
                                            "identityName": null,
                                            "fullName": _providerKolateral.controllerNameOnCollateral.text,
                                            "alias": null,
                                            "title": null,
                                            "dateOfBirth": _providerKolateral.controllerBirthDateProp.text != "" ? _providerKolateral.controllerBirthDateProp.text : null,
                                            "placeOfBirth": _providerKolateral.birthPlaceSelected.KABKOT_NAME,
                                            "placeOfBirthKabKota": _providerKolateral.birthPlaceSelected.KABKOT_ID,
                                            "gender": null,
                                            "identityActiveStart": null,
                                            "identityActiveEnd": null,
                                            "isLifetime": null,
                                            "religion": null,
                                            "occupationID": null,
                                            "positionID": null,
                                            "maritalStatusID": null
                                        },
                                        "detailAddresses": [
                                            {
                                                "addressID": _preferences.getString("last_known_state") != "IDE" ? _providerKolateral.addressID != "NEW" ? _providerKolateral.addressID : "NEW" : "NEW",
                                                "foreignBusinessID": _preferences.getString("last_known_state") != "IDE" ? _providerKolateral.foreignBusinessID != "NEW" ? _providerKolateral.foreignBusinessID : "CAU001" : "CAU001", // NEW
                                                "addressSpecification": {
                                                    "koresponden": _providerKolateral.isKorespondensi.toString(), // tanya oji
                                                    "matrixAddr": _preferences.getString("cust_type") == "PER" ? "6": "13",
                                                    "address": _providerKolateral.controllerAddress.text,
                                                    "rt": _providerKolateral.controllerRT.text,
                                                    "rw": _providerKolateral.controllerRW.text,
                                                    "provinsiID": _providerKolateral.kelurahanSelected.PROV_ID,
                                                    "kabkotID": _providerKolateral.kelurahanSelected.KABKOT_ID,
                                                    "kecamatanID": _providerKolateral.kelurahanSelected.KEC_ID,
                                                    "kelurahanID": _providerKolateral.kelurahanSelected.KEL_ID,
                                                    "zipcode": _providerKolateral.kelurahanSelected.ZIPCODE
                                                },
                                                "contactSpecification": {
                                                    "telephoneArea1": null,
                                                    "telephone1": null,
                                                    "telephoneArea2": null,
                                                    "telephone2": null,
                                                    "faxArea": null,
                                                    "fax": null,
                                                    "handphone": null,
                                                    "email": null,
                                                    "noWa": null,
                                                    "noWa2": null,
                                                    "noWa3": null
                                                },
                                                "addressType": _providerKolateral.addressTypeSelected.KODE,
                                                "creationalSpecification": {
                                                    "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                                                    "createdBy": _preferences.getString("username"),
                                                    "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                                                    "modifiedBy": _preferences.getString("username"),
                                                },
                                                "status": "ACTIVE"
                                            }
                                        ],
                                        "collateralPropertySpecification": {
                                            "certificateNumber": _providerKolateral.controllerCertificateNumber.text,
                                            "certificateTypeID": _providerKolateral.certificateTypeSelected.id,
                                            "propertyTypeID": _providerKolateral.propertyTypeSelected.id,
                                            "buildingArea": double.parse(_providerKolateral.controllerBuildingArea.text.replaceAll(",", "")),
                                            "landArea": double.parse(_providerKolateral.controllerSurfaceArea.text.replaceAll(",", "")),
                                            "appraisalSpecification": {
                                                "collateralDp": double.parse(_providerKolateral.controllerDPJaminan.text.replaceAll(",", "")).toInt(),
                                                "maximumPh": double.parse(_providerKolateral.controllerPHMax.text == '' ? '0' : _providerKolateral.controllerPHMax.text.replaceAll(",", "")),
                                                "appraisalPrice": double.parse(_providerKolateral.controllerTaksasiPrice.text.replaceAll(",", "")).toInt(),
                                                "collateralUtilizationID": _providerKolateral.collateralUsagePropertySelected != null ? _providerKolateral.collateralUsagePropertySelected.id : "" //""
                                                // "collateralUtilizationID": "05"//tanya oji
                                            },
                                            "positiveFasumDistance": double.parse(_providerKolateral.controllerJarakFasumPositif.text.replaceAll(",", "")).toInt(),
                                            "negativeFasumDistance": double.parse(_providerKolateral.controllerJarakFasumNegatif.text.replaceAll(",", "")).toInt(),
                                            "landPrice": double.parse(_providerKolateral.controllerHargaTanah.text.replaceAll(",", "")).toInt(),
                                            "njopPrice": double.parse(_providerKolateral.controllerHargaNJOP.text.replaceAll(",", "")).toInt(),
                                            "buildingPrice": double.parse(_providerKolateral.controllerHargaBangunan.text.replaceAll(",", "")).toInt(),
                                            "roofTypeID": _providerKolateral.typeOfRoofSelected != null ? _providerKolateral.typeOfRoofSelected.id : null,
                                            "wallTypeID": _providerKolateral.wallTypeSelected != null ? _providerKolateral.wallTypeSelected.id : null,
                                            "floorTypeID": _providerKolateral.floorTypeSelected != null ? _providerKolateral.floorTypeSelected.id : null,
                                            "foundationTypeID": _providerKolateral.foundationTypeSelected != null ? _providerKolateral.foundationTypeSelected.id : null,
                                            "roadTypeID": _providerKolateral.streetTypeSelected != null ? _providerKolateral.streetTypeSelected.id : null,
                                            "isCarPassable": _providerKolateral.radioValueAccessCar == 0,
                                            "totalOfHouseInRadius": _providerKolateral.controllerJumlahRumahDalamRadius.text != "" ? int.parse(_providerKolateral.controllerJumlahRumahDalamRadius.text) : 0,
                                            "sifatJaminan": _providerKolateral.controllerSifatJaminan.text,
                                            "buktiKepemilikanTanah": _providerKolateral.controllerBuktiKepemilikan.text,
                                            "tgglTerbitSertifikat": _providerKolateral.controllerCertificateReleaseDate.text != "" ? formatDateValidateAndSubmit(_providerKolateral.initialDateCertificateRelease) : "",
                                            "tahunTerbitSertifikat": _providerKolateral.controllerCertificateReleaseYear.text,
                                            "namaPemegangHak": _providerKolateral.controllerNamaPemegangHak.text,
                                            "noSuratUkurGmbrSituasi": _providerKolateral.controllerNoSuratUkur.text,
                                            "tgglSuratUkurGmbrSituasi": _providerKolateral.controllerDateOfMeasuringLetter.text != "" ? formatDateValidateAndSubmit(_providerKolateral.initialDateOfMeasuringLetter) : "",
                                            "sertifikatSuratUkurDikeluarkanOleh": _providerKolateral.controllerCertificateOfMeasuringLetter.text,
                                            "masaBerlakuHak": _providerKolateral.controllerMasaHakBerlaku.text,
                                            "noImb": _providerKolateral.controllerNoIMB.text,
                                            "tgglImb": _providerKolateral.controllerDateOfIMB.text != "" ? formatDateValidateAndSubmit(_providerKolateral.initialDateOfIMB) : "",
                                            "luasBangunanImb": double.parse(_providerKolateral.controllerLuasBangunanIMB.text.replaceAll(",", "")),
                                            "ltvRatio": 0,
                                            "letakTanah": null,
                                            "propertyGroupObjtID": null,
                                            "propertyObjtID": null
                                        },
                                        "status": "INACTIVE",
                                        "creationalSpecification": {
                                            "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                                            "createdBy": _preferences.getString("username"),
                                            "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                                            "modifiedBy": _preferences.getString("username"),
                                        }
                                    }
                                ]
                                    : [],
                                "orderProductProcessingCompletion": {
                                    "lastKnownOrderProductState": "APPLICATION_DOCUMENT",
                                    "nextKnownOrderProductState": "APPLICATION_DOCUMENT"
                                },
                                "isWithoutColla": _providerKolateral.collateralTypeModel.id == "003" ? 1 : 0, // _providerUnitObject.groupObjectSelected.KODE == "003" ? 1 : 0,
                                "isSaveKaroseri": false
                            }
                        ],
                        "orderProcessingCompletion": {
                            "calculatedAt": 0,
                            "lastKnownOrderState": null,
                            "lastKnownOrderStatePosition": null,
                            "lastKnownOrderStatus": null,
                            "lastKnownOrderHandledBy": null
                        },
                        "creationalSpecification": {
                            "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                            "createdBy": _preferences.getString("username"),
                            "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                            "modifiedBy": _preferences.getString("username"),
                        },
                        "status": "ACTIVE"
                    },
                    "surveyDTO": _preferences.getString("last_known_state") != "IDE"
                        ?
                    {
                        "surveyID": _dataResultSurvey.isNotEmpty ? _dataResultSurvey[0]['surveyID'] != null ? _dataResultSurvey[0]['surveyID'] : "NEW" : "NEW", // dibuat seperti ini karena ada kemungkinan IDE di buat di ACCTION, sehingga survey id sudah tergenerate saat melakukan SRE di MS2
                        "orderID": _preferences.getString("last_known_state") != "IDE" ? _providerApp.applNo : orderId,
                        "surveyType": "002",
                        "employeeID": _preferences.getString("username"),
                        "phoneNumber": null,
                        "janjiSurveyDate": formatDateValidateAndSubmit(_providerApp.initialSurveyDate),
                        "reason": "",
                        "flagBRMS": "BRMS",
                        "surveyStatus": "01",
                        "status": "ACTIVE",
                        "surveyProcess": "02",
                        "surveyResultSpecification": {
                            "surveyResultType": (_preferences.getString("last_known_state") == "AOS" || _preferences.getString("last_known_state") == "CONA") && _providerSurvey.radioValueApproved == "0"
                                ? _providerSurvey.resultSurveySelected != null
                                ? _providerSurvey.resultSurveySelected.KODE : "000"
                                :  _providerSurvey.resultSurveySelected != null
                                ? _providerSurvey.resultSurveySelected.KODE : "",
                            "recomendationType": _providerSurvey.recommendationSurveySelected != null ? _providerSurvey.recommendationSurveySelected.KODE : null,
                            "notes": _providerSurvey.controllerNote.text,
                            "surveyResultDate":  "${_providerSurvey.controllerResultSurveyDate.text} ${_providerSurvey.controllerResultSurveyTime.text}:00", //formatDateValidateAndSubmit(_providerSurvey.initialDateResultSurvey),
                            "distanceWithSentra": _providerSurvey.controllerDistanceLocationWithSentraUnit.text != "" ? double.parse(_providerSurvey.controllerDistanceLocationWithSentraUnit.text.replaceAll(",", "")).toInt() : 0,
                            "distanceWithDealer": _providerSurvey.controllerDistanceLocationWithDealerMerchantAXI.text != "" ? double.parse(_providerSurvey.controllerDistanceLocationWithDealerMerchantAXI.text.replaceAll(",", "")).toInt() : 0,
                            "distanceObjWithSentra": _providerSurvey.controllerDistanceLocationWithUsageObjectWithSentraUnit.text != "" ? double.parse(_providerSurvey.controllerDistanceLocationWithUsageObjectWithSentraUnit.text.replaceAll(",", "")).toInt() : 0,
                            "surveyResultAssets": _surveyResultAssets,
                            // [
                            //     {
                            //         "surveyResultAssetID": "NEW",
                            //         "assetType": _providerSurvey.assetTypeSelected != null ? _providerSurvey.assetTypeSelected.id : null,
                            //         "assetAmount": double.parse(_providerSurvey.controllerValueAsset.text.replaceAll(",", "")).toInt(),
                            //         "ownStatus": _providerSurvey.ownershipSelected != null ? _providerSurvey.ownershipSelected.id : null,
                            //         "sizeofLand": _providerSurvey.controllerSurfaceBuildingArea.text,
                            //         "streetType": _providerSurvey.roadTypeSelected != null ? _providerSurvey.roadTypeSelected.kode : null,
                            //         "electricityType": _providerSurvey.electricityTypeSelected != null ? _providerSurvey.electricityTypeSelected.PARA_ELECTRICITY_ID : null,
                            //         "electricityBill": _providerSurvey.controllerElectricityBills.text != "" ? double.parse(_providerSurvey.controllerElectricityBills.text.replaceAll(",", "")).toInt() : 0,
                            //         "expiredContractDate": _providerSurvey.controllerEndDateLease.text != "" ? formatDateValidateAndSubmit(_providerSurvey.initialEndDateLease) : null,
                            //         "longOfStay": _providerSurvey.controllerLengthStay.text,
                            //         "status": "ACTIVE",
                            //         "creationalSpecification": {
                            //             "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                            //             "createdBy": _preferences.getString("username"),
                            //             "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                            //             "modifiedBy": _preferences.getString("username")
                            //         }
                            //     }
                            // ],
                            "surveyResultDetails": _surveyResultDetails,
                            "surveyResultTelesurveys": []
                        },
                        "creationalSpecification": {
                            "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                            "createdBy": _preferences.getString("username"),
                            "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                            "modifiedBy": _preferences.getString("username")
                        },
                        "jobSurveyor": ""
                    }
                        :
                    null,
                    "userCredentialMS2": {
                        "nik": _preferences.getString("username"),
                        "branchCode": _preferences.getString("branchid"),
                        "fullName": _preferences.getString("fullname"),
                        "role": _preferences.getString("job_name")
                    },
                },
                "ms2TaskID": _preferences.getString("order_no")
            }) : jsonEncode({
                "applicationDTO": {
                    "customerDTO": {
                        "customerID": _preferences.getString("last_known_state") != "IDE" ? _providerRincian.customerID : "NEW",// perlu di rencanakan mau ditaruh di sqlite atau di sharedPreference
                        "customerType": _preferences.getString("cust_type") == "PER" ? "PER" : "COM",
                        "customerSpecification": {
                            "customerNPWP": {
                                "npwpAddress": _providerRincian.controllerNPWPAddress.text,//_providerInfoNasabah.radioValueIsHaveNPWP == 1 ? _providerInfoNasabah.controllerAlamatSesuaiNPWP.text : _providerInfoAlamat.listAlamatKorespondensi[0].address,
                                "npwpFullname": _npwpFullName,//_providerInfoNasabah.radioValueIsHaveNPWP == 1 ? _providerInfoNasabah.controllerNamaSesuaiNPWP.text  : _providerInfoNasabah.controllerNamaLengkapSesuaiIdentitas.text,
                                "npwpNumber": _npwpNumber,//_providerInfoNasabah.radioValueIsHaveNPWP == 1 ? _providerInfoNasabah.controllerNoNPWP.text : "00000000000000",
                                "npwpType": _npwpType,
                                "hasNpwp": _providerRincian.radioValueIsHaveNPWP == 1 ? true : false,//_providerInfoNasabah.radioValueIsHaveNPWP == 1 ? true : false,
                                "pkpIdentifier": _providerRincian.signPKPSelected != null ? _providerRincian.signPKPSelected.id : "2",
                            },
                            "detailAddresses": _detailAddressesInfoNasabah,
                            "customerGuarantor": {
                                "isGuarantor": _providerGuarantor.radioValueIsWithGuarantor == 0,
                                "guarantorCorporates": _guarantorCorporates,
                                "guarantorIndividuals": _guarantorIndividuals
                            }
                        },
                        "customerIndividualDTO": [],
                        "customerCorporateDTO": [
                            {
                                "customerCorporateID": _preferences.getString("last_known_state") != "IDE" ? _providerRincian.customerCorporateID : "CUST001",
                                "businessSpecification": {
                                    "economySector": _providerRincian.sectorEconomicModelSelected != null ? _providerRincian.sectorEconomicModelSelected.KODE : null,//_preferences.getString("cust_type") == "PER" ? "" : _providerRincian.sectorEconomicModelSelected.KODE,
                                    "businessField": _providerRincian.businessFieldModelSelected != null ? _providerRincian.businessFieldModelSelected.KODE : null ,//_preferences.getString("cust_type") == "PER" ? "" : _providerRincian.businessFieldModelSelected.KODE,
                                    "locationStatus": _providerRincian.locationStatusSelected != null ?  _providerRincian.locationStatusSelected.id : null,//_preferences.getString("cust_type") == "PER" ? "" : _providerRincian.locationStatusSelected.id,
                                    "businessLocation": _providerRincian.businessLocationSelected != null ? _providerRincian.businessLocationSelected.id : null,//_preferences.getString("cust_type") == "PER" ? "" : _providerRincian.businessLocationSelected.id,
                                    "employeeTotal": _providerRincian.controllerTotalEmployees.text != "" ? int.parse(_providerRincian.controllerTotalEmployees.text) : null,//_preferences.getString("cust_type") == "PER" ? 0 : double.parse(_providerRincian.controllerTotalEmployees.text.replaceAll(",", "")),
                                    "bussinessLengthInMonth": _providerRincian.controllerLamaUsahaBerjalan.text != "" ? double.parse(_providerRincian.controllerLamaUsahaBerjalan.text.replaceAll(",", "")) : null,//_preferences.getString("cust_type") == "PER" ? 0 : double.parse(_providerRincian.controllerLamaUsahaBerjalan.text.replaceAll(",", "")),
                                    "totalBussinessLengthInMonth": _providerRincian.controllerTotalLamaUsahaBerjalan.text != "" ? int.parse(_providerRincian.controllerTotalLamaUsahaBerjalan.text.replaceAll(",", "")) : null,//_preferences.getString("cust_type") == "PER" ? 0 : double.parse(_providerRincian.controllerTotalLamaUsahaBerjalan.text.replaceAll(",", "")),
                                },
                                "businessPermitSpecification": {
                                    "institutionType": _providerRincian.typeInstitutionSelected != null ?  _providerRincian.typeInstitutionSelected.PARA_ID : "",//_preferences.getString("cust_type") == "PER" ? "" : _providerRincian.typeInstitutionSelected.PARA_ID,
                                    "profile": _providerRincian.profilSelected != null ? _providerRincian.profilSelected.id : "",//_preferences.getString("cust_type") == "PER" ? "" : _providerRincian.profilSelected.id,
                                    "fullName": _providerRincian.controllerInstitutionName.text != "" ? _providerRincian.controllerInstitutionName.text : "",//_preferences.getString("cust_type") == "PER" ? "" : _providerRincian.controllerInstitutionName.text,
                                    "deedOfIncorporationNumber": null,
                                    "deedEndDate": null,
                                    "siupNumber": null,
                                    "siupStartDate": null,
                                    "tdpNumber": null,
                                    "tdpStartDate": null,
                                    "establishmentDate": _providerRincian.controllerDateEstablishment.text != "" ? "${_providerRincian.controllerDateEstablishment.text} 00:00:00" : "01-01-1900 00:00:00",//formatDateValidateAndSubmit(_providerRincian.initialDateForDateEstablishment),//_preferences.getString("cust_type") == "PER" ? "" : _providerRincian.controllerDateEstablishment.text,
                                    "isCompany": false,
                                    "authorizedCapital": 0,
                                    "paidCapital": 0
                                },
                                "managementPIC": {
                                    "picIdentity": {
                                        "identityType": _providerManajemenPIC.typeIdentitySelected != null ? _providerManajemenPIC.typeIdentitySelected.id : "",
                                        "identityNumber":  _providerManajemenPIC.controllerIdentityNumber.text != "" ? _providerManajemenPIC.controllerIdentityNumber.text : "",
                                        "identityName": _providerManajemenPIC.controllerFullNameIdentity.text != "" ? _providerManajemenPIC.controllerFullNameIdentity.text : "",
                                        "fullName": _providerManajemenPIC.controllerFullName.text != "" ? _providerManajemenPIC.controllerFullName.text : "",
                                        "alias": null,
                                        "title": null,
                                        "dateOfBirth": _providerManajemenPIC.controllerBirthOfDate.text != "" ? formatDateValidateAndSubmit(_providerManajemenPIC.initialDateForBirthOfDate) : "01-01-1900 00:00:00",//_preferences.getString("cust_type") == "PER" ? "" : _providerManajemenPIC.controllerBirthOfDate.text,
                                        "placeOfBirth": _providerManajemenPIC.controllerPlaceOfBirthIdentity.text != "" ? _providerManajemenPIC.controllerPlaceOfBirthIdentity.text : "",//_preferences.getString("cust_type") == "PER" ? "" : _providerManajemenPIC.controllerPlaceOfBirthIdentity.text,
                                        "placeOfBirthKabKota": _providerManajemenPIC.birthPlaceSelected != null ? _providerManajemenPIC.birthPlaceSelected.KABKOT_ID : "",//_preferences.getString("cust_type") == "PER" ? "" : _providerManajemenPIC.birthPlaceSelected.KABKOT_ID,
                                        "gender": null,
                                        "identityActiveStart": null,
                                        "identityActiveEnd": null,
                                        "isLifetime": null,
                                        "religion": null,
                                        "occupationID": null,
                                        "positionID": _providerManajemenPIC.positionSelected != null ? _providerManajemenPIC.positionSelected.PARA_FAMILY_TYPE_ID : "10", // "02"
                                        "maritalStatusID": "02"
                                    },
                                    "picContact": {
                                        "telephoneArea1": null,
                                        "telephone1": null,
                                        "telephoneArea2": null,
                                        "telephone2": null,
                                        "faxArea": null,
                                        "fax": null,
                                        "handphone": _providerManajemenPIC.controllerHandphoneNumber.text != "" ? "08${_providerManajemenPIC.controllerHandphoneNumber.text}" : "",//_preferences.getString("cust_type") == "PER" ? "" : _providerManajemenPIC.controllerHandphoneNumber.text,
                                        "email": _providerManajemenPIC.controllerEmail.text != "" ? _providerManajemenPIC.controllerEmail.text : "",//_preferences.getString("cust_type") == "PER" ? "" : _providerManajemenPIC.controllerEmail.text,
                                        "noWa": null,
                                        "noWa2": null,
                                        "noWa3": null
                                    },
                                    "picAddresses": [],//_detailAddressPIC,
                                    "shareholding": 0
                                },
                                "shareholdersCorporates": _shareholdersCorporates,
                                "shareholdersIndividuals": _pemegangSahamPribadi,
                                "dedupScore": 0.0,
                                "status": "ACTIVE",
                                "creationalSpecification": {
                                    "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                                    "createdBy": _preferences.getString("username"),
                                    "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                                    "modifiedBy": _preferences.getString("username"),
                                },
                            }
                        ],
                        "customerAdditionalInformation": null,
                        "customerProcessingCompletion": {
                            "lastKnownCustomerState": "CUSTOMER_GUARANTOR",
                            "nextKnownCustomerState": "CUSTOMER_GUARANTOR"
                        },
                        "creationalSpecification": {
                            "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                            "createdBy": _preferences.getString("username"),
                            "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                            "modifiedBy": _preferences.getString("username"),
                        },
                        "status": "ACTIVE",
                        "obligorID": _providerRincian.obligorID != "NEW" && _providerRincian.obligorID != "null" && _providerRincian.obligorID != null ? _providerRincian.obligorID : _preferences.getString("oid").toString(), // _preferences.getString("status_aoro") == "1" || _preferences.getString("status_aoro") == "2" ? _providerRincian.obligorID != "NEW" ? _providerRincian.obligorID : _preferences.getString("oid").toString() : "NEW",// _preferences.getString("last_known_state") != "IDE" ? _providerRincian.obligorID != "NEW" ? _providerRincian.obligorID : _preferences.getString("oid").toString() : "NEW",
                        "exposureInformations": null,
                        "customerIncomes": _customerIncomes
                    },
                    "orderDTO": {
                        "orderID": _preferences.getString("last_known_state") != "IDE" ? _providerApp.applNo : orderId,
                        "customerID": _preferences.getString("last_known_state") != "IDE" ? _providerRincian.customerID : "NEW",
                        "orderSpecification": {
                            "aosApprovalStatus": _preferences.getString("last_known_state") == "AOS" ? _providerSurvey.radioValueApproved : "",
                            "isMayor": false, // _preferences.getString("last_known_state") == "AOS" && _providerSurvey.radioValueApproved == "0" ? true : | _preferences.getString("is_mayor") == "0",
                            "hasFiducia": false,
                            "applicationSource": "003",
                            "orderDate": "${_providerApp.controllerOrderDate.text} 00:00:00",//di edit karena tgl orderDate lebih besar dr surveyAppointmentDate <formatDateValidateAndSubmit(_providerApp.initialDateOrder)>
                            "applicationDate": "${_providerApp.controllerOrderDate.text} 00:00:00",// di edit karena tgl applicationDate lebih besar dr surveyAppointmentDate <formatDateValidateAndSubmit(_providerApp.initialDateOrder)>
                            "surveyAppointmentDate": formatDateValidateAndSubmit(_providerApp.initialSurveyDate),
                            "orderType": _preferences.getString("status_aoro") != null ? _preferences.getString("status_aoro") : "0",//_preferences.getString("status_aoro"),
                            "isOpenAccount": true,
                            "accountFormNumber": null,
                            "sentraCID": _preferences.getString("SentraD"),
                            "unitCID": _preferences.getString("UnitD"),
                            "initialRecommendation": _providerApp.initialRecomendation,
                            "finalRecommendation": _providerApp.finalRecomendation,
                            "brmsScoring": _providerApp.brmsScoring,
                            "isInPlaceApproval": false,
                            "isPkSigned": _providerApp.isSignedPK,
                            "maxApprovalLevel": null,
                            "jenisKonsep": _providerApp.conceptTypeModelSelected != null ? _providerApp.conceptTypeModelSelected.id : "",
                            "jumlahObjek": _providerApp.controllerTotalObject.text == "" ? null : int.parse(_providerApp.controllerTotalObject.text),
                            "jenisProporsionalAsuransi": _providerApp.proportionalTypeOfInsuranceModelSelected != null ? _providerApp.proportionalTypeOfInsuranceModelSelected.kode : "",
                            "unitKe": _providerApp.numberOfUnitSelected == "" ? null : int.parse(_providerApp.numberOfUnitSelected),
                            "isWillingToAcceptInfo": false,
                            "applicationContractNumber": null,
                            "dealerNote": Provider.of<MarketingNotesChangeNotifier>(context,listen: false).controllerMarketingNotes.text,
                            "userDealer": "",
                            "orderDealer": "",
                            "flagSourceMS2": "103", // diganti dari 006 ke 103 sesuai arahan mas Oji (per tanggal 06.07.2021)
                            "orderSupportingDocuments": _orderSupportingDocuments,
                            "collateralTypeID": _providerKolateral.collateralTypeModel.id,
                            "applicationDocVerStatus": null
                        },
                        "orderProducts": [
                            {
                                "orderProductID": _preferences.getString("last_known_state") != "IDE" ? _providerUnitObject.applObjtID : orderProductId, // jika IDE ngambil dari nomer unit, selain IDE ngambil dari APPL_NO get data from DB API
                                "orderProductSpecification": {
                                    "financingTypeID": _providerUnitObject.typeOfFinancingModelSelected.financingTypeId,//_preferences.getString("cust_type") == "PER" ? _providerFoto.typeOfFinancingModelSelected.financingTypeId : _providerUnitObject.typeOfFinancingModelSelected.financingTypeId,
                                    "ojkBusinessTypeID": _providerUnitObject.businessActivitiesModelSelected.id,//_preferences.getString("cust_type") == "PER" ? _providerFoto.kegiatanUsahaSelected.id.toString() : _providerUnitObject.typeOfFinancingModelSelected.financingTypeId.toString(),
                                    "ojkBussinessDetailID": _providerUnitObject.businessActivitiesTypeModelSelected.id,//_preferences.getString("cust_type") == "PER" ? _providerFoto.jenisKegiatanUsahaSelected.id.toString() : _providerUnitObject.typeOfFinancingModelSelected.financingTypeId,
                                    "orderUnitSpecification": {
                                        "objectGroupID": _providerUnitObject.groupObjectSelected != null ? _providerUnitObject.groupObjectSelected.KODE : "",
                                        "objectID": _providerUnitObject.objectSelected != null ? _providerUnitObject.objectSelected.id : "",
                                        "productTypeID": _providerUnitObject.productTypeSelected != null ? _providerUnitObject.productTypeSelected.id : "",
                                        "objectBrandID": _providerUnitObject.brandObjectSelected != null ? _providerUnitObject.brandObjectSelected.id : "",
                                        "objectTypeID": _providerUnitObject.objectTypeSelected != null ? _providerUnitObject.objectTypeSelected.id : "",
                                        "objectModelID": _providerUnitObject.modelObjectSelected != null ? _providerUnitObject.modelObjectSelected.id : "",
                                        "objectUsageID": _providerUnitObject.objectUsageModel != null ? _providerUnitObject.objectUsageModel.id : "", // field pemakaian objek (unit)
                                        "objectPurposeID": _providerUnitObject.objectPurposeSelected != null ? _providerUnitObject.objectPurposeSelected.id : "", // field tujuan objek (unit)
                                        "modelDetail": _providerUnitObject.controllerDetailModel.text == "" ? null : _providerUnitObject.controllerDetailModel.text
                                    },
                                    "salesGroupID": _providerUnitObject.groupSalesSelected != null ? _providerUnitObject.groupSalesSelected.kode : "",
                                    "orderSourceID": _providerUnitObject.sourceOrderSelected != null ? _providerUnitObject.sourceOrderSelected.kode : "",
                                    "orderSourceName": _providerUnitObject.sourceOrderNameSelected != null ? _providerUnitObject.sourceOrderNameSelected.kode : "",
                                    "orderProductDealerSpecification": {
                                        "isThirdParty": false,
                                        "thirdPartyTypeID": _providerUnitObject.thirdPartyTypeSelected != null ? _providerUnitObject.thirdPartyTypeSelected.kode : "",
                                        "thirdPartyID": _providerUnitObject.thirdPartySelected != null ? _providerUnitObject.thirdPartySelected.kode : "",
                                        "thirdPartyActivity": _providerUnitObject.activitiesModel != null ? _providerUnitObject.activitiesModel.kode : null,
                                        "sentradID": _preferences.getString("SentraD"), //_providerUnitObject.thirdPartySelected != null ? _providerUnitObject.thirdPartySelected.sentraId : "",
                                        "unitdID": _preferences.getString("UnitD"), //_providerUnitObject.thirdPartySelected != null ? _providerUnitObject.thirdPartySelected.unitId : "",
                                        "dealerMatrix": _providerUnitObject.matriksDealerSelected != null ? _providerUnitObject.matriksDealerSelected.kode : ""
                                    },
                                    "programID": _providerUnitObject.programSelected != null ? _providerUnitObject.programSelected.kode : "",
                                    "rehabType": _providerUnitObject.rehabTypeSelected != null ? _providerUnitObject.rehabTypeSelected.id : "",
                                    "referenceNumber": _providerUnitObject.referenceNumberModel != null ? _providerUnitObject.referenceNumberModel.noPK : null,
                                    "applicationUnitContractNumber": null,
                                    "orderProductSaleses": _orderProductSaleses,
                                    "orderKaroseris": _orderKaroseris,
                                    "orderProductInsurances": _orderProductInsurances,
                                    "orderWmps": _orderWmps,
                                    "groupID": _providerUnitObject.grupIdSelected != null ? _providerUnitObject.grupIdSelected.kode : null
                                },
                                "orderCreditStructure": {
                                    "installmentTypeID": _providerCreditStructure.installmentTypeSelected != null ? _providerCreditStructure.installmentTypeSelected.id : "", // sebelumnya ngambil by kode
                                    "tenor": _providerCreditStructure.periodOfTimeSelected != null ? int.parse(_providerCreditStructure.periodOfTimeSelected) : "",
                                    "paymentMethodID": _providerCreditStructure.paymentMethodSelected != null ? _providerCreditStructure.paymentMethodSelected.id : "", // sebelumnya ngambil by kode
                                    "effectiveRate": _providerCreditStructure.controllerInterestRateEffective.text != "" ? double.parse(_providerCreditStructure.controllerInterestRateEffective.text) : "",
                                    "flatRate": _providerCreditStructure.controllerInterestRateFlat.text != "" ? double.parse(_providerCreditStructure.controllerInterestRateFlat.text) : "", //0.48501
                                    "objectPrice": _providerCreditStructure.controllerObjectPrice.text != "" ? double.parse(_providerCreditStructure.controllerObjectPrice.text.replaceAll(",", "")) : 0,
                                    "karoseriTotalPrice": _providerCreditStructure.controllerKaroseriTotalPrice.text != "" ? double.parse(_providerCreditStructure.controllerKaroseriTotalPrice.text.replaceAll(",", "")) : 0,
                                    "totalPrice": _providerCreditStructure.controllerTotalPrice.text != "" ? double.parse(_providerCreditStructure.controllerTotalPrice.text.replaceAll(",", "")) : 0,
                                    "nettDownPayment": _providerCreditStructure.controllerNetDP.text != "" ? double.parse(_providerCreditStructure.controllerNetDP.text.replaceAll(",", "")) : 0,
                                    "declineNInstallment": _providerCreditStructure.installmentDeclineN,
                                    "paymentOfYear": _providerCreditStructure.installmentTypeSelected != null ? _providerCreditStructure.installmentTypeSelected.id == "04" ? int.parse(_providerCreditStructure.controllerPaymentPerYear.text) : 1 : 1,
                                    "branchDownPayment": _providerCreditStructure.controllerBranchDP.text != "" ? double.parse(_providerCreditStructure.controllerBranchDP.text.replaceAll(",", "")) : 0,
                                    "grossDownPayment": _providerCreditStructure.controllerGrossDP.text != "" ? double.parse(_providerCreditStructure.controllerGrossDP.text.replaceAll(",", "")) : 0,
                                    "totalLoan": _providerCreditStructure.controllerTotalLoan.text != "" ? double.parse(_providerCreditStructure.controllerTotalLoan.text.replaceAll(",", "")) : 0,
                                    "installmentAmount": _providerCreditStructure.controllerInstallment.text != "" ? double.parse(_providerCreditStructure.controllerInstallment.text.replaceAll(",", "")) : 0,
                                    "ltvRatio": _providerCreditStructure.controllerLTV.text != "" ? double.parse(_providerCreditStructure.controllerLTV.text.replaceAll(",", "")) : 0.0,
                                    "pencairan": 0,
                                    "interestAmount": _providerCreditStructure.interestAmount,
                                    "gpType": null,
                                    "newTenor": 0,
                                    "totalStepping": 0,
                                    "installmentBalloonPayment": {
                                        "balloonType": _providerCreditStructureType.radioValueBalloonPHOrBalloonInstallment,
                                        "lastInstallmentPercentage": _providerCreditStructureType.controllerInstallmentPercentage.text != "" ? double.parse(_providerCreditStructureType.controllerInstallmentPercentage.text.replaceAll(",", "")) : 0,
                                        "lastInstallmentValue": _providerCreditStructureType.controllerInstallmentValue.text != "" ? double.parse(_providerCreditStructureType.controllerInstallmentValue.text.replaceAll(",", "")) : 0
                                    },
                                    "installmentSchedule": {
                                        "installmentType": _providerCreditStructure.installmentTypeSelected != null ? _providerCreditStructure.installmentTypeSelected.name : null, // OLD: ambil dari ID
                                        "paymentMethodType": _providerCreditStructure.paymentMethodSelected != null ? _providerCreditStructure.paymentMethodSelected.name : null, // OLD: ambil dari ID
                                        "installment": _providerCreditStructure.controllerInstallment.text != "" ? double.parse(_providerCreditStructure.controllerInstallment.text.replaceAll(",", "")) : 0,
                                        "installmentRound": 0,
                                        "installmentMethod": 0,
                                        "lastKnownOutstanding": 0,
                                        "minInterest": 0,
                                        "maxInterest": 0,
                                        "futureValue": 0,
                                        "isInstallment": false,
                                        "roundingValue": null,
                                        "balloonInstallment": 0,
                                        "gracePeriode": 0,
                                        "gracePeriodes": [],
                                        "seasonal": 0,
                                        "steppings": [],
                                        "installmentScheduleDetails": []
                                    },
                                    "dsr": _providerCreditIncome.controllerDebtComparison.text != "" ? double.parse(_providerCreditIncome.controllerDebtComparison.text.replaceAll(",", "")) : 0,
                                    "dir": _providerCreditIncome.controllerIncomeComparison.text != "" ? double.parse(_providerCreditIncome.controllerIncomeComparison.text.replaceAll(",", "")) : 0,
                                    "dsc": _providerCreditIncome.controllerDSC.text != "" ? double.parse(_providerCreditIncome.controllerDSC.text.replaceAll(",", "")) : 0.0,
                                    "irr": _providerCreditIncome.controllerIRR.text != "" ? double.parse(_providerCreditIncome.controllerIRR.text.replaceAll(",", "")) : 0.0,
                                    "installmentDetails": _installmentDetails,
                                    "uangMukaKaroseri": 0, // diset 0
                                    "uangMukaChasisNet": _providerCreditStructure.controllerNetDP.text != "" ? double.parse(_providerCreditStructure.controllerNetDP.text.replaceAll(",", "")) : 0, // set sama kaya nettDownPayment
                                    "uangMukaChasisGross": _providerCreditStructure.controllerGrossDP.text != "" ? double.parse(_providerCreditStructure.controllerGrossDP.text.replaceAll(",", "")) : 0, // set sama kaya grossDownPayment
                                    // [
                                    //   {
                                    //     "installmentID": null,
                                    //     "installmentNumber": 0,
                                    //     "percentage": 0,
                                    //     "amount": 0,
                                    //     "creationalSpecification": {
                                    //       "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                                    //       "createdBy": "10056030",
                                    //       "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                                    //       "modifiedBy": "10056030"
                                    //     },
                                    //     "status": "ACTIVE"
                                    //   }
                                    // ],
                                    "realDSR": 0 // di jadikan model dari get dsr
                                },
                                "orderFees": _orderFees,
                                "orderSubsidies": [],
                                "orderProductAdditionalSpecification": {
                                    "virtualAccountID": null,
                                    "virtualAccountValue": 0,
                                    "cashingPurposeID": null,
                                    "cashingTypeID": null
                                },
                                "creationalSpecification": {
                                    "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                                    "createdBy": _preferences.getString("username"),
                                    "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                                    "modifiedBy": _preferences.getString("username"),
                                },
                                "status": "ACTIVE",
                                "collateralAutomotives": _providerKolateral.collateralTypeModel.id == "001"
                                    ?
                                [
                                    {
                                        "collateralID": _preferences.getString("last_known_state") != "IDE" ? _providerKolateral.collateralID : "CAU001", // NEW | _providerKolateral.collateralTypeModel != null ? _providerKolateral.collateralTypeModel.id : "",
                                        "isCollaSameUnit": _providerKolateral.radioValueIsCollateralSameWithUnitOto, // Jika Collateral = Unit dipilih YA, maka kirim TRUE. Begitu juga sebaliknya (0 = YA/true | 1 = TIDAK/false)
                                        "isMultiUnitCollateral": _providerKolateral.radioValueForAllUnitOto, // ya = false = 0 | tidak = true = 1
                                        "isCollaNameSameWithCustomerName": _providerKolateral.radioValueIsCollaNameSameWithApplicantOto, // Jika Nama Jaminan = Pemohon dipilih YA, maka kirim TRUE. Begitu juga sebaliknya (0 = YA/true | 1 = TIDAK/false)
                                        "identitySpecification": {
                                            "identityType": _providerKolateral.identityTypeSelectedAuto != null ? _providerKolateral.identityTypeSelectedAuto.id : "",
                                            "identityNumber":  _providerKolateral.controllerIdentityNumberAuto.text,
                                            "identityName": _providerKolateral.controllerNameOnCollateralAuto.text,
                                            "fullName" : _providerKolateral.controllerNameOnCollateralAuto.text,
                                            "alias": null,
                                            "title": null,
                                            "dateOfBirth": _providerKolateral.controllerBirthDateAuto.text != "" ? _providerKolateral.controllerBirthDateAuto.text+" 00:00:00" : null,
                                            "placeOfBirth": null, //_providerKolateral.controllerBirthPlaceValidWithIdentityAuto.text != "" ? _providerKolateral.controllerBirthPlaceValidWithIdentityAuto.text : "",
                                            "placeOfBirthKabKota": null, //_providerKolateral.birthPlaceAutoSelected != null ? _providerKolateral.birthPlaceAutoSelected.KABKOT_ID : _providerInfoNasabah.birthPlaceSelected.KABKOT_ID,
                                            "gender": null,//_providerKolateral.radioValueIsCollaNameSameWithApplicantOto == 0 ? _providerInfoNasabah.radioValueGender : null,
                                            "identityActiveStart": null,
                                            "identityActiveEnd": null,
                                            "religion": null, // _providerKolateral.radioValueIsCollaNameSameWithApplicantOto == 0 ? _providerInfoNasabah.religionSelected == null ? null : _providerInfoNasabah.religionSelected.id : null
                                            "occupationID": null,//_providerFoto.occupationSelected != null ? _providerFoto.occupationSelected.KODE : "",
                                            "positionID": null
                                        },
                                        "collateralAutomotiveSpecification": {
                                            "orderUnitSpecification": {
                                                "objectGroupID": _providerKolateral.groupObjectSelected != null ? _providerKolateral.groupObjectSelected.KODE : null,
                                                "objectID": _providerKolateral.objectSelected != null ? _providerKolateral.objectSelected.id : null,
                                                "productTypeID": _providerKolateral.productTypeSelected != null ? _providerKolateral.productTypeSelected.id : null,
                                                "objectBrandID":  _providerKolateral.brandObjectSelected != null ? _providerKolateral.brandObjectSelected.id : null,
                                                "objectTypeID": _providerKolateral.objectTypeSelected != null ? _providerKolateral.objectTypeSelected.id : null,
                                                "objectModelID": _providerKolateral.modelObjectSelected != null ? _providerKolateral.modelObjectSelected.id : null,
                                                "objectUsageID":  "", // tidak dipakai
                                                "objectPurposeID": _providerKolateral.objectUsageSelected != null ? _providerKolateral.objectUsageSelected.id : null, // field tujuan penggunaan collateral
                                            },
                                            "mpAdiraUpld": _providerKolateral.controllerMPAdiraUpload.text != "" ? double.parse(_providerKolateral.controllerMPAdiraUpload.text.replaceAll(",", "")) : null,
                                            "productionYear": _providerKolateral.yearProductionSelected != null ? int.parse(_providerKolateral.yearProductionSelected) : "",
                                            "registrationYear": _providerKolateral.yearRegistrationSelected != null ? int.parse(_providerKolateral.yearRegistrationSelected) : "",
                                            "isYellowPlate": _providerKolateral.radioValueYellowPlat, // per tanggal 10.05.2021 = dirubah ga dirubah jadi true false (OLD: == 0 ? false : true)
                                            "isBuiltUp": _providerKolateral.radioValueBuiltUpNonATPM == 0 ? true : false,
                                            "utjSpecification": {
                                                "bpkbNumber": _providerKolateral.controllerBPKPNumber.text != "" ? _providerKolateral.controllerBPKPNumber.text : null,
                                                "chassisNumber":  _providerKolateral.controllerFrameNumber.text != "" ? _providerKolateral.controllerFrameNumber.text : null,
                                                "engineNumber": _providerKolateral.controllerMachineNumber.text != "" ? _providerKolateral.controllerMachineNumber.text : null,
                                                "policeNumber": _providerKolateral.controllerPoliceNumber.text != "" ? _providerKolateral.controllerPoliceNumber.text : null,
                                            },
                                            "gradeUnit": _providerKolateral.controllerGradeUnit.text != "" ? _providerKolateral.controllerGradeUnit.text : null,
                                            "utjFacility": _providerKolateral.controllerFasilitasUTJ.text != "" ? _providerKolateral.controllerFasilitasUTJ.text == "TUNAI" ? "1" : "0" : null,
                                            "bidderName": _providerKolateral.controllerNamaBidder.text != "" ? _providerKolateral.controllerNamaBidder.text : null,
                                            "showroomPrice": _providerKolateral.controllerHargaJualShowroom.text != "" ? double.parse(_providerKolateral.controllerHargaJualShowroom.text.replaceAll(",", "")).round() : "",
                                            "additionalEquipment": _providerKolateral.controllerPerlengkapanTambahan.text != "" ? _providerKolateral.controllerPerlengkapanTambahan.text : "",
                                            "mpAdira":  _providerKolateral.controllerMPAdira.text != "" ? double.parse(_providerKolateral.controllerMPAdira.text.replaceAll(",", "")) : null,
                                            "physicalRecondition": _providerKolateral.controllerRekondisiFisik.text != "" ? double.parse(_providerKolateral.controllerRekondisiFisik.text.replaceAll(",", "")) : 0,
                                            "isProper": _providerKolateral.radioValueWorthyOrUnworthy, // 0 = layak = false | 1 = tidak layak = true
                                            "ltvRatio": 0.0,
                                            "appraisalSpecification": {
                                                "collateralDp": double.parse(_providerKolateral.controllerDPJaminan.text == '' ? '0' : _providerKolateral.controllerDPJaminan.text.replaceAll(",", "")),
                                                "maximumPh": double.parse(_providerKolateral.controllerPHMaxAutomotive.text == '' ? '0' : _providerKolateral.controllerPHMaxAutomotive.text.replaceAll(",", "")),
                                                "appraisalPrice": double.parse(_providerKolateral.controllerTaksasiPriceAutomotive.text == '' ? '0' : _providerKolateral.controllerTaksasiPriceAutomotive.text.replaceAll(",", "")),
                                                "collateralUtilizationID": _providerKolateral.collateralUsageOtoSelected != null ? _providerKolateral.collateralUsageOtoSelected.id : "" // field tujuan penggunaan collateral
                                            },
                                            "collateralAutomotiveAdditionalSpecification": {
                                                "capacity": 0,
                                                "color": null,
                                                "manufacturer": null,
                                                "serialNumber": null,
                                                "invoiceNumber": null,
                                                "stnkActiveDate": null,
                                                "bpkbAddress": null,
                                                "bpkbIdentityTypeID": null
                                            },
                                        },
                                        "status": "ACTIVE",
                                        "creationalSpecification": {
                                            "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                                            "createdBy": _preferences.getString("username"),
                                            "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                                            "modifiedBy": _preferences.getString("username"),
                                        }
                                    }
                                ] : [],
                                "collateralProperties": _providerKolateral.collateralTypeModel.id == "002"
                                    ?
                                [
                                    {
                                        "isMultiUnitCollateral": true, // _providerKolateral.radioValueForAllUnitOto, // ya = false = 0 | tidak = true = 1
                                        "isCollaNameSameWithCustomerName": _providerKolateral.radioValueIsCollateralSameWithApplicantProperty == 0,
                                        "collateralID": _preferences.getString("last_known_state") != "IDE" ? _providerKolateral.collateralID : "COLP001", // NEW
                                        "identitySpecification": {
                                            "identityType": _providerKolateral.identityModel != null ? _providerKolateral.identityModel.id : "",
                                            "identityNumber": _providerKolateral.controllerIdentityNumber.text,
                                            "identityName": null,
                                            "fullName": _providerKolateral.controllerNameOnCollateral.text,
                                            "alias": null,
                                            "title": null,
                                            "dateOfBirth": _providerKolateral.controllerBirthDateProp.text != "" ? _providerKolateral.controllerBirthDateProp.text : null,
                                            "placeOfBirth": _providerKolateral.birthPlaceSelected.KABKOT_NAME,
                                            "placeOfBirthKabKota": _providerKolateral.birthPlaceSelected.KABKOT_ID,
                                            "gender": null,
                                            "identityActiveStart": null,
                                            "identityActiveEnd": null,
                                            "isLifetime": null,
                                            "religion": null,
                                            "occupationID": null,
                                            "positionID": null,
                                            "maritalStatusID": null
                                        },
                                        "detailAddresses": [
                                            {
                                                "addressID": _preferences.getString("last_known_state") != "IDE" ? _providerKolateral.addressID != "NEW" ? _providerKolateral.addressID : "NEW" : "NEW",
                                                "foreignBusinessID": _preferences.getString("last_known_state") != "IDE" ? _providerKolateral.foreignBusinessID != "NEW" ? _providerKolateral.foreignBusinessID : "CAU001" : "CAU001", // NEW
                                                "addressSpecification": {
                                                    "koresponden": _providerKolateral.isKorespondensi.toString(), // tanya oji
                                                    "matrixAddr": _preferences.getString("cust_type") == "PER" ? "6": "13",
                                                    "address": _providerKolateral.controllerAddress.text,
                                                    "rt": _providerKolateral.controllerRT.text,
                                                    "rw": _providerKolateral.controllerRW.text,
                                                    "provinsiID": _providerKolateral.kelurahanSelected.PROV_ID,
                                                    "kabkotID": _providerKolateral.kelurahanSelected.KABKOT_ID,
                                                    "kecamatanID": _providerKolateral.kelurahanSelected.KEC_ID,
                                                    "kelurahanID": _providerKolateral.kelurahanSelected.KEL_ID,
                                                    "zipcode": _providerKolateral.kelurahanSelected.ZIPCODE
                                                },
                                                "contactSpecification": {
                                                    "telephoneArea1": null,
                                                    "telephone1": null,
                                                    "telephoneArea2": null,
                                                    "telephone2": null,
                                                    "faxArea": null,
                                                    "fax": null,
                                                    "handphone": null,
                                                    "email": null,
                                                    "noWa": null,
                                                    "noWa2": null,
                                                    "noWa3": null
                                                },
                                                "addressType": _providerKolateral.addressTypeSelected.KODE,
                                                "creationalSpecification": {
                                                    "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                                                    "createdBy": _preferences.getString("username"),
                                                    "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                                                    "modifiedBy": _preferences.getString("username"),
                                                },
                                                "status": "ACTIVE"
                                            }
                                        ],
                                        "collateralPropertySpecification": {
                                            "certificateNumber": _providerKolateral.controllerCertificateNumber.text,
                                            "certificateTypeID": _providerKolateral.certificateTypeSelected.id,
                                            "propertyTypeID": _providerKolateral.propertyTypeSelected.id,
                                            "buildingArea": double.parse(_providerKolateral.controllerBuildingArea.text.replaceAll(",", "")),
                                            "landArea": double.parse(_providerKolateral.controllerSurfaceArea.text.replaceAll(",", "")),
                                            "appraisalSpecification": {
                                                "collateralDp": double.parse(_providerKolateral.controllerDPJaminan.text.replaceAll(",", "")).toInt(),
                                                "maximumPh": double.parse(_providerKolateral.controllerPHMax.text == '' ? '0' : _providerKolateral.controllerPHMax.text.replaceAll(",", "")),
                                                "appraisalPrice": double.parse(_providerKolateral.controllerTaksasiPrice.text.replaceAll(",", "")).toInt(),
                                                "collateralUtilizationID": _providerKolateral.collateralUsagePropertySelected != null ? _providerKolateral.collateralUsagePropertySelected.id : "" //""
                                                // "collateralUtilizationID": "05"//tanya oji
                                            },
                                            "positiveFasumDistance": double.parse(_providerKolateral.controllerJarakFasumPositif.text.replaceAll(",", "")).toInt(),
                                            "negativeFasumDistance": double.parse(_providerKolateral.controllerJarakFasumNegatif.text.replaceAll(",", "")).toInt(),
                                            "landPrice": double.parse(_providerKolateral.controllerHargaTanah.text.replaceAll(",", "")).toInt(),
                                            "njopPrice": double.parse(_providerKolateral.controllerHargaNJOP.text.replaceAll(",", "")).toInt(),
                                            "buildingPrice": double.parse(_providerKolateral.controllerHargaBangunan.text.replaceAll(",", "")).toInt(),
                                            "roofTypeID": _providerKolateral.typeOfRoofSelected != null ? _providerKolateral.typeOfRoofSelected.id : null,
                                            "wallTypeID": _providerKolateral.wallTypeSelected != null ? _providerKolateral.wallTypeSelected.id : null,
                                            "floorTypeID": _providerKolateral.floorTypeSelected != null ? _providerKolateral.floorTypeSelected.id : null,
                                            "foundationTypeID": _providerKolateral.foundationTypeSelected != null ? _providerKolateral.foundationTypeSelected.id : null,
                                            "roadTypeID": _providerKolateral.streetTypeSelected != null ? _providerKolateral.streetTypeSelected.id : null,
                                            "isCarPassable": _providerKolateral.radioValueAccessCar == 0,
                                            "totalOfHouseInRadius": _providerKolateral.controllerJumlahRumahDalamRadius.text != "" ? int.parse(_providerKolateral.controllerJumlahRumahDalamRadius.text) : 0,
                                            "sifatJaminan": _providerKolateral.controllerSifatJaminan.text,
                                            "buktiKepemilikanTanah": _providerKolateral.controllerBuktiKepemilikan.text,
                                            "tgglTerbitSertifikat": _providerKolateral.controllerCertificateReleaseDate.text != "" ? formatDateValidateAndSubmit(_providerKolateral.initialDateCertificateRelease) : "",
                                            "tahunTerbitSertifikat": _providerKolateral.controllerCertificateReleaseYear.text,
                                            "namaPemegangHak": _providerKolateral.controllerNamaPemegangHak.text,
                                            "noSuratUkurGmbrSituasi": _providerKolateral.controllerNoSuratUkur.text,
                                            "tgglSuratUkurGmbrSituasi": _providerKolateral.controllerDateOfMeasuringLetter.text != "" ? formatDateValidateAndSubmit(_providerKolateral.initialDateOfMeasuringLetter) : "",
                                            "sertifikatSuratUkurDikeluarkanOleh": _providerKolateral.controllerCertificateOfMeasuringLetter.text,
                                            "masaBerlakuHak": _providerKolateral.controllerMasaHakBerlaku.text,
                                            "noImb": _providerKolateral.controllerNoIMB.text,
                                            "tgglImb": _providerKolateral.controllerDateOfIMB.text != "" ? formatDateValidateAndSubmit(_providerKolateral.initialDateOfIMB) : "",
                                            "luasBangunanImb": double.parse(_providerKolateral.controllerLuasBangunanIMB.text.replaceAll(",", "")),
                                            "ltvRatio": 0,
                                            "letakTanah": null,
                                            "propertyGroupObjtID": null,
                                            "propertyObjtID": null
                                        },
                                        "status": "INACTIVE",
                                        "creationalSpecification": {
                                            "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                                            "createdBy": _preferences.getString("username"),
                                            "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                                            "modifiedBy": _preferences.getString("username"),
                                        }
                                    }
                                ]
                                    : [],
                                "orderProductProcessingCompletion": {
                                    "lastKnownOrderProductState": "APPLICATION_DOCUMENT",
                                    "nextKnownOrderProductState": "APPLICATION_DOCUMENT"
                                },
                                "isWithoutColla": _providerKolateral.collateralTypeModel.id == "003" ? 1 : 0, // _providerUnitObject.groupObjectSelected.KODE == "003" ? 1 : 0,
                                "isSaveKaroseri": false
                            }
                        ],
                        "orderProcessingCompletion": {
                            "calculatedAt": 0,
                            "lastKnownOrderState": null,
                            "lastKnownOrderStatePosition": null,
                            "lastKnownOrderStatus": null,
                            "lastKnownOrderHandledBy": null
                        },
                        "creationalSpecification": {
                            "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                            "createdBy": _preferences.getString("username"),
                            "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                            "modifiedBy": _preferences.getString("username"),
                        },
                        "status": "ACTIVE"
                    },
                    "surveyDTO": _preferences.getString("last_known_state") != "IDE"
                        ?
                    {
                        "surveyID": _dataResultSurvey.isNotEmpty ? _dataResultSurvey[0]['surveyID'] != null ? _dataResultSurvey[0]['surveyID'] : "NEW" : "NEW", // dibuat seperti ini karena ada kemungkinan IDE di buat di ACCTION, sehingga survey id sudah tergenerate saat melakukan SRE di MS2
                        "orderID": _preferences.getString("last_known_state") != "IDE" ? _providerApp.applNo : orderId,
                        "surveyType": "002",
                        "employeeID": _preferences.getString("username"),
                        "phoneNumber": null,
                        "janjiSurveyDate": formatDateValidateAndSubmit(_providerApp.initialSurveyDate),
                        "reason": "",
                        "flagBRMS": "BRMS",
                        "surveyStatus": "01",
                        "status": "ACTIVE",
                        "surveyProcess": "02",
                        "surveyResultSpecification": {
                            "surveyResultType": (_preferences.getString("last_known_state") == "AOS" || _preferences.getString("last_known_state") == "CONA") && _providerSurvey.radioValueApproved == "0"
                                ? _providerSurvey.resultSurveySelected != null
                                ? _providerSurvey.resultSurveySelected.KODE : "000"
                                :  _providerSurvey.resultSurveySelected != null
                                ? _providerSurvey.resultSurveySelected.KODE : "",
                            "recomendationType": _providerSurvey.recommendationSurveySelected != null ? _providerSurvey.recommendationSurveySelected.KODE : null,
                            "notes": _providerSurvey.controllerNote.text,
                            "surveyResultDate":  "${_providerSurvey.controllerResultSurveyDate.text} ${_providerSurvey.controllerResultSurveyTime.text}:00", //formatDateValidateAndSubmit(_providerSurvey.initialDateResultSurvey),
                            "distanceWithSentra": _providerSurvey.controllerDistanceLocationWithSentraUnit.text != "" ? double.parse(_providerSurvey.controllerDistanceLocationWithSentraUnit.text.replaceAll(",", "")).toInt() : 0,
                            "distanceWithDealer": _providerSurvey.controllerDistanceLocationWithDealerMerchantAXI.text != "" ? double.parse(_providerSurvey.controllerDistanceLocationWithDealerMerchantAXI.text.replaceAll(",", "")).toInt() : 0,
                            "distanceObjWithSentra": _providerSurvey.controllerDistanceLocationWithUsageObjectWithSentraUnit.text != "" ? double.parse(_providerSurvey.controllerDistanceLocationWithUsageObjectWithSentraUnit.text.replaceAll(",", "")).toInt() : 0,
                            "surveyResultAssets": _surveyResultAssets,
                            // [
                            //     {
                            //         "surveyResultAssetID": "NEW",
                            //         "assetType": _providerSurvey.assetTypeSelected != null ? _providerSurvey.assetTypeSelected.id : null,
                            //         "assetAmount": double.parse(_providerSurvey.controllerValueAsset.text.replaceAll(",", "")).toInt(),
                            //         "ownStatus": _providerSurvey.ownershipSelected != null ? _providerSurvey.ownershipSelected.id : null,
                            //         "sizeofLand": _providerSurvey.controllerSurfaceBuildingArea.text,
                            //         "streetType": _providerSurvey.roadTypeSelected != null ? _providerSurvey.roadTypeSelected.kode : null,
                            //         "electricityType": _providerSurvey.electricityTypeSelected != null ? _providerSurvey.electricityTypeSelected.PARA_ELECTRICITY_ID : null,
                            //         "electricityBill": _providerSurvey.controllerElectricityBills.text != "" ? double.parse(_providerSurvey.controllerElectricityBills.text.replaceAll(",", "")).toInt() : 0,
                            //         "expiredContractDate": _providerSurvey.controllerEndDateLease.text != "" ? formatDateValidateAndSubmit(_providerSurvey.initialEndDateLease) : null,
                            //         "longOfStay": _providerSurvey.controllerLengthStay.text,
                            //         "status": "ACTIVE",
                            //         "creationalSpecification": {
                            //             "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                            //             "createdBy": _preferences.getString("username"),
                            //             "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                            //             "modifiedBy": _preferences.getString("username")
                            //         }
                            //     }
                            // ],
                            "surveyResultDetails": _surveyResultDetails,
                            "surveyResultTelesurveys": []
                        },
                        "creationalSpecification": {
                            "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                            "createdBy": _preferences.getString("username"),
                            "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                            "modifiedBy": _preferences.getString("username")
                        },
                        "jobSurveyor": ""
                    }
                        :
                    null,
                    "userCredentialMS2": {
                        "nik": _preferences.getString("username"),
                        "branchCode": _preferences.getString("branchid"),
                        "fullName": _preferences.getString("fullname"),
                        "role": _preferences.getString("job_name")
                    },
                },
                "ms2TaskID": _preferences.getString("order_no")
            });

        // await http.post(
        //     "http://192.168.57.149/orca_api/public/login",
        //     body: _body,
        //     headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
        // );

        String _validateIDE = await storage.read(key: "ValidateIDE");
        DateTime _timeStartValidate = DateTime.now();
        insertLog(context,_timeStartValidate,DateTime.now(),_body,"Start Validate","validate ${_preferences.getString("last_known_state")}");
        try{
            final _response = await _http.post(
                // "http://192.168.57.122/orca_api/public/login",
                "${BaseUrl.urlGeneral}$_validateIDE",
                body: _body,
                headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
            ).timeout(Duration(seconds: 300));
            debugPrint("halooo = ${_response.body}");
            final _data = jsonDecode(_response.body);
            if(_response.statusCode == 201){
                if(_data['status'] == "1"){
                    loadData = false;
                    _showSnackBarSuccess("Validate ${_data['message']}");
                    insertLog(context,_timeStartValidate,DateTime.now(),_body,jsonEncode(_data),"validate ${_preferences.getString("last_known_state")}");
                    if(_preferences.getString("last_known_state") != "DKR") {
                        submitData(_body, context);
                    } else {
                        submitDakor(context, orderId, orderProductId);
                    }
                }
                else{
                    loadData = false;
                    insertLog(context,_timeStartValidate,DateTime.now(),_body,jsonEncode(_data),"validate ${_preferences.getString("last_known_state")}");
                    _showSnackBar("${_data['message']}");
                }
            }
            else{
                loadData = false;
                insertLog(context,_timeStartValidate,DateTime.now(),_body,jsonEncode(_data),"validate ${_preferences.getString("last_known_state")}");
                print(_response.statusCode);
                _showSnackBar("Error ${_response.statusCode}");
            }
        }
        on TimeoutException catch(_){
            loadData = false;
            _showSnackBar("Timeout connection APK");
        }
        catch(e){
            print("masuk catch");
            loadData = false;
            insertLog(context,_timeStartValidate,DateTime.now(),_body,e.toString(),"validate ${_preferences.getString("last_known_state")}");
            // print(_response.statusCode);
            _showSnackBar("Error validate ${e.toString()}");
        }
    }

    void submitData(String body,BuildContext context) async {
        loadData = true;
        messageProcess = "Proses submit data...";
        SharedPreferences _preferences = await SharedPreferences.getInstance();
        var _providerResultSurvey = Provider.of<ResultSurveyChangeNotifier>(context, listen: false);
        final ioc = new HttpClient();
        ioc.badCertificateCallback =
            (X509Certificate cert, String host, int port) => true;

        final _http = IOClient(ioc);
        String urlAcction = await storage.read(key: "urlAcction");
        String _submitIDE = await storage.read(key: "SubmitIDE");
        String _submitRegulerSurvey = await storage.read(key: "SubmitRegulerSurvey");
        String _submitPAC = await storage.read(key: "SubmitPAC");
        String _submitIA = await storage.read(key: "SubmitIA");
        String _submitAOSCMOApprove = await storage.read(key: "AOSCMOApprove");
        String _submitAOSCMOUnapprove = await storage.read(key: "AOSCMOUnapprove");
        String _submitAOSCFOApprove = await storage.read(key: "AOSCFOApprove");
        String _submitAOSCFOUnapprove = await storage.read(key: "AOSCFOUnapprove");
        // String _submitIAdanPACNDS = await storage.read(key: "SubmitIA(approve)danPACNDS");
        // String _submitIAUnapprovePACDS = await storage.read(key: "SubmitIA(unapprove)PACDS");
        // String _submitDakor = await storage.read(key: "SubmitDakor");

        if(_preferences.getString("last_known_state") == "IDE") {
            // IDE
            DateTime _timeStartValidate = DateTime.now();
            insertLog(context, _timeStartValidate, DateTime.now(), body, "Start Submit IDE", "submit ${_preferences.getString("last_known_state")}");
            try {
                final _response = await _http.post(
                    // "${BaseUrl.urlAcction}adira-acction-071043/acction/service/mss/ide",
                    // "${urlAcction}adira-acction-071043/acction/service/mss/ide",
                    "${BaseUrl.urlGeneral}$_submitIDE",
                    body: body,
                    headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
                ).timeout(Duration(seconds: 300));

                // print("cek response submit ${_response.statusCode}");
                print("cek response code submit ${_response.statusCode}");

                debugPrint("cek response code submit ${_response.body}");
                insertLog(context, _timeStartValidate, DateTime.now(), body, _response.body, "submit ${_preferences.getString("last_known_state")}");
                _insertApplNoDeleteTask(context);

                // if(_response.statusCode == 200){
                //     _showSnackBar("Berhasil Submit");
                //     Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context) => HomePage()), (Route <dynamic> route) => false);
                // }
                // else{
                //     _showSnackBar("Error ${_response.statusCode}");
                // }
            }
            on TimeoutException catch(_){
                loadData = false;
                _showSnackBar("Timeout connection APK submit IDE");
            }
            catch(e){
                // print("cek error submit ${e.toString()}");
                loadData = false;
                insertLog(context, _timeStartValidate, DateTime.now(), body, e.toString(), "submit ${_preferences.getString("last_known_state")}");
                _showSnackBar("Error submit data ${e.toString()}");
            }
        }
        else if(_preferences.getString("last_known_state") == "SRE" || _preferences.getString("last_known_state") == "RSVY") {
            DateTime _timeStartValidate = DateTime.now();
            insertLog(context, _timeStartValidate, DateTime.now(), body, "Start Submit IDE", "submit ${_preferences.getString("last_known_state")}");
            try {
                final _response = await _http.post(
                    // "${BaseUrl.urlAcction}adira-acction-071043/acction/service/mss/ide",
                    // "${urlAcction}adira-acction-071043/acction/service/mss/ide",
                    "${BaseUrl.urlAcction}$_submitRegulerSurvey",
                    body: body,
                    headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
                ).timeout(Duration(seconds: 300));

                // print("cek response submit ${_response.statusCode}");
                print("cek response code submit ${_response.statusCode}");

                debugPrint("cek response code submit ${_response.body}");
                insertLog(context, _timeStartValidate, DateTime.now(), body, _response.body, "submit ${_preferences.getString("last_known_state")}");
                _insertApplNoDeleteTask(context);

                // if(_response.statusCode == 200){
                //     _showSnackBar("Berhasil Submit");
                //     Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context) => HomePage()), (Route <dynamic> route) => false);
                // }
                // else{
                //     _showSnackBar("Error ${_response.statusCode}");
                // }
            }
            on TimeoutException catch(_){
                loadData = false;
                _showSnackBar("Timeout connection APK submit IDE");
            }
            catch(e){
                // print("cek error submit ${e.toString()}");
                loadData = false;
                insertLog(context, _timeStartValidate, DateTime.now(), body, e.toString(), "submit ${_preferences.getString("last_known_state")}");
                _showSnackBar("Error submit data ${e.toString()}");
            }
        }
        else if(_preferences.getString("last_known_state") == "PAC") {
            // IA
            DateTime _timeStartValidate = DateTime.now();
            insertLog(context, _timeStartValidate, DateTime.now(), body, "Start Submit ${_preferences.getString("last_known_state")}", "Start Submit ${_preferences.getString("last_known_state")}");
            if(Provider.of<InformationObjectUnitChangeNotifier>(context, listen: false).groupSalesSelected.kode == "000W") {
                // PAC NDS
                try{
                    final _response = await _http.post(
                        "${BaseUrl.urlAcction}$_submitPAC",
                        // "${urlAcction}adira-acction-071043/acction/service/mss/sre/ia/nds",
                        body: body,
                        headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
                    ).timeout(Duration(seconds: 300));

                    debugPrint("cek response submit ${_preferences.getString("last_known_state")} ${_response.toString()} ");
                    _showSnackBar("Berhasil Submit ${_preferences.getString("last_known_state")}");
                    insertLog(context, _timeStartValidate, DateTime.now(), body,  _response.toString(), "Submit ${_preferences.getString("last_known_state")}");
                    if(_providerResultSurvey.resultSurveySelected != null) {
                        if(_providerResultSurvey.resultSurveySelected.KODE != "001") {
                            _insertApplNoDeleteTask(context);
                        } else {
                            loadData = false;
                            messageProcess = "";
                            Future.delayed(Duration(seconds: 1), () {
                                Navigator.pop(context);
                            });
                        }
                    }

                    // if(_response.statusCode == 201){
                    //   var _result = jsonDecode(_response.body);
                    //   if(_result['status'] == "1"){
                    //     debugPrint("cek response submit ${_preferences.getString("last_known_state")} $_result ");
                    //     _showSnackBar("Berhasil Submit ${_preferences.getString("last_known_state")}");
                    //     _insertApplNoDeleteTask(context);
                    //     insertLog(context, _timeStartValidate, DateTime.now(), body, jsonEncode(_result), "submit ${_preferences.getString("last_known_state")}");
                    //   }
                    //   else{
                    //     loadData = false;
                    //     insertLog(context, _timeStartValidate, DateTime.now(), body, jsonEncode(_result), "submit ${_preferences.getString("last_known_state")}");
                    //     _showSnackBar("Gagal Submit Read Timeout ${_preferences.getString("last_known_state")}");
                    //   }
                    //   // Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context) => HomePage()), (Route <dynamic> route) => false);
                    // }
                    // else{
                    //   loadData = false;
                    //   _showSnackBar("Error ${_response.statusCode}");
                    // }
                }
                on TimeoutException catch(_){
                    loadData = false;
                    debugPrint("Timeout connection APK submit ${_preferences.getString("last_known_state")} ");
                    _showSnackBar("Timeout connection APK submit ${_preferences.getString("last_known_state")}");
                }
                catch(e){
                    loadData = false;
                    debugPrint("Error submit data ${_preferences.getString("last_known_state")} ${e.toString()}");
                    insertLog(context, _timeStartValidate, DateTime.now(), body, e.toString(), "Submit ${_preferences.getString("last_known_state")}");
                    _showSnackBar("Error submit data ${_preferences.getString("last_known_state")}  ${e.toString()}");
                }
            }
            else {
                // PAC DS
                try{
                    final _response = await _http.post(
                        "${BaseUrl.urlAcction}$_submitIA",
                        // "${urlAcction}adira-acction-071043/acction/service/mss/sre/ia",
                        body: body,
                        headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
                    );

                    debugPrint("cek response submit ${_preferences.getString("last_known_state")} ${_response.toString()} ");
                    _showSnackBar("Berhasil Submit ${_preferences.getString("last_known_state")}");
                    _insertApplNoDeleteTask(context);
                    insertLog(context, _timeStartValidate, DateTime.now(), body,  _response.toString(), "submit ${_preferences.getString("last_known_state")}");

                    // if(_response.statusCode == 201){
                    //   var _result = jsonDecode(_response.body);
                    //   if(_result['status'] == "1"){
                    //     debugPrint("cek response submit ${_preferences.getString("last_known_state")} $_result ");
                    //     _showSnackBar("Berhasil Submit ${_preferences.getString("last_known_state")}");
                    //     _insertApplNoDeleteTask(context);
                    //     insertLog(context, _timeStartValidate, DateTime.now(), body, jsonEncode(_result), "submit ${_preferences.getString("last_known_state")}");
                    //   }
                    //   else{
                    //     loadData = false;
                    //     debugPrint("cek response submit ${_preferences.getString("last_known_state")} $_result ");
                    //     insertLog(context, _timeStartValidate, DateTime.now(), body, jsonEncode(_result), "submit ${_preferences.getString("last_known_state")}");
                    //     _showSnackBar("Gagal Submit Read Timeout ${_preferences.getString("last_known_state")}");
                    //   }
                    //   // Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context) => HomePage()), (Route <dynamic> route) => false);
                    // }
                    // else{
                    //   loadData = false;
                    //   debugPrint("cek response submit ${_preferences.getString("last_known_state")} ${_response.body} ");
                    //   _showSnackBar("Error ${_response.statusCode}");
                    // }
                }
                on TimeoutException catch(_){
                    loadData = false;
                    debugPrint("Timeout connection APK submit ${_preferences.getString("last_known_state")} ");
                    _showSnackBar("Timeout connection APK submit ${_preferences.getString("last_known_state")}");
                }
                catch(e){
                    loadData = false;
                    debugPrint("Timeout connection APK submit ${e.toString()} ${_preferences.getString("last_known_state")} ");
                    insertLog(context, _timeStartValidate, DateTime.now(), body, e.toString(), "submit ${_preferences.getString("last_known_state")}");
                    _showSnackBar("Error submit data ${e.toString()} ${_preferences.getString("last_known_state")} ");
                }
            }
        }
        else if(_preferences.getString("last_known_state") == "IA") {
            // IA
            DateTime _timeStartValidate = DateTime.now();
            insertLog(context, _timeStartValidate, DateTime.now(), body, "Start Submit ${_preferences.getString("last_known_state")}", "Start Submit ${_preferences.getString("last_known_state")}");
            if(Provider.of<InformationObjectUnitChangeNotifier>(context, listen: false).groupSalesSelected.kode == "000W") {
                // PAC NDS
                try{
                    final _response = await _http.post(
                        "${BaseUrl.urlAcction}$_submitIA",
                        // "${urlAcction}adira-acction-071043/acction/service/mss/sre/ia/nds",
                        body: body,
                        headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
                    ).timeout(Duration(seconds: 300));

                    debugPrint("cek response submit ${_preferences.getString("last_known_state")} ${_response.toString()} ");
                    _showSnackBar("Berhasil Submit ${_preferences.getString("last_known_state")}");
                    insertLog(context, _timeStartValidate, DateTime.now(), body,  _response.toString(), "Submit ${_preferences.getString("last_known_state")}");
                    if(_providerResultSurvey.resultSurveySelected != null) {
                        if(_providerResultSurvey.resultSurveySelected.KODE != "001") {
                            _insertApplNoDeleteTask(context);
                        } else {
                            loadData = false;
                            messageProcess = "";
                            Future.delayed(Duration(seconds: 1), () {
                                Navigator.pop(context);
                            });
                        }
                    }

                    // if(_response.statusCode == 201){
                    //   var _result = jsonDecode(_response.body);
                    //   if(_result['status'] == "1"){
                    //     debugPrint("cek response submit ${_preferences.getString("last_known_state")} $_result ");
                    //     _showSnackBar("Berhasil Submit ${_preferences.getString("last_known_state")}");
                    //     _insertApplNoDeleteTask(context);
                    //     insertLog(context, _timeStartValidate, DateTime.now(), body, jsonEncode(_result), "submit ${_preferences.getString("last_known_state")}");
                    //   }
                    //   else{
                    //     loadData = false;
                    //     insertLog(context, _timeStartValidate, DateTime.now(), body, jsonEncode(_result), "submit ${_preferences.getString("last_known_state")}");
                    //     _showSnackBar("Gagal Submit Read Timeout ${_preferences.getString("last_known_state")}");
                    //   }
                    //   // Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context) => HomePage()), (Route <dynamic> route) => false);
                    // }
                    // else{
                    //   loadData = false;
                    //   _showSnackBar("Error ${_response.statusCode}");
                    // }
                }
                on TimeoutException catch(_){
                    loadData = false;
                    debugPrint("Timeout connection APK submit ${_preferences.getString("last_known_state")} ");
                    _showSnackBar("Timeout connection APK submit ${_preferences.getString("last_known_state")}");
                }
                catch(e){
                    loadData = false;
                    debugPrint("Error submit data ${_preferences.getString("last_known_state")} ${e.toString()}");
                    insertLog(context, _timeStartValidate, DateTime.now(), body, e.toString(), "Submit ${_preferences.getString("last_known_state")}");
                    _showSnackBar("Error submit data ${_preferences.getString("last_known_state")}  ${e.toString()}");
                }
            }
            else {
                // PAC DS
                try{
                    final _response = await _http.post(
                        "${BaseUrl.urlAcction}$_submitIA",
                        // "${urlAcction}adira-acction-071043/acction/service/mss/sre/ia",
                        body: body,
                        headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
                    );

                    debugPrint("cek response submit ${_preferences.getString("last_known_state")} ${_response.toString()} ");
                    _showSnackBar("Berhasil Submit ${_preferences.getString("last_known_state")}");
                    _insertApplNoDeleteTask(context);
                    insertLog(context, _timeStartValidate, DateTime.now(), body,  _response.toString(), "submit ${_preferences.getString("last_known_state")}");

                    // if(_response.statusCode == 201){
                    //   var _result = jsonDecode(_response.body);
                    //   if(_result['status'] == "1"){
                    //     debugPrint("cek response submit ${_preferences.getString("last_known_state")} $_result ");
                    //     _showSnackBar("Berhasil Submit ${_preferences.getString("last_known_state")}");
                    //     _insertApplNoDeleteTask(context);
                    //     insertLog(context, _timeStartValidate, DateTime.now(), body, jsonEncode(_result), "submit ${_preferences.getString("last_known_state")}");
                    //   }
                    //   else{
                    //     loadData = false;
                    //     debugPrint("cek response submit ${_preferences.getString("last_known_state")} $_result ");
                    //     insertLog(context, _timeStartValidate, DateTime.now(), body, jsonEncode(_result), "submit ${_preferences.getString("last_known_state")}");
                    //     _showSnackBar("Gagal Submit Read Timeout ${_preferences.getString("last_known_state")}");
                    //   }
                    //   // Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context) => HomePage()), (Route <dynamic> route) => false);
                    // }
                    // else{
                    //   loadData = false;
                    //   debugPrint("cek response submit ${_preferences.getString("last_known_state")} ${_response.body} ");
                    //   _showSnackBar("Error ${_response.statusCode}");
                    // }
                }
                on TimeoutException catch(_){
                    loadData = false;
                    debugPrint("Timeout connection APK submit ${_preferences.getString("last_known_state")} ");
                    _showSnackBar("Timeout connection APK submit ${_preferences.getString("last_known_state")}");
                }
                catch(e){
                    loadData = false;
                    debugPrint("Timeout connection APK submit ${e.toString()} ${_preferences.getString("last_known_state")} ");
                    insertLog(context, _timeStartValidate, DateTime.now(), body, e.toString(), "submit ${_preferences.getString("last_known_state")}");
                    _showSnackBar("Error submit data ${e.toString()} ${_preferences.getString("last_known_state")} ");
                }
            }
        }
        else if(_preferences.getString("last_known_state") == "AOS") {
            // AOS
            DateTime _timeStartValidate = DateTime.now();
            insertLog(context, _timeStartValidate, DateTime.now(), body, "Start Submit ${_preferences.getString("last_known_state")}", "Start Submit ${_preferences.getString("last_known_state")}");
            try{
                final _response =  await _http.post(_providerResultSurvey.radioValueApproved == "1"
                    ? "${BaseUrl.urlAcction}$_submitAOSCMOApprove" : "${BaseUrl.urlAcction}$_submitAOSCMOUnapprove",
                    // "${urlAcction}adira-acction-071043/acction/service/mss/sre/ia/nds",
                    body: body,
                    headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
                ).timeout(Duration(seconds: 300));

                debugPrint("cek response submit ${_preferences.getString("last_known_state")} ${_response.toString()} ");
                _showSnackBar("Berhasil Submit ${_preferences.getString("last_known_state")}");
                insertLog(context, _timeStartValidate, DateTime.now(), body,  _response.toString(), "Submit ${_preferences.getString("last_known_state")}");
                if(_providerResultSurvey.resultSurveySelected != null) {
                    if(_providerResultSurvey.resultSurveySelected.KODE != "001") {
                        _insertApplNoDeleteTask(context);
                    } else {
                        loadData = false;
                        messageProcess = "";
                        Future.delayed(Duration(seconds: 1), () {
                            Navigator.pop(context);
                        });
                    }
                } else {
                    _insertApplNoDeleteTask(context);
                }

                // if(_response.statusCode == 201){
                //   var _result = jsonDecode(_response.body);
                //   if(_result['status'] == "1"){
                //     debugPrint("cek response submit ${_preferences.getString("last_known_state")} $_result ");
                //     _showSnackBar("Berhasil Submit ${_preferences.getString("last_known_state")}");
                //     _insertApplNoDeleteTask(context);
                //     insertLog(context, _timeStartValidate, DateTime.now(), body, jsonEncode(_result), "submit ${_preferences.getString("last_known_state")}");
                //   }
                //   else{
                //     loadData = false;
                //     insertLog(context, _timeStartValidate, DateTime.now(), body, jsonEncode(_result), "submit ${_preferences.getString("last_known_state")}");
                //     _showSnackBar("Gagal Submit Read Timeout ${_preferences.getString("last_known_state")}");
                //   }
                //   // Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context) => HomePage()), (Route <dynamic> route) => false);
                // }
                // else{
                //   loadData = false;
                //   _showSnackBar("Error ${_response.statusCode}");
                // }
            }
            on TimeoutException catch(_){
                loadData = false;
                debugPrint("Timeout connection APK submit ${_preferences.getString("last_known_state")} ");
                _showSnackBar("Timeout connection APK submit ${_preferences.getString("last_known_state")}");
            }
            catch(e){
                loadData = false;
                debugPrint("Error submit data ${_preferences.getString("last_known_state")} ${e.toString()}");
                insertLog(context, _timeStartValidate, DateTime.now(), body, e.toString(), "Submit ${_preferences.getString("last_known_state")}");
                _showSnackBar("Error submit data ${_preferences.getString("last_known_state")}  ${e.toString()}");
            }
        }
        else if(_preferences.getString("last_known_state") == "CONA") {
            // AOS
            DateTime _timeStartValidate = DateTime.now();
            insertLog(context, _timeStartValidate, DateTime.now(), body, "Start Submit ${_preferences.getString("last_known_state")}", "Start Submit ${_preferences.getString("last_known_state")}");
            try{
                final _response =  await _http.post(_providerResultSurvey.radioValueApproved == "1"
                    ? "${BaseUrl.urlAcction}$_submitAOSCFOApprove" : "${BaseUrl.urlAcction}$_submitAOSCFOUnapprove",
                    // "${urlAcction}adira-acction-071043/acction/service/mss/sre/ia/nds",
                    body: body,
                    headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
                ).timeout(Duration(seconds: 300));

                debugPrint("cek response submit ${_preferences.getString("last_known_state")} ${_response.toString()} ");
                _showSnackBar("Berhasil Submit ${_preferences.getString("last_known_state")}");
                insertLog(context, _timeStartValidate, DateTime.now(), body,  _response.toString(), "Submit ${_preferences.getString("last_known_state")}");
                if(_providerResultSurvey.resultSurveySelected != null) {
                    if(_providerResultSurvey.resultSurveySelected.KODE != "001") {
                        _insertApplNoDeleteTask(context);
                    } else {
                        loadData = false;
                        messageProcess = "";
                        Future.delayed(Duration(seconds: 1), () {
                            Navigator.pop(context);
                        });
                    }
                } else {
                    _insertApplNoDeleteTask(context);
                }

                // if(_response.statusCode == 201){
                //   var _result = jsonDecode(_response.body);
                //   if(_result['status'] == "1"){
                //     debugPrint("cek response submit ${_preferences.getString("last_known_state")} $_result ");
                //     _showSnackBar("Berhasil Submit ${_preferences.getString("last_known_state")}");
                //     _insertApplNoDeleteTask(context);
                //     insertLog(context, _timeStartValidate, DateTime.now(), body, jsonEncode(_result), "submit ${_preferences.getString("last_known_state")}");
                //   }
                //   else{
                //     loadData = false;
                //     insertLog(context, _timeStartValidate, DateTime.now(), body, jsonEncode(_result), "submit ${_preferences.getString("last_known_state")}");
                //     _showSnackBar("Gagal Submit Read Timeout ${_preferences.getString("last_known_state")}");
                //   }
                //   // Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context) => HomePage()), (Route <dynamic> route) => false);
                // }
                // else{
                //   loadData = false;
                //   _showSnackBar("Error ${_response.statusCode}");
                // }
            }
            on TimeoutException catch(_){
                loadData = false;
                debugPrint("Timeout connection APK submit ${_preferences.getString("last_known_state")} ");
                _showSnackBar("Timeout connection APK submit ${_preferences.getString("last_known_state")}");
            }
            catch(e){
                loadData = false;
                debugPrint("Error submit data ${_preferences.getString("last_known_state")} ${e.toString()}");
                insertLog(context, _timeStartValidate, DateTime.now(), body, e.toString(), "Submit ${_preferences.getString("last_known_state")}");
                _showSnackBar("Error submit data ${_preferences.getString("last_known_state")}  ${e.toString()}");
            }
        }
        // else if(_preferences.getString("last_known_state") == "DKR") {
        //     // Dakor
        //     final _response = await _http.post(
        //         // "${BaseUrl.urlAcction}adira-acction-071043/acction/service/mss/dakor",
        //         "${urlAcction}adira-acction-071043/acction/service/mss/dakor",
        //         body: body,
        //         headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
        //     );
        //
        //     if(_response.statusCode == 200){
        //         _showSnackBar("Berhasil Submit");
        //         Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context) => HomePage()), (Route <dynamic> route) => false);
        //     }
        //     else{
        //         _showSnackBar("Error ${_response.statusCode}");
        //     }
        // }
    }

    void submitDakor(BuildContext context, String orderId, String orderProductId) async {
        debugPrint("DAKOR_JALAN");
        DateTime _timeStartValidate = DateTime.now();
        List _dataResultSurvey = await _dbHelper.selectResultSurveyMS2Assignment();
        // Form IDE Individu
        // var _providerFoto = Provider.of<FormMFotoChangeNotifier>(context, listen: false);
        // var _providerInfoNasabah = Provider.of<FormMInfoNasabahChangeNotif>(context, listen: false);
        // var _providerInfoAlamatNasabah = Provider.of<FormMInfoAlamatChangeNotif>(context, listen: false);
        // var _providerIbu = Provider.of<FormMInformasiKeluargaIBUChangeNotif>(context, listen: false);
        // var _providerKeluarga = Provider.of<FormMInfoKeluargaChangeNotif>(context, listen: false);
        // var _providerOccupation = Provider.of<FormMOccupationChangeNotif>(context,listen: false);
        // var _providerIncome = Provider.of<FormMIncomeChangeNotifier>(context, listen: false);

        // Form IDE Company
        var _providerRincian = Provider.of<FormMCompanyRincianChangeNotifier>(context, listen: false);
        var _providerInfoAlamatCompany = Provider.of<FormMCompanyAlamatChangeNotifier>(context, listen: false);
        var _providerIncomeCompany = Provider.of<FormMCompanyPendapatanChangeNotifier>(context, listen: false);
        var _providerGuarantor = Provider.of<FormMGuarantorChangeNotifier>(context, listen: false);
        var _providerManajemenPIC = Provider.of<FormMCompanyManajemenPICChangeNotifier>(context, listen: false);
        var _providerPemegangSaham = Provider.of<PemegangSahamPribadiChangeNotifier>(context, listen: false);
        var _providerPemegangSahamCompany = Provider.of<PemegangSahamLembagaChangeNotifier>(context, listen: false);
        var _providerAddressManajemenPIC = Provider.of<FormMCompanyManajemenPICAlamatChangeNotifier>(context,listen: false);

        // Form App
        var _providerApp = Provider.of<InfoAppChangeNotifier>(context, listen: false);
        var _providerUnitObject = Provider.of<InformationObjectUnitChangeNotifier>(context, listen: false);
        var _providerKolateral = Provider.of<InformationCollateralChangeNotifier>(context, listen: false);
        var _providerSales = Provider.of<InformationSalesmanChangeNotifier>(context, listen: false);
        // var _providerAddSales = Provider.of<AddSalesmanChangeNotifier>(context, listen: false);
        var _providerInsurance = Provider.of<InfoMajorInsuranceChangeNotifier>(context, listen: false);
        var _providerAdditionalInsurance = Provider.of<InfoAdditionalInsuranceChangeNotifier>(context, listen: false);
        var _providerKaroseri = Provider.of<InfoObjectKaroseriChangeNotifier>(context, listen: false);
        var _providerCreditStructure = Provider.of<InfoCreditStructureChangeNotifier>(context, listen: false);
        var _providerCreditStructureType = Provider.of<InfoCreditStructureTypeInstallmentChangeNotifier>(context, listen: false);
        var _providerCreditIncome = Provider.of<InfoCreditIncomeChangeNotifier>(context, listen: false);
        var _providerWmp = Provider.of<InfoWMPChangeNotifier>(context,listen: false);
        var _providerSubsidy = Provider.of<InfoCreditSubsidyChangeNotifier>(context,listen: false);
        var _providerSurvey = Provider.of<ResultSurveyChangeNotifier>(context,listen:false);
        // var _providerSubsidyDetail = Provider.of<AddCreditSubsidyChangeNotifier>(context, listen: false);
        // var _providerCreditStructureTypeInstallment = Provider.of<InfoCreditStructureTypeInstallmentChangeNotifier>(context,listen: false);
        // var _providerDocument = Provider.of<InfoDocumentChangeNotifier>(context,listen: false);

        // var occupation = Provider.of<FormMFotoChangeNotifier>(context, listen: false).occupationSelected.KODE;
        // var _providerInfoDocument = Provider.of<InfoDocumentChangeNotifier>(context, listen: false);

        SharedPreferences _preferences = await SharedPreferences.getInstance();
        // var _customerIncomes, _detailAddresses, _installmentDetails, _guarantorIndividuals, _guarantorCorporates, _orderKaroseris,
        //     _orderWmps, _orderSubsidies, _familyInfoID, _orderProductSaleses, _orderProductInsurances, _orderFees = [];

        await setValueCustomerIncome(context);

        String _npwpType = "";
        if(_providerRincian.typeNPWPSelected != null) {
            _npwpType = _providerRincian.typeNPWPSelected != null ? _providerRincian.typeNPWPSelected.id : "2";
        } else {
            _npwpType = "2";
        }

        String _npwpAddress = "";
        if(_providerRincian.radioValueIsHaveNPWP == 1){
            for(int i=0; i < _providerInfoAlamatCompany.listAlamatKorespondensi.length; i++) {
                if(_providerInfoAlamatCompany.listAlamatKorespondensi[i].jenisAlamatModel.KODE == "03") {
                    _npwpAddress = _providerInfoAlamatCompany.listAlamatKorespondensi[i].address.toString();
                }
            }
            // _npwpAddress = _providerRincian.controllerNPWPAddress.text.isNotEmpty ? _providerRincian.controllerNPWPAddress.text : _providerInfoAlamatCompany.listAlamatKorespondensi[i].address.toString();
        }
        else {
            // _npwpAddress = "INDONESIA";
            for(int i=0; i < _providerInfoAlamatCompany.listAlamatKorespondensi.length; i++) {
                if(_providerInfoAlamatCompany.listAlamatKorespondensi[i].jenisAlamatModel.KODE == "03") {
                    _npwpAddress = _providerInfoAlamatCompany.listAlamatKorespondensi[i].address.toString();
                }
            }
        }

        String _npwpFullName = "";
        if(_providerRincian.radioValueIsHaveNPWP == 1){
            _npwpFullName = _providerRincian.controllerFullNameNPWP.text;
        } else {
            _npwpFullName = _providerRincian.controllerFullNameNPWP.text;
        }

        String _npwpNumber = "";
        if(_providerRincian.radioValueIsHaveNPWP == 1){
            _npwpNumber = _providerRincian.controllerNPWP.text;
        }
        else{
            _npwpNumber = "000000000000000";
        }

        var _detailAddressesInfoNasabah = [];
        for (int i = 0; i <_providerInfoAlamatCompany.listAlamatKorespondensi.length; i++) {
            _detailAddressesInfoNasabah.add({
                "addressID": _preferences.getString("last_known_state") != "IDE" ? _providerInfoAlamatCompany.listAlamatKorespondensi[i].addressID != "NEW" ? _providerInfoAlamatCompany.listAlamatKorespondensi[i].addressID : "NEW" : "NEW",
                "addressSpecification": {
                    "koresponden": _providerInfoAlamatCompany.listAlamatKorespondensi[i].isCorrespondence ? "1" : "0",
                    "matrixAddr": "10",
                    "address": _providerInfoAlamatCompany.listAlamatKorespondensi[i].address,
                    "rt": _providerInfoAlamatCompany.listAlamatKorespondensi[i].rt,
                    "rw": _providerInfoAlamatCompany.listAlamatKorespondensi[i].rw,
                    "provinsiID": _providerInfoAlamatCompany.listAlamatKorespondensi[i].kelurahanModel.PROV_ID,
                    "kabkotID": _providerInfoAlamatCompany.listAlamatKorespondensi[i].kelurahanModel.KABKOT_ID,
                    "kecamatanID": _providerInfoAlamatCompany.listAlamatKorespondensi[i].kelurahanModel.KEC_ID,
                    "kelurahanID": _providerInfoAlamatCompany.listAlamatKorespondensi[i].kelurahanModel.KEL_ID,
                    "zipcode": _providerInfoAlamatCompany.listAlamatKorespondensi[i].kelurahanModel.ZIPCODE
                },
                "contactSpecification": {
                    "telephoneArea1": _providerInfoAlamatCompany.listAlamatKorespondensi[i].phoneArea1,
                    "telephone1": _providerInfoAlamatCompany.listAlamatKorespondensi[i].phone1,
                    "telephoneArea2": _providerInfoAlamatCompany.listAlamatKorespondensi[i].phoneArea2 == "" ? null : _providerInfoAlamatCompany.listAlamatKorespondensi[i].phoneArea2,
                    "telephone2": _providerInfoAlamatCompany.listAlamatKorespondensi[i].phone2,
                    "faxArea": _providerInfoAlamatCompany.listAlamatKorespondensi[i].faxArea,
                    "fax": _providerInfoAlamatCompany.listAlamatKorespondensi[i].fax,
                    "handphone": "",
                    "email": ""
                },
                "addressType": _providerInfoAlamatCompany.listAlamatKorespondensi[i].jenisAlamatModel.KODE,
                "creationalSpecification": {
                    "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                    "createdBy": _preferences.getString("username"),
                    "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                    "modifiedBy": _preferences.getString("username"),
                },
                "status": _providerInfoAlamatCompany.listAlamatKorespondensi[i].active == 0 ? "ACTIVE" : "INACTIVE",
                "foreignBusinessID": _preferences.getString("last_known_state") != "IDE" ?
                _providerInfoAlamatCompany.listAlamatKorespondensi[i].foreignBusinessID != "NEW" ?
                _providerInfoAlamatCompany.listAlamatKorespondensi[i].foreignBusinessID :
                "CUST001" : "CUST001" // NEW
            });
        }

        // for(int i=0; i < _providerOccupation.listOccupationAddress.length; i++){
        //     _detailAddressesInfoNasabah.add({
        //         "addressID": _preferences.getString("last_known_state") != "IDE" ? _providerOccupation.listOccupationAddress[i].addressID : "NEW",
        //         "addressSpecification": {
        //             "koresponden": _providerOccupation.listOccupationAddress[i].isCorrespondence ? "1" : "0",
        //             "matrixAddr": "4",
        //             "address": _providerOccupation.listOccupationAddress[i].address,
        //             "rt": _providerOccupation.listOccupationAddress[i].rt,
        //             "rw": _providerOccupation.listOccupationAddress[i].rw,
        //             "provinsiID": _providerOccupation.listOccupationAddress[i].kelurahanModel.PROV_ID,
        //             "kabkotID": _providerOccupation.listOccupationAddress[i].kelurahanModel.KABKOT_ID,
        //             "kecamatanID": _providerOccupation.listOccupationAddress[i].kelurahanModel.KEC_ID,
        //             "kelurahanID": _providerOccupation.listOccupationAddress[i].kelurahanModel.KEL_ID,
        //             "zipcode": _providerOccupation.listOccupationAddress[i].kelurahanModel.ZIPCODE
        //         },
        //         "contactSpecification": {
        //             "telephoneArea1": _providerOccupation.listOccupationAddress[i].areaCode,
        //             "telephone1": _providerOccupation.listOccupationAddress[i].phone,
        //             "telephoneArea2": null,
        //             "telephone2": null,
        //             "faxArea": null,
        //             "fax": null,
        //             "handphone": "",
        //             "email": ""
        //         },
        //         "addressType": _providerOccupation.listOccupationAddress[i].jenisAlamatModel.KODE,
        //         "creationalSpecification": {
        //             "createdAt": formatDateValidateAndSubmit(DateTime.now()),
        //             "createdBy": _preferences.getString("username"),
        //             "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
        //             "modifiedBy": _preferences.getString("username"),
        //         },
        //         "status": _providerOccupation.listOccupationAddress[i].active == 0 ? "ACTIVE" : "INACTIVE",
        //         "foreignBusinessID": _preferences.getString("last_known_state") != "IDE" ? _providerOccupation.listOccupationAddress[i].addressID : "IP001", // NEW
        //     });
        // }

        // var _familyInfoID = [];
        // _familyInfoID.add(
        //     {
        //         "familyInfoID": _preferences.getString("last_known_state") != "IDE" ? _providerIbu.familyInfoID : "NEW",
        //         "relationshipStatus": "05",
        //         "familyIdentity": {
        //             "identityType": "",
        //             "identityNumber": "",
        //             "identityName": _providerIbu.controllerNamaIdentitas.text,
        //             "fullName": _providerIbu.controllerNamaLengkapdentitas.text,
        //             "alias": null,
        //             "title": null,
        //             "dateOfBirth": null,
        //             "placeOfBirth": "",
        //             "placeOfBirthKabKota": "",
        //             "gender": "02",
        //             "identityActiveStart": null,
        //             "identityActiveEnd": null,
        //             "religion": null,
        //             "occupationID": "0",
        //             "positionID": null
        //         },
        //         "creationalSpecification": {
        //             "createdAt": formatDateValidateAndSubmit(DateTime.now()),
        //             "createdBy": _preferences.getString("username"),
        //             "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
        //             "modifiedBy": _preferences.getString("username"),
        //         },
        //         "contactSpecification": {
        //             "telephoneArea1": _providerIbu.controllerKodeArea.text,
        //             "telephone1": _providerIbu.controllerTlpn.text,
        //             "telephoneArea2": null,
        //             "telephone2": null,
        //             "faxArea": null,
        //             "fax": null,
        //             "handphone": "",
        //             "email": null
        //         },
        //         "dedupScore": 0,
        //         "status": "ACTIVE"
        //     },
        // );
        // if (_providerKeluarga.listFormInfoKel.isNotEmpty) {
        //     for (int i = 0; i < _providerKeluarga.listFormInfoKel.length; i++) {
        //         if (_preferences.getString("cust_type") == "PER") {
        //             _familyInfoID.add({
        //                 "familyInfoID": _preferences.getString("last_known_state") != "IDE" ? _providerKeluarga.listFormInfoKel[i].familyInfoID : "NEW",
        //                 "relationshipStatus": _providerKeluarga.listFormInfoKel[i]
        //                     .relationshipStatusModel == null ? "" : _providerKeluarga
        //                     .listFormInfoKel[i].relationshipStatusModel.PARA_FAMILY_TYPE_ID,
        //                 "familyIdentity": {
        //                     "identityType": _providerKeluarga.listFormInfoKel[i]
        //                         .identityModel == null ? "" : _providerKeluarga
        //                         .listFormInfoKel[i].identityModel.id,
        //                     "identityNumber": _providerKeluarga.listFormInfoKel[i].noIdentitas,
        //                     "identityName": _providerKeluarga.listFormInfoKel[i]
        //                         .namaLengkapSesuaiIdentitas,
        //                     "fullName": _providerKeluarga.listFormInfoKel[i].namaLengkap,
        //                     "alias": null,
        //                     "title": null,
        //                     "dateOfBirth": _providerKeluarga.listFormInfoKel[i].birthDate == ""
        //                         ? null
        //                         : formatDateValidateAndSubmit(DateTime.parse(_providerKeluarga.listFormInfoKel[i].birthDate)),//"${_providerKeluarga.listFormInfoKel[i].birthDate} 00:00:00",
        //                     "placeOfBirth": _providerKeluarga.listFormInfoKel[i]
        //                         .tmptLahirSesuaiIdentitas,
        //                     "placeOfBirthKabKota": _providerKeluarga.listFormInfoKel[i]
        //                         .birthPlaceModel == null ? "" : _providerKeluarga
        //                         .listFormInfoKel[i].birthPlaceModel.KABKOT_ID,
        //                     "gender": _providerKeluarga.listFormInfoKel[i].gender,
        //                     "identityActiveStart": null,
        //                     "identityActiveEnd": null,
        //                     "religion": null,
        //                     "occupationID": "0",
        //                     "positionID": null
        //                 },
        //                 "creationalSpecification": {
        //                     "createdAt": formatDateValidateAndSubmit(DateTime.now()),
        //                     "createdBy": _preferences.getString("username"),
        //                     "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
        //                     "modifiedBy": _preferences.getString("username")
        //                 },
        //                 "contactSpecification": {
        //                     "telephoneArea1": _providerKeluarga.listFormInfoKel[i].kodeArea,
        //                     "telephone1": _providerKeluarga.listFormInfoKel[i].noTlpn,
        //                     "telephoneArea2": null,
        //                     "telephone2": null,
        //                     "faxArea": null,
        //                     "fax": null,
        //                     "handphone": _providerKeluarga.listFormInfoKel[i].noHp != "" ? "08${_providerKeluarga.listFormInfoKel[i].noHp}" : "",
        //                     "email": null
        //                 },
        //                 "dedupScore": 0,
        //                 "status": "ACTIVE"
        //             });
        //         }
        //     }
        // }

        var _guarantorIndividuals = [];
        if(_providerGuarantor.listGuarantorIndividual.isNotEmpty) {
            for(int i=0; i < _providerGuarantor.listGuarantorIndividual.length; i++) {
                List _detailAddressGuarantorIndividu = [];
                String _foreignBKGuarantorIndividu = "IKPG";
                if(_providerGuarantor.listGuarantorIndividual.length < 10) {
                    _foreignBKGuarantorIndividu = "IKPG00" + (i+1).toString();
                } else if(_providerGuarantor.listGuarantorIndividual.length < 100) {
                    _foreignBKGuarantorIndividu = "IKPG0" + (i+1).toString();
                } else if(_providerGuarantor.listGuarantorIndividual.length < 1000) {
                    _foreignBKGuarantorIndividu = "IKPG" + (i+1).toString();
                }
                if(_providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel.isNotEmpty){
                    for(int j=0; j < _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel.length; j++){
                        _detailAddressesInfoNasabah.add({
                            "addressID": _preferences.getString("last_known_state") != "IDE"
                                ? _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].addressID != "NEW"
                                ? _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].addressID : "NEW" : "NEW",
                            "addressSpecification": {
                                "koresponden": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].isCorrespondence ? "1" : "0",
                                "matrixAddr": _preferences.getString("cust_type") == "PER" ? "1" : "7",
                                "address": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].address,
                                "rt": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].rt,
                                "rw": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].rw,
                                "provinsiID": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].kelurahanModel.PROV_ID,
                                "kabkotID": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].kelurahanModel.KABKOT_ID,
                                "kecamatanID": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].kelurahanModel.KEC_ID,
                                "kelurahanID": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].kelurahanModel.KEL_ID,
                                "zipcode": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].kelurahanModel.ZIPCODE
                            },
                            "contactSpecification": {
                                "telephoneArea1": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].areaCode,
                                "telephone1": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].phone,
                                "telephoneArea2": null,
                                "telephone2": null,
                                "faxArea": null,
                                "fax": null,
                                "handphone": "",
                                "email": ""
                            },
                            "addressType": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].jenisAlamatModel.KODE,
                            "creationalSpecification": {
                                "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                                "createdBy": _preferences.getString("username"),
                                "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                                "modifiedBy": _preferences.getString("username"),
                            },
                            "status": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].active == 0 ? "ACTIVE" : "INACTIVE",
                            "foreignBusinessID": _preferences.getString("last_known_state") != "IDE"
                                ? _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].foreignBusinessID != "NEW"
                                ? _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].foreignBusinessID
                                : _foreignBKGuarantorIndividu : _foreignBKGuarantorIndividu
                        });
                        _detailAddressGuarantorIndividu.add({
                            "addressID": _preferences.getString("last_known_state") != "IDE"
                                ? _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].addressID != "NEW"
                                ? _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].addressID : "NEW" : "NEW",
                            "addressSpecification": {
                                "koresponden": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].isCorrespondence ? "1" : "0",
                                "matrixAddr": _preferences.getString("cust_type") == "PER" ? "1" : "7",
                                "address": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].address,
                                "rt": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].rt,
                                "rw": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].rw,
                                "provinsiID": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].kelurahanModel.PROV_ID,
                                "kabkotID": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].kelurahanModel.KABKOT_ID,
                                "kecamatanID": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].kelurahanModel.KEC_ID,
                                "kelurahanID": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].kelurahanModel.KEL_ID,
                                "zipcode": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].kelurahanModel.ZIPCODE
                            },
                            "contactSpecification": {
                                "telephoneArea1": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].areaCode,
                                "telephone1": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].phone,
                                "telephoneArea2": null,
                                "telephone2": null,
                                "faxArea": null,
                                "fax": null,
                                "handphone": "",
                                "email": ""
                            },
                            "addressType": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].jenisAlamatModel.KODE,
                            "creationalSpecification": {
                                "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                                "createdBy": _preferences.getString("username"),
                                "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                                "modifiedBy": _preferences.getString("username"),
                            },
                            "status": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].active == 0 ? "ACTIVE" : "INACTIVE",
                            "foreignBusinessID": _preferences.getString("last_known_state") != "IDE"
                                ? _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].foreignBusinessID != "NEW"
                                ? _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].foreignBusinessID
                                : _foreignBKGuarantorIndividu : _foreignBKGuarantorIndividu
                        });
                    }
                }
                _guarantorIndividuals.add(
                    {
                        "guarantorIndividualID": _preferences.getString("last_known_state") != "IDE"
                            ? _providerGuarantor.listGuarantorIndividual[i].guarantorIndividualID != null
                            ? _providerGuarantor.listGuarantorIndividual[i].guarantorIndividualID : _foreignBKGuarantorIndividu : _foreignBKGuarantorIndividu,
                        "relationshipStatus":_providerGuarantor.listGuarantorIndividual[i].relationshipStatusModel != null ?
                        _providerGuarantor.listGuarantorIndividual[i].relationshipStatusModel.PARA_FAMILY_TYPE_ID : "",
                        "dedupScore": 0.0,
                        "guarantorIdentity": {
                            "identityType": _providerGuarantor.listGuarantorIndividual[i].identityModel != null ?
                            _providerGuarantor.listGuarantorIndividual[i].identityModel.id : "",
                            "identityNumber": _providerGuarantor.listGuarantorIndividual[i].identityNumber,
                            "identityName": _providerGuarantor.listGuarantorIndividual[i].fullNameIdentity,
                            "fullName": _providerGuarantor.listGuarantorIndividual[i].fullName,
                            "alias": "",
                            "title": "",
                            "dateOfBirth": _providerGuarantor.listGuarantorIndividual[i].birthDate != "" ? formatDateValidateAndSubmit(DateTime.parse(_providerGuarantor.listGuarantorIndividual[i].birthDate)) : "01-01-1900 00:00:00",
                            "placeOfBirth": _providerGuarantor.listGuarantorIndividual[i].birthPlaceIdentity1 != "" ? _providerGuarantor.listGuarantorIndividual[i].birthPlaceIdentity1 : "",
                            "placeOfBirthKabKota": _providerGuarantor.listGuarantorIndividual[i].birthPlaceIdentity2 != null ? _providerGuarantor.listGuarantorIndividual[i].birthPlaceIdentity2.KABKOT_ID : "",
                            "gender": _providerGuarantor.listGuarantorIndividual[i].gender,
                            "identityActiveStart": null,
                            "identityActiveEnd": null,
                            "isLifetime": null,
                            "religion": null,
                            "occupationID": null,
                            "positionID": null,
                            "maritalStatusID": null
                        },
                        "guarantorContact": {
                            "telephoneArea1": null,
                            "telephone1": null,
                            "telephoneArea2": null,
                            "telephone2": null,
                            "faxArea": null,
                            "fax": null,
                            "handphone": _providerGuarantor.listGuarantorIndividual[i].cellPhoneNumber != "" && _providerGuarantor.listGuarantorIndividual[i].cellPhoneNumber != null ? "08${_providerGuarantor.listGuarantorIndividual[i].cellPhoneNumber}" : "",
                            "email": null,
                            "noWa": null,
                            "noWa2": null,
                            "noWa3": null
                        },
                        "guarantorIndividualAddresses": _detailAddressGuarantorIndividu,
                        "creationalSpecification": {
                            "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                            "createdBy": _preferences.getString("username"),
                            "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                            "modifiedBy": _preferences.getString("username"),
                        },
                        "status": "ACTIVE"
                    }
                );
            }
        }

        var _guarantorCorporates = [];
        if(_providerGuarantor.listGuarantorCompany.isNotEmpty) {
            for(int i=0; i < _providerGuarantor.listGuarantorCompany.length; i++) {
                String _npwpAddressGuarantorCorporate = "";
                List _detailAddressGuarantorCompany = [];
                String _foreignBKGuarantorCorporate = "IKGK";
                if(_providerGuarantor.listGuarantorCompany.length < 10) {
                    _foreignBKGuarantorCorporate = "IKGK00" + (i+1).toString();
                } else if(_providerGuarantor.listGuarantorCompany.length < 100) {
                    _foreignBKGuarantorCorporate = "IKGK0" + (i+1).toString();
                } else if(_providerGuarantor.listGuarantorCompany.length < 1000) {
                    _foreignBKGuarantorCorporate = "IKGK" + (i+1).toString();
                }
                if(_providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel.isNotEmpty){
                    for(int j=0; j < _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel.length; j++){
                        _detailAddressesInfoNasabah.add({
                            "addressID": _preferences.getString("last_known_state") != "IDE"
                                ? _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].addressID != "NEW"
                                ? _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].addressID : "NEW" : "NEW",
                            "addressSpecification": {
                                "koresponden": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].isCorrespondence ? "1" : "0",
                                "matrixAddr": _preferences.getString("cust_type") == "PER" ? "2" : "8",
                                "address": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].address,
                                "rt": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].rt,
                                "rw": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].rw,
                                "provinsiID": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].kelurahanModel.PROV_ID,
                                "kabkotID": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].kelurahanModel.KABKOT_ID,
                                "kecamatanID": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].kelurahanModel.KEC_ID,
                                "kelurahanID": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].kelurahanModel.KEL_ID,
                                "zipcode": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].kelurahanModel.ZIPCODE
                            },
                            "contactSpecification": {
                                "telephoneArea1": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].phoneArea1,
                                "telephone1": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].phone1,
                                "telephoneArea2": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].phoneArea2,
                                "telephone2": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].phone2,
                                "faxArea": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].faxArea,
                                "fax": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].fax,
                                "handphone": "",
                                "email": ""
                            },
                            "addressType": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].jenisAlamatModel.KODE,
                            "creationalSpecification": {
                                "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                                "createdBy": _preferences.getString("username"),
                                "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                                "modifiedBy": _preferences.getString("username"),
                            },
                            "status": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].active == 0 ? "ACTIVE" : "INACTIVE",
                            "foreignBusinessID": _preferences.getString("last_known_state") != "IDE"
                                ? _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].foreignBusinessID != "NEW"
                                ? _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].foreignBusinessID
                                : _foreignBKGuarantorCorporate : _foreignBKGuarantorCorporate
                        });
                        _detailAddressGuarantorCompany.add({
                            "addressID": _preferences.getString("last_known_state") != "IDE"
                                ? _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].addressID != "NEW"
                                 ? _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].addressID : "NEW" : "NEW",
                            "addressSpecification": {
                                "koresponden": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].isCorrespondence ? "1" : "0",
                                "matrixAddr": _preferences.getString("cust_type") == "PER" ? "2" : "8",
                                "address": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].address,
                                "rt": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].rt,
                                "rw": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].rw,
                                "provinsiID": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].kelurahanModel.PROV_ID,
                                "kabkotID": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].kelurahanModel.KABKOT_ID,
                                "kecamatanID": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].kelurahanModel.KEC_ID,
                                "kelurahanID": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].kelurahanModel.KEL_ID,
                                "zipcode": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].kelurahanModel.ZIPCODE
                            },
                            "contactSpecification": {
                                "telephoneArea1": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].phoneArea1,
                                "telephone1": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].phone1,
                                "telephoneArea2": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].phoneArea2,
                                "telephone2": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].phone2,
                                "faxArea": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].faxArea,
                                "fax": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].fax,
                                "handphone": "",
                                "email": ""
                            },
                            "addressType": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].jenisAlamatModel.KODE,
                            "creationalSpecification": {
                                "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                                "createdBy": _preferences.getString("username"),
                                "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                                "modifiedBy": _preferences.getString("username"),
                            },
                            "status": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].active == 0 ? "ACTIVE" : "INACTIVE",
                            "foreignBusinessID": _preferences.getString("last_known_state") != "IDE"
                                ? _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].foreignBusinessID != "NEW"
                                ? _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].foreignBusinessID
                                : _foreignBKGuarantorCorporate : _foreignBKGuarantorCorporate
                        });
                        if(_providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].jenisAlamatModel.KODE == "03") {
                            _npwpAddressGuarantorCorporate = _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].address;
                        }
                    }
                }
                _guarantorCorporates.add(
                    {
                        "guarantorCorporateID": _preferences.getString("last_known_state") != "IDE"
                            ? _providerGuarantor.listGuarantorCompany[i].guarantorCorporateID != null
                            ? _providerGuarantor.listGuarantorCompany[i].guarantorCorporateID : _foreignBKGuarantorCorporate : _foreignBKGuarantorCorporate,
                        "dedupScore": 0.0,
                        "businessPermitSpecification": {
                            "institutionType": _providerGuarantor.listGuarantorCompany[i].typeInstitutionModel.PARA_ID,
                            "profile": _preferences.getString("cust_type") == "PER" ? _providerGuarantor.listGuarantorCompany[i].profilModel.id : "",
                            "fullName": _providerGuarantor.listGuarantorCompany[i].institutionName,
                            "deedOfIncorporationNumber": null,
                            "deedEndDate": null,
                            "siupNumber": null,
                            "siupStartDate": null,
                            "tdpNumber": null,
                            "tdpStartDate": null,
                            "establishmentDate": formatDateValidateAndSubmit(DateTime.parse(_providerGuarantor.listGuarantorCompany[i].establishDate)),//"${_providerGuarantor.listGuarantorCompany[i].establishDate} 00:00:00",
                            "isCompany": false,
                            "authorizedCapital": 0,
                            "paidCapital": 0
                        },
                        "businessSpecification": {
                            "economySector": null,
                            "businessField": null,
                            "locationStatus": null,
                            "businessLocation": null,
                            "employeeTotal": 0,
                            "bussinessLengthInMonth": 0,
                            "totalBussinessLengthInMonth": 0
                        },
                        "customerNpwpSpecification": {
                            "hasNpwp": true,
                            "npwpAddress": _npwpAddressGuarantorCorporate,
                            "npwpFullname": _providerGuarantor.listGuarantorCompany[i].institutionName,
                            "npwpNumber": _providerGuarantor.listGuarantorCompany[i].npwp,
                            "npwpType": _npwpType,
                            "pkpIdentifier": _providerRincian.signPKPSelected != null ? _providerRincian.signPKPSelected.id : "2"
                        },
                        "guarantorCorporateAddresses": _detailAddressGuarantorCompany,
                        "creationalSpecification": {
                            "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                            "createdBy": _preferences.getString("username"),
                            "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                            "modifiedBy": _preferences.getString("username"),
                        },
                        "status": "ACTIVE"
                    }
                );
            }
        }

        var _orderProductSaleses = [];
        if(_providerSales.listInfoSales.isNotEmpty) {
            for(int i=0; i<_providerSales.listInfoSales.length; i++){
                _orderProductSaleses.add({
                    "orderProductSalesID": _preferences.getString("last_known_state") != "IDE" ? _providerSales.listInfoSales[i].orderProductSalesID : "NEW",
                    "salesType": _providerSales.listInfoSales[i].salesmanTypeModel.id,
                    "employeeJob": _providerSales.listInfoSales[i].positionModel.id == "" ? null : _providerSales.listInfoSales[i].positionModel.id,
                    "employeeID": _providerSales.listInfoSales[i].employeeModel == null ? '' : _providerSales.listInfoSales[i].employeeModel.NIK,
                    "employeeHeadID": _providerSales.listInfoSales[i].employeeHeadModel == null ? '' : _providerSales.listInfoSales[i].employeeHeadModel.NIK,
                    "referalContractNumber": null,
                    "creationalSpecification": {
                        "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                        "createdBy": _preferences.getString("username"),
                        "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                        "modifiedBy": _preferences.getString("username"),
                    },
                    "status": "ACTIVE"
                });
            }
        }

        var _orderKaroseris = [];
        if(_providerKaroseri.listFormKaroseriObject.isNotEmpty) {
            for(int i=0; i < _providerKaroseri.listFormKaroseriObject.length; i++) {
                _orderKaroseris.add(
                    {
                        "orderKaroseriID": _preferences.getString("last_known_state") != "IDE" ? _providerKaroseri.listFormKaroseriObject[i].orderKaroseriID : "NEW",
                        "isPksKaroseri": _providerKaroseri.listFormKaroseriObject[i].PKSKaroseri,
                        "karoseriCompanyID": _providerKaroseri.listFormKaroseriObject[i].company != null ? _providerKaroseri.listFormKaroseriObject[i].company.id : null,
                        "karoseriID": _providerKaroseri.listFormKaroseriObject[i].karoseri.kode,
                        "karoseriPrice": double.parse(_providerKaroseri.listFormKaroseriObject[i].price.replaceAll(",", "")),
                        "karoseriQty": _providerKaroseri.listFormKaroseriObject[i].jumlahKaroseri,
                        "karoseriTotalPrice": double.parse(_providerKaroseri.listFormKaroseriObject[i].totalPrice.replaceAll(",", "")),
                        "uangMukaKaroseri": 0,
                        "flagKaroseri": 1,
                        "creationalSpecification": {
                            "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                            "createdBy": _preferences.getString("username"),
                            "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                            "modifiedBy": _preferences.getString("username"),
                        },
                        "status": "ACTIVE"
                    }
                );
            }
        }

        var _installmentDetails = [];
        if(_providerCreditStructure.installmentTypeSelected.id == "06") {
            // Stepping
            if(_providerCreditStructureType.listInfoCreditStructureStepping.isNotEmpty) {
                for(int i=0; i < _providerCreditStructureType.listInfoCreditStructureStepping.length; i++) {
                    _installmentDetails.add({
                        "installmentID": _preferences.getString("last_known_state") != "IDE" ? null : "NEW",// nanti di bagian if di ambil dr sharedPreference
                        "installmentNumber": 0,
                        "percentage": _providerCreditStructureType.listInfoCreditStructureStepping[i].controllerPercentage.text != "" ? double.parse(_providerCreditStructureType.listInfoCreditStructureStepping[i].controllerPercentage.text.replaceAll(",", "")) : 0,
                        "amount": _providerCreditStructureType.listInfoCreditStructureStepping[i].controllerValue.text != "" ? double.parse(_providerCreditStructureType.listInfoCreditStructureStepping[i].controllerValue.text.replaceAll(",", "")) : 0,
                        "creationalSpecification": {
                            "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                            "createdBy": _preferences.getString("username"),
                            "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                            "modifiedBy": _preferences.getString("username"),
                        },
                        "status": "ACTIVE"
                    });
                }
            }
        }
        else if(_providerCreditStructure.installmentTypeSelected.id == "07") {
            // Irreguler
            if(_providerCreditStructureType.listInfoCreditStructureIrregularModel.isNotEmpty) {
                for(int i=0; i < _providerCreditStructureType.listInfoCreditStructureIrregularModel.length; i++) {
                    _installmentDetails.add({
                        "installmentID": _preferences.getString("last_known_state") != "IDE" ? null : "NEW",// nanti di bagian if di ambil dr sharedPreference
                        "installmentNumber": i+1,
                        "percentage": 0,
                        "amount": _providerCreditStructureType.listInfoCreditStructureIrregularModel[i].controller.text != "" ? double.parse(_providerCreditStructureType.listInfoCreditStructureIrregularModel[i].controller.text.replaceAll(",", "")) : 0,
                        "creationalSpecification": {
                            "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                            "createdBy": _preferences.getString("username"),
                            "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                            "modifiedBy": _preferences.getString("username"),
                        },
                        "status": "ACTIVE"
                    });
                }
            }
        }

        var _orderFees = [];
        if(_providerCreditStructure.listInfoFeeCreditStructure.isNotEmpty) {
            bool _isProvisiEmpty = true;
            bool _isAdminEmpty = true;
            for (int i = 0; i <  _providerCreditStructure.listInfoFeeCreditStructure.length; i++) {
                if(_providerCreditStructure.listInfoFeeCreditStructure[i].feeTypeModel.id == "01"){
                    _isAdminEmpty = false;
                }
                if(_providerCreditStructure.listInfoFeeCreditStructure[i].feeTypeModel.id == "02"){
                    _isProvisiEmpty = false;
                }
                _orderFees.add({
                    "orderFeeID": _preferences.getString("last_known_state") != "IDE" ? _providerCreditStructure.listInfoFeeCreditStructure[i].orderFeeID : "NEW",
                    "feeTypeID": _providerCreditStructure.listInfoFeeCreditStructure[i].feeTypeModel.id,
                    "cashFee": _providerCreditStructure.listInfoFeeCreditStructure[i].cashCost == "" ? null : double.parse(_providerCreditStructure.listInfoFeeCreditStructure[i].cashCost.replaceAll(",", "")).round(),
                    "creditFee": _providerCreditStructure.listInfoFeeCreditStructure[i].creditCost == "" ? null : double.parse(_providerCreditStructure.listInfoFeeCreditStructure[i].creditCost.replaceAll(",", "")).round(),
                    "totalFee": _providerCreditStructure.listInfoFeeCreditStructure[i].totalCost == "" ? null : double.parse(_providerCreditStructure.listInfoFeeCreditStructure[i].totalCost.replaceAll(",", "")).round(),
                    "creationalSpecification": {
                        "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                        "createdBy": _preferences.getString("username"),
                        "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                        "modifiedBy": _preferences.getString("username"),
                    },
                    "status": "ACTIVE"
                });
            }

            if(_isAdminEmpty){
                _orderFees.add({
                    "orderFeeID": "NEW",
                    "feeTypeID": "01",
                    "cashFee": 0,
                    "creditFee": 0,
                    "totalFee": 0,
                    "creationalSpecification": {
                        "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                        "createdBy": _preferences.getString("username"),
                        "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                        "modifiedBy": _preferences.getString("username"),
                    },
                    "status": "ACTIVE"
                });
            }

            if(_isProvisiEmpty){
                _orderFees.add({
                    "orderFeeID": "NEW",
                    "feeTypeID": "02",
                    "cashFee": 0,
                    "creditFee": 0,
                    "totalFee": 0,
                    "creationalSpecification": {
                        "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                        "createdBy": _preferences.getString("username"),
                        "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                        "modifiedBy": _preferences.getString("username"),
                    },
                    "status": "ACTIVE"
                });
            }
        }
        else{
            _orderFees.add({
                "orderFeeID": "NEW",
                "feeTypeID": "01",
                "cashFee": 0,
                "creditFee": 0,
                "totalFee": 0,
                "creationalSpecification": {
                    "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                    "createdBy": _preferences.getString("username"),
                    "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                    "modifiedBy": _preferences.getString("username"),
                },
                "status": "ACTIVE"
            });
            _orderFees.add({
                "orderFeeID": "NEW",
                "feeTypeID": "02",
                "cashFee": 0,
                "creditFee": 0,
                "totalFee": 0,
                "creationalSpecification": {
                    "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                    "createdBy": _preferences.getString("username"),
                    "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                    "modifiedBy": _preferences.getString("username"),
                },
                "status": "ACTIVE"
            });
        }

        var _orderProductInsurances = [];
        if(_providerInsurance.listFormMajorInsurance.isNotEmpty) {
            String _collateralIDMajorInsurance = "";
            if(_providerKolateral.collateralTypeModel.id == "001") {
                _collateralIDMajorInsurance = "CAU001";
            } else if(_providerKolateral.collateralTypeModel.id == "002") {
                _collateralIDMajorInsurance = "COLP001";
            } else {
                _collateralIDMajorInsurance = "";
            }
            for(int i=0; i<_providerInsurance.listFormMajorInsurance.length; i++) {
                // debugPrint("CEK_ID ${_providerInsurance.listFormMajorInsurance[i].orderProductInsuranceID}");
                _orderProductInsurances.add({
                    "orderProductInsuranceID": _providerInsurance.listFormMajorInsurance[i].orderProductInsuranceID,//_preferences.getString("last_known_state") != "IDE" ? _providerInsurance.listFormMajorInsurance[i].orderProductInsuranceID : "NEW",
                    "insuranceCollateralReferenceSpecification": {
                        "collateralID": _preferences.getString("last_known_state") != "IDE" ? _providerKolateral.collateralID : _collateralIDMajorInsurance,
                        "collateralTypeID": _providerKolateral.collateralTypeModel.id, // _providerInsurance.listFormMajorInsurance[i].colaType
                    },
                    "insuranceTypeID": _providerInsurance.listFormMajorInsurance[i].insuranceType.KODE,
                    "insuranceCompanyID": _providerInsurance.listFormMajorInsurance[i].company  == null ? "02" : _providerInsurance.listFormMajorInsurance[i].company.KODE, // field perusahaan
                    "insuranceProductID": _providerInsurance.listFormMajorInsurance[i].product != null ? _providerInsurance.listFormMajorInsurance[i].product.KODE : "", // field produk
                    "period": _providerInsurance.listFormMajorInsurance[i].periodType == "" ? null : int.parse(_providerInsurance.listFormMajorInsurance[i].periodType),
                    "type": _providerInsurance.listFormMajorInsurance[i].type,
                    "type1": _providerInsurance.listFormMajorInsurance[i].coverage1 != null ? _providerInsurance.listFormMajorInsurance[i].coverage1.KODE :"", // field coverage 1
                    "type2": _providerInsurance.listFormMajorInsurance[i].coverage2 != null ? _providerInsurance.listFormMajorInsurance[i].coverage2.KODE : "", // field coverage 2
                    "subType": _providerInsurance.listFormMajorInsurance[i].subType != null ? _providerInsurance.listFormMajorInsurance[i].subType.KODE :"", // field coverage 1
                    "subTypeCoverage": _providerInsurance.listFormMajorInsurance[i].coverageType == null ? null : _providerInsurance.listFormMajorInsurance[i].coverageType.KODE, // field sub type
                    "insuranceSourceTypeID": _providerInsurance.listFormMajorInsurance[i].coverageType != null ? _providerInsurance.listFormMajorInsurance[i].coverageType.KODE : "", // field jenis pertanggungan
                    "insuranceValue": _providerInsurance.listFormMajorInsurance[i].coverageValue.isEmpty ? 0 : double.parse(_providerInsurance.listFormMajorInsurance[i].coverageValue.replaceAll(",", "")).round(),
                    "upperLimitPercentage": _providerInsurance.listFormMajorInsurance[i].upperLimitRate == "" ? 0.0 : double.parse(_providerInsurance.listFormMajorInsurance[i].upperLimitRate),
                    "upperLimitAmount": _providerInsurance.listFormMajorInsurance[i].upperLimit == "" ? 0 : double.parse(_providerInsurance.listFormMajorInsurance[i].upperLimit.replaceAll(",", "")),
                    "lowerLimitPercentage": _providerInsurance.listFormMajorInsurance[i].lowerLimitRate == "" ? 0 : double.parse(_providerInsurance.listFormMajorInsurance[i].lowerLimitRate),
                    "lowerLimitAmount": _providerInsurance.listFormMajorInsurance[i].lowerLimit == "" ? 0 : double.parse(_providerInsurance.listFormMajorInsurance[i].lowerLimit.replaceAll(",", "")),
                    "insuranceCashFee": _providerInsurance.listFormMajorInsurance[i].priceCash == "" ? 0 : double.parse(_providerInsurance.listFormMajorInsurance[i].priceCash.replaceAll(",", "")).round(),
                    "insuranceCreditFee": _providerInsurance.listFormMajorInsurance[i].priceCredit == "" ? 0 : double.parse(_providerInsurance.listFormMajorInsurance[i].priceCredit.replaceAll(",", "")).round(),
                    "totalSplitFee": _providerInsurance.listFormMajorInsurance[i].totalPriceSplit == "" ? 0 : double.parse(_providerInsurance.listFormMajorInsurance[i].totalPriceSplit.replaceAll(",", "")),
                    "totalInsuranceFeePercentage": _providerInsurance.listFormMajorInsurance[i].totalPriceRate == "" ? 0 : double.parse(_providerInsurance.listFormMajorInsurance[i].totalPriceRate),
                    "totalInsuranceFeeAmount": _providerInsurance.listFormMajorInsurance[i].totalPrice == "" ? 0 : double.parse(_providerInsurance.listFormMajorInsurance[i].totalPrice.replaceAll(",", "")),
                    "creationalSpecification": {
                        "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                        "createdBy": _preferences.getString("username"),
                        "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                        "modifiedBy": _preferences.getString("username"),
                    },
                    "status": "ACTIVE",
                    "insurancePaymentType": null
                });
            }
        }

        if(_providerAdditionalInsurance.listFormAdditionalInsurance.isNotEmpty) {
            // debugPrint("CHECK INSURANCE ${_providerAdditionalInsurance.listFormAdditionalInsurance.length}");
            for(int i=0; i<_providerAdditionalInsurance.listFormAdditionalInsurance.length; i++) {
                _orderProductInsurances.add({
                    "orderProductInsuranceID": _providerAdditionalInsurance.listFormAdditionalInsurance[i].orderProductInsuranceID,//_preferences.getString("last_known_state") != "IDE" ? _providerAdditionalInsurance.listFormAdditionalInsurance[i].orderProductInsuranceID : "NEW",
                    "insuranceCollateralReferenceSpecification": {
                        "collateralID": _preferences.getString("last_known_state") != "IDE" ? _providerKolateral.collateralID : "", // NEW
                        "collateralTypeID": _providerKolateral.collateralTypeModel.id, // _providerAdditionalInsurance.listFormAdditionalInsurance[i].colaType
                    },
                    "insuranceTypeID": _providerAdditionalInsurance.listFormAdditionalInsurance[i].insuranceType.KODE,
                    "insuranceCompanyID": _providerAdditionalInsurance.listFormAdditionalInsurance[i].company  == null ? "02" : _providerAdditionalInsurance.listFormAdditionalInsurance[i].company.KODE,
                    "insuranceProductID": _providerAdditionalInsurance.listFormAdditionalInsurance[i].product.KODE,
                    "period": _providerAdditionalInsurance.listFormAdditionalInsurance[i].periodType == "" ? null : int.parse(_providerAdditionalInsurance.listFormAdditionalInsurance[i].periodType),
                    "type": "",
                    "type1": "",
                    "type2": "",
                    "subType": "",
                    "subTypeCoverage": "",
                    "insuranceSourceTypeID": _providerAdditionalInsurance.listFormAdditionalInsurance[i].coverageType != null ? _providerAdditionalInsurance.listFormAdditionalInsurance[i].coverageType.KODE : "", // field jenis pertanggungan
                    "insuranceValue": _providerAdditionalInsurance.listFormAdditionalInsurance[i].coverageValue.isEmpty ? 0 : double.parse(_providerAdditionalInsurance.listFormAdditionalInsurance[i].coverageValue.replaceAll(",", "")).round(),
                    "upperLimitPercentage": _providerAdditionalInsurance.listFormAdditionalInsurance[i].upperLimitRate == "" ? 0.0 : double.parse(_providerAdditionalInsurance.listFormAdditionalInsurance[i].upperLimitRate),
                    "upperLimitAmount": _providerAdditionalInsurance.listFormAdditionalInsurance[i].upperLimit == "" ? 0 : double.parse(_providerAdditionalInsurance.listFormAdditionalInsurance[i].upperLimit.replaceAll(",", "")),
                    "lowerLimitPercentage": _providerAdditionalInsurance.listFormAdditionalInsurance[i].lowerLimitRate == "" ? 0 : double.parse(_providerAdditionalInsurance.listFormAdditionalInsurance[i].lowerLimitRate),
                    "lowerLimitAmount": _providerAdditionalInsurance.listFormAdditionalInsurance[i].lowerLimit == "" ? 0 : double.parse(_providerAdditionalInsurance.listFormAdditionalInsurance[i].lowerLimit.replaceAll(",", "")),
                    "insuranceCashFee": _providerAdditionalInsurance.listFormAdditionalInsurance[i].priceCash == "" ? 0 : double.parse(_providerAdditionalInsurance.listFormAdditionalInsurance[i].priceCash.replaceAll(",", "")).round(),
                    "insuranceCreditFee": _providerAdditionalInsurance.listFormAdditionalInsurance[i].priceCredit == "" ? 0 : double.parse(_providerAdditionalInsurance.listFormAdditionalInsurance[i].priceCredit.replaceAll(",", "")).round(),
                    "totalSplitFee": "",
                    "totalInsuranceFeePercentage": _providerAdditionalInsurance.listFormAdditionalInsurance[i].totalPriceRate == "" ? 0 : double.parse(_providerAdditionalInsurance.listFormAdditionalInsurance[i].totalPriceRate),
                    "totalInsuranceFeeAmount": _providerAdditionalInsurance.listFormAdditionalInsurance[i].totalPrice == "" ? 0 : double.parse(_providerAdditionalInsurance.listFormAdditionalInsurance[i].totalPrice.replaceAll(",", "")),
                    "creationalSpecification": {
                        "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                        "createdBy": _preferences.getString("username"),
                        "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                        "modifiedBy": _preferences.getString("username"),
                    },
                    "status": "ACTIVE",
                    "insurancePaymentType": null
                });
            }
        }

        var _orderWmps = [];
        // print("cek wmp ${_providerWmp.listWMP.isNotEmpty}");
        if(_providerWmp.listWMP.isNotEmpty) {
            print("masuk if");
            for(int i=0; i < _providerWmp.listWMP.length; i++) {
                _orderWmps.add(
                    {
                        "orderWmpID": _preferences.getString("last_known_state") != "IDE" ? _providerWmp.listWMP[i].orderWmpID : "NEW",
                        "wmpNumber": _providerWmp.listWMP[i].noProposal,
                        "wmpType": _providerWmp.listWMP[i].type,
                        "wmpJob": "",
                        "wmpSubsidyTypeID": _providerWmp.listWMP[i].typeSubsidi,
                        "maxRefundAmount": _providerWmp.listWMP[i].amount,
                        "status": "ACTIVE",
                        "creationalSpecification": {
                            "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                            "createdBy": _preferences.getString("username"),
                            "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                            "modifiedBy": _preferences.getString("username"),
                        },
                    },
                );
            }
        }

        var _orderSubsidies = [];
        var _orderSubsidyDetails = [];
        if(_providerSubsidy.listInfoCreditSubsidy.isNotEmpty) {
            for(int i=0; i < _providerSubsidy.listInfoCreditSubsidy.length; i++) {
                for(int j=0; j < _providerSubsidy.listInfoCreditSubsidy[i].installmentDetail.length; j++) {
                    _orderSubsidyDetails.add(
                        {
                            "orderSubsidyDetailID": _preferences.getString("last_known_state") != "IDE" ? _providerSubsidy.listInfoCreditSubsidy[i].installmentDetail[j].orderSubsidyDetailID : "NEW",
                            "installmentNumber": _providerSubsidy.listInfoCreditSubsidy[i].installmentDetail[j].installmentIndex != null ? double.parse(_providerSubsidy.listInfoCreditSubsidy[i].installmentDetail[j].installmentIndex.replaceAll(",", "")) : 0,
                            "installmentAmount": _providerSubsidy.listInfoCreditSubsidy[i].installmentDetail[j].installmentSubsidy != null ? double.parse(_providerSubsidy.listInfoCreditSubsidy[i].installmentDetail[j].installmentSubsidy.replaceAll(",", "")) : 0,
                            "creationalSpecification": {
                                "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                                "createdBy": _preferences.getString("username"),
                                "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                                "modifiedBy": _preferences.getString("username"),
                            },
                            "status": "ACTIVE"
                        }
                    );
                }
                _orderSubsidies.add(
                    {
                        "orderSubsidyID": _preferences.getString("last_known_state") != "IDE" ? _providerSubsidy.listInfoCreditSubsidy[i].orderSubsidyID : "NEW",
                        "subsidyProviderID": _providerSubsidy.listInfoCreditSubsidy[i].giver,
                        "subsidyTypeID": _providerSubsidy.listInfoCreditSubsidy[i].type != null ? _providerSubsidy.listInfoCreditSubsidy[i].type.id : "",
                        "subsidyMethodTypeID": _providerSubsidy.listInfoCreditSubsidy[i].cuttingMethod != null ? _providerSubsidy.listInfoCreditSubsidy[i].cuttingMethod.id : "02",
                        "refundAmount": _providerSubsidy.listInfoCreditSubsidy[i].value != ""
                            ? double.parse(_providerSubsidy.listInfoCreditSubsidy[i].value.replaceAll(",", "")) : 0,
                        "refundAmountKlaim": _providerSubsidy.listInfoCreditSubsidy[i].claimValue != "" ?
                        double.parse(_providerSubsidy.listInfoCreditSubsidy[i].claimValue.replaceAll(",", "")) : 0, //Nilai Klaim
                        "effectiveRate": _providerSubsidy.listInfoCreditSubsidy[i].rateBeforeEff != ""
                            ? double.parse(_providerSubsidy.listInfoCreditSubsidy[i].rateBeforeEff.replaceAll(",", "")) : 0,
                        "flatRate":_providerSubsidy.listInfoCreditSubsidy[i].rateBeforeFlat != "" ?
                        double.parse(_providerSubsidy.listInfoCreditSubsidy[i].rateBeforeFlat.replaceAll(",", "")) : 0,
                        "dpReal": 0,
                        "totalInstallment": _providerSubsidy.listInfoCreditSubsidy[i].totalInstallment != "" && _providerSubsidy.listInfoCreditSubsidy[i].totalInstallment != null
                            ? _providerSubsidy.listInfoCreditSubsidy[i].totalInstallment.replaceAll(",", "") : "",
                        "interestRate": 0,
                        "creationalSpecification": {
                            "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                            "createdBy": _preferences.getString("username"),
                            "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                            "modifiedBy": _preferences.getString("username"),
                        },
                        "status": "ACTIVE",
                        "orderSubsidyDetails": _orderSubsidyDetails
                    }
                );
            }
        }

        var _surveyResultDetails = [];
        if(_providerSurvey.listResultSurveyDetailSurveyModel.isNotEmpty) {
            for(int i=0; i < _providerSurvey.listResultSurveyDetailSurveyModel.length; i++) {
                _surveyResultDetails.add({
                    "surveyResultDetailID": _providerSurvey.listResultSurveyDetailSurveyModel[i].surveyResultDetailID != null ? _providerSurvey.listResultSurveyDetailSurveyModel[i].surveyResultDetailID : "NEW",
                    "infoEnvironment": _providerSurvey.listResultSurveyDetailSurveyModel[i].environmentalInformationModel.id,
                    "sourceInfo": _providerSurvey.listResultSurveyDetailSurveyModel[i].recourcesInfoSurveyModel.PARA_INFORMATION_SOURCE_ID,
                    "sourceNameInfo": _providerSurvey.listResultSurveyDetailSurveyModel[i].resourceInformationName,
                    "status": "ACTIVE",
                    "creationalSpecification": {
                        "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                        "createdBy": _preferences.getString("username"),
                        "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                        "modifiedBy": _preferences.getString("username"),
                    }
                });
            }
        }

        var _surveyResultAssets = [];
        if(_providerSurvey.listResultSurveyAssetModel.isNotEmpty){
            for(int i=0; i< _providerSurvey.listResultSurveyAssetModel.length; i++){
                _surveyResultAssets.add({
                    "surveyResultAssetID": _providerSurvey.listResultSurveyAssetModel[i].surveyResultAssetID != null ? _providerSurvey.listResultSurveyAssetModel[i].surveyResultAssetID : "NEW",
                    "assetType": _providerSurvey.listResultSurveyAssetModel[i].assetTypeModel != null ? _providerSurvey.listResultSurveyAssetModel[i].assetTypeModel.id : null,
                    "assetAmount": _providerSurvey.listResultSurveyAssetModel[i].valueAsset != "" ? double.parse(_providerSurvey.listResultSurveyAssetModel[i].valueAsset.replaceAll(",", "")).toInt() : 0,
                    "ownStatus": _providerSurvey.listResultSurveyAssetModel[i].ownershipModel != null ? _providerSurvey.listResultSurveyAssetModel[i].ownershipModel.id : null,
                    "sizeofLand": _providerSurvey.listResultSurveyAssetModel[i].surfaceBuildingArea != "" ? _providerSurvey.listResultSurveyAssetModel[i].surfaceBuildingArea : null,
                    "streetType": _providerSurvey.listResultSurveyAssetModel[i].roadTypeModel != null ? _providerSurvey.listResultSurveyAssetModel[i].roadTypeModel.kode : null,
                    "electricityType": _providerSurvey.listResultSurveyAssetModel[i].electricityTypeModel != null ? _providerSurvey.listResultSurveyAssetModel[i].electricityTypeModel.PARA_ELECTRICITY_ID : null,
                    "electricityBill": _providerSurvey.listResultSurveyAssetModel[i].electricityBills != "" ? double.parse(_providerSurvey.listResultSurveyAssetModel[i].electricityBills.replaceAll(",", "")).toInt() : 0,
                    "expiredContractDate": _providerSurvey.listResultSurveyAssetModel[i].endDateLease != null ? formatDateValidateAndSubmit(_providerSurvey.listResultSurveyAssetModel[i].endDateLease) : null,
                    "longOfStay": _providerSurvey.listResultSurveyAssetModel[i].lengthStay != "" ? _providerSurvey.listResultSurveyAssetModel[i].lengthStay : null,
                    "status": "ACTIVE",
                    "creationalSpecification": {
                        "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                        "createdBy": _preferences.getString("username"),
                        "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                        "modifiedBy": _preferences.getString("username")
                    }
                });
            }
        }

        var _orderProductSalesesDakor = [];
        if(_providerSales.listInfoSales.isNotEmpty) {
            for(int i=0; i<_providerSales.listInfoSales.length; i++){
                _orderProductSalesesDakor.add({
                    "productSalesIDReason": null,
                    "productSalesTypeReason": _providerSales.listInfoSales[i].isSalesmanTypeModelChanges ? "99" : null,
                    "productSalesEmployeeJobReason": _providerSales.listInfoSales[i].isPositionModelChanges ? "99" : null,
                    "productSalesEmployeeIDReason": _providerSales.listInfoSales[i].isEmployeeChanges ? "99" : null,
                    "productSalesEmployeeHeadIDReason": _providerSales.listInfoSales[i].isEmployeeHeadModelChanges ? "99" : null,
                    "productSalesReferalContractNumberReason": null,
                    "productSalesCreatedAtReason": null,
                    "productSalesCreatedByReason": null,
                    "productSalesModifiedAtReason": null,
                    "productSalesModifiedByReason": null,
                    "productSalesStatusReason": null,
                    "productSalesFlagCorrectionReason": null,
                });
            }
        }

        List _orderProductFeesReason = [];
        for(int i=0; i < _providerCreditStructure.listInfoFeeCreditStructure.length; i++){
            _orderProductFeesReason.add({
                    "orderProductFeeIDReason": null,
                    "orderProductFeeTypeIDReason": _providerCreditStructure.listInfoFeeCreditStructure[i].editFeeType  ? "99" : null,
                    "orderProductFeeCashFeeReason": _providerCreditStructure.listInfoFeeCreditStructure[i].editFeeCash ? "99" : null,
                    "orderProductFeeCreditFeeReason": _providerCreditStructure.listInfoFeeCreditStructure[i].editFeeCredit ? "99" : null,
                    "orderProductTotalFeeReason": _providerCreditStructure.listInfoFeeCreditStructure[i].editTotalFee ? "99" : null,
                    "orderProductFeeCreatedAtReason": null,
                    "orderProductFeeCreatedByReason": null,
                    "orderProductFeeModifiedAtReason": null,
                    "orderProductFeeModifiedByReason": null,
                    "orderProductFeeStatusReason": null,
                    "orderProductFeeFlagCorrectionReason": null
                });
        }

        var _orderProductKaroserisReason = [];
        if(_providerKaroseri.listFormKaroseriObject.isNotEmpty) {
            for(int i=0; i < _providerKaroseri.listFormKaroseriObject.length; i++) {
                _orderProductKaroserisReason.add({
                    "orderOrderKaroseriIDReason": null,
                    "orderKaroseriIsPksKaroseriReason": _providerKaroseri.listFormKaroseriObject[i].isRadioValuePKSKaroseriChanges ? "99" : null,
                    "orderKaroseriCompanyIDReason": _providerKaroseri.listFormKaroseriObject[i].isCompanyChanges ? "99" : null,
                    "orderKaroseriIDReason": _providerKaroseri.listFormKaroseriObject[i].isKaroseriChanges ? "99" : null,
                    "orderKaroseriPriceReason": _providerKaroseri.listFormKaroseriObject[i].isPriceChanges ? "99" : null,
                    "orderKaroseriQtyReason": _providerKaroseri.listFormKaroseriObject[i].isJumlahKaroseriChanges ? "99" : null,
                    "orderKaroseriTotalPriceReason": _providerKaroseri.listFormKaroseriObject[i].isTotalPriceChanges ? "99" : null,
                    "orderKaroseriUangMukaKaroseriReason": null,
                    "orderKaroseriCreatedAtReason": null,
                    "orderKaroseriCreatedByReason": null,
                    "orderKaroseriModifiedAtReason": null,
                    "orderKaroseriModifiedByReason": null,
                    "orderKaroseriStatusReason": null,
                });
            }
        }

        // Asuransi Utama
        var _orderProductInsurancesReason = [];
        if(_providerInsurance.listFormMajorInsurance.isNotEmpty) {
            for(int i=0; i < _providerInsurance.listFormMajorInsurance.length; i++) {
                _orderProductInsurancesReason.add({
                    "orderProductInsuranceIDReason": null,
                    "orderProductInsuranceCollateralIDReason": null,
                    "orderProductInsuranceCollateralTypeReason": null,
                    "orderProductInsuranceTypeIDReason": null,
                    "orderProductInsuranceCompanyIDReason": _providerInsurance.listFormMajorInsurance[i].companyDakor ? "99" : null,
                    "orderProductInsuranceProductIDReason": _providerInsurance.listFormMajorInsurance[i].productDakor ? "99" : null,
                    "orderProductInsurancePeriodReason": _providerInsurance.listFormMajorInsurance[i].periodDakor ? "99" : null,
                    "orderProductInsuranceTypeReason": _providerInsurance.listFormMajorInsurance[i].typeDakor ? "99" : null,
                    "orderProductInsuranceType1Reason": _providerInsurance.listFormMajorInsurance[i].coverage1Dakor ? "99" : null,
                    "orderProductInsuranceType2Reason": _providerInsurance.listFormMajorInsurance[i].coverage2Dakor ? "99" : null,
                    "orderProductInsuranceSubTypeReason": _providerInsurance.listFormMajorInsurance[i].coverageTypeDakor ? "99" : null,
                    "orderProductInsuranceSubTypeCoverageReason": _providerInsurance.listFormMajorInsurance[i].coverageTypeDakor ? "99" : null,
                    "orderProductInsuranceSourceTypeIDReason": null,
                    "orderProductInsuranceValueReason": _providerInsurance.listFormMajorInsurance[i].coverageValueDakor ? "99": null,
                    "orderProductInsuranceUpperLimitPercentageReason": _providerInsurance.listFormMajorInsurance[i].upperLimitRateDakor ? "99" : null,
                    "orderProductInsuranceUpperLimitAmountReason": _providerInsurance.listFormMajorInsurance[i].upperLimitValueDakor ? "99" : null,
                    "orderProductInsuranceLowerLimitPercentageReason": _providerInsurance.listFormMajorInsurance[i].lowerLimitRateDakor ? "99" : null,
                    "orderProductInsuranceLowerLimitAmountReason": _providerInsurance.listFormMajorInsurance[i].lowerLimitValueDakor ? "99" : null,
                    "orderProductInsuranceCashFeeReason": _providerInsurance.listFormMajorInsurance[i].cashDakor ? "99" : null,
                    "orderProductInsuranceCreditFeeReason": _providerInsurance.listFormMajorInsurance[i].creditDakor ? "99" : null,
                    "orderProductInsuranceTotalSplitFeeReason": _providerInsurance.listFormMajorInsurance[i].totalPriceSplitDakor ? "99" : null,
                    "orderProductTotalInsuranceFeePercentageReason": _providerInsurance.listFormMajorInsurance[i].totalPriceRateDakor ? "99" : null,
                    "orderProductTotalInsuranceFeeAmountReason": _providerInsurance.listFormMajorInsurance[i].totalPriceDakor ? "99" : null,
                    "orderProductInsuranceCreatedAtReason": null,
                    "orderProductInsuranceCreatedByReason": null,
                    "orderProductInsuranceModifiedAtReason": null,
                    "orderProductInsuranceModifiedByReason": null,
                    "orderProductInsuranceStatusReason": null,
                    "orderProductInsurancePaymentTypeReason": null,
                    "orderProductInsuranceFlagCorrectionReason": null
                });
            }
        }

        // Asuransi Tambahan
        if(_providerAdditionalInsurance.listFormAdditionalInsurance.isNotEmpty) {
            for(int i=0; i < _providerAdditionalInsurance.listFormAdditionalInsurance.length; i++) {
                _orderProductInsurancesReason.add({
                    "orderProductInsuranceIDReason": null,
                    "orderProductInsuranceCollateralIDReason": null,
                    "orderProductInsuranceCollateralTypeReason": null,
                    "orderProductInsuranceTypeIDReason": null,
                    "orderProductInsuranceCompanyIDReason": _providerAdditionalInsurance.listFormAdditionalInsurance[i].companyDakor ? "99" : null,
                    "orderProductInsuranceProductIDReason": _providerAdditionalInsurance.listFormAdditionalInsurance[i].productDakor ? "99" : null,
                    "orderProductInsurancePeriodReason": _providerAdditionalInsurance.listFormAdditionalInsurance[i].periodDakor ? "99" : null,
                    "orderProductInsuranceTypeReason": null,
                    "orderProductInsuranceType1Reason": null,
                    "orderProductInsuranceType2Reason": null,
                    "orderProductInsuranceSubTypeReason": null,
                    "orderProductInsuranceSubTypeCoverageReason": _providerAdditionalInsurance.listFormAdditionalInsurance[i].coverageTypeDakor ? "99" : null,
                    "orderProductInsuranceSourceTypeIDReason": null,
                    "orderProductInsuranceValueReason": _providerAdditionalInsurance.listFormAdditionalInsurance[i].coverageValueDakor ? "99": null,
                    "orderProductInsuranceUpperLimitPercentageReason": _providerAdditionalInsurance.listFormAdditionalInsurance[i].upperLimitRateDakor ? "99" : null,
                    "orderProductInsuranceUpperLimitAmountReason": _providerAdditionalInsurance.listFormAdditionalInsurance[i].upperLimitValueDakor ? "99" : null,
                    "orderProductInsuranceLowerLimitPercentageReason": _providerAdditionalInsurance.listFormAdditionalInsurance[i].lowerLimitRateDakor ? "99" : null,
                    "orderProductInsuranceLowerLimitAmountReason": _providerAdditionalInsurance.listFormAdditionalInsurance[i].lowerLimitValueDakor ? "99" : null,
                    "orderProductInsuranceCashFeeReason": _providerAdditionalInsurance.listFormAdditionalInsurance[i].cashDakor ? "99" : null,
                    "orderProductInsuranceCreditFeeReason": _providerAdditionalInsurance.listFormAdditionalInsurance[i].creditDakor ? "99" : null,
                    "orderProductInsuranceTotalSplitFeeReason": null,
                    "orderProductTotalInsuranceFeePercentageReason": _providerAdditionalInsurance.listFormAdditionalInsurance[i].totalPriceRateDakor ? "99" : null,
                    "orderProductTotalInsuranceFeeAmountReason": _providerAdditionalInsurance.listFormAdditionalInsurance[i].totalPriceDakor ? "99" : null,
                    "orderProductInsuranceCreatedAtReason": null,
                    "orderProductInsuranceCreatedByReason": null,
                    "orderProductInsuranceModifiedAtReason": null,
                    "orderProductInsuranceModifiedByReason": null,
                    "orderProductInsuranceStatusReason": null,
                    "orderProductInsurancePaymentTypeReason": null,
                    "orderProductInsuranceFlagCorrectionReason": null
                });
            }
        }

        var _orderProductWmpsReason = [];
        if(_providerWmp.listWMP.isNotEmpty) {
            for(int i=0; i < _providerWmp.listWMP.length; i++) {
                _orderProductWmpsReason.add({
                    "orderWmpIDReason": null,
                    "orderWmpNumberReason": _providerWmp.listWMP[i].isEditNoProposal ? "99" : null,
                    "orderWmpTypeReason": _providerWmp.listWMP[i].isEditType ? "99" : null,
                    "orderWmpJobReason": null,
                    "orderWmpSubsidyTypeIDReason": _providerWmp.listWMP[i].isEditTypeSubsidi ? "99" : null,
                    "orderWmpMaxRefundAmountReason": _providerWmp.listWMP[i].isEditAmount ? "99" : null,
                    "orderWmpCreatedAtReason": null,
                    "orderWmpCreatedByReason": null,
                    "orderWmpModifiedAtReason": null,
                    "orderWmpModifiedByReason": null,
                    "orderWmpStatusReason": null,
                    "orderWmpFlagCorrectionReason": null,
                });
            }
        }

        // Subsidi
        var _orderProductSubsidiesReason = [];
        var _orderSubsidyDetailsReason = [];
        if(_providerSubsidy.listInfoCreditSubsidy.isNotEmpty) {
            for(int i=0; i < _providerSubsidy.listInfoCreditSubsidy.length; i++) {
                for(int j=0; j < _providerSubsidy.listInfoCreditSubsidy[i].installmentDetail.length; j++) {
                    _orderSubsidyDetailsReason.add({
                        "orderSubsidyDetailIDReason": null,
                        "orderSubsidyDetailInstallmentNumberReason": null, // _providerSubsidy.listInfoCreditSubsidy[i].installmentDetail[j].installmentIndex ? "99" : null
                        "orderSubsidyDetailInstallmentAmountReason": null, // _providerSubsidy.listInfoCreditSubsidy[i].installmentDetail[j].installmentSubsidy ? "99" : null
                        "orderSubsidyDetailCreatedAtReason": null,
                        "orderSubsidyDetailCreatedByReason": null,
                        "orderSubsidyDetailModifiedAtReason": null,
                        "orderSubsidyDetailModifiedByReason": null,
                        "orderSubsidyDetailStatusReason": null,
                        "orderSubsidyDetailFlagCorrectionReason": null,
                    });
                }
                _orderProductSubsidiesReason.add({
                    "orderSubsidyIDReason": null,
                    "orderSubsidyProviderIDReason": null,
                    "orderSubsidyTypeIDReason": _providerSubsidy.listInfoCreditSubsidy[i].typeDakor ? "99" : null,
                    "orderSubsidyMethodTypeIDReason": _providerSubsidy.listInfoCreditSubsidy[i].cuttingMethodDakor ? "99" : null,
                    "orderSubsidyRefundAmountReason": _providerSubsidy.listInfoCreditSubsidy[i].cuttingValueDakor ? "99" : null,
                    "orderSubsidyRefundAmountKlaimReason": _providerSubsidy.listInfoCreditSubsidy[i].claimValueDakor ? "99" : null,
                    "orderSubsidyEffectiveRateReason": _providerSubsidy.listInfoCreditSubsidy[i].rateBeforeEffDakor ? "99" : null,
                    "orderSubsidyFlatRateReason": _providerSubsidy.listInfoCreditSubsidy[i].rateBeforeFlatDakor ? "99" : null,
                    "orderSubsidyDpRealReason": null,
                    "orderSubsidyTotalInstallmentReason": _providerSubsidy.listInfoCreditSubsidy[i].totalInstallmentDakor ? "99" : null,
                    "orderSubsidyInterestRateReason": null,
                    "orderSubsidyCreatedAtReason": null,
                    "orderSubsidyCreatedByReason": null,
                    "orderSubsidyModifiedAtReason": null,
                    "orderSubsidyModifiedByReason": null,
                    "orderSubsidyStatusReason": null,
                    "orderSubsidyFlagCorrectionReason": null,
                    "orderSubsidyDetailsReason": _orderSubsidyDetailsReason
                });
            }
        }

        //survey
        var _surveyResultAssetsDakor = [];
        if(_providerSurvey.listResultSurveyAssetModel.isNotEmpty){
            for(int i=0; i< _providerSurvey.listResultSurveyAssetModel.length; i++){
                _surveyResultAssetsDakor.add({
                    "surveyResultAssetIDReason": null,
                    "surveyResultAssetTypeReason": _providerSurvey.listResultSurveyAssetModel[i].isAssetTypeChanges ? "99" : null,
                    "surveyResultAssetAmountReason": _providerSurvey.listResultSurveyAssetModel[i].isValueAssetChanges ? "99" : null,
                    "surveyResultAssetOwnStatusReason": _providerSurvey.listResultSurveyAssetModel[i].isOwneshipInfoChanges ? "99" : null,
                    "surveyResultAssetSizeofLandReason": _providerSurvey.listResultSurveyAssetModel[i].isSurfaceBuildingAreaInfoChanges ? "99" : null,
                    "surveyResultAssetStreetTypeReason": _providerSurvey.listResultSurveyAssetModel[i].isRoadTypeInfoChanges ? "99" : null,
                    "surveyResultAssetElectricityTypeReason": _providerSurvey.listResultSurveyAssetModel[i].isElectricityTypeInfoChanges ? "99" : null,
                    "surveyResultAssetElectricityBillReason": _providerSurvey.listResultSurveyAssetModel[i].isElectricityBillInfoChanges ? "99" : null,
                    "surveyResultAssetExpiredContractDateReason": _providerSurvey.listResultSurveyAssetModel[i].isEndDateInfoChanges ? "99" : null,
                    "surveyResultAssetLongOfStayReason": _providerSurvey.listResultSurveyAssetModel[i].isLengthStayInfoChanges ? "99" : null,
                    "surveyResultAssetStatusReason": null,
                    "surveyResultAssetCreatedAtReason": null,
                    "surveyResultAssetCreatedByReason": null,
                    "surveyResultAssetModifiedAtReason": null,
                    "surveyResultAssetModifiedByReason": null,
                    "surveyResultAssetFlagCorrectionReason": null,
                });
            }
        }

        var _surveyResultDetailsDakor = [];
        if(_providerSurvey.listResultSurveyDetailSurveyModel.isNotEmpty){
            for(int i=0; i < _providerSurvey.listResultSurveyDetailSurveyModel.length; i++) {
                _surveyResultDetailsDakor.add({
                    "surveyResultDetailIDReason": null,
                    "surveyResultDetailInfoEnvironmentReason": _providerSurvey.listResultSurveyDetailSurveyModel[i].isEnvInformationChange ? "99" : null,
                    "surveyResultDetailSourceInfoReason": _providerSurvey.listResultSurveyDetailSurveyModel[i].isSourceInformationChange ? "99" : null,
                    "surveyResultDetailSourceNameInfoReason": _providerSurvey.listResultSurveyDetailSurveyModel[i].isSourceInfoNameChange ? "99" : null,
                    "surveyResultDetailStatusReason": null,
                    "surveyResultDetailCreatedAtReason": null,
                    "surveyResultDetailCreatedByReason": null,
                    "surveyResultDetailModifiedAtReason": null,
                    "surveyResultDetailModifiedByReason": null,
                    "surveyResultDetailFlagCorrectionReason": null,
                });
            }
        }

        // var _customerIndividualOccupationBusinessTypeReason;
        // if(_providerFoto.occupationSelected.KODE == "05" || _providerFoto.occupationSelected.KODE == "07"){
        //     _customerIndividualOccupationBusinessTypeReason = _providerOccupation.editJenisBadanUsahaWiraswasta ? "99" : null;
        // }
        // else if(_providerFoto.occupationSelected.KODE != "08"){
        //     _customerIndividualOccupationBusinessTypeReason = _providerOccupation.editJenisPerusahaanLainnya ? "99" : null;
        // }
        //
        // var _customerIndividualOccupationBusinessEconomySectorReason;
        // if(_providerFoto.occupationSelected.KODE == "05" || _providerFoto.occupationSelected.KODE == "07"){
        //     _customerIndividualOccupationBusinessEconomySectorReason = _providerOccupation.editSektorEkonomiWiraswasta ? "99" : null;
        // }
        // else if(_providerFoto.occupationSelected.KODE == "08"){
        //     _customerIndividualOccupationBusinessEconomySectorReason = _providerOccupation.editSektorEkonomiProfesional ? "99" : null;
        // }
        // else{
        //     _customerIndividualOccupationBusinessEconomySectorReason = _providerOccupation.editSektorEkonomiLainnya ? "99" : null;
        // }
        //
        // var _customerIndividualOccupationBusinessBusinessFieldReason;
        // if(_providerFoto.occupationSelected.KODE == "05" || _providerFoto.occupationSelected.KODE == "07"){
        //     _customerIndividualOccupationBusinessBusinessFieldReason = _providerOccupation.editLapanganUsahaWiraswasta ? "99" : null;
        // }
        // else if(_providerFoto.occupationSelected.KODE == "08"){
        //     _customerIndividualOccupationBusinessBusinessFieldReason = _providerOccupation.editLapanganUsahaProfesional ? "99" : null;
        // }
        // else{
        //     _customerIndividualOccupationBusinessBusinessFieldReason = _providerOccupation.editLapanganUsahaLainnya ? "99" : null;
        // }
        //
        // var _customerIndividualOccupationBusinessLocationStatusReason;
        // if(_providerFoto.occupationSelected.KODE == "05" || _providerFoto.occupationSelected.KODE == "07"){
        //     _customerIndividualOccupationBusinessLocationStatusReason = _providerOccupation.editStatusLokasiWiraswasta ? "99" : null;
        // }
        // else if(_providerFoto.occupationSelected.KODE == "08"){
        //     _customerIndividualOccupationBusinessLocationStatusReason = _providerOccupation.editStatusLokasiProfesional ? "99" : null;
        // }
        //
        // var _customerIndividualOccupationBusinessBusinessLocationReason;
        // if(_providerFoto.occupationSelected.KODE == "05" || _providerFoto.occupationSelected.KODE == "07"){
        //     _customerIndividualOccupationBusinessBusinessLocationReason = _providerOccupation.editLokasiUsahaWiraswasta ? "99" : null;
        // }
        // else if(_providerFoto.occupationSelected.KODE == "08"){
        //     _customerIndividualOccupationBusinessBusinessLocationReason = _providerOccupation.editLokasiUsahaProfesional ? "99" : null;
        // }
        //
        // var _customerIndividualOccupationBusinessEmployeeTotalReason;
        // if(_providerFoto.occupationSelected.KODE == "05" || _providerFoto.occupationSelected.KODE == "07"){
        //     _customerIndividualOccupationBusinessEmployeeTotalReason = _providerOccupation.editTotalPegawaiWiraswasta ? "99" : null;
        // }
        // else if(_providerFoto.occupationSelected.KODE != "08"){
        //     _customerIndividualOccupationBusinessEmployeeTotalReason = _providerOccupation.editTotalPegawaiLainnya ? "99" : null;
        // }
        //
        // var _customerIndividualOccupationBusinessTotalBussinessLengthInMonthReason;
        // if(_providerFoto.occupationSelected.KODE == "05" || _providerFoto.occupationSelected.KODE == "07"){
        //     _customerIndividualOccupationBusinessTotalBussinessLengthInMonthReason = _providerOccupation.editTotalLamaBekerjaWiraswasta ? "99" : null;
        // }
        // else if(_providerFoto.occupationSelected.KODE == "08"){
        //     _customerIndividualOccupationBusinessTotalBussinessLengthInMonthReason = _providerOccupation.editTotalLamaBekerjaProfesional ? "99" : null;
        // }
        // else{
        //     _customerIndividualOccupationBusinessTotalBussinessLengthInMonthReason = _providerOccupation.editTotalLamaBekerjaLainnya ? "99" : null;
        // }
        //
        // var _custIndividualCustomerFamiliesReason = [];
        var _customerIncomesReason = [];

        for(int i=0; i < _providerIncomeCompany.listIncome.length; i++){
            _customerIncomesReason.add({
                "customerIncomeIDReason": null,
                "customerIncomeFrmlTypeReason": null,
                "customerIncomeTypeReason": null,
                "customerIncomeValueReason": _providerIncomeCompany.listIncome[i].edit_income_value == "1" ? "99" : null,
                "customerIncomeStatusReason": null,
                "customerIncomeCreatedAtReason": null,
                "customerIncomeCreatedByReason": null,
                "customerIncomeModifiedAtReason": null,
                "customerIncomeModifiedByReason": null,
                "customerIncomeFlagCorrectionReason": null
            });
        }

        // _custIndividualCustomerFamiliesReason.add(
        //     {
        //         "custIndividualFamilyInfoIDReason": null,
        //         "custIndividualFamilyRelationshipStatusReason": _providerIbu.isRelationshipStatusChanges ? "99" : null,
        //         "custIndividualFamilyIdentityTypeReason": null,
        //         "custIndividualFamilyIdentityNumberReason": null,
        //         "custIndividualFamilyIdentityNameReason": _providerIbu.isNamaIdentitasChanges ? "99" : null,
        //         "custIndividualFamilyFullNameReason": _providerIbu.isNamaLengkapdentitasChanges ? "99" : null,
        //         "custIndividualFamilyAliasReason": null,
        //         "custIndividualFamilyTitleReason": null,
        //         "custIndividualFamilyDateOfBirthReason": null,
        //         "custIndividualFamilyPlaceOfBirthReason": null,
        //         "custIndividualFamilyPlaceOfBirthKabKotaReason": null,
        //         "custIndividualFamilyGenderReason": _providerIbu.isRadioValueGenderChanges ? "99" : null,
        //         "custIndividualFamilyIdentityActiveStartReason": null,
        //         "custIndividualFamilyIdentityActiveEndReason": null,
        //         "custIndividualFamilyReligionReason": null,
        //         "custIndividualFamilyOccupationIDReason": null,
        //         "custIndividualFamilyPositionIDReason": null,
        //         "custIndividualFamilyCreatedAtReason": null,
        //         "custIndividualFamilyCreatedByReason": null,
        //         "custIndividualFamilyModifiedAtReason": null,
        //         "custIndividualFamilyModifiedByReason": null,
        //         "custIndividualFamilyContactTelephoneArea1Reason": _providerIbu.isTlpnChanges ? "99" : null,
        //         "custIndividualFamilyContactTelephone1Reason": _providerIbu.isTlpnChanges ? "99" : null,
        //         "custIndividualFamilyContactTelephoneArea2Reason": null,
        //         "custIndividualFamilyContactTelephone2Reason": null,
        //         "custIndividualFamilyContactFaxAreaReason": null,
        //         "custIndividualFamilyContactFaxReason": null,
        //         "custIndividualFamilyContactHandphoneReason": _providerIbu.isNoHpChanges ? "99" : null,
        //         "custIndividualFamilyContactEmailReason": null,
        //         "custIndividualFamilyDedupScoreReason": null,
        //         "custIndividualFamilyStatusReason": null,
        //         "familyFlagCorrectionReason": null,
        //     }
        // );

        // for(int i=0; i < _providerKeluarga.listFormInfoKel.length; i++){
        //     _custIndividualCustomerFamiliesReason.add(
        //         {
        //             "custIndividualFamilyInfoIDReason": null,
        //             "custIndividualFamilyRelationshipStatusReason": _providerKeluarga.listFormInfoKel[i].isrelationshipStatusModelChanges ? "99" : null,
        //             "custIndividualFamilyIdentityTypeReason": _providerKeluarga.listFormInfoKel[i].isidentityModelChanges ? "99" : null,
        //             "custIndividualFamilyIdentityNumberReason": _providerKeluarga.listFormInfoKel[i].isnoIdentitasChanges ? "99" : null,
        //             "custIndividualFamilyIdentityNameReason": _providerKeluarga.listFormInfoKel[i].isnamaLengkapSesuaiIdentitasChanges ? "99" : null,
        //             "custIndividualFamilyFullNameReason": _providerKeluarga.listFormInfoKel[i].isnamaLengkapChanges ? "99" : null,
        //             "custIndividualFamilyAliasReason": null,
        //             "custIndividualFamilyTitleReason": null,
        //             "custIndividualFamilyDateOfBirthReason": _providerKeluarga.listFormInfoKel[i].isbirthDateChanges ? "99" : null,
        //             "custIndividualFamilyPlaceOfBirthReason": _providerKeluarga.listFormInfoKel[i].istmptLahirSesuaiIdentitasChanges ? "99" : null,
        //             "custIndividualFamilyPlaceOfBirthKabKotaReason": _providerKeluarga.listFormInfoKel[i].isbirthPlaceModelChanges ? "99" : null,
        //             "custIndividualFamilyGenderReason": _providerKeluarga.listFormInfoKel[i].isgenderChanges ? "99" : null,
        //             "custIndividualFamilyIdentityActiveStartReason": null,
        //             "custIndividualFamilyIdentityActiveEndReason": null,
        //             "custIndividualFamilyReligionReason": null,
        //             "custIndividualFamilyOccupationIDReason": null,
        //             "custIndividualFamilyPositionIDReason": null,
        //             "custIndividualFamilyCreatedAtReason": null,
        //             "custIndividualFamilyCreatedByReason": null,
        //             "custIndividualFamilyModifiedAtReason": null,
        //             "custIndividualFamilyModifiedByReason": null,
        //             "custIndividualFamilyContactTelephoneArea1Reason": _providerKeluarga.listFormInfoKel[i].isnoTlpnChanges ? "99" : null,
        //             "custIndividualFamilyContactTelephone1Reason": _providerKeluarga.listFormInfoKel[i].isnoTlpnChanges ? "99" : null,
        //             "custIndividualFamilyContactTelephoneArea2Reason": null,
        //             "custIndividualFamilyContactTelephone2Reason": null,
        //             "custIndividualFamilyContactFaxAreaReason": null,
        //             "custIndividualFamilyContactFaxReason": null,
        //             "custIndividualFamilyContactHandphoneReason": _providerKeluarga.listFormInfoKel[i].isnoHpChanges ? "99" : null,
        //             "custIndividualFamilyContactEmailReason": null,
        //             "custIndividualFamilyDedupScoreReason": null,
        //             "custIndividualFamilyStatusReason": null,
        //             "familyFlagCorrectionReason": null
        //         }
        //     );
        // }

        ////income individu
        // for(int i=0; i < _providerIncome.listIncome.length; i++){
        //     _customerIncomes.add({
        //         "customerIncomeIDReason": null,
        //         "customerIncomeFrmlTypeReason": null,
        //         "customerIncomeTypeReason": null,
        //         "customerIncomeValueReason": null,
        //         "customerIncomeStatusReason": null,
        //         "customerIncomeCreatedAtReason": null,
        //         "customerIncomeCreatedByReason": null,
        //         "customerIncomeModifiedAtReason": null,
        //         "customerIncomeModifiedByReason": null,
        //         "customerIncomeFlagCorrectionReason": null
        //     });
        // }

        List _detailAddressCompany = [];
        for(int i=0; i < _providerInfoAlamatCompany.listAlamatKorespondensi.length; i++){
            _detailAddressCompany.add({
                "addressID": _preferences.getString("last_known_state") != "IDE" ? _providerInfoAlamatCompany.listAlamatKorespondensi[i].addressID != "NEW" ? _providerInfoAlamatCompany.listAlamatKorespondensi[i].addressID : "NEW" : "NEW",
                "addressSpecification": {
                    "koresponden": _providerInfoAlamatCompany.listAlamatKorespondensi[i].isCorrespondence ? "1" : "0",
                    "matrixAddr": _preferences.getString("cust_type") == "PER" ? "2" : "8",
                    "address": _providerInfoAlamatCompany.listAlamatKorespondensi[i].address,
                    "rt": _providerInfoAlamatCompany.listAlamatKorespondensi[i].rt,
                    "rw": _providerInfoAlamatCompany.listAlamatKorespondensi[i].rw,
                    "provinsiID": _providerInfoAlamatCompany.listAlamatKorespondensi[i].kelurahanModel.PROV_ID,
                    "kabkotID": _providerInfoAlamatCompany.listAlamatKorespondensi[i].kelurahanModel.KABKOT_ID,
                    "kecamatanID": _providerInfoAlamatCompany.listAlamatKorespondensi[i].kelurahanModel.KEC_ID,
                    "kelurahanID": _providerInfoAlamatCompany.listAlamatKorespondensi[i].kelurahanModel.KEL_ID,
                    "zipcode": _providerInfoAlamatCompany.listAlamatKorespondensi[i].kelurahanModel.ZIPCODE
                },
                "contactSpecification": {
                    "telephoneArea1": _providerInfoAlamatCompany.listAlamatKorespondensi[i].phoneArea1,
                    "telephone1": _providerInfoAlamatCompany.listAlamatKorespondensi[i].phone1,
                    "telephoneArea2": _providerInfoAlamatCompany.listAlamatKorespondensi[i].phoneArea2,
                    "telephone2": _providerInfoAlamatCompany.listAlamatKorespondensi[i].phone2,
                    "faxArea": _providerInfoAlamatCompany.listAlamatKorespondensi[i].faxArea,
                    "fax": _providerInfoAlamatCompany.listAlamatKorespondensi[i].fax,
                    "handphone": "",
                    "email": ""
                },
                "addressType": _providerInfoAlamatCompany.listAlamatKorespondensi[i].jenisAlamatModel.KODE,
                "creationalSpecification": {
                    "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                    "createdBy": _preferences.getString("username"),
                    "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                    "modifiedBy": _preferences.getString("username"),
                },
                "status": _providerInfoAlamatCompany.listAlamatKorespondensi[i].active == 0 ? "ACTIVE" : "INACTIVE",
                "foreignBusinessID": _preferences.getString("last_known_state") != "IDE" ?
                _providerInfoAlamatCompany.listAlamatKorespondensi[i].foreignBusinessID != "NEW" ?
                _providerInfoAlamatCompany.listAlamatKorespondensi[i].foreignBusinessID
                    : "IKGK" : "IKGK", // "NEW"
            });
        }

        List _detailAddressPIC = [];
        for(int i=0; i < _providerAddressManajemenPIC.listManajemenPICAddress.length; i++){
            _detailAddressPIC.add({
                "addressID": _preferences.getString("last_known_state") != "IDE" ? _providerAddressManajemenPIC.listManajemenPICAddress[i].addressID != "NEW" ? _providerAddressManajemenPIC.listManajemenPICAddress[i].addressID : "NEW" : "NEW",
                "addressSpecification": {
                    "koresponden": _providerAddressManajemenPIC.listManajemenPICAddress[i].isCorrespondence ? "1" : "0",
                    "matrixAddr": "9",
                    "address": _providerAddressManajemenPIC.listManajemenPICAddress[i].address,
                    "rt": _providerAddressManajemenPIC.listManajemenPICAddress[i].rt,
                    "rw": _providerAddressManajemenPIC.listManajemenPICAddress[i].rw,
                    "provinsiID": _providerAddressManajemenPIC.listManajemenPICAddress[i].kelurahanModel.PROV_ID,
                    "kabkotID": _providerAddressManajemenPIC.listManajemenPICAddress[i].kelurahanModel.KABKOT_ID,
                    "kecamatanID": _providerAddressManajemenPIC.listManajemenPICAddress[i].kelurahanModel.KEC_ID,
                    "kelurahanID": _providerAddressManajemenPIC.listManajemenPICAddress[i].kelurahanModel.KEL_ID,
                    "zipcode": _providerAddressManajemenPIC.listManajemenPICAddress[i].kelurahanModel.ZIPCODE
                },
                "contactSpecification": {
                    "telephoneArea1": _providerAddressManajemenPIC.listManajemenPICAddress[i].areaCode != null ? _providerAddressManajemenPIC.listManajemenPICAddress[i].areaCode : "0",
                    "telephone1": _providerAddressManajemenPIC.listManajemenPICAddress[i].phone != null ? _providerAddressManajemenPIC.listManajemenPICAddress[i].phone : "0",
                    "telephoneArea2": null,
                    "telephone2": null,
                    "faxArea": null,
                    "fax": null,
                    "handphone": "",
                    "email": ""
                },
                "addressType": _providerAddressManajemenPIC.listManajemenPICAddress[i].jenisAlamatModel.KODE,
                "creationalSpecification": {
                    "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                    "createdBy": _preferences.getString("username"),
                    "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                    "modifiedBy": _preferences.getString("username"),
                },
                "status": _providerAddressManajemenPIC.listManajemenPICAddress[i].active == 0 ? "ACTIVE" : "INACTIVE",
                "foreignBusinessID": _preferences.getString("last_known_state") != "IDE" ?
                _providerAddressManajemenPIC.listManajemenPICAddress[i].addressID != "NEW" ?
                _providerAddressManajemenPIC.listManajemenPICAddress[i].addressID
                    : "CUST001" : "CUST001", // NEW
            });
            _detailAddressesInfoNasabah.add({
                "addressID": _preferences.getString("last_known_state") != "IDE" ? _providerAddressManajemenPIC.listManajemenPICAddress[i].addressID != "NEW" ? _providerAddressManajemenPIC.listManajemenPICAddress[i].addressID : "NEW" : "NEW",
                "addressSpecification": {
                    "koresponden": _providerAddressManajemenPIC.listManajemenPICAddress[i].isCorrespondence ? "1" : "0",
                    "matrixAddr": "9",
                    "address": _providerAddressManajemenPIC.listManajemenPICAddress[i].address,
                    "rt": _providerAddressManajemenPIC.listManajemenPICAddress[i].rt,
                    "rw": _providerAddressManajemenPIC.listManajemenPICAddress[i].rw,
                    "provinsiID": _providerAddressManajemenPIC.listManajemenPICAddress[i].kelurahanModel.PROV_ID,
                    "kabkotID": _providerAddressManajemenPIC.listManajemenPICAddress[i].kelurahanModel.KABKOT_ID,
                    "kecamatanID": _providerAddressManajemenPIC.listManajemenPICAddress[i].kelurahanModel.KEC_ID,
                    "kelurahanID": _providerAddressManajemenPIC.listManajemenPICAddress[i].kelurahanModel.KEL_ID,
                    "zipcode": _providerAddressManajemenPIC.listManajemenPICAddress[i].kelurahanModel.ZIPCODE
                },
                "contactSpecification": {
                    "telephoneArea1": _providerAddressManajemenPIC.listManajemenPICAddress[i].areaCode,
                    "telephone1": _providerAddressManajemenPIC.listManajemenPICAddress[i].phone,
                    "telephoneArea2": null,
                    "telephone2": null,
                    "faxArea": null,
                    "fax": null,
                    "handphone": "",
                    "email": ""
                },
                "addressType": _providerAddressManajemenPIC.listManajemenPICAddress[i].jenisAlamatModel.KODE,
                "creationalSpecification": {
                    "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                    "createdBy": _preferences.getString("username"),
                    "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                    "modifiedBy": _preferences.getString("username"),
                },
                "status": _providerAddressManajemenPIC.listManajemenPICAddress[i].active == 0 ? "ACTIVE" : "INACTIVE",
                "foreignBusinessID": _preferences.getString("last_known_state") != "IDE" ?
                _providerAddressManajemenPIC.listManajemenPICAddress[i].addressID != "NEW" ?
                _providerAddressManajemenPIC.listManajemenPICAddress[i].addressID
                    : "CUST001" : "CUST001", // NEW
            });
        }

        List _shareholdersCorporates = [];
        if(_providerPemegangSahamCompany.listPemegangSahamLembaga.isNotEmpty){
            for(int i=0; i < _providerPemegangSahamCompany.listPemegangSahamLembaga.length; i++){
                debugPrint("CEK SHARE_HOLDER_ID_COM ${_providerPemegangSaham.listPemegangSahamPribadi[i].shareholdersIndividualID}");
                debugPrint("CEK_EST_DATE_SHARE_COM ${_providerPemegangSahamCompany.listPemegangSahamLembaga[i].initialDateForDateOfEstablishment}");
                List _detailAddressShareHolderCompany = [];
                String _foreignBKPemegangSahamCompany = "KSK";
                if(_providerGuarantor.listGuarantorCompany.length < 10) {
                    _foreignBKPemegangSahamCompany = "KSK00" + (i+1).toString();
                } else if(_providerGuarantor.listGuarantorCompany.length < 100) {
                    _foreignBKPemegangSahamCompany = "KSK0" + (i+1).toString();
                } else if(_providerGuarantor.listGuarantorCompany.length < 1000) {
                    _foreignBKPemegangSahamCompany = "KSK" + (i+1).toString();
                }
                for(int j = 0; j < _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress.length; j++){
                    _detailAddressShareHolderCompany.add({
                        "addressID": _preferences.getString("last_known_state") != "IDE"
                            ? _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].addressID != "NEW"
                            ? _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].addressID : "NEW" : "NEW",
                        "addressSpecification": {
                            "koresponden": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].isCorrespondence ? "1" : "0",
                            "matrixAddr": "12",
                            "address": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].address,
                            "rt": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].rt,
                            "rw": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].rw,
                            "provinsiID": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].kelurahanModel.PROV_ID,
                            "kabkotID": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].kelurahanModel.KABKOT_ID,
                            "kecamatanID": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].kelurahanModel.KEC_ID,
                            "kelurahanID": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].kelurahanModel.KEL_ID,
                            "zipcode": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].kelurahanModel.ZIPCODE
                        },
                        "contactSpecification": {
                            "telephoneArea1": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].phoneArea1,
                            "telephone1": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].phone1,
                            "telephoneArea2":_providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].phoneArea2,
                            "telephone2": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].phone2,
                            "faxArea": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].faxArea,
                            "fax": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].fax,
                            "handphone": "",
                            "email": ""
                        },
                        "addressType": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].jenisAlamatModel.KODE,
                        "creationalSpecification": {
                            "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                            "createdBy": _preferences.getString("username"),
                            "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                            "modifiedBy": _preferences.getString("username"),
                        },
                        "status": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].active == 0 ? "ACTIVE" : "INACTIVE",
                        "foreignBusinessID": _preferences.getString("last_known_state") != "IDE" ?
                        _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].foreignBusinessID != "NEW" ?
                        _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].foreignBusinessID
                            : _foreignBKPemegangSahamCompany : _foreignBKPemegangSahamCompany
                    });
                    _detailAddressesInfoNasabah.add({
                        "addressID": _preferences.getString("last_known_state") != "IDE"
                            ? _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].addressID != "NEW"
                            ? _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].addressID : "NEW" : "NEW",
                        "addressSpecification": {
                            "koresponden": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].isCorrespondence ? "1" : "0",
                            "matrixAddr": "12",
                            "address": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].address,
                            "rt": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].rt,
                            "rw": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].rw,
                            "provinsiID": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].kelurahanModel.PROV_ID,
                            "kabkotID": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].kelurahanModel.KABKOT_ID,
                            "kecamatanID": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].kelurahanModel.KEC_ID,
                            "kelurahanID": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].kelurahanModel.KEL_ID,
                            "zipcode": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].kelurahanModel.ZIPCODE
                        },
                        "contactSpecification": {
                            "telephoneArea1": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].phoneArea1,
                            "telephone1": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].phone1,
                            "telephoneArea2":_providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].phoneArea2,
                            "telephone2": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].phone1,
                            "faxArea": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].faxArea,
                            "fax": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].fax,
                            "handphone": "",
                            "email": ""
                        },
                        "addressType": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].jenisAlamatModel.KODE,
                        "creationalSpecification": {
                            "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                            "createdBy": _preferences.getString("username"),
                            "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                            "modifiedBy": _preferences.getString("username"),
                        },
                        "status": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].active == 0 ? "ACTIVE" : "INACTIVE",
                        "foreignBusinessID": _preferences.getString("last_known_state") != "IDE" ?
                        _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].foreignBusinessID != "NEW" ?
                        _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].foreignBusinessID
                            : _foreignBKPemegangSahamCompany : _foreignBKPemegangSahamCompany
                    });
                }
                _shareholdersCorporates.add(
                    {
                        "shareholdersCorporateID": _preferences.getString("last_known_state") != "IDE"
                            ? _providerPemegangSahamCompany.listPemegangSahamLembaga[i].shareholdersCorporateID != null
                            ? _providerPemegangSahamCompany.listPemegangSahamLembaga[i].shareholdersCorporateID : _foreignBKPemegangSahamCompany : _foreignBKPemegangSahamCompany,
                        "businessSpecification": {
                            "economySector": "",
                            "businessField": "",
                            "locationStatus": "",
                            "businessLocation": "",
                            "employeeTotal": 0,
                            "bussinessLengthInMonth": 0,
                            "totalBussinessLengthInMonth": 0
                        },
                        "businessPermitSpecification": {
                            "institutionType": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].typeInstitutionModel.PARA_ID,
                            "profile": null,
                            "fullName": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].institutionName,
                            "deedOfIncorporationNumber": "",
                            "deedEndDate": null,
                            "siupNumber": null,
                            "siupStartDate": null,
                            "tdpNumber": null,
                            "tdpStartDate": null,
                            "establishmentDate": formatDateValidateAndSubmit(DateTime.parse(_providerPemegangSahamCompany.listPemegangSahamLembaga[i].initialDateForDateOfEstablishment)),//"${_providerPemegangSahamCompany.listPemegangSahamLembaga[i].initialDateForDateOfEstablishment} 00:00:00",
                            "isCompany": false,
                            "authorizedCapital": 0,
                            "paidCapital": 0
                        },
                        "customerNpwpSpecification": {
                            "hasNpwp": true,
                            "npwpAddress": null,
                            "npwpFullname": null,
                            "npwpNumber": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].npwp,
                            "npwpType": null,
                            "pkpIdentifier": null
                        },
                        "positionID": null,
                        "shareholding": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].percentShare,
                        "dedupScore": 0.0,
                        "shareholdersCorporateAddresses": _detailAddressShareHolderCompany,
                        "creationalSpecification": {
                            "createdAt":  formatDateValidateAndSubmit(DateTime.now()),
                            "createdBy": _preferences.getString("username"),
                            "modifiedAt":  formatDateValidateAndSubmit(DateTime.now()),
                            "modifiedBy": _preferences.getString("username"),
                        },
                        "status": "ACTIVE"
                    }
                );
            }
        }

        List _pemegangSahamPribadi = [];
        if(_providerPemegangSaham.listPemegangSahamPribadi.isNotEmpty){
            for(int i=0; i< _providerPemegangSaham.listPemegangSahamPribadi.length; i++){
                debugPrint("CEK SHARE_HOLDER_ID ${_providerPemegangSaham.listPemegangSahamPribadi[i].shareholdersIndividualID}");
                List _detailAddressShareHolderIndividu = [];
                String _foreignBKPemegangSahamPribadi = "KSP";
                if(_providerGuarantor.listGuarantorCompany.length < 10) {
                    _foreignBKPemegangSahamPribadi = "KSP00" + (i+1).toString();
                } else if(_providerGuarantor.listGuarantorCompany.length < 100) {
                    _foreignBKPemegangSahamPribadi = "KSP0" + (i+1).toString();
                } else if(_providerGuarantor.listGuarantorCompany.length < 1000) {
                    _foreignBKPemegangSahamPribadi = "KSP" + (i+1).toString();
                }
                for(int j =0; j < _providerPemegangSaham.listPemegangSahamPribadi[i].listAddress.length; j++){
                    _detailAddressShareHolderIndividu.add({
                        "addressID": _preferences.getString("last_known_state") != "IDE" ? _providerPemegangSaham.listPemegangSahamPribadi[i].listAddress[j].addressID != "NEW"
                            ? _providerPemegangSaham.listPemegangSahamPribadi[i].listAddress[j].addressID : "NEW": "NEW",
                        "addressSpecification": {
                            "koresponden": _providerPemegangSaham.listPemegangSahamPribadi[i].listAddress[j].isCorrespondence ? "1" : "0",
                            "matrixAddr": "11",
                            "address": _providerPemegangSaham.listPemegangSahamPribadi[i].listAddress[j].address,
                            "rt": _providerPemegangSaham.listPemegangSahamPribadi[i].listAddress[j].rt,
                            "rw": _providerPemegangSaham.listPemegangSahamPribadi[i].listAddress[j].rw,
                            "provinsiID": _providerPemegangSaham.listPemegangSahamPribadi[i].listAddress[j].kelurahanModel.PROV_ID,
                            "kabkotID": _providerPemegangSaham.listPemegangSahamPribadi[i].listAddress[j].kelurahanModel.KABKOT_ID,
                            "kecamatanID": _providerPemegangSaham.listPemegangSahamPribadi[i].listAddress[j].kelurahanModel.KEC_ID,
                            "kelurahanID": _providerPemegangSaham.listPemegangSahamPribadi[i].listAddress[j].kelurahanModel.KEL_ID,
                            "zipcode": _providerPemegangSaham.listPemegangSahamPribadi[i].listAddress[j].kelurahanModel.ZIPCODE
                        },
                        "contactSpecification": {
                            "telephoneArea1": _providerPemegangSaham.listPemegangSahamPribadi[i].listAddress[j].areaCode,
                            "telephone1": _providerPemegangSaham.listPemegangSahamPribadi[i].listAddress[j].phone,
                            "telephoneArea2": "",
                            "telephone2": "",
                            "faxArea": "",
                            "fax": "",
                            "handphone": "",
                            "email": ""
                        },
                        "addressType": _providerPemegangSaham.listPemegangSahamPribadi[i].listAddress[j].jenisAlamatModel.KODE,
                        "creationalSpecification": {
                            "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                            "createdBy": _preferences.getString("username"),
                            "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                            "modifiedBy": _preferences.getString("username"),
                        },
                        "status": _providerPemegangSaham.listPemegangSahamPribadi[i].listAddress[j].active == 0 ? "ACTIVE" : "INACTIVE",
                        "foreignBusinessID": _preferences.getString("last_known_state") != "IDE" ?
                        _providerPemegangSaham.listPemegangSahamPribadi[i].listAddress[j].foreignBusinessID != "NEW" ?
                        _providerPemegangSaham.listPemegangSahamPribadi[i].listAddress[j].foreignBusinessID
                            : _foreignBKPemegangSahamPribadi :  _foreignBKPemegangSahamPribadi
                    });
                    _detailAddressesInfoNasabah.add({
                        "addressID": _preferences.getString("last_known_state") != "IDE"
                            ? _providerPemegangSaham.listPemegangSahamPribadi[i].listAddress[j].addressID != "NEW"
                            ? _providerPemegangSaham.listPemegangSahamPribadi[i].listAddress[j].addressID : "NEW" : "NEW",
                        "addressSpecification": {
                            "koresponden": _providerPemegangSaham.listPemegangSahamPribadi[i].listAddress[j].isCorrespondence ? "1" : "0",
                            "matrixAddr": "11",
                            "address": _providerPemegangSaham.listPemegangSahamPribadi[i].listAddress[j].address,
                            "rt": _providerPemegangSaham.listPemegangSahamPribadi[i].listAddress[j].rt,
                            "rw": _providerPemegangSaham.listPemegangSahamPribadi[i].listAddress[j].rw,
                            "provinsiID": _providerPemegangSaham.listPemegangSahamPribadi[i].listAddress[j].kelurahanModel.PROV_ID,
                            "kabkotID": _providerPemegangSaham.listPemegangSahamPribadi[i].listAddress[j].kelurahanModel.KABKOT_ID,
                            "kecamatanID": _providerPemegangSaham.listPemegangSahamPribadi[i].listAddress[j].kelurahanModel.KEC_ID,
                            "kelurahanID": _providerPemegangSaham.listPemegangSahamPribadi[i].listAddress[j].kelurahanModel.KEL_ID,
                            "zipcode": _providerPemegangSaham.listPemegangSahamPribadi[i].listAddress[j].kelurahanModel.ZIPCODE
                        },
                        "contactSpecification": {
                            "telephoneArea1": _providerPemegangSaham.listPemegangSahamPribadi[i].listAddress[j].areaCode,
                            "telephone1": _providerPemegangSaham.listPemegangSahamPribadi[i].listAddress[j].phone,
                            "telephoneArea2": "",
                            "telephone2": "",
                            "faxArea": "",
                            "fax": "",
                            "handphone": "",
                            "email": ""
                        },
                        "addressType": _providerPemegangSaham.listPemegangSahamPribadi[i].listAddress[j].jenisAlamatModel.KODE,
                        "creationalSpecification": {
                            "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                            "createdBy": _preferences.getString("username"),
                            "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                            "modifiedBy": _preferences.getString("username"),
                        },
                        "status": _providerPemegangSaham.listPemegangSahamPribadi[i].listAddress[j].active == 0 ? "ACTIVE" : "INACTIVE",
                        "foreignBusinessID": _preferences.getString("last_known_state") != "IDE" ?
                        _providerPemegangSaham.listPemegangSahamPribadi[i].listAddress[j].foreignBusinessID != "NEW" ?
                        _providerPemegangSaham.listPemegangSahamPribadi[i].listAddress[j].foreignBusinessID
                            : _foreignBKPemegangSahamPribadi : _foreignBKPemegangSahamPribadi
                    });
                }
                _pemegangSahamPribadi.add(
                    {
                        "shareholdersIndividualID": _preferences.getString("last_known_state") != "IDE"
                            ? _providerPemegangSaham.listPemegangSahamPribadi[i].shareholdersIndividualID != null
                            ? _providerPemegangSaham.listPemegangSahamPribadi[i].shareholdersIndividualID : _foreignBKPemegangSahamPribadi : _foreignBKPemegangSahamPribadi,
                        "shareholding": _providerPemegangSaham.listPemegangSahamPribadi[i].percentShare != "" ? double.parse(_providerPemegangSaham.listPemegangSahamPribadi[i].percentShare.replaceAll(",", "")) : 0,
                        "dedupScore": 0,
                        "relationshipStatus": _providerPemegangSaham.listPemegangSahamPribadi[i].relationshipStatusSelected != null ? _providerPemegangSaham.listPemegangSahamPribadi[i].relationshipStatusSelected.PARA_FAMILY_TYPE_ID : "",
                        "shareholdersIndividualIdentity": {
                            "identityType": _providerPemegangSaham.listPemegangSahamPribadi[i].typeIdentitySelected != null ? _providerPemegangSaham.listPemegangSahamPribadi[i].typeIdentitySelected.id : "",
                            "identityNumber": _providerPemegangSaham.listPemegangSahamPribadi[i].identityNumber != "" ? _providerPemegangSaham.listPemegangSahamPribadi[i].identityNumber : "",
                            "identityName": _providerPemegangSaham.listPemegangSahamPribadi[i].fullNameIdentity != "" ? _providerPemegangSaham.listPemegangSahamPribadi[i].fullNameIdentity : "",
                            "fullName": _providerPemegangSaham.listPemegangSahamPribadi[i].fullName != "" ? _providerPemegangSaham.listPemegangSahamPribadi[i].fullName : "",
                            "alias": null,
                            "title": null,
                            "dateOfBirth": _providerPemegangSaham.listPemegangSahamPribadi[i].birthOfDate != "" ? formatDateValidateAndSubmit(DateTime.parse(_providerPemegangSaham.listPemegangSahamPribadi[i].birthOfDate)) : "01-01-1900 00:00:00",
                            "placeOfBirth": _providerPemegangSaham.listPemegangSahamPribadi[i].placeOfBirthIdentity != "" ? _providerPemegangSaham.listPemegangSahamPribadi[i].placeOfBirthIdentity : "",
                            "placeOfBirthKabKota": _providerPemegangSaham.listPemegangSahamPribadi[i].birthPlaceIdentityLOV != null ? _providerPemegangSaham.listPemegangSahamPribadi[i].birthPlaceIdentityLOV.KABKOT_ID : "",
                            "gender": _providerPemegangSaham.listPemegangSahamPribadi[i].gender,
                            "identityActiveStart": null,
                            "identityActiveEnd": null,
                            "religion": null,
                            "occupationID": "0",
                            "positionID": null,
                            "maritalStatusID": ""
                        },
                        "shareholderIndividualContact": {
                            "telephoneArea1": null,
                            "telephone1": null,
                            "telephoneArea2": null,
                            "telephone2": null,
                            "faxArea": null,
                            "fax": null,
                            "handphone": null,
                            "email": null
                        },
                        "shareholderIndividualAddresses": _detailAddressShareHolderIndividu,
                        "creationalSpecification": {
                            "createdAt":  formatDateValidateAndSubmit(DateTime.now()),
                            "createdBy": _preferences.getString("username"),
                            "modifiedAt":  formatDateValidateAndSubmit(DateTime.now()),
                            "modifiedBy": _preferences.getString("username"),
                        },
                        "status": "ACTIVE"
                    }
                );
            }
        }

        List _customerCorporateShareholdersIndividualsReason = [];
        for(int i=0; i < _providerPemegangSaham.listPemegangSahamPribadi.length; i++){
            _customerCorporateShareholdersIndividualsReason.add(
                {
                    "shareholdersIndividualIDReason": null,
                    "shareholdersIndividualShareholdingReason": _providerPemegangSaham.listPemegangSahamPribadi[i].isShareChange ? "99" : null,
                    "shareholdersIndividualDedupScoreReason": null,
                    "shareholdersIndividualRelationshipStatusReason": _providerPemegangSaham.listPemegangSahamPribadi[i].isRelationshipStatusChange ? "99" : null,
                    "shareholdersIndividualIdentityTypeReason": _providerPemegangSaham.listPemegangSahamPribadi[i].isIdentityTypeChange ? "99" : null,
                    "shareholdersIndividualIdentityNumberReason": _providerPemegangSaham.listPemegangSahamPribadi[i].isIdentityNoChange ? "99" : null,
                    "shareholdersIndividualIdentityNameReason": _providerPemegangSaham.listPemegangSahamPribadi[i].isFullnameIDChange ? "99" : null,
                    "shareholdersIndividualIdentityFullNameReason": _providerPemegangSaham.listPemegangSahamPribadi[i].isFullnameChange ? "99" : null,
                    "shareholdersIndividualIdentityAliasReason": null,
                    "shareholdersIndividualIdentityTitleReason": null,
                    "shareholdersIndividualIdentityDateOfBirthReason": _providerPemegangSaham.listPemegangSahamPribadi[i].isDateOfBirthChange ? "99" : null,
                    "shareholdersIndividualIdentityPlaceOfBirthReason": _providerPemegangSaham.listPemegangSahamPribadi[i].isPlaceOfBirthChange ? "99" : null,
                    "shareholdersIndividualIdentityPlaceOfBirthKabKotaReason": _providerPemegangSaham.listPemegangSahamPribadi[i].isPlaceOfBirthLOVChange ? "99" : null,
                    "shareholdersIndividualIdentityGenderReason": _providerPemegangSaham.listPemegangSahamPribadi[i].isGenderChange ? "99" : null,
                    "shareholdersIndividualIdentityActiveStartReason": null,
                    "shareholdersIndividualIdentityActiveEndReason": null,
                    "shareholdersIndividualIdentityReligionReason": null,
                    "shareholdersIndividualIdentityOccupationIDReason": null,
                    "shareholdersIndividualIdentityPositionIDReason": null,
                    "shareholdersIndividualIdentityMaritalStatusIDReason": null,
                    "shareholdersIndividualContactTelephoneArea1Reason": null,
                    "shareholdersIndividualContactTelephone1Reason": null,
                    "shareholdersIndividualContactTelephoneArea2Reason": null,
                    "shareholdersIndividualContactTelephone2Reason": null,
                    "shareholdersIndividualContactFaxAreaReason": null,
                    "shareholdersIndividualContactFaxReason": null,
                    "shareholdersIndividualContactHandphoneReason": null,
                    "shareholdersIndividualContactEmailReason": null,
                    "shareholdersIndividualCreatedAtReason": null,
                    "shareholdersIndividualCreatedByReason": null,
                    "shareholdersIndividualModifiedAtReason": null,
                    "shareholdersIndividualModifiedByReason": null,
                    "shareholdersIndividualStatusReason": null,
                    "shareholdersIndividualFlagCorrectionReason": null,
                }
            );
        }

        List _customerCorporateShareholdersCorporatesReason = [];
        for(int i=0; i < _providerPemegangSahamCompany.listPemegangSahamLembaga.length; i++){
            _customerCorporateShareholdersCorporatesReason.add(
                {
                    "shareholdersCorporateIDReason": null,
                    "shareholdersCorporateEconomySectorReason": null,
                    "shareholdersCorporateBusinessFieldReason": null,
                    "shareholdersCorporateLocationStatusReason": null,
                    "shareholdersCorporateBusinessLocationReason": null,
                    "shareholdersCorporateEmployeeTotalReason": null,
                    "shareholdersCorporateBussinessLengthInMonthReason": null,
                    "shareholdersCorporateTotalBussinessLengthInMonthReason": null,
                    "shareholdersCorporateInstitutionTypeReason": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].isTypeInstitutionModelChange ? "99" : null,
                    "shareholdersCorporateProfileReason": null,
                    "shareholdersCorporateFullNameReason": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].isInstitutionNameChange ? "99" : null,
                    "shareholdersCorporateDeedOfIncorporationNumberReason": null,
                    "shareholdersCorporateDeedEndDateReason": null,
                    "shareholdersCorporateSiupNumberReason": null,
                    "shareholdersCorporateSiupStartDateReason": null,
                    "shareholdersCorporateTdpNumberReason": null,
                    "shareholdersCorporateTdpStartDateReason": null,
                    "shareholdersCorporateEstablishmentDateReason": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].isInitialDateForDateOfEstablishmentChange ? "99" : null,
                    "shareholdersCorporateIsCompanyReason": null,
                    "shareholdersCorporateAuthorizedCapitalReason": null,
                    "shareholdersCorporatePaidCapitalReason": null,
                    "shareholdersCorporateHasNpwpReason": null,
                    "shareholdersCorporateNpwpNumberReason": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].isNpwpChange ? "99" : null,
                    "shareholdersCorporateNpwpFullNameReason": null,
                    "shareholdersCorporateNpwpTypeReason": null,
                    "shareholdersCorporatePkpIdentifierTypeReason": null,
                    "shareholdersCorporateAddressReason": null,
                    "shareholdersCorporatePositionIDReason": null,
                    "shareholdersCorporateShareholdingReason": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].isPercentShareChange ? "99" : null,
                    "shareholdersCorporateDedupScoreReason": null,
                    "shareholdersCorporateCreatedAtReason": null,
                    "shareholdersCorporateCreatedByReason": null,
                    "shareholdersCorporateModifiedAtReason": null,
                    "shareholdersCorporateModifiedByReason": null,
                    "shareholdersCorporateStatusReason": null,
                    "shareholdersCorporateFlagCorrectionReason": null,
                }
            );
        }

        List _customerGuarantorCorporatesReason = [];
        List _customerGuarantorIndividualsReason = [];
        for(int i=0; i < _providerGuarantor.listGuarantorCompany.length; i++){
            _customerGuarantorCorporatesReason.add(
                {
                    "guarantorCorporateIDReason": null,
                    "guarantorCorporateDedupScoreReason": null,
                    "guarantorCorporateBussinessPermitInstitutionTypeReason": _providerGuarantor.listGuarantorCompany[i].companyTypeDakor ? "99" : null,
                    "guarantorCorporateBussinessPermitProfileReason": null,
                    "guarantorCorporateBussinessPermitFullNameReason": _providerGuarantor.listGuarantorCompany[i].companyNameDakor ? "99" : null,
                    "guarantorCorporateBussinessPermitDeedOfIncorporationNumberReason": null,
                    "guarantorCorporateBussinessPermitDeedEndDateReason": null,
                    "guarantorCorporateBussinessPermitSiupNumberReason": null,
                    "guarantorCorporateBussinessPermitSiupStartDateReason": null,
                    "guarantorCorporateBussinessPermitTdpNumberReason": null,
                    "guarantorCorporateBussinessPermitTdpStartDateReason": null,
                    "guarantorCorporateBussinessPermitEstablishmentDateReason": _providerGuarantor.listGuarantorCompany[i].establishedDateDakor ? "99" : null,
                    "guarantorCorporateBussinessPermitIsCompanyReason": null,
                    "guarantorCorporateBussinessPermitAuthorizedCapitalReason": null,
                    "guarantorCorporateBussinessPermitPaidCapitalReason": null,
                    "guarantorCorporateBusinessEconomySectorReason": null,
                    "guarantorCorporateBusinessBusinessFieldReason": null,
                    "guarantorCorporateBusinessLocationStatusReason": null,
                    "guarantorCorporateBusinessBusinessLocationReason": null,
                    "guarantorCorporateBusinessBusinessEmployeeTotalReason": null,
                    "guarantorCorporateBusinessBussinessLengthInMonthReason": null,
                    "guarantorCorporateBusinessTotalBussinessLengthInMonthReason": null,
                    "guarantorCorporateHasNpwpReason": _providerGuarantor.listGuarantorCompany[i].npwpDakor ? "99" : null,
                    "guarantorCorporateNpwpAddressReason": null,
                    "guarantorCorporateNpwpFullnameReason": null,
                    "guarantorCorporateNpwpNumberReason": null,
                    "guarantorCorporateNpwpTypeReason": null,
                    "guarantorCorporatePkpIdentifierReason": null,
                    "guarantorCorporateCreationalCreatedAtReason": null,
                    "guarantorCorporateCreationalCreatedByReason": null,
                    "guarantorCorporateCreationalModifiedAtReason": null,
                    "guarantorCorporateCreationalModifiedByReason": null,
                    "guarantorCorporateStatusReason": null,
                    "guarantorCorporateFlagCorrectionReason": null,
                }
            );
        }

        for(int i=0; i < _providerGuarantor.listGuarantorIndividual.length; i++){
            _customerGuarantorIndividualsReason.add(
                {
                    "guarantorIndividualIDReason": null,
                    "guarantorIndividualRelationshipStatusReason": _providerGuarantor.listGuarantorIndividual[i].relationshipStatusDakor ? "99" : null,
                    "guarantorIndividualDedupScoreReason": null,
                    "guarantorIndividualIdentityIdentityTypeReason": _providerGuarantor.listGuarantorIndividual[i].identityTypeDakor ? "99" : null,
                    "guarantorIndividualIdentityIdentityNumberReason": _providerGuarantor.listGuarantorIndividual[i].identityNoDakor ? "99" : null,
                    "guarantorIndividualIdentityIdentityNameReason": _providerGuarantor.listGuarantorIndividual[i].fullnameIDDakor ? "99" : null,
                    "guarantorIndividualIdentityFullNameReason": _providerGuarantor.listGuarantorIndividual[i].fullNameDakor ? "99" : null,
                    "guarantorIndividualIdentityAliasReason": null,
                    "guarantorIndividualIdentityTitleReason": null,
                    "guarantorIndividualIdentityDateOfBirthReason": _providerGuarantor.listGuarantorIndividual[i].dateOfBirthDakor ? "99" : null,
                    "guarantorIndividualIdentityPlaceOfBirthReason": _providerGuarantor.listGuarantorIndividual[i].placeOfBirthDakor ? "99" : null,
                    "guarantorIndividualIdentityPlaceOfBirthKabKotaReason": _providerGuarantor.listGuarantorIndividual[i].placeOfBirthLOVDakor ? "99" : null,
                    "guarantorIndividualIdentityGenderReason": _providerGuarantor.listGuarantorIndividual[i].genderDakor ? "99" : null,
                    "guarantorIndividualIdentityIdentityActiveStartReason": null,
                    "guarantorIndividualIdentityIdentityActiveEndReason": null,
                    "guarantorIndividualIdentityReligionReason": null,
                    "guarantorIndividualIdentityOccupationIDReason": null,
                    "guarantorIndividualIdentityPositionIDReason": null,
                    "guarantorIndividualContactTelephoneArea1Reason": null,
                    "guarantorIndividualContactTelephone1Reason": null,
                    "guarantorIndividualContactTelephoneArea2Reason": null,
                    "guarantorIndividualContactTelephone2Reason": null,
                    "guarantorIndividualContactFaxAreaReason": null,
                    "guarantorIndividualContactFaxReason": null,
                    "guarantorIndividualContactHandphoneReason": _providerGuarantor.listGuarantorIndividual[i].phoneDakor ? "99" : null,
                    "guarantorIndividualContactEmailReason": null,
                    "guarantorIndividualCreationalCreatedAtReason": null,
                    "guarantorIndividualCreationalCreatedByReason": null,
                    "guarantorIndividualCreationalModifiedAtReason": null,
                    "guarantorIndividualCreationalModifiedByReason": null,
                    "guarantorIndividualStatusReason": null,
                    "guarantorIndividualFlagCorrectionReason": null,
                }
            );
        }

        var _detailAddressesReason = [];
        for(int i=0; i < _providerInfoAlamatCompany.listAlamatKorespondensi.length; i++){
            _detailAddressesReason.add({
                "addressIDReason": null,
                "addressForeignBusinessIDReason": null,
                "addressSpecificationKorespondenReason": null,
                "addressSpecificationMatrixAddrReason": null,
                "addressSpecificationAddressReason": _providerInfoAlamatCompany.listAlamatKorespondensi[i].isAddressDakor ? "99" : null,
                "addressSpecificationRtReason": _providerInfoAlamatCompany.listAlamatKorespondensi[i].isRTDakor ? "99" : null,
                "addressSpecificationRwReason": _providerInfoAlamatCompany.listAlamatKorespondensi[i].isRWDakor ? "99" : null,
                "addressSpecificationProvinsiIDReason": _providerInfoAlamatCompany.listAlamatKorespondensi[i].isProvinsiDakor ? "99" : null,
                "addressSpecificationKabkotIDReason": _providerInfoAlamatCompany.listAlamatKorespondensi[i].isKabKotDakor ? "99" : null,
                "addressSpecificationKecamatanIDReason": _providerInfoAlamatCompany.listAlamatKorespondensi[i].isKecamatanDakor ? "99" : null,
                "addressSpecificationKelurahanIDReason": _providerInfoAlamatCompany.listAlamatKorespondensi[i].isKelurahanDakor ? "99" : null,
                "addressSpecificationZipcodeReason": _providerInfoAlamatCompany.listAlamatKorespondensi[i].isKodePosDakor ? "99" : null,
                "addressContactSpecificationTelephoneArea1Reason": _providerInfoAlamatCompany.listAlamatKorespondensi[i].isTelp1AreaDakor ? "99" : null,
                "addressContactSpecificationTelephone1Reason": _providerInfoAlamatCompany.listAlamatKorespondensi[i].isTelp1Dakor ? "99" : null,
                "addressContactSpecificationTelephoneArea2Reason": _providerInfoAlamatCompany.listAlamatKorespondensi[i].isTelp2AreaDakor ? "99" : null,
                "addressContactSpecificationTelephone2Reason": _providerInfoAlamatCompany.listAlamatKorespondensi[i].isTelp2Dakor ? "99" : null,
                "addressContactSpecificationFaxAreaReason": _providerInfoAlamatCompany.listAlamatKorespondensi[i].isFaxAreaDakor ? "99" : null,
                "addressContactSpecificationFaxReason": _providerInfoAlamatCompany.listAlamatKorespondensi[i].isFaxDakor ? "99" : null,
                "addressContactSpecificationHandphoneReason": null,
                "addressContactSpecificationEmailReason": null,
                "addressTypeReason": _providerInfoAlamatCompany.listAlamatKorespondensi[i].isAddressTypeDakor ? "99" : null,
                "addressCreationalSpecificationCreatedAtReason": null,
                "addressCreationalSpecificationCreatedByReason": null,
                "addressCreationalSpecificationModifiedAtReason": null,
                "addressCreationalSpecificationModifiedByReason": null,
                "addressStatusReason": null,
                "addressFlagCorrectionReason": null,
            });
        }

        for(int i=0; i < _providerGuarantor.listGuarantorIndividual.length; i++){
            for(int j=0; j < _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel.length; j++){
                _detailAddressesReason.add({
                    "addressIDReason": null,
                    "addressForeignBusinessIDReason": null,
                    "addressSpecificationKorespondenReason": null,
                    "addressSpecificationMatrixAddrReason": null,
                    "addressSpecificationAddressReason": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].isAlamatChanges ? "99" : null,
                    "addressSpecificationRtReason": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].isRTChanges ? "99" : null,
                    "addressSpecificationRwReason": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].isRWChanges ? "99" : null,
                    "addressSpecificationProvinsiIDReason": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].isProvinsiChanges ? "99" : null,
                    "addressSpecificationKabkotIDReason": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].isKotaChanges ? "99" : null,
                    "addressSpecificationKecamatanIDReason": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].isKecamatanChanges ? "99" : null,
                    "addressSpecificationKelurahanIDReason": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].isKelurahanChanges ? "99" : null,
                    "addressSpecificationZipcodeReason": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].isPostalCodeChanges ? "99" : null,
                    "addressContactSpecificationTelephoneArea1Reason": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].isTeleponAreaChanges ? "99" : null,
                    "addressContactSpecificationTelephone1Reason": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].isTeleponChanges ? "99" : null,
                    "addressContactSpecificationTelephoneArea2Reason": null,
                    "addressContactSpecificationTelephone2Reason": null,
                    "addressContactSpecificationFaxAreaReason": null,
                    "addressContactSpecificationFaxReason": null,
                    "addressContactSpecificationHandphoneReason": null,
                    "addressContactSpecificationEmailReason": null,
                    "addressTypeReason": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].isAddressTypeChanges ? "99" : null,
                    "addressCreationalSpecificationCreatedAtReason": null,
                    "addressCreationalSpecificationCreatedByReason": null,
                    "addressCreationalSpecificationModifiedAtReason": null,
                    "addressCreationalSpecificationModifiedByReason": null,
                    "addressStatusReason": null,
                    "addressFlagCorrectionReason": null,
                });
            }
        }

        for(int i=0; i < _providerGuarantor.listGuarantorCompany.length; i++){
            for(int j=0; j < _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel.length; j++){
                _detailAddressesReason.add({
                    "addressIDReason": null,
                    "addressForeignBusinessIDReason": null,
                    "addressSpecificationKorespondenReason": null,
                    "addressSpecificationMatrixAddrReason": null,
                    "addressSpecificationAddressReason": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].isAddressDakor ? "99" : null,
                    "addressSpecificationRtReason": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].isRTDakor ? "99" : null,
                    "addressSpecificationRwReason": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].isRWDakor ? "99" : null,
                    "addressSpecificationProvinsiIDReason": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].isProvinsiDakor ? "99" : null,
                    "addressSpecificationKabkotIDReason": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].isKabKotDakor ? "99" : null,
                    "addressSpecificationKecamatanIDReason": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].isKecamatanDakor ? "99" : null,
                    "addressSpecificationKelurahanIDReason": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].isKelurahanDakor ? "99" : null,
                    "addressSpecificationZipcodeReason": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].isKodePosDakor ? "99" : null,
                    "addressContactSpecificationTelephoneArea1Reason": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].isTelp1AreaDakor ? "99" : null,
                    "addressContactSpecificationTelephone1Reason": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].isTelp1Dakor ? "99" : null,
                    "addressContactSpecificationTelephoneArea2Reason": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].isTelp2AreaDakor ? "99" : null,
                    "addressContactSpecificationTelephone2Reason": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].isTelp2Dakor ? "99" : null,
                    "addressContactSpecificationFaxAreaReason": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].isFaxAreaDakor ? "99" : null,
                    "addressContactSpecificationFaxReason": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].isFaxDakor ? "99" : null,
                    "addressContactSpecificationHandphoneReason": null,
                    "addressContactSpecificationEmailReason": null,
                    "addressTypeReason": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].isAddressTypeDakor ? "99" : null,
                    "addressCreationalSpecificationCreatedAtReason": null,
                    "addressCreationalSpecificationCreatedByReason": null,
                    "addressCreationalSpecificationModifiedAtReason": null,
                    "addressCreationalSpecificationModifiedByReason": null,
                    "addressStatusReason": null,
                    "addressFlagCorrectionReason": null,
                });
            }
        }

        for(int i=0; i < _providerPemegangSaham.listPemegangSahamPribadi.length; i++){
            for(int j=0; j < _providerPemegangSaham.listPemegangSahamPribadi[i].listAddress.length; j++){
                _detailAddressesReason.add({
                    "addressIDReason": null,
                    "addressForeignBusinessIDReason": null,
                    "addressSpecificationKorespondenReason": null,
                    "addressSpecificationMatrixAddrReason": null,
                    "addressSpecificationAddressReason": _providerPemegangSaham.listPemegangSahamPribadi[i].listAddress[j].isAlamatChanges ? "99" : null,
                    "addressSpecificationRtReason": _providerPemegangSaham.listPemegangSahamPribadi[i].listAddress[j].isRTChanges ? "99" : null,
                    "addressSpecificationRwReason": _providerPemegangSaham.listPemegangSahamPribadi[i].listAddress[j].isRWChanges ? "99" : null,
                    "addressSpecificationProvinsiIDReason": _providerPemegangSaham.listPemegangSahamPribadi[i].listAddress[j].isProvinsiChanges ? "99" : null,
                    "addressSpecificationKabkotIDReason": _providerPemegangSaham.listPemegangSahamPribadi[i].listAddress[j].isKotaChanges ? "99" : null,
                    "addressSpecificationKecamatanIDReason": _providerPemegangSaham.listPemegangSahamPribadi[i].listAddress[j].isKecamatanChanges ? "99" : null,
                    "addressSpecificationKelurahanIDReason": _providerPemegangSaham.listPemegangSahamPribadi[i].listAddress[j].isKelurahanChanges ? "99" : null,
                    "addressSpecificationZipcodeReason": _providerPemegangSaham.listPemegangSahamPribadi[i].listAddress[j].isPostalCodeChanges ? "99" : null,
                    "addressContactSpecificationTelephoneArea1Reason": _providerPemegangSaham.listPemegangSahamPribadi[i].listAddress[j].isTeleponAreaChanges ? "99" : null,
                    "addressContactSpecificationTelephone1Reason": _providerPemegangSaham.listPemegangSahamPribadi[i].listAddress[j].isTeleponAreaChanges ? "99" : null,
                    "addressContactSpecificationTelephoneArea2Reason": null,
                    "addressContactSpecificationTelephone2Reason": null,
                    "addressContactSpecificationFaxAreaReason": null,
                    "addressContactSpecificationFaxReason": null,
                    "addressContactSpecificationHandphoneReason": null,
                    "addressContactSpecificationEmailReason": null,
                    "addressTypeReason": _providerPemegangSaham.listPemegangSahamPribadi[i].listAddress[j].isAddressTypeChanges ? "99" : null,
                    "addressCreationalSpecificationCreatedAtReason": null,
                    "addressCreationalSpecificationCreatedByReason": null,
                    "addressCreationalSpecificationModifiedAtReason": null,
                    "addressCreationalSpecificationModifiedByReason": null,
                    "addressStatusReason": null,
                    "addressFlagCorrectionReason": null,
                });
            }
        }

        for(int i=0; i < _providerPemegangSahamCompany.listPemegangSahamLembaga.length; i++){
            for(int j=0; j < _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress.length; j++){
                _detailAddressesReason.add({
                    "addressIDReason": null,
                    "addressForeignBusinessIDReason": null,
                    "addressSpecificationKorespondenReason": null,
                    "addressSpecificationMatrixAddrReason": null,
                    "addressSpecificationAddressReason": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].isAddressDakor ? "99" : null,
                    "addressSpecificationRtReason": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].isRTDakor ? "99" : null,
                    "addressSpecificationRwReason": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].isRWDakor ? "99" : null,
                    "addressSpecificationProvinsiIDReason": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].isProvinsiDakor ? "99" : null,
                    "addressSpecificationKabkotIDReason": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].isKabKotDakor ? "99" : null,
                    "addressSpecificationKecamatanIDReason": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].isKecamatanDakor ? "99" : null,
                    "addressSpecificationKelurahanIDReason": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].isKelurahanDakor ? "99" : null,
                    "addressSpecificationZipcodeReason": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].isKodePosDakor ? "99" : null,
                    "addressContactSpecificationTelephoneArea1Reason": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].isTelp1AreaDakor ? "99" : null,
                    "addressContactSpecificationTelephone1Reason": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].isTelp1Dakor ? "99" : null,
                    "addressContactSpecificationTelephoneArea2Reason": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].isTelp2AreaDakor ? "99" : null,
                    "addressContactSpecificationTelephone2Reason": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].isTelp2Dakor ? "99" : null,
                    "addressContactSpecificationFaxAreaReason": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].isFaxAreaDakor ? "99" : null,
                    "addressContactSpecificationFaxReason": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].isFaxDakor ? "99" : null,
                    "addressContactSpecificationHandphoneReason": null,
                    "addressContactSpecificationEmailReason": null,
                    "addressTypeReason": _providerPemegangSahamCompany.listPemegangSahamLembaga[i].listAddress[j].isAddressTypeDakor ? "99" : null,
                    "addressCreationalSpecificationCreatedAtReason": null,
                    "addressCreationalSpecificationCreatedByReason": null,
                    "addressCreationalSpecificationModifiedAtReason": null,
                    "addressCreationalSpecificationModifiedByReason": null,
                    "addressStatusReason": null,
                    "addressFlagCorrectionReason": null,
                });
            }
        }

        for(int i=0; i < _providerAddressManajemenPIC.listManajemenPICAddress.length; i++){
            _detailAddressesReason.add({
                "addressIDReason": null,
                "addressForeignBusinessIDReason": null,
                "addressSpecificationKorespondenReason": null,
                "addressSpecificationMatrixAddrReason": null,
                "addressSpecificationAddressReason": _providerAddressManajemenPIC.listManajemenPICAddress[i].isAlamatChanges ? "99" : null,
                "addressSpecificationRtReason": _providerAddressManajemenPIC.listManajemenPICAddress[i].isRTChanges ? "99" : null,
                "addressSpecificationRwReason": _providerAddressManajemenPIC.listManajemenPICAddress[i].isRWChanges ? "99" : null,
                "addressSpecificationProvinsiIDReason": _providerAddressManajemenPIC.listManajemenPICAddress[i].isProvinsiChanges ? "99" : null,
                "addressSpecificationKabkotIDReason": _providerAddressManajemenPIC.listManajemenPICAddress[i].isKotaChanges ? "99" : null,
                "addressSpecificationKecamatanIDReason": _providerAddressManajemenPIC.listManajemenPICAddress[i].isKecamatanChanges ? "99" : null,
                "addressSpecificationKelurahanIDReason": _providerAddressManajemenPIC.listManajemenPICAddress[i].isKelurahanChanges ? "99" : null,
                "addressSpecificationZipcodeReason": _providerAddressManajemenPIC.listManajemenPICAddress[i].isPostalCodeChanges ? "99" : null,
                "addressContactSpecificationTelephoneArea1Reason": _providerAddressManajemenPIC.listManajemenPICAddress[i].isTeleponAreaChanges ? "99" : null,
                "addressContactSpecificationTelephone1Reason": _providerAddressManajemenPIC.listManajemenPICAddress[i].isTeleponChanges ? "99" : null,
                "addressContactSpecificationTelephoneArea2Reason": null,
                "addressContactSpecificationTelephone2Reason": null,
                "addressContactSpecificationFaxAreaReason": null,
                "addressContactSpecificationFaxReason": null,
                "addressContactSpecificationHandphoneReason": null,
                "addressContactSpecificationEmailReason": null,
                "addressTypeReason": _providerAddressManajemenPIC.listManajemenPICAddress[i].isAddressTypeChanges ? "99" : null,
                "addressCreationalSpecificationCreatedAtReason": null,
                "addressCreationalSpecificationCreatedByReason": null,
                "addressCreationalSpecificationModifiedAtReason": null,
                "addressCreationalSpecificationModifiedByReason": null,
                "addressStatusReason": null,
                "addressFlagCorrectionReason": null,
            });
        }

        try{
            var _oldApplicationDTO = await OldApplicationDTOProvider().oldApplication(_providerApp.applNo);

            var _body = _preferences.getString("status_aoro") == "1" || _preferences.getString("status_aoro") == "2"
            ?
            jsonEncode({
                "oldApplicationDTO": _oldApplicationDTO,
                "newApplicationDTO": {
                    "customerDTO": {
                        "customerID": _preferences.getString("last_known_state") != "IDE" ? _providerRincian.customerID : "NEW",// perlu di rencanakan mau ditaruh di sqlite atau di sharedPreference
                        "customerType": _preferences.getString("cust_type") == "PER" ? "PER" : "COM",
                        "customerSpecification": {
                            "customerNPWP": {
                                "npwpAddress": _providerRincian.controllerNPWPAddress.text,//_providerInfoNasabah.radioValueIsHaveNPWP == 1 ? _providerInfoNasabah.controllerAlamatSesuaiNPWP.text : _providerInfoAlamat.listAlamatKorespondensi[0].address,
                                "npwpFullname": _providerRincian.controllerFullNameNPWP.text,//_providerInfoNasabah.radioValueIsHaveNPWP == 1 ? _providerInfoNasabah.controllerNamaSesuaiNPWP.text  : _providerInfoNasabah.controllerNamaLengkapSesuaiIdentitas.text,
                                "npwpNumber": _providerRincian.controllerNPWP.text,//_providerInfoNasabah.radioValueIsHaveNPWP == 1 ? _providerInfoNasabah.controllerNoNPWP.text : "00000000000000",
                                "npwpType": _providerRincian.typeNPWPSelected != null ? _providerRincian.typeNPWPSelected.id : "2",
                                "hasNpwp": _providerRincian.radioValueIsHaveNPWP == 1 ? true : false,//_providerInfoNasabah.radioValueIsHaveNPWP == 1 ? true : false,
                                "pkpIdentifier": _providerRincian.signPKPSelected != null ? _providerRincian.signPKPSelected.id : "2",
                            },
                            "detailAddresses": _detailAddressesInfoNasabah,
                            "customerGuarantor": {
                                "isGuarantor": _providerGuarantor.radioValueIsWithGuarantor == 0,
                                "guarantorCorporates": _guarantorCorporates,
                                "guarantorIndividuals": _guarantorIndividuals
                            }
                        },
                        "customerIndividualDTO": [],
                        "customerCorporateDTO": [
                            {
                                "customerCorporateID": _preferences.getString("last_known_state") != "IDE" ? _providerRincian.customerCorporateID : "CUST001",
                                "businessSpecification": {
                                    "economySector": _providerRincian.sectorEconomicModelSelected != null ? _providerRincian.sectorEconomicModelSelected.KODE : null,//_preferences.getString("cust_type") == "PER" ? "" : _providerRincian.sectorEconomicModelSelected.KODE,
                                    "businessField": _providerRincian.businessFieldModelSelected != null ? _providerRincian.businessFieldModelSelected.KODE : null ,//_preferences.getString("cust_type") == "PER" ? "" : _providerRincian.businessFieldModelSelected.KODE,
                                    "locationStatus": _providerRincian.locationStatusSelected != null ?  _providerRincian.locationStatusSelected.id : null,//_preferences.getString("cust_type") == "PER" ? "" : _providerRincian.locationStatusSelected.id,
                                    "businessLocation": _providerRincian.businessLocationSelected != null ? _providerRincian.businessLocationSelected.id : null,//_preferences.getString("cust_type") == "PER" ? "" : _providerRincian.businessLocationSelected.id,
                                    "employeeTotal": _providerRincian.controllerTotalEmployees.text != "" ? int.parse(_providerRincian.controllerTotalEmployees.text) : null,//_preferences.getString("cust_type") == "PER" ? 0 : double.parse(_providerRincian.controllerTotalEmployees.text.replaceAll(",", "")),
                                    "bussinessLengthInMonth": _providerRincian.controllerLamaUsahaBerjalan.text != "" ? double.parse(_providerRincian.controllerLamaUsahaBerjalan.text.replaceAll(",", "")) : null,//_preferences.getString("cust_type") == "PER" ? 0 : double.parse(_providerRincian.controllerLamaUsahaBerjalan.text.replaceAll(",", "")),
                                    "totalBussinessLengthInMonth": _providerRincian.controllerTotalLamaUsahaBerjalan.text != "" ? int.parse(_providerRincian.controllerTotalLamaUsahaBerjalan.text.replaceAll(",", "")) : null,//_preferences.getString("cust_type") == "PER" ? 0 : double.parse(_providerRincian.controllerTotalLamaUsahaBerjalan.text.replaceAll(",", "")),
                                },
                                "businessPermitSpecification": {
                                    "institutionType": _providerRincian.typeInstitutionSelected != null ?  _providerRincian.typeInstitutionSelected.PARA_ID : "",//_preferences.getString("cust_type") == "PER" ? "" : _providerRincian.typeInstitutionSelected.PARA_ID,
                                    "profile": _providerRincian.profilSelected != null ? _providerRincian.profilSelected.id : "",//_preferences.getString("cust_type") == "PER" ? "" : _providerRincian.profilSelected.id,
                                    "fullName": _providerRincian.controllerInstitutionName.text != "" ? _providerRincian.controllerInstitutionName.text : "",//_preferences.getString("cust_type") == "PER" ? "" : _providerRincian.controllerInstitutionName.text,
                                    "deedOfIncorporationNumber": null,
                                    "deedEndDate": null,
                                    "siupNumber": null,
                                    "siupStartDate": null,
                                    "tdpNumber": null,
                                    "tdpStartDate": null,
                                    "establishmentDate": _providerRincian.controllerDateEstablishment.text != "" ? "${_providerRincian.controllerDateEstablishment.text} 00:00:00" : "01-01-1900 00:00:00",//formatDateValidateAndSubmit(_providerRincian.initialDateForDateEstablishment),//_preferences.getString("cust_type") == "PER" ? "" : _providerRincian.controllerDateEstablishment.text,
                                    "isCompany": false,
                                    "authorizedCapital": 0,
                                    "paidCapital": 0
                                },
                                "managementPIC": {
                                    "picIdentity": {
                                        "identityType": _providerManajemenPIC.typeIdentitySelected != null ? _providerManajemenPIC.typeIdentitySelected.id : "",
                                        "identityNumber":  _providerManajemenPIC.controllerIdentityNumber.text != "" ? _providerManajemenPIC.controllerIdentityNumber.text : "",
                                        "identityName": _providerManajemenPIC.controllerFullNameIdentity.text != "" ? _providerManajemenPIC.controllerFullNameIdentity.text : "",
                                        "fullName": _providerManajemenPIC.controllerFullName.text != "" ? _providerManajemenPIC.controllerFullName.text : "",
                                        "alias": null,
                                        "title": null,
                                        "dateOfBirth": _providerManajemenPIC.controllerBirthOfDate.text != "" ? formatDateValidateAndSubmit(_providerManajemenPIC.initialDateForBirthOfDate) : "",//_preferences.getString("cust_type") == "PER" ? "" : _providerManajemenPIC.controllerBirthOfDate.text,
                                        "placeOfBirth": _providerManajemenPIC.controllerPlaceOfBirthIdentity.text != "" ? _providerManajemenPIC.controllerPlaceOfBirthIdentity.text : "",//_preferences.getString("cust_type") == "PER" ? "" : _providerManajemenPIC.controllerPlaceOfBirthIdentity.text,
                                        "placeOfBirthKabKota": _providerManajemenPIC.birthPlaceSelected != null ? _providerManajemenPIC.birthPlaceSelected.KABKOT_ID : "",//_preferences.getString("cust_type") == "PER" ? "" : _providerManajemenPIC.birthPlaceSelected.KABKOT_ID,
                                        "gender": null,
                                        "identityActiveStart": null,
                                        "identityActiveEnd": null,
                                        "isLifetime": null,
                                        "religion": null,
                                        "occupationID": null,
                                        "positionID": _providerManajemenPIC.positionSelected != null ? _providerManajemenPIC.positionSelected.PARA_FAMILY_TYPE_ID : "10", // "02"
                                        "maritalStatusID": "02"
                                    },
                                    "picContact": {
                                        "telephoneArea1": null,
                                        "telephone1": null,
                                        "telephoneArea2": null,
                                        "telephone2": null,
                                        "faxArea": null,
                                        "fax": null,
                                        "handphone": _providerManajemenPIC.controllerHandphoneNumber.text != "" ? "08${_providerManajemenPIC.controllerHandphoneNumber.text}" : "",//_preferences.getString("cust_type") == "PER" ? "" : _providerManajemenPIC.controllerHandphoneNumber.text,
                                        "email": _providerManajemenPIC.controllerEmail.text != "" ? _providerManajemenPIC.controllerEmail.text : "",//_preferences.getString("cust_type") == "PER" ? "" : _providerManajemenPIC.controllerEmail.text,
                                        "noWa": null,
                                        "noWa2": null,
                                        "noWa3": null
                                    },
                                    "picAddresses": _detailAddressPIC,
                                    "shareholding": 0
                                },
                                "shareholdersCorporates": _shareholdersCorporates,
                                "shareholdersIndividuals": _pemegangSahamPribadi,
                                "dedupScore": 0.0,
                                "status": "ACTIVE",
                                "creationalSpecification": {
                                    "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                                    "createdBy": _preferences.getString("username"),
                                    "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                                    "modifiedBy": _preferences.getString("username"),
                                },
                            }
                        ],
                        "customerAdditionalInformation": null,
                        "customerProcessingCompletion": {
                            "lastKnownCustomerState": "CUSTOMER_GUARANTOR",
                            "nextKnownCustomerState": "CUSTOMER_GUARANTOR"
                        },
                        "creationalSpecification": {
                            "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                            "createdBy": _preferences.getString("username"),
                            "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                            "modifiedBy": _preferences.getString("username"),
                        },
                        "status": "ACTIVE",
                        "obligorID": _providerRincian.obligorID, // _preferences.getString("last_known_state") != "IDE" ? _providerRincian.obligorID != "NEW" ? _providerRincian.obligorID : _preferences.getString("oid").toString() : _preferences.getString("oid").toString(),
                        "exposureInformations": null,
                        "customerIncomes": _customerIncomes
                    },
                    "orderDTO": {
                        "orderID": _preferences.getString("last_known_state") != "IDE" ? _providerApp.applNo : orderId,
                        "customerID": _preferences.getString("last_known_state") != "IDE" ? _providerRincian.customerID : "NEW",
                        "orderSpecification": {
                            "flagDakorCA": 1,
                            "aosApprovalStatus": _preferences.getString("last_known_state") == "AOS" ? _providerSurvey.radioValueApproved : "",
                            "isMayor": false, // _preferences.getString("last_known_state") == "AOS" && _providerSurvey.radioValueApproved == "0" ? true : | _preferences.getString("is_mayor") == "0",
                            "hasFiducia": false,
                            "applicationSource": "003",
                            "orderDate": "${_providerApp.controllerOrderDate.text} 00:00:00",//di edit karena tgl orderDate lebih besar dr surveyAppointmentDate <formatDateValidateAndSubmit(_providerApp.initialDateOrder)>
                            "applicationDate": "${_providerApp.controllerOrderDate.text} 00:00:00",// di edit karena tgl applicationDate lebih besar dr surveyAppointmentDate <formatDateValidateAndSubmit(_providerApp.initialDateOrder)>
                            "surveyAppointmentDate": "${_providerApp.controllerSurveyAppointmentDate.text}" " ${_providerApp.controllerSurveyAppointmentTime.text}:00", //formatDateValidateAndSubmit(_providerApp.initialSurveyDate),
                            "orderType": _preferences.getString("status_aoro") != null ? _preferences.getString("status_aoro") : "0",//_preferences.getString("status_aoro"),
                            "isOpenAccount": true,
                            "accountFormNumber": null,
                            "sentraCID": _preferences.getString("SentraD"),
                            "unitCID": _preferences.getString("UnitD"),
                            "initialRecommendation": _providerApp.initialRecomendation,
                            "finalRecommendation": _providerApp.finalRecomendation,
                            "brmsScoring": _providerApp.brmsScoring,
                            "isInPlaceApproval": false,
                            "isPkSigned": _providerApp.isSignedPK,
                            "maxApprovalLevel": null,
                            "jenisKonsep": _providerApp.conceptTypeModelSelected != null ? _providerApp.conceptTypeModelSelected.id : "",
                            "jumlahObjek": _providerApp.controllerTotalObject.text == "" ? null : int.parse(_providerApp.controllerTotalObject.text),
                            "jenisProporsionalAsuransi": _providerApp.proportionalTypeOfInsuranceModelSelected != null ? _providerApp.proportionalTypeOfInsuranceModelSelected.kode : "",
                            "unitKe": _providerApp.numberOfUnitSelected == "" ? null : int.parse(_providerApp.numberOfUnitSelected),
                            "isWillingToAcceptInfo": false,
                            "applicationContractNumber": null,
                            "dealerNote": Provider.of<MarketingNotesChangeNotifier>(context,listen: false).controllerMarketingNotes.text,
                            "userDealer": "",
                            "orderDealer": "",
                            "flagSourceMS2": "103", // diganti dari 006 ke 103 sesuai arahan mas Oji (per tanggal 06.07.2021)
                            "orderSupportingDocuments": _orderSupportingDocuments,
                            "collateralTypeID": _providerKolateral.collateralTypeModel.id,
                            "applicationDocVerStatus": null
                        },
                        "orderProducts": [
                            {
                                "orderProductID": _preferences.getString("last_known_state") != "IDE" ? _providerUnitObject.applObjtID : orderProductId, // jika IDE ngambil dari nomer unit, selain IDE ngambil dari APPL_NO get data from DB API
                                "orderProductSpecification": {
                                    "financingTypeID": _providerUnitObject.typeOfFinancingModelSelected.financingTypeId,//_preferences.getString("cust_type") == "PER" ? _providerFoto.typeOfFinancingModelSelected.financingTypeId : _providerUnitObject.typeOfFinancingModelSelected.financingTypeId,
                                    "ojkBusinessTypeID": _providerUnitObject.businessActivitiesModelSelected.id,//_preferences.getString("cust_type") == "PER" ? _providerFoto.kegiatanUsahaSelected.id.toString() : _providerUnitObject.typeOfFinancingModelSelected.financingTypeId.toString(),
                                    "ojkBussinessDetailID": _providerUnitObject.businessActivitiesTypeModelSelected.id,//_preferences.getString("cust_type") == "PER" ? _providerFoto.jenisKegiatanUsahaSelected.id.toString() : _providerUnitObject.typeOfFinancingModelSelected.financingTypeId,
                                    "orderUnitSpecification": {
                                        "objectGroupID": _providerUnitObject.groupObjectSelected != null ? _providerUnitObject.groupObjectSelected.KODE : "",
                                        "objectID": _providerUnitObject.objectSelected != null ? _providerUnitObject.objectSelected.id : "",
                                        "productTypeID": _providerUnitObject.productTypeSelected != null ? _providerUnitObject.productTypeSelected.id : "",
                                        "objectBrandID": _providerUnitObject.brandObjectSelected != null ? _providerUnitObject.brandObjectSelected.id : "",
                                        "objectTypeID": _providerUnitObject.objectTypeSelected != null ? _providerUnitObject.objectTypeSelected.id : "",
                                        "objectModelID": _providerUnitObject.modelObjectSelected != null ? _providerUnitObject.modelObjectSelected.id : "",
                                        "objectUsageID": _providerUnitObject.objectUsageModel != null ? _providerUnitObject.objectUsageModel.id : "", // field pemakaian objek (unit)
                                        "objectPurposeID": _providerUnitObject.objectPurposeSelected != null ? _providerUnitObject.objectPurposeSelected.id : "", // field tujuan objek (unit)
                                        "modelDetail": _providerUnitObject.controllerDetailModel.text == "" ? null : _providerUnitObject.controllerDetailModel.text
                                    },
                                    "salesGroupID": _providerUnitObject.groupSalesSelected != null ? _providerUnitObject.groupSalesSelected.kode : "",
                                    "orderSourceID": _providerUnitObject.sourceOrderSelected != null ? _providerUnitObject.sourceOrderSelected.kode : "",
                                    "orderSourceName": _providerUnitObject.sourceOrderNameSelected != null ? _providerUnitObject.sourceOrderNameSelected.kode : "",
                                    "orderProductDealerSpecification": {
                                        "isThirdParty": false,
                                        "thirdPartyTypeID": _providerUnitObject.thirdPartyTypeSelected != null ? _providerUnitObject.thirdPartyTypeSelected.kode : "",
                                        "thirdPartyID": _providerUnitObject.thirdPartySelected != null ? _providerUnitObject.thirdPartySelected.kode : "",
                                        "thirdPartyActivity": _providerUnitObject.activitiesModel != null ? _providerUnitObject.activitiesModel.kode : null,
                                        "sentradID": _preferences.getString("SentraD"), //_providerUnitObject.thirdPartySelected != null ? _providerUnitObject.thirdPartySelected.sentraId : "",
                                        "unitdID": _preferences.getString("UnitD"), //_providerUnitObject.thirdPartySelected != null ? _providerUnitObject.thirdPartySelected.unitId : "",
                                        "dealerMatrix": _providerUnitObject.matriksDealerSelected != null ? _providerUnitObject.matriksDealerSelected.kode : ""
                                    },
                                    "programID": _providerUnitObject.programSelected != null ? _providerUnitObject.programSelected.kode : "",
                                    "rehabType": _providerUnitObject.rehabTypeSelected != null ? _providerUnitObject.rehabTypeSelected.id : "",
                                    "referenceNumber": _providerUnitObject.referenceNumberModel != null ? _providerUnitObject.referenceNumberModel.noPK : null,
                                    "applicationUnitContractNumber": null,
                                    "orderProductSaleses": _orderProductSaleses,
                                    "orderKaroseris": _orderKaroseris,
                                    "orderProductInsurances": _orderProductInsurances,
                                    "orderWmps": _orderWmps,
                                    "groupID": _providerUnitObject.grupIdSelected != null ? _providerUnitObject.grupIdSelected.kode : null
                                },
                                "orderCreditStructure": {
                                    "installmentTypeID": _providerCreditStructure.installmentTypeSelected != null ? _providerCreditStructure.installmentTypeSelected.id : "", // sebelumnya ngambil by kode
                                    "tenor": _providerCreditStructure.periodOfTimeSelected != null ? int.parse(_providerCreditStructure.periodOfTimeSelected) : "",
                                    "paymentMethodID": _providerCreditStructure.paymentMethodSelected != null ? _providerCreditStructure.paymentMethodSelected.id : "", // sebelumnya ngambil by kode
                                    "effectiveRate": _providerCreditStructure.controllerInterestRateEffective.text != "" ? double.parse(_providerCreditStructure.controllerInterestRateEffective.text) : "",
                                    "flatRate": _providerCreditStructure.controllerInterestRateFlat.text != "" ? double.parse(_providerCreditStructure.controllerInterestRateFlat.text) : "",//0.48501
                                    "objectPrice": _providerCreditStructure.controllerObjectPrice.text != "" ? double.parse(_providerCreditStructure.controllerObjectPrice.text.replaceAll(",", "")) : 0,
                                    "karoseriTotalPrice": _providerCreditStructure.controllerKaroseriTotalPrice.text != "" ? double.parse(_providerCreditStructure.controllerKaroseriTotalPrice.text.replaceAll(",", "")) : 0,
                                    "totalPrice": _providerCreditStructure.controllerTotalPrice.text != "" ? double.parse(_providerCreditStructure.controllerTotalPrice.text.replaceAll(",", "")) : 0,
                                    "nettDownPayment": _providerCreditStructure.controllerNetDP.text != "" ? double.parse(_providerCreditStructure.controllerNetDP.text.replaceAll(",", "")) : 0,
                                    "declineNInstallment": _providerCreditStructure.installmentDeclineN,
                                    "paymentOfYear": _providerCreditStructure.installmentTypeSelected != null ? _providerCreditStructure.installmentTypeSelected.id == "04" ? int.parse(_providerCreditStructure.controllerPaymentPerYear.text) : 1 : 1,
                                    "branchDownPayment": _providerCreditStructure.controllerBranchDP.text != "" ? double.parse(_providerCreditStructure.controllerBranchDP.text.replaceAll(",", "")) : 0,
                                    "grossDownPayment": _providerCreditStructure.controllerGrossDP.text != "" ? double.parse(_providerCreditStructure.controllerGrossDP.text.replaceAll(",", "")) : 0,
                                    "totalLoan": _providerCreditStructure.controllerTotalLoan.text != "" ? double.parse(_providerCreditStructure.controllerTotalLoan.text.replaceAll(",", "")) : 0,
                                    "installmentAmount": _providerCreditStructure.controllerInstallment.text != "" ? double.parse(_providerCreditStructure.controllerInstallment.text.replaceAll(",", "")) : 0,
                                    "ltvRatio": _providerCreditStructure.controllerLTV.text != "" ? double.parse(_providerCreditStructure.controllerLTV.text.replaceAll(",", "")) : 0.0,
                                    "pencairan": 0,
                                    "interestAmount": _providerCreditStructure.interestAmount,
                                    "gpType": null,
                                    "newTenor": 0,
                                    "totalStepping": 0,
                                    "installmentBalloonPayment": {
                                        "balloonType": _providerCreditStructureType.radioValueBalloonPHOrBalloonInstallment,
                                        "lastInstallmentPercentage": _providerCreditStructureType.controllerInstallmentPercentage.text != "" ? double.parse(_providerCreditStructureType.controllerInstallmentPercentage.text.replaceAll(",", "")) : 0,
                                        "lastInstallmentValue": _providerCreditStructureType.controllerInstallmentValue.text != "" ? double.parse(_providerCreditStructureType.controllerInstallmentValue.text.replaceAll(",", "")) : 0
                                    },
                                    "installmentSchedule": {
                                        "installmentType": _providerCreditStructure.installmentTypeSelected != null ? _providerCreditStructure.installmentTypeSelected.name : null, // OLD: ambil dari ID
                                        "paymentMethodType": _providerCreditStructure.paymentMethodSelected != null ? _providerCreditStructure.paymentMethodSelected.name : null,  // OLD: ambil dari ID
                                        "installment": _providerCreditStructure.controllerInstallment.text != "" ? double.parse(_providerCreditStructure.controllerInstallment.text.replaceAll(",", "")) : 0,
                                        "installmentRound": 0,
                                        "installmentMethod": 0,
                                        "lastKnownOutstanding": 0,
                                        "minInterest": 0,
                                        "maxInterest": 0,
                                        "futureValue": 0,
                                        "isInstallment": false,
                                        "roundingValue": null,
                                        "balloonInstallment": 0,
                                        "gracePeriode": 0,
                                        "gracePeriodes": [],
                                        "seasonal": 0,
                                        "steppings": [],
                                        "installmentScheduleDetails": []
                                    },
                                    "dsr": _providerCreditIncome.controllerDebtComparison.text != "" ? double.parse(_providerCreditIncome.controllerDebtComparison.text.replaceAll(",", "")) : 0,
                                    "dir": _providerCreditIncome.controllerIncomeComparison.text != "" ? double.parse(_providerCreditIncome.controllerIncomeComparison.text.replaceAll(",", "")) : 0,
                                    "dsc": _providerCreditIncome.controllerDSC.text != "" ? double.parse(_providerCreditIncome.controllerDSC.text.replaceAll(",", "")) : 0.0,
                                    "irr": _providerCreditIncome.controllerIRR.text != "" ? double.parse(_providerCreditIncome.controllerIRR.text.replaceAll(",", "")) : 0.0,
                                    "installmentDetails": _installmentDetails,
                                    "uangMukaKaroseri": 0, // diset 0
                                    "uangMukaChasisNet": _providerCreditStructure.controllerNetDP.text != "" ? double.parse(_providerCreditStructure.controllerNetDP.text.replaceAll(",", "")) : 0, // set sama kaya nettDownPayment
                                    "uangMukaChasisGross": _providerCreditStructure.controllerGrossDP.text != "" ? double.parse(_providerCreditStructure.controllerGrossDP.text.replaceAll(",", "")) : 0, // set sama kaya grossDownPayment
                                    // [
                                    //   {
                                    //     "installmentID": null,
                                    //     "installmentNumber": 0,
                                    //     "percentage": 0,
                                    //     "amount": 0,
                                    //     "creationalSpecification": {
                                    //       "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                                    //       "createdBy": "10056030",
                                    //       "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                                    //       "modifiedBy": "10056030"
                                    //     },
                                    //     "status": "ACTIVE"
                                    //   }
                                    // ],
                                    "realDSR": 0 // di jadikan model dari get dsr
                                },
                                "orderFees": _orderFees,
                                "orderSubsidies": _orderSubsidies,
                                "orderProductAdditionalSpecification": {
                                    "virtualAccountID": null,
                                    "virtualAccountValue": 0,
                                    "cashingPurposeID": null,
                                    "cashingTypeID": null
                                },
                                "creationalSpecification": {
                                    "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                                    "createdBy": _preferences.getString("username"),
                                    "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                                    "modifiedBy": _preferences.getString("username"),
                                },
                                "status": "ACTIVE",
                                "collateralAutomotives": _providerKolateral.collateralTypeModel.id == "001"
                                    ?
                                [
                                    {
                                        "collateralID": _preferences.getString("last_known_state") != "IDE" ? _providerKolateral.collateralID : "CAU001", // NEW | _providerKolateral.collateralTypeModel != null ? _providerKolateral.collateralTypeModel.id : "",
                                        "isCollaSameUnit": _providerKolateral.radioValueIsCollateralSameWithUnitOto, // Jika Collateral = Unit dipilih YA, maka kirim TRUE. Begitu juga sebaliknya (0 = YA/true | 1 = TIDAK/false)
                                        "isMultiUnitCollateral": _providerKolateral.radioValueForAllUnitOto, // ya = false = 0 | tidak = true = 1
                                        "isCollaNameSameWithCustomerName": _providerKolateral.radioValueIsCollaNameSameWithApplicantOto, // Jika Nama Jaminan = Pemohon dipilih YA, maka kirim TRUE. Begitu juga sebaliknya (0 = YA/true | 1 = TIDAK/false)
                                        "identitySpecification": {
                                            "identityType": _providerKolateral.identityTypeSelectedAuto != null ? _providerKolateral.identityTypeSelectedAuto.id : "",
                                            "identityNumber": _providerKolateral.controllerIdentityNumberAuto.text != "" ? _providerKolateral.controllerIdentityNumberAuto.text : "",
                                            "identityName": _providerKolateral.controllerNameOnCollateralAuto.text, // _providerKolateral.radioValueIsCollaNameSameWithApplicantOto == 0 ? _providerInfoNasabah.controllerNamaLengkapSesuaiIdentitas.text : _providerKolateral.controllerNameOnCollateralAuto.text,
                                            "fullName": _providerKolateral.controllerNameOnCollateralAuto.text, // _providerKolateral.radioValueIsCollaNameSameWithApplicantOto == 0 ? _providerInfoNasabah.controllerNamaLengkap.text : _providerKolateral.controllerNameOnCollateralAuto.text,
                                            "alias": null,
                                            "title": null,
                                            "dateOfBirth": _providerKolateral.controllerBirthDateAuto.text != "" ? _providerKolateral.controllerBirthDateAuto.text+" 00:00:00" : null,
                                            "placeOfBirth": _providerKolateral.controllerBirthPlaceValidWithIdentityAuto.text != "" ? _providerKolateral.controllerBirthPlaceValidWithIdentityAuto.text : "",
                                            "placeOfBirthKabKota": null, //_providerKolateral.radioValueIsCollaNameSameWithApplicantOto == 0 ? _providerInfoNasabah.birthPlaceSelected.KABKOT_ID : _providerKolateral.birthPlaceAutoSelected != null ? _providerKolateral.birthPlaceAutoSelected.KABKOT_ID : null,
                                            "gender": null, // _providerKolateral.radioValueIsCollaNameSameWithApplicantOto == 0 ? _providerInfoNasabah.radioValueGender : null
                                            "identityActiveStart": null,
                                            "identityActiveEnd": null,
                                            "religion": null , // _providerKolateral.radioValueIsCollaNameSameWithApplicantOto == 0 ? _providerInfoNasabah.religionSelected == null ? null : _providerInfoNasabah.religionSelected.id : null,
                                            "occupationID": null,//_providerFoto.occupationSelected != null ? _providerFoto.occupationSelected.KODE : "",
                                            "positionID": null
                                        },
                                        "collateralAutomotiveSpecification": {
                                            "orderUnitSpecification": {
                                                "objectGroupID": _providerKolateral.groupObjectSelected != null ? _providerKolateral.groupObjectSelected.KODE : null,
                                                "objectID": _providerKolateral.objectSelected != null ? _providerKolateral.objectSelected.id : null,
                                                "productTypeID": _providerKolateral.productTypeSelected != null ? _providerKolateral.productTypeSelected.id : null,
                                                "objectBrandID":  _providerKolateral.brandObjectSelected != null ? _providerKolateral.brandObjectSelected.id : null,
                                                "objectTypeID": _providerKolateral.objectTypeSelected != null ? _providerKolateral.objectTypeSelected.id : null,
                                                "objectModelID": _providerKolateral.modelObjectSelected != null ? _providerKolateral.modelObjectSelected.id : null,
                                                "objectUsageID":  "", // tidak dipakai
                                                "objectPurposeID": _providerKolateral.objectUsageSelected != null ? _providerKolateral.objectUsageSelected.id : null, // field tujuan penggunaan collateral
                                            },
                                            "mpAdiraUpld": _providerKolateral.controllerMPAdiraUpload.text != "" ? double.parse(_providerKolateral.controllerMPAdiraUpload.text.replaceAll(",", "")) : null,
                                            "productionYear": _providerKolateral.yearProductionSelected != null ? int.parse(_providerKolateral.yearProductionSelected) : "",
                                            "registrationYear": _providerKolateral.yearRegistrationSelected != null ? int.parse(_providerKolateral.yearRegistrationSelected) : "",
                                            "isYellowPlate": _providerKolateral.radioValueYellowPlat, // per tanggal 10.05.2021 = dirubah ga dirubah jadi true false (OLD: == 0 ? false : true)
                                            "isBuiltUp": _providerKolateral.radioValueBuiltUpNonATPM == 0 ? true : false,
                                            "utjSpecification": {
                                                "bpkbNumber": _providerKolateral.controllerBPKPNumber.text != "" ? _providerKolateral.controllerBPKPNumber.text : null,
                                                "chassisNumber":  _providerKolateral.controllerFrameNumber.text != "" ? _providerKolateral.controllerFrameNumber.text : null,
                                                "engineNumber": _providerKolateral.controllerMachineNumber.text != "" ? _providerKolateral.controllerMachineNumber.text : null,
                                                "policeNumber": _providerKolateral.controllerPoliceNumber.text != "" ? _providerKolateral.controllerPoliceNumber.text : null,
                                            },
                                            "gradeUnit": _providerKolateral.controllerGradeUnit.text != "" ? _providerKolateral.controllerGradeUnit.text : null,
                                            "utjFacility": _providerKolateral.controllerFasilitasUTJ.text != "" ? _providerKolateral.controllerFasilitasUTJ.text == "TUNAI" ? "1" : "0" : null,
                                            "bidderName": _providerKolateral.controllerNamaBidder.text != "" ? _providerKolateral.controllerNamaBidder.text : null,
                                            "showroomPrice": _providerKolateral.controllerHargaJualShowroom.text != "" ? double.parse(_providerKolateral.controllerHargaJualShowroom.text.replaceAll(",", "")).round() : "",
                                            "additionalEquipment": _providerKolateral.controllerPerlengkapanTambahan.text != "" ? _providerKolateral.controllerPerlengkapanTambahan.text : "",
                                            "mpAdira":  _providerKolateral.controllerMPAdira.text != "" ? double.parse(_providerKolateral.controllerMPAdira.text.replaceAll(",", "")) : null,
                                            "physicalRecondition": _providerKolateral.controllerRekondisiFisik.text != "" ? double.parse(_providerKolateral.controllerRekondisiFisik.text.replaceAll(",", "")) : 0,
                                            "isProper": _providerKolateral.radioValueWorthyOrUnworthy, // 0 = layak = false | 1 = tidak layak = true
                                            "ltvRatio": 0.0,
                                            "appraisalSpecification": {
                                                "collateralDp": double.parse(_providerKolateral.controllerDPJaminan.text == '' ? '0' : _providerKolateral.controllerDPJaminan.text.replaceAll(",", "")),
                                                "maximumPh": double.parse(_providerKolateral.controllerPHMaxAutomotive.text == '' ? '0' : _providerKolateral.controllerPHMaxAutomotive.text.replaceAll(",", "")),
                                                "appraisalPrice": double.parse(_providerKolateral.controllerTaksasiPriceAutomotive.text == '' ? '0' : _providerKolateral.controllerTaksasiPriceAutomotive.text.replaceAll(",", "")),
                                                "collateralUtilizationID": _providerKolateral.collateralUsageOtoSelected != null ? _providerKolateral.collateralUsageOtoSelected.id : "" // field tujuan penggunaan collateral
                                            },
                                            "collateralAutomotiveAdditionalSpecification": {
                                                "capacity": 0,
                                                "color": null,
                                                "manufacturer": null,
                                                "serialNumber": null,
                                                "invoiceNumber": null,
                                                "stnkActiveDate": null,
                                                "bpkbAddress": null,
                                                "bpkbIdentityTypeID": null
                                            },
                                        },
                                        "status": "ACTIVE",
                                        "creationalSpecification": {
                                            "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                                            "createdBy": _preferences.getString("username"),
                                            "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                                            "modifiedBy": _preferences.getString("username"),
                                        }
                                    }
                                ] : [],
                                "collateralProperties": _providerKolateral.collateralTypeModel.id == "002"
                                    ?
                                [
                                    {
                                        "isMultiUnitCollateral": true, // _providerKolateral.radioValueForAllUnitOto, // ya = false = 0 | tidak = true = 1
                                        "isCollaNameSameWithCustomerName": _providerKolateral.radioValueIsCollateralSameWithApplicantProperty == 0,
                                        "collateralID": _preferences.getString("last_known_state") != "IDE" ? _providerKolateral.collateralID : "COLP001", // NEW
                                        "identitySpecification": {
                                            "identityType": _providerKolateral.identityModel != null ? _providerKolateral.identityModel.id : "",
                                            "identityNumber": _providerKolateral.controllerIdentityNumber.text,
                                            "identityName": null,
                                            "fullName": _providerKolateral.controllerNameOnCollateral.text,
                                            "alias": null,
                                            "title": null,
                                            "dateOfBirth": _providerKolateral.controllerBirthDateProp.text != "" ? _providerKolateral.controllerBirthDateProp.text : null,
                                            "placeOfBirth": _providerKolateral.birthPlaceSelected.KABKOT_NAME,
                                            "placeOfBirthKabKota": _providerKolateral.birthPlaceSelected.KABKOT_ID,
                                            "gender": null,
                                            "identityActiveStart": null,
                                            "identityActiveEnd": null,
                                            "isLifetime": null,
                                            "religion": null,
                                            "occupationID": null,
                                            "positionID": null,
                                            "maritalStatusID": null
                                        },
                                        "detailAddresses": [
                                            {
                                                "addressID": _preferences.getString("last_known_state") != "IDE" ? _providerKolateral.addressID != "NEW" ? _providerKolateral.addressID : "NEW" : "NEW",
                                                "foreignBusinessID": _preferences.getString("last_known_state") != "IDE" ? _providerKolateral.foreignBusinessID != "NEW" ? _providerKolateral.foreignBusinessID : "CAU001" : "CAU001", // NEW
                                                "addressSpecification": {
                                                    "koresponden": _providerKolateral.isKorespondensi.toString(), // tanya oji
                                                    "matrixAddr": _preferences.getString("cust_type") == "PER" ? "6" : "13",
                                                    "address": _providerKolateral.controllerAddress.text,
                                                    "rt": _providerKolateral.controllerRT.text,
                                                    "rw": _providerKolateral.controllerRW.text,
                                                    "provinsiID": _providerKolateral.kelurahanSelected.PROV_ID,
                                                    "kabkotID": _providerKolateral.kelurahanSelected.KABKOT_ID,
                                                    "kecamatanID": _providerKolateral.kelurahanSelected.KEC_ID,
                                                    "kelurahanID": _providerKolateral.kelurahanSelected.KEL_ID,
                                                    "zipcode": _providerKolateral.kelurahanSelected.ZIPCODE
                                                },
                                                "contactSpecification": {
                                                    "telephoneArea1": null,
                                                    "telephone1": null,
                                                    "telephoneArea2": null,
                                                    "telephone2": null,
                                                    "faxArea": null,
                                                    "fax": null,
                                                    "handphone": null,
                                                    "email": null,
                                                    "noWa": null,
                                                    "noWa2": null,
                                                    "noWa3": null
                                                },
                                                "addressType": _providerKolateral.addressTypeSelected.KODE,
                                                "creationalSpecification": {
                                                    "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                                                    "createdBy": _preferences.getString("username"),
                                                    "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                                                    "modifiedBy": _preferences.getString("username"),
                                                },
                                                "status": "ACTIVE"
                                            }
                                        ],
                                        "collateralPropertySpecification": {
                                            "certificateNumber": _providerKolateral.controllerCertificateNumber.text,
                                            "certificateTypeID": _providerKolateral.certificateTypeSelected.id,
                                            "propertyTypeID": _providerKolateral.propertyTypeSelected.id,
                                            "buildingArea": double.parse(_providerKolateral.controllerBuildingArea.text.replaceAll(",", "")),
                                            "landArea": double.parse(_providerKolateral.controllerSurfaceArea.text.replaceAll(",", "")),
                                            "appraisalSpecification": {
                                                "collateralDp": double.parse(_providerKolateral.controllerDPJaminan.text.replaceAll(",", "")).toInt(),
                                                "maximumPh": double.parse(_providerKolateral.controllerPHMax.text == "" ? '0' : _providerKolateral.controllerPHMax.text.replaceAll(",", "")),
                                                "appraisalPrice": double.parse(_providerKolateral.controllerTaksasiPrice.text.replaceAll(",", "")).toInt(),
                                                "collateralUtilizationID": _providerKolateral.collateralUsagePropertySelected != null ? _providerKolateral.collateralUsagePropertySelected.id : "" //"" field tujuan penggunaan collateral
                                            },
                                            "positiveFasumDistance": double.parse(_providerKolateral.controllerJarakFasumPositif.text.replaceAll(",","")).toInt(),
                                            "negativeFasumDistance": double.parse(_providerKolateral.controllerJarakFasumNegatif.text.replaceAll(",","")).toInt(),
                                            "landPrice": double.parse(_providerKolateral.controllerHargaTanah.text.replaceAll(",","")).toInt(),
                                            "njopPrice": double.parse(_providerKolateral.controllerHargaNJOP.text.replaceAll(",","")).toInt(),
                                            "buildingPrice": double.parse(_providerKolateral.controllerHargaBangunan.text.replaceAll(",","")).toInt(),
                                            "roofTypeID": _providerKolateral.typeOfRoofSelected != null ? _providerKolateral.typeOfRoofSelected.id : null,
                                            "wallTypeID": _providerKolateral.wallTypeSelected != null ? _providerKolateral.wallTypeSelected.id : null,
                                            "floorTypeID": _providerKolateral.floorTypeSelected != null ? _providerKolateral.floorTypeSelected.id : null,
                                            "foundationTypeID": _providerKolateral.foundationTypeSelected != null ? _providerKolateral.foundationTypeSelected.id : null,
                                            "roadTypeID": _providerKolateral.streetTypeSelected != null ? _providerKolateral.streetTypeSelected.id : null,
                                            "isCarPassable": _providerKolateral.radioValueAccessCar == 0,
                                            "totalOfHouseInRadius": _providerKolateral.controllerJumlahRumahDalamRadius.text != "" ? int.parse(_providerKolateral.controllerJumlahRumahDalamRadius.text) : 0,
                                            "sifatJaminan": _providerKolateral.controllerSifatJaminan.text,
                                            "buktiKepemilikanTanah": _providerKolateral.controllerBuktiKepemilikan.text,
                                            "tgglTerbitSertifikat": _providerKolateral.controllerCertificateReleaseDate.text != "" ? formatDateValidateAndSubmit(_providerKolateral.initialDateCertificateRelease) : "",
                                            "tahunTerbitSertifikat": _providerKolateral.controllerCertificateReleaseYear.text,
                                            "namaPemegangHak": _providerKolateral.controllerNamaPemegangHak.text,
                                            "noSuratUkurGmbrSituasi": _providerKolateral.controllerNoSuratUkur.text,
                                            "tgglSuratUkurGmbrSituasi": _providerKolateral.controllerDateOfMeasuringLetter.text != "" ? formatDateValidateAndSubmit(_providerKolateral.initialDateOfMeasuringLetter) : "",
                                            "sertifikatSuratUkurDikeluarkanOleh": _providerKolateral.controllerCertificateOfMeasuringLetter.text,
                                            "masaBerlakuHak": _providerKolateral.controllerMasaHakBerlaku.text,
                                            "noImb": _providerKolateral.controllerNoIMB.text,
                                            "tgglImb": _providerKolateral.controllerDateOfIMB.text != "" ? formatDateValidateAndSubmit(_providerKolateral.initialDateOfIMB) : "",
                                            "luasBangunanImb": double.parse(_providerKolateral.controllerLuasBangunanIMB.text.replaceAll(",", "")),
                                            "ltvRatio": _providerKolateral.controllerLTV.text != "" ? int.parse(_providerKolateral.controllerLTV.text) : 0,
                                            "letakTanah": null,
                                            "propertyGroupObjtID": null,
                                            "propertyObjtID": null
                                        },
                                        "status": "INACTIVE",
                                        "creationalSpecification": {
                                            "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                                            "createdBy": _preferences.getString("username"),
                                            "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                                            "modifiedBy": _preferences.getString("username"),
                                        }
                                    }
                                ]
                                    :
                                [],
                                "orderProductProcessingCompletion": {
                                    "lastKnownOrderProductState": "APPLICATION_DOCUMENT",
                                    "nextKnownOrderProductState": "APPLICATION_DOCUMENT"
                                },
                                "isWithoutColla": _providerKolateral.collateralTypeModel.id == "003" ? 1 : 0,// _providerUnitObject.groupObjectSelected.KODE == "003" ? 1 : 0,
                                "isSaveKaroseri": false
                            }
                        ],
                        "orderProcessingCompletion": {
                            "calculatedAt": 0,
                            "lastKnownOrderState": null,
                            "lastKnownOrderStatePosition": null,
                            "lastKnownOrderStatus": null,
                            "lastKnownOrderHandledBy": null
                        },
                        "creationalSpecification": {
                            "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                            "createdBy": _preferences.getString("username"),
                            "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                            "modifiedBy": _preferences.getString("username"),
                        },
                        "status": "ACTIVE"
                    },
                    "surveyDTO": {
                        "surveyID": _dataResultSurvey[0]['surveyID'], // _providerSurvey.surveyID,
                        "orderID": _providerApp.applNo,
                        "surveyType": "002",
                        "employeeID": _preferences.getString("username"),
                        "phoneNumber": null,
                        "janjiSurveyDate": formatDateValidateAndSubmit(_providerApp.initialSurveyDate),
                        "reason": "",
                        "flagBRMS": "BRMS",
                        "surveyStatus": "01",
                        "status": "ACTIVE",
                        "surveyProcess": "02",
                        "surveyResultSpecification": {
                            "surveyResultType": _providerSurvey.resultSurveySelected != null ? _providerSurvey.resultSurveySelected.KODE : "",
                            "recomendationType": _providerSurvey.recommendationSurveySelected != null ? _providerSurvey.recommendationSurveySelected.KODE : null,
                            "notes": _providerSurvey.controllerNote.text != "" ? _providerSurvey.controllerNote.text : "",
                            "surveyResultDate": _providerSurvey.controllerResultSurveyDate.text.isNotEmpty ? "${_providerSurvey.controllerResultSurveyDate.text} ${_providerSurvey.controllerResultSurveyTime.text}:00" : formatDateValidateAndSubmit(DateTime.now()), //formatDateValidateAndSubmit(_providerSurvey.initialDateResultSurvey),
                            "distanceWithSentra": _providerSurvey.controllerDistanceLocationWithSentraUnit.text != "" ? double.parse(_providerSurvey.controllerDistanceLocationWithSentraUnit.text.replaceAll(",", "")).toInt() : 0,
                            "distanceWithDealer": _providerSurvey.controllerDistanceLocationWithDealerMerchantAXI.text != "" ? double.parse(_providerSurvey.controllerDistanceLocationWithDealerMerchantAXI.text.replaceAll(",", "")).toInt() : 0,
                            "distanceObjWithSentra": _providerSurvey.controllerDistanceLocationWithUsageObjectWithSentraUnit.text != "" ? double.parse(_providerSurvey.controllerDistanceLocationWithUsageObjectWithSentraUnit.text.replaceAll(",", "")).toInt() : 0,
                            "surveyResultAssets": _surveyResultAssets,
                            "surveyResultDetails": _surveyResultDetails,
                            "surveyResultTelesurveys": []
                        },
                        "creationalSpecification": {
                            "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                            "createdBy": _preferences.getString("username"),
                            "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                            "modifiedBy": _preferences.getString("username")
                        },
                        "jobSurveyor": ""
                    },
                    "userCredentialMS2": {
                        "nik": _preferences.getString("username"),
                        "branchCode": _preferences.getString("branchid"),
                        "fullName": _preferences.getString("fullname"),
                        "role": _preferences.getString("job_name")
                    }
                },
                "applicationDTOReason": {
                    "customerDTOReason": {
                        "customerIDReason": null,
                        "customerObligorIDReason": null,
                        "customerTypeReason": null,
                        "customerSpecificationFormNumberReason": null,
                        "customerNpwpSpecificationHasNpwpReason": null,
                        "customerNpwpSpecificationNpwpAddressReason": _providerRincian.isAddressNPWPDakor ? "99" : null,
                        "customerNpwpSpecificationNpwpFullnameReason": _providerRincian.isNameNPWPDakor ? "99" : null,
                        "customerNpwpSpecificationNpwpNumberReason": _providerRincian.isNoNPWPDakor ? "99" : null,
                        "customerNpwpSpecificationNpwpTypeReason": _providerRincian.isTypeNPWPDakor ? "99" : null,
                        "customerNpwpSpecificationPkpIdentifierReason": _providerRincian.isPKPSignDakor ? "99" : null,
                        "customerDetailAddressesReason": [
                            {
                                "addressIDReason": null,
                                "addressForeignBusinessIDReason": null,
                                "addressSpecificationKorespondenReason": null,
                                "addressSpecificationMatrixAddrReason": null,
                                "addressSpecificationAddressReason": null,
                                "addressSpecificationRtReason": null,
                                "addressSpecificationRwReason": null,
                                "addressSpecificationProvinsiIDReason": null,
                                "addressSpecificationKabkotIDReason": null,
                                "addressSpecificationKecamatanIDReason": null,
                                "addressSpecificationKelurahanIDReason": null,
                                "addressSpecificationZipcodeReason": null,
                                "addressContactSpecificationTelephoneArea1Reason": null,
                                "addressContactSpecificationTelephone1Reason": null,
                                "addressContactSpecificationTelephoneArea2Reason": null,
                                "addressContactSpecificationTelephone2Reason": null,
                                "addressContactSpecificationFaxAreaReason": null,
                                "addressContactSpecificationFaxReason": null,
                                "addressContactSpecificationHandphoneReason": null,
                                "addressContactSpecificationEmailReason": null,
                                "addressTypeReason": null,
                                "addressCreationalSpecificationCreatedAtReason": null,
                                "addressCreationalSpecificationCreatedByReason": null,
                                "addressCreationalSpecificationModifiedAtReason": null,
                                "addressCreationalSpecificationModifiedByReason": null,
                                "addressStatusReason": null,
                                "addressFlagCorrectionReason": null
                            },
                        ],
                        "customerGuarantorSpecificationIsGuarantorReason": null,
                        "customerGuarantorCorporatesReason": _customerGuarantorCorporatesReason,
                        "customerGuarantorIndividualsReason": _customerGuarantorIndividualsReason,
                        "customerIndividualReason": [],
                        // [
                        //     {
                        //         "customerIndividualIDReason": null,
                        //         "customerIndividualNumberOfDependentsReason": _providerInfoNasabah.isJumlahTanggunganChanges ? "99" : null,
                        //         "customerIndividualGroupCustomerReason": _providerInfoNasabah.isGCChanges ? "99" : null,
                        //         "customerIndividualStatusRelationshipReason": _providerInfoNasabah.isMaritalStatusSelectedChanges ? "99" : null,
                        //         "customerIndividualEducationIDReason": _providerInfoNasabah.isEducationSelectedChanges ? "99" : null,
                        //         "customerIndividualIdentityIdentityTypeReason": _providerInfoNasabah.isIdentitasModelChanges ? "99" : null,
                        //         "customerIndividualIdentityIdentityNumberReason": _providerInfoNasabah.isNoIdentitasChanges ? "99" : null,
                        //         "customerIndividualIdentityIdentityNameReason": _providerInfoNasabah.isNamaLengkapSesuaiIdentitasChanges ? "99" : null,
                        //         "customerIndividualIdentityFullNameReason": _providerInfoNasabah.isNamaLengkapChanges ? "99" : null,
                        //         "customerIndividualIdentityAliasReason": null,
                        //         "customerIndividualIdentityTitleReason": null,
                        //         "customerIndividualIdentityDateOfBirthReason": _providerInfoNasabah.isTglLahirChanges ? "99" : null,
                        //         "customerIndividualIdentityPlaceOfBirthReason": _providerInfoNasabah.isTempatLahirSesuaiIdentitasChanges ? "99" : null,
                        //         "customerIndividualIdentityPlaceOfBirthKabKotaReason": _providerInfoNasabah.isTempatLahirSesuaiIdentitasLOVChanges ? "99" : null,
                        //         "customerIndividualIdentityGenderReason": _providerInfoNasabah.isGenderChanges ? "99" : null,
                        //         "customerIndividualIdentityIdentityActiveStartReason": null,
                        //         "customerIndividualIdentityIdentityActiveEndReason": null,
                        //         "customerIndividualIdentityReligionReason": null,
                        //         "customerIndividualIdentityOccupationIDReason": null,
                        //         "customerIndividualIdentityPositionIDReason": null,
                        //         "customerIndividualIdentityisLifetimeReason": null,
                        //         "customerIndividualContactTelephoneArea1Reason": null,
                        //         "customerIndividualContactTelephone1Reason": null,
                        //         "customerIndividualContactTelephoneArea2Reason": null,
                        //         "customerIndividualContactTelephone2Reason": null,
                        //         "customerIndividualContactFaxAreaReason": null,
                        //         "customerIndividualContactFaxReason": null,
                        //         "customerIndividualContactHandphoneReason": _providerInfoNasabah.isNoHp1WAChanges ? "99" : null,
                        //         "customerIndividualContactEmailReason": null,
                        //         "noWaReason": null,
                        //         "noWa2Reason": null,
                        //         "noWa3Reason": null,
                        //         "customerOccupationsReason": [
                        //             // {
                        //             //     "customerIndividualCustomerOccupationIDReason": null,
                        //             //     "customerIndividualOccupationIDReason": null,
                        //             //     "customerIndividualOccupationBusinessNameReason": _providerFoto.occupationSelected.KODE == "05" || _providerFoto.occupationSelected.KODE == "07" ? _providerOccupation.editNamaUsahaWiraswasta :_providerOccupation.editNamaPerusahaanLainnya,
                        //             //     "customerIndividualOccupationBusinessTypeReason": null,//_customerIndividualOccupationBusinessTypeReason,
                        //             //     "customerIndividualOccupationProfessionTypeReason": _providerOccupation.editJenisProfesiProfesional ? "99" : null,
                        //             //     "customerIndividualOccupationPepTypeReason": _providerOccupation.editJenisPepHighRiskLainnya ? "99" : null,
                        //             //     "customerIndividualOccupationEmployeeStatusReason": _providerOccupation.editTotalPegawaiLainnya ? "99" : null,
                        //             //     "customerIndividualOccupationBusinessEconomySectorReason": null,//_customerIndividualOccupationBusinessEconomySectorReason,
                        //             //     "customerIndividualOccupationBusinessBusinessFieldReason": _customerIndividualOccupationBusinessBusinessFieldReason,
                        //             //     "customerIndividualOccupationBusinessLocationStatusReason": _customerIndividualOccupationBusinessLocationStatusReason,
                        //             //     "customerIndividualOccupationBusinessBusinessLocationReason": _customerIndividualOccupationBusinessBusinessLocationReason,
                        //             //     "customerIndividualOccupationBusinessEmployeeTotalReason": _customerIndividualOccupationBusinessEmployeeTotalReason,
                        //             //     "customerIndividualOccupationBusinessBussinessLengthInMonthReason": _providerOccupation.editLamaBekerjaBerjalan ? "99" : null,
                        //             //     "customerIndividualOccupationBusinessTotalBussinessLengthInMonthReason": _customerIndividualOccupationBusinessTotalBussinessLengthInMonthReason,
                        //             //     "customerIndividualOccupationCreationalCreatedAtReason": null,
                        //             //     "customerIndividualOccupationCreationalCreatedByReason": null,
                        //             //     "customerIndividualOccupationCreationalModifiedAtReason": null,
                        //             //     "customerIndividualOccupationCreationalModifiedByReason": null,
                        //             //     "customerIndividualOccupationStatusReason": null,
                        //             //     "customerOccupationFlagCorrectionReason": null
                        //             // }
                        //         ],
                        //         "customerIndividualNearFutureNeedIDsReason": [],
                        //         "custIndividualFacebookAccountReason": null,
                        //         "custIndividualBlackBerryPinReason": null,
                        //         "customerIndividualHobbyIDsReason": [],
                        //         "customerIndividualParticipantIDsReason": [],
                        //         "custIndividualFamilyCardNumberReason": null,
                        //         "custIndividualFavoriteColorReason": null,
                        //         "custIndividualFavoriteBrandReason": null,
                        //         "custIndividualEmergencyRelationshipTypeIDReason": null,
                        //         "custIndividualEmergencyIdentityFullNameReason": null,
                        //         "custIndividualEmergencyFullNameReason": null,
                        //         "custIndividualEmergencyTitleReason": null,
                        //         "custIndividualEmergencyEmailReason": null,
                        //         "custIndividualEmergencyHandphoneNumberReason": null,
                        //         "customerIndividualCustomerCreditCardReason": [],
                        //         "custIndividualCustomerFamiliesReason": null,//_custIndividualCustomerFamiliesReason,
                        //         "customerIndividualStatusReason": null,
                        //         "customerIndividualCreatedAtReason": null,
                        //         "customerIndividualCreatedByReason": null,
                        //         "customerIndividualModifiedAtReason": null,
                        //         "customerIndividualModifiedByReason": null
                        //     }
                        // ],
                        "customerCorporateReason": [
                            {
                                "customerCorporateIDReason": null,
                                "customerCorporateEconomySectorReason": _providerRincian.isSectorEconomicDakor ? "99" : null,
                                "customerCorporateBusinessFieldReason": _providerRincian.isBusinessFieldDakor ? "99" : null,
                                "customerCorporateLocationStatusReason": _providerRincian.isLocationStatusDakor ? "99" : null,
                                "customerCorporateBusinessLocationReason": _providerRincian.isBusinessLocationDakor ? "99" : null,
                                "customerCorporateEmployeeTotalReason": _providerRincian.isTotalEmpDakor ? "99" : null,
                                "customerCorporateBussinessLengthInMonthReason": _providerRincian.isLamaUsahaDakor ? "99" : null,
                                "customerCorporateTotalBussinessLengthInMonthReason": _providerRincian.isTotalLamaUsahaDakor ? "99" : null,
                                "customerCorporateBusinessPermitInstitutionTypeReason": _providerRincian.isCompanyTypeDakor ? "99" : null,
                                "customerCorporateBusinessPermitProfileReason": _providerRincian.isProfileDakor ? "99" : null,
                                "customerCorporateBusinessPermitFullNameReason":  _providerRincian.isCompanyNameDakor ? "99" : null,
                                "customerCorporateBusinessPermitDeedOfIncorporationNumberReason": null,
                                "customerCorporateBusinessPermitDeedEndDateReason": null,
                                "customerCorporateBusinessPermitSiupNumberReason": null,
                                "customerCorporateBusinessPermitSiupStartDateReason": null,
                                "customerCorporateBusinessPermitTdpNumberReason": null,
                                "customerCorporateBusinessPermitTdpStartDateReason": null,
                                "customerCorporateBusinessPermitEstablishmentDateReason": _providerRincian.isEstablishDateDakor ? "99" : null,
                                "customerCorporateBusinessPermitIsCompanyReason": null,
                                "customerCorporateBusinessPermitAuthorizedCapitalReason": null,
                                "customerCorporateBusinessPermitPaidCapitalReason": null,
                                "customerCorporatePicIdentityTypeReason": _providerManajemenPIC.identityTypeDakor ? "99" : null,
                                "customerCorporatePicIdentityNumberReason": _providerManajemenPIC.identityNoDakor ? "99" : null,
                                "customerCorporatePicIdentityNameReason": _providerManajemenPIC.fullnameDakor ? "99" : null,
                                "customerCorporatePicIdentityFullNameReason": _providerManajemenPIC.fullnameIdDakor ?  "99" : null,
                                "customerCorporatePicIdentitAliasReason": null,
                                "customerCorporatePicIdentityTitleReason": null,
                                "customerCorporatePicIdentityDateOfBirthReason": _providerManajemenPIC.dateOfBirthDakor ? "99" : null,
                                "customerCorporatePicIdentityPlaceOfBirthReason": _providerManajemenPIC.placeOfBirthDakor ? "99" : null,
                                "customerCorporatePicIdentityPlaceOfBirthKabKotaReason": _providerManajemenPIC.placeOfBirthLOVDakor ? "99" : null,
                                "customerCorporatePicIdentityGenderReason": null,
                                "customerCorporatePicIdentityActiveStartReason": null,
                                "customerCorporatePicIdentityActiveEndReason": null,
                                "customerCorporatePicIdentitReligionReason": null,
                                "customerCorporatePicIdentitOccupationIDReason": null,
                                "customerCorporatePicIdentitPositionIDReason": _providerManajemenPIC.positionDakor ? "99" : null,
                                "customerCorporatePicIdentitMaritalStatusIDReason": null,
                                "customerCorporatePicContactTelephoneArea1Reason": null,
                                "customerCorporatePicContactTelephone1Reason": null,
                                "customerCorporatePicContactTelephoneArea2Reason": null,
                                "customerCorporatePicContactTelephone2Reason": null,
                                "customerCorporatePicContactFaxAreaReason": null,
                                "customerCorporatePicContactFaxReason": null,
                                "customerCorporatePicContactHandphoneReason": _providerManajemenPIC.phoneDakor ? "99" : null,
                                "customerCorporatePicContactEmailReason": null,
                                "customerCorporateShareholdingReason": null,
                                "customerCorporateShareholdersIndividualsReason": _customerCorporateShareholdersIndividualsReason,
                                "customerCorporateShareholdersCorporatesReason": _customerCorporateShareholdersCorporatesReason,
                                "customerCorporateDedupScoreReason": null,
                                "customerCorporateStatusReason": null,
                                "customerCorporateCreatedAtReason": null,
                                "customerCorporateCreatedByReason": null,
                                "customerCorporateModifiedAtReason": null,
                                "customerCorporateModifiedByReason": null,
                            }
                        ],
                        "exposureInformationsReason": [
                            {
                                "exposureInformationIDReason": null,
                                "exposureGroupObjectIDReason": null,
                                "exposureOsArReason": null,
                                "exposureArOnProcessReason": null,
                                "exposureTotalReason": null,
                                "exposureObligorIDReason": null,
                                "exposureArNewReason": null,
                                "exposureExposureOthersReason": null,
                                "exposureStatusReason": null,
                                "exposureCreatedAtReason": null,
                                "exposureCreatedByReason": null,
                                "exposureModifiedAtReason": null,
                                "exposureModifiedByReason": null,
                                "exposureInformationFlagCorrectionReason": null
                            }
                        ],
                        "customerIncomesReason": _customerIncomesReason,
                        "customerAdditionalCorrespondenceTypeIDReason": null,
                        "customerAdditionalDebtorL2CodeReason": null,
                        "customerAdditionalDebtorL3CodeReason": null,
                        "customerBankAccountsReason": [],
                        "customerProcessingLastKnownCustomerStateReason": null,
                        "customerProcessingNextKnownCustomerStateReason": null,
                        "customerCreatedAtReason": null,
                        "customerCreatedByReason": null,
                        "customerModifiedAtReason": null,
                        "customerModifiedByReason": null,
                        "customerStatusReason": null
                    },
                    "orderDTOReason": {
                        "orderIDReason": null,
                        "orderCustomerIDReason": null,
                        "orderSpecificationIsMayorReason": null,
                        "orderSpecificationHasFiduciaReason": null,
                        "orderSpecificationApplicationSourceReason": null,
                        "orderSpecificationOrderDateReason": _providerApp.isOrderDateChanges ? "99" : null,
                        "orderSpecificationApplicationDateReason": _providerApp.isOrderDateChanges ? "99" : null,
                        "orderSpecificationSurveyAppointmentDateReason": _providerApp.isSurveyAppointmentDateChanges ? "99" : null,
                        "orderSpecificationOrderTypeReason": null,
                        "orderSpecificationIsOpenAccountReason": null,
                        "orderSpecificationAccountFormNumberReason": null,
                        "orderSpecificationSentraCIDReason": null,
                        "orderSpecificationUnitCIDReason": null,
                        "orderSpecificationInitialRecommendationReason": null,
                        "orderSpecificationFinalRecommendationReason": null,
                        "orderSpecificationBrmsScoringReason": null,
                        "orderSpecificationIsInPlaceApprovalReason": null,
                        "orderSpecificationIsPkSignedReason": _providerApp.isIsSignedPKChanges ? "99" : null,
                        "orderSpecificationMaxApprovalLevelReason": null,
                        "orderSpecificationJenisKonsepReason": _providerApp.isConceptTypeModelChanges ? "99" : null,
                        "orderSpecificationJumlahObjekReason": _providerApp.isTotalObjectChanges ? "99" : null,
                        "orderSpecificationJenisProporsionalAsuransiReason": _providerApp.isPropotionalObjectChanges ? "99" : null,
                        "orderSpecificationUnitKeReason": _providerApp.isNumberOfUnitChanges ? "99" : null,
                        "orderSpecificationflagConfirmDasReason": null,
                        "orderSpecificationflagUploadDRReason": null,
                        "orderSpecificationflagCADakorReason": null,
                        "orderSpecificationflagInstantApprovalReason": null,
                        "orderSpecificationCollateralTypeIDReason": null,
                        "orderSpecificationIsWillingToAcceptInfoReason": null,
                        "orderSpecificationApplicationContractNumberReason": null,
                        "orderSpecificationApplicationDocVerStatusReason": null,
                        "orderSpecificationCustomerStatusReason": null,
                        "orderSpecificationAosApprovalStatusReason": null,
                        "orderSupportingDocumentsReason": [
                            {
                                "orderSupportingDocumentIDReason": null,
                                "orderSupportingDocumentProductIDReason": null,
                                "orderSupportingDocumentTypeIDReason": null,
                                "orderSupportingDocumentIsMandatoryReason": null,
                                "orderSupportingDocumentIsMandatoryCAReason": null,
                                "orderSupportingDocumentIsDisplayReason": null,
                                "orderSupportingDocumentIsUnitReason": null,
                                "orderSupportingDocumentDocumentReceivedDateReason": null,
                                "orderSupportingDocumentFileNameReason": null,
                                "orderSupportingDocumentHeaderIDReason": null,
                                "orderSupportingDocumentUploadDateReason": null,
                                "orderSupportingDocumentCreatedAtReason": null,
                                "orderSupportingDocumentCreatedByReason": null,
                                "orderSupportingDocumentModifiedAtReason": null,
                                "orderSupportingDocumentModifiedByReason": null,
                                "orderSupportingDocumentStatusReason": null,
                                "orderSupportingDocumentFlagCorrectionReason": null
                            }
                        ],
                        "orderProductsReason": [
                            {
                                "orderProductIDReason": null,
                                "orderProductFinancingTypeIDReason": _providerUnitObject.isTypeOfFinancingModelChanges ? "99" : null,
                                "orderProductOjkBusinessTypeIDReason": _providerUnitObject.isBusinessActivitiesModelChanges ? "99" : null,
                                "orderProductOjkBussinessDetailIDReason": _providerUnitObject.isBusinessActivitiesTypeModelChanges ? "99" : null,
                                "orderProductObjectGroupIDReason": _providerUnitObject.isGroupObjectChanges ? "99" : null,
                                "orderProductObjectIDReason": _providerUnitObject.isObjectChanges ? "99" : null,
                                "orderProductProductTypeIDReason": _providerUnitObject.isTypeProductChanges ? "99" : null,
                                "orderProductObjectBrandIDReason": _providerUnitObject.isBrandObjectChanges ? "99" : null,
                                "orderProductObjectTypeIDReason": _providerUnitObject.isObjectTypeChanges ? "99" : null,
                                "orderProductObjectModelIDReason": _providerUnitObject.isModelObjectChanges ? "99" : null,
                                "orderProductModelDetailReason": _providerUnitObject.isDetailModelChanges ? "99" : null,
                                "orderProductObjectUsageIDReason": _providerUnitObject.isUsageObjectModelChanges ? "99" : null,
                                "orderProductObjectPurposeIDReason": _providerUnitObject.isObjectPurposeChanges ? "99" : null,
                                "orderProductSalesGroupIDReason": _providerUnitObject.isGroupSalesChanges ? "99" : null,
                                "orderProductOrderSourceIDReason": _providerUnitObject.isSourceOrderChanges ? "99" : null,
                                "orderProductOrderSourceNameReason": _providerUnitObject.isSourceOrderNameChanges ? "99" : null,
                                "orderProductGroupIDReason": _providerUnitObject.isGrupIdChanges ? "99" : null,
                                "orderProductOutletIDReason": null,
                                "orderProductIsThirdPartyReason": _providerUnitObject.isThirdPartyChanges ? "99" : null,
                                "orderProductThirdPartyTypeIDReason": _providerUnitObject.isThirdPartyTypeChanges ? "99" : null,
                                "orderProductThirdPartyIDReason": _providerUnitObject.isThirdPartyChanges ? "99" : null,
                                "orderProductThirdPartyActivityReason": _providerUnitObject.isKegiatanChanges ? "99" : null,
                                "orderProductSentradIDReason": null, //_providerUnitObject.isSentraDChanges ? "99" : null,
                                "orderProductUnitdIDReason": null, //_providerUnitObject.isUnitDChanges ? "99" : null,
                                "orderProductDealerMatrixReason": _providerUnitObject.isMatriksDealerChanges ? "99" : null,
                                "orderProductProgramIDReason": _providerUnitObject.isProgramChanges ? "99" : null,
                                "orderProductRehabTypeReason":  _providerUnitObject.isRehabTypeChanges ? "99" : null,
                                "orderProductReferenceNumberReason": _providerUnitObject.isReferenceNumberChanges ? "99" : null,
                                "orderProductApplicationUnitContractNumberReason": null,
                                "orderProductSalesesReason": _orderProductSalesesDakor,
                                "orderProductKaroserisReason": _orderProductKaroserisReason,
                                "orderProductInsurancesReason": _orderProductInsurancesReason,
                                "orderProductWmpsReason": _orderProductWmpsReason,
                                "orderProductCreditStructureInstallmentTypeIDReason": _providerCreditStructure.installmentTypeDakor ? "99" : null,
                                "orderProductCreditStructureTenorReason": _providerCreditStructure.periodTimeDakor ? "99" : null,
                                "orderProductCreditStructurePaymentMethodIDReason": _providerCreditStructure.paymentMethodDakor ? "99" : null,
                                "orderProductCreditStructureEffectiveRateReason": _providerCreditStructure.interestRateEffDakor ? "99" : null,
                                "orderProductCreditStructureFlatRateReason": _providerCreditStructure.interestRateFlatDakor ? "99" : null,
                                "orderProductCreditStructureObjectPriceReason": _providerCreditStructure.objectPriceDakor ? "99" : null,
                                "orderProductCreditStructureKaroseriTotalPriceReason": _providerCreditStructure.totalPriceKaroseriDakor ? "99" : null,
                                "orderProductCreditStructureTotalPriceReason": _providerCreditStructure.totalPriceDakor ? "99" : null,
                                "orderProductCreditStructureNettDownPaymentReason": _providerCreditStructure.netDPDakor ? "99" : null,
                                "orderProductCreditStructureDeclineNInstallmentReason": null,
                                "orderProductCreditStructurePaymentOfYearReason": _providerCreditStructure.paymentPerYearDakor ? "99" : null,
                                "orderProductCreditStructureBranchDownPaymentReason": _providerCreditStructure.branchDPDakor ? "99" : null,
                                "orderProductCreditStructureGrossDownPaymentReason": _providerCreditStructure.grossDPDakor ? "99" : null,
                                "orderProductCreditStructureTotalLoanReason": _providerCreditStructure.totalLoanDakor ? "99" : null,
                                "orderProductCreditStructureInstallmentAmountReason": _providerCreditStructure.installmentDakor ? "99" : null,
                                "orderProductCreditStructureLtvRatioReason": _providerCreditStructure.ltvDakor ? "99" : null,
                                "orderProductCreditStructureInterestAmountReason": null,
                                "orderProductCreditStructurePencairanReason": null,
                                "orderProductCreditStructureDSRReason": _providerCreditIncome.isDebtComparisonDakor ? "99" : null,
                                "orderProductCreditStructureDIRReason": _providerCreditIncome.isIncomeComparisonDakor ? "99" : null,
                                "orderProductCreditStructureDSCReason": _providerCreditIncome.isDSCDakor ? "99" : null,
                                "orderProductCreditStructureIRRReason": _providerCreditIncome.isIRRDakor ? "99" : null,
                                "orderProductCreditStructureGpTypeReason": null,
                                "orderProductCreditStructureNewTenorReason": null,
                                "orderProductCreditStructureTotalSteppingReason": null,
                                "orderProductCreditStructureBalloonTypeReason": _providerCreditStructureType.isRadioValueBalloonPHOrBalloonInstallmentChanges ? "99" : null,
                                "orderProductCreditStructureLastInstallmentPercentageReason": _providerCreditStructureType.isInstallmentPercentage ? "99" : null,
                                "orderProductCreditStructureLastInstallmentValueReason": _providerCreditStructureType.isInstallmentValue ? "99" : null,
                                "orderProductCreditStructureFirstInstallmentValueReason": null,
                                "orderProductFeesReason": _orderProductFeesReason,
                                "orderProductSubsidiesReason": _orderProductSubsidiesReason,
                                "orderProductCreditStructureInstallmentDetailsReason": [],
                                "orderProductCollateralAutomotivesReason": _providerKolateral.collateralTypeModel.id == "001"
                                    ?
                                [
                                    {
                                        "collateralAutoCollateralIDReason": null,
                                        "collateralAutoIsCollaSameUnitReason": _providerKolateral.isRadioValueIsCollateralSameWithUnitOtoChanges ? "99" : null,
                                        "collateralAutoIsMultiUnitCollateralReason": null,
                                        "collateralAutoIsCollaNameSameWithCustomerNameReason": _providerKolateral.isRadioValueIsCollaNameSameWithApplicantOtoChanges ? "99" : null,
                                        "collateralAutoIdentityTypeReason": _providerKolateral.isIdentityTypeSelectedAutoChanges ? "99" : null,
                                        "collateralAutoIdentityNumberReason": _providerKolateral.isIdentityNumberAutoChanges ? "99" : null,
                                        "collateralAutoIdentityNameReason": _providerKolateral.isNameOnCollateralAutoChanges ? "99" : null,
                                        "collateralAutoFullNameReason": _providerKolateral.isNameOnCollateralAutoChanges ? "99" : null,
                                        "collateralAutoAliasReason": null,
                                        "collateralAutoTitleReason": null,
                                        "collateralAutoDateOfBirthReason": _providerKolateral.isBirthDateAutoChanges ? "99" : null,
                                        "collateralAutoPlaceOfBirthReason": _providerKolateral.isBirthPlaceValidWithIdentityAutoChanges ? "99" : null,
                                        "collateralAutoPlaceOfBirthKabKotaReason": _providerKolateral.isBirthPlaceValidWithIdentityLOVAutoChanges ? "99" : null,
                                        "collateralAutoGenderReason": null,
                                        "collateralAutoIdentityActiveStartReason": null,
                                        "collateralAutoIdentityActiveEndReason": null,
                                        "collateralAutoReligionReason": null,
                                        "collateralAutoOccupationIDReason": null,
                                        "collateralAutoPositionIDReason": null,
                                        "collateralAutoObjectGroupIDReason": _providerKolateral.isGroupObjectChanges ? "99" : null,
                                        "collateralAutoObjectIDReason": _providerKolateral.isObjectChanges ? "99" : null,
                                        "collateralAutoProductTypeIDReason": null, //_providerKolateral.isTypeProductChanges ? "99" : null,
                                        "collateralAutoObjectBrandIDReason": _providerKolateral.isBrandObjectChanges ? "99" : null,
                                        "collateralAutoObjectTypeIDReason": _providerKolateral.isObjectTypeChanges ? "99" : null,
                                        "collateralAutoObjectModelIDReason": _providerKolateral.isModelObjectChanges ? "99" : null,
                                        "collateralAutoObjectUsageIDReason": _providerKolateral.isUsageObjectModelChanges ? "99" : null,
                                        "collateralAutoObjectPurposeIDReason": _providerKolateral.isUsageCollateralOtoChanges ? "99" : null,
                                        "collateralAutProductionYearReason": _providerKolateral.isYearProductionSelectedChanges ? "99" : null,
                                        "collateralAutoRegistrationYearReason": _providerKolateral.isYearRegistrationSelectedChanges ? "99" : null,
                                        "collateralAutoIsYellowPlateReason": _providerKolateral.isRadioValueYellowPlatChanges ? "99" : null,
                                        "collateralAutoIsBuiltUpReason": _providerKolateral.isRadioValueBuiltUpNonATPMChanges ? "99" : null,
                                        "collateralAutoBpkbNumberReason": _providerKolateral.isBPKPNumberChanges ? "99" : null,
                                        "collateralAutoChassisNumberReason": _providerKolateral.isFrameNumberChanges ? "99" : null,
                                        "collateralAutoEngineNumberReason": _providerKolateral.isMachineNumberChanges ? "99" : null,
                                        "collateralAutoPoliceNumberReason": _providerKolateral.isPoliceNumberChanges ? "99" : null,
                                        "collateralAutoGradeUnitReason": _providerKolateral.isGradeUnitChanges ? "99" : null,
                                        "collateralAutoUtjFacilityReason": _providerKolateral.isFasilitasUTJChanges ? "99" : null,
                                        "collateralAutoBidderNameReason": _providerKolateral.isNamaBidderChanges ? "99" : null,
                                        "collateralAutoShowroomPriceReason": _providerKolateral.isHargaJualShowroomChanges ? "99" : null,
                                        "collateralAutoAdditionalEquipmentReason": _providerKolateral.isPerlengkapanTambahanChanges ? "99" : null,
                                        "collateralAutoMpAdiraReason": _providerKolateral.isMPAdiraChanges ? "99" : null,
                                        "collateralAutoPhysicalReconditionReason": _providerKolateral.isRekondisiFisikChanges ? "99" : null,
                                        "collateralAutoIsProperReason": null,
                                        "collateralAutoLtvRatioReason": null,
                                        "collateralAutoMpAdiraUploadReason": _providerKolateral.isMPAdiraUploadChanges ? "99" : null,
                                        "collateralAutoCollateralDpReason": _providerKolateral.isDpGuaranteeChanges ? "99" : null,
                                        "collateralAutoMaximumPhReason": _providerKolateral.isPHMaxAutomotiveChanges ? "99" : null,
                                        "collateralAutoAppraisalPriceReason": _providerKolateral.isTaksasiPriceAutomotiveChanges ? "99" : null,
                                        "collateralAutoCollateralUtilizationIDReason": _providerKolateral.isUsageCollateralOtoChanges ? "99" : null,
                                        "collateralAutoAdditionalCapacityReason": null,
                                        "collateralAutoAdditionalColorReason": null,
                                        "collateralAutoAdditionalManufacturerReason": null,
                                        "collateralAutoAdditionalSerialNumberReason": null,
                                        "collateralAutoAdditionalInvoiceNumberReason": null,
                                        "collateralAutoAdditionalStnkActiveDateReason": null,
                                        "collateralAutoAdditionalBpkbAddressReason": null,
                                        "collateralAutoAdditionalBpkbIdentityTypeIDReason": null,
                                        "collateralAutoStatusReason": null,
                                        "collateralAutoCreatedAtReason": null,
                                        "collateralAutoCreatedByReason": null,
                                        "collateralAutoModifiedAtReason": null,
                                        "collateralAutoModifiedByReason": null,
                                        "collateralAutoFlagCorrectionReason": null
                                    }
                                ]
                                    : [],
                                "orderProductCollateralPropertiesReason": _providerKolateral.collateralTypeModel.id == "002"
                                    ?
                                [
                                    {
                                        "collateralPropertyIsMultiUnitCollateralReason": null,
                                        "collateralPropertyIsCollaNameSameWithCustomerNameReason": _providerKolateral.isRadioValueIsCollateralSameWithApplicantPropertyChanges ? "99" : null,
                                        "collateralPropertyIDReason": null,
                                        "collateralPropertyIdentityTypeReason": _providerKolateral.isIdentityModelChanges ? "99" : null,
                                        "collateralPropertyIdentityNumberReason": _providerKolateral.isIdentityNumberPropChanges ? "99" : null,
                                        "collateralPropertyIdentityNameReason": _providerKolateral.isNameOnCollateralChanges ? "99" : null,
                                        "collateralPropertyIdentityFullNameReason": _providerKolateral.isNameOnCollateralChanges ? "99" : null,
                                        "collateralPropertyIdentityAliasReason": null,
                                        "collateralPropertyIdentityTitleReason": null,
                                        "collateralPropertyIdentityDateOfBirthReason": _providerKolateral.isBirthDatePropChanges ? "99" : null,
                                        "collateralPropertyIdentityPlaceOfBirthReason": _providerKolateral.isBirthPlaceValidWithIdentity1Changes ? "99" : null,
                                        "collateralPropertyIdentityPlaceOfBirthKabKotaReason": _providerKolateral.isBirthPlaceValidWithIdentityLOVChanges ? "99" : null,
                                        "collateralPropertyIdentityGenderReason": null,
                                        "collateralPropertyIdentityActiveStartReason": null,
                                        "collateralPropertyIdentityActiveEndReason": null,
                                        "collateralPropertyIdentityReligionReason": null,
                                        "collateralPropertyIdentityOccupationIDReason": null,
                                        "collateralPropertyIdentityPositionIDReason": null,
                                        "collateralPropertyCertificateNumberReason": _providerKolateral.isCertificateNumberChanges ? "99" : null,
                                        "collateralPropertyCertificateTypeIDReason": _providerKolateral.isCertificateTypeSelectedChanges ? "99" : null,
                                        "collateralPropertyPropertyTypeIDReason": _providerKolateral.isPropertyTypeSelectedChanges ? "99" : null,
                                        "collateralPropertyBuildingAreaReason": _providerKolateral.isBuildingAreaChanges ? "99" : null,
                                        "collateralPropertyLandAreaReason": _providerKolateral.isSurfaceAreaChanges ? "99" : null,
                                        "collateralPropertyCollateralDpReason": _providerKolateral.isDPJaminanChanges ? "99" : null,
                                        "collateralPropertyMaximumPhReason": _providerKolateral.isPHMaxChanges ? "99" : null,
                                        "collateralPropertyAppraisalPriceReason": _providerKolateral.isTaksasiPriceChanges ? "99" : null,
                                        "collateralPropertyCollateralUtilizationIDReason": _providerKolateral.isUsageCollateralPropertyChanges ? "99" : null,
                                        "collateralPropertyPositiveFasumDistanceReason": _providerKolateral.isJarakFasumPositifChanges ? "99" : null,
                                        "collateralPropertyNegativeFasumDistanceReason": _providerKolateral.isJarakFasumNegatifChanges ? "99" : null,
                                        "collateralPropertyLandPriceReason": _providerKolateral.isHargaTanahChanges ? "99" : null,
                                        "collateralPropertyNjopPriceReason": _providerKolateral.isHargaNJOPChanges ? "99" : null,
                                        "collateralPropertyBuildingPriceReason": _providerKolateral.isHargaBangunanChanges ? "99" : null,
                                        "collateralPropertyRoofTypeIDReason": _providerKolateral.isTypeOfRoofSelectedChanges ? "99" : null,
                                        "collateralPropertyWallTypeIDReason": _providerKolateral.isWallTypeSelectedChanges ? "99" : null,
                                        "collateralPropertyFloorTypeIDReason": _providerKolateral.isFloorTypeSelectedChanges ? "99" : null,
                                        "collateralPropertyFoundationTypeIDReason": _providerKolateral.isFoundationTypeSelectedChanges ? "99" : null,
                                        "collateralPropertyRoadTypeIDReason": _providerKolateral.isStreetTypeSelectedChanges ? "99" : null,
                                        "collateralPropertyIsCarPassableReason": _providerKolateral.isRadioValueAccessCarChanges ? "99" : null,
                                        "collateralPropertyTotalOfHouseInRadiusReason": _providerKolateral.isJumlahRumahDalamRadiusChanges ? "99" : null,
                                        "collateralPropertySifatJaminanReason": _providerKolateral.isSifatJaminanChanges ? "99" : null,
                                        "collateralPropertyBuktiKepemilikanTanahReason": _providerKolateral.isBuktiKepemilikanChanges ? "99" : null,
                                        "collateralPropertyTgglTerbitSertifikatReason": _providerKolateral.isCertificateReleaseDateChanges ? "99" : null,
                                        "collateralPropertyTahunTerbitSertifikatReason": _providerKolateral.isCertificateReleaseYearChanges ? "99" : null,
                                        "collateralPropertyNamaPemegangHakReason": _providerKolateral.isNamaPemegangHakChanges ? "99" : null,
                                        "collateralPropertyNoSuratUkurGmbrSituasiReason": _providerKolateral.isNoSuratUkurChanges ? "99" : null,
                                        "collateralPropertyTgglSuratUkurGmbrSituasiReason": _providerKolateral.isDateOfMeasuringLetterChanges ? "99" : null,
                                        "collateralPropertySertifikatSuratUkurDikeluarkanOlehReason": _providerKolateral.isCertificateOfMeasuringLetterChanges ? "99" : null,
                                        "collateralPropertyMasaBerlakuHakReason": _providerKolateral.isMasaHakBerlakuChanges ? "99" : null,
                                        "collateralPropertyNoImbReason": _providerKolateral.isNoIMBChanges ? "99" : null,
                                        "collateralPropertyTgglImbReason": _providerKolateral.isDateOfIMBChanges ? "99" : null,
                                        "collateralPropertyLuasBangunanImbReason": _providerKolateral.isLuasBangunanIMBChanges ? "99" : null,
                                        "collateralPropertyLtvRatioReason": null,
                                        "collateralPropertyLetakTanahReason": null,
                                        "collateralPropertyDetailAddressesReason": [
                                            {
                                                "addressIDReason": null,
                                                "addressForeignBusinessIDReason": null,
                                                "addressSpecificationKorespondenReason": null,
                                                "addressSpecificationMatrixAddrReason": null,
                                                "addressSpecificationAddressReason": _providerKolateral.isAddressDakor ? "99" : null,
                                                "addressSpecificationRtReason": _providerKolateral.isRTDakor ? "99" : null,
                                                "addressSpecificationRwReason": _providerKolateral.isRWDakor ? "99" : null,
                                                "addressSpecificationProvinsiIDReason": _providerKolateral.isProvinsiDakor ? "99" : null,
                                                "addressSpecificationKabkotIDReason": _providerKolateral.isKabKotDakor ? "99" : null,
                                                "addressSpecificationKecamatanIDReason": _providerKolateral.isKecamatanDakor ? "99" : null,
                                                "addressSpecificationKelurahanIDReason": _providerKolateral.isKelurahanDakor ? "99" : null,
                                                "addressSpecificationZipcodeReason": _providerKolateral.isKodePosDakor ? "99" : null,
                                                "addressContactSpecificationTelephoneArea1Reason": null,
                                                "addressContactSpecificationTelephone1Reason": null,
                                                "addressContactSpecificationTelephoneArea2Reason": null,
                                                "addressContactSpecificationTelephone2Reason": null,
                                                "addressContactSpecificationFaxAreaReason": null,
                                                "addressContactSpecificationFaxReason": null,
                                                "addressContactSpecificationHandphoneReason": null,
                                                "addressContactSpecificationEmailReason": null,
                                                "addressTypeReason": _providerKolateral.isAddressTypeDakor ? "99" : null,
                                                "addressCreationalSpecificationCreatedAtReason": null,
                                                "addressCreationalSpecificationCreatedByReason": null,
                                                "addressCreationalSpecificationModifiedAtReason": null,
                                                "addressCreationalSpecificationModifiedByReason": null,
                                                "addressStatusReason": null,
                                                "addressFlagCorrectionReason": null,
                                            }
                                        ],
                                        "collateralPropertyStatusReason": null,
                                        "collateralPropertyCreatedAtReason": null,
                                        "collateralPropertyCreatedByReason": null,
                                        "collateralPropertyModifiedAtReason": null,
                                        "collateralPropertyModifiedByReason": null,
                                        "collateralPropertyFlagCorrectionReason": null,
                                    }
                                ]
                                    : [],
                                "orderProdAutoDebitIsAutoDebitReason": null,
                                "orderProdAutoDebitBankIDReason": null,
                                "orderProdAutoDebitAccountNumberReason": null,
                                "orderProdAutoDebitAccountBehalfReason": null,
                                "orderProdAutoDebitAccountPurposeTypeIDReason": null,
                                "orderProdAutoDebitDownPaymentSourceIDReason": null,
                                "orderProductAdditionalVirtualAccountIDReason": null,
                                "orderProductAdditionalVirtualAccountValueReason": null,
                                "orderProductAdditionalCashingPurposeIDReason": null,
                                "orderProductAdditionalCashingTypeIDReason": null,
                                "orderProductCompletionLastKnownOrderProductStateReason": null,
                                "orderProductCompletionNextKnownOrderProductStateReason": null,
                                "orderProductTacActualReason": null,
                                "orderProductTacMaxReason": null,
                                "orderProductCreatedAtReason": null,
                                "orderProductCreatedByReason": null,
                                "orderProductModifiedAtReason": null,
                                "orderProductModifiedByReason": null,
                                "orderProductStatusReason": null,
                                "applSendFlagReason": null,
                                "isWithoutCollaReason": null,
                                "isSaveKaroseriReason": null,
                                "orderProductFlagCorrectionReason": null,
                                "orderProductNik": null,
                                "orderProductNama": null,
                                "orderProductJabatan": null
                            }
                        ],
                        "orderProcessingCompletionCalculatedAtReason": null,
                        "orderProcessingCompletionLastKnownOrderStateReason": null,
                        "orderProcessingCompletionLastKnownOrderStatePositionReason": null,
                        "orderProcessingCompletionLastKnownOrderStatusReason": null,
                        "orderProcessingCompletionLastKnownOrderHandledByReason": null,
                        "orderTabFlagsReason": null,
                        "orderCreatedAtReason": null,
                        "orderCreatedByReason": null,
                        "orderModifiedAtReason": null,
                        "orderModifiedByReason": null,
                        "orderStatusReason": null
                    },
                    "surveyDTOReason": {
                        "surveyIDReason": null,
                        "surveyOrderIDReason": null,
                        "surveyTypeReason": null,
                        "surveyEmployeeIDReason": null,
                        "surveyPhoneNumberReason": null,
                        "surveyJanjiSurveyDateReason": null,
                        "surveyReasonReason": null,
                        "surveyFlagBRMSReason": null,
                        "surveyStatusReason": null,
                        "surveyDataStatusReason": null,
                        "surveyProcessReason": null,
                        "surveyCfoEligibilityStatusReason": null,
                        "surveyResultTypeReason": _providerSurvey.isSurveyTypeChanges ? "99" : null,
                        "surveyResultRecomendationTypeReason": _providerSurvey.isSurveyRecommendationChanges ? "99" : null,
                        "surveyResultNotesReason": _providerSurvey.isSurveyNotesChanges ? "99" : null,
                        "surveyResultDateReason": _providerSurvey.isSurveyDateChanges ? "99" : null,
                        "surveyResultDistanceWithSentraReason": _providerSurvey.isDistanceLocationWithSentraUnitChanges ? "99" : null,
                        "surveyResultDistanceWithDealerReason": _providerSurvey.isDistanceLocationWithDealerMerchantAXIChanges ? "99" : null,
                        "surveyResultDistanceObjWithSentraReason": _providerSurvey.isDistanceLocationWithUsageObjectWithSentraUnitChanges ? "99" : null,
                        "surveyResultAssetsReason": _surveyResultAssetsDakor,
                        "surveyResultDetailsReason": _surveyResultDetailsDakor,
                        "surveyResultTelesurveysReason": [],
                        "surveyCreatedAtReason": null,
                        "surveyCreatedByReason": null,
                        "surveyModifiedAtReason": null,
                        "surveyModifiedByReason": null
                    }
                },
                "ms2TaskID": _preferences.getString("order_no"),
                "flag": "",
                "creditLimitDTO": {
                    "lmeId": "",
                    "flagEligible": "",
                    "disburseType": "",
                    "productId": "",
                    "installmentAmt": "",
                    "phAmt": "",
                    "voucherCode": "",
                    "tenor": "",
                    "customerGrading": "",
                    "jenisPenawaran": "",
                    "urutanPencairan": "",
                    "jenisOpsi": "",
                    "maxPlafond": "",
                    "maxTenor": "",
                    "noReferensi": "",
                    "maxInstallment": "",
                    "portfolio": "",
                    "activationDate": "",
                }
            })
                :
            jsonEncode({
                "oldApplicationDTO": _oldApplicationDTO,
                "newApplicationDTO": {
                    "customerDTO": {
                        "customerID": _preferences.getString("last_known_state") != "IDE" ? _providerRincian.customerID : "NEW",// perlu di rencanakan mau ditaruh di sqlite atau di sharedPreference
                        "customerType": _preferences.getString("cust_type") == "PER" ? "PER" : "COM",
                        "customerSpecification": {
                            "customerNPWP": {
                                "npwpAddress": _providerRincian.controllerNPWPAddress.text,//_providerInfoNasabah.radioValueIsHaveNPWP == 1 ? _providerInfoNasabah.controllerAlamatSesuaiNPWP.text : _providerInfoAlamat.listAlamatKorespondensi[0].address,
                                "npwpFullname": _providerRincian.controllerFullNameNPWP.text,//_providerInfoNasabah.radioValueIsHaveNPWP == 1 ? _providerInfoNasabah.controllerNamaSesuaiNPWP.text  : _providerInfoNasabah.controllerNamaLengkapSesuaiIdentitas.text,
                                "npwpNumber": _providerRincian.controllerNPWP.text,//_providerInfoNasabah.radioValueIsHaveNPWP == 1 ? _providerInfoNasabah.controllerNoNPWP.text : "00000000000000",
                                "npwpType": _providerRincian.typeNPWPSelected != null ? _providerRincian.typeNPWPSelected.id : "2",
                                "hasNpwp": _providerRincian.radioValueIsHaveNPWP == 1 ? true : false,//_providerInfoNasabah.radioValueIsHaveNPWP == 1 ? true : false,
                                "pkpIdentifier": _providerRincian.signPKPSelected != null ? _providerRincian.signPKPSelected.id : "2",
                            },
                            "detailAddresses": _detailAddressesInfoNasabah,
                            "customerGuarantor": {
                                "isGuarantor": _providerGuarantor.radioValueIsWithGuarantor == 0,
                                "guarantorCorporates": _guarantorCorporates,
                                "guarantorIndividuals": _guarantorIndividuals
                            }
                        },
                        "customerIndividualDTO": [],
                        "customerCorporateDTO": [
                            {
                                "customerCorporateID": _preferences.getString("last_known_state") != "IDE" ? _providerRincian.customerCorporateID : "CUST001",
                                "businessSpecification": {
                                    "economySector": _providerRincian.sectorEconomicModelSelected != null ? _providerRincian.sectorEconomicModelSelected.KODE : null,//_preferences.getString("cust_type") == "PER" ? "" : _providerRincian.sectorEconomicModelSelected.KODE,
                                    "businessField": _providerRincian.businessFieldModelSelected != null ? _providerRincian.businessFieldModelSelected.KODE : null ,//_preferences.getString("cust_type") == "PER" ? "" : _providerRincian.businessFieldModelSelected.KODE,
                                    "locationStatus": _providerRincian.locationStatusSelected != null ?  _providerRincian.locationStatusSelected.id : null,//_preferences.getString("cust_type") == "PER" ? "" : _providerRincian.locationStatusSelected.id,
                                    "businessLocation": _providerRincian.businessLocationSelected != null ? _providerRincian.businessLocationSelected.id : null,//_preferences.getString("cust_type") == "PER" ? "" : _providerRincian.businessLocationSelected.id,
                                    "employeeTotal": _providerRincian.controllerTotalEmployees.text != "" ? int.parse(_providerRincian.controllerTotalEmployees.text) : null,//_preferences.getString("cust_type") == "PER" ? 0 : double.parse(_providerRincian.controllerTotalEmployees.text.replaceAll(",", "")),
                                    "bussinessLengthInMonth": _providerRincian.controllerLamaUsahaBerjalan.text != "" ? double.parse(_providerRincian.controllerLamaUsahaBerjalan.text.replaceAll(",", "")) : null,//_preferences.getString("cust_type") == "PER" ? 0 : double.parse(_providerRincian.controllerLamaUsahaBerjalan.text.replaceAll(",", "")),
                                    "totalBussinessLengthInMonth": _providerRincian.controllerTotalLamaUsahaBerjalan.text != "" ? int.parse(_providerRincian.controllerTotalLamaUsahaBerjalan.text.replaceAll(",", "")) : null,//_preferences.getString("cust_type") == "PER" ? 0 : double.parse(_providerRincian.controllerTotalLamaUsahaBerjalan.text.replaceAll(",", "")),
                                },
                                "businessPermitSpecification": {
                                    "institutionType": _providerRincian.typeInstitutionSelected != null ?  _providerRincian.typeInstitutionSelected.PARA_ID : "",//_preferences.getString("cust_type") == "PER" ? "" : _providerRincian.typeInstitutionSelected.PARA_ID,
                                    "profile": _providerRincian.profilSelected != null ? _providerRincian.profilSelected.id : "",//_preferences.getString("cust_type") == "PER" ? "" : _providerRincian.profilSelected.id,
                                    "fullName": _providerRincian.controllerInstitutionName.text != "" ? _providerRincian.controllerInstitutionName.text : "",//_preferences.getString("cust_type") == "PER" ? "" : _providerRincian.controllerInstitutionName.text,
                                    "deedOfIncorporationNumber": null,
                                    "deedEndDate": null,
                                    "siupNumber": null,
                                    "siupStartDate": null,
                                    "tdpNumber": null,
                                    "tdpStartDate": null,
                                    "establishmentDate": _providerRincian.controllerDateEstablishment.text != "" ? "${_providerRincian.controllerDateEstablishment.text} 00:00:00" : "01-01-1900 00:00:00",//formatDateValidateAndSubmit(_providerRincian.initialDateForDateEstablishment),//_preferences.getString("cust_type") == "PER" ? "" : _providerRincian.controllerDateEstablishment.text,
                                    "isCompany": false,
                                    "authorizedCapital": 0,
                                    "paidCapital": 0
                                },
                                "managementPIC": {
                                    "picIdentity": {
                                        "identityType": _providerManajemenPIC.typeIdentitySelected != null ? _providerManajemenPIC.typeIdentitySelected.id : "",
                                        "identityNumber":  _providerManajemenPIC.controllerIdentityNumber.text != "" ? _providerManajemenPIC.controllerIdentityNumber.text : "",
                                        "identityName": _providerManajemenPIC.controllerFullNameIdentity.text != "" ? _providerManajemenPIC.controllerFullNameIdentity.text : "",
                                        "fullName": _providerManajemenPIC.controllerFullName.text != "" ? _providerManajemenPIC.controllerFullName.text : "",
                                        "alias": null,
                                        "title": null,
                                        "dateOfBirth": _providerManajemenPIC.controllerBirthOfDate.text != "" ? formatDateValidateAndSubmit(_providerManajemenPIC.initialDateForBirthOfDate) : "",//_preferences.getString("cust_type") == "PER" ? "" : _providerManajemenPIC.controllerBirthOfDate.text,
                                        "placeOfBirth": _providerManajemenPIC.controllerPlaceOfBirthIdentity.text != "" ? _providerManajemenPIC.controllerPlaceOfBirthIdentity.text : "",//_preferences.getString("cust_type") == "PER" ? "" : _providerManajemenPIC.controllerPlaceOfBirthIdentity.text,
                                        "placeOfBirthKabKota": _providerManajemenPIC.birthPlaceSelected != null ? _providerManajemenPIC.birthPlaceSelected.KABKOT_ID : "",//_preferences.getString("cust_type") == "PER" ? "" : _providerManajemenPIC.birthPlaceSelected.KABKOT_ID,
                                        "gender": null,
                                        "identityActiveStart": null,
                                        "identityActiveEnd": null,
                                        "isLifetime": null,
                                        "religion": null,
                                        "occupationID": null,
                                        "positionID": _providerManajemenPIC.positionSelected != null ? _providerManajemenPIC.positionSelected.PARA_FAMILY_TYPE_ID : "10", // "02"
                                        "maritalStatusID": "02"
                                    },
                                    "picContact": {
                                        "telephoneArea1": null,
                                        "telephone1": null,
                                        "telephoneArea2": null,
                                        "telephone2": null,
                                        "faxArea": null,
                                        "fax": null,
                                        "handphone": _providerManajemenPIC.controllerHandphoneNumber.text != "" ? "08${_providerManajemenPIC.controllerHandphoneNumber.text}" : "",//_preferences.getString("cust_type") == "PER" ? "" : _providerManajemenPIC.controllerHandphoneNumber.text,
                                        "email": _providerManajemenPIC.controllerEmail.text != "" ? _providerManajemenPIC.controllerEmail.text : "",//_preferences.getString("cust_type") == "PER" ? "" : _providerManajemenPIC.controllerEmail.text,
                                        "noWa": null,
                                        "noWa2": null,
                                        "noWa3": null
                                    },
                                    "picAddresses": _detailAddressPIC,
                                    "shareholding": 0
                                },
                                "shareholdersCorporates": _shareholdersCorporates,
                                "shareholdersIndividuals": _pemegangSahamPribadi,
                                "dedupScore": 0.0,
                                "status": "ACTIVE",
                                "creationalSpecification": {
                                    "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                                    "createdBy": _preferences.getString("username"),
                                    "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                                    "modifiedBy": _preferences.getString("username"),
                                },
                            }
                        ],
                        "customerAdditionalInformation": null,
                        "customerProcessingCompletion": {
                            "lastKnownCustomerState": "CUSTOMER_GUARANTOR",
                            "nextKnownCustomerState": "CUSTOMER_GUARANTOR"
                        },
                        "creationalSpecification": {
                            "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                            "createdBy": _preferences.getString("username"),
                            "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                            "modifiedBy": _preferences.getString("username"),
                        },
                        "status": "ACTIVE",
                        "obligorID": _providerRincian.obligorID, // _preferences.getString("last_known_state") != "IDE" ? _providerRincian.obligorID != "NEW" ? _providerRincian.obligorID : _preferences.getString("oid").toString() : _preferences.getString("oid").toString(),
                        "exposureInformations": null,
                        "customerIncomes": _customerIncomes
                    },
                    "orderDTO": {
                        "orderID": _preferences.getString("last_known_state") != "IDE" ? _providerApp.applNo : orderId,
                        "customerID": _preferences.getString("last_known_state") != "IDE" ? _providerRincian.customerID : "NEW",
                        "orderSpecification": {
                            "flagDakorCA": 1,
                            "aosApprovalStatus": _preferences.getString("last_known_state") == "AOS" ? _providerSurvey.radioValueApproved : "",
                            "isMayor": false, // _preferences.getString("last_known_state") == "AOS" && _providerSurvey.radioValueApproved == "0" ? true : | _preferences.getString("is_mayor") == "0",
                            "hasFiducia": false,
                            "applicationSource": "003",
                            "orderDate": "${_providerApp.controllerOrderDate.text} 00:00:00",//di edit karena tgl orderDate lebih besar dr surveyAppointmentDate <formatDateValidateAndSubmit(_providerApp.initialDateOrder)>
                            "applicationDate": "${_providerApp.controllerOrderDate.text} 00:00:00",// di edit karena tgl applicationDate lebih besar dr surveyAppointmentDate <formatDateValidateAndSubmit(_providerApp.initialDateOrder)>
                            "surveyAppointmentDate": "${_providerApp.controllerSurveyAppointmentDate.text}" " ${_providerApp.controllerSurveyAppointmentTime.text}:00", //formatDateValidateAndSubmit(_providerApp.initialSurveyDate),
                            "orderType": _preferences.getString("status_aoro") != null ? _preferences.getString("status_aoro") : "0",//_preferences.getString("status_aoro"),
                            "isOpenAccount": true,
                            "accountFormNumber": null,
                            "sentraCID": _preferences.getString("SentraD"),
                            "unitCID": _preferences.getString("UnitD"),
                            "initialRecommendation": _providerApp.initialRecomendation,
                            "finalRecommendation": _providerApp.finalRecomendation,
                            "brmsScoring": _providerApp.brmsScoring,
                            "isInPlaceApproval": false,
                            "isPkSigned": _providerApp.isSignedPK,
                            "maxApprovalLevel": null,
                            "jenisKonsep": _providerApp.conceptTypeModelSelected != null ? _providerApp.conceptTypeModelSelected.id : "",
                            "jumlahObjek": _providerApp.controllerTotalObject.text == "" ? null : int.parse(_providerApp.controllerTotalObject.text),
                            "jenisProporsionalAsuransi": _providerApp.proportionalTypeOfInsuranceModelSelected != null ? _providerApp.proportionalTypeOfInsuranceModelSelected.kode : "",
                            "unitKe": _providerApp.numberOfUnitSelected == "" ? null : int.parse(_providerApp.numberOfUnitSelected),
                            "isWillingToAcceptInfo": false,
                            "applicationContractNumber": null,
                            "dealerNote": Provider.of<MarketingNotesChangeNotifier>(context,listen: false).controllerMarketingNotes.text,
                            "userDealer": "",
                            "orderDealer": "",
                            "flagSourceMS2": "103", // diganti dari 006 ke 103 sesuai arahan mas Oji (per tanggal 06.07.2021)
                            "orderSupportingDocuments": _orderSupportingDocuments,
                            "collateralTypeID": _providerKolateral.collateralTypeModel.id,
                            "applicationDocVerStatus": null
                        },
                        "orderProducts": [
                            {
                                "orderProductID": _preferences.getString("last_known_state") != "IDE" ? _providerUnitObject.applObjtID : orderProductId, // jika IDE ngambil dari nomer unit, selain IDE ngambil dari APPL_NO get data from DB API
                                "orderProductSpecification": {
                                    "financingTypeID": _providerUnitObject.typeOfFinancingModelSelected.financingTypeId,//_preferences.getString("cust_type") == "PER" ? _providerFoto.typeOfFinancingModelSelected.financingTypeId : _providerUnitObject.typeOfFinancingModelSelected.financingTypeId,
                                    "ojkBusinessTypeID": _providerUnitObject.businessActivitiesModelSelected.id,//_preferences.getString("cust_type") == "PER" ? _providerFoto.kegiatanUsahaSelected.id.toString() : _providerUnitObject.typeOfFinancingModelSelected.financingTypeId.toString(),
                                    "ojkBussinessDetailID": _providerUnitObject.businessActivitiesTypeModelSelected.id,//_preferences.getString("cust_type") == "PER" ? _providerFoto.jenisKegiatanUsahaSelected.id.toString() : _providerUnitObject.typeOfFinancingModelSelected.financingTypeId,
                                    "orderUnitSpecification": {
                                        "objectGroupID": _providerUnitObject.groupObjectSelected != null ? _providerUnitObject.groupObjectSelected.KODE : "",
                                        "objectID": _providerUnitObject.objectSelected != null ? _providerUnitObject.objectSelected.id : "",
                                        "productTypeID": _providerUnitObject.productTypeSelected != null ? _providerUnitObject.productTypeSelected.id : "",
                                        "objectBrandID": _providerUnitObject.brandObjectSelected != null ? _providerUnitObject.brandObjectSelected.id : "",
                                        "objectTypeID": _providerUnitObject.objectTypeSelected != null ? _providerUnitObject.objectTypeSelected.id : "",
                                        "objectModelID": _providerUnitObject.modelObjectSelected != null ? _providerUnitObject.modelObjectSelected.id : "",
                                        "objectUsageID": _providerUnitObject.objectUsageModel != null ? _providerUnitObject.objectUsageModel.id : "", // field pemakaian objek (unit)
                                        "objectPurposeID": _providerUnitObject.objectPurposeSelected != null ? _providerUnitObject.objectPurposeSelected.id : "", // field tujuan objek (unit)
                                        "modelDetail": _providerUnitObject.controllerDetailModel.text == "" ? null : _providerUnitObject.controllerDetailModel.text
                                    },
                                    "salesGroupID": _providerUnitObject.groupSalesSelected != null ? _providerUnitObject.groupSalesSelected.kode : "",
                                    "orderSourceID": _providerUnitObject.sourceOrderSelected != null ? _providerUnitObject.sourceOrderSelected.kode : "",
                                    "orderSourceName": _providerUnitObject.sourceOrderNameSelected != null ? _providerUnitObject.sourceOrderNameSelected.kode : "",
                                    "orderProductDealerSpecification": {
                                        "isThirdParty": false,
                                        "thirdPartyTypeID": _providerUnitObject.thirdPartyTypeSelected != null ? _providerUnitObject.thirdPartyTypeSelected.kode : "",
                                        "thirdPartyID": _providerUnitObject.thirdPartySelected != null ? _providerUnitObject.thirdPartySelected.kode : "",
                                        "thirdPartyActivity": _providerUnitObject.activitiesModel != null ? _providerUnitObject.activitiesModel.kode : null,
                                        "sentradID": _preferences.getString("SentraD"), //_providerUnitObject.thirdPartySelected != null ? _providerUnitObject.thirdPartySelected.sentraId : "",
                                        "unitdID": _preferences.getString("UnitD"), //_providerUnitObject.thirdPartySelected != null ? _providerUnitObject.thirdPartySelected.unitId : "",
                                        "dealerMatrix": _providerUnitObject.matriksDealerSelected != null ? _providerUnitObject.matriksDealerSelected.kode : ""
                                    },
                                    "programID": _providerUnitObject.programSelected != null ? _providerUnitObject.programSelected.kode : "",
                                    "rehabType": _providerUnitObject.rehabTypeSelected != null ? _providerUnitObject.rehabTypeSelected.id : "",
                                    "referenceNumber": _providerUnitObject.referenceNumberModel != null ? _providerUnitObject.referenceNumberModel.noPK : null,
                                    "applicationUnitContractNumber": null,
                                    "orderProductSaleses": _orderProductSaleses,
                                    "orderKaroseris": _orderKaroseris,
                                    "orderProductInsurances": _orderProductInsurances,
                                    "orderWmps": _orderWmps,
                                    "groupID": _providerUnitObject.grupIdSelected != null ? _providerUnitObject.grupIdSelected.kode : null
                                },
                                "orderCreditStructure": {
                                    "installmentTypeID": _providerCreditStructure.installmentTypeSelected != null ? _providerCreditStructure.installmentTypeSelected.id : "", // sebelumnya ngambil by kode
                                    "tenor": _providerCreditStructure.periodOfTimeSelected != null ? int.parse(_providerCreditStructure.periodOfTimeSelected) : "",
                                    "paymentMethodID": _providerCreditStructure.paymentMethodSelected != null ? _providerCreditStructure.paymentMethodSelected.id : "", // sebelumnya ngambil by kode
                                    "effectiveRate": _providerCreditStructure.controllerInterestRateEffective.text != "" ? double.parse(_providerCreditStructure.controllerInterestRateEffective.text) : "",
                                    "flatRate": _providerCreditStructure.controllerInterestRateFlat.text != "" ? double.parse(_providerCreditStructure.controllerInterestRateFlat.text) : "",//0.48501
                                    "objectPrice": _providerCreditStructure.controllerObjectPrice.text != "" ? double.parse(_providerCreditStructure.controllerObjectPrice.text.replaceAll(",", "")) : 0,
                                    "karoseriTotalPrice": _providerCreditStructure.controllerKaroseriTotalPrice.text != "" ? double.parse(_providerCreditStructure.controllerKaroseriTotalPrice.text.replaceAll(",", "")) : 0,
                                    "totalPrice": _providerCreditStructure.controllerTotalPrice.text != "" ? double.parse(_providerCreditStructure.controllerTotalPrice.text.replaceAll(",", "")) : 0,
                                    "nettDownPayment": _providerCreditStructure.controllerNetDP.text != "" ? double.parse(_providerCreditStructure.controllerNetDP.text.replaceAll(",", "")) : 0,
                                    "declineNInstallment": _providerCreditStructure.installmentDeclineN,
                                    "paymentOfYear": _providerCreditStructure.installmentTypeSelected != null ? _providerCreditStructure.installmentTypeSelected.id == "04" ? int.parse(_providerCreditStructure.controllerPaymentPerYear.text) : 1 : 1,
                                    "branchDownPayment": _providerCreditStructure.controllerBranchDP.text != "" ? double.parse(_providerCreditStructure.controllerBranchDP.text.replaceAll(",", "")) : 0,
                                    "grossDownPayment": _providerCreditStructure.controllerGrossDP.text != "" ? double.parse(_providerCreditStructure.controllerGrossDP.text.replaceAll(",", "")) : 0,
                                    "totalLoan": _providerCreditStructure.controllerTotalLoan.text != "" ? double.parse(_providerCreditStructure.controllerTotalLoan.text.replaceAll(",", "")) : 0,
                                    "installmentAmount": _providerCreditStructure.controllerInstallment.text != "" ? double.parse(_providerCreditStructure.controllerInstallment.text.replaceAll(",", "")) : 0,
                                    "ltvRatio": _providerCreditStructure.controllerLTV.text != "" ? double.parse(_providerCreditStructure.controllerLTV.text.replaceAll(",", "")) : 0.0,
                                    "pencairan": 0,
                                    "interestAmount": _providerCreditStructure.interestAmount,
                                    "gpType": null,
                                    "newTenor": 0,
                                    "totalStepping": 0,
                                    "installmentBalloonPayment": {
                                        "balloonType": _providerCreditStructureType.radioValueBalloonPHOrBalloonInstallment,
                                        "lastInstallmentPercentage": _providerCreditStructureType.controllerInstallmentPercentage.text != "" ? double.parse(_providerCreditStructureType.controllerInstallmentPercentage.text.replaceAll(",", "")) : 0,
                                        "lastInstallmentValue": _providerCreditStructureType.controllerInstallmentValue.text != "" ? double.parse(_providerCreditStructureType.controllerInstallmentValue.text.replaceAll(",", "")) : 0
                                    },
                                    "installmentSchedule": {
                                        "installmentType": _providerCreditStructure.installmentTypeSelected != null ? _providerCreditStructure.installmentTypeSelected.name : null, // OLD: ambil dari ID
                                        "paymentMethodType": _providerCreditStructure.paymentMethodSelected != null ? _providerCreditStructure.paymentMethodSelected.name : null,  // OLD: ambil dari ID
                                        "installment": _providerCreditStructure.controllerInstallment.text != "" ? double.parse(_providerCreditStructure.controllerInstallment.text.replaceAll(",", "")) : 0,
                                        "installmentRound": 0,
                                        "installmentMethod": 0,
                                        "lastKnownOutstanding": 0,
                                        "minInterest": 0,
                                        "maxInterest": 0,
                                        "futureValue": 0,
                                        "isInstallment": false,
                                        "roundingValue": null,
                                        "balloonInstallment": 0,
                                        "gracePeriode": 0,
                                        "gracePeriodes": [],
                                        "seasonal": 0,
                                        "steppings": [],
                                        "installmentScheduleDetails": []
                                    },
                                    "dsr": _providerCreditIncome.controllerDebtComparison.text != "" ? double.parse(_providerCreditIncome.controllerDebtComparison.text.replaceAll(",", "")) : 0,
                                    "dir": _providerCreditIncome.controllerIncomeComparison.text != "" ? double.parse(_providerCreditIncome.controllerIncomeComparison.text.replaceAll(",", "")) : 0,
                                    "dsc": _providerCreditIncome.controllerDSC.text != "" ? double.parse(_providerCreditIncome.controllerDSC.text.replaceAll(",", "")) : 0.0,
                                    "irr": _providerCreditIncome.controllerIRR.text != "" ? double.parse(_providerCreditIncome.controllerIRR.text.replaceAll(",", "")) : 0.0,
                                    "installmentDetails": _installmentDetails,
                                    "uangMukaKaroseri": 0, // diset 0
                                    "uangMukaChasisNet": _providerCreditStructure.controllerNetDP.text != "" ? double.parse(_providerCreditStructure.controllerNetDP.text.replaceAll(",", "")) : 0, // set sama kaya nettDownPayment
                                    "uangMukaChasisGross": _providerCreditStructure.controllerGrossDP.text != "" ? double.parse(_providerCreditStructure.controllerGrossDP.text.replaceAll(",", "")) : 0, // set sama kaya grossDownPayment
                                    // [
                                    //   {
                                    //     "installmentID": null,
                                    //     "installmentNumber": 0,
                                    //     "percentage": 0,
                                    //     "amount": 0,
                                    //     "creationalSpecification": {
                                    //       "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                                    //       "createdBy": "10056030",
                                    //       "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                                    //       "modifiedBy": "10056030"
                                    //     },
                                    //     "status": "ACTIVE"
                                    //   }
                                    // ],
                                    "realDSR": 0 // di jadikan model dari get dsr
                                },
                                "orderFees": _orderFees,
                                "orderSubsidies": _orderSubsidies,
                                "orderProductAdditionalSpecification": {
                                    "virtualAccountID": null,
                                    "virtualAccountValue": 0,
                                    "cashingPurposeID": null,
                                    "cashingTypeID": null
                                },
                                "creationalSpecification": {
                                    "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                                    "createdBy": _preferences.getString("username"),
                                    "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                                    "modifiedBy": _preferences.getString("username"),
                                },
                                "status": "ACTIVE",
                                "collateralAutomotives": _providerKolateral.collateralTypeModel.id == "001"
                                    ?
                                [
                                    {
                                        "collateralID": _preferences.getString("last_known_state") != "IDE" ? _providerKolateral.collateralID : "CAU001", // NEW | _providerKolateral.collateralTypeModel != null ? _providerKolateral.collateralTypeModel.id : "",
                                        "isCollaSameUnit": _providerKolateral.radioValueIsCollateralSameWithUnitOto, // Jika Collateral = Unit dipilih YA, maka kirim TRUE. Begitu juga sebaliknya (0 = YA/true | 1 = TIDAK/false)
                                        "isMultiUnitCollateral": _providerKolateral.radioValueForAllUnitOto, // ya = false = 0 | tidak = true = 1
                                        "isCollaNameSameWithCustomerName": _providerKolateral.radioValueIsCollaNameSameWithApplicantOto, // Jika Nama Jaminan = Pemohon dipilih YA, maka kirim TRUE. Begitu juga sebaliknya (0 = YA/true | 1 = TIDAK/false)
                                        "identitySpecification": {
                                            "identityType": _providerKolateral.identityTypeSelectedAuto != null ? _providerKolateral.identityTypeSelectedAuto.id : "",
                                            "identityNumber": _providerKolateral.controllerIdentityNumberAuto.text != "" ? _providerKolateral.controllerIdentityNumberAuto.text : "",
                                            "identityName": _providerKolateral.controllerNameOnCollateralAuto.text, // _providerKolateral.radioValueIsCollaNameSameWithApplicantOto == 0 ? _providerInfoNasabah.controllerNamaLengkapSesuaiIdentitas.text : _providerKolateral.controllerNameOnCollateralAuto.text,
                                            "fullName": _providerKolateral.controllerNameOnCollateralAuto.text, // _providerKolateral.radioValueIsCollaNameSameWithApplicantOto == 0 ? _providerInfoNasabah.controllerNamaLengkap.text : _providerKolateral.controllerNameOnCollateralAuto.text,
                                            "alias": null,
                                            "title": null,
                                            "dateOfBirth": _providerKolateral.controllerBirthDateAuto.text != "" ? _providerKolateral.controllerBirthDateAuto.text+" 00:00:00" : null,
                                            "placeOfBirth": _providerKolateral.controllerBirthPlaceValidWithIdentityAuto.text != "" ? _providerKolateral.controllerBirthPlaceValidWithIdentityAuto.text : "",
                                            "placeOfBirthKabKota": null, //_providerKolateral.radioValueIsCollaNameSameWithApplicantOto == 0 ? _providerInfoNasabah.birthPlaceSelected.KABKOT_ID : _providerKolateral.birthPlaceAutoSelected != null ? _providerKolateral.birthPlaceAutoSelected.KABKOT_ID : null,
                                            "gender": null, // _providerKolateral.radioValueIsCollaNameSameWithApplicantOto == 0 ? _providerInfoNasabah.radioValueGender : null
                                            "identityActiveStart": null,
                                            "identityActiveEnd": null,
                                            "religion": null , // _providerKolateral.radioValueIsCollaNameSameWithApplicantOto == 0 ? _providerInfoNasabah.religionSelected == null ? null : _providerInfoNasabah.religionSelected.id : null,
                                            "occupationID": null,//_providerFoto.occupationSelected != null ? _providerFoto.occupationSelected.KODE : "",
                                            "positionID": null
                                        },
                                        "collateralAutomotiveSpecification": {
                                            "orderUnitSpecification": {
                                                "objectGroupID": _providerKolateral.groupObjectSelected != null ? _providerKolateral.groupObjectSelected.KODE : null,
                                                "objectID": _providerKolateral.objectSelected != null ? _providerKolateral.objectSelected.id : null,
                                                "productTypeID": _providerKolateral.productTypeSelected != null ? _providerKolateral.productTypeSelected.id : null,
                                                "objectBrandID":  _providerKolateral.brandObjectSelected != null ? _providerKolateral.brandObjectSelected.id : null,
                                                "objectTypeID": _providerKolateral.objectTypeSelected != null ? _providerKolateral.objectTypeSelected.id : null,
                                                "objectModelID": _providerKolateral.modelObjectSelected != null ? _providerKolateral.modelObjectSelected.id : null,
                                                "objectUsageID":  "", // tidak dipakai
                                                "objectPurposeID": _providerKolateral.objectUsageSelected != null ? _providerKolateral.objectUsageSelected.id : null, // field tujuan penggunaan collateral
                                            },
                                            "mpAdiraUpld": _providerKolateral.controllerMPAdiraUpload.text != "" ? double.parse(_providerKolateral.controllerMPAdiraUpload.text.replaceAll(",", "")) : null,
                                            "productionYear": _providerKolateral.yearProductionSelected != null ? int.parse(_providerKolateral.yearProductionSelected) : "",
                                            "registrationYear": _providerKolateral.yearRegistrationSelected != null ? int.parse(_providerKolateral.yearRegistrationSelected) : "",
                                            "isYellowPlate": _providerKolateral.radioValueYellowPlat, // per tanggal 10.05.2021 = dirubah ga dirubah jadi true false (OLD: == 0 ? false : true)
                                            "isBuiltUp": _providerKolateral.radioValueBuiltUpNonATPM == 0 ? true : false,
                                            "utjSpecification": {
                                                "bpkbNumber": _providerKolateral.controllerBPKPNumber.text != "" ? _providerKolateral.controllerBPKPNumber.text : null,
                                                "chassisNumber":  _providerKolateral.controllerFrameNumber.text != "" ? _providerKolateral.controllerFrameNumber.text : null,
                                                "engineNumber": _providerKolateral.controllerMachineNumber.text != "" ? _providerKolateral.controllerMachineNumber.text : null,
                                                "policeNumber": _providerKolateral.controllerPoliceNumber.text != "" ? _providerKolateral.controllerPoliceNumber.text : null,
                                            },
                                            "gradeUnit": _providerKolateral.controllerGradeUnit.text != "" ? _providerKolateral.controllerGradeUnit.text : null,
                                            "utjFacility": _providerKolateral.controllerFasilitasUTJ.text != "" ? _providerKolateral.controllerFasilitasUTJ.text == "TUNAI" ? "1" : "0" : null,
                                            "bidderName": _providerKolateral.controllerNamaBidder.text != "" ? _providerKolateral.controllerNamaBidder.text : null,
                                            "showroomPrice": _providerKolateral.controllerHargaJualShowroom.text != "" ? double.parse(_providerKolateral.controllerHargaJualShowroom.text.replaceAll(",", "")).round() : "",
                                            "additionalEquipment": _providerKolateral.controllerPerlengkapanTambahan.text != "" ? _providerKolateral.controllerPerlengkapanTambahan.text : "",
                                            "mpAdira":  _providerKolateral.controllerMPAdira.text != "" ? double.parse(_providerKolateral.controllerMPAdira.text.replaceAll(",", "")) : null,
                                            "physicalRecondition": _providerKolateral.controllerRekondisiFisik.text != "" ? double.parse(_providerKolateral.controllerRekondisiFisik.text.replaceAll(",", "")) : 0,
                                            "isProper": _providerKolateral.radioValueWorthyOrUnworthy, // 0 = layak = false | 1 = tidak layak = true
                                            "ltvRatio": 0.0,
                                            "appraisalSpecification": {
                                                "collateralDp": double.parse(_providerKolateral.controllerDPJaminan.text == '' ? '0' : _providerKolateral.controllerDPJaminan.text.replaceAll(",", "")),
                                                "maximumPh": double.parse(_providerKolateral.controllerPHMaxAutomotive.text == '' ? '0' : _providerKolateral.controllerPHMaxAutomotive.text.replaceAll(",", "")),
                                                "appraisalPrice": double.parse(_providerKolateral.controllerTaksasiPriceAutomotive.text == '' ? '0' : _providerKolateral.controllerTaksasiPriceAutomotive.text.replaceAll(",", "")),
                                                "collateralUtilizationID": _providerKolateral.collateralUsageOtoSelected != null ? _providerKolateral.collateralUsageOtoSelected.id : "" // field tujuan penggunaan collateral
                                            },
                                            "collateralAutomotiveAdditionalSpecification": {
                                                "capacity": 0,
                                                "color": null,
                                                "manufacturer": null,
                                                "serialNumber": null,
                                                "invoiceNumber": null,
                                                "stnkActiveDate": null,
                                                "bpkbAddress": null,
                                                "bpkbIdentityTypeID": null
                                            },
                                        },
                                        "status": "ACTIVE",
                                        "creationalSpecification": {
                                            "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                                            "createdBy": _preferences.getString("username"),
                                            "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                                            "modifiedBy": _preferences.getString("username"),
                                        }
                                    }
                                ] : [],
                                "collateralProperties": _providerKolateral.collateralTypeModel.id == "002"
                                    ?
                                [
                                    {
                                        "isMultiUnitCollateral": true, // _providerKolateral.radioValueForAllUnitOto, // ya = false = 0 | tidak = true = 1
                                        "isCollaNameSameWithCustomerName": _providerKolateral.radioValueIsCollateralSameWithApplicantProperty == 0,
                                        "collateralID": _preferences.getString("last_known_state") != "IDE" ? _providerKolateral.collateralID : "COLP001", // NEW
                                        "identitySpecification": {
                                            "identityType": _providerKolateral.identityModel != null ? _providerKolateral.identityModel.id : "",
                                            "identityNumber": _providerKolateral.controllerIdentityNumber.text,
                                            "identityName": null,
                                            "fullName": _providerKolateral.controllerNameOnCollateral.text,
                                            "alias": null,
                                            "title": null,
                                            "dateOfBirth": _providerKolateral.controllerBirthDateProp.text != "" ? _providerKolateral.controllerBirthDateProp.text : null,
                                            "placeOfBirth": _providerKolateral.birthPlaceSelected.KABKOT_NAME,
                                            "placeOfBirthKabKota": _providerKolateral.birthPlaceSelected.KABKOT_ID,
                                            "gender": null,
                                            "identityActiveStart": null,
                                            "identityActiveEnd": null,
                                            "isLifetime": null,
                                            "religion": null,
                                            "occupationID": null,
                                            "positionID": null,
                                            "maritalStatusID": null
                                        },
                                        "detailAddresses": [
                                            {
                                                "addressID": _preferences.getString("last_known_state") != "IDE" ? _providerKolateral.addressID != "NEW" ? _providerKolateral.addressID : "NEW" : "NEW",
                                                "foreignBusinessID": _preferences.getString("last_known_state") != "IDE" ? _providerKolateral.foreignBusinessID != "NEW" ? _providerKolateral.foreignBusinessID : "CAU001" : "CAU001", // NEW
                                                "addressSpecification": {
                                                    "koresponden": _providerKolateral.isKorespondensi.toString(), // tanya oji
                                                    "matrixAddr": _preferences.getString("cust_type") == "PER" ? "6" : "13",
                                                    "address": _providerKolateral.controllerAddress.text,
                                                    "rt": _providerKolateral.controllerRT.text,
                                                    "rw": _providerKolateral.controllerRW.text,
                                                    "provinsiID": _providerKolateral.kelurahanSelected.PROV_ID,
                                                    "kabkotID": _providerKolateral.kelurahanSelected.KABKOT_ID,
                                                    "kecamatanID": _providerKolateral.kelurahanSelected.KEC_ID,
                                                    "kelurahanID": _providerKolateral.kelurahanSelected.KEL_ID,
                                                    "zipcode": _providerKolateral.kelurahanSelected.ZIPCODE
                                                },
                                                "contactSpecification": {
                                                    "telephoneArea1": null,
                                                    "telephone1": null,
                                                    "telephoneArea2": null,
                                                    "telephone2": null,
                                                    "faxArea": null,
                                                    "fax": null,
                                                    "handphone": null,
                                                    "email": null,
                                                    "noWa": null,
                                                    "noWa2": null,
                                                    "noWa3": null
                                                },
                                                "addressType": _providerKolateral.addressTypeSelected.KODE,
                                                "creationalSpecification": {
                                                    "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                                                    "createdBy": _preferences.getString("username"),
                                                    "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                                                    "modifiedBy": _preferences.getString("username"),
                                                },
                                                "status": "ACTIVE"
                                            }
                                        ],
                                        "collateralPropertySpecification": {
                                            "certificateNumber": _providerKolateral.controllerCertificateNumber.text,
                                            "certificateTypeID": _providerKolateral.certificateTypeSelected.id,
                                            "propertyTypeID": _providerKolateral.propertyTypeSelected.id,
                                            "buildingArea": double.parse(_providerKolateral.controllerBuildingArea.text.replaceAll(",", "")),
                                            "landArea": double.parse(_providerKolateral.controllerSurfaceArea.text.replaceAll(",", "")),
                                            "appraisalSpecification": {
                                                "collateralDp": double.parse(_providerKolateral.controllerDPJaminan.text.replaceAll(",", "")).toInt(),
                                                "maximumPh": double.parse(_providerKolateral.controllerPHMax.text == "" ? '0' : _providerKolateral.controllerPHMax.text.replaceAll(",", "")),
                                                "appraisalPrice": double.parse(_providerKolateral.controllerTaksasiPrice.text.replaceAll(",", "")).toInt(),
                                                "collateralUtilizationID": _providerKolateral.collateralUsagePropertySelected != null ? _providerKolateral.collateralUsagePropertySelected.id : "" //"" field tujuan penggunaan collateral
                                            },
                                            "positiveFasumDistance": double.parse(_providerKolateral.controllerJarakFasumPositif.text.replaceAll(",","")).toInt(),
                                            "negativeFasumDistance": double.parse(_providerKolateral.controllerJarakFasumNegatif.text.replaceAll(",","")).toInt(),
                                            "landPrice": double.parse(_providerKolateral.controllerHargaTanah.text.replaceAll(",","")).toInt(),
                                            "njopPrice": double.parse(_providerKolateral.controllerHargaNJOP.text.replaceAll(",","")).toInt(),
                                            "buildingPrice": double.parse(_providerKolateral.controllerHargaBangunan.text.replaceAll(",","")).toInt(),
                                            "roofTypeID": _providerKolateral.typeOfRoofSelected != null ? _providerKolateral.typeOfRoofSelected.id : null,
                                            "wallTypeID": _providerKolateral.wallTypeSelected != null ? _providerKolateral.wallTypeSelected.id : null,
                                            "floorTypeID": _providerKolateral.floorTypeSelected != null ? _providerKolateral.floorTypeSelected.id : null,
                                            "foundationTypeID": _providerKolateral.foundationTypeSelected != null ? _providerKolateral.foundationTypeSelected.id : null,
                                            "roadTypeID": _providerKolateral.streetTypeSelected != null ? _providerKolateral.streetTypeSelected.id : null,
                                            "isCarPassable": _providerKolateral.radioValueAccessCar == 0,
                                            "totalOfHouseInRadius": _providerKolateral.controllerJumlahRumahDalamRadius.text != "" ? int.parse(_providerKolateral.controllerJumlahRumahDalamRadius.text) : 0,
                                            "sifatJaminan": _providerKolateral.controllerSifatJaminan.text,
                                            "buktiKepemilikanTanah": _providerKolateral.controllerBuktiKepemilikan.text,
                                            "tgglTerbitSertifikat": _providerKolateral.controllerCertificateReleaseDate.text != "" ? formatDateValidateAndSubmit(_providerKolateral.initialDateCertificateRelease) : "",
                                            "tahunTerbitSertifikat": _providerKolateral.controllerCertificateReleaseYear.text,
                                            "namaPemegangHak": _providerKolateral.controllerNamaPemegangHak.text,
                                            "noSuratUkurGmbrSituasi": _providerKolateral.controllerNoSuratUkur.text,
                                            "tgglSuratUkurGmbrSituasi": _providerKolateral.controllerDateOfMeasuringLetter.text != "" ? formatDateValidateAndSubmit(_providerKolateral.initialDateOfMeasuringLetter) : "",
                                            "sertifikatSuratUkurDikeluarkanOleh": _providerKolateral.controllerCertificateOfMeasuringLetter.text,
                                            "masaBerlakuHak": _providerKolateral.controllerMasaHakBerlaku.text,
                                            "noImb": _providerKolateral.controllerNoIMB.text,
                                            "tgglImb": _providerKolateral.controllerDateOfIMB.text != "" ? formatDateValidateAndSubmit(_providerKolateral.initialDateOfIMB) : "",
                                            "luasBangunanImb": double.parse(_providerKolateral.controllerLuasBangunanIMB.text.replaceAll(",", "")),
                                            "ltvRatio": _providerKolateral.controllerLTV.text != "" ? int.parse(_providerKolateral.controllerLTV.text) : 0,
                                            "letakTanah": null,
                                            "propertyGroupObjtID": null,
                                            "propertyObjtID": null
                                        },
                                        "status": "INACTIVE",
                                        "creationalSpecification": {
                                            "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                                            "createdBy": _preferences.getString("username"),
                                            "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                                            "modifiedBy": _preferences.getString("username"),
                                        }
                                    }
                                ]
                                    :
                                [],
                                "orderProductProcessingCompletion": {
                                    "lastKnownOrderProductState": "APPLICATION_DOCUMENT",
                                    "nextKnownOrderProductState": "APPLICATION_DOCUMENT"
                                },
                                "isWithoutColla": _providerKolateral.collateralTypeModel.id == "003" ? 1 : 0,// _providerUnitObject.groupObjectSelected.KODE == "003" ? 1 : 0,
                                "isSaveKaroseri": false
                            }
                        ],
                        "orderProcessingCompletion": {
                            "calculatedAt": 0,
                            "lastKnownOrderState": null,
                            "lastKnownOrderStatePosition": null,
                            "lastKnownOrderStatus": null,
                            "lastKnownOrderHandledBy": null
                        },
                        "creationalSpecification": {
                            "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                            "createdBy": _preferences.getString("username"),
                            "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                            "modifiedBy": _preferences.getString("username"),
                        },
                        "status": "ACTIVE"
                    },
                    "surveyDTO": {
                        "surveyID": _dataResultSurvey[0]['surveyID'], // _providerSurvey.surveyID,
                        "orderID": _providerApp.applNo,
                        "surveyType": "002",
                        "employeeID": _preferences.getString("username"),
                        "phoneNumber": null,
                        "janjiSurveyDate": formatDateValidateAndSubmit(_providerApp.initialSurveyDate),
                        "reason": "",
                        "flagBRMS": "BRMS",
                        "surveyStatus": "01",
                        "status": "ACTIVE",
                        "surveyProcess": "02",
                        "surveyResultSpecification": {
                            "surveyResultType": _providerSurvey.resultSurveySelected != null ? _providerSurvey.resultSurveySelected.KODE : "",
                            "recomendationType": _providerSurvey.recommendationSurveySelected != null ? _providerSurvey.recommendationSurveySelected.KODE : null,
                            "notes": _providerSurvey.controllerNote.text != "" ? _providerSurvey.controllerNote.text : "",
                            "surveyResultDate": _providerSurvey.controllerResultSurveyDate.text.isNotEmpty ? "${_providerSurvey.controllerResultSurveyDate.text} ${_providerSurvey.controllerResultSurveyTime.text}:00" : formatDateValidateAndSubmit(DateTime.now()), //formatDateValidateAndSubmit(_providerSurvey.initialDateResultSurvey),
                            "distanceWithSentra": _providerSurvey.controllerDistanceLocationWithSentraUnit.text != "" ? double.parse(_providerSurvey.controllerDistanceLocationWithSentraUnit.text.replaceAll(",", "")).toInt() : 0,
                            "distanceWithDealer": _providerSurvey.controllerDistanceLocationWithDealerMerchantAXI.text != "" ? double.parse(_providerSurvey.controllerDistanceLocationWithDealerMerchantAXI.text.replaceAll(",", "")).toInt() : 0,
                            "distanceObjWithSentra": _providerSurvey.controllerDistanceLocationWithUsageObjectWithSentraUnit.text != "" ? double.parse(_providerSurvey.controllerDistanceLocationWithUsageObjectWithSentraUnit.text.replaceAll(",", "")).toInt() : 0,
                            "surveyResultAssets": _surveyResultAssets,
                            "surveyResultDetails": _surveyResultDetails,
                            "surveyResultTelesurveys": []
                        },
                        "creationalSpecification": {
                            "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                            "createdBy": _preferences.getString("username"),
                            "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                            "modifiedBy": _preferences.getString("username")
                        },
                        "jobSurveyor": ""
                    },
                    "userCredentialMS2": {
                        "nik": _preferences.getString("username"),
                        "branchCode": _preferences.getString("branchid"),
                        "fullName": _preferences.getString("fullname"),
                        "role": _preferences.getString("job_name")
                    }
                },
                "applicationDTOReason": {
                    "customerDTOReason": {
                        "customerIDReason": null,
                        "customerObligorIDReason": null,
                        "customerTypeReason": null,
                        "customerSpecificationFormNumberReason": null,
                        "customerNpwpSpecificationHasNpwpReason": null,
                        "customerNpwpSpecificationNpwpAddressReason": _providerRincian.isAddressNPWPDakor ? "99" : null,
                        "customerNpwpSpecificationNpwpFullnameReason": _providerRincian.isNameNPWPDakor ? "99" : null,
                        "customerNpwpSpecificationNpwpNumberReason": _providerRincian.isNoNPWPDakor ? "99" : null,
                        "customerNpwpSpecificationNpwpTypeReason": _providerRincian.isTypeNPWPDakor ? "99" : null,
                        "customerNpwpSpecificationPkpIdentifierReason": _providerRincian.isPKPSignDakor ? "99" : null,
                        "customerDetailAddressesReason": [
                            {
                                "addressIDReason": null,
                                "addressForeignBusinessIDReason": null,
                                "addressSpecificationKorespondenReason": null,
                                "addressSpecificationMatrixAddrReason": null,
                                "addressSpecificationAddressReason": null,
                                "addressSpecificationRtReason": null,
                                "addressSpecificationRwReason": null,
                                "addressSpecificationProvinsiIDReason": null,
                                "addressSpecificationKabkotIDReason": null,
                                "addressSpecificationKecamatanIDReason": null,
                                "addressSpecificationKelurahanIDReason": null,
                                "addressSpecificationZipcodeReason": null,
                                "addressContactSpecificationTelephoneArea1Reason": null,
                                "addressContactSpecificationTelephone1Reason": null,
                                "addressContactSpecificationTelephoneArea2Reason": null,
                                "addressContactSpecificationTelephone2Reason": null,
                                "addressContactSpecificationFaxAreaReason": null,
                                "addressContactSpecificationFaxReason": null,
                                "addressContactSpecificationHandphoneReason": null,
                                "addressContactSpecificationEmailReason": null,
                                "addressTypeReason": null,
                                "addressCreationalSpecificationCreatedAtReason": null,
                                "addressCreationalSpecificationCreatedByReason": null,
                                "addressCreationalSpecificationModifiedAtReason": null,
                                "addressCreationalSpecificationModifiedByReason": null,
                                "addressStatusReason": null,
                                "addressFlagCorrectionReason": null
                            },
                        ],
                        "customerGuarantorSpecificationIsGuarantorReason": null,
                        "customerGuarantorCorporatesReason": _customerGuarantorCorporatesReason,
                        "customerGuarantorIndividualsReason": _customerGuarantorIndividualsReason,
                        "customerIndividualReason": [],
                        // [
                        //     {
                        //         "customerIndividualIDReason": null,
                        //         "customerIndividualNumberOfDependentsReason": _providerInfoNasabah.isJumlahTanggunganChanges ? "99" : null,
                        //         "customerIndividualGroupCustomerReason": _providerInfoNasabah.isGCChanges ? "99" : null,
                        //         "customerIndividualStatusRelationshipReason": _providerInfoNasabah.isMaritalStatusSelectedChanges ? "99" : null,
                        //         "customerIndividualEducationIDReason": _providerInfoNasabah.isEducationSelectedChanges ? "99" : null,
                        //         "customerIndividualIdentityIdentityTypeReason": _providerInfoNasabah.isIdentitasModelChanges ? "99" : null,
                        //         "customerIndividualIdentityIdentityNumberReason": _providerInfoNasabah.isNoIdentitasChanges ? "99" : null,
                        //         "customerIndividualIdentityIdentityNameReason": _providerInfoNasabah.isNamaLengkapSesuaiIdentitasChanges ? "99" : null,
                        //         "customerIndividualIdentityFullNameReason": _providerInfoNasabah.isNamaLengkapChanges ? "99" : null,
                        //         "customerIndividualIdentityAliasReason": null,
                        //         "customerIndividualIdentityTitleReason": null,
                        //         "customerIndividualIdentityDateOfBirthReason": _providerInfoNasabah.isTglLahirChanges ? "99" : null,
                        //         "customerIndividualIdentityPlaceOfBirthReason": _providerInfoNasabah.isTempatLahirSesuaiIdentitasChanges ? "99" : null,
                        //         "customerIndividualIdentityPlaceOfBirthKabKotaReason": _providerInfoNasabah.isTempatLahirSesuaiIdentitasLOVChanges ? "99" : null,
                        //         "customerIndividualIdentityGenderReason": _providerInfoNasabah.isGenderChanges ? "99" : null,
                        //         "customerIndividualIdentityIdentityActiveStartReason": null,
                        //         "customerIndividualIdentityIdentityActiveEndReason": null,
                        //         "customerIndividualIdentityReligionReason": null,
                        //         "customerIndividualIdentityOccupationIDReason": null,
                        //         "customerIndividualIdentityPositionIDReason": null,
                        //         "customerIndividualIdentityisLifetimeReason": null,
                        //         "customerIndividualContactTelephoneArea1Reason": null,
                        //         "customerIndividualContactTelephone1Reason": null,
                        //         "customerIndividualContactTelephoneArea2Reason": null,
                        //         "customerIndividualContactTelephone2Reason": null,
                        //         "customerIndividualContactFaxAreaReason": null,
                        //         "customerIndividualContactFaxReason": null,
                        //         "customerIndividualContactHandphoneReason": _providerInfoNasabah.isNoHp1WAChanges ? "99" : null,
                        //         "customerIndividualContactEmailReason": null,
                        //         "noWaReason": null,
                        //         "noWa2Reason": null,
                        //         "noWa3Reason": null,
                        //         "customerOccupationsReason": [
                        //             // {
                        //             //     "customerIndividualCustomerOccupationIDReason": null,
                        //             //     "customerIndividualOccupationIDReason": null,
                        //             //     "customerIndividualOccupationBusinessNameReason": _providerFoto.occupationSelected.KODE == "05" || _providerFoto.occupationSelected.KODE == "07" ? _providerOccupation.editNamaUsahaWiraswasta :_providerOccupation.editNamaPerusahaanLainnya,
                        //             //     "customerIndividualOccupationBusinessTypeReason": null,//_customerIndividualOccupationBusinessTypeReason,
                        //             //     "customerIndividualOccupationProfessionTypeReason": _providerOccupation.editJenisProfesiProfesional ? "99" : null,
                        //             //     "customerIndividualOccupationPepTypeReason": _providerOccupation.editJenisPepHighRiskLainnya ? "99" : null,
                        //             //     "customerIndividualOccupationEmployeeStatusReason": _providerOccupation.editTotalPegawaiLainnya ? "99" : null,
                        //             //     "customerIndividualOccupationBusinessEconomySectorReason": null,//_customerIndividualOccupationBusinessEconomySectorReason,
                        //             //     "customerIndividualOccupationBusinessBusinessFieldReason": _customerIndividualOccupationBusinessBusinessFieldReason,
                        //             //     "customerIndividualOccupationBusinessLocationStatusReason": _customerIndividualOccupationBusinessLocationStatusReason,
                        //             //     "customerIndividualOccupationBusinessBusinessLocationReason": _customerIndividualOccupationBusinessBusinessLocationReason,
                        //             //     "customerIndividualOccupationBusinessEmployeeTotalReason": _customerIndividualOccupationBusinessEmployeeTotalReason,
                        //             //     "customerIndividualOccupationBusinessBussinessLengthInMonthReason": _providerOccupation.editLamaBekerjaBerjalan ? "99" : null,
                        //             //     "customerIndividualOccupationBusinessTotalBussinessLengthInMonthReason": _customerIndividualOccupationBusinessTotalBussinessLengthInMonthReason,
                        //             //     "customerIndividualOccupationCreationalCreatedAtReason": null,
                        //             //     "customerIndividualOccupationCreationalCreatedByReason": null,
                        //             //     "customerIndividualOccupationCreationalModifiedAtReason": null,
                        //             //     "customerIndividualOccupationCreationalModifiedByReason": null,
                        //             //     "customerIndividualOccupationStatusReason": null,
                        //             //     "customerOccupationFlagCorrectionReason": null
                        //             // }
                        //         ],
                        //         "customerIndividualNearFutureNeedIDsReason": [],
                        //         "custIndividualFacebookAccountReason": null,
                        //         "custIndividualBlackBerryPinReason": null,
                        //         "customerIndividualHobbyIDsReason": [],
                        //         "customerIndividualParticipantIDsReason": [],
                        //         "custIndividualFamilyCardNumberReason": null,
                        //         "custIndividualFavoriteColorReason": null,
                        //         "custIndividualFavoriteBrandReason": null,
                        //         "custIndividualEmergencyRelationshipTypeIDReason": null,
                        //         "custIndividualEmergencyIdentityFullNameReason": null,
                        //         "custIndividualEmergencyFullNameReason": null,
                        //         "custIndividualEmergencyTitleReason": null,
                        //         "custIndividualEmergencyEmailReason": null,
                        //         "custIndividualEmergencyHandphoneNumberReason": null,
                        //         "customerIndividualCustomerCreditCardReason": [],
                        //         "custIndividualCustomerFamiliesReason": null,//_custIndividualCustomerFamiliesReason,
                        //         "customerIndividualStatusReason": null,
                        //         "customerIndividualCreatedAtReason": null,
                        //         "customerIndividualCreatedByReason": null,
                        //         "customerIndividualModifiedAtReason": null,
                        //         "customerIndividualModifiedByReason": null
                        //     }
                        // ],
                        "customerCorporateReason": [
                            {
                                "customerCorporateIDReason": null,
                                "customerCorporateEconomySectorReason": _providerRincian.isSectorEconomicDakor ? "99" : null,
                                "customerCorporateBusinessFieldReason": _providerRincian.isBusinessFieldDakor ? "99" : null,
                                "customerCorporateLocationStatusReason": _providerRincian.isLocationStatusDakor ? "99" : null,
                                "customerCorporateBusinessLocationReason": _providerRincian.isBusinessLocationDakor ? "99" : null,
                                "customerCorporateEmployeeTotalReason": _providerRincian.isTotalEmpDakor ? "99" : null,
                                "customerCorporateBussinessLengthInMonthReason": _providerRincian.isLamaUsahaDakor ? "99" : null,
                                "customerCorporateTotalBussinessLengthInMonthReason": _providerRincian.isTotalLamaUsahaDakor ? "99" : null,
                                "customerCorporateBusinessPermitInstitutionTypeReason": _providerRincian.isCompanyTypeDakor ? "99" : null,
                                "customerCorporateBusinessPermitProfileReason": _providerRincian.isProfileDakor ? "99" : null,
                                "customerCorporateBusinessPermitFullNameReason":  _providerRincian.isCompanyNameDakor ? "99" : null,
                                "customerCorporateBusinessPermitDeedOfIncorporationNumberReason": null,
                                "customerCorporateBusinessPermitDeedEndDateReason": null,
                                "customerCorporateBusinessPermitSiupNumberReason": null,
                                "customerCorporateBusinessPermitSiupStartDateReason": null,
                                "customerCorporateBusinessPermitTdpNumberReason": null,
                                "customerCorporateBusinessPermitTdpStartDateReason": null,
                                "customerCorporateBusinessPermitEstablishmentDateReason": _providerRincian.isEstablishDateDakor ? "99" : null,
                                "customerCorporateBusinessPermitIsCompanyReason": null,
                                "customerCorporateBusinessPermitAuthorizedCapitalReason": null,
                                "customerCorporateBusinessPermitPaidCapitalReason": null,
                                "customerCorporatePicIdentityTypeReason": _providerManajemenPIC.identityTypeDakor ? "99" : null,
                                "customerCorporatePicIdentityNumberReason": _providerManajemenPIC.identityNoDakor ? "99" : null,
                                "customerCorporatePicIdentityNameReason": _providerManajemenPIC.fullnameDakor ? "99" : null,
                                "customerCorporatePicIdentityFullNameReason": _providerManajemenPIC.fullnameIdDakor ?  "99" : null,
                                "customerCorporatePicIdentitAliasReason": null,
                                "customerCorporatePicIdentityTitleReason": null,
                                "customerCorporatePicIdentityDateOfBirthReason": _providerManajemenPIC.dateOfBirthDakor ? "99" : null,
                                "customerCorporatePicIdentityPlaceOfBirthReason": _providerManajemenPIC.placeOfBirthDakor ? "99" : null,
                                "customerCorporatePicIdentityPlaceOfBirthKabKotaReason": _providerManajemenPIC.placeOfBirthLOVDakor ? "99" : null,
                                "customerCorporatePicIdentityGenderReason": null,
                                "customerCorporatePicIdentityActiveStartReason": null,
                                "customerCorporatePicIdentityActiveEndReason": null,
                                "customerCorporatePicIdentitReligionReason": null,
                                "customerCorporatePicIdentitOccupationIDReason": null,
                                "customerCorporatePicIdentitPositionIDReason": _providerManajemenPIC.positionDakor ? "99" : null,
                                "customerCorporatePicIdentitMaritalStatusIDReason": null,
                                "customerCorporatePicContactTelephoneArea1Reason": null,
                                "customerCorporatePicContactTelephone1Reason": null,
                                "customerCorporatePicContactTelephoneArea2Reason": null,
                                "customerCorporatePicContactTelephone2Reason": null,
                                "customerCorporatePicContactFaxAreaReason": null,
                                "customerCorporatePicContactFaxReason": null,
                                "customerCorporatePicContactHandphoneReason": _providerManajemenPIC.phoneDakor ? "99" : null,
                                "customerCorporatePicContactEmailReason": null,
                                "customerCorporateShareholdingReason": null,
                                "customerCorporateShareholdersIndividualsReason": _customerCorporateShareholdersIndividualsReason,
                                "customerCorporateShareholdersCorporatesReason": _customerCorporateShareholdersCorporatesReason,
                                "customerCorporateDedupScoreReason": null,
                                "customerCorporateStatusReason": null,
                                "customerCorporateCreatedAtReason": null,
                                "customerCorporateCreatedByReason": null,
                                "customerCorporateModifiedAtReason": null,
                                "customerCorporateModifiedByReason": null,
                            }
                        ],
                        "exposureInformationsReason": [
                            {
                                "exposureInformationIDReason": null,
                                "exposureGroupObjectIDReason": null,
                                "exposureOsArReason": null,
                                "exposureArOnProcessReason": null,
                                "exposureTotalReason": null,
                                "exposureObligorIDReason": null,
                                "exposureArNewReason": null,
                                "exposureExposureOthersReason": null,
                                "exposureStatusReason": null,
                                "exposureCreatedAtReason": null,
                                "exposureCreatedByReason": null,
                                "exposureModifiedAtReason": null,
                                "exposureModifiedByReason": null,
                                "exposureInformationFlagCorrectionReason": null
                            }
                        ],
                        "customerIncomesReason": _customerIncomesReason,
                        "customerAdditionalCorrespondenceTypeIDReason": null,
                        "customerAdditionalDebtorL2CodeReason": null,
                        "customerAdditionalDebtorL3CodeReason": null,
                        "customerBankAccountsReason": [],
                        "customerProcessingLastKnownCustomerStateReason": null,
                        "customerProcessingNextKnownCustomerStateReason": null,
                        "customerCreatedAtReason": null,
                        "customerCreatedByReason": null,
                        "customerModifiedAtReason": null,
                        "customerModifiedByReason": null,
                        "customerStatusReason": null
                    },
                    "orderDTOReason": {
                        "orderIDReason": null,
                        "orderCustomerIDReason": null,
                        "orderSpecificationIsMayorReason": null,
                        "orderSpecificationHasFiduciaReason": null,
                        "orderSpecificationApplicationSourceReason": null,
                        "orderSpecificationOrderDateReason": _providerApp.isOrderDateChanges ? "99" : null,
                        "orderSpecificationApplicationDateReason": _providerApp.isOrderDateChanges ? "99" : null,
                        "orderSpecificationSurveyAppointmentDateReason": _providerApp.isSurveyAppointmentDateChanges ? "99" : null,
                        "orderSpecificationOrderTypeReason": null,
                        "orderSpecificationIsOpenAccountReason": null,
                        "orderSpecificationAccountFormNumberReason": null,
                        "orderSpecificationSentraCIDReason": null,
                        "orderSpecificationUnitCIDReason": null,
                        "orderSpecificationInitialRecommendationReason": null,
                        "orderSpecificationFinalRecommendationReason": null,
                        "orderSpecificationBrmsScoringReason": null,
                        "orderSpecificationIsInPlaceApprovalReason": null,
                        "orderSpecificationIsPkSignedReason": _providerApp.isIsSignedPKChanges ? "99" : null,
                        "orderSpecificationMaxApprovalLevelReason": null,
                        "orderSpecificationJenisKonsepReason": _providerApp.isConceptTypeModelChanges ? "99" : null,
                        "orderSpecificationJumlahObjekReason": _providerApp.isTotalObjectChanges ? "99" : null,
                        "orderSpecificationJenisProporsionalAsuransiReason": _providerApp.isPropotionalObjectChanges ? "99" : null,
                        "orderSpecificationUnitKeReason": _providerApp.isNumberOfUnitChanges ? "99" : null,
                        "orderSpecificationflagConfirmDasReason": null,
                        "orderSpecificationflagUploadDRReason": null,
                        "orderSpecificationflagCADakorReason": null,
                        "orderSpecificationflagInstantApprovalReason": null,
                        "orderSpecificationCollateralTypeIDReason": null,
                        "orderSpecificationIsWillingToAcceptInfoReason": null,
                        "orderSpecificationApplicationContractNumberReason": null,
                        "orderSpecificationApplicationDocVerStatusReason": null,
                        "orderSpecificationCustomerStatusReason": null,
                        "orderSpecificationAosApprovalStatusReason": null,
                        "orderSupportingDocumentsReason": [
                            {
                                "orderSupportingDocumentIDReason": null,
                                "orderSupportingDocumentProductIDReason": null,
                                "orderSupportingDocumentTypeIDReason": null,
                                "orderSupportingDocumentIsMandatoryReason": null,
                                "orderSupportingDocumentIsMandatoryCAReason": null,
                                "orderSupportingDocumentIsDisplayReason": null,
                                "orderSupportingDocumentIsUnitReason": null,
                                "orderSupportingDocumentDocumentReceivedDateReason": null,
                                "orderSupportingDocumentFileNameReason": null,
                                "orderSupportingDocumentHeaderIDReason": null,
                                "orderSupportingDocumentUploadDateReason": null,
                                "orderSupportingDocumentCreatedAtReason": null,
                                "orderSupportingDocumentCreatedByReason": null,
                                "orderSupportingDocumentModifiedAtReason": null,
                                "orderSupportingDocumentModifiedByReason": null,
                                "orderSupportingDocumentStatusReason": null,
                                "orderSupportingDocumentFlagCorrectionReason": null
                            }
                        ],
                        "orderProductsReason": [
                            {
                                "orderProductIDReason": null,
                                "orderProductFinancingTypeIDReason": _providerUnitObject.isTypeOfFinancingModelChanges ? "99" : null,
                                "orderProductOjkBusinessTypeIDReason": _providerUnitObject.isBusinessActivitiesModelChanges ? "99" : null,
                                "orderProductOjkBussinessDetailIDReason": _providerUnitObject.isBusinessActivitiesTypeModelChanges ? "99" : null,
                                "orderProductObjectGroupIDReason": _providerUnitObject.isGroupObjectChanges ? "99" : null,
                                "orderProductObjectIDReason": _providerUnitObject.isObjectChanges ? "99" : null,
                                "orderProductProductTypeIDReason": _providerUnitObject.isTypeProductChanges ? "99" : null,
                                "orderProductObjectBrandIDReason": _providerUnitObject.isBrandObjectChanges ? "99" : null,
                                "orderProductObjectTypeIDReason": _providerUnitObject.isObjectTypeChanges ? "99" : null,
                                "orderProductObjectModelIDReason": _providerUnitObject.isModelObjectChanges ? "99" : null,
                                "orderProductModelDetailReason": _providerUnitObject.isDetailModelChanges ? "99" : null,
                                "orderProductObjectUsageIDReason": _providerUnitObject.isUsageObjectModelChanges ? "99" : null,
                                "orderProductObjectPurposeIDReason": _providerUnitObject.isObjectPurposeChanges ? "99" : null,
                                "orderProductSalesGroupIDReason": _providerUnitObject.isGroupSalesChanges ? "99" : null,
                                "orderProductOrderSourceIDReason": _providerUnitObject.isSourceOrderChanges ? "99" : null,
                                "orderProductOrderSourceNameReason": _providerUnitObject.isSourceOrderNameChanges ? "99" : null,
                                "orderProductGroupIDReason": _providerUnitObject.isGrupIdChanges ? "99" : null,
                                "orderProductOutletIDReason": null,
                                "orderProductIsThirdPartyReason": _providerUnitObject.isThirdPartyChanges ? "99" : null,
                                "orderProductThirdPartyTypeIDReason": _providerUnitObject.isThirdPartyTypeChanges ? "99" : null,
                                "orderProductThirdPartyIDReason": _providerUnitObject.isThirdPartyChanges ? "99" : null,
                                "orderProductThirdPartyActivityReason": _providerUnitObject.isKegiatanChanges ? "99" : null,
                                "orderProductSentradIDReason": null, //_providerUnitObject.isSentraDChanges ? "99" : null,
                                "orderProductUnitdIDReason": null, //_providerUnitObject.isUnitDChanges ? "99" : null,
                                "orderProductDealerMatrixReason": _providerUnitObject.isMatriksDealerChanges ? "99" : null,
                                "orderProductProgramIDReason": _providerUnitObject.isProgramChanges ? "99" : null,
                                "orderProductRehabTypeReason":  _providerUnitObject.isRehabTypeChanges ? "99" : null,
                                "orderProductReferenceNumberReason": _providerUnitObject.isReferenceNumberChanges ? "99" : null,
                                "orderProductApplicationUnitContractNumberReason": null,
                                "orderProductSalesesReason": _orderProductSalesesDakor,
                                "orderProductKaroserisReason": _orderProductKaroserisReason,
                                "orderProductInsurancesReason": _orderProductInsurancesReason,
                                "orderProductWmpsReason": _orderProductWmpsReason,
                                "orderProductCreditStructureInstallmentTypeIDReason": _providerCreditStructure.installmentTypeDakor ? "99" : null,
                                "orderProductCreditStructureTenorReason": _providerCreditStructure.periodTimeDakor ? "99" : null,
                                "orderProductCreditStructurePaymentMethodIDReason": _providerCreditStructure.paymentMethodDakor ? "99" : null,
                                "orderProductCreditStructureEffectiveRateReason": _providerCreditStructure.interestRateEffDakor ? "99" : null,
                                "orderProductCreditStructureFlatRateReason": _providerCreditStructure.interestRateFlatDakor ? "99" : null,
                                "orderProductCreditStructureObjectPriceReason": _providerCreditStructure.objectPriceDakor ? "99" : null,
                                "orderProductCreditStructureKaroseriTotalPriceReason": _providerCreditStructure.totalPriceKaroseriDakor ? "99" : null,
                                "orderProductCreditStructureTotalPriceReason": _providerCreditStructure.totalPriceDakor ? "99" : null,
                                "orderProductCreditStructureNettDownPaymentReason": _providerCreditStructure.netDPDakor ? "99" : null,
                                "orderProductCreditStructureDeclineNInstallmentReason": null,
                                "orderProductCreditStructurePaymentOfYearReason": _providerCreditStructure.paymentPerYearDakor ? "99" : null,
                                "orderProductCreditStructureBranchDownPaymentReason": _providerCreditStructure.branchDPDakor ? "99" : null,
                                "orderProductCreditStructureGrossDownPaymentReason": _providerCreditStructure.grossDPDakor ? "99" : null,
                                "orderProductCreditStructureTotalLoanReason": _providerCreditStructure.totalLoanDakor ? "99" : null,
                                "orderProductCreditStructureInstallmentAmountReason": _providerCreditStructure.installmentDakor ? "99" : null,
                                "orderProductCreditStructureLtvRatioReason": _providerCreditStructure.ltvDakor ? "99" : null,
                                "orderProductCreditStructureInterestAmountReason": null,
                                "orderProductCreditStructurePencairanReason": null,
                                "orderProductCreditStructureDSRReason": _providerCreditIncome.isDebtComparisonDakor ? "99" : null,
                                "orderProductCreditStructureDIRReason": _providerCreditIncome.isIncomeComparisonDakor ? "99" : null,
                                "orderProductCreditStructureDSCReason": _providerCreditIncome.isDSCDakor ? "99" : null,
                                "orderProductCreditStructureIRRReason": _providerCreditIncome.isIRRDakor ? "99" : null,
                                "orderProductCreditStructureGpTypeReason": null,
                                "orderProductCreditStructureNewTenorReason": null,
                                "orderProductCreditStructureTotalSteppingReason": null,
                                "orderProductCreditStructureBalloonTypeReason": _providerCreditStructureType.isRadioValueBalloonPHOrBalloonInstallmentChanges ? "99" : null,
                                "orderProductCreditStructureLastInstallmentPercentageReason": _providerCreditStructureType.isInstallmentPercentage ? "99" : null,
                                "orderProductCreditStructureLastInstallmentValueReason": _providerCreditStructureType.isInstallmentValue ? "99" : null,
                                "orderProductCreditStructureFirstInstallmentValueReason": null,
                                "orderProductFeesReason": _orderProductFeesReason,
                                "orderProductSubsidiesReason": _orderProductSubsidiesReason,
                                "orderProductCreditStructureInstallmentDetailsReason": [],
                                "orderProductCollateralAutomotivesReason": _providerKolateral.collateralTypeModel.id == "001"
                                    ?
                                [
                                    {
                                        "collateralAutoCollateralIDReason": null,
                                        "collateralAutoIsCollaSameUnitReason": _providerKolateral.isRadioValueIsCollateralSameWithUnitOtoChanges ? "99" : null,
                                        "collateralAutoIsMultiUnitCollateralReason": null,
                                        "collateralAutoIsCollaNameSameWithCustomerNameReason": _providerKolateral.isRadioValueIsCollaNameSameWithApplicantOtoChanges ? "99" : null,
                                        "collateralAutoIdentityTypeReason": _providerKolateral.isIdentityTypeSelectedAutoChanges ? "99" : null,
                                        "collateralAutoIdentityNumberReason": _providerKolateral.isIdentityNumberAutoChanges ? "99" : null,
                                        "collateralAutoIdentityNameReason": _providerKolateral.isNameOnCollateralAutoChanges ? "99" : null,
                                        "collateralAutoFullNameReason": _providerKolateral.isNameOnCollateralAutoChanges ? "99" : null,
                                        "collateralAutoAliasReason": null,
                                        "collateralAutoTitleReason": null,
                                        "collateralAutoDateOfBirthReason": _providerKolateral.isBirthDateAutoChanges ? "99" : null,
                                        "collateralAutoPlaceOfBirthReason": _providerKolateral.isBirthPlaceValidWithIdentityAutoChanges ? "99" : null,
                                        "collateralAutoPlaceOfBirthKabKotaReason": _providerKolateral.isBirthPlaceValidWithIdentityLOVAutoChanges ? "99" : null,
                                        "collateralAutoGenderReason": null,
                                        "collateralAutoIdentityActiveStartReason": null,
                                        "collateralAutoIdentityActiveEndReason": null,
                                        "collateralAutoReligionReason": null,
                                        "collateralAutoOccupationIDReason": null,
                                        "collateralAutoPositionIDReason": null,
                                        "collateralAutoObjectGroupIDReason": _providerKolateral.isGroupObjectChanges ? "99" : null,
                                        "collateralAutoObjectIDReason": _providerKolateral.isObjectChanges ? "99" : null,
                                        "collateralAutoProductTypeIDReason": null, //_providerKolateral.isTypeProductChanges ? "99" : null,
                                        "collateralAutoObjectBrandIDReason": _providerKolateral.isBrandObjectChanges ? "99" : null,
                                        "collateralAutoObjectTypeIDReason": _providerKolateral.isObjectTypeChanges ? "99" : null,
                                        "collateralAutoObjectModelIDReason": _providerKolateral.isModelObjectChanges ? "99" : null,
                                        "collateralAutoObjectUsageIDReason": _providerKolateral.isUsageObjectModelChanges ? "99" : null,
                                        "collateralAutoObjectPurposeIDReason": _providerKolateral.isUsageCollateralOtoChanges ? "99" : null,
                                        "collateralAutProductionYearReason": _providerKolateral.isYearProductionSelectedChanges ? "99" : null,
                                        "collateralAutoRegistrationYearReason": _providerKolateral.isYearRegistrationSelectedChanges ? "99" : null,
                                        "collateralAutoIsYellowPlateReason": _providerKolateral.isRadioValueYellowPlatChanges ? "99" : null,
                                        "collateralAutoIsBuiltUpReason": _providerKolateral.isRadioValueBuiltUpNonATPMChanges ? "99" : null,
                                        "collateralAutoBpkbNumberReason": _providerKolateral.isBPKPNumberChanges ? "99" : null,
                                        "collateralAutoChassisNumberReason": _providerKolateral.isFrameNumberChanges ? "99" : null,
                                        "collateralAutoEngineNumberReason": _providerKolateral.isMachineNumberChanges ? "99" : null,
                                        "collateralAutoPoliceNumberReason": _providerKolateral.isPoliceNumberChanges ? "99" : null,
                                        "collateralAutoGradeUnitReason": _providerKolateral.isGradeUnitChanges ? "99" : null,
                                        "collateralAutoUtjFacilityReason": _providerKolateral.isFasilitasUTJChanges ? "99" : null,
                                        "collateralAutoBidderNameReason": _providerKolateral.isNamaBidderChanges ? "99" : null,
                                        "collateralAutoShowroomPriceReason": _providerKolateral.isHargaJualShowroomChanges ? "99" : null,
                                        "collateralAutoAdditionalEquipmentReason": _providerKolateral.isPerlengkapanTambahanChanges ? "99" : null,
                                        "collateralAutoMpAdiraReason": _providerKolateral.isMPAdiraChanges ? "99" : null,
                                        "collateralAutoPhysicalReconditionReason": _providerKolateral.isRekondisiFisikChanges ? "99" : null,
                                        "collateralAutoIsProperReason": null,
                                        "collateralAutoLtvRatioReason": null,
                                        "collateralAutoMpAdiraUploadReason": _providerKolateral.isMPAdiraUploadChanges ? "99" : null,
                                        "collateralAutoCollateralDpReason": _providerKolateral.isDpGuaranteeChanges ? "99" : null,
                                        "collateralAutoMaximumPhReason": _providerKolateral.isPHMaxAutomotiveChanges ? "99" : null,
                                        "collateralAutoAppraisalPriceReason": _providerKolateral.isTaksasiPriceAutomotiveChanges ? "99" : null,
                                        "collateralAutoCollateralUtilizationIDReason": _providerKolateral.isUsageCollateralOtoChanges ? "99" : null,
                                        "collateralAutoAdditionalCapacityReason": null,
                                        "collateralAutoAdditionalColorReason": null,
                                        "collateralAutoAdditionalManufacturerReason": null,
                                        "collateralAutoAdditionalSerialNumberReason": null,
                                        "collateralAutoAdditionalInvoiceNumberReason": null,
                                        "collateralAutoAdditionalStnkActiveDateReason": null,
                                        "collateralAutoAdditionalBpkbAddressReason": null,
                                        "collateralAutoAdditionalBpkbIdentityTypeIDReason": null,
                                        "collateralAutoStatusReason": null,
                                        "collateralAutoCreatedAtReason": null,
                                        "collateralAutoCreatedByReason": null,
                                        "collateralAutoModifiedAtReason": null,
                                        "collateralAutoModifiedByReason": null,
                                        "collateralAutoFlagCorrectionReason": null
                                    }
                                ]
                                    : [],
                                "orderProductCollateralPropertiesReason": _providerKolateral.collateralTypeModel.id == "002"
                                    ?
                                [
                                    {
                                        "collateralPropertyIsMultiUnitCollateralReason": null,
                                        "collateralPropertyIsCollaNameSameWithCustomerNameReason": _providerKolateral.isRadioValueIsCollateralSameWithApplicantPropertyChanges ? "99" : null,
                                        "collateralPropertyIDReason": null,
                                        "collateralPropertyIdentityTypeReason": _providerKolateral.isIdentityModelChanges ? "99" : null,
                                        "collateralPropertyIdentityNumberReason": _providerKolateral.isIdentityNumberPropChanges ? "99" : null,
                                        "collateralPropertyIdentityNameReason": _providerKolateral.isNameOnCollateralChanges ? "99" : null,
                                        "collateralPropertyIdentityFullNameReason": _providerKolateral.isNameOnCollateralChanges ? "99" : null,
                                        "collateralPropertyIdentityAliasReason": null,
                                        "collateralPropertyIdentityTitleReason": null,
                                        "collateralPropertyIdentityDateOfBirthReason": _providerKolateral.isBirthDatePropChanges ? "99" : null,
                                        "collateralPropertyIdentityPlaceOfBirthReason": _providerKolateral.isBirthPlaceValidWithIdentity1Changes ? "99" : null,
                                        "collateralPropertyIdentityPlaceOfBirthKabKotaReason": _providerKolateral.isBirthPlaceValidWithIdentityLOVChanges ? "99" : null,
                                        "collateralPropertyIdentityGenderReason": null,
                                        "collateralPropertyIdentityActiveStartReason": null,
                                        "collateralPropertyIdentityActiveEndReason": null,
                                        "collateralPropertyIdentityReligionReason": null,
                                        "collateralPropertyIdentityOccupationIDReason": null,
                                        "collateralPropertyIdentityPositionIDReason": null,
                                        "collateralPropertyCertificateNumberReason": _providerKolateral.isCertificateNumberChanges ? "99" : null,
                                        "collateralPropertyCertificateTypeIDReason": _providerKolateral.isCertificateTypeSelectedChanges ? "99" : null,
                                        "collateralPropertyPropertyTypeIDReason": _providerKolateral.isPropertyTypeSelectedChanges ? "99" : null,
                                        "collateralPropertyBuildingAreaReason": _providerKolateral.isBuildingAreaChanges ? "99" : null,
                                        "collateralPropertyLandAreaReason": _providerKolateral.isSurfaceAreaChanges ? "99" : null,
                                        "collateralPropertyCollateralDpReason": _providerKolateral.isDPJaminanChanges ? "99" : null,
                                        "collateralPropertyMaximumPhReason": _providerKolateral.isPHMaxChanges ? "99" : null,
                                        "collateralPropertyAppraisalPriceReason": _providerKolateral.isTaksasiPriceChanges ? "99" : null,
                                        "collateralPropertyCollateralUtilizationIDReason": _providerKolateral.isUsageCollateralPropertyChanges ? "99" : null,
                                        "collateralPropertyPositiveFasumDistanceReason": _providerKolateral.isJarakFasumPositifChanges ? "99" : null,
                                        "collateralPropertyNegativeFasumDistanceReason": _providerKolateral.isJarakFasumNegatifChanges ? "99" : null,
                                        "collateralPropertyLandPriceReason": _providerKolateral.isHargaTanahChanges ? "99" : null,
                                        "collateralPropertyNjopPriceReason": _providerKolateral.isHargaNJOPChanges ? "99" : null,
                                        "collateralPropertyBuildingPriceReason": _providerKolateral.isHargaBangunanChanges ? "99" : null,
                                        "collateralPropertyRoofTypeIDReason": _providerKolateral.isTypeOfRoofSelectedChanges ? "99" : null,
                                        "collateralPropertyWallTypeIDReason": _providerKolateral.isWallTypeSelectedChanges ? "99" : null,
                                        "collateralPropertyFloorTypeIDReason": _providerKolateral.isFloorTypeSelectedChanges ? "99" : null,
                                        "collateralPropertyFoundationTypeIDReason": _providerKolateral.isFoundationTypeSelectedChanges ? "99" : null,
                                        "collateralPropertyRoadTypeIDReason": _providerKolateral.isStreetTypeSelectedChanges ? "99" : null,
                                        "collateralPropertyIsCarPassableReason": _providerKolateral.isRadioValueAccessCarChanges ? "99" : null,
                                        "collateralPropertyTotalOfHouseInRadiusReason": _providerKolateral.isJumlahRumahDalamRadiusChanges ? "99" : null,
                                        "collateralPropertySifatJaminanReason": _providerKolateral.isSifatJaminanChanges ? "99" : null,
                                        "collateralPropertyBuktiKepemilikanTanahReason": _providerKolateral.isBuktiKepemilikanChanges ? "99" : null,
                                        "collateralPropertyTgglTerbitSertifikatReason": _providerKolateral.isCertificateReleaseDateChanges ? "99" : null,
                                        "collateralPropertyTahunTerbitSertifikatReason": _providerKolateral.isCertificateReleaseYearChanges ? "99" : null,
                                        "collateralPropertyNamaPemegangHakReason": _providerKolateral.isNamaPemegangHakChanges ? "99" : null,
                                        "collateralPropertyNoSuratUkurGmbrSituasiReason": _providerKolateral.isNoSuratUkurChanges ? "99" : null,
                                        "collateralPropertyTgglSuratUkurGmbrSituasiReason": _providerKolateral.isDateOfMeasuringLetterChanges ? "99" : null,
                                        "collateralPropertySertifikatSuratUkurDikeluarkanOlehReason": _providerKolateral.isCertificateOfMeasuringLetterChanges ? "99" : null,
                                        "collateralPropertyMasaBerlakuHakReason": _providerKolateral.isMasaHakBerlakuChanges ? "99" : null,
                                        "collateralPropertyNoImbReason": _providerKolateral.isNoIMBChanges ? "99" : null,
                                        "collateralPropertyTgglImbReason": _providerKolateral.isDateOfIMBChanges ? "99" : null,
                                        "collateralPropertyLuasBangunanImbReason": _providerKolateral.isLuasBangunanIMBChanges ? "99" : null,
                                        "collateralPropertyLtvRatioReason": null,
                                        "collateralPropertyLetakTanahReason": null,
                                        "collateralPropertyDetailAddressesReason": [
                                            {
                                                "addressIDReason": null,
                                                "addressForeignBusinessIDReason": null,
                                                "addressSpecificationKorespondenReason": null,
                                                "addressSpecificationMatrixAddrReason": null,
                                                "addressSpecificationAddressReason": _providerKolateral.isAddressDakor ? "99" : null,
                                                "addressSpecificationRtReason": _providerKolateral.isRTDakor ? "99" : null,
                                                "addressSpecificationRwReason": _providerKolateral.isRWDakor ? "99" : null,
                                                "addressSpecificationProvinsiIDReason": _providerKolateral.isProvinsiDakor ? "99" : null,
                                                "addressSpecificationKabkotIDReason": _providerKolateral.isKabKotDakor ? "99" : null,
                                                "addressSpecificationKecamatanIDReason": _providerKolateral.isKecamatanDakor ? "99" : null,
                                                "addressSpecificationKelurahanIDReason": _providerKolateral.isKelurahanDakor ? "99" : null,
                                                "addressSpecificationZipcodeReason": _providerKolateral.isKodePosDakor ? "99" : null,
                                                "addressContactSpecificationTelephoneArea1Reason": null,
                                                "addressContactSpecificationTelephone1Reason": null,
                                                "addressContactSpecificationTelephoneArea2Reason": null,
                                                "addressContactSpecificationTelephone2Reason": null,
                                                "addressContactSpecificationFaxAreaReason": null,
                                                "addressContactSpecificationFaxReason": null,
                                                "addressContactSpecificationHandphoneReason": null,
                                                "addressContactSpecificationEmailReason": null,
                                                "addressTypeReason": _providerKolateral.isAddressTypeDakor ? "99" : null,
                                                "addressCreationalSpecificationCreatedAtReason": null,
                                                "addressCreationalSpecificationCreatedByReason": null,
                                                "addressCreationalSpecificationModifiedAtReason": null,
                                                "addressCreationalSpecificationModifiedByReason": null,
                                                "addressStatusReason": null,
                                                "addressFlagCorrectionReason": null,
                                            }
                                        ],
                                        "collateralPropertyStatusReason": null,
                                        "collateralPropertyCreatedAtReason": null,
                                        "collateralPropertyCreatedByReason": null,
                                        "collateralPropertyModifiedAtReason": null,
                                        "collateralPropertyModifiedByReason": null,
                                        "collateralPropertyFlagCorrectionReason": null,
                                    }
                                ]
                                    : [],
                                "orderProdAutoDebitIsAutoDebitReason": null,
                                "orderProdAutoDebitBankIDReason": null,
                                "orderProdAutoDebitAccountNumberReason": null,
                                "orderProdAutoDebitAccountBehalfReason": null,
                                "orderProdAutoDebitAccountPurposeTypeIDReason": null,
                                "orderProdAutoDebitDownPaymentSourceIDReason": null,
                                "orderProductAdditionalVirtualAccountIDReason": null,
                                "orderProductAdditionalVirtualAccountValueReason": null,
                                "orderProductAdditionalCashingPurposeIDReason": null,
                                "orderProductAdditionalCashingTypeIDReason": null,
                                "orderProductCompletionLastKnownOrderProductStateReason": null,
                                "orderProductCompletionNextKnownOrderProductStateReason": null,
                                "orderProductTacActualReason": null,
                                "orderProductTacMaxReason": null,
                                "orderProductCreatedAtReason": null,
                                "orderProductCreatedByReason": null,
                                "orderProductModifiedAtReason": null,
                                "orderProductModifiedByReason": null,
                                "orderProductStatusReason": null,
                                "applSendFlagReason": null,
                                "isWithoutCollaReason": null,
                                "isSaveKaroseriReason": null,
                                "orderProductFlagCorrectionReason": null,
                                "orderProductNik": null,
                                "orderProductNama": null,
                                "orderProductJabatan": null
                            }
                        ],
                        "orderProcessingCompletionCalculatedAtReason": null,
                        "orderProcessingCompletionLastKnownOrderStateReason": null,
                        "orderProcessingCompletionLastKnownOrderStatePositionReason": null,
                        "orderProcessingCompletionLastKnownOrderStatusReason": null,
                        "orderProcessingCompletionLastKnownOrderHandledByReason": null,
                        "orderTabFlagsReason": null,
                        "orderCreatedAtReason": null,
                        "orderCreatedByReason": null,
                        "orderModifiedAtReason": null,
                        "orderModifiedByReason": null,
                        "orderStatusReason": null
                    },
                    "surveyDTOReason": {
                        "surveyIDReason": null,
                        "surveyOrderIDReason": null,
                        "surveyTypeReason": null,
                        "surveyEmployeeIDReason": null,
                        "surveyPhoneNumberReason": null,
                        "surveyJanjiSurveyDateReason": null,
                        "surveyReasonReason": null,
                        "surveyFlagBRMSReason": null,
                        "surveyStatusReason": null,
                        "surveyDataStatusReason": null,
                        "surveyProcessReason": null,
                        "surveyCfoEligibilityStatusReason": null,
                        "surveyResultTypeReason": _providerSurvey.isSurveyTypeChanges ? "99" : null,
                        "surveyResultRecomendationTypeReason": _providerSurvey.isSurveyRecommendationChanges ? "99" : null,
                        "surveyResultNotesReason": _providerSurvey.isSurveyNotesChanges ? "99" : null,
                        "surveyResultDateReason": _providerSurvey.isSurveyDateChanges ? "99" : null,
                        "surveyResultDistanceWithSentraReason": _providerSurvey.isDistanceLocationWithSentraUnitChanges ? "99" : null,
                        "surveyResultDistanceWithDealerReason": _providerSurvey.isDistanceLocationWithDealerMerchantAXIChanges ? "99" : null,
                        "surveyResultDistanceObjWithSentraReason": _providerSurvey.isDistanceLocationWithUsageObjectWithSentraUnitChanges ? "99" : null,
                        "surveyResultAssetsReason": _surveyResultAssetsDakor,
                        "surveyResultDetailsReason": _surveyResultDetailsDakor,
                        "surveyResultTelesurveysReason": [],
                        "surveyCreatedAtReason": null,
                        "surveyCreatedByReason": null,
                        "surveyModifiedAtReason": null,
                        "surveyModifiedByReason": null
                    }
                },
                "ms2TaskID": _preferences.getString("order_no"),
                "flag": ""
            });

            insertLog(context, _timeStartValidate, DateTime.now(), _body, "Start Submit Dakor ${_preferences.getString("last_known_state")}", "Start Submit Dakor ${_preferences.getString("last_known_state")}");

            final ioc = new HttpClient();
            ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
            final _http = IOClient(ioc);
            print("lalalalala");

            String _submitDakor = await storage.read(key: "SubmitDakor");
            final _response = await _http.post(
                // "http://192.168.57.122/orca_api/public/login",
                "${BaseUrl.urlGeneral}$_submitDakor",
                body: _body,
                headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
            );

            debugPrint("CEK_RESPONSE_DKR ${_response.statusCode}");
            debugPrint("CEK_RESPONSE_DKR2 ${_response.body}");
            insertLog(context, _timeStartValidate, DateTime.now(), _body, _response.body, "Submit ${_preferences.getString("last_known_state")}");
            if(_providerSurvey.resultSurveySelected != null) {
                if(_providerSurvey.resultSurveySelected.KODE != "001") {
                    _insertApplNoDeleteTask(context);
                } else {
                    loadData = false;
                    messageProcess = "";
                    Future.delayed(Duration(seconds: 1), () {
                        Navigator.pop(context);
                    });
                }
            } else {
                _insertApplNoDeleteTask(context);
            }
        }
        catch(e){
            _showSnackBar(e.toString());
        }
        // final _data = jsonDecode(_response.body);
        // if(_response.statusCode == 201){
        //   _showSnackBar("${_data['message']}");
        // }
        // else{
        //   _showSnackBar("Error ${_response.statusCode}");
        // }
    }

    void _insertApplNoDeleteTask(BuildContext context) async {
        DateTime _timeStartValidate = DateTime.now();
        loadData = true;
        messageProcess = "Proses insert appl no & delete task...";
        String _insertApplNo = await storage.read(key: "InsertApplNo");
        SharedPreferences _preferences = await SharedPreferences.getInstance();
        List _dataPayung = await _dbHelper.selectDataNoPayung();
        List _dataNoUnit = await _dbHelper.selectDataNoUnit();
        var _providerInfoAppl = Provider.of<InfoAppChangeNotifier>(context,listen: false);
        var _providerInfoApplObject = Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false);
        var _body = jsonEncode({
            "P_APPLNO_PAYUNG": _preferences.getString("last_known_state") == "IDE" ? _dataPayung[0]['no_aplikasi_payung'] : _providerInfoAppl.applNo,
            "P_APPLNO_UNIT": _preferences.getString("last_known_state") == "IDE" ? _dataNoUnit[0]['no_unit'] : _providerInfoApplObject.applObjtID,
            "P_ORDER_NO": _preferences.getString("order_no"),
            "P_STATUS": _preferences.getString("last_known_state")
        });
        debugPrint("body _insertApplNoDeleteTask ${_preferences.getString("last_known_state")} $_body");
        insertLog(context, _timeStartValidate, DateTime.now(), _body, "Start Delete Task ${_preferences.getString("last_known_state")}", "Start Delete Task ${_preferences.getString("last_known_state")}");
        try{
            var _response = await http.post(
                "${BaseUrl.urlGeneral}$_insertApplNo",
                body: _body,
                headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
            ).timeout(Duration(seconds: 30));

            debugPrint("body _insertApplNoDeleteTask ${_response.statusCode}");
            debugPrint("body _insertApplNoDeleteTask ${_response.body} ${_preferences.getString("last_known_state")}");

            if(_response.statusCode == 200){
                // print("insertApplNoDeleteTask1 ${_response.body}");
                var _result = jsonDecode(_response.body);
                if(_result['status'] != "1"){
                    // print("insertApplNoDeleteTask2 ${_response.body}");
                    loadData = false;
                    _showSnackBar("${_result['message']}");
                }
                else{
                    // print("insertApplNoDeleteTask3 ${_response.body}");
                    loadData = false;
                    _showSnackBarSuccess("Success ${_result['message']}");
                    messageProcess = "";
                    Future.delayed(Duration(seconds: 1), () {
                        Navigator.pop(context);
                    });
                    // Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context) => HomePage()), (Route <dynamic> route) => false);
                }
            }
            else{
                // print("insertApplNoDeleteTask4 ${_response.statusCode}");
                loadData = false;
                _showSnackBar("Error response ${_response.statusCode} insert appl no");
            }
        }
        on TimeoutException catch(_){
            loadData = false;
            _showSnackBar("Timeout connection APK insert appl no & delete task");
        }
        catch(e){
            // print("insertApplNoDeleteTask5 ${e.toString()}");
            loadData = false;
            _showSnackBar("Error insert appl no & delete task ${e.toString()}");
        }
    }

    void insertLog(BuildContext context,DateTime timeStart, DateTime timeEnd,String body,String response,String labelLogName) async {
        String _insertLog = await storage.read(key: "InsertLog");
        SharedPreferences _preferences = await SharedPreferences.getInstance();
        var diff = timeEnd.difference(timeStart).inMilliseconds;
        debugPrint(dateFormat3.format(DateTime.now()));

        var _body = jsonEncode({
            "duration": diff.toString(),
            "process_date": dateFormatInsertLog.format(DateTime.now()),
            "idx_step": "1",
            "json": body,
            "order_no": _preferences.getString("order_no"),
            "response": response,
            "user_id": _preferences.getString("username"),
            "log_name": labelLogName,
        });

        // await http.post(
        //     "http://192.168.57.122/orca_api/public/login",
        //     body: _body,
        //     headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
        // );

        debugPrint("$_body");
        debugPrint("${BaseUrl.urlGeneral}$_insertLog");

        try{
            var _response = await http.post(
                "${BaseUrl.urlGeneral}$_insertLog",
                body: _body,
                headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
            ).timeout(Duration(seconds: 30));

            if(_response.statusCode == 200){
                // print("insertApplNoDeleteTask1 ${_response.body}");
                var _result = jsonDecode(_response.body);
                debugPrint("$_result");
                if(_result['message'] != "SUCCESS"){
                    debugPrint("${_result['message']}");
                }
                else{
                    debugPrint("${_result['message']}");
                }
            }
            else{
                debugPrint("status code insert log ${_response.statusCode}");
            }
        }
        on TimeoutException catch(_){
            debugPrint("insert log timeout");
        }
        catch(e){
            debugPrint("insert log ${e.toString()}");
        }
    }

    void calculate(BuildContext context,String orderId,String orderProductId) async {
        try{
            loadData = true;
            await Provider.of<InfoCreditStructureChangeNotifier>(context,listen: false).calculateCreditNew(context);
            await Provider.of<InfoCreditIncomeChangeNotifier>(context, listen: false).getDSR(context);
            loadData = false;
            submitFotoToECM(context, orderId, orderProductId);
        }
        catch(e){
            loadData = false;
            dialogFailedSavePartial(context, e.toString());
        }
    }
}
