import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/employee_model.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/add_salesman_change_notifier.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/io_client.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'change_notifier_app/information_object_unit_change_notifier.dart';

class SearchEmployeeChangeNotifier with ChangeNotifier {
  bool _showClear = false;
  bool _loadData = false;
  TextEditingController _controllerSearch = TextEditingController();
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  List<EmployeeModel> _listEmployeeModel = [];
  List<EmployeeModel> _listEmployeeModelTemp = [];

  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  TextEditingController get controllerSearch => _controllerSearch;

  bool get showClear => _showClear;

  set showClear(bool value) {
    this._showClear = value;
    notifyListeners();
  }

  bool get loadData => _loadData;

  set loadData(bool value) {
    this._loadData = value;
  }

  void changeAction(String value) {
    if (value != "") {
      showClear = true;
    } else {
      showClear = false;
    }
  }

  UnmodifiableListView<EmployeeModel> get listEmployeeModel {
    return UnmodifiableListView(this._listEmployeeModel);
  }

  UnmodifiableListView<EmployeeModel> get listEmployeeModelTemp {
    return UnmodifiableListView(this._listEmployeeModelTemp);
  }

  void getEmployee(BuildContext context, String salesType, String positionId, String position) async{
    SharedPreferences _preference = await SharedPreferences.getInstance();
    var _providerUnitObject = Provider.of<InformationObjectUnitChangeNotifier>(context, listen: false);
    this._listEmployeeModel.clear();
    loadData = true;
    final ioc = new HttpClient();
    ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;

    final _http = IOClient(ioc);

    var _body = jsonEncode(
        salesType == "001" ?
        {
          "P_JABATAN_DESC": position,
          "P_KODE_CABANG": _preference.getString("branchid")
        }
        // {
        //   "GroupAlias": ["$position"],
        //   "LocationAlias": ["SAP_GRP_${_preference.getString("SentraD")}"],
        //   "isAlias": false
        // }
          :
        {
          "SZ_SENTRA": _preference.getString("SentraD"),
          "SZ_UNIT": _preference.getString("UnitD"),
          "P_JOB_ID": positionId,
          "P_ORDER_SOURCE": _providerUnitObject.sourceOrderSelected.kode,
          "P_SALES_TYPE": salesType,
          "P_THIRD_PARTY_ID": _providerUnitObject.thirdPartySelected.kode
        }
    );
    print("body pegawai = $_body");

    var storage = FlutterSecureStorage();
    String _fieldPegawai = await storage.read(key: salesType == "001" ? "FieldEmployeeInternal" : "FieldEmployeeEksternal");
    final _response = await _http.post(
      "${BaseUrl.urlGeneral}$_fieldPegawai",
        // "${urlPublic}api/GetPerson/GetListPersonByJobLocationINT",
        body: _body,
        headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
    );
    if(_response.statusCode == 200){
      final _result = jsonDecode(_response.body);
      final _data = _result['data'];
      print("cek data pegawai = $_data");
      if(_data.isEmpty){
        showSnackBar("Pegawai tidak ditemukan");
        loadData = false;
      }
      else{
        for(int i=0; i <_data.length; i++){
          this._listEmployeeModel.add(EmployeeModel(_data[i]['NIK'], salesType == "001" ? _data[i]['Nama'] : _data[i]['NAMA'], _data[i]['Jabatan'], _data[i]['AliasJabatan'])
          );
        }
        loadData = false;
      }
    }
    else{
      showSnackBar("Error response status ${_response.statusCode}");
      this._loadData = false;
    }
    notifyListeners();
  }

  void showSnackBar(String text){
    this._scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor));
  }

  void searchEmployee(String query) {
    if(query.length < 3) {
      showSnackBar("Input minimal 3 karakter");
    } else {
      _listEmployeeModelTemp.clear();
      if (query.isEmpty) {
        return;
      }

      _listEmployeeModel.forEach((dataEmployee) {
        if (dataEmployee.NIK.contains(query) || dataEmployee.Nama.contains(query)) {
          _listEmployeeModelTemp.add(dataEmployee);
        }
      });
    }
    notifyListeners();
  }

  void clearSearchTemp() {
    _listEmployeeModelTemp.clear();
  }
}
