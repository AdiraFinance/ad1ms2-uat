import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/models/lookup_utj_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/io_client.dart';

import '../main.dart';
import 'constants.dart';

class FormMLookupUTJChangeNotif with ChangeNotifier{
    bool _autoValidate = false;
    bool _loadData = false;
    GlobalKey<FormState> _key = GlobalKey<FormState>();
    GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
    TextEditingController _controllerPoliceNumber = TextEditingController();
    TextEditingController _controllerFrameNumber = TextEditingController();
    TextEditingController _controllerMachineNumber = TextEditingController();
    TextEditingController _controllerBPKPNumber = TextEditingController();
    LookupUTJModel _lookupUTJModel;

    GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;
    GlobalKey<FormState> get keyForm => _key;

    get loadData => _loadData;

    set loadData(value) {
      _loadData = value;
    }

    bool get autoValidate => _autoValidate;

    set autoValidate(bool value) {
      _autoValidate = value;
      notifyListeners();
    }

    TextEditingController get controllerPoliceNumber => _controllerPoliceNumber;

    set controllerPoliceNumber(TextEditingController value) {
      _controllerPoliceNumber = value;
      notifyListeners();
    }

    TextEditingController get controllerBPKPNumber => _controllerBPKPNumber;

    set controllerBPKPNumber(TextEditingController value) {
      _controllerBPKPNumber = value;
      notifyListeners();
    }

    TextEditingController get controllerMachineNumber => _controllerMachineNumber;

    set controllerMachineNumber(TextEditingController value) {
      _controllerMachineNumber = value;
      notifyListeners();
    }

    TextEditingController get controllerFrameNumber => _controllerFrameNumber;

    set controllerFrameNumber(TextEditingController value) {
      _controllerFrameNumber = value;
      notifyListeners();
    }

    LookupUTJModel get lookupUTJModel => _lookupUTJModel;

    set lookupUTJModel(LookupUTJModel value) {
      this._lookupUTJModel = value;
    }

    void check(BuildContext context) {
        final _form = _key.currentState;
        if (_form.validate()) {
            autoValidate = false;
            _lookupUtj(context);
        } else {
            autoValidate = true;
        }
    }

    Future<bool> onBackPress() async{
        final _form = _key.currentState;
        if (_form.validate()) {
            autoValidate = false;
        } else {
            autoValidate = true;
        }
        return true;
    }

    void showSnackBar(String text){
        this._scaffoldKey.currentState.showSnackBar(new SnackBar(
            content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor));
    }

    void clearData(){
      this._controllerBPKPNumber.clear();
      this._controllerFrameNumber.clear();
      this._controllerMachineNumber.clear();
      this._controllerPoliceNumber.clear();
      notifyListeners();
    }

    void _lookupUtj(BuildContext context) async {
        loadData = true;
        final ioc = new HttpClient();
        ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;

        final _http = IOClient(ioc);
        var _body = jsonEncode({
          "SZBPKB": _controllerBPKPNumber.text,
          "SZNOKA": _controllerFrameNumber.text,
          "SZNOSIN": _controllerMachineNumber.text,
          "SZNOPOL": _controllerPoliceNumber.text
        });
        print("body lookup utj: $_body");
        try{
            var storage = FlutterSecureStorage();
            String _looktupUTJ = await storage.read(key: "LooktupUTJ");
            final _response = await _http.post(
                "${BaseUrl.urlGeneral}$_looktupUTJ",//GetUKJ/api/getDataUKJ/GetUKJ",
                body: _body,
                headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
            );
            print("cek response utj ${_response.body}");
            print("cek status code utj ${_response.statusCode}");
            if(_response.statusCode == 200){
                final _result = jsonDecode(_response.body);
                if(_result['RC1'] != null){
                  loadData = false;
                  lookupUTJModel = LookupUTJModel(
                      _result['RC1'][0]['COLA_BPKB_NO'].toString(),
                      _result['RC1'][0]['COLA_FRAME_NO'].toString(),
                      _result['RC1'][0]['COLA_ENG_NO'].toString(),
                      _result['RC1'][0]['COLA_PLAT_NO'].toString(),
                      _result['RC1'][0]['COLA_YR_PRODUCT'].toString(),
                      _result['RC1'][0]['AREC_GRADE'].toString(),
                      _result['RC1'][0]['FAS_UKJ'].toString(),
                      _result['RC1'][0]['NAMA_BIDDER'].toString(),
                      _result['RC1'][0]['HARGA_PASAR'].toString(),
                      _result['RC1'][0]['HARGA_JUAL'].toString(),
                  );
                  Navigator.pop(context, lookupUTJModel);
                }
                else {
                  loadData = false;
                  showDialog(
                      context: context,
                      barrierDismissible: false,
                      builder: (BuildContext context){
                        return Theme(
                          data: ThemeData(
                              fontFamily: "NunitoSans",
                              primaryColor: Colors.black,
                              primarySwatch: primaryOrange,
                              accentColor: myPrimaryColor
                          ),
                          child: AlertDialog(
                            title: Text("Informasi", style: TextStyle(fontWeight: FontWeight.bold)),
                            content: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                Text(_result['STRMESSAGE']),
                              ],
                            ),
                            actions: <Widget>[
                              new FlatButton(
                                onPressed: () {
                                  lookupUTJModel = LookupUTJModel(null,null,null,null,null,null,null,null,null,null);
                                  Navigator.pop(context, lookupUTJModel);
                                  Navigator.pop(context, lookupUTJModel);
                                },
                                child: new Text('Close'),
                              ),
                            ],
                          ),
                        );
                      }
                  );
                }
            }
            else{
                showSnackBar("Error response status ${_response.statusCode}");
                loadData = false;
            }
        } catch (e) {
            loadData = false;
        }
        loadData = false;
        notifyListeners();
    }
}