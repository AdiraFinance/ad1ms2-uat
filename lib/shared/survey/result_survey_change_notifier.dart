import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/asset_type_model.dart';
import 'package:ad1ms2_dev/models/electricity_type_model.dart';
import 'package:ad1ms2_dev/models/environmental_information_model.dart';
import 'package:ad1ms2_dev/models/form_m_foto_model.dart';
import 'package:ad1ms2_dev/models/info_document_detail_model.dart';
import 'package:ad1ms2_dev/models/info_document_type_model.dart';
import 'package:ad1ms2_dev/models/ms2_document_model.dart';
import 'package:ad1ms2_dev/models/ms2_sby_assgnmt_model.dart';
import 'package:ad1ms2_dev/models/ms2_svy_rslt_asst_model.dart';
import 'package:ad1ms2_dev/models/ms2_svy_rslt_dtl_model.dart';
import 'package:ad1ms2_dev/models/photo_type_model.dart';
import 'package:ad1ms2_dev/models/recomm_model.dart';
import 'package:ad1ms2_dev/models/recommendation_surveyor_model.dart';
import 'package:ad1ms2_dev/models/recources_survey_info_model.dart';
import 'package:ad1ms2_dev/models/result_survey_asset_model.dart';
import 'package:ad1ms2_dev/models/result_survey_detail_survey_model.dart';
import 'package:ad1ms2_dev/models/result_survey_model.dart';
import 'package:ad1ms2_dev/models/ownership_model.dart';
import 'package:ad1ms2_dev/models/road_type_model.dart';
import 'package:ad1ms2_dev/models/show_mandatory_visible_resultgroupnote.dart';
import 'package:ad1ms2_dev/models/survey_photo_model.dart';
import 'package:ad1ms2_dev/models/survey_type_model.dart';
import 'package:ad1ms2_dev/screens/detail_image.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_application_change_notifier.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/dashboard/dashboard_change_notif.dart';
import 'package:ad1ms2_dev/shared/format_currency.dart';
import 'package:ad1ms2_dev/shared/regex_input_formatter.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:geolocator/geolocator.dart';
import 'package:http/io_client.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../group_unit_object_model.dart';
import 'list_survey_photo_change_notifier.dart';
import 'package:http/http.dart' as http;

class ResultSurveyChangeNotifier with ChangeNotifier{
  var storage = FlutterSecureStorage();
  FormatCurrency _formatCurrency = FormatCurrency();
  RegExInputFormatter _amountValidator = RegExInputFormatter.withRegex('^[0-9]{0,13}(\\.[0-9]{0,2})?\$');
  bool _loadData = false;
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  GlobalKey<ScaffoldState> _scaffoldKeySurvey = new GlobalKey<ScaffoldState>(); // dibuat karena ada error widget duplicate global key
  DbHelper _dbHelper = DbHelper();
  FormatCurrency get formatCurrency => _formatCurrency;
  String _orderSupportingDocumentID = "NEW";
  List<RecommModel> _listRecomm = [];
  bool _mandatoryFieldResultSurveyBerhasil = false;
  String _messageErrorAsset = "";
  String _radioValueApproved = '1';
  bool _mandatoryTanggalKontrak = false;
  String _surveyResultDetailID;
  String _surveyResultAssetID;

  String get surveyResultDetailID => _surveyResultDetailID;

  set surveyResultDetailID(String value) {
    this._surveyResultDetailID = value;
    notifyListeners();
  }

  String get surveyResultAssetID => _surveyResultAssetID;

  set surveyResultAssetID(String value) {
    this._surveyResultAssetID = value;
    notifyListeners();
  }

  bool get mandatoryTanggalKontrak => _mandatoryTanggalKontrak;

  set mandatoryTanggalKontrak(bool value) {
    this._mandatoryTanggalKontrak = value;
    notifyListeners();
  }

  String get radioValueApproved => _radioValueApproved;

  set radioValueApproved(String value) {
    this._radioValueApproved = value;
    notifyListeners();
  }

  String get messageErrorAsset => _messageErrorAsset;

  set messageErrorAsset(String value) {
    this._messageErrorAsset = value;
  }

  bool get mandatoryFieldResultSurveyBerhasil =>
      _mandatoryFieldResultSurveyBerhasil;

  set mandatoryFieldResultSurveyBerhasil(bool value) {
    this._mandatoryFieldResultSurveyBerhasil = value;
    notifyListeners();
  }

  //foto survey
  bool _autoValidateSurveyPhoto = false;
  bool _validatePhoto = false;
  GlobalKey<FormState> _keySurveyPhoto = GlobalKey<FormState>();

  GlobalKey<FormState> get keyFormSurveyPhoto => _keySurveyPhoto;
  bool get validatePhoto => _validatePhoto;

  RegExInputFormatter get amountValidator => _amountValidator;

  set formatCurrency(FormatCurrency value) {
    this._formatCurrency = value;
    notifyListeners();
  }

  bool get loadData => _loadData;

  set loadData(bool value) {
    this._loadData = value;
    notifyListeners();
  }

  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  GlobalKey<ScaffoldState> get scaffoldKeySurvey => _scaffoldKeySurvey;

  /*
  *
  * Hasil Survey Group Catatan
  *
  * */
  bool _autoValidateResultSurveyGroupNote  = false;
  List<RecommendationSurveyorModel> _listRecomSurvey = [];
  List<ResultSurveyModel> _listResultSurvey = [
    ResultSurveyModel("000", "BERHASIL"),
    ResultSurveyModel("001", "TUNDA TIDAK BERTEMU NASABAH"),
    ResultSurveyModel("002", "ALAMAT TIDAK DITEMUKAN"),
    ResultSurveyModel("003", "NASABAH TIDAK TINGGAL DI ALAMAT SURVEY"),
    ResultSurveyModel("004", "NASABAH BATAL"),
  ];
  RecommendationSurveyorModel _recommendationSurveySelected;
  ResultSurveyModel _resultSurveySelected;
  TextEditingController _controllerNote = TextEditingController();
  TimeOfDay _selectedTime = TimeOfDay.now();
  String _hour, _minute, _time;
  TextEditingController _controllerResultSurveyDate = TextEditingController();
  TextEditingController _controllerResultSurveyTime = TextEditingController();
  DateTime _initialDateResultSurvey = DateTime(dateNow.year,dateNow.month,dateNow.day);
  DateTime _initialEndDateLease = DateTime(dateNow.year,dateNow.month,dateNow.day);
  bool _flagResultSurveyGroupNotes = false;
  GlobalKey<FormState> _keyResultSurveyGroupNotes = GlobalKey<FormState>();
  String _pathFile;
  String _typeForm;
  String _custType;

  bool _isResultSurveyChanges = false;
  bool _isResultSurveyGroupNotesChanges = false;
  bool _isSurveyRecommendationChanges = false;
  bool _isSurveyNotesChanges = false;
  bool _isSurveyDateChanges = false;
  bool _isPhotoTypeChanges = false;
  bool _isSurveyTypeChanges = false;
  bool _isDistanceLocationWithSentraUnitChanges = false;
  bool _isDistanceLocationWithDealerMerchantAXIChanges = false;
  bool _isDistanceLocationWithUsageObjectWithSentraUnitChanges = false;
  bool _isAssetTypeChanges = false;
  bool _isValueAssetChanges = false;
  bool _isIdentityInfoChanges = false;
  bool _isSurfaceBuildingAreaInfoChanges = false;
  bool _isRoadTypeInfoChanges = false;
  bool _isElectricityTypeInfoChanges = false;
  bool _isElectricityBillInfoChanges = false;
  bool _isEndDateInfoChanges = false;
  bool _isLengthStayInfoChanges = false;
  bool _isEnvironmentalInfoChanges = false;
  bool _isSourceInfoChanges = false;
  bool _isSourceNameInfoChanges = false;
  bool _isEditResultSurveyAsset = false;

  /*
  *
  * Survey Catatan
  *
  * */
  String get typeForm => _typeForm;

  set typeForm(String value) {
    this._typeForm = value;
    notifyListeners();
  }

  String get pathFile => _pathFile;

  bool get autoValidateResultSurveyGroupNote => _autoValidateResultSurveyGroupNote;

  set autoValidateResultSurveyGroupNote(bool value) {
    this._autoValidateResultSurveyGroupNote = value;
    notifyListeners();
  }

  RecommendationSurveyorModel get recommendationSurveySelected => _recommendationSurveySelected;

  set recommendationSurveySelected(RecommendationSurveyorModel value) {
    this._recommendationSurveySelected = value;
    notifyListeners();
  }

  List<RecommendationSurveyorModel> get listRecomSurvey => _listRecomSurvey;

  TextEditingController get controllerNote => _controllerNote;

  List<ResultSurveyModel> get listResultSurvey => _listResultSurvey;

  ResultSurveyModel get resultSurveySelected => _resultSurveySelected;

  set resultSurveySelected(ResultSurveyModel value) {
    this._resultSurveySelected = value;
    notifyListeners();
  }

  bool get flagResultSurveyGroupNotes => _flagResultSurveyGroupNotes;

  set flagResultSurveyGroupNotes(bool value) {
    this._flagResultSurveyGroupNotes = value;
    notifyListeners();
  }

  TextEditingController get controllerResultSurveyDate => _controllerResultSurveyDate;
  TextEditingController get controllerResultSurveyTime => _controllerResultSurveyTime;
  TimeOfDay get selectedTime => _selectedTime;

  GlobalKey<FormState> get keyResultSurveyGroupNotes => _keyResultSurveyGroupNotes;

  DateTime get initialDateResultSurvey => _initialDateResultSurvey;

  set initialDateResultSurvey(DateTime value) {
    this._initialDateResultSurvey = value;
    notifyListeners();
  }

  Future<void> getListHasilSurvey(BuildContext context) {
    if(this._typeForm == "AOS" && this._listResultSurvey[1].KODE == "001") {
      this._listResultSurvey.removeAt(1);
    }
  }

  void checkForMandatoryField(BuildContext context, ResultSurveyModel value) {
    // var _providerPhotoSurvey = Provider.of<ListSurveyPhotoChangeNotifier>(context, listen: false);
    if(value.KODE == "000") {
      this._mandatoryFieldResultSurveyBerhasil = true;

      // // Lokasi
      // this._flagResultSurveyGroupLocation = false;
      // this._autoValidateResultSurveyGroupLocation = true;
      //
      // // Survey Detail
      // this._flagResultSurveyCreateEditDetailSurvey = false;
      // this._checkDataResultSurveyCreateEditDetailSurvey = true;
      //
      // // Asset
      // this._flagResultSurveyAsset = false;
      // this._checkDataResultSurveyAsset = true;
      //
      // // Photo Survey
      // _providerPhotoSurvey.isFlagSurveyPhoto = false;
      // _providerPhotoSurvey.checkDataSurveyPhoto = true;
    } else {
      this._mandatoryFieldResultSurveyBerhasil = false;

      // // Lokasi
      // // this._flagResultSurveyGroupLocation = true;
      // this._autoValidateResultSurveyGroupLocation = false;
      //
      // // Survey Detail
      // // this._flagResultSurveyCreateEditDetailSurvey = true;
      // this._checkDataResultSurveyCreateEditDetailSurvey = false;
      //
      // // Asset
      // // this._flagResultSurveyAsset = true;
      // this._checkDataResultSurveyAsset = false;
      //
      // // Photo Survey
      // // _providerPhotoSurvey.isFlagSurveyPhoto = false;
      // _providerPhotoSurvey.checkDataSurveyPhoto = false;
    }
  }

  void selectDateSurveyNote(BuildContext context) async {
    // final DateTime _picked = await showDatePicker(
    //     context: context,
    //     initialDate: _initialDateResultSurvey,
    //     firstDate: DateTime(
    //         DateTime.now().year-5, DateTime.now().month, DateTime.now().day),
    //     lastDate: DateTime(
    //         DateTime.now().year + 5, DateTime.now().month, DateTime.now().day));
    // if (_picked != null) {
    //   this._initialDateResultSurvey = _picked;
    //   this._controllerResultSurveyDate.text = dateFormat.format(_picked);
    //   notifyListeners();
    // } else {
    //   return;
    // }
    var _picked = await selectDate(context,  this._initialDateResultSurvey);
    if (_picked != null) {
      this._controllerResultSurveyDate.text = dateFormat.format(_picked);
      this._initialDateResultSurvey = _picked;
      notifyListeners();
    } else {
      return;
    }
  }

  void selectTimeSurveyNote(BuildContext context) async {
    final TimeOfDay _picked = await showTimePicker(
      context: context,
      initialTime: _selectedTime,
      initialEntryMode: TimePickerEntryMode.dial,
    );
    if (_picked != null){
      _selectedTime = _picked;
      _hour = _selectedTime.hour.toString();
      _minute = _selectedTime.minute.toString();
      if(_hour.length < 2){
        _hour = "0${_selectedTime.hour.toString()}";
      }
      else{
        _hour = _selectedTime.hour.toString();
      }
      if(_minute.length < 2){
        _minute = "0${_selectedTime.minute.toString()}";
      }
      else{
        _minute = _selectedTime.minute.toString();
      }
      _time = _hour + ':' + _minute;
      _controllerResultSurveyTime.text = _time;
      notifyListeners();
    }
    else{
      return;
    }
  }

  void getStatusSurveyNote() async{
    this._listResultSurvey.clear();
    final ioc = new HttpClient();
    ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;

    final _http = IOClient(ioc);
    String _fieldStatusSurvey = await storage.read(key: "FieldStatusSurvey");
    final _response = await _http.get(
      "${BaseUrl.urlGeneral}$_fieldStatusSurvey",
      headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
        // "${urlPublic}api/statussurvey/get-status-survey"
    );
    final _data = jsonDecode(_response.body);
    if(_response.statusCode == 200){
      for(int i=0; i < _data.length; i++){
        this._listResultSurvey.add(ResultSurveyModel(_data[i]['KODE'], _data[i]['DESKRIPSI']));
      }
    }
    else{
      showSnackBar("Error ${_response.statusCode}");
    }
    notifyListeners();
  }

  Future<void> getRecomSurveyNote() async{
    this._listRecomSurvey.clear();
    this._controllerResultSurveyDate.text = dateFormat.format(DateTime.now());
    this._controllerResultSurveyTime.text = time.format(DateTime.now());
    final ioc = new HttpClient();
    ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;

    final _http = IOClient(ioc);
    String _fieldRekomendasiSurvey = await storage.read(key: "FieldRekomendasiSurvey");
    final _response = await _http.get(
      "${BaseUrl.urlGeneral}$_fieldRekomendasiSurvey",
      headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
        // "${urlPublic}api/rekomresurvey/get-rekom-resurvey"
    );
    final _data = jsonDecode(_response.body);
    if(_response.statusCode == 200){
      for(int i=0; i < _data.length; i++){
        this._listRecomSurvey.add(RecommendationSurveyorModel(_data[i]['KODE'], _data[i]['DESKRIPSI']));
      }
      notifyListeners();
      // getStatusSurvey();
    }
    else{
      showSnackBar("Error ${_response.statusCode}");
    }
  }

  void checkSurveyNote(BuildContext context) {
    final _form = _keyResultSurveyGroupNotes.currentState;
    if (_form.validate()) {
      flagResultSurveyGroupNotes = true;
      autoValidateResultSurveyGroupNote = false;
      checkDataDakorMS2SvyAssgnmt();
      Navigator.pop(context);
    } else {
      flagResultSurveyGroupNotes = false;
      autoValidateResultSurveyGroupNote = true;
    }
  }

  Future<bool> onBackPressSurveyNote() async{
    final _form = _keyResultSurveyGroupNotes.currentState;
    if (_form.validate()) {
      flagResultSurveyGroupNotes = true;
      autoValidateResultSurveyGroupNote = false;
      checkDataDakorMS2SvyAssgnmt();
    } else {
      flagResultSurveyGroupNotes = false;
      autoValidateResultSurveyGroupNote = true;
    }
    return true;
  }

  /*
  *
  * Hasil Survey Group Lokasi
  *
  * */
  bool _autoValidateResultSurveyGroupLocation  = false;
  List<SurveyTypeModel> _listSurveyType= [];
  SurveyTypeModel _surveyTypeSelected;
  TextEditingController _controllerDistanceLocationWithSentraUnit = TextEditingController();
  TextEditingController _controllerDistanceLocationWithDealerMerchantAXI = TextEditingController();
  TextEditingController _controllerDistanceLocationWithUsageObjectWithSentraUnit = TextEditingController();
  TextEditingController _controllerSurveyType = TextEditingController();
  bool _flagResultSurveyGroupLocation = false;
  GlobalKey<FormState> _keyResultSurveyGroupLocation = GlobalKey<FormState>();

  bool get autoValidateResultSurveyGroupLocation => _autoValidateResultSurveyGroupLocation;

  set autoValidateResultSurveyGroupLocation(bool value) {
    this._autoValidateResultSurveyGroupLocation = value;
    notifyListeners();
  }

  List<SurveyTypeModel> get listSurveyType => _listSurveyType;

  SurveyTypeModel get surveyTypeSelected => _surveyTypeSelected;

  set surveyTypeSelected(SurveyTypeModel value) {
    this._surveyTypeSelected = value;
    notifyListeners();
  }

  TextEditingController get controllerDistanceLocationWithUsageObjectWithSentraUnit => _controllerDistanceLocationWithUsageObjectWithSentraUnit;

  TextEditingController get controllerDistanceLocationWithDealerMerchantAXI => _controllerDistanceLocationWithDealerMerchantAXI;

  TextEditingController get controllerDistanceLocationWithSentraUnit => _controllerDistanceLocationWithSentraUnit;

  TextEditingController get controllerSurveyType => _controllerSurveyType;

  bool get flagResultSurveyGroupLocation => _flagResultSurveyGroupLocation;

  set flagResultSurveyGroupLocation(bool value) {
    this._flagResultSurveyGroupLocation = value;
    notifyListeners();
  }

  GlobalKey<FormState> get keyResultSurveyGroupLocation => _keyResultSurveyGroupLocation;

  void setDefaultTypeSurvey(){
    _surveyTypeSelected = SurveyTypeModel("002", "REGULER SURVEY");
    _controllerSurveyType.text = "${_surveyTypeSelected.id} - ${_surveyTypeSelected.name}";
    setDefaultJarak();
  }

  void setDefaultJarak() {
    if(this._controllerDistanceLocationWithSentraUnit.text == "") {
      this._controllerDistanceLocationWithSentraUnit.text = "0";
    }

    if(this._controllerDistanceLocationWithDealerMerchantAXI.text == "") {
      this._controllerDistanceLocationWithDealerMerchantAXI.text = "0";
    }

    if(this._controllerDistanceLocationWithUsageObjectWithSentraUnit.text == "") {
      this._controllerDistanceLocationWithUsageObjectWithSentraUnit.text = "0";
    }
  }

  void checkResultSurveyGroupLocation(BuildContext context) {
    final _form = _keyResultSurveyGroupLocation.currentState;
    if (_form.validate()) {
      flagResultSurveyGroupLocation = true;
      autoValidateResultSurveyGroupLocation = false;
      checkDataDakorMS2SvyAssgnmt();
      Navigator.pop(context);
    } else {
      flagResultSurveyGroupLocation = false;
      autoValidateResultSurveyGroupLocation = true;
    }
  }

  Future<bool> onBackPressResultSurveyGroupLocation() async{
    final _form = _keyResultSurveyGroupLocation.currentState;
    if (_form.validate()) {
      flagResultSurveyGroupLocation = true;
      autoValidateResultSurveyGroupLocation = false;
      checkDataDakorMS2SvyAssgnmt();
    } else {
      flagResultSurveyGroupLocation = false;
      autoValidateResultSurveyGroupLocation = true;
    }
    return true;
  }

  void saveToSQLiteMS2SvyAssgnmt(BuildContext context) async {
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    _dbHelper.insertMS2SvyAssgnmt(MS2SvyAssgnmtModel(
      surveyID,
      null,
      this._surveyTypeSelected != null ? this._surveyTypeSelected.id : "",
      this._surveyTypeSelected != null ? this._surveyTypeSelected.name : "",
      _preferences.getString("username"),
      null,
      Provider.of<InfoAppChangeNotifier>(context, listen: false).controllerSurveyAppointmentDate.text,
      null,
      null,
      null,
      this._resultSurveySelected != null ? this._resultSurveySelected.KODE : "",
      1,
      DateTime.now().toString(),
      _preferences.getString("username"),
      null,
      null,
      this._resultSurveySelected != null ? this._resultSurveySelected.KODE : "",
      this._resultSurveySelected != null ? this._resultSurveySelected.DESKRIPSI : "",
      this._recommendationSurveySelected != null ? this._recommendationSurveySelected.KODE : "",
      this._recommendationSurveySelected != null ? this._recommendationSurveySelected.DESKRIPSI : "",
      this._controllerNote.text != "" ? this._controllerNote.text : "",
      this._controllerResultSurveyDate.text +" "+ this._controllerResultSurveyTime.text,
      this._controllerDistanceLocationWithSentraUnit.text != "" ? double.parse(this._controllerDistanceLocationWithSentraUnit.text.replaceAll(",", "")) : null,
      this._controllerDistanceLocationWithDealerMerchantAXI.text != "" ? double.parse(this._controllerDistanceLocationWithDealerMerchantAXI.text.replaceAll(",", "")) : null,
      this._controllerDistanceLocationWithUsageObjectWithSentraUnit.text != "" ? double.parse(this._controllerDistanceLocationWithUsageObjectWithSentraUnit.text.replaceAll(",", "")) : null,
      null,
      null,
    ));
  }

  void insertDataMS2SvyAssgnmt(BuildContext context) async{
    if(await _dbHelper.deleteMS2SvyAssgnmt()){
      saveToSQLiteMS2SvyAssgnmt(context);
    }
  }

  String _surveyID;

  String get
  surveyID => _surveyID;

  set surveyID(String value) {
    this._surveyID = value;
  }

  Future<void> selectDataMS2SvyAssgnmt() async{
    List _data = await _dbHelper.selectResultSurveyMS2Assignment();
    if(_data.isNotEmpty){
      print("set data survey assigment");
      await getRecomSurveyNote();
      for(int i=0; i < this._listResultSurvey.length; i++){
        if(_data[0]['survey_result'] == _listResultSurvey[i].KODE){
          _resultSurveySelected = _listResultSurvey[i];
        }
      }

      for(int i=0; i < this._listRecomSurvey.length; i++){
        if(_data[0]['survey_recomendation'] == _listRecomSurvey[i].KODE){
          _recommendationSurveySelected = _listRecomSurvey[i];
        }
      }
      this._surveyTypeSelected = SurveyTypeModel(_data[0]['survey_type_id'], _data[0]['survey_type_desc']);
      this._controllerSurveyType.text = "${_data[0]['survey_type_id']}-${_data[0]['survey_type_desc']}";
      this._controllerNote.text = _data[0]['notes'] != "null" ? _data[0]['notes'] : "";
      this._controllerResultSurveyDate.text = _data[0]['survey_result_date'].toString().split(" ")[0];
      this._controllerResultSurveyTime.text = _data[0]['survey_result_date'].toString().split(" ")[1];
      this._controllerDistanceLocationWithUsageObjectWithSentraUnit.text = _data[0]['distance_objt_purp_sentra'] != "null" ? _data[0]['distance_objt_purp_sentra'].toString() : "";
      this._controllerDistanceLocationWithDealerMerchantAXI.text = _data[0]['distance_deal'] != "null" ? _data[0]['distance_deal'].toString() : "";
      this._controllerDistanceLocationWithSentraUnit.text = _data[0]['distance_sentra'] != "null" ? _data[0]['distance_sentra'].toString() : "";
      surveyID = _data[0]['surveyID'];
    }
    checkDataDakorMS2SvyAssgnmt();
  }

  void checkDataDakorMS2SvyAssgnmt() async{
    List data = await _dbHelper.selectResultSurveyMS2Assignment();
    if(this._typeForm == "DKR" && data.isNotEmpty){
      isResultSurveyGroupNotesChanges = data[0]['survey_result'] != _resultSurveySelected.KODE || data[0]['edit_survey_result'] == "1";
      isSurveyRecommendationChanges = data[0]['survey_recomendation'] != _recommendationSurveySelected.KODE || data[0]['edit_survey_recomendation'] == "1";
      isSurveyTypeChanges = data[0]['survey_type_id'] !=  this._surveyTypeSelected.id || data[0]['edit_survey_type_id'] == "1";
      isSurveyNotesChanges = data[0]['notes'] != this._controllerNote.text || data[0]['edit_notes'] == "1";
      isSurveyDateChanges = data[0]['survey_result_date'] != "${this._controllerResultSurveyDate.text} ${this._controllerResultSurveyTime.text}" || data[0]['edit_survey_result_date'] == "1";
      isDistanceLocationWithUsageObjectWithSentraUnitChanges = data[0]['distance_objt_purp_sentra'].toString() != this._controllerDistanceLocationWithUsageObjectWithSentraUnit.text || data[0]['edit_distance_objt_purp_sentra'] == "1";
      isDistanceLocationWithDealerMerchantAXIChanges = data[0]['distance_deal'].toString() != this._controllerDistanceLocationWithDealerMerchantAXI.text || data[0]['edit_distance_deal'] == "1";
      isDistanceLocationWithSentraUnitChanges = data[0]['distance_sentra'].toString()!= this._controllerDistanceLocationWithSentraUnit.text || data[0]['distance_sentra'] == "1";
    }
    notifyListeners();
  }

  /*
  *
  * Hasil Survey Buat Baru/Update Survey Detail
  *
  * */
  GlobalKey<FormState> _keyResultSurveyCreateEditDetailSurvey = GlobalKey<FormState>();
  List<ResultSurveyDetailSurveyModel> _listResultSurveyDetailSurveyModel = [];
  List<EnvironmentalInformationModel> _listEnviInfo = [
    // EnvironmentalInformationModel("001", "REPUTASI BAIK"),
    // EnvironmentalInformationModel("002", "REPUTASI BURUK"),
  ];
  EnvironmentalInformationModel _environmentalInformationSelected;
  EnvironmentalInformationModel _environmentalInformationTemp;
  List<ResourcesInfoSurveyModel> _listResourcesInfoSurvey = ResourceInfoSurveyList().resourcesInfoSurveyList;
  ResourcesInfoSurveyModel _resourcesInfoSurveySelected;
  ResourcesInfoSurveyModel _resourcesInfoSurveyTemp;
  TextEditingController _controllerResourceInformationName = TextEditingController();
  String _resourceInfoName;
  bool _autoValidateResultSurveyCreateEditDetailSurvey  = false;
  bool _flagResultSurveyCreateEditDetailSurvey = false;
  bool _checkDataResultSurveyCreateEditDetailSurvey = false;

  bool get checkDataResultSurveyCreateEditDetailSurvey =>
      _checkDataResultSurveyCreateEditDetailSurvey;

  set checkDataResultSurveyCreateEditDetailSurvey(bool value) {
    this._checkDataResultSurveyCreateEditDetailSurvey = value;
    notifyListeners();
  }

  List<EnvironmentalInformationModel> get listEnviInfo => _listEnviInfo;

  EnvironmentalInformationModel get environmentalInformationSelected => _environmentalInformationSelected;

  set environmentalInformationSelected(EnvironmentalInformationModel value) {
    this._environmentalInformationSelected = value;
    notifyListeners();
  }

  UnmodifiableListView<ResourcesInfoSurveyModel> get listResourcesInfoSurvey {
    return UnmodifiableListView(this._listResourcesInfoSurvey);
  }

  TextEditingController get controllerResourceInformationName => _controllerResourceInformationName;

  ResourcesInfoSurveyModel get resourcesInfoSurveySelected => _resourcesInfoSurveySelected;

  set resourcesInfoSurveySelected(ResourcesInfoSurveyModel value) {
    this._resourcesInfoSurveySelected = value;
    notifyListeners();
  }

  List<ResultSurveyDetailSurveyModel> get listResultSurveyDetailSurveyModel => _listResultSurveyDetailSurveyModel;

  void addResultSurveyDetailSurveyModel(ResultSurveyDetailSurveyModel value,BuildContext context){
    this._listResultSurveyDetailSurveyModel.add(value);
    notifyListeners();
    Navigator.pop(context);
  }

  void updateResultSurveyDetailSurveyModel(ResultSurveyDetailSurveyModel value,int index,BuildContext context){
    this._listResultSurveyDetailSurveyModel[index] = value;
    notifyListeners();
    Navigator.pop(context);
  }

  void deleteResultSurveyDetailSurveyModel(int index){
    this._listResultSurveyDetailSurveyModel.removeAt(index);
    notifyListeners();
  }

  bool get autoValidateResultSurveyCreateEditDetailSurvey => _autoValidateResultSurveyCreateEditDetailSurvey;

  set autoValidateResultSurveyCreateEditDetailSurvey(bool value) {
    this._autoValidateResultSurveyCreateEditDetailSurvey = value;
    notifyListeners();
  }

  bool get autoValidateSurveyPhoto => _autoValidateSurveyPhoto;

  set autoValidateSurveyPhoto(bool value) {
    this._autoValidateSurveyPhoto = value;
  }

  Future<bool> onBackPressSurveyDetail() async{
    if(this._mandatoryFieldResultSurveyBerhasil) {
      if(this._listResultSurveyDetailSurveyModel.isNotEmpty){
        this._flagResultSurveyCreateEditDetailSurvey = true;
        this._checkDataResultSurveyCreateEditDetailSurvey = false;
      }
      else{
        this._flagResultSurveyCreateEditDetailSurvey = false;
        this._checkDataResultSurveyCreateEditDetailSurvey = true;
      }
    } else {
      this._flagResultSurveyCreateEditDetailSurvey = true;
      this._checkDataResultSurveyCreateEditDetailSurvey = false;
    }
    notifyListeners();
    return true;
  }

  void clearDataFieldResultSurveyGroupLocation(){
    this._environmentalInformationSelected = null;
    this._controllerResourceInformationName.clear();
    this._resourcesInfoSurveySelected = null;
  }

  GlobalKey<FormState> get keyResultSurveyCreateEditDetailSurvey => _keyResultSurveyCreateEditDetailSurvey;

  EnvironmentalInformationModel get environmentalInformationTemp => _environmentalInformationTemp;

  ResourcesInfoSurveyModel get resourcesInfoSurveyTemp => _resourcesInfoSurveyTemp;

  String get resourceInfoName => _resourceInfoName;

  bool get flagResultSurveyCreateEditDetailSurvey => _flagResultSurveyCreateEditDetailSurvey;

  set flagResultSurveyCreateEditDetailSurvey(bool value) {
    this._flagResultSurveyCreateEditDetailSurvey = value;
    notifyListeners();
  }

  // Informasi Lingkungan
  // EnvironmentalInformationModel _environmentalInformationModelSelected;
  bool _isEnvInformationChange = false;
  bool _isSourceInformationChange = false;
  bool _isSourceInfoNameChange = false;
  bool _isEditListResultSurveyDetail = false;
  // EnvironmentalInformationModel get environmentalInformationModelSelected => _environmentalInformationModelSelected;
  //
  // set environmentalInformationModelSelected(EnvironmentalInformationModel value) {
  //   this._environmentalInformationModelSelected = value;
  //   notifyListeners();
  // }

  Future<void> getEnvironmentalInformation(BuildContext context,ResultSurveyDetailSurveyModel value,int flag,int index) async {
    this._resourcesInfoSurveySelected = null;
    this._controllerResourceInformationName.clear();
    _listEnviInfo.clear();
    this._environmentalInformationSelected = null;
    this._loadData = true;
    String _fieldInformasiLingkungan = await storage.read(key: "FieldInformasiLingkungan");
    try{
      final ioc = new HttpClient();
      ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
      final _http = IOClient(ioc);
      final _response = await _http.get(
        "${BaseUrl.urlGeneral}$_fieldInformasiLingkungan",
        headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
          // "${urlPublic}jenis-rehab/get_survey_info"
      );
      final _data = jsonDecode(_response.body);

      for(int i=0; i < _data['data'].length;i++){
        _listEnviInfo.add(EnvironmentalInformationModel(_data['data'][i]['kode'], _data['data'][i]['deskripsi'].trim()));
      }
      this._environmentalInformationSelected = this._listEnviInfo[0];
      this._loadData = false;
      setValueEditSurveyDetail(value,flag,index);
    } catch (e) {
      this._loadData = false;
    }
    notifyListeners();
  }

  void checkSurveyDetail(int flag,int index,BuildContext context){
    final _form = _keyResultSurveyCreateEditDetailSurvey.currentState;
    if(_form.validate()){
      if(flag == 0){
        addResultSurveyDetailSurveyModel(
            ResultSurveyDetailSurveyModel(
                "NEW",
                this._environmentalInformationSelected,
                this._resourcesInfoSurveySelected,
                this._controllerResourceInformationName.text,
                this._isEnvInformationChange,
                this._isSourceInformationChange,
                this._isSourceInfoNameChange,
              this._isEditListResultSurveyDetail
            ),context
        );
      }
      else{
        updateResultSurveyDetailSurveyModel(
            ResultSurveyDetailSurveyModel(
                this._surveyResultDetailID,
                this._environmentalInformationSelected,
                this._resourcesInfoSurveySelected,
                this._controllerResourceInformationName.text,
                this._isEnvInformationChange,
                this._isSourceInformationChange,
                this._isSourceInfoNameChange,
                this._isEditListResultSurveyDetail
            ),
            index,context
        );
        checkDataDakorSurveyDetail(index);
      }
    }
    else{
      autoValidateResultSurveyCreateEditDetailSurvey = true;
    }
  }

  Future<void> setValueEditSurveyDetail(ResultSurveyDetailSurveyModel value,int flag,int index) async{
    if(flag != 0){
      for(int i=0; i < this._listEnviInfo.length; i++){
        if(_listEnviInfo[i].id == value.environmentalInformationModel.id){
          this._environmentalInformationSelected = _listEnviInfo[i];
          this._environmentalInformationTemp = _listEnviInfo[i];
        }
      }

      for(int i=0; i < this._listResourcesInfoSurvey.length; i++){
        if(_listResourcesInfoSurvey[i].PARA_INFORMATION_SOURCE_ID == value.recourcesInfoSurveyModel.PARA_INFORMATION_SOURCE_ID){
          this._resourcesInfoSurveySelected = _listResourcesInfoSurvey[i];
          this._resourcesInfoSurveyTemp = _listResourcesInfoSurvey[i];
        }
      }

      this._controllerResourceInformationName.text = value.resourceInformationName;
      this._resourceInfoName = this._controllerResourceInformationName.text;
      checkDataDakorSurveyDetail(index);
    }
  }

  void saveToSQLiteMS2SurveyDetail(BuildContext context) async {
    List<MS2SvyRsltDtlModel> _listSvyRsltDtl = [];
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    if(_listResultSurveyDetailSurveyModel.isNotEmpty) {
      for(int i=0; i<_listResultSurveyDetailSurveyModel.length; i++){
        _listSvyRsltDtl.add(MS2SvyRsltDtlModel(
          null,
          this._listResultSurveyDetailSurveyModel[i].surveyResultDetailID != null ? this._listResultSurveyDetailSurveyModel[i].surveyResultDetailID : "NEW",
          this._listResultSurveyDetailSurveyModel[i].environmentalInformationModel != null ? this._listResultSurveyDetailSurveyModel[i].environmentalInformationModel.id : "",
          this._listResultSurveyDetailSurveyModel[i].environmentalInformationModel != null ? this._listResultSurveyDetailSurveyModel[i].environmentalInformationModel.name : "",
          this._listResultSurveyDetailSurveyModel[i].recourcesInfoSurveyModel != null ? this._listResultSurveyDetailSurveyModel[i].recourcesInfoSurveyModel.PARA_INFORMATION_SOURCE_ID : "",
          this._listResultSurveyDetailSurveyModel[i].recourcesInfoSurveyModel != null ? this._listResultSurveyDetailSurveyModel[i].recourcesInfoSurveyModel.PARA_INFORMATION_SOURCE_NAME : "",
          this._listResultSurveyDetailSurveyModel[i].resourceInformationName,
          1,
          DateTime.now().toString(),
          _preferences.getString("username"),
          null,
          null,
        ));
      }
    }
    _dbHelper.insertMS2SvyRsltDtl(_listSvyRsltDtl);
  }

  void insertDataSurveyDetail(BuildContext context) async{
    if(await _dbHelper.deleteResultSurveyDetail()){
      saveToSQLiteMS2SurveyDetail(context);
    }
  }

  Future<void> selectDataSurveyDetail() async{
    List _data = await _dbHelper.selectResultSurveyDetail();
    if(_data.isNotEmpty){
      print("set data detail survey");
      this._listResultSurveyDetailSurveyModel.clear();
      for(int i=0; i < _data.length; i++){
        this._listResultSurveyDetailSurveyModel.add(
            ResultSurveyDetailSurveyModel(
                _data[i]['surveyResultDetailID'],
                EnvironmentalInformationModel(_data[i]['info_envirnmt'], _data[i]['info_environt_desc']),
                ResourcesInfoSurveyModel(_data[i]['info_source'], _data[i]['info_source_desc']),
                _data[i]['info_source_name'],
                _data[i]['edit_info_envirnmt'] == "1",
                _data[i]['edit_info_source'] == "1",
                _data[i]['edit_info_source_name'] == "1",
                _data[i]['edit_info_envirnmt'] == "1" || _data[i]['edit_info_source'] == "1" || _data[i]['edit_info_source_name'] == "1",
            )
        );
      }
    }
    notifyListeners();
  }

  void checkDataDakorSurveyDetail(int index) async{
    List _data = await _dbHelper.selectResultSurveyDetail();
    if(_data.isNotEmpty && this._typeForm == "DKR"){
      isEnvInformationChange = _data[index]['info_envirnmt'] != _environmentalInformationSelected.id; //|| _data[index]['edit_info_envirnmt'];
      isSourceInfoChanges = _data[index]['info_source'] != _resourcesInfoSurveySelected.PARA_INFORMATION_SOURCE_ID; //|| _data[index]['edit_info_source'];
      isSourceNameInfoChanges = _data[index]['info_source_name'] != this._controllerResourceInformationName.text; //|| _data[index]['edit_info_source_name'];
      if(this._isEnvInformationChange || this._isSourceInfoChanges || this._isSourceNameInfoChanges){
        isEditListResultSurveyDetail = true;
      }
      else{
        isEditListResultSurveyDetail = false;
      }
    }
    notifyListeners();
  }

  /*
  *
  * Hasil Survey Buat Baru/Update Aset
  *
  * */
  bool _autoValidateResultSurveyAssetModel = false;
  GlobalKey<FormState> _keyResultSurveyAsset= GlobalKey<FormState>();
  List<ResultSurveyAssetModel> _listResultSurveyAssetModel = [];
  List<AssetTypeModel> _listAssetType = [
    AssetTypeModel("001", "MOTOR"),
    AssetTypeModel("002", "MOBIL"),
    AssetTypeModel("003", "PROPERTY"),
  ];
  AssetTypeModel _assetTypeSelected;
  TextEditingController _controllerValueAsset = TextEditingController();
  List<OwnershipModel> _listOwnership = [
    OwnershipModel("01", "MILIK SENDIRI"),
    OwnershipModel("02", "MILIK KELUARGA"),
    OwnershipModel("03", "MILIK PERUSAHAAN"),
    OwnershipModel("04", "RUMAH DINAS"),
    OwnershipModel("05", "KONTRAK/SEWA"),
    OwnershipModel("06", "KOST"),
    OwnershipModel("07", "LAIN-LAIN"),
  ];
  OwnershipModel _ownershipSelected;
  TextEditingController _controllerSurfaceBuildingArea = TextEditingController();
  // List<RoadTypeModel> _listRoadType = [
  //   RoadTypeModel("001", "ASPAL"),
  //   RoadTypeModel("002", "PAVING")
  // ];
  List<RoadTypeModel> _listRoadType = [];
  TextEditingController _controllerRoadType = TextEditingController();
  RoadTypeModel _roadTypeSelected;
  List<ElectricityTypeModel> _listElectricityType = ElectricityType().electricityTypeList;
  ElectricityTypeModel _electricityTypeSelected;
  TextEditingController _controllerElectricityBills = TextEditingController();
  TextEditingController _controllerEndDateLease = TextEditingController();
  TextEditingController _controllerLengthStay= TextEditingController();
  AssetTypeModel _assetTypeTemp;
  String _valueAssetTemp,_surfaceBuildingAreaTemp,_electricityBillsTemp,_endDateLeaseTemp,_lengthStayTemp;
  OwnershipModel _ownershipTemp;
  RoadTypeModel _roadTypeTemp;
  ElectricityTypeModel _electricityTypeTemp;
  bool _flagResultSurveyAsset = false;
  bool _checkDataResultSurveyAsset = false;

  bool get checkDataResultSurveyAsset => _checkDataResultSurveyAsset;

  set checkDataResultSurveyAsset(bool value) {
    this._checkDataResultSurveyAsset = value;
    notifyListeners();
  }

  List<ResultSurveyAssetModel> get listResultSurveyAssetModel => _listResultSurveyAssetModel;

  List<AssetTypeModel> get listAssetType => _listAssetType;

  AssetTypeModel get assetTypeSelected => _assetTypeSelected;

  set assetTypeSelected(AssetTypeModel value) {
    this._assetTypeSelected = value;
    notifyListeners();
  }

  TextEditingController get controllerValueAsset => _controllerValueAsset;

  List<OwnershipModel> get listOwnership => _listOwnership;

  OwnershipModel get ownershipSelected => _ownershipSelected;

  set ownershipSelected(OwnershipModel value) {
    this._ownershipSelected = value;
    setMandatoryTanggalKontrak();
    notifyListeners();
  }

  TextEditingController get controllerSurfaceBuildingArea => _controllerSurfaceBuildingArea;

  List<RoadTypeModel> get listRoadType => _listRoadType;

  RoadTypeModel get roadTypeSelected => _roadTypeSelected;

  set roadTypeSelected(RoadTypeModel value) {
    this._roadTypeSelected = value;
    notifyListeners();
  }

  List<ElectricityTypeModel> get listElectricityType => _listElectricityType;

  ElectricityTypeModel get electricityTypeSelected => _electricityTypeSelected;

  set electricityTypeSelected(ElectricityTypeModel value) {
    this._electricityTypeSelected = value;
    notifyListeners();
  }

  TextEditingController get controllerElectricityBills => _controllerElectricityBills;

  TextEditingController get controllerEndDateLease => _controllerEndDateLease;

  TextEditingController get controllerLengthStay => _controllerLengthStay;

  bool get autoValidateResultSurveyAssetModel => _autoValidateResultSurveyAssetModel;

  set autoValidateResultSurveyAssetModel(bool value) {
    this._autoValidateResultSurveyAssetModel = value;
    notifyListeners();
  }

  GlobalKey<FormState> get keyResultSurveyAsset => _keyResultSurveyAsset;

  bool get flagResultSurveyAsset => _flagResultSurveyAsset;

  set flagResultSurveyAsset(bool value) {
    this._flagResultSurveyAsset = value;
    notifyListeners();
  }

  DateTime get initialEndDateLease => _initialEndDateLease;

  set initialEndDateLease(DateTime value) {
    this._initialEndDateLease = value;
    notifyListeners();
  }

  Future<bool> onBackPressSurveyAsset() async{
    if(this._mandatoryFieldResultSurveyBerhasil) {
      if(this._listResultSurveyAssetModel.isNotEmpty){
        for(int i = 0; i < this._listResultSurveyAssetModel.length; i++) {
          if(this._listResultSurveyAssetModel[i].assetTypeModel.id == "003") {
            this._flagResultSurveyAsset = true;
            this._checkDataResultSurveyAsset = false;
            break;
          } else {
            this._flagResultSurveyAsset = false;
            this._checkDataResultSurveyAsset = true;
          }
        }
      }
      else{
        this._flagResultSurveyAsset = false;
        this._checkDataResultSurveyAsset = true;
      }
    }
    else {
      this._flagResultSurveyAsset = true;
      this._checkDataResultSurveyAsset = false;
    }
    if(this._checkDataResultSurveyAsset) {
      this._messageErrorAsset = "Asset Property wajib ditambahkan.";
    } else {
      this._messageErrorAsset = "";
    }
    notifyListeners();
    return true;
  }

  void clearFieldData() {
    this._controllerEndDateLease.clear();
    // this._initialEndDateLease = null;
  }

  void setMandatoryTanggalKontrak() {
    if(this._ownershipSelected != null) {
      if(this._ownershipSelected.id == "05") {
        this._mandatoryTanggalKontrak = true;
      } else {
        this._mandatoryTanggalKontrak = false;
      }
    } else {
      this._mandatoryTanggalKontrak = false;
    }
  }

  void selectEndDateLease(BuildContext context) async {
    var _dateSelected = await selectDate(context, _initialEndDateLease);
    if (_dateSelected != null) {
      _initialEndDateLease = _dateSelected;
      _controllerEndDateLease.text = dateFormat.format(_dateSelected);
      notifyListeners();
    } else {
      return;
    }
  }

  void addResultSurveyAssetModel(BuildContext context,ResultSurveyAssetModel value){
    this._listResultSurveyAssetModel.add(value);
    notifyListeners();
    Navigator.pop(context);
  }

  void updateResultSurveyAssetModel(BuildContext context,ResultSurveyAssetModel value, int index){
    this._listResultSurveyAssetModel[index] = value;
    notifyListeners();
    Navigator.pop(context);
  }

  void deleteResultSurveyAssetModel(int index){
    this._listResultSurveyAssetModel.removeAt(index);
    notifyListeners();
  }

  AssetTypeModel get assetTypeTemp => _assetTypeTemp;

  String get valueAssetTemp => _valueAssetTemp;

  get surfaceBuildingAreaTemp => _surfaceBuildingAreaTemp;

  get electricityBillsTemp => _electricityBillsTemp;

  get endDateLeaseTemp => _endDateLeaseTemp;

  get lengthStayTemp => _lengthStayTemp;

  OwnershipModel get ownershipTemp => _ownershipTemp;

  ElectricityTypeModel get electricityTypeTemp => _electricityTypeTemp;

  RoadTypeModel get roadTypeTemp => _roadTypeTemp;

  TextEditingController get controllerRoadType => _controllerRoadType;

  set controllerRoadType(TextEditingController value) {
    this._controllerRoadType = value;
    notifyListeners();
  }

  void checkSurveyAsset(BuildContext context,int flag,int index){
    final _form = _keyResultSurveyAsset.currentState;
    if(_form.validate()){
      if(flag == 0){
        addResultSurveyAssetModel(context, ResultSurveyAssetModel(
                "NEW",
                this._assetTypeSelected,
                this._controllerValueAsset.text,
                this._ownershipSelected,
                this._controllerSurfaceBuildingArea.text,
                this._roadTypeSelected,
                this._electricityTypeSelected,
                this._controllerElectricityBills.text,
                this._initialEndDateLease,
                this._controllerLengthStay.text,
                this._isEditResultSurveyAsset,
                this._isAssetTypeChanges,
                this._isValueAssetChanges,
                this._isIdentityInfoChanges,
                this._isSurfaceBuildingAreaInfoChanges,
                this._isRoadTypeInfoChanges,
                this._isElectricityTypeInfoChanges,
                this._isElectricityBillInfoChanges,
                this._isEndDateInfoChanges,
                this._isLengthStayInfoChanges
            )
        );
      }
      else{
        updateResultSurveyAssetModel(context, ResultSurveyAssetModel(
                this._surveyResultAssetID,
                this._assetTypeSelected,
                this._controllerValueAsset.text,
                this._ownershipSelected,
                this._controllerSurfaceBuildingArea.text,
                this._roadTypeSelected,
                this._electricityTypeSelected,
                this._controllerElectricityBills.text,
                this._initialEndDateLease,
                this._controllerLengthStay.text,
                this._isEditResultSurveyAsset,
                this._isAssetTypeChanges,
                this._isValueAssetChanges,
                this._isIdentityInfoChanges,
                this._isSurfaceBuildingAreaInfoChanges,
                this._isRoadTypeInfoChanges,
                this._isElectricityTypeInfoChanges,
                this._isElectricityBillInfoChanges,
                this._isEndDateInfoChanges,
                this._isLengthStayInfoChanges
            ), index
        );
        checkDataDakorSurveyAsset(index);
      }
    }
    else{
      autoValidateResultSurveyAssetModel = true;
    }
  }

  void setValueDefaultSurveyAsset() async{
    this._listAssetType.clear();
    _listAssetType = [
      AssetTypeModel("001", "MOTOR"),
      AssetTypeModel("002", "MOBIL"),
      AssetTypeModel("003", "PROPERTY"),
    ];
    for(int i = 0; i <this._listAssetType.length; i++){
      if(this._listAssetType[i].id == "003"){
        this._assetTypeSelected = this._listAssetType[i];
      }
    }
  }

  void clearDataSurveyAsset(){
    this._autoValidateResultSurveyAssetModel = false;
    this._mandatoryTanggalKontrak = false;
    this._flagResultSurveyAsset = false;
    this._assetTypeSelected = null;
    this._controllerValueAsset.clear();
    this._ownershipSelected = null;
    this._controllerSurfaceBuildingArea.clear();
    this._roadTypeSelected = null;
    this._electricityTypeSelected = null;
    this._controllerElectricityBills.clear();
    this._controllerEndDateLease.clear();
    this._initialEndDateLease = null;
    this._controllerLengthStay.clear();
  }

  Future<void> getRoadType(ResultSurveyAssetModel value,int flag,int index) async{
    clearDataSurveyAsset();
    this._listRoadType.clear();
    final ioc = new HttpClient();
    ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;

    final _http = IOClient(ioc);
    String _fieldJenisJalan = await storage.read(key: "FieldJenisJalan");
    final _response = await _http.get(
        "${BaseUrl.urlGeneral}$_fieldJenisJalan",
        headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
        // "${urlPublic}jenis-rehab/get_street_type"
    );
    final _data = jsonDecode(_response.body);
    if(_response.statusCode == 200){
      for(int i=0; i < _data['data'].length; i++){
        this._listRoadType.add(RoadTypeModel(_data['data'][i]['kode'], _data['data'][i]['deskripsi']));
      }
      // if(_listRoadType.isNotEmpty){
      setValueEditSurveyAsset(value,flag,index);
      // }
    }
    else{
      showSnackBar("Error ${_response.statusCode}");
    }
    // setValueDefaultAsset();
    notifyListeners();
  }

  void setValueEditSurveyAsset(ResultSurveyAssetModel value,int flag,int index) async{
    if(flag != 0){
      // this._listAssetType.clear();
      // _listAssetType = [
      //   AssetTypeModel("001", "MOTOR"),
      //   AssetTypeModel("002", "MOBIL"),
      //   AssetTypeModel("003", "PROPERTY"),
      // ];
      for(int i=0; i < this._listAssetType.length; i++){
        if(_listAssetType[i].id == value.assetTypeModel.id){
          this._assetTypeSelected = _listAssetType[i];
          this._assetTypeTemp = _listAssetType[i];
        }
      }

      this._controllerValueAsset.text = value.valueAsset;
      this._valueAssetTemp = this._controllerValueAsset.text;

      for(int i=0; i < this._listOwnership.length; i++){
        if(_listOwnership[i].id == value.ownershipModel.id){
          this._ownershipSelected = _listOwnership[i];
          this._ownershipTemp = _listOwnership[i];
        }
      }
      if(value.assetTypeModel.id == "003"){
        this._controllerSurfaceBuildingArea.text = value.surfaceBuildingArea;
        this._surfaceBuildingAreaTemp = this._controllerSurfaceBuildingArea.text;

        for(int i=0; i < _listRoadType.length; i++){
          if(_listRoadType[i].kode == value.roadTypeModel.kode){
            this._roadTypeSelected = _listRoadType[i];
            this._roadTypeTemp = _listRoadType[i];
          }
        }

        for(int j=0; j < _listElectricityType.length; j++){
          if(_listElectricityType[j].PARA_ELECTRICITY_ID == value.electricityTypeModel.PARA_ELECTRICITY_ID){
            this._electricityTypeSelected = _listElectricityType[j];
            this._electricityTypeTemp = _listElectricityType[j];
          }
        }

        this._controllerElectricityBills.text = value.electricityBills;
        this._electricityBillsTemp = this._controllerElectricityBills.text;

        if(value.endDateLease != null){
          this._initialEndDateLease = value.endDateLease;
          this._controllerEndDateLease.text = dateFormat.format(this._initialEndDateLease);
          this._endDateLeaseTemp = this._controllerEndDateLease.text;
        }

        this._controllerLengthStay.text = value.lengthStay;
        this._lengthStayTemp = this._controllerLengthStay.text;
      }
      formattingSurveyAsset();
      checkDataDakorSurveyAsset(index);
    }
    else{
      for(int i = 0; i <this._listAssetType.length; i++){
        if(this._listAssetType[i].id == "003"){
          this._assetTypeSelected = this._listAssetType[i];
        }
      }
    }
  }

  void saveToSQLiteMS2SurveyAsset(BuildContext context) async {
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    List<MS2SvyRsltAsstModel> _list = [];
    if(_listResultSurveyAssetModel.isNotEmpty) {
      for(int i=0; i < _listResultSurveyAssetModel.length; i++){
        _list.add(MS2SvyRsltAsstModel(
            null,
            this._listResultSurveyAssetModel[i].surveyResultAssetID != null ? this._listResultSurveyAssetModel[i].surveyResultAssetID : "NEW",
            this._listResultSurveyAssetModel[i].assetTypeModel != null ? this._listResultSurveyAssetModel[i].assetTypeModel : "",
            this._listResultSurveyAssetModel[i].valueAsset != null ? this._listResultSurveyAssetModel[i].valueAsset : "",
            this._listResultSurveyAssetModel[i].ownershipModel != null ? this._listResultSurveyAssetModel[i].ownershipModel : "",
            this._listResultSurveyAssetModel[i].surfaceBuildingArea != null ? this._listResultSurveyAssetModel[i].surfaceBuildingArea : "",
            this._listResultSurveyAssetModel[i].roadTypeModel != null ? this._listResultSurveyAssetModel[i].roadTypeModel : null,
            this._listResultSurveyAssetModel[i].electricityTypeModel != null ? this._listResultSurveyAssetModel[i].electricityTypeModel : null,
            this._listResultSurveyAssetModel[i].electricityBills != "" ? double.parse(this._listResultSurveyAssetModel[i].electricityBills.replaceAll(",", "")) : 0,
            this._listResultSurveyAssetModel[i].endDateLease.toString() != "" ? this._listResultSurveyAssetModel[i].endDateLease.toString() : "",
            this._listResultSurveyAssetModel[i].lengthStay != "" ? int.parse( this._listResultSurveyAssetModel[i].lengthStay) : 0,
            1,
            DateTime.now().toString(),
            _preferences.getString("username"),
            null,
            null,
            this._listResultSurveyAssetModel[i].isEditResultSurveyAsset ? "1": "0",
            this._listResultSurveyAssetModel[i].isAssetTypeChanges ? "1": "0",
            this._listResultSurveyAssetModel[i].isValueAssetChanges ? "1": "0",
            this._listResultSurveyAssetModel[i].isOwneshipInfoChanges ? "1": "0",
            this._listResultSurveyAssetModel[i].isSurfaceBuildingAreaInfoChanges ? "1": "0",
            this._listResultSurveyAssetModel[i].isRoadTypeInfoChanges ? "1": "0",
            this._listResultSurveyAssetModel[i].isElectricityTypeInfoChanges ? "1": "0",
            this._listResultSurveyAssetModel[i].isElectricityBillInfoChanges ? "1": "0",
            this._listResultSurveyAssetModel[i].isEndDateInfoChanges ? "1": "0",
            this._listResultSurveyAssetModel[i].isLengthStayInfoChanges ? "1": "0",
        ));
      }
    }
    _dbHelper.insertMS2SvyRsltAsst(_list);
  }

  void insertDataSurveyAsset(BuildContext context) async{
    if(await _dbHelper.deleteResultSurveyAsset()){
      saveToSQLiteMS2SurveyAsset(context);
    }
  }

  Future<void> selectDataSurveyAsset() async{
    List _data = await _dbHelper.selectResultSurveyAsset();
    if(_data.isNotEmpty){
      print("set data asset survey");
      this._listResultSurveyAssetModel.clear();
      for(int i=0; i < _data.length; i++){
        this._listResultSurveyAssetModel.add(
            ResultSurveyAssetModel(
                _data[i]['surveyResultAssetID'].toString(),
                AssetTypeModel(_data[i]['asset_type'], _data[i]['asset_type_desc']),
                _data[i]['asset_amt'].toString(),
                OwnershipModel(_data[i]['asset_own'], _data[i]['asset_own_desc']),
                _data[i]['size_of_land'] != "null" && _data[i]['size_of_land'] != "" ? _data[i]['size_of_land'] : "",
                _data[i]['stree_type'] != "null" &&  _data[i]['stree_type'] != "" ? RoadTypeModel(_data[i]['stree_type'], _data[i]['street_type_desc']) : null,
                _data[i]['electricity'] != "null" &&  _data[i]['electricity'] != "" ? ElectricityTypeModel(_data[i]['electricity'], _data[i]['electricity_desc']) : null,
                _data[i]['electricity_bill'].toString(),
                _data[i]['expired_date_contract'] != "" && _data[i]['expired_date_contract'] != "null" ? DateTime.parse(_data[i]['expired_date_contract']) : null,
                _data[i]['no_of_stay'],
                _data[i]['edit_asset_type'] == "1" || _data[i]['edit_asset_amt'] == "1"
                    || _data[i]['edit_asset_own'] == "1" || _data[i]['edit_size_of_land'] == "1"
                    || _data[i]['edit_stree_type'] == "1" || _data[i]['edit_electricity'] == "1"
                    || _data[i]['edit_electricity_bill'] == "1" || _data[i]['edit_expired_date_contract'] == "1"
                    || _data[i]['edit_no_of_stay'] == "1",
                _data[i]['edit_asset_type'] == "1",
                _data[i]['edit_asset_amt'] == "1",
                _data[i]['edit_asset_own'] == "1",
                _data[i]['edit_size_of_land'] == "1",
                _data[i]['edit_stree_type'] == "1",
                _data[i]['edit_electricity'] == "1",
                _data[i]['edit_electricity_bill'] == "1",
                _data[i]['edit_expired_date_contract'] == "1",
                _data[i]['edit_no_of_stay'] == "1"
            )
        );
      }
    }
    formattingSurveyAsset();
    notifyListeners();
  }

  void checkDataDakorSurveyAsset(int index) async{
    List _data = await _dbHelper.selectResultSurveyAsset();
    if(_data.isNotEmpty && this._typeForm == "DKR"){
      isAssetTypeChanges = _data[index]['asset_type'] != _assetTypeSelected.id || _data[index]['edit_asset_type'] == "1";
      isValueAssetChanges = _data[index]['asset_amt'] != double.parse(this._controllerValueAsset.text.replaceAll(",", "")) || _data[index]['edit_asset_amt'] == "1";
      isIdentityInfoChanges = _data[index]['asset_own'] != this._ownershipSelected.id || _data[index]['edit_asset_own'] == "1";
      if(_data[index]['asset_type'] == "003"){
        isSurfaceBuildingAreaInfoChanges = _data[index]['size_of_land'] != this._controllerSurfaceBuildingArea.text || _data[index]['edit_size_of_land'] == "1";
        isRoadTypeInfoChanges = _data[index]['stree_type'] != _roadTypeSelected.kode || _data[index]['edit_stree_type'] == "1";
        isElectricityTypeInfoChanges = _data[index]['electricity'] != _electricityTypeSelected.PARA_ELECTRICITY_ID || _data[index]['edit_electricity'] == "1";
        isElectricityBillInfoChanges = _data[index]['electricity_bill'] != double.parse(this._controllerElectricityBills.text.replaceAll(",", "")) || _data[index]['edit_electricity_bill'] == "1";
        isEndDateInfoChanges = _data[index]['expired_date_contract'] != "null" && _data[index]['expired_date_contract'] != "" ? DateTime.parse(_data[index]['expired_date_contract']) != _initialEndDateLease : false || _data[index]['edit_expired_date_contract'] == "1";
        isLengthStayInfoChanges = _data[index]['no_of_stay'] != _controllerLengthStay.text || _data[index]['edit_no_of_stay'] == "1";
      }
      if(this._isAssetTypeChanges || this._isValueAssetChanges || this._isIdentityInfoChanges
          || this._isSurfaceBuildingAreaInfoChanges || this._isRoadTypeInfoChanges
          || this._isElectricityTypeInfoChanges || this._isElectricityBillInfoChanges
          || this._isEndDateInfoChanges || this._isLengthStayInfoChanges){
        isEditResultSurveyAsset = true;
      }
      else{
        isEditResultSurveyAsset = false;
      }
    }
  }

  void formattingSurveyAsset() {
    // Asset
    this._controllerValueAsset.text = formatCurrency.formatCurrency2(_controllerValueAsset.text);
    this._controllerElectricityBills.text = formatCurrency.formatCurrency2(_controllerElectricityBills.text);
  }

  /*
  *
  * Hasil Survey - Foto Survey
  *
  * */
  final _imagePicker = ImagePicker();
  List<PhotoTypeModel> _listPhotoType = [
    PhotoTypeModel("SC1", "UNIT"),
    PhotoTypeModel("SC2", "RUMAH"),
    PhotoTypeModel("SC3", "USAHA"),
    PhotoTypeModel("SC4", "SELFIE"),
    // PhotoTypeModel("001", "DOKUMEN"),
    // PhotoTypeModel("002", "RUMAH"),
    // PhotoTypeModel("003", "USAHA"),
  ];
  PhotoTypeModel _photoTypeSelected;
  List<ImageFileModel> _listPhoto = [];
  bool _autoValidateResultSurveyPhoto = false;
  bool _flagResultSurveyPhoto = false;

  PhotoTypeModel get photoTypeSelected => _photoTypeSelected;

  set photoTypeSelected(PhotoTypeModel value) {
    this._photoTypeSelected = value;
    this._validatePhoto = false;
    notifyListeners();
  }

  List<PhotoTypeModel> get listPhotoType => _listPhotoType;

  List<ImageFileModel> get listPhoto => _listPhoto;

  bool get autoValidateResultSurveyPhoto => _autoValidateResultSurveyPhoto;

  set autoValidateResultSurveyPhoto(bool value) {
    this._autoValidateResultSurveyPhoto = value;
    notifyListeners();
  }

  bool get flagResultSurveyPhoto => _flagResultSurveyPhoto;

  set flagResultSurveyPhoto(bool value) {
    this._flagResultSurveyPhoto = value;
    notifyListeners();
  }

  String get orderSupportingDocumentID => _orderSupportingDocumentID;

  set orderSupportingDocumentID(String value) {
    this._orderSupportingDocumentID = value;
  }

  void addFile(BuildContext context) async {
    var _image = await _imagePicker.getImage(
        source: ImageSource.camera,
        maxHeight: 1920.0,
        maxWidth: 1080.0,
        imageQuality: 10);
    savePhoto(context, _image);
    // if (_image != null) {
    //   File _imageFile = File(_image.path);
    //   Position _position = await Geolocator().getCurrentPosition(
    //       desiredAccuracy: LocationAccuracy.bestForNavigation);
    //
    //   this._listPhoto.add(ImageFileModel(
    //       _imageFile, _position.latitude, _position.longitude, ""));
    //   notifyListeners();
    // } else {
    //   return;
    // }
  }

  void actionDetailPhoto(String action,int index,BuildContext context){
    if(TitlePopUpMenuButton.Delete == action){
      deletePhoto(index);
    }
    else{
      Navigator.push(context, MaterialPageRoute(builder: (context) => DetailImage(imageFile: this._listPhoto[index].imageFile),));
    }
  }

  void deletePhoto(int index){
    this._listPhoto.removeAt(index);
    notifyListeners();
  }

  void clearListPhoto(){
    this._listPhoto.clear();
    notifyListeners();
  }

  Future<bool> onBackPressResultSurveyPhotoSurvey() async{
    if (this._listPhoto.length != 0) {
      autoValidateResultSurveyPhoto = false;
    } else {
      autoValidateResultSurveyPhoto = true;
    }
    return true;
  }

  void savePhoto(BuildContext context, PickedFile data) async{
    try{
      if (data != null) {
        var _provider = Provider.of<ListSurveyPhotoChangeNotifier>(context, listen: false);
        String _date = formatDateListOID(DateTime.now()).replaceAll("-", "");
        String _time = formatTime(DateTime.now()).replaceAll(":", "");
        File _imageFile = File(data.path);
        // List<String> split = data.path.split("/");
        // String fileName = split[split.length-1];
        // File _path = await _imageFile.copy("$globalPath/$fileName");
        // _pathFile = _path.path;
        // File _path = await _imageFile.copy("$globalPath/FOTO_SURVEY_${_provider.listSurveyPhoto.length+1}_${this._listPhoto.length+1}_${_date}_$_time.JPG");
        File _path = await _imageFile.copy("$globalPath/FOTO_SURVEY_${_date}_$_time.JPG");
        _pathFile = _path.path;
        Position _position = await Geolocator().getCurrentPosition(desiredAccuracy: LocationAccuracy.bestForNavigation);
        this._listPhoto.add(ImageFileModel(_path, _position.latitude, _position.longitude, this._pathFile, ""));
        notifyListeners();
      } else {
        return;
      }
//      Uint8List bytes = this._fileDocumentUnitObject['file'].readAsBytesSync();
//      await file.writeAsBytes(bytes);
    }
    catch(e){
      print(e);
    }
  }

  Future<void> setValueEditSurveyPhoto(BuildContext context, SurveyPhotoModel model, int flag) async{
    this._listPhoto.clear();
    var _provider = Provider.of<ListSurveyPhotoChangeNotifier>(context, listen: false);
    if(flag != 0){
      for (int i = 0; i < this._listPhotoType.length; i++) {
        if (model.photoTypeModel.id == this._listPhotoType[i].id) {
          this._photoTypeSelected = this._listPhotoType[i];
        }
      }
      for (int i = 0; i < model.listImageModel.length;i++){
        this._listPhoto.add(model.listImageModel[i]);
      }
    }
    else{
      for(int i=0; i<_provider.listSurveyPhoto.length; i++){
        for(int j=0; j<_listPhotoType.length; j++){
          if(_provider.listSurveyPhoto[i].photoTypeModel.id == _listPhotoType[j].id){
            _listPhotoType.removeAt(j);
          }
        }
      }
    }
  }

  void checkSurveyPhoto(BuildContext context, int flag, int index){
    var _provider = Provider.of<ListSurveyPhotoChangeNotifier>(context, listen: false);
    final _form = _keySurveyPhoto.currentState;
    if(this._listPhoto.isEmpty){
      this._validatePhoto = true;
    }
    if(_form.validate() && this._listPhoto.isNotEmpty){
      if (flag == 0) {
        _provider.addSurveyPhoto(SurveyPhotoModel(
            _photoTypeSelected, listPhoto, false, false, false
        ));
      }
      else {
        _provider.updateListSurveyPhoto(index,SurveyPhotoModel(
            _photoTypeSelected, listPhoto, false, false, false
        ));
      }
      Navigator.pop(context);
    }
    else{
      this._autoValidateSurveyPhoto = true;
    }
    notifyListeners();
  }

  void deleteFile(int index) async{
    final file =  File("${this._listPhoto[index].path}");
    await file.delete();
  }

  //ga kepake
  void saveToSQLitePhotoSurvey(){
    List<MS2DocumentModel> _listData = [];
    for(int i=0; i<_listPhoto.length; i++){
      _listData.add(MS2DocumentModel(
          "a",
          _orderSupportingDocumentID,
          "b",
          "c",
          _listPhoto[i].path,
          null,
          null,
          null,
          null,
          _photoTypeSelected.id,
          _photoTypeSelected.name,
          null,
          null,
          null,
          null,
          _listPhoto[i].latitude.toString(),
          _listPhoto[i].longitude.toString()
      ));
    }
    _dbHelper.insertMS2Document(_listData);
  }

  // harus ada penanda untuk membedakan file tipe mana, app document atau photo survey
  // harus ada tambahan kolom penanda di sqlite table ms2document,untuk membedakan file tipe mana, app document atau photo survey,untuk set photo survey dan app document
  //ga kepake
  void setDataFromSQLite() async{
    var _check = await _dbHelper.selectMS2Document();
    if(_check.isNotEmpty){
      for(int i=0; i<_check.length; i++){
        this._orderSupportingDocumentID = _check[i]['_orderSupportingDocumentID'];
        File _imageFile = File(_check[i]['file_name']);
        _listPhoto.add(ImageFileModel(_imageFile,double.parse(_check[i]['latitude']), double.parse(_check[i]['longitude']),_check[i]['file_name'], _check[i]['file_header_id']));
      }
    }
    notifyListeners();
  }

  //Widget Result Survey Group Notes
  ShowMandatoryEnabledResultSurvey _showMandatoryResultEnabledRegularSurvey;
  ShowMandatoryEnabledResultSurvey _showMandatoryResultEnabledPAC;
  ShowMandatoryEnabledResultSurvey _showMandatoryResultEnabledResurvey;
  ShowMandatoryEnabledResultSurvey _showMandatoryResultEnabledDakor;

  ShowMandatoryEnabledResultSurvey _showMandatoryResultEnabledComRegularSurvey;
  ShowMandatoryEnabledResultSurvey _showMandatoryResultEnabledComPAC;
  ShowMandatoryEnabledResultSurvey _showMandatoryResultEnabledComResurvey;
  ShowMandatoryEnabledResultSurvey _showMandatoryResultEnabledComDakor;

  Future<void> setTypeForm() async{
    debugPrint("SLOW");
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    this._typeForm = _preferences.getString("last_known_state");
    this._custType = _preferences.getString("cust_type");
  }

  Future<void> setShowMandatoryResultEnabledGroupNote (BuildContext context) async{
    _showMandatoryResultEnabledRegularSurvey = Provider.of<DashboardChangeNotif>(context, listen: false).showMandatoryResultGroupNoteRegularSurvey;
    _showMandatoryResultEnabledComRegularSurvey = Provider.of<DashboardChangeNotif>(context, listen: false).showMandatoryResultSurveyComRegularSurvey;
    _showMandatoryResultEnabledPAC =  Provider.of<DashboardChangeNotif>(context, listen: false).showMandatoryResultSurveyPAC;
    _showMandatoryResultEnabledResurvey =  Provider.of<DashboardChangeNotif>(context, listen: false).showMandatoryResultSurveyResurvey;
    _showMandatoryResultEnabledDakor =  Provider.of<DashboardChangeNotif>(context, listen: false).showMandatoryResultSurveyDakor;
    _showMandatoryResultEnabledComDakor = Provider.of<DashboardChangeNotif>(context, listen: false).showMandatoryResultSurveyComDakor;
    _showMandatoryResultEnabledComPAC = Provider.of<DashboardChangeNotif>(context, listen: false).showMandatoryResultSurveyComPAC;
    _showMandatoryResultEnabledComResurvey = Provider.of<DashboardChangeNotif>(context, listen: false).showMandatoryResultSurveyComResurvey;
    _showMandatoryResultEnabledComRegularSurvey = Provider.of<DashboardChangeNotif>(context, listen: false).showMandatoryResultSurveyComRegularSurvey;
    await _getRekom();
    await setTypeForm();
  }

  void clearDataSurvey() {
    this._mandatoryFieldResultSurveyBerhasil = false;
    this._messageErrorAsset = "";
    this._listRecomm.clear();

    // Hasil Survey - Catatan
    this._listRecomSurvey.clear();
    this._flagResultSurveyGroupNotes = false;
    this._autoValidateResultSurveyGroupNote = false;
    this._resultSurveySelected = null;
    this._recommendationSurveySelected = null;
    this._controllerNote.clear();
    this._controllerResultSurveyDate.clear();
    this._initialDateResultSurvey = DateTime(dateNow.year,dateNow.month,dateNow.day);
    this._controllerResultSurveyTime.clear();
    this._selectedTime = TimeOfDay.now();

    // Hasil Survey - Lokasi
    this._flagResultSurveyGroupLocation = false;
    this._autoValidateResultSurveyGroupLocation = false;
    this._controllerSurveyType.clear();
    this._controllerDistanceLocationWithSentraUnit.clear();
    this._controllerDistanceLocationWithDealerMerchantAXI.clear();
    this._controllerDistanceLocationWithUsageObjectWithSentraUnit.clear();

    // Hasil Survey - Detail
    this._listResultSurveyDetailSurveyModel.clear();
    this._flagResultSurveyCreateEditDetailSurvey = false;
    this._checkDataResultSurveyAsset = false;
    this._autoValidateResultSurveyCreateEditDetailSurvey = false;
    this._autoValidateSurveyPhoto = false;
    this._validatePhoto = false;
    this._environmentalInformationSelected = null;
    this._resourcesInfoSurveySelected = null;
    this._controllerResourceInformationName.clear();

    // Hasil Survey - Aset
    this._listResultSurveyAssetModel.clear();
    this._flagResultSurveyAsset = false;
    this._checkDataResultSurveyAsset = false;
    this._autoValidateResultSurveyAssetModel = false;
    this._assetTypeSelected = null;
    this._controllerValueAsset.clear();
    this._ownershipSelected = null;
    this._controllerSurfaceBuildingArea.clear();
    this._roadTypeSelected = null;
    this._electricityTypeSelected = null;
    this._controllerElectricityBills.clear();
    this._controllerEndDateLease.clear();
    this._controllerLengthStay.clear();

    // Hasil Survey - Foto Survey
    this._listPhoto.clear();
    this._autoValidateResultSurveyPhoto = false;
    this._photoTypeSelected = null;
    this._listPhoto.clear();
  }
  //Widget Result Survey Group Notes

  //Result Survey
  bool isResultSurveyVisible(){
    bool value;
    if(_custType == "PER") {
      if (_typeForm == "SRE") {
        value = _showMandatoryResultEnabledRegularSurvey.isResultSurveyGroupNotesVisible;
      } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
        value = _showMandatoryResultEnabledPAC.isResultSurveyGroupNotesVisible;
      } else if (_typeForm == "RSVY") {
        value = _showMandatoryResultEnabledResurvey.isResultSurveyGroupNotesVisible;
      } else if (_typeForm == "DKR") {
        value = _showMandatoryResultEnabledDakor.isResultSurveyGroupNotesVisible;
      }
    }
    else
    if (_typeForm == "SRE") {
      value = _showMandatoryResultEnabledComRegularSurvey.isResultSurveyGroupNotesVisible;
    } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
      value = _showMandatoryResultEnabledComPAC.isResultSurveyGroupNotesVisible;
    } else if (_typeForm == "RSVY") {
      value = _showMandatoryResultEnabledComResurvey.isResultSurveyGroupNotesVisible;
    } else if (_typeForm == "DKR") {
      value = _showMandatoryResultEnabledComDakor.isResultSurveyGroupNotesVisible;
    }
    return value;
  }

  bool isResultSurveyMandatory(){
    bool value;
    if(_custType == "PER") {
      if (_typeForm == "SRE") {
        value = _showMandatoryResultEnabledRegularSurvey.isResultSurveyGroupNotesMandatory;
      } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
        value = _showMandatoryResultEnabledPAC.isResultSurveyGroupNotesMandatory;
      } else if (_typeForm == "RSVY") {
        value = _showMandatoryResultEnabledResurvey.isResultSurveyGroupNotesMandatory;
      } else if (_typeForm == "DKR") {
        value = _showMandatoryResultEnabledDakor.isResultSurveyGroupNotesMandatory;
      }
    }
    else
    if (_typeForm == "SRE") {
      value = _showMandatoryResultEnabledComRegularSurvey.isResultSurveyGroupNotesMandatory;
    } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
      value = _showMandatoryResultEnabledComPAC.isResultSurveyGroupNotesMandatory;
    } else if (_typeForm == "RSVY") {
      value = _showMandatoryResultEnabledComResurvey.isResultSurveyGroupNotesMandatory;
    } else if (_typeForm == "DKR") {
      value = _showMandatoryResultEnabledComDakor.isResultSurveyGroupNotesMandatory;
    }
    return value;
  }

  bool isResultSurveyEnabled(){
    bool value;
    if(_custType == "PER") {
      if (_typeForm == "SRE") {
        value = _showMandatoryResultEnabledRegularSurvey.isResultSurveyGroupNotesEnabled;
      } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
        value = _showMandatoryResultEnabledPAC.isResultSurveyGroupNotesEnabled;
      } else if (_typeForm == "RSVY") {
        value = _showMandatoryResultEnabledResurvey.isResultSurveyGroupNotesEnabled;
      } else if (_typeForm == "DKR") {
        value = _showMandatoryResultEnabledDakor.isResultSurveyGroupNotesEnabled;
      }
    }
    else
    if (_typeForm == "SRE") {
      value = _showMandatoryResultEnabledComRegularSurvey.isResultSurveyGroupNotesEnabled;
    } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
      value = _showMandatoryResultEnabledComPAC.isResultSurveyGroupNotesEnabled;
    } else if (_typeForm == "RSVY") {
      value = _showMandatoryResultEnabledComResurvey.isResultSurveyGroupNotesEnabled;
    } else if (_typeForm == "DKR") {
      value = _showMandatoryResultEnabledComDakor.isResultSurveyGroupNotesEnabled;
    }
    return value;
  }

  //Result Survey Group Notes
  bool isResultSurveyGroupNotesVisible(){
    bool value;
    if(_custType == "PER") {
      if (_typeForm == "SRE") {
        value = _showMandatoryResultEnabledRegularSurvey.isResultSurveyGroupNotesVisible;
      } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
        value = _showMandatoryResultEnabledPAC.isResultSurveyGroupNotesVisible;
      } else if (_typeForm == "RSVY") {
        value = _showMandatoryResultEnabledResurvey.isResultSurveyGroupNotesVisible;
      } else if (_typeForm == "DKR") {
        value = _showMandatoryResultEnabledDakor.isResultSurveyGroupNotesVisible;
      }
    }
    else
    if (_typeForm == "SRE") {
      value = _showMandatoryResultEnabledComRegularSurvey.isResultSurveyGroupNotesVisible;
    } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
      value = _showMandatoryResultEnabledComPAC.isResultSurveyGroupNotesVisible;
    } else if (_typeForm == "RSVY") {
      value = _showMandatoryResultEnabledComResurvey.isResultSurveyGroupNotesVisible;
    } else if (_typeForm == "DKR") {
      value = _showMandatoryResultEnabledComDakor.isResultSurveyGroupNotesVisible;
    }
    return value;
  }

  bool isResultSurveyGroupNotesMandatory(){
    bool value;
    if(_custType == "PER") {
      if (_typeForm == "SRE") {
        value = _showMandatoryResultEnabledRegularSurvey.isResultSurveyGroupNotesMandatory;
      } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
        value = _showMandatoryResultEnabledPAC.isResultSurveyGroupNotesMandatory;
      } else if (_typeForm == "RSVY") {
        value = _showMandatoryResultEnabledResurvey.isResultSurveyGroupNotesMandatory;
      } else if (_typeForm == "DKR") {
        value = _showMandatoryResultEnabledDakor.isResultSurveyGroupNotesMandatory;
      }
    }
    else
    if (_typeForm == "SRE") {
      value = _showMandatoryResultEnabledComRegularSurvey.isResultSurveyGroupNotesMandatory;
    } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
      value = _showMandatoryResultEnabledComPAC.isResultSurveyGroupNotesMandatory;
    } else if (_typeForm == "RSVY") {
      value = _showMandatoryResultEnabledComResurvey.isResultSurveyGroupNotesMandatory;
    } else if (_typeForm == "DKR") {
      value = _showMandatoryResultEnabledComDakor.isResultSurveyGroupNotesMandatory;
    }
    return value;
  }

  bool isResultSurveyGroupNotesEnabled(){
    bool value;
    if(_custType == "PER") {
      if (_typeForm == "SRE") {
        value = _showMandatoryResultEnabledRegularSurvey.isResultSurveyGroupNotesEnabled;
      } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
        value = _showMandatoryResultEnabledPAC.isResultSurveyGroupNotesEnabled;
      } else if (_typeForm == "RSVY") {
        value = _showMandatoryResultEnabledResurvey.isResultSurveyGroupNotesEnabled;
      } else if (_typeForm == "DKR") {
        value = _showMandatoryResultEnabledDakor.isResultSurveyGroupNotesEnabled;
      }
    }
    else
    if (_typeForm == "SRE") {
      value = _showMandatoryResultEnabledComRegularSurvey.isResultSurveyGroupNotesEnabled;
    } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
      value = _showMandatoryResultEnabledComPAC.isResultSurveyGroupNotesEnabled;
    } else if (_typeForm == "RSVY") {
      value = _showMandatoryResultEnabledComResurvey.isResultSurveyGroupNotesEnabled;
    } else if (_typeForm == "DKR") {
      value = _showMandatoryResultEnabledComDakor.isResultSurveyGroupNotesEnabled;
    }
    return value;
  }

  //Survey Recommendation
  bool isSurveyRecommendationVisible(){
    bool value;
    if(_custType == "PER") {
      if (_typeForm == "SRE") {
        value = _showMandatoryResultEnabledRegularSurvey.isSurveyRecommendationVisible;
      } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
        value = _showMandatoryResultEnabledPAC.isSurveyRecommendationVisible;
      } else if (_typeForm == "RSVY") {
        value = _showMandatoryResultEnabledResurvey.isSurveyRecommendationVisible;
      } else if (_typeForm == "DKR") {
        value = _showMandatoryResultEnabledDakor.isSurveyRecommendationVisible;
      }
    }
    else
    if (_typeForm == "SRE") {
      value = _showMandatoryResultEnabledComRegularSurvey.isSurveyRecommendationVisible;
    } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
      value = _showMandatoryResultEnabledComPAC.isSurveyRecommendationVisible;
    } else if (_typeForm == "RSVY") {
      value = _showMandatoryResultEnabledComResurvey.isSurveyRecommendationVisible;
    } else if (_typeForm == "DKR") {
      value = _showMandatoryResultEnabledComDakor.isSurveyRecommendationVisible;
    }
    return value;
  }

  bool isSurveyRecommendationMandatory(){
    bool value;
    if(_custType == "PER") {
      if (_typeForm == "SRE") {
        value = _showMandatoryResultEnabledRegularSurvey.isSurveyRecommendationMandatory;
      } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
        value = _showMandatoryResultEnabledPAC.isSurveyRecommendationMandatory;
      } else if (_typeForm == "RSVY") {
        value = _showMandatoryResultEnabledResurvey.isSurveyRecommendationMandatory;
      } else if (_typeForm == "DKR") {
        value = _showMandatoryResultEnabledDakor.isSurveyRecommendationMandatory;
      }
    }
    else
    if (_typeForm == "SRE") {
      value = _showMandatoryResultEnabledComRegularSurvey.isSurveyRecommendationMandatory;
    } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
      value = _showMandatoryResultEnabledComPAC.isSurveyRecommendationMandatory;
    } else if (_typeForm == "RSVY") {
      value = _showMandatoryResultEnabledComResurvey.isSurveyRecommendationMandatory;
    } else if (_typeForm == "DKR") {
      value = _showMandatoryResultEnabledComDakor.isSurveyRecommendationMandatory;
    }
    return value;
  }

  bool isSurveyRecommendationEnabled(){
    bool value;
    if(_custType == "PER") {
      if (_typeForm == "SRE") {
        value = _showMandatoryResultEnabledRegularSurvey.isSurveyRecommendationEnabled;
      } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
        value = _showMandatoryResultEnabledPAC.isSurveyRecommendationEnabled;
      } else if (_typeForm == "RSVY") {
        value = _showMandatoryResultEnabledResurvey.isSurveyRecommendationEnabled;
      } else if (_typeForm == "DKR") {
        value = _showMandatoryResultEnabledDakor.isSurveyRecommendationEnabled;
      }
    }
    else
    if (_typeForm == "SRE") {
      value = _showMandatoryResultEnabledComRegularSurvey.isSurveyRecommendationEnabled;
    } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
      value = _showMandatoryResultEnabledComPAC.isSurveyRecommendationEnabled;
    } else if (_typeForm == "RSVY") {
      value = _showMandatoryResultEnabledComResurvey.isSurveyRecommendationEnabled;
    } else if (_typeForm == "DKR") {
      value = _showMandatoryResultEnabledComDakor.isSurveyRecommendationEnabled;
    }
    return value;
  }

  //Survey Notes
  bool isSurveyNotesVisible(){
    bool value;
    if(_custType == "PER") {
      if (_typeForm == "SRE") {
        value = _showMandatoryResultEnabledRegularSurvey.isSurveyNotesVisible;
      } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
        value = _showMandatoryResultEnabledPAC.isSurveyNotesVisible;
      } else if (_typeForm == "RSVY") {
        value = _showMandatoryResultEnabledResurvey.isSurveyNotesVisible;
      } else if (_typeForm == "DKR") {
        value = _showMandatoryResultEnabledDakor.isSurveyNotesVisible;
      }
    }
    else
    if (_typeForm == "SRE") {
      value = _showMandatoryResultEnabledComRegularSurvey.isSurveyNotesVisible;
    } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
      value = _showMandatoryResultEnabledComPAC.isSurveyNotesVisible;
    } else if (_typeForm == "RSVY") {
      value = _showMandatoryResultEnabledComResurvey.isSurveyNotesVisible;
    } else if (_typeForm == "DKR") {
      value = _showMandatoryResultEnabledComDakor.isSurveyNotesVisible;
    }
    return value;
  }

  bool isSurveyNotesMandatory(){
    bool value;
    if(_custType == "PER") {
      if (_typeForm == "SRE") {
        value = _showMandatoryResultEnabledRegularSurvey.isSurveyNotesMandatory;
      } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
        value = _showMandatoryResultEnabledPAC.isSurveyNotesMandatory;
      } else if (_typeForm == "RSVY") {
        value = _showMandatoryResultEnabledResurvey.isSurveyNotesMandatory;
      } else if (_typeForm == "DKR") {
        value = _showMandatoryResultEnabledDakor.isSurveyNotesMandatory;
      }
    }
    else
    if (_typeForm == "SRE") {
      value = _showMandatoryResultEnabledComRegularSurvey.isSurveyNotesMandatory;
    } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
      value = _showMandatoryResultEnabledComPAC.isSurveyNotesMandatory;
    } else if (_typeForm == "RSVY") {
      value = _showMandatoryResultEnabledComResurvey.isSurveyNotesMandatory;
    } else if (_typeForm == "DKR") {
      value = _showMandatoryResultEnabledComDakor.isSurveyNotesMandatory;
    }
    return value;
  }

  bool isSurveyNotesEnabled(){
    bool value;
    if(_custType == "PER") {
      if (_typeForm == "SRE") {
        value = _showMandatoryResultEnabledRegularSurvey.isSurveyNotesEnabled;
      } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
        value = _showMandatoryResultEnabledPAC.isSurveyNotesEnabled;
      } else if (_typeForm == "RSVY") {
        value = _showMandatoryResultEnabledResurvey.isSurveyNotesEnabled;
      } else if (_typeForm == "DKR") {
        value = _showMandatoryResultEnabledDakor.isSurveyNotesEnabled;
      }
    }
    else
    if (_typeForm == "SRE") {
      value = _showMandatoryResultEnabledComRegularSurvey.isSurveyNotesEnabled;
    } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
      value = _showMandatoryResultEnabledComPAC.isSurveyNotesEnabled;
    } else if (_typeForm == "RSVY") {
      value = _showMandatoryResultEnabledComResurvey.isSurveyNotesEnabled;
    } else if (_typeForm == "DKR") {
      value = _showMandatoryResultEnabledComDakor.isSurveyNotesEnabled;
    }
    return value;
  }

  //Survey Date
  bool isSurveyDateVisible(){
    bool value;
    if(_custType == "PER") {
      if (_typeForm == "SRE") {
        value = _showMandatoryResultEnabledRegularSurvey.isSurveyDateVisible;
      } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
        value = _showMandatoryResultEnabledPAC.isSurveyDateVisible;
      } else if (_typeForm == "RSVY") {
        value = _showMandatoryResultEnabledResurvey.isSurveyDateVisible;
      } else if (_typeForm == "DKR") {
        value = _showMandatoryResultEnabledDakor.isSurveyDateVisible;
      }
    }
    else
    if (_typeForm == "SRE") {
      value = _showMandatoryResultEnabledComRegularSurvey.isSurveyDateVisible;
    } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
      value = _showMandatoryResultEnabledComPAC.isSurveyDateVisible;
    } else if (_typeForm == "RSVY") {
      value = _showMandatoryResultEnabledComResurvey.isSurveyDateVisible;
    } else if (_typeForm == "DKR") {
      value = _showMandatoryResultEnabledComDakor.isSurveyDateVisible;
    }
    return value;
  }

  bool isSurveyDateMandatory(){
    bool value;
    if(_custType == "PER") {
      if (_typeForm == "SRE") {
        value = _showMandatoryResultEnabledRegularSurvey.isSurveyDateMandatory;
      } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
        value = _showMandatoryResultEnabledPAC.isSurveyDateMandatory;
      } else if (_typeForm == "RSVY") {
        value = _showMandatoryResultEnabledResurvey.isSurveyDateMandatory;
      } else if (_typeForm == "DKR") {
        value = _showMandatoryResultEnabledDakor.isSurveyDateMandatory;
      }
    }
    else
    if (_typeForm == "SRE") {
      value = _showMandatoryResultEnabledComRegularSurvey.isSurveyDateMandatory;
    } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
      value = _showMandatoryResultEnabledComPAC.isSurveyDateMandatory;
    } else if (_typeForm == "RSVY") {
      value = _showMandatoryResultEnabledComResurvey.isSurveyDateMandatory;
    } else if (_typeForm == "DKR") {
      value = _showMandatoryResultEnabledComDakor.isSurveyDateMandatory;
    }
    return value;
  }

  bool isSurveyDateEnabled(){
    bool value;
    if(_custType == "PER") {
      if (_typeForm == "SRE") {
        value = _showMandatoryResultEnabledRegularSurvey.isSurveyDateEnabled;
      } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
        value = _showMandatoryResultEnabledPAC.isSurveyDateEnabled;
      } else if (_typeForm == "RSVY") {
        value = _showMandatoryResultEnabledResurvey.isSurveyDateEnabled;
      } else if (_typeForm == "DKR") {
        value = _showMandatoryResultEnabledDakor.isSurveyDateEnabled;
      }
    }
    else
    if (_typeForm == "SRE") {
      value = _showMandatoryResultEnabledComRegularSurvey.isSurveyDateEnabled;
    } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
      value = _showMandatoryResultEnabledComPAC.isSurveyDateEnabled;
    } else if (_typeForm == "RSVY") {
      value = _showMandatoryResultEnabledComResurvey.isSurveyDateEnabled;
    } else if (_typeForm == "DKR") {
      value = _showMandatoryResultEnabledComDakor.isSurveyDateEnabled;
    }
    return value;
  }

  //Widget Result Survey Photo
  bool isPhotoTypeVisible(){
    bool value;
    if(_custType == "PER") {
      if (_typeForm == "SRE") {
        value = _showMandatoryResultEnabledRegularSurvey.isPhotoTypeVisible;
      } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
        value = _showMandatoryResultEnabledPAC.isPhotoTypeVisible;
      } else if (_typeForm == "RSVY") {
        value = _showMandatoryResultEnabledResurvey.isPhotoTypeVisible;
      } else if (_typeForm == "DKR") {
        value = _showMandatoryResultEnabledDakor.isPhotoTypeVisible;
      }
    }
    else
    if (_typeForm == "SRE") {
      value = _showMandatoryResultEnabledComRegularSurvey.isPhotoTypeVisible;
    } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
      value = _showMandatoryResultEnabledComPAC.isPhotoTypeVisible;
    } else if (_typeForm == "RSVY") {
      value = _showMandatoryResultEnabledComResurvey.isPhotoTypeVisible;
    } else if (_typeForm == "DKR") {
      value = _showMandatoryResultEnabledComDakor.isPhotoTypeVisible;
    }
    return value;
  }

  bool isPhotoTypeMandatory(){
    bool value;
    if(_custType == "PER") {
      if (_typeForm == "SRE") {
        value = _showMandatoryResultEnabledRegularSurvey.isPhotoTypeMandatory;
      } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
        value = _showMandatoryResultEnabledPAC.isPhotoTypeMandatory;
      } else if (_typeForm == "RSVY") {
        value = _showMandatoryResultEnabledResurvey.isPhotoTypeMandatory;
      } else if (_typeForm == "DKR") {
        value = _showMandatoryResultEnabledDakor.isPhotoTypeMandatory;
      }
    }
    else
    if (_typeForm == "SRE") {
      value = _showMandatoryResultEnabledComRegularSurvey.isPhotoTypeMandatory;
    } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
      value = _showMandatoryResultEnabledComPAC.isPhotoTypeMandatory;
    } else if (_typeForm == "RSVY") {
      value = _showMandatoryResultEnabledComResurvey.isPhotoTypeMandatory;
    } else if (_typeForm == "DKR") {
      value = _showMandatoryResultEnabledComDakor.isPhotoTypeMandatory;
    }
    return value;
  }

  bool isPhotoTypeEnabled(){
    bool value;
    if(_custType == "PER") {
      if (_typeForm == "SRE") {
        value = _showMandatoryResultEnabledRegularSurvey.isPhotoTypeEnabled;
      } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
        value = _showMandatoryResultEnabledPAC.isPhotoTypeEnabled;
      } else if (_typeForm == "RSVY") {
        value = _showMandatoryResultEnabledResurvey.isPhotoTypeEnabled;
      } else if (_typeForm == "DKR") {
        value = _showMandatoryResultEnabledDakor.isPhotoTypeEnabled;
      }
    }
    else
    if (_typeForm == "SRE") {
      value = _showMandatoryResultEnabledComRegularSurvey.isPhotoTypeEnabled;
    } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
      value = _showMandatoryResultEnabledComPAC.isPhotoTypeEnabled;
    } else if (_typeForm == "RSVY") {
      value = _showMandatoryResultEnabledComResurvey.isPhotoTypeEnabled;
    } else if (_typeForm == "DKR") {
      value = _showMandatoryResultEnabledComDakor.isPhotoTypeEnabled;
    }
    return value;
  }

  //Widget Result Survey Group Location
  //Survey Type
  bool isSurveyTypeVisible(){
    bool value;
    if(_custType == "PER") {
      if (_typeForm == "SRE") {
        value = _showMandatoryResultEnabledRegularSurvey.isSurveyTypeVisible;
      } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
        value = _showMandatoryResultEnabledPAC.isSurveyTypeVisible;
      } else if (_typeForm == "RSVY") {
        value = _showMandatoryResultEnabledResurvey.isSurveyTypeVisible;
      } else if (_typeForm == "DKR") {
        value = _showMandatoryResultEnabledDakor.isSurveyTypeVisible;
      }
    }
    else
    if (_typeForm == "SRE") {
      value = _showMandatoryResultEnabledComRegularSurvey.isSurveyTypeVisible;
    } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
      value = _showMandatoryResultEnabledComPAC.isSurveyTypeVisible;
    } else if (_typeForm == "RSVY") {
      value = _showMandatoryResultEnabledComResurvey.isSurveyTypeVisible;
    } else if (_typeForm == "DKR") {
      value = _showMandatoryResultEnabledComDakor.isSurveyTypeVisible;
    }
    return value;
  }

  bool isSurveyTypeMandatory(){
    bool value;
    if(_custType == "PER") {
      if (_typeForm == "SRE") {
        value = _showMandatoryResultEnabledRegularSurvey.isSurveyTypeMandatory;
      } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
        value = _showMandatoryResultEnabledPAC.isSurveyTypeMandatory;
      } else if (_typeForm == "RSVY") {
        value = _showMandatoryResultEnabledResurvey.isSurveyTypeMandatory;
      } else if (_typeForm == "DKR") {
        value = _showMandatoryResultEnabledDakor.isSurveyTypeMandatory;
      }
    }
    else
    if (_typeForm == "SRE") {
      value = _showMandatoryResultEnabledComRegularSurvey.isSurveyTypeMandatory;
    } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
      value = _showMandatoryResultEnabledComPAC.isSurveyTypeMandatory;
    } else if (_typeForm == "RSVY") {
      value = _showMandatoryResultEnabledComResurvey.isSurveyTypeMandatory;
    } else if (_typeForm == "DKR") {
      value = _showMandatoryResultEnabledComDakor.isSurveyTypeMandatory;
    }
    return value;
  }

  bool isSurveyTypeEnabled(){
    bool value;
    if(_custType == "PER") {
      if (_typeForm == "SRE") {
        value = _showMandatoryResultEnabledRegularSurvey.isSurveyTypeEnabled;
      } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
        value = _showMandatoryResultEnabledPAC.isSurveyTypeEnabled;
      } else if (_typeForm == "RSVY") {
        value = _showMandatoryResultEnabledResurvey.isSurveyTypeEnabled;
      } else if (_typeForm == "DKR") {
        value = _showMandatoryResultEnabledDakor.isSurveyTypeEnabled;
      }
    }
    else
    if (_typeForm == "SRE") {
      value = _showMandatoryResultEnabledComRegularSurvey.isSurveyTypeEnabled;
    } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
      value = _showMandatoryResultEnabledComPAC.isSurveyTypeEnabled;
    } else if (_typeForm == "RSVY") {
      value = _showMandatoryResultEnabledComResurvey.isSurveyTypeEnabled;
    } else if (_typeForm == "DKR") {
      value = _showMandatoryResultEnabledComDakor.isSurveyTypeEnabled;
    }
    return value;
  }

  //Distance with Sentra Unit
  bool isDistanceLocationWithSentraUnitVisible(){
    bool value;
    if(_custType == "PER") {
      if (_typeForm == "SRE") {
        value = _showMandatoryResultEnabledRegularSurvey.isDistanceLocationWithSentraUnitVisible;
      } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
        value = _showMandatoryResultEnabledPAC.isDistanceLocationWithSentraUnitVisible;
      } else if (_typeForm == "RSVY") {
        value = _showMandatoryResultEnabledResurvey.isDistanceLocationWithSentraUnitVisible;
      } else if (_typeForm == "DKR") {
        value = _showMandatoryResultEnabledDakor.isDistanceLocationWithSentraUnitVisible;
      }
    }
    else
    if (_typeForm == "SRE") {
      value = _showMandatoryResultEnabledComRegularSurvey.isDistanceLocationWithSentraUnitVisible;
    } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
      value = _showMandatoryResultEnabledComPAC.isDistanceLocationWithSentraUnitVisible;
    } else if (_typeForm == "RSVY") {
      value = _showMandatoryResultEnabledComResurvey.isDistanceLocationWithSentraUnitVisible;
    } else if (_typeForm == "DKR") {
      value = _showMandatoryResultEnabledComDakor.isDistanceLocationWithSentraUnitVisible;
    }
    return value;
  }

  bool isDistanceLocationWithSentraUnitMandatory(){
    bool value;
    if(_custType == "PER") {
      if (_typeForm == "SRE") {
        value = _showMandatoryResultEnabledRegularSurvey.isDistanceLocationWithSentraUnitMandatory;
      } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
        value = _showMandatoryResultEnabledPAC.isDistanceLocationWithSentraUnitMandatory;
      } else if (_typeForm == "RSVY") {
        value = _showMandatoryResultEnabledResurvey.isDistanceLocationWithSentraUnitMandatory;
      } else if (_typeForm == "DKR") {
        value = _showMandatoryResultEnabledDakor.isDistanceLocationWithSentraUnitMandatory;
      }
    }
    else
    if (_typeForm == "SRE") {
      value = _showMandatoryResultEnabledComRegularSurvey.isDistanceLocationWithSentraUnitMandatory;
    } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
      value = _showMandatoryResultEnabledComPAC.isDistanceLocationWithSentraUnitMandatory;
    } else if (_typeForm == "RSVY") {
      value = _showMandatoryResultEnabledComResurvey.isDistanceLocationWithSentraUnitMandatory;
    } else if (_typeForm == "DKR") {
      value = _showMandatoryResultEnabledComDakor.isDistanceLocationWithSentraUnitMandatory;
    }
    return value;
  }

  bool isDistanceLocationWithSentraUnitEnabled(){
    bool value;
    if(_custType == "PER") {
      if (_typeForm == "SRE") {
        value = _showMandatoryResultEnabledRegularSurvey.isDistanceLocationWithSentraUnitEnabled;
      } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
        value = _showMandatoryResultEnabledPAC.isDistanceLocationWithSentraUnitEnabled;
      } else if (_typeForm == "RSVY") {
        value = _showMandatoryResultEnabledResurvey.isDistanceLocationWithSentraUnitEnabled;
      } else if (_typeForm == "DKR") {
        value = _showMandatoryResultEnabledDakor.isDistanceLocationWithSentraUnitEnabled;
      }
    }
    else
    if (_typeForm == "SRE") {
      value = _showMandatoryResultEnabledComRegularSurvey.isDistanceLocationWithSentraUnitEnabled;
    } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
      value = _showMandatoryResultEnabledComPAC.isDistanceLocationWithSentraUnitEnabled;
    } else if (_typeForm == "RSVY") {
      value = _showMandatoryResultEnabledComResurvey.isDistanceLocationWithSentraUnitEnabled;
    } else if (_typeForm == "DKR") {
      value = _showMandatoryResultEnabledComDakor.isDistanceLocationWithSentraUnitEnabled;
    }
    return value;
  }

  //Distance with Merchant
  bool isDistanceLocationWithDealerMerchantAXIVisible(){
    bool value;
    if(_custType == "PER") {
      if (_typeForm == "SRE") {
        value = _showMandatoryResultEnabledRegularSurvey.isDistanceLocationWithDealerMerchantAXIVisible;
      } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
        value = _showMandatoryResultEnabledPAC.isDistanceLocationWithDealerMerchantAXIVisible;
      } else if (_typeForm == "RSVY") {
        value = _showMandatoryResultEnabledResurvey.isDistanceLocationWithDealerMerchantAXIVisible;
      } else if (_typeForm == "DKR") {
        value = _showMandatoryResultEnabledDakor.isDistanceLocationWithDealerMerchantAXIVisible;
      }
    }
    else
    if (_typeForm == "SRE") {
      value = _showMandatoryResultEnabledComRegularSurvey.isDistanceLocationWithDealerMerchantAXIVisible;
    } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
      value = _showMandatoryResultEnabledComPAC.isDistanceLocationWithDealerMerchantAXIVisible;
    } else if (_typeForm == "RSVY") {
      value = _showMandatoryResultEnabledComResurvey.isDistanceLocationWithDealerMerchantAXIVisible;
    } else if (_typeForm == "DKR") {
      value = _showMandatoryResultEnabledComDakor.isDistanceLocationWithDealerMerchantAXIVisible;
    }
    return value;
  }

  bool isDistanceLocationWithDealerMerchantAXIMandatory(){
    bool value;
    if(_custType == "PER") {
      if (_typeForm == "SRE") {
        value = _showMandatoryResultEnabledRegularSurvey.isDistanceLocationWithDealerMerchantAXIMandatory;
      } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
        value = _showMandatoryResultEnabledPAC.isDistanceLocationWithDealerMerchantAXIMandatory;
      } else if (_typeForm == "RSVY") {
        value = _showMandatoryResultEnabledResurvey.isDistanceLocationWithDealerMerchantAXIMandatory;
      } else if (_typeForm == "DKR") {
        value = _showMandatoryResultEnabledDakor.isDistanceLocationWithDealerMerchantAXIMandatory;
      }
    }
    else
    if (_typeForm == "SRE") {
      value = _showMandatoryResultEnabledComRegularSurvey.isDistanceLocationWithDealerMerchantAXIMandatory;
    } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
      value = _showMandatoryResultEnabledComPAC.isDistanceLocationWithDealerMerchantAXIMandatory;
    } else if (_typeForm == "RSVY") {
      value = _showMandatoryResultEnabledComResurvey.isDistanceLocationWithDealerMerchantAXIMandatory;
    } else if (_typeForm == "DKR") {
      value = _showMandatoryResultEnabledComDakor.isDistanceLocationWithDealerMerchantAXIMandatory;
    }
    return value;
  }

  bool isDistanceLocationWithDealerMerchantAXIEnabled(){
    bool value;
    if(_custType == "PER") {
      if (_typeForm == "SRE") {
        value = _showMandatoryResultEnabledRegularSurvey.isDistanceLocationWithDealerMerchantAXIEnabled;
      } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
        value = _showMandatoryResultEnabledPAC.isDistanceLocationWithDealerMerchantAXIEnabled;
      } else if (_typeForm == "RSVY") {
        value = _showMandatoryResultEnabledResurvey.isDistanceLocationWithDealerMerchantAXIEnabled;
      } else if (_typeForm == "DKR") {
        value = _showMandatoryResultEnabledDakor.isDistanceLocationWithDealerMerchantAXIEnabled;
      }
    }
    else
    if (_typeForm == "SRE") {
      value = _showMandatoryResultEnabledComRegularSurvey.isDistanceLocationWithDealerMerchantAXIEnabled;
    } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
      value = _showMandatoryResultEnabledComPAC.isDistanceLocationWithDealerMerchantAXIEnabled;
    } else if (_typeForm == "RSVY") {
      value = _showMandatoryResultEnabledComResurvey.isDistanceLocationWithDealerMerchantAXIEnabled;
    } else if (_typeForm == "DKR") {
      value = _showMandatoryResultEnabledComDakor.isDistanceLocationWithDealerMerchantAXIEnabled;
    }
    return value;
  }

  //Distance of Object to Sentra Unit
  bool isDistanceLocationWithUsageObjectWithSentraUnitVisible(){
    bool value;
    if(_custType == "PER") {
      if (_typeForm == "SRE") {
        value = _showMandatoryResultEnabledRegularSurvey.isDistanceLocationWithUsageObjectWithSentraUnitVisible;
      } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
        value = _showMandatoryResultEnabledPAC.isDistanceLocationWithUsageObjectWithSentraUnitVisible;
      } else if (_typeForm == "RSVY") {
        value = _showMandatoryResultEnabledResurvey.isDistanceLocationWithUsageObjectWithSentraUnitVisible;
      } else if (_typeForm == "DKR") {
        value = _showMandatoryResultEnabledDakor.isDistanceLocationWithUsageObjectWithSentraUnitVisible;
      }
    }
    else
    if (_typeForm == "SRE") {
      value = _showMandatoryResultEnabledComRegularSurvey.isDistanceLocationWithUsageObjectWithSentraUnitVisible;
    } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
      value = _showMandatoryResultEnabledComPAC.isDistanceLocationWithUsageObjectWithSentraUnitVisible;
    } else if (_typeForm == "RSVY") {
      value = _showMandatoryResultEnabledComResurvey.isDistanceLocationWithUsageObjectWithSentraUnitVisible;
    } else if (_typeForm == "DKR") {
      value = _showMandatoryResultEnabledComDakor.isDistanceLocationWithUsageObjectWithSentraUnitVisible;
    }
    return value;
  }

  bool isDistanceLocationWithUsageObjectWithSentraUnitMandatory(){
    bool value;
    if(_custType == "PER") {
      if (_typeForm == "SRE") {
        value = _showMandatoryResultEnabledRegularSurvey.isDistanceLocationWithUsageObjectWithSentraUnitMandatory;
      } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
        value = _showMandatoryResultEnabledPAC.isDistanceLocationWithUsageObjectWithSentraUnitMandatory;
      } else if (_typeForm == "RSVY") {
        value = _showMandatoryResultEnabledResurvey.isDistanceLocationWithUsageObjectWithSentraUnitMandatory;
      } else if (_typeForm == "DKR") {
        value = _showMandatoryResultEnabledDakor.isDistanceLocationWithUsageObjectWithSentraUnitMandatory;
      }
    }
    else
    if (_typeForm == "SRE") {
      value = _showMandatoryResultEnabledComRegularSurvey.isDistanceLocationWithUsageObjectWithSentraUnitMandatory;
    } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
      value = _showMandatoryResultEnabledComPAC.isDistanceLocationWithUsageObjectWithSentraUnitMandatory;
    } else if (_typeForm == "RSVY") {
      value = _showMandatoryResultEnabledComResurvey.isDistanceLocationWithUsageObjectWithSentraUnitMandatory;
    } else if (_typeForm == "DKR") {
      value = _showMandatoryResultEnabledComDakor.isDistanceLocationWithUsageObjectWithSentraUnitMandatory;
    }
    return value;
  }

  bool isDistanceLocationWithUsageObjectWithSentraUnitEnabled(){
    bool value;
    if(_custType == "PER") {
      if (_typeForm == "SRE") {
        value = _showMandatoryResultEnabledRegularSurvey.isDistanceLocationWithUsageObjectWithSentraUnitEnabled;
      } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
        value = _showMandatoryResultEnabledPAC.isDistanceLocationWithUsageObjectWithSentraUnitEnabled;
      } else if (_typeForm == "RSVY") {
        value = _showMandatoryResultEnabledResurvey.isDistanceLocationWithUsageObjectWithSentraUnitEnabled;
      } else if (_typeForm == "DKR") {
        value = _showMandatoryResultEnabledDakor.isDistanceLocationWithUsageObjectWithSentraUnitEnabled;
      }
    }
    else
    if (_typeForm == "SRE") {
      value = _showMandatoryResultEnabledComRegularSurvey.isDistanceLocationWithUsageObjectWithSentraUnitEnabled;
    } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
      value = _showMandatoryResultEnabledComPAC.isDistanceLocationWithUsageObjectWithSentraUnitEnabled;
    } else if (_typeForm == "RSVY") {
      value = _showMandatoryResultEnabledComResurvey.isDistanceLocationWithUsageObjectWithSentraUnitEnabled;
    } else if (_typeForm == "DKR") {
      value = _showMandatoryResultEnabledComDakor.isDistanceLocationWithUsageObjectWithSentraUnitEnabled;
    }
    return value;
  }

  //Widget Add Edit Asset
  //Asset Type
  bool isAssetTypeVisible(){
    bool value;
    if(_custType == "PER") {
      if (_typeForm == "SRE") {
        value = _showMandatoryResultEnabledRegularSurvey.isAssetTypeVisible;
      } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
        value = _showMandatoryResultEnabledPAC.isAssetTypeVisible;
      } else if (_typeForm == "RSVY") {
        value = _showMandatoryResultEnabledResurvey.isAssetTypeVisible;
      } else if (_typeForm == "DKR") {
        value = _showMandatoryResultEnabledDakor.isAssetTypeVisible;
      }
    }
    else
    if (_typeForm == "SRE") {
      value = _showMandatoryResultEnabledComRegularSurvey.isAssetTypeVisible;
    } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
      value = _showMandatoryResultEnabledComPAC.isAssetTypeVisible;
    } else if (_typeForm == "RSVY") {
      value = _showMandatoryResultEnabledComResurvey.isAssetTypeVisible;
    } else if (_typeForm == "DKR") {
      value = _showMandatoryResultEnabledComDakor.isAssetTypeVisible;
    }
    return value;
  }

  bool isAssetTypeMandatory(){
    bool value;
    if(_custType == "PER") {
      if (_typeForm == "SRE") {
        value = _showMandatoryResultEnabledRegularSurvey.isAssetTypeMandatory;
      } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
        value = _showMandatoryResultEnabledPAC.isAssetTypeMandatory;
      } else if (_typeForm == "RSVY") {
        value = _showMandatoryResultEnabledResurvey.isAssetTypeMandatory;
      } else if (_typeForm == "DKR") {
        value = _showMandatoryResultEnabledDakor.isAssetTypeMandatory;
      }
    }
    else
    if (_typeForm == "SRE") {
      value = _showMandatoryResultEnabledComRegularSurvey.isAssetTypeMandatory;
    } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
      value = _showMandatoryResultEnabledComPAC.isAssetTypeMandatory;
    } else if (_typeForm == "RSVY") {
      value = _showMandatoryResultEnabledComResurvey.isAssetTypeMandatory;
    } else if (_typeForm == "DKR") {
      value = _showMandatoryResultEnabledComDakor.isAssetTypeMandatory;
    }
    return value;
  }

  bool isAssetTypeEnabled(){
    bool value;
    if(_custType == "PER") {
      if (_typeForm == "SRE") {
        value = _showMandatoryResultEnabledRegularSurvey.isAssetTypeEnabled;
      } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
        value = _showMandatoryResultEnabledPAC.isAssetTypeEnabled;
      } else if (_typeForm == "RSVY") {
        value = _showMandatoryResultEnabledResurvey.isAssetTypeEnabled;
      } else if (_typeForm == "DKR") {
        value = _showMandatoryResultEnabledDakor.isAssetTypeEnabled;
      }
    }
    else
    if (_typeForm == "SRE") {
      value = _showMandatoryResultEnabledComRegularSurvey.isAssetTypeEnabled;
    } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
      value = _showMandatoryResultEnabledComPAC.isAssetTypeEnabled;
    } else if (_typeForm == "RSVY") {
      value = _showMandatoryResultEnabledComResurvey.isAssetTypeEnabled;
    } else if (_typeForm == "DKR") {
      value = _showMandatoryResultEnabledComDakor.isAssetTypeEnabled;
    }
    return value;
  }

  //Value Asset
  bool isValueAssetVisible(){
    bool value;
    if(_custType == "PER") {
      if (_typeForm == "SRE") {
        value = _showMandatoryResultEnabledRegularSurvey.isValueAssetVisible;
      } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
        value = _showMandatoryResultEnabledPAC.isValueAssetVisible;
      } else if (_typeForm == "RSVY") {
        value = _showMandatoryResultEnabledResurvey.isValueAssetVisible;
      } else if (_typeForm == "DKR") {
        value = _showMandatoryResultEnabledDakor.isValueAssetVisible;
      }
    }
    else
    if (_typeForm == "SRE") {
      value = _showMandatoryResultEnabledComRegularSurvey.isValueAssetVisible;
    } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
      value = _showMandatoryResultEnabledComPAC.isValueAssetVisible;
    } else if (_typeForm == "RSVY") {
      value = _showMandatoryResultEnabledComResurvey.isValueAssetVisible;
    } else if (_typeForm == "DKR") {
      value = _showMandatoryResultEnabledComDakor.isValueAssetVisible;
    }
    return value;
  }

  bool isValueAssetMandatory(){
    bool value;
    if(_custType == "PER") {
      if (_typeForm == "SRE") {
        value = _showMandatoryResultEnabledRegularSurvey.isValueAssetMandatory;
      } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
        value = _showMandatoryResultEnabledPAC.isValueAssetMandatory;
      } else if (_typeForm == "RSVY") {
        value = _showMandatoryResultEnabledResurvey.isValueAssetMandatory;
      } else if (_typeForm == "DKR") {
        value = _showMandatoryResultEnabledDakor.isValueAssetMandatory;
      }
    }
    else
    if (_typeForm == "SRE") {
      value = _showMandatoryResultEnabledComRegularSurvey.isValueAssetMandatory;
    } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
      value = _showMandatoryResultEnabledComPAC.isValueAssetMandatory;
    } else if (_typeForm == "RSVY") {
      value = _showMandatoryResultEnabledComResurvey.isValueAssetMandatory;
    } else if (_typeForm == "DKR") {
      value = _showMandatoryResultEnabledComDakor.isValueAssetMandatory;
    }
    return value;
  }

  bool isValueAssetEnabled(){
    bool value;
    if(_custType == "PER") {
      if (_typeForm == "SRE") {
        value = _showMandatoryResultEnabledRegularSurvey.isValueAssetEnabled;
      } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
        value = _showMandatoryResultEnabledPAC.isValueAssetEnabled;
      } else if (_typeForm == "RSVY") {
        value = _showMandatoryResultEnabledResurvey.isValueAssetEnabled;
      } else if (_typeForm == "DKR") {
        value = _showMandatoryResultEnabledDakor.isValueAssetEnabled;
      }
    }
    else
    if (_typeForm == "SRE") {
      value = _showMandatoryResultEnabledComRegularSurvey.isValueAssetEnabled;
    } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
      value = _showMandatoryResultEnabledComPAC.isValueAssetEnabled;
    } else if (_typeForm == "RSVY") {
      value = _showMandatoryResultEnabledComResurvey.isValueAssetEnabled;
    } else if (_typeForm == "DKR") {
      value = _showMandatoryResultEnabledComDakor.isValueAssetEnabled;
    }
    return value;
  }

  //Identity Info
  bool isIdentityInfoVisible(){
    bool value;
    if(_custType == "PER") {
      if (_typeForm == "SRE") {
        value = _showMandatoryResultEnabledRegularSurvey.isIdentityInfoVisible;
      } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
        value = _showMandatoryResultEnabledPAC.isIdentityInfoVisible;
      } else if (_typeForm == "RSVY") {
        value = _showMandatoryResultEnabledResurvey.isIdentityInfoVisible;
      } else if (_typeForm == "DKR") {
        value = _showMandatoryResultEnabledDakor.isIdentityInfoVisible;
      }
    }
    else
    if (_typeForm == "SRE") {
      value = _showMandatoryResultEnabledComRegularSurvey.isIdentityInfoVisible;
    } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
      value = _showMandatoryResultEnabledComPAC.isIdentityInfoVisible;
    } else if (_typeForm == "RSVY") {
      value = _showMandatoryResultEnabledComResurvey.isIdentityInfoVisible;
    } else if (_typeForm == "DKR") {
      value = _showMandatoryResultEnabledComDakor.isIdentityInfoVisible;
    }
    return value;
  }

  bool isIdentityInfoMandatory(){
    bool value;
    if(_custType == "PER") {
      if (_typeForm == "SRE") {
        value = _showMandatoryResultEnabledRegularSurvey.isIdentityInfoMandatory;
      } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
        value = _showMandatoryResultEnabledPAC.isIdentityInfoMandatory;
      } else if (_typeForm == "RSVY") {
        value = _showMandatoryResultEnabledResurvey.isIdentityInfoMandatory;
      } else if (_typeForm == "DKR") {
        value = _showMandatoryResultEnabledDakor.isIdentityInfoMandatory;
      }
    }
    else
    if (_typeForm == "SRE") {
      value = _showMandatoryResultEnabledComRegularSurvey.isIdentityInfoMandatory;
    } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
      value = _showMandatoryResultEnabledComPAC.isIdentityInfoMandatory;
    } else if (_typeForm == "RSVY") {
      value = _showMandatoryResultEnabledComResurvey.isIdentityInfoMandatory;
    } else if (_typeForm == "DKR") {
      value = _showMandatoryResultEnabledComDakor.isIdentityInfoMandatory;
    }
    return value;
  }

  bool isIdentityInfoEnabled(){
    bool value;
    if(_custType == "PER") {
      if (_typeForm == "SRE") {
        value = _showMandatoryResultEnabledRegularSurvey.isIdentityInfoEnabled;
      } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
        value = _showMandatoryResultEnabledPAC.isIdentityInfoEnabled;
      } else if (_typeForm == "RSVY") {
        value = _showMandatoryResultEnabledResurvey.isIdentityInfoEnabled;
      } else if (_typeForm == "DKR") {
        value = _showMandatoryResultEnabledDakor.isIdentityInfoEnabled;
      }
    }
    else
    if (_typeForm == "SRE") {
      value = _showMandatoryResultEnabledComRegularSurvey.isIdentityInfoEnabled;
    } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
      value = _showMandatoryResultEnabledComPAC.isIdentityInfoEnabled;
    } else if (_typeForm == "RSVY") {
      value = _showMandatoryResultEnabledComResurvey.isIdentityInfoEnabled;
    } else if (_typeForm == "DKR") {
      value = _showMandatoryResultEnabledComDakor.isIdentityInfoEnabled;
    }
    return value;
  }

  //Surface Building Area Info
  bool isSurfaceBuildingAreaInfoVisible(){
    bool value;
    if(_custType == "PER") {
      if (_typeForm == "SRE") {
        value = _showMandatoryResultEnabledRegularSurvey.isSurfaceBuildingAreaInfoVisible;
      } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
        value = _showMandatoryResultEnabledPAC.isSurfaceBuildingAreaInfoVisible;
      } else if (_typeForm == "RSVY") {
        value = _showMandatoryResultEnabledResurvey.isSurfaceBuildingAreaInfoVisible;
      } else if (_typeForm == "DKR") {
        value = _showMandatoryResultEnabledDakor.isSurfaceBuildingAreaInfoVisible;
      }
    }
    else
    if (_typeForm == "SRE") {
      value = _showMandatoryResultEnabledComRegularSurvey.isSurfaceBuildingAreaInfoVisible;
    } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
      value = _showMandatoryResultEnabledComPAC.isSurfaceBuildingAreaInfoVisible;
    } else if (_typeForm == "RSVY") {
      value = _showMandatoryResultEnabledComResurvey.isSurfaceBuildingAreaInfoVisible;
    } else if (_typeForm == "DKR") {
      value = _showMandatoryResultEnabledComDakor.isSurfaceBuildingAreaInfoVisible;
    }
    return value;
  }

  bool isSurfaceBuildingAreaInfoMandatory(){
    bool value;
    if(_custType == "PER") {
      if (_typeForm == "SRE") {
        value = _showMandatoryResultEnabledRegularSurvey.isSurfaceBuildingAreaInfoMandatory;
      } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
        value = _showMandatoryResultEnabledPAC.isSurfaceBuildingAreaInfoMandatory;
      } else if (_typeForm == "RSVY") {
        value = _showMandatoryResultEnabledResurvey.isSurfaceBuildingAreaInfoMandatory;
      } else if (_typeForm == "DKR") {
        value = _showMandatoryResultEnabledDakor.isSurfaceBuildingAreaInfoMandatory;
      }
    }
    else
    if (_typeForm == "SRE") {
      value = _showMandatoryResultEnabledComRegularSurvey.isSurfaceBuildingAreaInfoMandatory;
    } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
      value = _showMandatoryResultEnabledComPAC.isSurfaceBuildingAreaInfoMandatory;
    } else if (_typeForm == "RSVY") {
      value = _showMandatoryResultEnabledComResurvey.isSurfaceBuildingAreaInfoMandatory;
    } else if (_typeForm == "DKR") {
      value = _showMandatoryResultEnabledComDakor.isSurfaceBuildingAreaInfoMandatory;
    }
    return value;
  }

  bool isSurfaceBuildingAreaInfoEnabled(){
    bool value;
    if(_custType == "PER") {
      if (_typeForm == "SRE") {
        value = _showMandatoryResultEnabledRegularSurvey.isSurfaceBuildingAreaInfoEnabled;
      } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
        value = _showMandatoryResultEnabledPAC.isSurfaceBuildingAreaInfoEnabled;
      } else if (_typeForm == "RSVY") {
        value = _showMandatoryResultEnabledResurvey.isSurfaceBuildingAreaInfoEnabled;
      } else if (_typeForm == "DKR") {
        value = _showMandatoryResultEnabledDakor.isSurfaceBuildingAreaInfoEnabled;
      }
    }
    else
    if (_typeForm == "SRE") {
      value = _showMandatoryResultEnabledComRegularSurvey.isSurfaceBuildingAreaInfoEnabled;
    } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
      value = _showMandatoryResultEnabledComPAC.isSurfaceBuildingAreaInfoEnabled;
    } else if (_typeForm == "RSVY") {
      value = _showMandatoryResultEnabledComResurvey.isSurfaceBuildingAreaInfoEnabled;
    } else if (_typeForm == "DKR") {
      value = _showMandatoryResultEnabledComDakor.isSurfaceBuildingAreaInfoEnabled;
    }
    return value;
  }

  //Road Type Info
  bool isRoadTypeInfoVisible(){
    bool value;
    if(_custType == "PER") {
      if (_typeForm == "SRE") {
        value = _showMandatoryResultEnabledRegularSurvey.isRoadTypeInfoVisible;
      } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
        value = _showMandatoryResultEnabledPAC.isRoadTypeInfoVisible;
      } else if (_typeForm == "RSVY") {
        value = _showMandatoryResultEnabledResurvey.isRoadTypeInfoVisible;
      } else if (_typeForm == "DKR") {
        value = _showMandatoryResultEnabledDakor.isRoadTypeInfoVisible;
      }
    }
    else
    if (_typeForm == "SRE") {
      value = _showMandatoryResultEnabledComRegularSurvey.isRoadTypeInfoVisible;
    } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
      value = _showMandatoryResultEnabledComPAC.isRoadTypeInfoVisible;
    } else if (_typeForm == "RSVY") {
      value = _showMandatoryResultEnabledComResurvey.isRoadTypeInfoVisible;
    } else if (_typeForm == "DKR") {
      value = _showMandatoryResultEnabledComDakor.isRoadTypeInfoVisible;
    }
    return value;
  }

  bool isRoadTypeInfoMandatory(){
    bool value;
    if(_custType == "PER") {
      if (_typeForm == "SRE") {
        value = _showMandatoryResultEnabledRegularSurvey.isRoadTypeInfoMandatory;
      } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
        value = _showMandatoryResultEnabledPAC.isRoadTypeInfoMandatory;
      } else if (_typeForm == "RSVY") {
        value = _showMandatoryResultEnabledResurvey.isRoadTypeInfoMandatory;
      } else if (_typeForm == "DKR") {
        value = _showMandatoryResultEnabledDakor.isRoadTypeInfoMandatory;
      }
    }
    else
    if (_typeForm == "SRE") {
      value = _showMandatoryResultEnabledComRegularSurvey.isRoadTypeInfoMandatory;
    } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
      value = _showMandatoryResultEnabledComPAC.isRoadTypeInfoMandatory;
    } else if (_typeForm == "RSVY") {
      value = _showMandatoryResultEnabledComResurvey.isRoadTypeInfoMandatory;
    } else if (_typeForm == "DKR") {
      value = _showMandatoryResultEnabledComDakor.isRoadTypeInfoMandatory;
    }
    return value;
  }

  bool isRoadTypeInfoEnabled(){
    bool value;
    if(_custType == "PER") {
      if (_typeForm == "SRE") {
        value = _showMandatoryResultEnabledRegularSurvey.isRoadTypeInfoEnabled;
      } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
        value = _showMandatoryResultEnabledPAC.isRoadTypeInfoEnabled;
      } else if (_typeForm == "RSVY") {
        value = _showMandatoryResultEnabledResurvey.isRoadTypeInfoEnabled;
      } else if (_typeForm == "DKR") {
        value = _showMandatoryResultEnabledDakor.isRoadTypeInfoEnabled;
      }
    }
    else
    if (_typeForm == "SRE") {
      value = _showMandatoryResultEnabledComRegularSurvey.isRoadTypeInfoEnabled;
    } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
      value = _showMandatoryResultEnabledComPAC.isRoadTypeInfoEnabled;
    } else if (_typeForm == "RSVY") {
      value = _showMandatoryResultEnabledComResurvey.isRoadTypeInfoEnabled;
    } else if (_typeForm == "DKR") {
      value = _showMandatoryResultEnabledComDakor.isRoadTypeInfoEnabled;
    }
    return value;
  }

  //Electricity Type Info
  bool isElectricityTypeInfoVisible(){
    bool value;
    if(_custType == "PER") {
      if (_typeForm == "SRE") {
        value = _showMandatoryResultEnabledRegularSurvey.isElectricityTypeInfoVisible;
      } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
        value = _showMandatoryResultEnabledPAC.isElectricityTypeInfoVisible;
      } else if (_typeForm == "RSVY") {
        value = _showMandatoryResultEnabledResurvey.isElectricityTypeInfoVisible;
      } else if (_typeForm == "DKR") {
        value = _showMandatoryResultEnabledDakor.isElectricityTypeInfoVisible;
      }
    }
    else
    if (_typeForm == "SRE") {
      value = _showMandatoryResultEnabledComRegularSurvey.isElectricityTypeInfoVisible;
    } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
      value = _showMandatoryResultEnabledComPAC.isElectricityTypeInfoVisible;
    } else if (_typeForm == "RSVY") {
      value = _showMandatoryResultEnabledComResurvey.isElectricityTypeInfoVisible;
    } else if (_typeForm == "DKR") {
      value = _showMandatoryResultEnabledComDakor.isElectricityTypeInfoVisible;
    }
    return value;
  }

  bool isElectricityTypeInfoMandatory(){
    bool value;
    if(_custType == "PER") {
      if (_typeForm == "SRE") {
        value = _showMandatoryResultEnabledRegularSurvey.isElectricityTypeInfoMandatory;
      } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
        value = _showMandatoryResultEnabledPAC.isElectricityTypeInfoMandatory;
      } else if (_typeForm == "RSVY") {
        value = _showMandatoryResultEnabledResurvey.isElectricityTypeInfoMandatory;
      } else if (_typeForm == "DKR") {
        value = _showMandatoryResultEnabledDakor.isElectricityTypeInfoMandatory;
      }
    }
    else
    if (_typeForm == "SRE") {
      value = _showMandatoryResultEnabledComRegularSurvey.isElectricityTypeInfoMandatory;
    } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
      value = _showMandatoryResultEnabledComPAC.isElectricityTypeInfoMandatory;
    } else if (_typeForm == "RSVY") {
      value = _showMandatoryResultEnabledComResurvey.isElectricityTypeInfoMandatory;
    } else if (_typeForm == "DKR") {
      value = _showMandatoryResultEnabledComDakor.isElectricityTypeInfoMandatory;
    }
    return value;
  }

  bool isElectricityTypeInfoEnabled(){
    bool value;
    if(_custType == "PER") {
      if (_typeForm == "SRE") {
        value = _showMandatoryResultEnabledRegularSurvey.isElectricityTypeInfoEnabled;
      } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
        value = _showMandatoryResultEnabledPAC.isElectricityTypeInfoEnabled;
      } else if (_typeForm == "RSVY") {
        value = _showMandatoryResultEnabledResurvey.isElectricityTypeInfoEnabled;
      } else if (_typeForm == "DKR") {
        value = _showMandatoryResultEnabledDakor.isElectricityTypeInfoEnabled;
      }
    }
    else
    if (_typeForm == "SRE") {
      value = _showMandatoryResultEnabledComRegularSurvey.isElectricityTypeInfoEnabled;
    } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
      value = _showMandatoryResultEnabledComPAC.isElectricityTypeInfoEnabled;
    } else if (_typeForm == "RSVY") {
      value = _showMandatoryResultEnabledComResurvey.isElectricityTypeInfoEnabled;
    } else if (_typeForm == "DKR") {
      value = _showMandatoryResultEnabledComDakor.isElectricityTypeInfoEnabled;
    }
    return value;
  }

  //Electricity Bill Info
  bool isElectricityBillInfoVisible(){
    bool value;
    if(_custType == "PER") {
      if (_typeForm == "SRE") {
        value = _showMandatoryResultEnabledRegularSurvey.isElectricityBillInfoVisible;
      } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
        value = _showMandatoryResultEnabledPAC.isElectricityBillInfoVisible;
      } else if (_typeForm == "RSVY") {
        value = _showMandatoryResultEnabledResurvey.isElectricityBillInfoVisible;
      } else if (_typeForm == "DKR") {
        value = _showMandatoryResultEnabledDakor.isElectricityBillInfoVisible;
      }
    }
    else
    if (_typeForm == "SRE") {
      value = _showMandatoryResultEnabledComRegularSurvey.isElectricityBillInfoVisible;
    } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
      value = _showMandatoryResultEnabledComPAC.isElectricityBillInfoVisible;
    } else if (_typeForm == "RSVY") {
      value = _showMandatoryResultEnabledComResurvey.isElectricityBillInfoVisible;
    } else if (_typeForm == "DKR") {
      value = _showMandatoryResultEnabledComDakor.isElectricityBillInfoVisible;
    }
    return value;
  }

  bool isElectricityBillInfoMandatory(){
    bool value;
    if(_custType == "PER") {
      if (_typeForm == "SRE") {
        value = _showMandatoryResultEnabledRegularSurvey.isElectricityBillInfoMandatory;
      } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
        value = _showMandatoryResultEnabledPAC.isElectricityBillInfoMandatory;
      } else if (_typeForm == "RSVY") {
        value = _showMandatoryResultEnabledResurvey.isElectricityBillInfoMandatory;
      } else if (_typeForm == "DKR") {
        value = _showMandatoryResultEnabledDakor.isElectricityBillInfoMandatory;
      }
    }
    else
    if (_typeForm == "SRE") {
      value = _showMandatoryResultEnabledComRegularSurvey.isElectricityBillInfoMandatory;
    } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
      value = _showMandatoryResultEnabledComPAC.isElectricityBillInfoMandatory;
    } else if (_typeForm == "RSVY") {
      value = _showMandatoryResultEnabledComResurvey.isElectricityBillInfoMandatory;
    } else if (_typeForm == "DKR") {
      value = _showMandatoryResultEnabledComDakor.isElectricityBillInfoMandatory;
    }
    return value;
  }

  bool isElectricityBillInfoEnabled(){
    bool value;
    if(_custType == "PER") {
      if (_typeForm == "SRE") {
        value = _showMandatoryResultEnabledRegularSurvey.isElectricityBillInfoEnabled;
      } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
        value = _showMandatoryResultEnabledPAC.isElectricityBillInfoEnabled;
      } else if (_typeForm == "RSVY") {
        value = _showMandatoryResultEnabledResurvey.isElectricityBillInfoEnabled;
      } else if (_typeForm == "DKR") {
        value = _showMandatoryResultEnabledDakor.isElectricityBillInfoEnabled;
      }
    }
    else
    if (_typeForm == "SRE") {
      value = _showMandatoryResultEnabledComRegularSurvey.isElectricityBillInfoEnabled;
    } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
      value = _showMandatoryResultEnabledComPAC.isElectricityBillInfoEnabled;
    } else if (_typeForm == "RSVY") {
      value = _showMandatoryResultEnabledComResurvey.isElectricityBillInfoEnabled;
    } else if (_typeForm == "DKR") {
      value = _showMandatoryResultEnabledComDakor.isElectricityBillInfoEnabled;
    }
    return value;
  }

  //End Date Info
  bool isEndDateInfoVisible(){
    bool value;
    if(_custType == "PER") {
      if (_typeForm == "SRE") {
        value = _showMandatoryResultEnabledRegularSurvey.isEndDateInfoVisible;
      } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
        value = _showMandatoryResultEnabledPAC.isEndDateInfoVisible;
      } else if (_typeForm == "RSVY") {
        value = _showMandatoryResultEnabledResurvey.isEndDateInfoVisible;
      } else if (_typeForm == "DKR") {
        value = _showMandatoryResultEnabledDakor.isEndDateInfoVisible;
      }
    }
    else
    if (_typeForm == "SRE") {
      value = _showMandatoryResultEnabledComRegularSurvey.isEndDateInfoVisible;
    } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
      value = _showMandatoryResultEnabledComPAC.isEndDateInfoVisible;
    } else if (_typeForm == "RSVY") {
      value = _showMandatoryResultEnabledComResurvey.isEndDateInfoVisible;
    } else if (_typeForm == "DKR") {
      value = _showMandatoryResultEnabledComDakor.isEndDateInfoVisible;
    }
    return value;
  }

  bool isEndDateInfoMandatory(){
    bool value;
    if(_custType == "PER") {
      if (_typeForm == "SRE") {
        value = _showMandatoryResultEnabledRegularSurvey.isEndDateInfoMandatory;
      } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
        value = _showMandatoryResultEnabledPAC.isEndDateInfoMandatory;
      } else if (_typeForm == "RSVY") {
        value = _showMandatoryResultEnabledResurvey.isEndDateInfoMandatory;
      } else if (_typeForm == "DKR") {
        value = _showMandatoryResultEnabledDakor.isEndDateInfoMandatory;
      }
    }
    else
    if (_typeForm == "SRE") {
      value = _showMandatoryResultEnabledComRegularSurvey.isEndDateInfoMandatory;
    } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
      value = _showMandatoryResultEnabledComPAC.isEndDateInfoMandatory;
    } else if (_typeForm == "RSVY") {
      value = _showMandatoryResultEnabledComResurvey.isEndDateInfoMandatory;
    } else if (_typeForm == "DKR") {
      value = _showMandatoryResultEnabledComDakor.isEndDateInfoMandatory;
    }
    return value;
  }

  bool isEndDateInfoEnabled(){
    bool value;
    if(_custType == "PER") {
      if (_typeForm == "SRE") {
        value = _showMandatoryResultEnabledRegularSurvey.isEndDateInfoEnabled;
      } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
        value = _showMandatoryResultEnabledPAC.isEndDateInfoEnabled;
      } else if (_typeForm == "RSVY") {
        value = _showMandatoryResultEnabledResurvey.isEndDateInfoEnabled;
      } else if (_typeForm == "DKR") {
        value = _showMandatoryResultEnabledDakor.isEndDateInfoEnabled;
      }
    }
    else
    if (_typeForm == "SRE") {
      value = _showMandatoryResultEnabledComRegularSurvey.isEndDateInfoEnabled;
    } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
      value = _showMandatoryResultEnabledComPAC.isEndDateInfoEnabled;
    } else if (_typeForm == "RSVY") {
      value = _showMandatoryResultEnabledComResurvey.isEndDateInfoEnabled;
    } else if (_typeForm == "DKR") {
      value = _showMandatoryResultEnabledComDakor.isEndDateInfoEnabled;
    }
    return value;
  }

  //Length Stay Info
  bool isLengthStayInfoVisible(){
    bool value;
    if(_custType == "PER") {
      if (_typeForm == "SRE") {
        value = _showMandatoryResultEnabledRegularSurvey.isLengthStayInfoVisible;
      } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
        value = _showMandatoryResultEnabledPAC.isLengthStayInfoVisible;
      } else if (_typeForm == "RSVY") {
        value = _showMandatoryResultEnabledResurvey.isLengthStayInfoVisible;
      } else if (_typeForm == "DKR") {
        value = _showMandatoryResultEnabledDakor.isLengthStayInfoVisible;
      }
    }
    else
    if (_typeForm == "SRE") {
      value = _showMandatoryResultEnabledComRegularSurvey.isLengthStayInfoVisible;
    } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
      value = _showMandatoryResultEnabledComPAC.isLengthStayInfoVisible;
    } else if (_typeForm == "RSVY") {
      value = _showMandatoryResultEnabledComResurvey.isLengthStayInfoVisible;
    } else if (_typeForm == "DKR") {
      value = _showMandatoryResultEnabledComDakor.isLengthStayInfoVisible;
    }
    return value;
  }

  bool isLengthStayInfoMandatory(){
    bool value;
    if(_custType == "PER") {
      if (_typeForm == "SRE") {
        value = _showMandatoryResultEnabledRegularSurvey.isLengthStayInfoMandatory;
      } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
        value = _showMandatoryResultEnabledPAC.isLengthStayInfoMandatory;
      } else if (_typeForm == "RSVY") {
        value = _showMandatoryResultEnabledResurvey.isLengthStayInfoMandatory;
      } else if (_typeForm == "DKR") {
        value = _showMandatoryResultEnabledDakor.isLengthStayInfoMandatory;
      }
    }
    else
    if (_typeForm == "SRE") {
      value = _showMandatoryResultEnabledComRegularSurvey.isLengthStayInfoMandatory;
    } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
      value = _showMandatoryResultEnabledComPAC.isLengthStayInfoMandatory;
    } else if (_typeForm == "RSVY") {
      value = _showMandatoryResultEnabledComResurvey.isLengthStayInfoMandatory;
    } else if (_typeForm == "DKR") {
      value = _showMandatoryResultEnabledComDakor.isLengthStayInfoMandatory;
    }
    return value;
  }

  bool isLengthStayInfoEnabled(){
    bool value;
    if(_custType == "PER") {
      if (_typeForm == "SRE") {
        value = _showMandatoryResultEnabledRegularSurvey.isLengthStayInfoEnabled;
      } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
        value = _showMandatoryResultEnabledPAC.isLengthStayInfoEnabled;
      } else if (_typeForm == "RSVY") {
        value = _showMandatoryResultEnabledResurvey.isLengthStayInfoEnabled;
      } else if (_typeForm == "DKR") {
        value = _showMandatoryResultEnabledDakor.isLengthStayInfoEnabled;
      }
    }
    else
    if (_typeForm == "SRE") {
      value = _showMandatoryResultEnabledComRegularSurvey.isLengthStayInfoEnabled;
    } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
      value = _showMandatoryResultEnabledComPAC.isLengthStayInfoEnabled;
    } else if (_typeForm == "RSVY") {
      value = _showMandatoryResultEnabledComResurvey.isLengthStayInfoEnabled;
    } else if (_typeForm == "DKR") {
      value = _showMandatoryResultEnabledComDakor.isLengthStayInfoEnabled;
    }
    return value;
  }

  //Widget Add Edit Survey Detail
  //Environmental Info
  bool isEnvironmentalInfoVisible(){
    bool value;
    if(_custType == "PER") {
      if (_typeForm == "SRE") {
        value = _showMandatoryResultEnabledRegularSurvey.isEnvironmentalInfoVisible;
      } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
        value = _showMandatoryResultEnabledPAC.isEnvironmentalInfoVisible;
      } else if (_typeForm == "RSVY") {
        value = _showMandatoryResultEnabledResurvey.isEnvironmentalInfoVisible;
      } else if (_typeForm == "DKR") {
        value = _showMandatoryResultEnabledDakor.isEnvironmentalInfoVisible;
      }
    }
    else
    if (_typeForm == "SRE") {
      value = _showMandatoryResultEnabledComRegularSurvey.isEnvironmentalInfoVisible;
    } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
      value = _showMandatoryResultEnabledComPAC.isEnvironmentalInfoVisible;
    } else if (_typeForm == "RSVY") {
      value = _showMandatoryResultEnabledComResurvey.isEnvironmentalInfoVisible;
    } else if (_typeForm == "DKR") {
      value = _showMandatoryResultEnabledComDakor.isEnvironmentalInfoVisible;
    }
    return value;
  }

  bool isEnvironmentalInfoMandatory(){
    bool value;
    if(_custType == "PER") {
      if (_typeForm == "SRE") {
        value = _showMandatoryResultEnabledRegularSurvey.isEnvironmentalInfoMandatory;
      } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
        value = _showMandatoryResultEnabledPAC.isEnvironmentalInfoMandatory;
      } else if (_typeForm == "RSVY") {
        value = _showMandatoryResultEnabledResurvey.isEnvironmentalInfoMandatory;
      } else if (_typeForm == "DKR") {
        value = _showMandatoryResultEnabledDakor.isEnvironmentalInfoMandatory;
      }
    }
    else
    if (_typeForm == "SRE") {
      value = _showMandatoryResultEnabledComRegularSurvey.isEnvironmentalInfoMandatory;
    } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
      value = _showMandatoryResultEnabledComPAC.isEnvironmentalInfoMandatory;
    } else if (_typeForm == "RSVY") {
      value = _showMandatoryResultEnabledComResurvey.isEnvironmentalInfoMandatory;
    } else if (_typeForm == "DKR") {
      value = _showMandatoryResultEnabledComDakor.isEnvironmentalInfoMandatory;
    }
    return value;
  }

  bool isEnvironmentalInfoEnabled(){
    bool value;
    if(_custType == "PER") {
      if (_typeForm == "SRE") {
        value = _showMandatoryResultEnabledRegularSurvey.isEnvironmentalInfoEnabled;
      } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
        value = _showMandatoryResultEnabledPAC.isEnvironmentalInfoEnabled;
      } else if (_typeForm == "RSVY") {
        value = _showMandatoryResultEnabledResurvey.isEnvironmentalInfoEnabled;
      } else if (_typeForm == "DKR") {
        value = _showMandatoryResultEnabledDakor.isEnvironmentalInfoEnabled;
      }
    }
    else
    if (_typeForm == "SRE") {
      value = _showMandatoryResultEnabledComRegularSurvey.isEnvironmentalInfoEnabled;
    } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
      value = _showMandatoryResultEnabledComPAC.isEnvironmentalInfoEnabled;
    } else if (_typeForm == "RSVY") {
      value = _showMandatoryResultEnabledComResurvey.isEnvironmentalInfoEnabled;
    } else if (_typeForm == "DKR") {
      value = _showMandatoryResultEnabledComDakor.isEnvironmentalInfoEnabled;
    }
    return value;
  }

  //Source Info
  bool isSourceInfoVisible(){
    bool value;
    if(_custType == "PER") {
      if (_typeForm == "SRE") {
        value = _showMandatoryResultEnabledRegularSurvey.isSourceInfoVisible;
      } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
        value = _showMandatoryResultEnabledPAC.isSourceInfoVisible;
      } else if (_typeForm == "RSVY") {
        value = _showMandatoryResultEnabledResurvey.isSourceInfoVisible;
      } else if (_typeForm == "DKR") {
        value = _showMandatoryResultEnabledDakor.isSourceInfoVisible;
      }
    }
    else
    if (_typeForm == "SRE") {
      value = _showMandatoryResultEnabledComRegularSurvey.isSourceInfoVisible;
    } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
      value = _showMandatoryResultEnabledComPAC.isSourceInfoVisible;
    } else if (_typeForm == "RSVY") {
      value = _showMandatoryResultEnabledComResurvey.isSourceInfoVisible;
    } else if (_typeForm == "DKR") {
      value = _showMandatoryResultEnabledComDakor.isSourceInfoVisible;
    }
    return value;
  }

  bool isSourceInfoMandatory(){
    bool value;
    if(_custType == "PER") {
      if (_typeForm == "SRE") {
        value = _showMandatoryResultEnabledRegularSurvey.isSourceInfoMandatory;
      } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
        value = _showMandatoryResultEnabledPAC.isSourceInfoMandatory;
      } else if (_typeForm == "RSVY") {
        value = _showMandatoryResultEnabledResurvey.isSourceInfoMandatory;
      } else if (_typeForm == "DKR") {
        value = _showMandatoryResultEnabledDakor.isSourceInfoMandatory;
      }
    }
    else
    if (_typeForm == "SRE") {
      value = _showMandatoryResultEnabledComRegularSurvey.isSourceInfoMandatory;
    } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
      value = _showMandatoryResultEnabledComPAC.isSourceInfoMandatory;
    } else if (_typeForm == "RSVY") {
      value = _showMandatoryResultEnabledComResurvey.isSourceInfoMandatory;
    } else if (_typeForm == "DKR") {
      value = _showMandatoryResultEnabledComDakor.isSourceInfoMandatory;
    }
    return value;
  }

  bool isSourceInfoEnabled(){
    bool value;
    if(_custType == "PER") {
      if (_typeForm == "SRE") {
        value = _showMandatoryResultEnabledRegularSurvey.isSourceInfoEnabled;
      } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
        value = _showMandatoryResultEnabledPAC.isSourceInfoEnabled;
      } else if (_typeForm == "RSVY") {
        value = _showMandatoryResultEnabledResurvey.isSourceInfoEnabled;
      } else if (_typeForm == "DKR") {
        value = _showMandatoryResultEnabledDakor.isSourceInfoEnabled;
      }
    }
    else
    if (_typeForm == "SRE") {
      value = _showMandatoryResultEnabledComRegularSurvey.isSourceInfoEnabled;
    } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
      value = _showMandatoryResultEnabledComPAC.isSourceInfoEnabled;
    } else if (_typeForm == "RSVY") {
      value = _showMandatoryResultEnabledComResurvey.isSourceInfoEnabled;
    } else if (_typeForm == "DKR") {
      value = _showMandatoryResultEnabledComDakor.isSourceInfoEnabled;
    }
    return value;
  }

  //Source Name Info
  bool isSourceNameInfoVisible(){
    bool value;
    if(_custType == "PER") {
      if (_typeForm == "SRE") {
        value = _showMandatoryResultEnabledRegularSurvey.isSourceNameInfoVisible;
      } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
        value = _showMandatoryResultEnabledPAC.isSourceNameInfoVisible;
      } else if (_typeForm == "RSVY") {
        value = _showMandatoryResultEnabledResurvey.isSourceNameInfoVisible;
      } else if (_typeForm == "DKR") {
        value = _showMandatoryResultEnabledDakor.isSourceNameInfoVisible;
      }
    }
    else
    if (_typeForm == "SRE") {
      value = _showMandatoryResultEnabledComRegularSurvey.isSourceNameInfoVisible;
    } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
      value = _showMandatoryResultEnabledComPAC.isSourceNameInfoVisible;
    } else if (_typeForm == "RSVY") {
      value = _showMandatoryResultEnabledComResurvey.isSourceNameInfoVisible;
    } else if (_typeForm == "DKR") {
      value = _showMandatoryResultEnabledComDakor.isSourceNameInfoVisible;
    }
    return value;
  }

  bool isSourceNameInfoMandatory(){
    bool value;
    if(_custType == "PER") {
      if (_typeForm == "SRE") {
        value = _showMandatoryResultEnabledRegularSurvey.isSourceNameInfoMandatory;
      } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
        value = _showMandatoryResultEnabledPAC.isSourceNameInfoMandatory;
      } else if (_typeForm == "RSVY") {
        value = _showMandatoryResultEnabledResurvey.isSourceNameInfoMandatory;
      } else if (_typeForm == "DKR") {
        value = _showMandatoryResultEnabledDakor.isSourceNameInfoMandatory;
      }
    }
    else
    if (_typeForm == "SRE") {
      value = _showMandatoryResultEnabledComRegularSurvey.isSourceNameInfoMandatory;
    } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
      value = _showMandatoryResultEnabledComPAC.isSourceNameInfoMandatory;
    } else if (_typeForm == "RSVY") {
      value = _showMandatoryResultEnabledComResurvey.isSourceNameInfoMandatory;
    } else if (_typeForm == "DKR") {
      value = _showMandatoryResultEnabledComDakor.isSourceNameInfoMandatory;
    }
    return value;
  }

  bool isSourceNameInfoEnabled(){
    bool value;
    if(_custType == "PER") {
      if (_typeForm == "SRE") {
        value = _showMandatoryResultEnabledRegularSurvey.isSourceNameInfoEnabled;
      } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
        value = _showMandatoryResultEnabledPAC.isSourceNameInfoEnabled;
      } else if (_typeForm == "RSVY") {
        value = _showMandatoryResultEnabledResurvey.isSourceNameInfoEnabled;
      } else if (_typeForm == "DKR") {
        value = _showMandatoryResultEnabledDakor.isSourceNameInfoEnabled;
      }
    }
    else
    if (_typeForm == "SRE") {
      value = _showMandatoryResultEnabledComRegularSurvey.isSourceNameInfoEnabled;
    } else if (_typeForm == "AOS" || _typeForm == "CONA" || _typeForm == "IA" || _typeForm == "PAC") {
      value = _showMandatoryResultEnabledComPAC.isSourceNameInfoEnabled;
    } else if (_typeForm == "RSVY") {
      value = _showMandatoryResultEnabledComResurvey.isSourceNameInfoEnabled;
    } else if (_typeForm == "DKR") {
      value = _showMandatoryResultEnabledComDakor.isSourceNameInfoEnabled;
    }
    return value;
  }

  //Widget Result Survey Photo

  // //Result Survey Photo
  // bool _isResultSurveyPhotoVisible = false;
  // bool _isResultSurveyPhotoMandatory = false;
  //
  // bool get isResultSurveyPhotoMandatory =>
  //     _isResultSurveyPhotoMandatory;
  //
  // set isResultSurveyPhotoMandatory(bool value) {
  //   this._isResultSurveyPhotoMandatory = value;
  // }
  //
  // bool get isResultSurveyPhotoVisible =>
  //     _isResultSurveyPhotoVisible;
  //
  // set isResultSurveyPhotoVisible(bool value) {
  //   this._isResultSurveyPhotoVisible = value;
  // }

  //Widget Add Edit Asset

  // //Add Edit Asset
  // bool _isAddEditAssetVisible = false;
  // bool _isAddEditAssetMandatory = false;
  //
  // bool get isAddEditAssetMandatory =>
  //     _isAddEditAssetMandatory;
  //
  // set isAddEditAssetMandatory(bool value) {
  //   this._isAddEditAssetMandatory = value;
  // }
  //
  // bool get isAddEditAssetVisible =>
  //     _isAddEditAssetVisible;
  //
  // set isResultSurveyCreateEditAssetVisible(bool value) {
  //   this._isAddEditAssetVisible = value;
  // }
  //
  // //Value Asset
  // bool _isValueAssetVisible = false;
  // bool _isValueAssetMandatory = false;
  //
  // bool get isValueAssetMandatory =>
  //     _isValueAssetMandatory;
  //
  // set isValueAssetMandatory(bool value) {
  //   this._isValueAssetMandatory = value;
  // }
  //
  // bool get isValueAssetVisible =>
  //     _isValueAssetVisible;
  //
  // set isValueAssetVisible(bool value) {
  //   this._isValueAssetVisible = value;
  // }
  //
  // //Identity Info
  // bool _isIdentityInfoVisible = false;
  // bool _isIdentityInfoMandatory = false;
  //
  // bool get isIdentityInfoMandatory =>
  //     _isIdentityInfoMandatory;
  //
  // set isIdentityInfoMandatory(bool value) {
  //   this._isIdentityInfoMandatory = value;
  // }
  //
  // bool get isIdentityInfoVisible =>
  //     _isIdentityInfoVisible;
  //
  // set isIdentityInfoVisible(bool value) {
  //   this._isIdentityInfoVisible = value;
  // }
  //
  // //Surface Building Area Info
  // bool _isSurfaceBuildingAreaInfoVisible = false;
  // bool _isSurfaceBuildingAreaInfoMandatory = false;
  //
  // bool get isSurfaceBuildingAreaInfoMandatory =>
  //     _isSurfaceBuildingAreaInfoMandatory;
  //
  // set isSurfaceBuildingAreaInfoMandatory(bool value) {
  //   this._isSurfaceBuildingAreaInfoMandatory = value;
  // }
  //
  // bool get isSurfaceBuildingAreaInfoVisible =>
  //     _isSurfaceBuildingAreaInfoVisible;
  //
  // set isSurfaceBuildingAreaInfoVisible(bool value) {
  //   this._isSurfaceBuildingAreaInfoVisible = value;
  // }
  //
  // //Road Type Info
  // bool _isRoadTypeInfoVisible = false;
  // bool _isRoadTypeInfoMandatory = false;
  //
  // bool get isRoadTypeInfoMandatory =>
  //     _isRoadTypeInfoMandatory;
  //
  // set isRoadTypeInfoMandatory(bool value) {
  //   this._isRoadTypeInfoMandatory = value;
  // }
  //
  // bool get isRoadTypeInfoVisible =>
  //     _isRoadTypeInfoVisible;
  //
  // set isRoadTypeInfoVisible(bool value) {
  //   this._isRoadTypeInfoVisible = value;
  // }
  //
  // //Electricity Type Info
  // bool _isElectricityTypeInfoVisible = false;
  // bool _isElectricityTypeInfoMandatory = false;
  //
  // bool get isElectricityTypeInfoMandatory =>
  //     _isElectricityTypeInfoMandatory;
  //
  // set isElectricityTypeInfoMandatory(bool value) {
  //   this._isElectricityTypeInfoMandatory = value;
  // }
  //
  // bool get isElectricityTypeInfoVisible =>
  //     _isElectricityTypeInfoVisible;
  //
  // set isElectricityTypeInfoVisible(bool value) {
  //   this._isElectricityTypeInfoVisible = value;
  // }
  //
  // //Electricity Bill Info
  // bool _isElectricityBillInfoVisible = false;
  // bool _isElectricityBillInfoMandatory = false;
  //
  // bool get isElectricityBillInfoMandatory =>
  //     _isElectricityBillInfoMandatory;
  //
  // set isElectricityBillInfoMandatory(bool value) {
  //   this._isElectricityBillInfoMandatory = value;
  // }
  //
  // bool get isElectricityBillInfoVisible =>
  //     _isElectricityBillInfoVisible;
  //
  // set isElectricityBillInfoVisible(bool value) {
  //   this._isElectricityBillInfoVisible = value;
  // }
  //
  // //End Date Lease Info
  // bool _isEndDateInfoVisible = false;
  // bool _isEndDateInfoMandatory = false;
  //
  // bool get isEndDateInfoMandatory =>
  //     _isEndDateInfoMandatory;
  //
  // set isEndDateInfoMandatory(bool value) {
  //   this._isEndDateInfoMandatory = value;
  // }
  //
  // bool get isEndDateInfoVisible =>
  //     _isEndDateInfoVisible;
  //
  // set isEndDateInfoVisible(bool value) {
  //   this._isEndDateInfoVisible = value;
  // }
  //
  // //Length Stay Info
  // bool _isLengthStayInfoVisible = false;
  // bool _isLengthStayInfoMandatory = false;
  //
  // bool get isLengthStayInfoMandatory =>
  //     _isLengthStayInfoMandatory;
  //
  // set isLengthStayInfoMandatory(bool value) {
  //   this._isLengthStayInfoMandatory = value;
  // }
  //
  // bool get isLengthStayInfoVisible =>
  //     _isLengthStayInfoVisible;
  //
  // set isLengthStayInfoVisible(bool value) {
  //   this._isLengthStayInfoVisible = value;
  // }


  // //Widget Group Location
  //
  // bool _isResultSurveyGroupLocationVisible = false;
  // bool _isResultSurveyGroupLocationMandatory = false;
  //
  // bool get isResultSurveyGroupLocationMandatory =>
  //     _isResultSurveyGroupLocationMandatory;
  //
  // set isResultSurveyGroupLocationMandatory(bool value) {
  //   this._isResultSurveyGroupLocationMandatory = value;
  // }
  //
  // bool get isResultSurveyGroupLocationVisible =>
  //     _isResultSurveyGroupLocationVisible;
  //
  // set isResultSurveyGroupLocationVisible(bool value) {
  //   this._isResultSurveyGroupLocationVisible = value;
  // }

  // //Widget Add Edit Survey Detail
  //
  // //Source Information
  // bool _isSourceInfoVisible = false;
  // bool _isSourceInfoMandatory = false;
  //
  // bool get isSourceInfoMandatory =>
  //     _isSourceInfoMandatory;
  //
  // set isSourceInfoMandatory(bool value) {
  //   this._isSourceInfoMandatory = value;
  // }
  //
  // bool get isSourceInfoVisible =>
  //     _isSourceInfoVisible;
  //
  // set isSourceInfoVisible(bool value) {
  //   this._isSourceInfoVisible = value;
  // }
  //
  // //Environmental Information
  // bool _isEnvironmentalInfoVisible = false;
  // bool _isEnvironmentalInfoMandatory = false;
  //
  // bool get isEnvironmentalInfoMandatory =>
  //     _isEnvironmentalInfoMandatory;
  //
  // set isEnvironmentalInfoMandatory(bool value) {
  //   this._isEnvironmentalInfoMandatory = value;
  // }
  //
  // bool get isEnvironmentalInfoVisible =>
  //     _isEnvironmentalInfoVisible;
  //
  // set isEnvironmentalInfoVisible(bool value) {
  //   this._isEnvironmentalInfoVisible = value;
  // }
  //
  // //Resource Information
  // bool _isResourceInfoVisible = false;
  // bool _isResourceInfoMandatory = false;
  //
  // bool get isResourceInfoMandatory =>
  //     _isResourceInfoMandatory;
  //
  // set isResourceInfoMandatory(bool value) {
  //   this._isResourceInfoMandatory = value;
  // }
  //
  // bool get isResourceInfoVisible =>
  //     _isResourceInfoVisible;
  //
  // set isResourceInfoVisible(bool value) {

  //   this._isResourceInfoVisible = value;
  // }

  // //Widget Result Survey Group Location
  //
  // //Survey Type Information
  // bool _isSurveyTypeVisible = false;
  // bool _isSurveyTypeMandatory = false;
  //
  // bool get isSurveyTypeMandatory =>
  //     _isSurveyTypeMandatory;
  //
  // set isSurveyTypeMandatory(bool value) {
  //   this._isSurveyTypeMandatory = value;
  // }
  //
  // bool get isSurveyTypeVisible =>
  //     _isSurveyTypeVisible;
  //
  // set isSurveyTypeVisible(bool value) {
  //   this._isSurveyTypeVisible = value;
  // }
  //
  // //Distance Location With Sentra Unit Information
  // bool _isDistanceLocationWithSentraUnitVisible = false;
  // bool _isDistanceLocationWithSentraUnitMandatory = false;
  //
  // bool get isDistanceLocationWithSentraUnitMandatory =>
  //     _isDistanceLocationWithSentraUnitMandatory;
  //
  // set isDistanceLocationWithSentraUnitMandatory(bool value) {
  //   this._isDistanceLocationWithSentraUnitMandatory = value;
  // }
  //
  // bool get isDistanceLocationWithSentraUnitVisible =>
  //     _isDistanceLocationWithSentraUnitVisible;
  //
  // set isDistanceLocationWithSentraUnitVisible(bool value) {
  //   this._isDistanceLocationWithSentraUnitVisible = value;
  // }
  //
  // //Distance Location With Sentra Unit Information
  // bool _isDistanceLocationWithDealerMerchantAXIVisible = false;
  // bool _isDistanceLocationWithDealerMerchantAXIMandatory = false;
  //
  // bool get isDistanceLocationWithDealerMerchantAXIMandatory =>
  //     _isDistanceLocationWithDealerMerchantAXIMandatory;
  //
  // set isDistanceLocationWithDealerMerchantAXIMandatory(bool value) {
  //   this._isDistanceLocationWithDealerMerchantAXIMandatory = value;
  // }
  //
  // bool get isDistanceLocationWithDealerMerchantAXIVisible =>
  //     _isDistanceLocationWithDealerMerchantAXIVisible;
  //
  // set isDistanceLocationWithDealerMerchantAXIVisible(bool value) {
  //   this._isDistanceLocationWithDealerMerchantAXIVisible = value;
  // }
  //
  // //Distance Location With Sentra Unit Information
  // bool _isDistanceLocationWithUsageObjectWithSentraUnitVisible = false;
  // bool _isDistanceLocationWithUsageObjectWithSentraUnitMandatory = false;
  //
  // bool get isDistanceLocationWithUsageObjectWithSentraUnitMandatory =>
  //     _isDistanceLocationWithUsageObjectWithSentraUnitMandatory;
  //
  // set isDistanceLocationWithUsageObjectWithSentraUnitMandatory(bool value) {
  //   this._isDistanceLocationWithUsageObjectWithSentraUnitMandatory = value;
  // }
  //
  // bool get isDistanceLocationWithUsageObjectWithSentraUnitVisible =>
  //     _isDistanceLocationWithUsageObjectWithSentraUnitVisible;
  //
  // set isDistanceLocationWithUsageObjectWithSentraUnitVisible(bool value) {
  //   this._isDistanceLocationWithUsageObjectWithSentraUnitVisible = value;
  // }

  bool get isResultSurveyChanges => _isResultSurveyChanges;

  set isResultSurveyChanges(bool value) {
    this._isResultSurveyChanges = value;
  }

  bool get isSourceNameInfoChanges => _isSourceNameInfoChanges;

  set isSourceNameInfoChanges(bool value) {
    this._isSourceNameInfoChanges = value;
  }

  bool get isSourceInfoChanges => _isSourceInfoChanges;

  set isSourceInfoChanges(bool value) {
    this._isSourceInfoChanges = value;
  }

  bool get isEnvironmentalInfoChanges => _isEnvironmentalInfoChanges;

  set isEnvironmentalInfoChanges(bool value) {
    this._isEnvironmentalInfoChanges = value;
  }

  bool get isLengthStayInfoChanges => _isLengthStayInfoChanges;

  set isLengthStayInfoChanges(bool value) {
    this._isLengthStayInfoChanges = value;
  }

  bool get isEndDateInfoChanges => _isEndDateInfoChanges;

  set isEndDateInfoChanges(bool value) {
    this._isEndDateInfoChanges = value;
  }

  bool get isElectricityBillInfoChanges => _isElectricityBillInfoChanges;

  set isElectricityBillInfoChanges(bool value) {
    this._isElectricityBillInfoChanges = value;
  }

  bool get isElectricityTypeInfoChanges => _isElectricityTypeInfoChanges;

  set isElectricityTypeInfoChanges(bool value) {
    this._isElectricityTypeInfoChanges = value;
  }

  bool get isRoadTypeInfoChanges => _isRoadTypeInfoChanges;

  set isRoadTypeInfoChanges(bool value) {
    this._isRoadTypeInfoChanges = value;
  }

  bool get isSurfaceBuildingAreaInfoChanges =>
      _isSurfaceBuildingAreaInfoChanges;

  set isSurfaceBuildingAreaInfoChanges(bool value) {
    this._isSurfaceBuildingAreaInfoChanges = value;
  }

  bool get isIdentityInfoChanges => _isIdentityInfoChanges;

  set isIdentityInfoChanges(bool value) {
    this._isIdentityInfoChanges = value;
  }

  bool get isValueAssetChanges => _isValueAssetChanges;

  set isValueAssetChanges(bool value) {
    this._isValueAssetChanges = value;
  }

  bool get isAssetTypeChanges => _isAssetTypeChanges;

  set isAssetTypeChanges(bool value) {
    this._isAssetTypeChanges = value;
  }

  bool get isSurveyTypeChanges => _isSurveyTypeChanges;

  set isSurveyTypeChanges(bool value) {
    this._isSurveyTypeChanges = value;
  }

  bool get isPhotoTypeChanges => _isPhotoTypeChanges;

  set isPhotoTypeChanges(bool value) {
    this._isPhotoTypeChanges = value;
  }

  bool get isSurveyDateChanges => _isSurveyDateChanges;

  set isSurveyDateChanges(bool value) {
    this._isSurveyDateChanges = value;
  }

  bool get isSurveyNotesChanges => _isSurveyNotesChanges;

  set isSurveyNotesChanges(bool value) {
    this._isSurveyNotesChanges = value;
  }

  bool get isDistanceLocationWithUsageObjectWithSentraUnitChanges => _isDistanceLocationWithUsageObjectWithSentraUnitChanges;

  set isDistanceLocationWithUsageObjectWithSentraUnitChanges(bool value) {
    this._isDistanceLocationWithUsageObjectWithSentraUnitChanges = value;
  }

  bool get isDistanceLocationWithSentraUnitChanges => _isDistanceLocationWithSentraUnitChanges;

  set isDistanceLocationWithSentraUnitChanges(bool value) {
    this._isDistanceLocationWithSentraUnitChanges = value;
  }

  bool get isSurveyRecommendationChanges => _isSurveyRecommendationChanges;

  set isSurveyRecommendationChanges(bool value) {
    this._isSurveyRecommendationChanges = value;
  }

  bool get isResultSurveyGroupNotesChanges => _isResultSurveyGroupNotesChanges;

  set isResultSurveyGroupNotesChanges(bool value) {
    this._isResultSurveyGroupNotesChanges = value;
  }

  bool get isDistanceLocationWithDealerMerchantAXIChanges => _isDistanceLocationWithDealerMerchantAXIChanges;

  set isDistanceLocationWithDealerMerchantAXIChanges(bool value) {
    this._isDistanceLocationWithDealerMerchantAXIChanges = value;
  }

  bool get isSourceInfoNameChange => _isSourceInfoNameChange;

  set isSourceInfoNameChange(bool value) {
    this._isSourceInfoNameChange = value;
  }

  bool get isSourceInformationChange => _isSourceInformationChange;

  set isSourceInformationChange(bool value) {
    this._isSourceInformationChange = value;
  }

  bool get isEnvInformationChange => _isEnvInformationChange;

  set isEnvInformationChange(bool value) {
    this._isEnvInformationChange = value;
  }

  bool get isEditListResultSurveyDetail => _isEditListResultSurveyDetail;

  set isEditListResultSurveyDetail(bool value) {
    this._isEditListResultSurveyDetail = value;
  }

  bool get isEditResultSurveyAsset => _isEditResultSurveyAsset;

  set isEditResultSurveyAsset(bool value) {
    this._isEditResultSurveyAsset = value;
  }

  List<RecommModel> get listRecomm => _listRecomm;


  //fungsi untuk get rekomendasi dari server
  Future<void> _getRekom() async{
    this._listRecomm.clear();
    List _application = await _dbHelper.selectMS2Application();
    var _body = jsonEncode({
      "P_APPL_NO": "${_application[0]['applNo']}"
    });
    String _getRekomen = await storage.read(key: "GetRekommen");
    final _response = await http.post(
        "${BaseUrl.urlGeneral}$_getRekomen",
        // "${BaseUrl.urlGeneral}ms2/api/approval/get-rekommen",
        body: _body,
        headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
    );

    debugPrint("${_response.statusCode}");
    if(_response.statusCode == 200){
      debugPrint("IF");
      final _result = jsonDecode(_response.body);
      List _data = _result['data'];
      for(int i=0; i < _data.length; i++){
        _listRecomm.add(
          RecommModel(
              _data[i]['LEVEL_KOMITE'] != null ? _data[i]['LEVEL_KOMITE'] : "-",
              _data[i]['FIELD'] != null ? _data[i]['FIELD'] : "-",
              _data[i]['REKOMENDASI'] != null ? _data[i]['REKOMENDASI'] : "-",
              _data[i]['KETERANGAN'] != null ? _data[i]['KETERANGAN'] : "-",
              _data[i]['CREATED_BY'] != null ? _data[i]['CREATED_BY'] : "-",
          )
        );
      }
    }
    else{
      debugPrint("ELSE");
      _snackBarSurvey("Error get rekomendasi response ${_response.statusCode}");
    }
  }

  void showSnackBar(String text){
    this._scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor));
  }

  void _snackBarSurvey(String text){
    this._scaffoldKeySurvey.currentState.showSnackBar(new SnackBar(
        content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor));
  }
}