import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/screens/dashboard.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/responsive_screen.dart';
import 'package:ad1ms2_dev/shared/upper_clipper.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/io_client.dart';
import 'package:random_string/random_string.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

import '../main.dart';

class LoginChangeNotifier with ChangeNotifier {
    TextEditingController _controllerEmail = TextEditingController();
    TextEditingController _controllerPassword = TextEditingController();
    String _randomText;
    TextEditingController _valueRandomText = TextEditingController();
    bool _secureText = true;
    bool _autoValidate = false;
    bool _loginProcess = false;
    GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
    GlobalKey<FormState> _key = GlobalKey<FormState>();
    var storage = FlutterSecureStorage();
    bool _resultCheckVersion = false;

    bool get resultCheckVersion => _resultCheckVersion;

    set resultCheckVersion(bool value) {
        _resultCheckVersion = value;
        notifyListeners();
    }

    TextEditingController get controllerEmail => _controllerEmail;
    TextEditingController get controllerPassword => _controllerPassword;
    bool get secureText => _secureText;
    bool get loginProcess => _loginProcess;

    set loginProcess(bool value) {
        _loginProcess = value;
        notifyListeners();
    }

    set secureText(bool value) {
        this._secureText = value;
        notifyListeners();
    }

    bool get autoValidate => _autoValidate;

    set autoValidate(bool value) {
        this._autoValidate = value;
        notifyListeners();
    }

    GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;
    GlobalKey<FormState> get key => _key;

    showHidePass() {
       _secureText = !_secureText;
       notifyListeners();
    }

    Widget upperPart(BuildContext context) {
        var _size = Screen(MediaQuery.of(context).size);
        return Stack(
            children: <Widget>[
                ClipPath(
                    clipper: UpperClipper(),
                    child: Container(
                        height: _size.hp(35),
                        decoration: BoxDecoration(color: myPrimaryColor),
                    ),
                ),
                Container(
                    margin: EdgeInsets.only(top: _size.hp(4), left: _size.wp(8)),
                    child: Image.asset('img/logo_adira.png', height: 150, width: 150))
            ],
        );
    }

    void setRandomText(int status) {
        _randomText = randomAlphaNumeric(4);
        if(status == 1){
            notifyListeners();
        }
    }

    void checkLogin(BuildContext context) async{
        this._loginProcess = true;
        SharedPreferences _preference = await SharedPreferences.getInstance();
        String _token = _preference.getString("token");
        if(_token != null){
            if(await _checkToken(_token)){
                Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => Dashboard()));
            }
            else{
                this._loginProcess = false;
            }
        }
        this._loginProcess = false;
        notifyListeners();
    }

    Future<bool> _checkToken(String token) async{
        bool _status = false;
        loginProcess = true;
        final ioc = new HttpClient();
        ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
        final _http = IOClient(ioc);
        String _checkToken = await storage.read(key: "CheckToken");
        try {
            final _response = await _http.post(
                "${BaseUrl.urlGeneralPublic}$_checkToken",
                body: {
                    "token": token
                },
            ).timeout(Duration(seconds: 30));
            final _result = jsonDecode(_response.body);
            if (_response.statusCode == 200) {
                if(_result['active']){
                    _status = true;
                }
                else{
                    showSnackBar(_result['error_description']);
                }
            }
            else {
                showSnackBar("Token was not recognised");
            }
        }
        on TimeoutException catch(_){
            loginProcess = false;
            showSnackBar("Timeout connection");
        }
        catch (e) {
            loginProcess = false;
            showSnackBar(e.toString());
        }
        loginProcess = false;
        return _status;
    }

    bool _errorRandomNotmatch = false;

    bool get errorRandomNotmatch => _errorRandomNotmatch;

    set errorRandomNotmatch(bool value) {
        this._errorRandomNotmatch = value;
        notifyListeners();
    }

    void validasiRandomText(BuildContext context) {
        if (_valueRandomText.text != _randomText) {
            _errorRandomNotmatch = true;
            autoValidate = true;
            _valueRandomText.clear();
            setRandomText(1);
        } else {
            _errorRandomNotmatch = false;
        }
    }

    void check(BuildContext context) async{
        validasiRandomText(context);
        if(_controllerEmail.text != "" && _controllerPassword.text != "" && _valueRandomText.text != "") {
            // await checkVersion(context);
            // if(this._resultCheckVersion) {
                try {
                    String _login = await storage.read(key: "Login");
                    SharedPreferences _preferences = await SharedPreferences.getInstance();
                    loginProcess = true;
                    final ioc = new HttpClient();
                    ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
                    final _http = IOClient(ioc);
                    print("${BaseUrl.urlGeneralPublic}$_login");
                    // if(_form.validate()){
                    final _response = await _http.post(
                        "${BaseUrl.urlGeneralPublic}$_login",
                        // "${urlPublic}api/account/authuser",
                        // "https://mobilegatewayuat.adira.co.id/uat/public/ms2/api/account/authuser",
                        body: {
                            "login": _controllerEmail.text,
                            "password": _controllerPassword.text
                        },
                        // headers: {"Content-Type":"application/json"}
                    ).timeout(Duration(seconds: 30));

                    final _result = jsonDecode(_response.body);
                    print("cek login ${_response.statusCode}");
                    if (_result['result']) {
                        // Ngecek stop selling
                        // String _salesStopSelling = await storage.read(key: "SalesStopSelling");
                        // ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
                        // var _bodySalesStopSelling = jsonEncode({
                        //     "SZNIK": _controllerEmail.text
                        // });
                        // try {
                        // final _response = await _http.post(
                        //     "${BaseUrl.urlGeneral}$_salesStopSelling",
                        //     body: _bodySalesStopSelling,
                        //     headers: {"Content-Type":"application/json"}
                        // );
                        // final _resultStopSelling = jsonDecode(_response.body);
                        // if (_resultStopSelling['message'] == "SUCCESS") {
                        // jika message SUCCESS (bisa login)
                        // Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => Dashboard()));
                        // loginProcess = false;
                        // showSnackBar("Stop Selling - Hubungi PIC Cabang");
                        // _valueRandomText.clear();
                        // setRandomText(1);
                        // } else {
                        // jika message FAILED (tidak bisa login)
                        // showSnackBar(_result['message']);
                        // _valueRandomText.clear();
                        // setRandomText();
                        _preferences.setString("username", "${_result['username']}");
                        _preferences.setString("fullname", "${_result['fullname']}");
                        _preferences.setString("job_id", "${_result['job_id']}");
                        _preferences.setString("job_name", "${_result['job_name']}");
                        _preferences.setString("is_mayor", "${_result['is_mayor']}");
                        _preferences.setString("branchid", "${_result['branchid']}");
                        _preferences.setString("ou_type", "${_result['ou_type']}");
                        _preferences.setString("UnitD", "${_result['UnitD']}");
                        _preferences.setString("SentraD", "${_result['SentraD']}");
                        _preferences.setString("token", "${_result['token']}");
                        loginProcess = false;
                        Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => Dashboard()));
                        // }
                        // } catch (e) {
                        //     loginProcess = false;
                        //     showSnackBar(e.toString());
                        // }
                    } else {
                        showSnackBar(_result['message']);
                        _valueRandomText.clear();
                        setRandomText(1);
                    }
                    // } else {
                    //     autoValidate = true;
                    // }
                }
                on TimeoutException catch(_){
                    loginProcess = false;
                    showSnackBar("Timeout connection");
                }
                catch (e) {
                    // print(e);
                    loginProcess = false;
                    showSnackBar(e.toString());
                }
                loginProcess = false;
            // }
            // else {
            //     showDialog(
            //         context: context,
            //         barrierDismissible: true,
            //         builder: (BuildContext context){
            //             return Theme(
            //                 data: ThemeData(
            //                     fontFamily: "NunitoSans",
            //                     primaryColor: Colors.black,
            //                     primarySwatch: primaryOrange,
            //                     accentColor: myPrimaryColor
            //                 ),
            //                 child: AlertDialog(
            //                     title: Text("Get the latest MS2 updates", style: TextStyle(fontWeight: FontWeight.bold)),
            //                     content: Column(
            //                         crossAxisAlignment: CrossAxisAlignment.start,
            //                         mainAxisSize: MainAxisSize.min,
            //                         children: <Widget>[
            //                             Text("Invalid Version, please get the latest MS2 updates",),
            //                         ],
            //                     ),
            //                     actions: <Widget>[
            //                         new FlatButton(
            //                             onPressed: () => Navigator.of(context).pop(true),
            //                             child: new Text('Close'),
            //                         ),
            //                     ],
            //                 ),
            //             );
            //         }
            //     );
            // }
        }
        else {
            autoValidate = true;
        }
    }

    Future<void> checkVersion(BuildContext context) async{
        print("MASUK CHECK VERSION");
        final ioc = new HttpClient();
        ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;

        final _http = IOClient(ioc);
        print("${BaseUrl.urlGeneralPublic}ms2/api/list-param/get_list_param");
        try{
            final _response = await _http.get("${BaseUrl.urlGeneralPublic}ms2/api/list-param/get_list_param",).timeout(Duration(seconds: 60));
            if(_response.statusCode == 200){
                final _result = jsonDecode(_response.body);
                for(int i=0; i<_result['model'].length; i++){
                    //xapikey
                    if(_result['model'][i]['PARAM_NAME'] == "KeyECM"){
                        xApiKey = _result['model'][i]['PARAM_VALUE'];
                        print("api key $xApiKey");
                    }
                    //check version
                    else if(_result['model'][i]['PARAM_NAME'] == "Version"){
                        print("versi ${_result['model'][i]['PARAM_VALUE']} == ${versionApp(1)}");
                        print(_result['model'][i]['PARAM_VALUE']);
                        print(versionApp(0));
                        if(_result['model'][i]['PARAM_VALUE'] == versionApp(1)){
                            print("sesuai version app");
                            this._resultCheckVersion = true;
                        }
                    }
                }
            }
            else{
                throw Exception("Failed get data error ${_response.statusCode}");
            }
        }
        on TimeoutException catch(_){
            throw "Timeout connection";
        }
        catch (e) {
            throw "${e.toString()}";
        }
    }

    showSnackBar(String text){
        this._scaffoldKey.currentState.showSnackBar(new SnackBar(
            content: Text("$text"), behavior: SnackBarBehavior.floating,
            backgroundColor: snackbarColor, duration: Duration(seconds: 2))
        );
    }

    TextEditingController get valueRandomText => _valueRandomText;

    String get randomText => _randomText;

    set randomText(String value) {
        this._randomText = value;
        notifyListeners();
    }

    void dialogAbout(BuildContext context) {
        showDialog(
            context: context,
            barrierDismissible: true,
            builder: (BuildContext context){
                return Theme(
                    data: ThemeData(
                        fontFamily: "NunitoSans"
                    ),
                    child: AlertDialog(
                        title: Text("About", style: TextStyle(fontWeight: FontWeight.bold)),
                        content: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                                Row(
                                    mainAxisSize: MainAxisSize.min,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: <Widget>[
                                        Text("version ${versionApp(0)}")
                                        // SizedBox(width: 12.0),
                                    ]
                                ),
                            ],
                        ),
                        actions: <Widget>[
                            FlatButton(
                                onPressed: (){
                                    Navigator.pop(context);
                                },
                                child: Text(
                                    "CLOSE",
                                    style: TextStyle(
                                        color: primaryOrange,
                                        fontSize: 14,
                                        fontWeight: FontWeight.w500,
                                        letterSpacing: 1.25
                                    )
                                )
                            )
                        ],
                    ),
                );
            }
        );
    }
}
