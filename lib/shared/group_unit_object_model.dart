import 'package:ad1ms2_dev/models/form_m_foto_model.dart';

class GroupUnitObjectModel {
  final GroupObjectUnit groupObjectUnit;
  final ObjectUnit objectUnit;
  final List<ImageFileModel> listImageGroupObjectUnit;
  final bool isDeleted;
  final bool isEdit;
  final bool isEditGroupObjectUnit;
  final bool isEditObjectUnit;
  final bool isEditListImageGroupObjectUnit;

  GroupUnitObjectModel(
      this.groupObjectUnit, this.objectUnit, this.listImageGroupObjectUnit, this.isDeleted,
      this.isEdit, this.isEditGroupObjectUnit, this.isEditObjectUnit, this.isEditListImageGroupObjectUnit);
}

class GroupObjectUnit {
  final String id;
  final String groupObject;

  GroupObjectUnit(this.id, this.groupObject);
}

class ObjectUnit {
  final String id;
  final String objectUnit;

  ObjectUnit(this.id, this.objectUnit);
}
