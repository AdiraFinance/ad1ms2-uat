import 'dart:collection';
import 'dart:io';

import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/models/birth_place_model.dart';
import 'package:ad1ms2_dev/models/form_m_company_alamat_model.dart';
import 'package:ad1ms2_dev/models/form_m_company_manajemen_pic_alamat_model.dart';
import 'package:ad1ms2_dev/models/form_m_company_manajemen_pic_model.dart';
import 'package:ad1ms2_dev/models/form_m_company_rincian_model.dart';
import 'package:ad1ms2_dev/models/manajemen_pic_sqlite_model.dart';
import 'package:ad1ms2_dev/models/showhide_mandatory_visibility/set_management_pic_company_model.dart';
import 'package:ad1ms2_dev/screens/search_birth_place.dart';
import 'package:ad1ms2_dev/shared/dashboard/dashboard_change_notif.dart';
import 'package:ad1ms2_dev/shared/date_picker.dart';
import 'package:ad1ms2_dev/shared/search_birth_place_change_notifier.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'constants.dart';

class FormMCompanyManajemenPICChangeNotifier with ChangeNotifier {
  bool _autoValidate = false;
  int _selectedIndex = -1;
  TypeIdentityModel _typeIdentitySelected;
  TextEditingController _controllerIdentityNumber = TextEditingController();
  TextEditingController _controllerFullNameIdentity = TextEditingController();
  TextEditingController _controllerFullName = TextEditingController();
  TextEditingController _controllerAlias = TextEditingController();
  TextEditingController _controllerDegree = TextEditingController();
  TextEditingController _controllerBirthOfDate = TextEditingController();
  DateTime _initialDateForBirthOfDate = DateTime(dateNow.year, dateNow.month, dateNow.day);
  TextEditingController _controllerPlaceOfBirthIdentity = TextEditingController();
  TextEditingController _controllerBirthPlaceIdentityLOV = TextEditingController();
  TextEditingController _controllerPosition = TextEditingController();
  TextEditingController _controllerEmail = TextEditingController();
  TextEditingController _controllerHandphoneNumber = TextEditingController();
  BirthPlaceModel _birthPlaceSelected;
  GlobalKey<FormState> _key = GlobalKey<FormState>();
  bool _flag = false;
  DbHelper _dbHelper = DbHelper();
  String _custType;
  String _lastKnownState;
  RelationshipStatusModel _positionSelected;
  List<RelationshipStatusModel> _listPosition = RelationshipStatusList().relationshipStatusItemsPemegangSaham;

  RelationshipStatusModel get positionSelected => _positionSelected;

  set positionSelected(RelationshipStatusModel value) {
    _positionSelected = value;
  }

  UnmodifiableListView<RelationshipStatusModel> get listPosition {
    return UnmodifiableListView(this._listPosition);
  }

  SetManagementPICCompanyModel _setManagementPICModel;

  bool _identityTypeDakor = false;
  bool _identityNoDakor = false;
  bool _fullnameIdDakor = false;
  bool _fullnameDakor = false;
  bool _dateOfBirthDakor = false;
  bool _placeOfBirthDakor = false;
  bool _placeOfBirthLOVDakor = false;
  bool _positionDakor = false;
  bool _emailDakor = false;
  bool _phoneDakor = false;

  bool get autoValidate => _autoValidate;

  set autoValidate(bool value) {
    this._autoValidate = value;
    notifyListeners();
  }

  List<TypeIdentityModel> _listTypeIdentity = [
//    TypeIdentityModel("01", "KTP"),
//    TypeIdentityModel("02", "NPWP"),
    TypeIdentityModel("01", "KTP"),
    TypeIdentityModel("03", "PASSPORT"),
    TypeIdentityModel("04", "SIM"),
    TypeIdentityModel("05", "KTP Sementara"),
    TypeIdentityModel("06", "Resi KTP"),
    TypeIdentityModel("07", "Ket. Domisili"),
  ];

  // Index
  int get selectedIndex => _selectedIndex;

  set selectedIndex(int value) {
    this._selectedIndex = value;
    notifyListeners();
  }

  // Jenis Identitas
  UnmodifiableListView<TypeIdentityModel> get listTypeIdentity {
    return UnmodifiableListView(this._listTypeIdentity);
  }

  TypeIdentityModel get typeIdentitySelected => _typeIdentitySelected;

  set typeIdentitySelected(TypeIdentityModel value) {
    this._typeIdentitySelected = value;
    this._controllerIdentityNumber.clear();
    notifyListeners();
  }

  // No Identitas
  TextEditingController get controllerIdentityNumber {
    return this._controllerIdentityNumber;
  }

  set controllerIdentityNumber(value) {
    this._controllerIdentityNumber = value;
    this.notifyListeners();
  }

  // Nama Lengkap Sesuai Identitas
  TextEditingController get controllerFullNameIdentity {
    return this._controllerFullNameIdentity;
  }

  set controllerFullNameIdentity(value) {
    this._controllerFullNameIdentity = value;
    this.notifyListeners();
  }

  // Nama Lengkap
  TextEditingController get controllerFullName {
    return this._controllerFullName;
  }

  set controllerFullName(value) {
    this._controllerFullName = value;
    this.notifyListeners();
  }

  // Alias
  TextEditingController get controllerAlias {
    return this._controllerAlias;
  }

  set controllerAlias(value) {
    this._controllerAlias = value;
    this.notifyListeners();
  }

  // Gelar
  TextEditingController get controllerDegree {
    return this._controllerDegree;
  }

  set controllerDegree(value) {
    this._controllerDegree = value;
    this.notifyListeners();
  }

  // Tanggal Lahir
  TextEditingController get controllerBirthOfDate => _controllerBirthOfDate;

  DateTime get initialDateForBirthOfDate => _initialDateForBirthOfDate;

  void selectBirthDate(BuildContext context) async {
    // DatePickerShared _datePickerShared = DatePickerShared();
    // var _datePickerSelected = await _datePickerShared.selectStartDate(
    //     context, this._initialDateForBirthOfDate,
    //     canAccessNextDay: false);
    // if (_datePickerSelected != null) {
    //   this.controllerBirthOfDate.text = dateFormat.format(_datePickerSelected);
    //   this._initialDateForBirthOfDate = _datePickerSelected;
    //   notifyListeners();
    // } else {
    //   return;
    // }
    var _datePickerSelected = await selectDate(context, this._initialDateForBirthOfDate);
    if (_datePickerSelected != null) {
      this.controllerBirthOfDate.text = dateFormat.format(_datePickerSelected);
      this._initialDateForBirthOfDate = _datePickerSelected;
      notifyListeners();
    } else {
      return;
    }
  }

  // Tempat Lahir Sesuai Identitas
  TextEditingController get controllerPlaceOfBirthIdentity {
    return this._controllerPlaceOfBirthIdentity;
  }

  set controllerPlaceOfBirthIdentity(value) {
    this._controllerPlaceOfBirthIdentity = value;
    this.notifyListeners();
  }

  // Tempat Lahir Sesuai Identitas LOV
  TextEditingController get controllerBirthPlaceIdentityLOV =>
      _controllerBirthPlaceIdentityLOV;

  set controllerBirthPlaceIdentityLOV(TextEditingController value) {
    this._controllerBirthPlaceIdentityLOV = value;
    notifyListeners();
  }

  // Jabatan
  TextEditingController get controllerPosition {
    return this._controllerPosition;
  }

  set controllerPosition(value) {
    this._controllerPosition = value;
    this.notifyListeners();
  }

  // Email
  TextEditingController get controllerEmail {
    return this._controllerEmail;
  }

  set controllerEmail(value) {
    this._controllerEmail = value;
    this.notifyListeners();
  }

  // Handphone
  TextEditingController get controllerHandphoneNumber {
    return this._controllerHandphoneNumber;
  }

  set controllerHandphoneNumber(value) {
    this._controllerHandphoneNumber = value;
    this.notifyListeners();
  }

  String validateEmail(String value) {
    if (value.isEmpty && isEmailMandatory()) {
      // The form is empty
      return "Enter email address";
    }
    // This is just a regular expression for email addresses
    String p = "[a-zA-Z0-9\+\.\_\%\-\+]{1,256}" +
        "\\@" +
        "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
        "(" +
        "\\." +
        "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
        ")+";
    RegExp regExp = new RegExp(p);

    if (regExp.hasMatch(value)) {
      // So, the email is valid
      return null;
    }
    // The pattern of the email didn't match the regex above.
    return 'Email is not valid';
  }

  GlobalKey<FormState> get keyForm => _key;

  bool get flag => _flag;

  set flag(bool value) {
    _flag = value;
    notifyListeners();
  }


  BirthPlaceModel get birthPlaceSelected => _birthPlaceSelected;

  set birthPlaceSelected(BirthPlaceModel value) {
    this._birthPlaceSelected = value;
    notifyListeners();
  }

  void check(BuildContext context) {
    final _form = _key.currentState;
    if (_form.validate()) {
      checkDataDakor();
      flag = true;
      autoValidate = false;
      Navigator.pop(context);
    } else {
      flag = false;
      autoValidate = true;
    }
  }

  Future<bool> onBackPress() async{
    final _form = _key.currentState;
    if (_form.validate()) {
      flag = true;
      autoValidate = false;
    } else {
      flag = false;
      autoValidate = true;
    }
    return true;
  }

  void searchBirthPlace(BuildContext context) async{
    BirthPlaceModel data = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => SearchBirthPlaceChangeNotifier(),
                child: SearchBirthPlace())));
    if (data != null) {
      this._birthPlaceSelected = data;
      this._controllerBirthPlaceIdentityLOV.text = "${data.KABKOT_ID} - ${data.KABKOT_NAME}";
      notifyListeners();
    } else {
      return;
    }
  }

  String get custType => _custType;

  set custType(String value) {
    this._custType = value;
    notifyListeners();
  }

  String get lastKnownState => _lastKnownState;

  set lastKnownState(String value) {
    this._lastKnownState = value;
    notifyListeners();
  }

  Future<void> setPreference(BuildContext context) async {
    SharedPreferences _preference = await SharedPreferences.getInstance();
    this._custType = _preference.getString("cust_type");
    this._lastKnownState = _preference.getString("last_known_state");
  }

  // Future<void> clearDataManajemenPIC() async {
  void clearDataManajemenPIC() {
    this._flag = false;
    this._autoValidate = false;
    this._selectedIndex = -1;
    this._typeIdentitySelected = null;
    this._controllerIdentityNumber.clear();
    this._controllerFullNameIdentity.clear();
    this._controllerFullName.clear();
    // this._controllerAlias.clear();
    // this._controllerDegree.clear();
    this._controllerBirthOfDate.clear();
    this._controllerPlaceOfBirthIdentity.clear();
    this._controllerBirthPlaceIdentityLOV.clear();
    this._positionSelected = null;
    // _controllerPosition.clear();
    // _controllerEmail.clear();
    this._controllerHandphoneNumber.clear();
    this._identityTypeDakor = false;
    this._identityNoDakor = false;
    this._fullnameIdDakor = false;
    this._fullnameDakor = false;
    this._dateOfBirthDakor = false;
    this._placeOfBirthDakor = false;
    this._placeOfBirthLOVDakor = false;
    this._positionDakor = false;
    this._emailDakor = false;
    this._phoneDakor = false;
  }

  void saveToSQLite() async{
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    _dbHelper.insertMS2CustomerCompanyForPIC(
        ManagementPICSQLiteModel(
            this._typeIdentitySelected,
            this._controllerIdentityNumber.text,
            this._controllerFullNameIdentity.text,
            this._controllerFullName.text,
            this._controllerAlias.text,
            this._controllerDegree.text,
            this._initialDateForBirthOfDate.toString(),
            this._controllerPlaceOfBirthIdentity.text,
            this._birthPlaceSelected,
            null,
            null,
            null,
            null,
            null,
            null,
            this._positionSelected != null ? this._positionSelected.PARA_FAMILY_TYPE_ID : "", // AC_LEVEL
            null, // POSITION | this._controllerPosition.text
            this._controllerHandphoneNumber.text,
            this._controllerEmail.text,
            null, null,
            DateTime.now().toString(),
            _preferences.getString("username"),
            null, null, 1, null,
            this._identityTypeDakor ? "1" : "0",
            this._identityNoDakor ? "1" : "0",
            this._fullnameIdDakor ? "1" : "0",
            this._fullnameDakor ? "1" : "0",
            this._dateOfBirthDakor ? "1" : "0",
            this._placeOfBirthDakor ? "1" : "0",
            this._placeOfBirthLOVDakor ? "1" : "0",
            this._positionDakor ? "1" : "0",
            this._emailDakor ? "1" : "0",
            this._phoneDakor ? "1" : "0"
        ));
  }


  void setDataSQLite() async{
    this._positionSelected = null;
    List _dataCustomerCompanyForPIC = await _dbHelper.selectMS2CustomerCompanyForPIC();
    if(_dataCustomerCompanyForPIC[0]['id_type'] != null){
      for(int i=0; i < this._listTypeIdentity.length; i++){
        if(_dataCustomerCompanyForPIC[0]['id_type'] == _listTypeIdentity[i].id){
          _typeIdentitySelected = _listTypeIdentity[i];
        }
      }
      this._controllerIdentityNumber.text = _dataCustomerCompanyForPIC[0]['id_no'].toString();
      this._controllerFullNameIdentity.text = _dataCustomerCompanyForPIC[0]['full_name_id'].toString();
      this._controllerFullName.text = _dataCustomerCompanyForPIC[0]['full_name'].toString();
      this._initialDateForBirthOfDate = _dataCustomerCompanyForPIC[0]['date_of_birth'] != "null" ? DateTime.parse(_dataCustomerCompanyForPIC[0]['date_of_birth']) : DateTime(dateNow.year, dateNow.month, dateNow.day);
      this._controllerBirthOfDate.text = _dataCustomerCompanyForPIC[0]['date_of_birth'] != "null" ? dateFormat.format(DateTime.parse(_dataCustomerCompanyForPIC[0]['date_of_birth'])) : "";
      this._controllerPlaceOfBirthIdentity.text = _dataCustomerCompanyForPIC[0]['place_of_birth'].toString();
      this._birthPlaceSelected = BirthPlaceModel(_dataCustomerCompanyForPIC[0]['place_of_birth_kabkota'].toString(), _dataCustomerCompanyForPIC[0]['place_of_birth_kabkota_desc'].toString());
      this._controllerBirthPlaceIdentityLOV.text = "${_dataCustomerCompanyForPIC[0]['place_of_birth_kabkota'].toString()} -${_dataCustomerCompanyForPIC[0]['place_of_birth_kabkota_desc'].toString()}";
      for(int i=0; i < _listPosition.length; i++) {
        if(_listPosition[i].PARA_FAMILY_TYPE_ID == _dataCustomerCompanyForPIC[0]['ac_level'].toString()) {
          this._positionSelected = _listPosition[i];
        }
      }
      // this._controllerPosition.text = _dataCustomerCompanyForPIC[0]['position'].toString();
      this._controllerEmail.text = _dataCustomerCompanyForPIC[0]['email'].toString();
      this._controllerHandphoneNumber.text = _dataCustomerCompanyForPIC[0]['handphone_no'].toString();
      checkDataDakor();
    }
    notifyListeners();
  }

  void getDataFromDashboard(BuildContext context){
    _setManagementPICModel = Provider.of<DashboardChangeNotif>(context, listen: false).setManagementPICModel;
  }

  bool isIdentityTypeVisible() => _setManagementPICModel.identityTypeVisible;
  bool isIdentityNoVisible() => _setManagementPICModel.identityNoVisible;
  bool isFullnameIdVisible() => _setManagementPICModel.fullnameIdVisible;
  bool isFullnameVisible() => _setManagementPICModel.fullnameVisible;
  bool isDateOfBirthVisible() => _setManagementPICModel.dateOfBirthVisible;
  bool isPlaceOfBirthVisible() => _setManagementPICModel.placeOfBirthVisible;
  bool isPlaceOfBirthLOVVisible() => _setManagementPICModel.placeOfBirthLOVVisible;
  bool isPositionVisible() => _setManagementPICModel.positionVisible;
  bool isEmailVisible() => _setManagementPICModel.emailVisible;
  bool isPhoneVisible() => _setManagementPICModel.phoneVisible;

  bool isIdentityTypeMandatory() => _setManagementPICModel.identityTypeMandatory;
  bool isIdentityNoMandatory() => _setManagementPICModel.identityNoMandatory;
  bool isFullnameIdMandatory() => _setManagementPICModel.fullnameIdMandatory;
  bool isFullnameMandatory() => _setManagementPICModel.fullnameMandatory;
  bool isDateOfBirthMandatory() => _setManagementPICModel.dateOfBirthMandatory;
  bool isPlaceOfBirthMandatory() => _setManagementPICModel.placeOfBirthMandatory;
  bool isPlaceOfBirthLOVMandatory() => _setManagementPICModel.placeOfBirthLOVMandatory;
  bool isPositionMandatory() => _setManagementPICModel.positionMandatory;
  bool isEmailMandatory() => _setManagementPICModel.emailMandatory;
  bool isPhoneMandatory() => _setManagementPICModel.phoneMandatory;

  void checkDataDakor() async{
    List _data = await _dbHelper.selectMS2CustomerCompanyForPIC();
    debugPrint("LAST_KNOWN_STATE $_lastKnownState");
    debugPrint("ID_TYPE ${_data[0]['id_type']}");
    debugPrint("DATE_BIRTH ${_data[0]['date_of_birth']}");
    if(_data[0]['id_type'] != null && _lastKnownState == "DKR"){
      if(this._typeIdentitySelected != null){
        identityTypeDakor = this._typeIdentitySelected.id != _data[0]['id_type'].toString() || _data[0]['edit_id_type'] == "1";
      }
      identityNoDakor = this._controllerIdentityNumber.text != _data[0]['id_no'].toString() || _data[0]['edit_id_no'] == "1";
      fullnameIdDakor = this._controllerFullNameIdentity.text != _data[0]['full_name_id'].toString() || _data[0]['edit_full_name_id'] == "1";
      fullnameDakor = this._controllerFullName.text != _data[0]['full_name'].toString() || _data[0]['edit_full_name'] == "1";
      dateOfBirthDakor = this._controllerBirthOfDate.text != dateFormat.format(DateTime.parse(_data[0]['date_of_birth'].toString())) || _data[0]['edit_date_of_birth'] == "1";
      placeOfBirthDakor = this._controllerPlaceOfBirthIdentity.text != _data[0]['place_of_birth'].toString() || _data[0]['edit_place_of_birth'] == "1";
      placeOfBirthLOVDakor = birthPlaceSelected.KABKOT_ID != _data[0]['place_of_birth_kabkota'].toString() || _data[0]['edit_place_of_birth_kabkota'] == "1";
      positionDakor = this._positionSelected.PARA_FAMILY_TYPE_ID != _data[0]['ac_level'].toString() || _data[0]['edit_ac_level'] == "1";
      // positionDakor = this._controllerPosition.text != _data[0]['position'].toString() || _data[0]['edit_position'] == "1";
      emailDakor = this._controllerEmail.text != _data[0]['email'].toString() || _data[0]['edit_email'] == "1";
      phoneDakor = this._controllerHandphoneNumber.text != _data[0]['handphone_no'].toString() || _data[0]['edit_handphone_no'] == "1";
    }
    notifyListeners();
  }

  bool get phoneDakor => _phoneDakor;

  set phoneDakor(bool value) {
    _phoneDakor = value;
    notifyListeners();
  }

  bool get emailDakor => _emailDakor;

  set emailDakor(bool value) {
    _emailDakor = value;
    notifyListeners();
  }

  bool get positionDakor => _positionDakor;

  set positionDakor(bool value) {
    _positionDakor = value;
    notifyListeners();
  }

  bool get placeOfBirthLOVDakor => _placeOfBirthLOVDakor;

  set placeOfBirthLOVDakor(bool value) {
    _placeOfBirthLOVDakor = value;
    notifyListeners();
  }

  bool get placeOfBirthDakor => _placeOfBirthDakor;

  set placeOfBirthDakor(bool value) {
    _placeOfBirthDakor = value;
    notifyListeners();
  }

  bool get dateOfBirthDakor => _dateOfBirthDakor;

  set dateOfBirthDakor(bool value) {
    _dateOfBirthDakor = value;
    notifyListeners();
  }

  bool get fullnameDakor => _fullnameDakor;

  set fullnameDakor(bool value) {
    _fullnameDakor = value;
    notifyListeners();
  }

  bool get fullnameIdDakor => _fullnameIdDakor;

  set fullnameIdDakor(bool value) {
    _fullnameIdDakor = value;
    notifyListeners();
  }

  bool get identityNoDakor => _identityNoDakor;

  set identityNoDakor(bool value) {
    _identityNoDakor = value;
    notifyListeners();
  }

  bool get identityTypeDakor => _identityTypeDakor;

  set identityTypeDakor(bool value) {
    _identityTypeDakor = value;
    notifyListeners();
  }
}
