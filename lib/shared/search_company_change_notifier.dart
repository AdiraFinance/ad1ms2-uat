import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/company_model.dart';
import 'package:ad1ms2_dev/models/form_m_pekerjaan_model.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_object_unit_change_notifier.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/form_m_foto_change_notif.dart';
import 'package:ad1ms2_dev/shared/list_oid_change_notifier.dart';
import 'package:ad1ms2_dev/shared/resource/get_company_type.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/io_client.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:ad1ms2_dev/shared/form_m_informasi_alamat_change_notif.dart';
import 'form_m_company_alamat_change_notif.dart';

class SearchCompanyChangeNotifier with ChangeNotifier {
    bool _showClear = false;
    TextEditingController _controllerSearch = TextEditingController();
    bool _loadData = false;
    GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

    List<CompanyTypeModel> _listCompany = [
//        CompanyModel("01", "Company A"),
//        CompanyModel("02", "Company B"),
//        CompanyModel("03", "Company C")
    ];
    List<CompanyTypeModel> _listCompanyTemp = [];

    TextEditingController get controllerSearch => _controllerSearch;

    bool get showClear => _showClear;

    set showClear(bool value) {
        this._showClear = value;
        notifyListeners();
    }

    void changeAction(String value) {
        if (value != "") {
            showClear = true;
        } else {
            showClear = false;
        }
    }

    UnmodifiableListView<CompanyTypeModel> get listCompanyModel {
        return UnmodifiableListView(this._listCompany);
    }

    UnmodifiableListView<CompanyTypeModel> get listCompanyTemp {
        return UnmodifiableListView(this._listCompanyTemp);
    }

    bool get loadData => _loadData;

    set loadData(bool value) {
        this._loadData = value;
    }

    GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

    void getCompanyType(BuildContext context) async{
        print("masuk");
        this._listCompany.clear();
        loadData = true;
        SharedPreferences _preference = await SharedPreferences.getInstance();
        var _providerInfoAlamat = Provider.of<FormMInfoAlamatChangeNotif>(context,listen: false);
        var _providerAlamatLembaga = Provider.of<FormMCompanyAlamatChangeNotifier>(context,listen: false);
        var _kecamatanId = "";
        for(int i=0; i<_providerInfoAlamat.listAlamatKorespondensi.length;i++) {
            if(_providerInfoAlamat.listAlamatKorespondensi[i].isCorrespondence) {
                _kecamatanId = _providerInfoAlamat.listAlamatKorespondensi[i].kelurahanModel.KEC_ID;
                break;
            }
        }

        try{
            final ioc = new HttpClient();
            ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
            final _http = IOClient(ioc);
            var _body = jsonEncode({
                // "P_INSR_LEVEL": "$flag",
                // "P_PARA_CP_SENTRA": _preferences.getString("SentraD"),
                // "P_PARA_FIN_TYPE": _preferences.getString("cust_type") == "COM" ? "1" : "",
                // "P_PARA_OBJECT": Provider.of<InformationObjectUnitChangeNotifier>(context, listen: false).objectSelected.id,
                // "P_PARA_OBJECT_BRAND": Provider.of<InformationObjectUnitChangeNotifier>(context, listen: false).brandObjectSelected.id,
                // "P_PARA_OBJECT_GENRE": Provider.of<InformationObjectUnitChangeNotifier>(context, listen: false).modelObjectSelected.id,
                // "P_PARA_OBJECT_TYPE": Provider.of<InformationObjectUnitChangeNotifier>(context, listen: false).objectTypeSelected.id,
                // "P_PARA_OUTLET": Provider.of<InformationObjectUnitChangeNotifier>(context, listen: false).sourceOrderNameSelected.kode
                // "refOne": _preference.getString("cust_type") == "PER" ? _providerInfoAlamat.kelurahanSelected.KEC_ID : _providerAlamatLembaga.controllerKelurahan.text
                "P_PARA_KECAMATAN_ID": _kecamatanId.toString()
            });
            print("body company = $_body");

            var storage = FlutterSecureStorage();
            String _fieldPerusahaan = await storage.read(key: "FieldPerusahaanKaroseri");
            print("${BaseUrl.urlGeneral}$_fieldPerusahaan");
            final _response = await _http.post(
                "${BaseUrl.urlGeneral}$_fieldPerusahaan",
                // "${urlPublic}api/asuransi/get-insurance-company",
                body: _body,
                headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
            );
            print("response: ${_response.statusCode}");
            if(_response.statusCode == 200){
                var _result = jsonDecode(_response.body);
                print("result api perusahaan karoseri: $_result");
                var _data = _result['data'];
                if(_data.isNotEmpty){
                    for(int i = 0 ; i < _data.length ; i++) {
                        _listCompany.add(CompanyTypeModel(_data[i]['KODE'].toString(), _data[i]['DESKRIPSI'].toString()));
                    }
                }
                this._loadData = false;
            } else {
                showSnackBar("Error response status ${_response.statusCode}");
                this._loadData = false;
            }
            // final _response = await _http.post(
            //     "https://ad1ms2newdev/api/paramcredcalc/get-insurance-type",
            //     body: _body,
            //     headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
            // );
            //
            // var _result = await GetCompanyType().getCompanyTypeData();
            // for(int i=0; i <_result['data'].length; i++){
            //     this._listCompany.add(
            //         CompanyTypeModel(_result['data'][i]['kode'], _result['data'][i]['deskripsi'])
            //     );
            // }
            // loadData = false;
        }
        catch(e){
            loadData = false;
            showSnackBar(e.toString());
        }
        notifyListeners();
    }

    void showSnackBar(String text){
        this._scaffoldKey.currentState.showSnackBar(new SnackBar(
            content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor));
    }

    void searchCompanyType(String query) {
        if(query.length < 3) {
            showSnackBar("Input minimal 3 karakter");
        } else {
            _listCompanyTemp.clear();
            if (query.isEmpty) {
                return;
            }

            _listCompany.forEach((dataCompanyType) {
                if (dataCompanyType.id.contains(query) || dataCompanyType.desc.contains(query)) {
                    _listCompanyTemp.add(dataCompanyType);
                }
            });
        }
        notifyListeners();
    }

    void clearSearchTemp() {
        _listCompanyTemp.clear();
    }
}
