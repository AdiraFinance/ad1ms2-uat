import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/third_party_model.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_application_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_object_unit_change_notifier.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/form_m_info_nasabah_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_informasi_alamat_change_notif.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/io_client.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'form_m_company_alamat_change_notif.dart';

class SearchThirdPartyChangeNotifier with ChangeNotifier {
  bool _showClear = false;
  bool _isAllSentra = true;
  int _radioValueNameOrCityFilter = 0;
  TextEditingController _controllerSearch = TextEditingController();
  bool _loadData = false;
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  List<ThirdPartyModel> _listThirdParty = [
    // ThirdPartyModel("001956", "ALMAS MOTOR",
    //     "Jl. Pahlawan No.17, Wire, Gedongombo, Semanding,Tuban"),
    // ThirdPartyModel("019526", "PATRA MOTOR", "Jl Raya Tajur No 7 Bogor"),
  ];

  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  TextEditingController get controllerSearch => _controllerSearch;

  bool get showClear => _showClear;

  set showClear(bool value) {
    this._showClear = value;
    notifyListeners();
  }

  bool get isAllSentra => _isAllSentra;

  set isAllSentra(bool value) {
    this._isAllSentra = value;
    notifyListeners();
  }

  void changeAction(String value) {
    if (value != "") {
      showClear = true;
    } else {
      showClear = false;
    }
  }

  int get radioValueNameOrCityFilter => _radioValueNameOrCityFilter;

  set radioValueNameOrCityFilter(int value) {
    this._radioValueNameOrCityFilter = value;
    notifyListeners();
  }

  UnmodifiableListView<ThirdPartyModel> get listThirdParty {
    return UnmodifiableListView(this._listThirdParty);
  }

  bool get loadData => _loadData;

  set loadData(bool value) {
    this._loadData = value;
  }

  void getThirdParty(String flag, BuildContext context, String query) async{
    print(flag);
    var _providerObjectUnit = Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false);
    var _providerInfoApp = Provider.of<InfoAppChangeNotifier>(context,listen: false);
    // var _providerInfoCust = Provider.of<FormMInfoAlamatChangeNotif>(context,listen: false);
    // var _providerInfoCom = Provider.of<FormMCompanyAlamatChangeNotifier>(context,listen: false);
    SharedPreferences _pref = await SharedPreferences.getInstance();
    this._listThirdParty.clear();
    loadData = true;
    final ioc = new HttpClient();
    ioc.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;

    final _http = IOClient(ioc);

    var _body = jsonEncode({
      "refOne": "${_providerObjectUnit.thirdPartyTypeSelected.kode}",
      "refTwo": "${_providerObjectUnit.brandObjectSelected.id}",
      "refThree": "",
      "refFour": "${_providerInfoApp.controllerTotalObject.text}",
      "refFive": "${_pref.getString("SentraD")}",
      "refSix": query, //"${_providerObjectUnit.sourceOrderNameSelected.deskripsi}",
      "refSeven": "", // flag != "COM" ? "${_providerInfoCust.kelurahanSelected.KABKOT_ID}" : "${_providerInfoCom.kelurahanSelected.KABKOT_ID}"
      "refEight": this._isAllSentra ?"1":"0",
      "refNine": "01",
      "refTen": "${_pref.getString("SentraD")}",
      "refEleven": "${_pref.getString("UnitD")}"
    });
    print("get nama pihak ketiga = $_body");

    var storage = FlutterSecureStorage();
    String _fieldPihakKetiga = await storage.read(key: "FieldPihakKetiga");
    final _response = await _http.post(
        "${BaseUrl.urlGeneral}$_fieldPihakKetiga",
        //   "${urlPublic}pihak-ketiga/get_nama_pihakketiga",
        body: _body,
        headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
    );
    if(_response.statusCode == 200){
      final _result = jsonDecode(_response.body);
      print("result: $_result");
      if(_result == null){
        showSnackBar("Pihak ketiga tidak ditemukan");
        loadData = false;
      }
      else{
        if(_result.isNotEmpty) {
          for(int i=0; i <_result.length; i++){
            this._listThirdParty.add(
                ThirdPartyModel(_result[i]['kode'], _result[i]['deskripsi'], _result[i]['sentraId'],
                    _result[i]['sentraName'], _result[i]['unitId'], _result[i]['unitName'].toString().trim(),
                    _result[i]['kabKotID'], _result[i]['kabKotName'].toString().trim(), _result[i]['address']
                )
            );
          }
          if(_radioValueNameOrCityFilter == 0){
            _listThirdParty.sort((a, b) => a.kode.compareTo(b.kode));
          }
          else{
            _listThirdParty.sort((a, b) => a.kabKotName.compareTo(b.kabKotName));
          }
        } else {
          showSnackBar("Pihak ketiga tidak ditemukan");
        }
        loadData = false;
      }
    }
    else{
      showSnackBar("Error response status ${_response.statusCode}");
      this._loadData = false;
    }
    notifyListeners();
  }

  void showSnackBar(String text){
    this._scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor));
  }

  // void searchThirdParty(String query) {
  //   if(query.length < 3) {
  //     showSnackBar("Input minimal 3 karakter");
  //   } else {
  //     _listThirdPartyTemp.clear();
  //     if (query.isEmpty) {
  //       return;
  //     }
  //
  //     _listThirdParty.forEach((dataSourceOrder) {
  //       if (dataSourceOrder.kode.contains(query) || dataSourceOrder.deskripsi.contains(query)) {
  //         _listThirdPartyTemp.add(dataSourceOrder);
  //       }
  //     });
  //   }
  //   notifyListeners();
  // }

  void clearSearchTemp() {
    // _listSourceOrderTemp.clear();
  }
}
