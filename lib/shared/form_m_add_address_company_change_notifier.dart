import 'dart:collection';

import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/models/address_guarantor_model.dart';
import 'package:ad1ms2_dev/models/form_m_informasi_alamat_model.dart';
import 'package:ad1ms2_dev/models/showhide_mandatory_visibility/set_address_company_model.dart';
import 'package:ad1ms2_dev/screens/form_m/search_kelurahan.dart';
import 'package:ad1ms2_dev/screens/map_page.dart';
import 'package:ad1ms2_dev/shared/dashboard/dashboard_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_add_guarantor_company_change_notifier.dart';
import 'package:ad1ms2_dev/shared/resource/address_type_resource.dart';
import 'package:ad1ms2_dev/shared/search_kelurahan_change_notif.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../main.dart';
import 'add_pemegang_saham_lembaga_change_notif.dart';
import 'form_m_company_alamat_change_notif.dart';
import 'form_m_company_pemegang_saham_kelembagaan_alamat_change_notif.dart';
import 'form_m_occupation_change_notif.dart';

class FormMAddAddressCompanyChangeNotifier with ChangeNotifier {
  bool _autoValidate = false;
  bool _isSameWithIdentity = false;
  bool _isCorrespondence = false;
  TextEditingController _controllerAlamat = TextEditingController();
  TextEditingController _controllerRT = TextEditingController();
  TextEditingController _controllerRW = TextEditingController();
  TextEditingController _controllerKelurahan = TextEditingController();
  TextEditingController _controllerKecamatan = TextEditingController();
  TextEditingController _controllerKota = TextEditingController();
  TextEditingController _controllerProvinsi = TextEditingController();
  TextEditingController _controllerTelephone1Area = TextEditingController();
  TextEditingController _controllerTelephone1 = TextEditingController();
  TextEditingController _controllerTelephone2Area = TextEditingController();
  TextEditingController _controllerTelephone2 = TextEditingController();
  TextEditingController _controllerFaxArea = TextEditingController();
  TextEditingController _controllerFax = TextEditingController();
  TextEditingController _controllerPostalCode = TextEditingController();
  TextEditingController _controllerAddressFromMap = TextEditingController();
  JenisAlamatModel _jenisAlamatSelected;
  JenisAlamatModel _jenisAlamatSelectedTemp;
  KelurahanModel _kelurahanSelected;
  KelurahanModel _kelurahanSelectedTemp;
  AddressModelCompany _addressModelTemp;
  String _alamatTemp,
      _rtTemp,
      _rwTemp,
      _kelurahanTemp,
      _kecamatanTemp,
      _kotaTemp,
      _provinsiTemp,
      _phone1AreaTemp,
      _phone1Temp,
      _phone2AreaTemp,
      _phone2Temp,
      _faxAreaTemp,
      _faxTemp,
      _postalCodeTemp;
  GlobalKey<FormState> _key = GlobalKey<FormState>();
  bool _enableTfAddress = true;
  bool _enableTfRT = true;
  bool _enableTfRW = true;
  bool _enableTfKelurahan = true;
  bool _enableTfKecamatan = true;
  bool _enableTfKota = true;
  bool _enableTfProv = true;
  bool _enableTfPostalCode = true;
  bool _enableTfTelephone1Area = true;
  bool _enableTfTelephone2Area = true;
  bool _enableTfPhone1 = true;
  bool _enableTfPhone2 = true;
  bool _enableTfFaxArea = true;
  bool _enableTfFax = true;
  Map _addressFromMap;
  DbHelper _dbHelper = DbHelper();

  bool _isEditAddress = false;
  bool _isAddressTypeDakor = false;
  bool _isAddressDakor = false;
  bool _isRTDakor = false;
  bool _isRWDakor = false;
  bool _isKelurahanDakor = false;
  bool _isKecamatanDakor = false;
  bool _isKabKotDakor = false;
  bool _isProvinsiDakor = false;
  bool _isKodePosDakor = false;
  bool _isTelp1AreaDakor = false;
  bool _isTelp1Dakor = false;
  bool _isTelp2AreaDakor = false;
  bool _isTelp2Dakor = false;
  bool _isFaxAreaDakor = false;
  bool _isFaxDakor = false;
  bool _isDataLatLongDakor = false;

  SetAddressCompanyModel _setAddressCompanyGuarantorIndividuModel, _setAddressCompanyGuarantorModel, _setAddressCompanyModel,_setAddressCompanySahamModel;
  var _custType;
  String _addressID = "NEW";
  String _foreignBusinessID = "NEW";
  int _active = 0;

  List<JenisAlamatModel> _listJenisAlamat = [];

  Future<void> addDataListJenisAlamat(AddressModelCompany data, BuildContext context, int flag,
      bool isSameWithIdentity,int index, int typeAddress) async{
      var _provider = Provider.of<FormMAddGuarantorCompanyChangeNotifier>(context, listen: false);
      var _providerAlamatCompany = Provider.of<FormMCompanyAlamatChangeNotifier>(context, listen: false);
      var _providerSahamLembaga = Provider.of<AddPemegangSahamLembagaChangeNotif>(context, listen: false);
      try {
    //typeAddres 1 = list alamat guarantor addres individu
    //typeAddres 2 = list alamat company
    //typeAddres 3 = list saham lembaga
          this._listJenisAlamat.clear();
          if (flag == 0) {
              print("create baru");
              // add
              if(typeAddress == 1){
                  this._listJenisAlamat = await getAddressType(10);
                  // remove used type
                  for(int x=0; x<_provider.listGuarantorAddress.length; x++){
                      for(int i=0; i < _listJenisAlamat.length;i++){
                          if(_listJenisAlamat[i].KODE == _provider.listGuarantorAddress[x].jenisAlamatModel.KODE){
                              _listJenisAlamat.removeAt(i);
                          }
                      }
                  }
              }
              if(typeAddress == 2){
                  if (_providerAlamatCompany.listAlamatKorespondensi.isEmpty) {
                      this._listJenisAlamat = await getAddressType(1);
                      if(this._listJenisAlamat.isNotEmpty) {
                          for (int i = 0; i < _listJenisAlamat.length; i++) {
                              if (_listJenisAlamat[i].KODE == "03") {
                                  this._jenisAlamatSelected =
                                  this._listJenisAlamat[i];
                              }
                          }
                          List _dedup = await _dbHelper.selectDataInfoNasabahFromDataDedup();
                          if(_dedup.isNotEmpty){
                              if(_dedup[0]['alamat'] != "null"){
                                  this._controllerAlamat.text = _dedup[0]['alamat'];
                              }
                          }
                      }
                  }
                  else {
                      this._listJenisAlamat = await getAddressType(3);
                      // remove used type
                      for(int x=0; x<_providerAlamatCompany.listAlamatKorespondensi.length; x++){
                          for(int i=0; i < _listJenisAlamat.length;i++){
                              if(_listJenisAlamat[i].KODE == _providerAlamatCompany.listAlamatKorespondensi[x].jenisAlamatModel.KODE){
                                  _listJenisAlamat.removeAt(i);
                              }
                          }
                      }
                  }
              }
              if(typeAddress == 3){
                  // if (_providerSahamLembaga.listPemegangSahamKelembagaanAddress.isEmpty) {
                  //     this._listJenisAlamat = await getAddressType(1);
                  // }
                  // else {
                      this._listJenisAlamat = await getAddressType(10);
                      // remove used type
                      for(int x=0; x<_providerSahamLembaga.listPemegangSahamKelembagaanAddress.length; x++){
                          for(int i=0; i < _listJenisAlamat.length;i++){
                              if(_listJenisAlamat[i].KODE == _providerSahamLembaga.listPemegangSahamKelembagaanAddress[x].jenisAlamatModel.KODE){
                                  _listJenisAlamat.removeAt(i);
                              }
                          }
                      }
                  // }
              }
          }
          else {
              // edit
              if(typeAddress == 1){
                  this._listJenisAlamat = await getAddressType(10);
                  for(int x=0; x<_provider.listGuarantorAddress.length; x++){
                      for(int i=0; i < _listJenisAlamat.length;i++){
                          if(_listJenisAlamat[i].KODE == _provider.listGuarantorAddress[x].jenisAlamatModel.KODE && _listJenisAlamat[i].KODE != data.jenisAlamatModel.KODE){
                              _listJenisAlamat.removeAt(i);
                          }
                      }
                  }
              }
              if(typeAddress == 2){
                  print("edit 2");
                  if (_providerAlamatCompany.listAlamatKorespondensi[index].jenisAlamatModel.KODE == '03') {
                      this._listJenisAlamat = await getAddressType(1);
                  } else if (_providerAlamatCompany.listAlamatKorespondensi[index].jenisAlamatModel.KODE == '01'){
                      this._listJenisAlamat = await getAddressType(2);
                  }
                  else {
                      this._listJenisAlamat = await getAddressType(3);
                  }
                  // remove used type
                  for(int x=0; x<_providerAlamatCompany.listAlamatKorespondensi.length; x++){
                      for(int i=0; i < _listJenisAlamat.length;i++){
                          if(_listJenisAlamat[i].KODE == _providerAlamatCompany.listAlamatKorespondensi[x].jenisAlamatModel.KODE && _listJenisAlamat[i].KODE != data.jenisAlamatModel.KODE){
                              _listJenisAlamat.removeAt(i);
                          }
                      }
                  }
              }
              if(typeAddress == 3){
                  // this._listJenisAlamat = await getAddressType(9);
                  // for(int x=0; x<_providerSahamLembaga.listPemegangSahamKelembagaanAddress.length; x++){
                  //     for(int i=0; i < _listJenisAlamat.length;i++){
                  //         if(_listJenisAlamat[i].KODE == _providerSahamLembaga.listPemegangSahamKelembagaanAddress[x].jenisAlamatModel.KODE && _listJenisAlamat[i].KODE != data.jenisAlamatModel.KODE){
                  //             _listJenisAlamat.removeAt(i);
                  //         }
                  //     }
                  // }
                  if (_providerSahamLembaga.listPemegangSahamKelembagaanAddress[index].jenisAlamatModel.KODE == '03') {
                      this._listJenisAlamat = await getAddressType(1);
                  } else if (_providerSahamLembaga.listPemegangSahamKelembagaanAddress[index].jenisAlamatModel.KODE == '01'){
                      this._listJenisAlamat = await getAddressType(2);
                  }
                  else {
                      this._listJenisAlamat = await getAddressType(3);
                  }
                  // remove used type
                  for(int x=0; x<_providerSahamLembaga.listPemegangSahamKelembagaanAddress.length; x++){
                      for(int i=0; i < _listJenisAlamat.length;i++){
                          if(_listJenisAlamat[i].KODE == _providerSahamLembaga.listPemegangSahamKelembagaanAddress[x].jenisAlamatModel.KODE && _listJenisAlamat[i].KODE != data.jenisAlamatModel.KODE){
                              _listJenisAlamat.removeAt(i);
                          }
                      }
                  }
              }
              setValueForEdit(data, context, index, typeAddress);
          }
          notifyListeners();
      } catch (e) {
          print(e);
      }
  }

  bool get autoValidate => _autoValidate;

  set autoValidate(bool value) {
      this._autoValidate = value;
      notifyListeners();
  }

  TextEditingController get controllerPostalCode => _controllerPostalCode;

  TextEditingController get controllerTelephone1 => _controllerTelephone1;

  TextEditingController get controllerTelephone1Area => _controllerTelephone1Area;

  TextEditingController get controllerProvinsi => _controllerProvinsi;

  TextEditingController get controllerKota => _controllerKota;

  TextEditingController get controllerKecamatan => _controllerKecamatan;

  TextEditingController get controllerKelurahan => _controllerKelurahan;

  TextEditingController get controllerRT => _controllerRT;

  TextEditingController get controllerRW => _controllerRW;

  TextEditingController get controllerAlamat => _controllerAlamat;

  TextEditingController get controllerTelephone2Area => _controllerTelephone2Area;

  TextEditingController get controllerTelephone2 => _controllerTelephone2;

  TextEditingController get controllerFax => _controllerFax;

  TextEditingController get controllerFaxArea => _controllerFaxArea;

  JenisAlamatModel get jenisAlamatSelected => _jenisAlamatSelected;

  TextEditingController get controllerAddressFromMap => _controllerAddressFromMap;

  set jenisAlamatSelected(JenisAlamatModel value) {
      this._jenisAlamatSelected = value;
      notifyListeners();
  }

  KelurahanModel get kelurahanSelected => _kelurahanSelected;

  set kelurahanSelected(KelurahanModel value) {
      this._kelurahanSelected = value;
      notifyListeners();
  }

  UnmodifiableListView<JenisAlamatModel> get listJenisAlamat {
      return UnmodifiableListView(this._listJenisAlamat);
  }

  bool get isCorrespondence => _isCorrespondence;

  set isCorrespondence(bool value) {
      this._isCorrespondence = value;
      notifyListeners();
  }

  bool get isSameWithIdentity => _isSameWithIdentity;

  set isSameWithIdentity(bool value) {
      this._isSameWithIdentity = value;
      notifyListeners();
  }

  bool get enableTfPhone1 => _enableTfPhone1;

  set enableTfPhone1(bool value) {
      this._enableTfPhone1 = value;
  }

  bool get enableTfTelephone1Area => _enableTfTelephone1Area;

  set enableTfTelephone1Area(bool value) {
      this._enableTfTelephone1Area = value;
  }

  bool get enableTfTelephone2Area => _enableTfTelephone2Area;

  set enableTfTelephone2Area(bool value) {
      this._enableTfTelephone2Area = value;
  }

  bool get enableTfPhone2 => _enableTfPhone2;

  set enableTfPhone2(bool value) {
      this._enableTfPhone2 = value;
  }

  bool get enableTfFax => _enableTfFax;

  set enableTfFax(bool value) {
      this._enableTfFax = value;
  }

  bool get enableTfFaxArea => _enableTfFaxArea;

  set enableTfFaxArea(bool value) {
      this._enableTfFaxArea = value;
  }

  bool get enableTfPostalCode => _enableTfPostalCode;

  set enableTfPostalCode(bool value) {
      this._enableTfPostalCode = value;
  }

  bool get enableTfProv => _enableTfProv;

  set enableTfProv(bool value) {
      this._enableTfProv = value;
  }

  bool get enableTfKota => _enableTfKota;

  set enableTfKota(bool value) {
      this._enableTfKota = value;
  }

  bool get enableTfKecamatan => _enableTfKecamatan;

  set enableTfKecamatan(bool value) {
      this._enableTfKecamatan = value;
  }

  bool get enableTfKelurahan => _enableTfKelurahan;

  set enableTfKelurahan(bool value) {
      this._enableTfKelurahan = value;
  }

  bool get enableTfRW => _enableTfRW;

  set enableTfRW(bool value) {
      this._enableTfRW = value;
  }

  bool get enableTfRT => _enableTfRT;

  set enableTfRT(bool value) {
      this._enableTfRT = value;
  }

  bool get enableTfAddress => _enableTfAddress;

  set enableTfAddress(bool value) {
      this._enableTfAddress = value;
  }

  String get addressID => _addressID;

  set addressID(String value) {
    this._addressID = value;
  }

  String get foreignBusinessID => _foreignBusinessID;

  set foreignBusinessID(String value) {
    this._foreignBusinessID = value;
  }

  void isShowDialog(BuildContext context, int flag, int index, int typeAddress) {
        showDialog(
            context: context,
            barrierDismissible: true,
            builder: (BuildContext context){
                return Theme(
                    data: ThemeData(
                        fontFamily: "NunitoSans"
                    ),
                    child: AlertDialog(
                        title: Text("Informasi", style: TextStyle(fontWeight: FontWeight.bold)),
                        content: Text(typeAddress == 2 ? "Apakah alamat Kantor sama dengan alamat Identitas" : "Apakah alamat Domisili sama dengan alamat Identitas?"),
                        actions: <Widget>[
                            FlatButton(
                                onPressed: (){
                                    _isSameWithIdentity = true;
                                    if(typeAddress == 1){
                                        Provider.of<FormMAddGuarantorCompanyChangeNotifier>(context,
                                            listen: true)
                                            .addGuarantorAddress(AddressModelCompany(
                                            this._jenisAlamatSelected,
                                            this._addressID,
                                            this._foreignBusinessID,
                                            null, //no id
                                            this._kelurahanSelected,
                                            this._controllerAlamat.text,
                                            this._controllerRT.text,
                                            this._controllerRW.text,
                                            this._controllerTelephone1Area.text,
                                            this._controllerTelephone1.text,
                                            this._controllerTelephone2Area.text,
                                            this._controllerTelephone2.text,
                                            this._controllerFaxArea.text,
                                            this._controllerFax.text,
                                            this._isSameWithIdentity,
                                            this._addressFromMap,
                                            this._isCorrespondence,
                                            this._active,
                                            this._isEditAddress,
                                            this._isAddressTypeDakor,
                                            this._isAddressDakor,
                                            this._isRTDakor,
                                            this._isRWDakor,
                                            this._isKelurahanDakor,
                                            this._isKecamatanDakor,
                                            this._isKabKotDakor,
                                            this._isProvinsiDakor,
                                            this._isKodePosDakor,
                                            this._isTelp1AreaDakor,
                                            this._isTelp1Dakor,
                                            this._isTelp2AreaDakor,
                                            this._isTelp2Dakor,
                                            this._isFaxAreaDakor,
                                            this._isFaxDakor,
                                            this._isDataLatLongDakor));

                                        Provider.of<FormMAddGuarantorCompanyChangeNotifier>(context,
                                            listen: true)
                                            .addGuarantorAddress(AddressModelCompany(
                                            JenisAlamatModel("01", "DOMISILI"),
                                            this._addressID,
                                            this._foreignBusinessID,
                                            null, //no id
                                            this._kelurahanSelected,
                                            this._controllerAlamat.text,
                                            this._controllerRT.text,
                                            this._controllerRW.text,
                                            this._controllerTelephone1Area.text,
                                            this._controllerTelephone1.text,
                                            this._controllerTelephone2Area.text,
                                            this._controllerTelephone2.text,
                                            this._controllerFaxArea.text,
                                            this._controllerFax.text,
                                            this._isSameWithIdentity,
                                            this._addressFromMap,
                                            this._isCorrespondence,
                                            this._active,
                                            this._isEditAddress,
                                            this._isAddressTypeDakor,
                                            this._isAddressDakor,
                                            this._isRTDakor,
                                            this._isRWDakor,
                                            this._isKelurahanDakor,
                                            this._isKecamatanDakor,
                                            this._isKabKotDakor,
                                            this._isProvinsiDakor,
                                            this._isKodePosDakor,
                                            this._isTelp1AreaDakor,
                                            this._isTelp1Dakor,
                                            this._isTelp2AreaDakor,
                                            this._isTelp2Dakor,
                                            this._isFaxAreaDakor,
                                            this._isFaxDakor,
                                            this._isDataLatLongDakor));
                                    }
                                    else if(typeAddress == 2){
                                        Provider.of<FormMCompanyAlamatChangeNotifier>(context,
                                            listen: true)
                                            .addAlamatKorespondensi(AddressModelCompany(
                                            this._jenisAlamatSelected,
                                            this._addressID,
                                            this._foreignBusinessID,
                                            null, //no id
                                            this._kelurahanSelected,
                                            this._controllerAlamat.text,
                                            this._controllerRT.text,
                                            this._controllerRW.text,
                                            this._controllerTelephone1Area.text,
                                            this._controllerTelephone1.text,
                                            this._controllerTelephone2Area.text,
                                            this._controllerTelephone2.text,
                                            this._controllerFaxArea.text,
                                            this._controllerFax.text,
                                            this._isSameWithIdentity,
                                            this._addressFromMap,
                                            this._isCorrespondence,
                                            this._active,
                                            this._isEditAddress,
                                            this._isAddressTypeDakor,
                                            this._isAddressDakor,
                                            this._isRTDakor,
                                            this._isRWDakor,
                                            this._isKelurahanDakor,
                                            this._isKecamatanDakor,
                                            this._isKabKotDakor,
                                            this._isProvinsiDakor,
                                            this._isKodePosDakor,
                                            this._isTelp1AreaDakor,
                                            this._isTelp1Dakor,
                                            this._isTelp2AreaDakor,
                                            this._isTelp2Dakor,
                                            this._isFaxAreaDakor,
                                            this._isFaxDakor,
                                            this._isDataLatLongDakor));

                                        Provider.of<FormMCompanyAlamatChangeNotifier>(context,
                                            listen: true)
                                            .addAlamatKorespondensi(AddressModelCompany(
                                            JenisAlamatModel("02", "KANTOR 1"),
                                            this._addressID,
                                            this._foreignBusinessID,
                                            null, //no id
                                            this._kelurahanSelected,
                                            this._controllerAlamat.text,
                                            this._controllerRT.text,
                                            this._controllerRW.text,
                                            this._controllerTelephone1Area.text,
                                            this._controllerTelephone1.text,
                                            this._controllerTelephone2Area.text,
                                            this._controllerTelephone2.text,
                                            this._controllerFaxArea.text,
                                            this._controllerFax.text,
                                            this._isSameWithIdentity,
                                            this._addressFromMap,
                                            this._isCorrespondence,
                                            this._active,
                                            this._isEditAddress,
                                            this._isAddressTypeDakor,
                                            this._isAddressDakor,
                                            this._isRTDakor,
                                            this._isRWDakor,
                                            this._isKelurahanDakor,
                                            this._isKecamatanDakor,
                                            this._isKabKotDakor,
                                            this._isProvinsiDakor,
                                            this._isKodePosDakor,
                                            this._isTelp1AreaDakor,
                                            this._isTelp1Dakor,
                                            this._isTelp2AreaDakor,
                                            this._isTelp2Dakor,
                                            this._isFaxAreaDakor,
                                            this._isFaxDakor,
                                            this._isDataLatLongDakor));
                                    }
                                    else if(typeAddress == 3){
                                        Provider.of<AddPemegangSahamLembagaChangeNotif>(context,
                                            listen: true)
                                            .addPemegangSahamKelembagaanAddress(AddressModelCompany(
                                            this._jenisAlamatSelected,
                                            this._addressID,
                                            this._foreignBusinessID,
                                            null, //no id
                                            this._kelurahanSelected,
                                            this._controllerAlamat.text,
                                            this._controllerRT.text,
                                            this._controllerRW.text,
                                            this._controllerTelephone1Area.text,
                                            this._controllerTelephone1.text,
                                            this._controllerTelephone2Area.text,
                                            this._controllerTelephone2.text,
                                            this._controllerFaxArea.text,
                                            this._controllerFax.text,
                                            this._isSameWithIdentity,
                                            this._addressFromMap,
                                            this._isCorrespondence,
                                            this._active,
                                            this._isEditAddress,
                                            this._isAddressTypeDakor,
                                            this._isAddressDakor,
                                            this._isRTDakor,
                                            this._isRWDakor,
                                            this._isKelurahanDakor,
                                            this._isKecamatanDakor,
                                            this._isKabKotDakor,
                                            this._isProvinsiDakor,
                                            this._isKodePosDakor,
                                            this._isTelp1AreaDakor,
                                            this._isTelp1Dakor,
                                            this._isTelp2AreaDakor,
                                            this._isTelp2Dakor,
                                            this._isFaxAreaDakor,
                                            this._isFaxDakor,
                                            this._isDataLatLongDakor));

                                        Provider.of<AddPemegangSahamLembagaChangeNotif>(context,
                                            listen: true)
                                            .addPemegangSahamKelembagaanAddress(AddressModelCompany(
                                            JenisAlamatModel("01", "DOMISILI"),
                                            this._addressID,
                                            this._foreignBusinessID,
                                            null, //no id
                                            this._kelurahanSelected,
                                            this._controllerAlamat.text,
                                            this._controllerRT.text,
                                            this._controllerRW.text,
                                            this._controllerTelephone1Area.text,
                                            this._controllerTelephone1.text,
                                            this._controllerTelephone2Area.text,
                                            this._controllerTelephone2.text,
                                            this._controllerFaxArea.text,
                                            this._controllerFax.text,
                                            this._isSameWithIdentity,
                                            this._addressFromMap,
                                            this._isCorrespondence,
                                            this._active,
                                            this._isEditAddress,
                                            this._isAddressTypeDakor,
                                            this._isAddressDakor,
                                            this._isRTDakor,
                                            this._isRWDakor,
                                            this._isKelurahanDakor,
                                            this._isKecamatanDakor,
                                            this._isKabKotDakor,
                                            this._isProvinsiDakor,
                                            this._isKodePosDakor,
                                            this._isTelp1AreaDakor,
                                            this._isTelp1Dakor,
                                            this._isTelp2AreaDakor,
                                            this._isTelp2Dakor,
                                            this._isFaxAreaDakor,
                                            this._isFaxDakor,
                                            this._isDataLatLongDakor));
                                    }
                                    Navigator.pop(context);
                                    Navigator.pop(context);
                                },
                                child: Text("Ya",
                                    style: TextStyle(
                                        color: primaryOrange,
                                        fontSize: 14,
                                        fontWeight: FontWeight.w500,
                                        letterSpacing: 1.25
                                    )
                                )
                            ),
                            FlatButton(
                                onPressed: (){
                                    //bikin identitas
                                    if(typeAddress == 1){
                                        Provider.of<FormMAddGuarantorCompanyChangeNotifier>(context,
                                            listen: true)
                                            .addGuarantorAddress(AddressModelCompany(
                                            this._jenisAlamatSelected,
                                            this._addressID,
                                            this._foreignBusinessID,
                                            null, //no id
                                            this._kelurahanSelected,
                                            this._controllerAlamat.text,
                                            this._controllerRT.text,
                                            this._controllerRW.text,
                                            this._controllerTelephone1Area.text,
                                            this._controllerTelephone1.text,
                                            this._controllerTelephone2Area.text,
                                            this._controllerTelephone2.text,
                                            this._controllerFaxArea.text,
                                            this._controllerFax.text,
                                            this._isSameWithIdentity,
                                            this._addressFromMap,
                                            this._isCorrespondence,
                                            this._active,
                                            this._isEditAddress,
                                            this._isAddressTypeDakor,
                                            this._isAddressDakor,
                                            this._isRTDakor,
                                            this._isRWDakor,
                                            this._isKelurahanDakor,
                                            this._isKecamatanDakor,
                                            this._isKabKotDakor,
                                            this._isProvinsiDakor,
                                            this._isKodePosDakor,
                                            this._isTelp1AreaDakor,
                                            this._isTelp1Dakor,
                                            this._isTelp2AreaDakor,
                                            this._isTelp2Dakor,
                                            this._isFaxAreaDakor,
                                            this._isFaxDakor,
                                            this._isDataLatLongDakor));
                                    }
                                    else if(typeAddress == 2){
                                        Provider.of<FormMCompanyAlamatChangeNotifier>(context,
                                            listen: true)
                                            .addAlamatKorespondensi(AddressModelCompany(
                                            this._jenisAlamatSelected,
                                            this._addressID,
                                            this._foreignBusinessID,
                                            null, //no id
                                            this._kelurahanSelected,
                                            this._controllerAlamat.text,
                                            this._controllerRT.text,
                                            this._controllerRW.text,
                                            this._controllerTelephone1Area.text,
                                            this._controllerTelephone1.text,
                                            this._controllerTelephone2Area.text,
                                            this._controllerTelephone2.text,
                                            this._controllerFaxArea.text,
                                            this._controllerFax.text,
                                            this._isSameWithIdentity,
                                            this._addressFromMap,
                                            this._isCorrespondence,
                                            this._active,
                                            this._isEditAddress,
                                            this._isAddressTypeDakor,
                                            this._isAddressDakor,
                                            this._isRTDakor,
                                            this._isRWDakor,
                                            this._isKelurahanDakor,
                                            this._isKecamatanDakor,
                                            this._isKabKotDakor,
                                            this._isProvinsiDakor,
                                            this._isKodePosDakor,
                                            this._isTelp1AreaDakor,
                                            this._isTelp1Dakor,
                                            this._isTelp2AreaDakor,
                                            this._isTelp2Dakor,
                                            this._isFaxAreaDakor,
                                            this._isFaxDakor,
                                            this._isDataLatLongDakor),);
                                    }
                                    else if(typeAddress == 3){
                                        Provider.of<AddPemegangSahamLembagaChangeNotif>(context,
                                            listen: true)
                                            .addPemegangSahamKelembagaanAddress(AddressModelCompany(
                                            this._jenisAlamatSelected,
                                            this._addressID,
                                            this._foreignBusinessID,
                                            null, //no id
                                            this._kelurahanSelected,
                                            this._controllerAlamat.text,
                                            this._controllerRT.text,
                                            this._controllerRW.text,
                                            this._controllerTelephone1Area.text,
                                            this._controllerTelephone1.text,
                                            this._controllerTelephone2Area.text,
                                            this._controllerTelephone2.text,
                                            this._controllerFaxArea.text,
                                            this._controllerFax.text,
                                            this._isSameWithIdentity,
                                            this._addressFromMap,
                                            this._isCorrespondence,
                                            this._active,
                                            this._isEditAddress,
                                            this._isAddressTypeDakor,
                                            this._isAddressDakor,
                                            this._isRTDakor,
                                            this._isRWDakor,
                                            this._isKelurahanDakor,
                                            this._isKecamatanDakor,
                                            this._isKabKotDakor,
                                            this._isProvinsiDakor,
                                            this._isKodePosDakor,
                                            this._isTelp1AreaDakor,
                                            this._isTelp1Dakor,
                                            this._isTelp2AreaDakor,
                                            this._isTelp2Dakor,
                                            this._isFaxAreaDakor,
                                            this._isFaxDakor,
                                            this._isDataLatLongDakor));
                                    }
                                    Navigator.pop(context);
                                    Navigator.pop(context);
                                },
                                child: Text("Tidak",
                                    style: TextStyle(
                                        color: Colors.grey,
                                        fontSize: 14,
                                        fontWeight: FontWeight.w500,
                                        letterSpacing: 1.25
                                    )
                                )
                            )
                        ],
                    ),
                );
            }
        );
    }

  void check(BuildContext context, int flag, int index, int typeAddress) {
//typeAddres 1 = list alamat guarantor addres individu
//typeAddres 2 = list alamat company
//typeAddres 3 = list saham lembaga
      final _form = _key.currentState;
      if (flag == 0) {
          if (_form.validate()) {
              print(typeAddress);
              if(typeAddress == 1){
                  print("list alamat company new");
                  if(jenisAlamatSelected.KODE == "03"){
                      this._isSameWithIdentity = true;
                      isShowDialog(context, flag, index, typeAddress);
                  } else {
                      Provider.of<FormMAddGuarantorCompanyChangeNotifier>(context,
                          listen: true).addGuarantorAddress(AddressModelCompany(
                          this._jenisAlamatSelected,
                          this._addressID,
                          this._foreignBusinessID,
                          null, //no id
                          this._kelurahanSelected,
                          this._controllerAlamat.text,
                          this._controllerRT.text,
                          this._controllerRW.text,
                          this._controllerTelephone1Area.text,
                          this._controllerTelephone1.text,
                          this._controllerTelephone2Area.text,
                          this._controllerTelephone2.text,
                          this._controllerFaxArea.text,
                          this._controllerFax.text,
                          this._isSameWithIdentity,
                          this._addressFromMap,
                          this._isCorrespondence,
                          this._active,
                          this._isEditAddress,
                          this._isAddressTypeDakor,
                          this._isAddressDakor,
                          this._isRTDakor,
                          this._isRWDakor,
                          this._isKelurahanDakor,
                          this._isKecamatanDakor,
                          this._isKabKotDakor,
                          this._isProvinsiDakor,
                          this._isKodePosDakor,
                          this._isTelp1AreaDakor,
                          this._isTelp1Dakor,
                          this._isTelp2AreaDakor,
                          this._isTelp2Dakor,
                          this._isFaxAreaDakor,
                          this._isFaxDakor,
                          this._isDataLatLongDakor));
                      Navigator.pop(context);
                  }
              }
              else if(typeAddress == 2){
                  print("list alamat company new2");
                  if(jenisAlamatSelected.KODE == "03"){
                      this._isSameWithIdentity = true;
                      isShowDialog(context, flag, index, typeAddress);
                  } else {
                      Provider.of<FormMCompanyAlamatChangeNotifier>(context,
                          listen: true)
                          .addAlamatKorespondensi(AddressModelCompany(
                          this._jenisAlamatSelected,
                          this._addressID,
                          this._foreignBusinessID,
                          null, //no id
                          this._kelurahanSelected,
                          this._controllerAlamat.text,
                          this._controllerRT.text,
                          this._controllerRW.text,
                          this._controllerTelephone1Area.text,
                          this._controllerTelephone1.text,
                          this._controllerTelephone2Area.text,
                          this._controllerTelephone2.text,
                          this._controllerFaxArea.text,
                          this._controllerFax.text,
                          this._isSameWithIdentity,
                          this._addressFromMap,
                          this._isCorrespondence,
                          this._active,
                          this._isEditAddress,
                          this._isAddressTypeDakor,
                          this._isAddressDakor,
                          this._isRTDakor,
                          this._isRWDakor,
                          this._isKelurahanDakor,
                          this._isKecamatanDakor,
                          this._isKabKotDakor,
                          this._isProvinsiDakor,
                          this._isKodePosDakor,
                          this._isTelp1AreaDakor,
                          this._isTelp1Dakor,
                          this._isTelp2AreaDakor,
                          this._isTelp2Dakor,
                          this._isFaxAreaDakor,
                          this._isFaxDakor,
                          this._isDataLatLongDakor));
                      Navigator.pop(context);
                  }
              }
              else if(typeAddress == 3){
                  print("list alamat company new2");
                  if(jenisAlamatSelected.KODE == "03"){
                      this._isSameWithIdentity = true;
                      isShowDialog(context, flag, index, typeAddress);
                  } else {
                      Provider.of<AddPemegangSahamLembagaChangeNotif>(context,
                          listen: true)
                          .addPemegangSahamKelembagaanAddress(AddressModelCompany(
                          this._jenisAlamatSelected,
                          this._addressID,
                          this._foreignBusinessID,
                          null, //no id
                          this._kelurahanSelected,
                          this._controllerAlamat.text,
                          this._controllerRT.text,
                          this._controllerRW.text,
                          this._controllerTelephone1Area.text,
                          this._controllerTelephone1.text,
                          this._controllerTelephone2Area.text,
                          this._controllerTelephone2.text,
                          this._controllerFaxArea.text,
                          this._controllerFax.text,
                          this._isSameWithIdentity,
                          this._addressFromMap,
                          this._isCorrespondence,
                          this._active,
                          this._isEditAddress,
                          this._isAddressTypeDakor,
                          this._isAddressDakor,
                          this._isRTDakor,
                          this._isRWDakor,
                          this._isKelurahanDakor,
                          this._isKecamatanDakor,
                          this._isKabKotDakor,
                          this._isProvinsiDakor,
                          this._isKodePosDakor,
                          this._isTelp1AreaDakor,
                          this._isTelp1Dakor,
                          this._isTelp2AreaDakor,
                          this._isTelp2Dakor,
                          this._isFaxAreaDakor,
                          this._isFaxDakor,
                          this._isDataLatLongDakor));
                      Navigator.pop(context);
                  }
              }
          } else {
              autoValidate = true;
          }
      }
      else {
          if (_form.validate()) {
              if(typeAddress == 1){
                  print("list alamat guarantor company edit");
                  Provider.of<FormMAddGuarantorCompanyChangeNotifier>(context,
                      listen: true)
                      .updateGuarantorAddress(
                      AddressModelCompany(
                          this._jenisAlamatSelected,
                          this._addressID,
                          this._foreignBusinessID,
                          null, //no id
                          this._kelurahanSelected,
                          this._controllerAlamat.text,
                          this._controllerRT.text,
                          this._controllerRW.text,
                          this._controllerTelephone1Area.text,
                          this._controllerTelephone1.text,
                          this._controllerTelephone2Area.text,
                          this._controllerTelephone2.text,
                          this._controllerFaxArea.text,
                          this._controllerFax.text,
                          this._isSameWithIdentity,
                          this._addressFromMap,
                          this._isCorrespondence,
                          this._active,
                          this._isEditAddress,
                          this._isAddressTypeDakor,
                          this._isAddressDakor,
                          this._isRTDakor,
                          this._isRWDakor,
                          this._isKelurahanDakor,
                          this._isKecamatanDakor,
                          this._isKabKotDakor,
                          this._isProvinsiDakor,
                          this._isKodePosDakor,
                          this._isTelp1AreaDakor,
                          this._isTelp1Dakor,
                          this._isTelp2AreaDakor,
                          this._isTelp2Dakor,
                          this._isFaxAreaDakor,
                          this._isFaxDakor,
                          this._isDataLatLongDakor),
                      index);
              }
              else if (typeAddress == 2){
                  print("list alamat company edit");
                  Provider.of<FormMCompanyAlamatChangeNotifier>(context,
                      listen: true)
                      .updateAlamatKorespondensi(
                      AddressModelCompany(
                          this._jenisAlamatSelected,
                          this._addressID,
                          this._foreignBusinessID,
                          null, //no id
                          this._kelurahanSelected,
                          this._controllerAlamat.text,
                          this._controllerRT.text,
                          this._controllerRW.text,
                          this._controllerTelephone1Area.text,
                          this._controllerTelephone1.text,
                          this._controllerTelephone2Area.text,
                          this._controllerTelephone2.text,
                          this._controllerFaxArea.text,
                          this._controllerFax.text,
                          this._isSameWithIdentity,
                          this._addressFromMap,
                          this._isCorrespondence,
                          this._active,
                          this._isEditAddress,
                          this._isAddressTypeDakor,
                          this._isAddressDakor,
                          this._isRTDakor,
                          this._isRWDakor,
                          this._isKelurahanDakor,
                          this._isKecamatanDakor,
                          this._isKabKotDakor,
                          this._isProvinsiDakor,
                          this._isKodePosDakor,
                          this._isTelp1AreaDakor,
                          this._isTelp1Dakor,
                          this._isTelp2AreaDakor,
                          this._isTelp2Dakor,
                          this._isFaxAreaDakor,
                          this._isFaxDakor,
                          this._isDataLatLongDakor),
                      index);
              }
              else if (typeAddress == 3){
                  print("list alamat company edit");
                  Provider.of<AddPemegangSahamLembagaChangeNotif>(context,
                      listen: true)
                      .updatePemegangSahamKelembagaanAddress(
                      AddressModelCompany(
                          this._jenisAlamatSelected,
                          this._addressID,
                          this._foreignBusinessID,
                          null, //no id
                          this._kelurahanSelected,
                          this._controllerAlamat.text,
                          this._controllerRT.text,
                          this._controllerRW.text,
                          this._controllerTelephone1Area.text,
                          this._controllerTelephone1.text,
                          this._controllerTelephone2Area.text,
                          this._controllerTelephone2.text,
                          this._controllerFaxArea.text,
                          this._controllerFax.text,
                          this._isSameWithIdentity,
                          this._addressFromMap,
                          this._isCorrespondence,
                          this._active,
                          this._isEditAddress,
                          this._isAddressTypeDakor,
                          this._isAddressDakor,
                          this._isRTDakor,
                          this._isRWDakor,
                          this._isKelurahanDakor,
                          this._isKecamatanDakor,
                          this._isKabKotDakor,
                          this._isProvinsiDakor,
                          this._isKodePosDakor,
                          this._isTelp1AreaDakor,
                          this._isTelp1Dakor,
                          this._isTelp2AreaDakor,
                          this._isTelp2Dakor,
                          this._isFaxAreaDakor,
                          this._isFaxDakor,
                          this._isDataLatLongDakor),
                      index);
              }
              Navigator.pop(context);
          } else {
              autoValidate = true;
          }
      }
  }

//  void setValueIsSameWithIdentity(bool value, BuildContext context, AddressGuarantorModelCompany model) {
//      var _provider = Provider.of<FormMAddGuarantorCompanyChangeNotifier>(context,
//          listen: false);
//      AddressGuarantorModelCompany data;
//      if (_addressModelTemp == null) {
//          if (value) {
//              for (int i = 0; i < _provider.listGuarantorAddress.length; i++) {
//                  if (_provider.listGuarantorAddress[i].jenisAlamatModel.KODE == "01") {
//                      data = _provider.listGuarantorAddress[i];
//                  }
//              }
//              if (model != null) {
//                  for (int i = 0; i < _listJenisAlamat.length; i++) {
//                      if (model.jenisAlamatModel.KODE == _listJenisAlamat[i].KODE) {
//                          this._jenisAlamatSelected = _listJenisAlamat[i];
//                          this._jenisAlamatSelectedTemp = this._jenisAlamatSelected;
//                          this._isSameWithIdentity = value;
//                      }
//                  }
//              }
//              this._controllerAlamat.text = data.address;
//              this._controllerRT.text = data.rt;
//              this._controllerRW.text = data.rw;
//              this._kelurahanSelected = data.kelurahanModel;
//              this._controllerAlamat.text = data.address;
//              this._alamatTemp = this._controllerAlamat.text;
//              this._controllerRT.text = data.rt;
//              this._rtTemp = this._controllerRT.text;
//              this._controllerRW.text = data.rw;
//              this._rwTemp = this._controllerRW.text;
//              this._controllerKelurahan.text = data.kelurahanModel.KEL_NAME;
//              this._kelurahanTemp = this._controllerKelurahan.text;
//              this._controllerKecamatan.text = data.kelurahanModel.KEC_NAME;
//              this._kecamatanTemp = this._controllerKecamatan.text;
//              this._controllerKota.text = data.kelurahanModel.KABKOT_NAME;
//              this._kotaTemp = this._controllerKota.text;
//              this._controllerProvinsi.text = data.kelurahanModel.PROV_NAME;
//              this._provinsiTemp = this._controllerProvinsi.text;
//
//              this._controllerTelephone1Area.text = data.phoneArea1;
//              this._phone1AreaTemp = this._controllerTelephone1Area.text;
//
//              this._controllerTelephone1.text = data.phone1;
//              this._phone1Temp = this._controllerTelephone1.text;
//
//              this._controllerTelephone2Area.text = data.phoneArea2;
//              this._phone2AreaTemp = this._controllerTelephone2Area.text;
//
//              this._controllerTelephone2.text = data.phone2;
//              this._phone2Temp = this._controllerTelephone2.text;
//
//              this._controllerFaxArea.text = data.faxArea;
//              this._faxAreaTemp = this._controllerFaxArea.text;
//
//              this._controllerFax.text = data.fax;
//              this._faxTemp = this._controllerFax.text;
//
//              this._controllerPostalCode.text = data.kelurahanModel.ZIPCODE;
//              this._postalCodeTemp = this._controllerPostalCode.text;
//              enableTfAddress = !value;
//              enableTfRT = !value;
//              enableTfRW = !value;
//              enableTfKelurahan = !value;
//              enableTfKecamatan = !value;
//              enableTfKota = !value;
//              enableTfProv = !value;
//              enableTfPostalCode = !value;
//              enableTfTelephone1Area = !value;
//              enableTfPhone1 = !value;
//              enableTfTelephone2Area = !value;
//              enableTfPhone2 = !value;
//              enableTfFaxArea = !value;
//              enableTfFax = !value;
//          } else {
//              this._controllerAlamat.clear();
//              this._controllerRT.clear();
//              this._controllerRW.clear();
//              this._kelurahanSelected = null;
//              this._controllerAlamat.clear();
//        this._alamatTemp = "";
//              this._controllerRT.clear();
//        this._rtTemp = "";
//              this._controllerRW.clear();
//        this._rwTemp = "";
//              this._controllerKelurahan.clear();
//        this._kelurahanTemp = "";
//              this._controllerKecamatan.clear();
//        this._kecamatanTemp = "";
//              this._controllerKota.clear();
//        this._kotaTemp = "";
//              this._controllerProvinsi.clear();
//        this._provinsiTemp = "";
//              this._controllerTelephone1Area.clear();
//        this._areaCodeTemp = "";
//              this._controllerTelephone1.clear();
//        this._phoneTemp = "";
//              this._controllerPostalCode.clear();
//        this._postalCodeTemp = "";
//              this._controllerTelephone1Area.clear();
//              this._controllerTelephone1.clear();
//
//              this._controllerTelephone2Area.clear();
//              this._controllerTelephone2.clear();
//
//              this._controllerFaxArea.clear();
//              this._controllerFax.clear();
//
//              enableTfAddress = !value;
//              enableTfRT = !value;
//              enableTfRW = !value;
//              enableTfKelurahan = !value;
//              enableTfKecamatan = !value;
//              enableTfKota = !value;
//              enableTfProv = !value;
//              enableTfPostalCode = !value;
//              enableTfTelephone1Area = !value;
//              enableTfPhone1 = !value;
//              enableTfTelephone2Area = !value;
//              enableTfPhone2 = !value;
//              enableTfFaxArea = !value;
//              enableTfFax = !value;
//          }
//      } else {
//          if (value) {
//              for (int i = 0; i < _provider.listGuarantorAddress.length; i++) {
//                  if (_provider.listGuarantorAddress[i].jenisAlamatModel.KODE == "01") {
//                      data = _provider.listGuarantorAddress[i];
//                  }
//              }
//              this._controllerAlamat.text = data.address;
//              this._controllerRT.text = data.rt;
//              this._controllerRW.text = data.rw;
//              this._kelurahanSelected = data.kelurahanModel;
//              this._controllerAlamat.text = data.address;
//      this._alamatTemp = this._controllerAlamat.text;
//              this._controllerRT.text = data.rt;
//      this._rtTemp = this._controllerRT.text;
//              this._controllerRW.text = data.rw;
//      this._rwTemp = this._controllerRW.text;
//              this._controllerKelurahan.text = data.kelurahanModel.KEL_NAME;
//      this._kelurahanTemp = this._controllerKelurahan.text;
//              this._controllerKecamatan.text = data.kelurahanModel.KEC_NAME;
//      this._kecamatanTemp = this._controllerKecamatan.text;
//              this._controllerKota.text = data.kelurahanModel.KABKOT_NAME;
//      this._kotaTemp = this._controllerKota.text;
//              this._controllerProvinsi.text = data.kelurahanModel.PROV_NAME;
//      this._provinsiTemp = this._controllerProvinsi.text;
//              this._controllerTelephone1Area.text = data.phoneArea1;
//      this._areaCodeTemp = this._controllerKodeArea.text;
//              this._controllerTelephone1.text = data.phone1;
//      this._phoneTemp = this._controllerTlpn.text;
//
//              this._controllerTelephone2Area.text = data.phoneArea2;
//              this._controllerTelephone2.text = data.phone2;
//
//              this._controllerFaxArea.text = data.faxArea;
//              this._controllerFax.text = data.fax;
//
//              this._controllerPostalCode.text = data.kelurahanModel.ZIPCODE;
//      this._postalCodeTemp = this._controllerPostalCode.text;
//              enableTfAddress = !value;
//              enableTfRT = !value;
//              enableTfRW = !value;
//              enableTfKelurahan = !value;
//              enableTfKecamatan = !value;
//              enableTfKota = !value;
//              enableTfProv = !value;
//              enableTfPostalCode = !value;
//              enableTfTelephone1Area = !value;
//              enableTfPhone1 = !value;
//              enableTfTelephone2Area = !value;
//              enableTfPhone2 = !value;
//              enableTfFaxArea = !value;
//              enableTfFax = !value;
//          } else {
//              this._controllerAlamat.clear();
//              this._controllerRT.clear();
//              this._controllerRW.clear();
//              this._kelurahanSelected = null;
//              this._controllerAlamat.clear();
//        this._alamatTemp = "";
//              this._controllerRT.clear();
//        this._rtTemp = "";
//              this._controllerRW.clear();
//        this._rwTemp = "";
//              this._controllerKelurahan.clear();
//        this._kelurahanTemp = "";
//              this._controllerKecamatan.clear();
//        this._kecamatanTemp = "";
//              this._controllerKota.clear();
//        this._kotaTemp = "";
//              this._controllerProvinsi.clear();
//        this._provinsiTemp = "";
//              this._controllerTelephone1Area.clear();
//        this._areaCodeTemp = "";
//              this._controllerTelephone1.clear();
//        this._phoneTemp = "";
//              this._controllerPostalCode.clear();
//        this._postalCodeTemp = "";
//              this._controllerTelephone1Area.clear();
//              this._controllerTelephone1.clear();
//
//              this._controllerTelephone2Area.clear();
//              this._controllerTelephone2.clear();
//
//              this._controllerFaxArea.clear();
//              this._controllerFax.clear();
//
//              enableTfAddress = !value;
//              enableTfRT = !value;
//              enableTfRW = !value;
//              enableTfKelurahan = !value;
//              enableTfKecamatan = !value;
//              enableTfKota = !value;
//              enableTfProv = !value;
//              enableTfPostalCode = !value;
//              enableTfTelephone1Area = !value;
//              enableTfPhone1 = !value;
//              enableTfTelephone2Area = !value;
//              enableTfPhone2 = !value;
//              enableTfFaxArea = !value;
//              enableTfFax = !value;
//          }
//      }
//  }

  setValueForEdit(AddressModelCompany data, BuildContext context, int index, int typeAddress){
//      addDataListAddressType(context, index);
//      if (isSameWithIdentity) {
//          setValueIsSameWithIdentity(isSameWithIdentity, context, data);
//      } else {
//          _addressModelTemp = data;
          for (int i = 0; i < this._listJenisAlamat.length; i++) {
              if (data.jenisAlamatModel.KODE == this._listJenisAlamat[i].KODE) {
                  this._jenisAlamatSelected = this._listJenisAlamat[i];
                  this._jenisAlamatSelectedTemp = this._listJenisAlamat[i];
              }
          }
          this._addressID = data.addressID;
          this._foreignBusinessID = data.foreignBusinessID;
          this._kelurahanSelected = data.kelurahanModel;
          this._controllerAlamat.text = data.address;
          this._alamatTemp = this._controllerAlamat.text;
          this._controllerRT.text = data.rt;
          this._rtTemp = this._controllerRT.text;
          this._controllerRW.text = data.rw;
          this._rwTemp = this._controllerRW.text;
          this._controllerKelurahan.text = data.kelurahanModel.KEL_ID + " - " + data.kelurahanModel.KEL_NAME;
          this._kelurahanTemp = this._controllerKelurahan.text;
          this._controllerKecamatan.text = data.kelurahanModel.KEC_ID + " - " + data.kelurahanModel.KEC_NAME;
          this._kecamatanTemp = this._controllerKecamatan.text;
          this._controllerKota.text = data.kelurahanModel.KABKOT_ID + " - " + data.kelurahanModel.KABKOT_NAME;
          this._kotaTemp = this._controllerKota.text;
          this._controllerProvinsi.text = data.kelurahanModel.PROV_ID + " - " + data.kelurahanModel.PROV_NAME;
          this._provinsiTemp = this._controllerProvinsi.text;
          this._controllerTelephone1Area.text = data.phoneArea1;
          this._phone1AreaTemp = this._controllerTelephone1Area.text;
          this._controllerTelephone1.text = data.phone1;
          this._phone1Temp = this._controllerTelephone1.text;
          this._controllerTelephone2Area.text = data.phoneArea2;
          this._phone2AreaTemp = this._controllerTelephone2Area.text;
          this._controllerTelephone2.text = data.phone2;
          this._phone2Temp = this._controllerTelephone2.text;
          this._controllerFaxArea.text = data.faxArea;
          this._faxAreaTemp = this._controllerFaxArea.text;
          this._controllerFax.text = data.fax;
          this._faxTemp = this._controllerFax.text;
          this._controllerPostalCode.text = data.kelurahanModel.ZIPCODE;
          this._postalCodeTemp = this._controllerPostalCode.text;
          if(data.addressLatLong != null) this._controllerAddressFromMap.text = this._addressFromMap['address'];
          this._addressFromMap = data.addressLatLong;
//      }
      checkDataDakor(data, context, index, typeAddress);
  }

  void checkValidCodeArea1(String value) {
      if (value == "08") {
          Future.delayed(Duration(milliseconds: 100), () {
              this._controllerTelephone1Area.clear();
          });
      } else {
          return;
      }
  }

  void checkValidCodeArea2(String value) {
      if (value == "08") {
          Future.delayed(Duration(milliseconds: 100), () {
              this._controllerTelephone2Area.clear();
          });
      } else {
          return;
      }
  }

  void checkValidCodeAreaFax(String value) {
      if (value == "08") {
          Future.delayed(Duration(milliseconds: 100), () {
              this._controllerFaxArea.clear();
          });
      } else {
          return;
      }
  }

  void searchKelurahan(BuildContext context) async {
      KelurahanModel data = await Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => ChangeNotifierProvider(
                  create: (context) => SearchKelurahanChangeNotif(),
                  child: SearchKelurahan())));
      if (data != null) {
          print("test data $data");
          kelurahanSelected = data;
          this._controllerKelurahan.text = data.KEL_ID + " - " + data.KEL_NAME;
          this._controllerKecamatan.text = data.KEC_ID + " - " + data.KEC_NAME;
          this._controllerKota.text = data.KABKOT_ID + " - " + data.KABKOT_NAME;
          this._controllerProvinsi.text = data.PROV_ID + " - " + data.PROV_NAME;
          this._controllerPostalCode.text = data.ZIPCODE;
          notifyListeners();
      } else {
          return;
      }
  }

  void setLocationAddressByMap(BuildContext context) async{
      Map _result = await Navigator.push(context, MaterialPageRoute(builder: (context) => MapPage()));
      if(_result != null){
          _addressFromMap = _result;
          this._controllerAddressFromMap.text = _result["address"];
          notifyListeners();
      }
  }

  get phone2Temp => _phone2Temp;

  get faxAreaTemp => _faxAreaTemp;

  get faxTemp => _faxTemp;

  get postalCodeTemp => _postalCodeTemp;

  get phone1AreaTemp => _phone1AreaTemp;

  get provinsiTemp => _provinsiTemp;

  get kotaTemp => _kotaTemp;

  get kecamatanTemp => _kecamatanTemp;

  get kelurahanTemp => _kelurahanTemp;

  get rwTemp => _rwTemp;

  get rtTemp => _rtTemp;

  get phone1Temp => _phone1Temp;

  get phone2AreaTemp => _phone2AreaTemp;

  String get alamatTemp => _alamatTemp;

  JenisAlamatModel get jenisAlamatSelectedTemp => _jenisAlamatSelectedTemp;

  KelurahanModel get kelurahanSelectedTemp => _kelurahanSelectedTemp;

  GlobalKey<FormState> get key => _key;

  void clearData(){
      this._autoValidate = false;
      this._jenisAlamatSelected = null;
      this._controllerAlamat.clear();
      this._controllerRT.clear();
      this._controllerRW.clear();
      this._kelurahanSelected = null;
      this._controllerKelurahan.clear();
      this._controllerKecamatan.clear();
      this._controllerKota.clear();
      this._controllerProvinsi.clear();
      this._controllerPostalCode.clear();
      this._controllerTelephone1Area.clear();
      this._controllerTelephone1.clear();
      this._controllerTelephone2Area.clear();
      this._controllerTelephone2.clear();
      this._controllerFaxArea.clear();
      this._controllerFax.clear();
      this._controllerAddressFromMap.clear();
      this._isEditAddress = false;
      this._isAddressTypeDakor = false;
      this._isAddressDakor = false;
      this._isRTDakor = false;
      this._isRWDakor = false;
      this._isKelurahanDakor = false;
      this._isKecamatanDakor = false;
      this._isKabKotDakor = false;
      this._isProvinsiDakor = false;
      this._isKodePosDakor = false;
      this._isTelp1AreaDakor = false;
      this._isTelp1Dakor = false;
      this._isTelp2AreaDakor = false;
      this._isTelp2Dakor = false;
      this._isFaxAreaDakor = false;
      this._isFaxDakor = false;
      this._isDataLatLongDakor = false;
  }

  get custType => _custType;

  set custType(value) {
      this._custType = value;
  }

  void checkDataDakor(AddressModelCompany data, BuildContext context, int index, int typeAddress) async{
    //typeAddres 1 = list alamat guarantor addres individu 2-8
    //typeAddres 2 = list alamat company 10
    //typeAddres 3 = list saham lembaga
      SharedPreferences _preferences = await SharedPreferences.getInstance();
      _custType = _preferences.getString("cust_type");
      if(_preferences.getString("last_known_state") == "DKR"){
          var _check = await _dbHelper.selectMS2CustAddr(typeAddress == 1 ? _custType == "PER" ? "2" : "8" : typeAddress == 2 ? "10" : "12");
          if(_check.isNotEmpty){
              if(this._jenisAlamatSelected != null){
                  _isAddressTypeDakor = this._jenisAlamatSelected.DESKRIPSI != _check[index]['addr_desc'] || _check[index]['edit_addr_type'] == "1";
              }
              _isAddressDakor = this._controllerAlamat.text != _check[index]['address'] || _check[index]['edit_address'] == "1";
              _isRTDakor = this._controllerRT.text != _check[index]['rt'] || _check[index]['edit_rt'] == "1";
              _isRWDakor = this._controllerRW.text != _check[index]['rw'] || _check[index]['edit_rw'] == "1";
              _isKelurahanDakor = this._controllerKelurahan.text != _check[index]['kelurahan_desc'] || _check[index]['edit_kelurahan'] == "1";
              _isKecamatanDakor = this._controllerKecamatan.text != _check[index]['kecamatan_desc'] || _check[index]['edit_kecamatan'] == "1";
              _isKabKotDakor = this._controllerKota.text != _check[index]['kabkot_desc'] || _check[index]['edit_kabkot'] == "1";
              _isProvinsiDakor = this._controllerProvinsi.text != _check[index]['provinsi_desc'] || _check[index]['edit_provinsi'] == "1";
              _isKodePosDakor = this._controllerPostalCode.text != _check[index]['zip_code'] || _check[index]['edit_zip_code'] == "1";
              _isTelp1AreaDakor = this._controllerTelephone1Area.text != _check[index]['phone1_area'] || _check[index]['edit_phone1_area'] == "1";
              _isTelp1Dakor = this._controllerTelephone1.text != _check[index]['phone1'] || _check[index]['edit_phone1'] == "1";
              _isTelp2AreaDakor = this._controllerTelephone2Area.text != _check[index]['phone_2_area'] || _check[index]['edit_phone_2_area'] == "1";
              _isTelp2Dakor = this._controllerTelephone2.text != _check[index]['phone_2'] || _check[index]['edit_phone_2'] == "1";
              _isFaxAreaDakor = this._controllerFaxArea.text != _check[index]['fax_area'] || _check[index]['edit_fax_area'] == "1";
              _isFaxDakor = this._controllerFax.text != _check[index]['fax'] || _check[index]['edit_fax'] == "1";
              _isDataLatLongDakor = this._controllerAddressFromMap.text != _check[index]['address_from_map'] || _check[index]['edit_address_from_map'] == "1";

              print(_isAddressDakor);
              print("ha;ads $_isRTDakor");
              print(this._controllerRW.text);
              print(_check[index]['rw']);
              // if(this._jenisAlamatSelected != null){
              //     _isAddressTypeDakor = this._jenisAlamatSelected.DESKRIPSI != data.jenisAlamatModel.DESKRIPSI || data.isAddressTypeDakor;
              // }
              // _isAddressDakor = this._controllerAlamat.text != data.address || data.isAddressDakor;
              // _isRTDakor = this._controllerRT.text != data.rt || data.isRTDakor;
              // _isRWDakor = this._controllerRW.text != data.rw || data.isRWDakor;
              // _isKelurahanDakor = this._controllerKelurahan.text != data.kelurahanModel.KEL_NAME || data.isKelurahanDakor;
              // _isKecamatanDakor = this._controllerKecamatan.text != data.kelurahanModel.KEC_NAME || data.isKecamatanDakor;
              // _isKabKotDakor = this._controllerKota.text != data.kelurahanModel.KABKOT_NAME || data.isKabKotDakor;
              // _isProvinsiDakor = this._controllerProvinsi.text != data.kelurahanModel.PROV_NAME || data.isProvinsiDakor;
              // _isKodePosDakor = this._controllerPostalCode.text != data.kelurahanModel.ZIPCODE || data.isKodePosDakor;
              // _isTelp1AreaDakor = this._controllerTelephone1Area.text != data.phoneArea1 || data.isTelp1AreaDakor;
              // _isTelp1Dakor = this._controllerTelephone1.text != data.phone1 || data.isTelp1Dakor;
              // _isTelp2AreaDakor = this._controllerTelephone2Area.text != data.phoneArea2 || data.isTelp2AreaDakor;
              // _isTelp2Dakor = this._controllerTelephone2.text != data.phone2 || data.isTelp2Dakor;
              // _isFaxAreaDakor = this._controllerFaxArea.text != data.faxArea || data.isFaxAreaDakor;
              // _isFaxDakor = this._controllerFax.text != data.fax || data.isFaxDakor;
              // _isDataLatLongDakor = this._controllerAddressFromMap.text != data.addressLatLong['address_from_map'] || data.isDataLatLongDakor;

              if(_isAddressTypeDakor || _isAddressDakor || _isRTDakor || _isRWDakor || _isKelurahanDakor ||
                  _isKecamatanDakor || _isKabKotDakor || _isProvinsiDakor || _isKodePosDakor ||
                  _isTelp1AreaDakor || _isTelp1Dakor || _isTelp2AreaDakor || _isTelp2Dakor ||
                  _isFaxAreaDakor || _isFaxDakor){
                  this._isEditAddress = true;
              }
              else {
                  this._isEditAddress = false;
              }
          }
          notifyListeners();
      }
  }

  void getDataFromDashboard(BuildContext context) async{
      SharedPreferences _preferences = await SharedPreferences.getInstance();
      _custType = _preferences.getString("cust_type");
      _setAddressCompanyGuarantorIndividuModel = Provider.of<DashboardChangeNotif>(context, listen: false).setAddressCompanyGuarantorIndividuModel;
      _setAddressCompanyGuarantorModel = Provider.of<DashboardChangeNotif>(context, listen: false).setAddressCompanyGuarantorModel;
      _setAddressCompanyModel = Provider.of<DashboardChangeNotif>(context, listen: false).setAddressCompanyModel;
      _setAddressCompanySahamModel = Provider.of<DashboardChangeNotif>(context, listen: false).setAddressSahamLembagaCompanyModel;
  }

  //fungsi show hide form
  bool setFaxShow(int typeAddress){
      bool status = false;
      if(typeAddress == 1){
          status = custType == "PER" ? _setAddressCompanyGuarantorIndividuModel.isFaxShow : _setAddressCompanyGuarantorModel.isFaxShow;
      }
      else if(typeAddress == 2){
          status = _setAddressCompanyModel.isFaxShow;
      }
      else if(typeAddress == 3){
          status = _setAddressCompanySahamModel.isFaxShow;
      }
      return status;
  }

  bool setFaxAreaShow(int typeAddress){
      bool status = false;
      if(typeAddress == 1){
          status = custType == "PER" ? _setAddressCompanyGuarantorIndividuModel.isFaxAreaShow : _setAddressCompanyGuarantorModel.isFaxAreaShow;
      }
      else if(typeAddress == 2){
          status = _setAddressCompanyModel.isFaxAreaShow;
      }
      else if(typeAddress == 3){
          status = _setAddressCompanySahamModel.isFaxAreaShow;
      }
      return status;
  }

  bool setTelp2Show(int typeAddress){
      bool status = false;
      if(typeAddress == 1){
          status = custType == "PER" ? _setAddressCompanyGuarantorIndividuModel.isTelp2Show : _setAddressCompanyGuarantorModel.isTelp2Show;
      }
      else if(typeAddress == 2){
          status = _setAddressCompanyModel.isTelp2Show;
      }
      else if(typeAddress == 3){
          status = _setAddressCompanySahamModel.isTelp2Show;
      }
      return status;
  }

  bool setTelp2AreaShow(int typeAddress){
      bool status = false;
      if(typeAddress == 1){
          status = custType == "PER" ? _setAddressCompanyGuarantorIndividuModel.isTelp2AreaShow : _setAddressCompanyGuarantorModel.isTelp2AreaShow;
      }
      else if(typeAddress == 2){
          status = _setAddressCompanyModel.isTelp2AreaShow;
      }
      else if(typeAddress == 3){
          status = _setAddressCompanySahamModel.isTelp2AreaShow;
      }
      return status;
  }

  bool setTelp1Show(int typeAddress){
      bool status = false;
      if(typeAddress == 1){
          status = custType == "PER" ? _setAddressCompanyGuarantorIndividuModel.isTelp1Show : _setAddressCompanyGuarantorModel.isTelp1Show;
      }
      else if(typeAddress == 2){
          status = _setAddressCompanyModel.isTelp1Show;
      }
      else if(typeAddress == 3){
          status = _setAddressCompanySahamModel.isTelp1Show;
      }
      return status;
  }

  bool setTelp1AreaShow(int typeAddress){
      bool status = false;
      if(typeAddress == 1){
          status = custType == "PER" ? _setAddressCompanyGuarantorIndividuModel.isTelp1AreaShow : _setAddressCompanyGuarantorModel.isTelp1AreaShow;
      }
      else if(typeAddress == 2){
          status = _setAddressCompanyModel.isTelp1AreaShow;
      }
      else if(typeAddress == 3){
          status = _setAddressCompanySahamModel.isTelp1AreaShow;
      }
      return status;
  }

  bool setKodePosShow(int typeAddress){
      bool status = false;
      if(typeAddress == 1){
          status = custType == "PER" ? _setAddressCompanyGuarantorIndividuModel.isKodePosShow :  _setAddressCompanyGuarantorModel.isKodePosShow;
      }
      else if(typeAddress == 2){
          status = _setAddressCompanyModel.isKodePosShow;
      }
      else if(typeAddress == 3){
          status = _setAddressCompanySahamModel.isKodePosShow;
      }
      return status;
  }

  bool setProvinsiShow(int typeAddress){
      bool status = false;
      if(typeAddress == 1){
          status = custType == "PER" ? _setAddressCompanyGuarantorIndividuModel.isProvinsiShow : _setAddressCompanyGuarantorModel.isProvinsiShow;
      }
      else if(typeAddress == 2){
          status = _setAddressCompanyModel.isProvinsiShow;
      }
      else if(typeAddress == 3){
          status = _setAddressCompanySahamModel.isProvinsiShow;
      }
      return status;
  }

  bool setKabKotShow(int typeAddress){
      bool status = false;
      if(typeAddress == 1){
          status = custType == "PER" ? _setAddressCompanyGuarantorIndividuModel.isKabKotShow : _setAddressCompanyGuarantorModel.isKabKotShow;
      }
      else if(typeAddress == 2){
          status = _setAddressCompanyModel.isKabKotShow;
      }
      else if(typeAddress == 3){
          status = _setAddressCompanySahamModel.isKabKotShow;
      }
      return status;
  }

  bool setKecamatanShow(int typeAddress){
      bool status = false;
      if(typeAddress == 1){
          status = custType == "PER" ? _setAddressCompanyGuarantorIndividuModel.isKecamatanShow : _setAddressCompanyGuarantorModel.isKecamatanShow;
      }
      else if(typeAddress == 2){
          status = _setAddressCompanyModel.isKecamatanShow;
      }
      else if(typeAddress == 3){
          status = _setAddressCompanySahamModel.isKecamatanShow;
      }
      return status;
  }

  bool setKelurahanShow(int typeAddress){
      bool status = false;
      if(typeAddress == 1){
          status = custType == "PER" ? _setAddressCompanyGuarantorIndividuModel.isKelurahanShow : _setAddressCompanyGuarantorModel.isKelurahanShow;
      }
      else if(typeAddress == 2){
          status = _setAddressCompanyModel.isKelurahanShow;
      }
      else if(typeAddress == 3){
          status = _setAddressCompanySahamModel.isKelurahanShow;
      }
      return status;
  }

  bool setRWShow(int typeAddress){
      bool status = false;
      if(typeAddress == 1){
          status = custType == "PER" ? _setAddressCompanyGuarantorIndividuModel.isRWShow : _setAddressCompanyGuarantorModel.isRWShow;
      }
      else if(typeAddress == 2){
          status = _setAddressCompanyModel.isRWShow;
      }
      else if(typeAddress == 3){
          status = _setAddressCompanySahamModel.isRWShow;
      }
      return status;
  }

  bool setRTShow(int typeAddress){
      bool status = false;
      if(typeAddress == 1){
          status = custType == "PER" ? _setAddressCompanyGuarantorIndividuModel.isRTShow : _setAddressCompanyGuarantorModel.isRTShow;
      }
      else if(typeAddress == 2){
          status = _setAddressCompanyModel.isRTShow;
      }
      else if(typeAddress == 3){
          status = _setAddressCompanySahamModel.isRTShow;
      }
      return status;
  }

  bool setAddressShow(int typeAddress){
      bool status = false;
      if(typeAddress == 1){
          status = custType == "PER" ? _setAddressCompanyGuarantorIndividuModel.isAddressShow : _setAddressCompanyGuarantorModel.isAddressShow;
      }
      else if(typeAddress == 2){
          status = _setAddressCompanyModel.isAddressShow;
      }
      else if(typeAddress == 3){
          status = _setAddressCompanySahamModel.isAddressShow;
      }
      return status;
  }

  bool setAddressTypeShow(int typeAddress){
      bool status = false;
      if(typeAddress == 1){
          status = custType == "PER" ? _setAddressCompanyGuarantorIndividuModel.isAddressTypeShow : _setAddressCompanyGuarantorModel.isAddressTypeShow;
      }
      else if(typeAddress == 2){
          status = _setAddressCompanyModel.isAddressTypeShow;
      }
      else if(typeAddress == 3){
          status = _setAddressCompanySahamModel.isAddressTypeShow;
      }
      return status;
  }


  //fungsi set data mandatory
  bool setFaxMandatory(int typeAddress){
      bool status = false;
      if(typeAddress == 1){
          status = custType == "PER" ? _setAddressCompanyGuarantorIndividuModel.isFaxMandatory : _setAddressCompanyGuarantorModel.isFaxMandatory;
      }
      else if(typeAddress == 2){
          status = _setAddressCompanyModel.isFaxMandatory;
      }
      else if(typeAddress == 3){
          status = _setAddressCompanySahamModel.isFaxMandatory;
      }
      return status;
  }

  bool setFaxAreaMandatory(int typeAddress){
      bool status = false;
      if(typeAddress == 1){
          status = custType == "PER" ? _setAddressCompanyGuarantorIndividuModel.isFaxAreaMandatory : _setAddressCompanyGuarantorModel.isFaxAreaMandatory;
      }
      else if(typeAddress == 2){
          status = _setAddressCompanyModel.isFaxAreaMandatory;
      }
      else if(typeAddress == 3){
          status = _setAddressCompanySahamModel.isFaxAreaMandatory;
      }
      return status;
  }

  bool setTelp2Mandatory(int typeAddress){
      bool status = false;
      if(typeAddress == 1){
          status = custType == "PER" ? _setAddressCompanyGuarantorIndividuModel.isTelp2Mandatory : _setAddressCompanyGuarantorModel.isTelp2Mandatory;
      }
      else if(typeAddress == 2){
          status = _setAddressCompanyModel.isTelp2Mandatory;
      }
      else if(typeAddress == 3){
          status = _setAddressCompanySahamModel.isTelp2Mandatory;
      }
      return status;
  }

  bool setTelp2AreaMandatory(int typeAddress){
      bool status = false;
      if(typeAddress == 1){
          status = custType == "PER" ? _setAddressCompanyGuarantorIndividuModel.isTelp2AreaMandatory : _setAddressCompanyGuarantorModel.isTelp2AreaMandatory;
      }
      else if(typeAddress == 2){
          status = _setAddressCompanyModel.isTelp2AreaMandatory;
      }
      else if(typeAddress == 3){
          status = _setAddressCompanySahamModel.isTelp2AreaMandatory;
      }
      return status;
  }

  bool setTelp1Mandatory(int typeAddress){
      bool status = false;
      if(typeAddress == 1){
          status = custType == "PER" ? _setAddressCompanyGuarantorIndividuModel.isTelp1Mandatory : _setAddressCompanyGuarantorModel.isTelp1Mandatory;
      }
      else if(typeAddress == 2){
          status = _setAddressCompanyModel.isTelp1Mandatory;
      }
      else if(typeAddress == 3){
          status = _setAddressCompanySahamModel.isTelp1Mandatory;
      }
      return status;
  }

  bool setTelp1AreaMandatory(int typeAddress){
      bool status = false;
      if(typeAddress == 1){
          status = custType == "PER" ? _setAddressCompanyGuarantorIndividuModel.isTelp1AreaMandatory : _setAddressCompanyGuarantorModel.isTelp1AreaMandatory;
      }
      else if(typeAddress == 2){
          status = _setAddressCompanyModel.isTelp1AreaMandatory;
      }
      else if(typeAddress == 3){
          status = _setAddressCompanySahamModel.isTelp1AreaMandatory;
      }
      return status;
  }

  bool setKodePosMandatory(int typeAddress){
      bool status = false;
      if(typeAddress == 1){
          status = custType == "PER" ? _setAddressCompanyGuarantorIndividuModel.isKodePosMandatory : _setAddressCompanyGuarantorModel.isKodePosMandatory;
      }
      else if(typeAddress == 2){
          status = _setAddressCompanyModel.isKodePosMandatory;
      }
      else if(typeAddress == 3){
          status = _setAddressCompanySahamModel.isKodePosMandatory;
      }
      return status;
  }

  bool setProvinsiMandatory(int typeAddress){
      bool status = false;
      if(typeAddress == 1){
          status = custType == "PER" ? _setAddressCompanyGuarantorIndividuModel.isProvinsiMandatory : _setAddressCompanyGuarantorModel.isProvinsiMandatory;
      }
      else if(typeAddress == 2){
          status = _setAddressCompanyModel.isProvinsiMandatory;
      }
      else if(typeAddress == 3){
          status = _setAddressCompanySahamModel.isProvinsiMandatory;
      }
      return status;
  }

  bool setKabKotMandatory(int typeAddress){
      bool status = false;
      if(typeAddress == 1){
          status = custType == "PER" ? _setAddressCompanyGuarantorIndividuModel.isKabKotMandatory : _setAddressCompanyGuarantorModel.isKabKotMandatory;
      }
      else if(typeAddress == 2){
          status = _setAddressCompanyModel.isKabKotMandatory;
      }
      else if(typeAddress == 3){
          status = _setAddressCompanySahamModel.isKabKotMandatory;
      }
      return status;
  }

  bool setKecamatanMandatory(int typeAddress){
      bool status = false;
      if(typeAddress == 1){
          status = custType == "PER" ? _setAddressCompanyGuarantorIndividuModel.isKecamatanMandatory : _setAddressCompanyGuarantorModel.isKecamatanMandatory;
      }
      else if(typeAddress == 2){
          status = _setAddressCompanyModel.isKecamatanMandatory;
      }
      else if(typeAddress == 3){
          status = _setAddressCompanySahamModel.isKecamatanMandatory;
      }
      return status;
  }

  bool setKelurahanMandatory(int typeAddress){
      bool status = false;
      if(typeAddress == 1){
          status = custType == "PER" ? _setAddressCompanyGuarantorIndividuModel.isKelurahanMandatory : _setAddressCompanyGuarantorModel.isKelurahanMandatory;
      }
      else if(typeAddress == 2){
          status = _setAddressCompanyModel.isKelurahanMandatory;
      }
      else if(typeAddress == 3){
          status = _setAddressCompanySahamModel.isKelurahanMandatory;
      }
      return status;
  }

  bool setRWMandatory(int typeAddress){
      bool status = false;
      if(typeAddress == 1){
          status = custType == "PER" ? _setAddressCompanyGuarantorIndividuModel.isRWMandatory : _setAddressCompanyGuarantorModel.isRWMandatory;
      }
      else if(typeAddress == 2){
          status = _setAddressCompanyModel.isRWMandatory;
      }
      else if(typeAddress == 3){
          status = _setAddressCompanySahamModel.isRWMandatory;
      }
      return status;
  }

  bool setRTMandatory(int typeAddress){
      bool status = false;
      if(typeAddress == 1){
          status = custType == "PER" ? _setAddressCompanyGuarantorIndividuModel.isRTMandatory : _setAddressCompanyGuarantorModel.isRTMandatory;
      }
      else if(typeAddress == 2){
          status = _setAddressCompanyModel.isRTMandatory;
      }
      else if(typeAddress == 3){
          status = _setAddressCompanySahamModel.isRTMandatory;
      }
      return status;
  }

  bool setAddressMandatory(int typeAddress){
      bool status = false;
      if(typeAddress == 1){
          status = custType == "PER" ? _setAddressCompanyGuarantorIndividuModel.isAddressMandatory : _setAddressCompanyGuarantorModel.isAddressMandatory;
      }
      else if(typeAddress == 2){
          status = _setAddressCompanyModel.isAddressMandatory;
      }
      else if(typeAddress == 3){
          status = _setAddressCompanySahamModel.isAddressMandatory;
      }
      return status;
  }

  bool setAddressTypeMandatory(int typeAddress){
      bool status = false;
      if(typeAddress == 1){
          status = custType == "PER" ? _setAddressCompanyGuarantorIndividuModel.isAddressTypeMandatory : _setAddressCompanyGuarantorModel.isAddressTypeMandatory;
      }
      else if(typeAddress == 2){
          status = _setAddressCompanyModel.isAddressTypeMandatory;
      }
      else if(typeAddress == 3){
          status = _setAddressCompanySahamModel.isAddressTypeMandatory;
      }
      return status;
  }


  bool get isEditAddress => _isEditAddress;

  set isEditAddress(bool value) {
      _isEditAddress = value;
      notifyListeners();
  }

  bool get isFaxDakor => _isFaxDakor;

  set isFaxDakor(bool value) {
    _isFaxDakor = value;
    notifyListeners();
  }

  bool get isFaxAreaDakor => _isFaxAreaDakor;

  set isFaxAreaDakor(bool value) {
    _isFaxAreaDakor = value;
    notifyListeners();
  }

  bool get isTelp2Dakor => _isTelp2Dakor;

  set isTelp2Dakor(bool value) {
    _isTelp2Dakor = value;
    notifyListeners();
  }

  bool get isTelp2AreaDakor => _isTelp2AreaDakor;

  set isTelp2AreaDakor(bool value) {
    _isTelp2AreaDakor = value;
    notifyListeners();
  }

  bool get isTelp1Dakor => _isTelp1Dakor;

  set isTelp1Dakor(bool value) {
    _isTelp1Dakor = value;
    notifyListeners();
  }

  bool get isTelp1AreaDakor => _isTelp1AreaDakor;

  set isTelp1AreaDakor(bool value) {
    _isTelp1AreaDakor = value;
    notifyListeners();
  }

  bool get isKodePosDakor => _isKodePosDakor;

  set isKodePosDakor(bool value) {
    _isKodePosDakor = value;
    notifyListeners();
  }

  bool get isProvinsiDakor => _isProvinsiDakor;

  set isProvinsiDakor(bool value) {
    _isProvinsiDakor = value;
    notifyListeners();
  }

  bool get isKabKotDakor => _isKabKotDakor;

  set isKabKotDakor(bool value) {
    _isKabKotDakor = value;
    notifyListeners();
  }

  bool get isKecamatanDakor => _isKecamatanDakor;

  set isKecamatanDakor(bool value) {
    _isKecamatanDakor = value;
    notifyListeners();
  }

  bool get isKelurahanDakor => _isKelurahanDakor;

  set isKelurahanDakor(bool value) {
    _isKelurahanDakor = value;
    notifyListeners();
  }

  bool get isRWDakor => _isRWDakor;

  set isRWDakor(bool value) {
    _isRWDakor = value;
    notifyListeners();
  }

  bool get isRTDakor => _isRTDakor;

  set isRTDakor(bool value) {
    _isRTDakor = value;
    notifyListeners();
  }

  bool get isAddressDakor => _isAddressDakor;

  set isAddressDakor(bool value) {
    _isAddressDakor = value;
    notifyListeners();
  }

  bool get isAddressTypeDakor => _isAddressTypeDakor;

  set isAddressTypeDakor(bool value) {
    _isAddressTypeDakor = value;
    notifyListeners();
  }

  bool get isDataLatLongDakor => _isDataLatLongDakor;

  set isDataLatLongDakor(bool value) {
    _isDataLatLongDakor = value;
    notifyListeners();
  }
}
