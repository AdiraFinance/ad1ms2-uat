import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/address_guarantor_model.dart';
import 'package:ad1ms2_dev/models/form_m_foto_model.dart';
import 'package:ad1ms2_dev/models/form_m_informasi_alamat_model.dart';
import 'package:ad1ms2_dev/models/form_m_pekerjaan_model.dart';
import 'package:ad1ms2_dev/models/ms2_cust_addr_model.dart';
import 'package:ad1ms2_dev/models/ms2_cust_ind_occupation_model.dart';
import 'package:ad1ms2_dev/models/show_mandatory_occupation_model.dart';
import 'package:ad1ms2_dev/screens/form_m/search_business_field.dart';
import 'package:ad1ms2_dev/screens/form_m/search_pep.dart';
import 'package:ad1ms2_dev/screens/form_m/search_sector_economi.dart';
import 'package:ad1ms2_dev/screens/search_business_location.dart';
import 'package:ad1ms2_dev/screens/search_profesi.dart';
import 'package:ad1ms2_dev/screens/search_status_location.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/dashboard/dashboard_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_foto_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_income_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_individu_change_notifier/form_m_parent_individu_change_notifier.dart';
import 'package:ad1ms2_dev/shared/resource/get_company_type.dart';
import 'package:ad1ms2_dev/shared/search_business_field_change_notif.dart';
import 'package:ad1ms2_dev/shared/search_business_location_change_notif.dart';
import 'package:ad1ms2_dev/shared/search_pep_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_profesi_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_sector_economic_change_notif.dart';
import 'package:ad1ms2_dev/shared/search_status_location_field_change_notif.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/io_client.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class FormMOccupationChangeNotif with ChangeNotifier {
  var storage = FlutterSecureStorage();
  int _oldListSize = 0;
  bool _autoValidate = false;
  int _selectedIndex = -1;
  OccupationModel _occupationSelected;
  TextEditingController _controllerOccupation = TextEditingController();
  TextEditingController _controllerEstablishedDate = TextEditingController();
  TextEditingController _controllerBusinessName = TextEditingController();
  TextEditingController _controllerStatusLocation = TextEditingController();
  TextEditingController _controllerBusinessLocation = TextEditingController();
  TextEditingController _controllerTotalEstablishedDate = TextEditingController();
  TextEditingController _controllerEmployeeTotal = TextEditingController();
  TextEditingController _controllerSectorEconomic = TextEditingController();
  TextEditingController _controllerBusinessField = TextEditingController();
  TextEditingController _controllerCompanyName = TextEditingController();
  TextEditingController _controllerProfessionType = TextEditingController();
  TextEditingController _controllerJenisPEP = TextEditingController();
  TextEditingController _controllerAddress = TextEditingController();
  TextEditingController _controllerAddressType = TextEditingController();
  TextEditingController _controllerRT = TextEditingController();
  TextEditingController _controllerRW = TextEditingController();
  TextEditingController _controllerKelurahan = TextEditingController();
  TextEditingController _controllerKecamatan = TextEditingController();
  TextEditingController _controllerKota = TextEditingController();
  TextEditingController _controllerProvinsi = TextEditingController();
  TextEditingController _controllerPostalCode = TextEditingController();
  TextEditingController _controllerTelepon1Area = TextEditingController();
  TextEditingController _controllerTelepon1 = TextEditingController();
  // TextEditingController _controllerTelepon2Area = TextEditingController();
  // TextEditingController _controllerTelepon2 = TextEditingController();
  // TextEditingController _controllerFaxArea = TextEditingController();
  // TextEditingController _controllerFax = TextEditingController();
  TypeOfBusinessModel _typeOfBusinessModelSelected;
  StatusLocationModel _statusLocationModelSelected;
  BusinessLocationModel _businessLocationModelSelected;
  ProfessionTypeModel _professionTypeModelSelected;
  CompanyTypeModel _companyTypeModelSelected;
  EmployeeStatusModel _employeeStatusModelSelected;
  SectorEconomicModel _sectorEconomicModelSelected;
  BusinessFieldModel _businessFieldModelSelected;
  PEPModel _pepModelSelected;
  String _flagChangeOccupation = "";
  List<OccupationModel> _listOccupation = [];
  bool _loadData = false;
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  DbHelper _dbHelper = DbHelper();
  String _customerOccupationID;
  bool _isDisablePACIAAOSCONA = false;

  ShowMandatoryOccupationModel _showMandatoryOccupationModel;

  void setShowMandatoryOccupationModel(BuildContext context){
    _showMandatoryOccupationModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryOccupationModel;
  }

  bool isLamaBekerjaBerjalan() => _showMandatoryOccupationModel.isLamaBekerjaBerjalan;
  bool isNamaUsahaWiraswasta() => _showMandatoryOccupationModel.isNamaUsahaWiraswasta;
  bool isJenisBadanUsahaWiraswasta() => _showMandatoryOccupationModel.isJenisBadanUsahaWiraswasta;
  bool isSektorEkonomiWiraswasta() => _showMandatoryOccupationModel.isSektorEkonomiWiraswasta;
  bool isLapanganUsahaWiraswasta() => _showMandatoryOccupationModel.isLapanganUsahaWiraswasta;
  bool isStatusLokasiWiraswasta() => _showMandatoryOccupationModel.isStatusLokasiWiraswasta;
  bool isLokasiUsahaWiraswasta() => _showMandatoryOccupationModel.isLokasiUsahaWiraswasta;
  bool isTotalPegawaiWiraswasta() => _showMandatoryOccupationModel.isTotalPegawaiWiraswasta;
  bool isTotalLamaBekerjaWiraswasta() => _showMandatoryOccupationModel.isTotalLamaBekerjaWiraswasta;
  bool isJenisProfesiProfesional() => _showMandatoryOccupationModel.isJenisProfesiProfesional;
  bool isSektorEkonomiProfesional() => _showMandatoryOccupationModel.isSektorEkonomiProfesional;
  bool isLapanganUsahaProfesional() => _showMandatoryOccupationModel.isLapanganUsahaProfesional;
  bool isStatusLokasiProfesional() => _showMandatoryOccupationModel.isStatusLokasiProfesional;
  bool isLokasiUsahaProfesional() => _showMandatoryOccupationModel.isLokasiUsahaProfesional;
  bool isTotalLamaBekerjaProfesional() => _showMandatoryOccupationModel.isTotalLamaBekerjaProfesional;
  bool isNamaPerusahaanLainnya() => _showMandatoryOccupationModel.isNamaPerusahaanLainnya;
  bool isJenisPerusahaanLainnya() => _showMandatoryOccupationModel.isJenisPerusahaanLainnya;
  bool isSektorEkonomiLainnya() => _showMandatoryOccupationModel.isSektorEkonomiLainnya;
  bool isLapanganUsahaLainnya() => _showMandatoryOccupationModel.isLapanganUsahaLainnya;
  bool isTotalPegawaiLainnya() => _showMandatoryOccupationModel.isTotalPegawaiLainnya;
  bool isTotalLamaBekerjaLainnya() => _showMandatoryOccupationModel.isTotalLamaBekerjaLainnya;
  bool isJenisPepHighRiskLainnya() => _showMandatoryOccupationModel.isJenisPepHighRiskLainnya;
  bool isStatusPegawaiLainnya() => _showMandatoryOccupationModel.isStatusPegawaiLainnya;

  bool isLamaBekerjaBerjalanMandatory() => _showMandatoryOccupationModel.isLamaBekerjaBerjalanMandatory;
  bool isNamaUsahaWiraswastaMandatory() => _showMandatoryOccupationModel.isNamaUsahaWiraswastaMandatory;
  bool isJenisBadanUsahaWiraswastaMandatory() => _showMandatoryOccupationModel.isJenisBadanUsahaWiraswastaMandatory;
  bool isSektorEkonomiWiraswastaMandatory() => _showMandatoryOccupationModel.isSektorEkonomiWiraswastaMandatory;
  bool isLapanganUsahaWiraswastaMandatory() => _showMandatoryOccupationModel.isLapanganUsahaWiraswastaMandatory;
  bool isStatusLokasiWiraswastaMandatory() => _showMandatoryOccupationModel.isStatusLokasiWiraswastaMandatory;
  bool isLokasiUsahaWiraswastaMandatory() => _showMandatoryOccupationModel.isLokasiUsahaWiraswastaMandatory;
  bool isTotalPegawaiWiraswastaMandatory() => _showMandatoryOccupationModel.isTotalPegawaiWiraswastaMandatory;
  bool isTotalLamaBekerjaWiraswastaMandatory() => _showMandatoryOccupationModel.isTotalLamaBekerjaWiraswastaMandatory;
  bool isJenisProfesiProfesionalMandatory() => _showMandatoryOccupationModel.isJenisProfesiProfesionalMandatory;
  bool isSektorEkonomiProfesionalMandatory() => _showMandatoryOccupationModel.isSektorEkonomiProfesionalMandatory;
  bool isLapanganUsahaProfesionalMandatory() => _showMandatoryOccupationModel.isLapanganUsahaProfesionalMandatory;
  bool isStatusLokasiProfesionalMandatory() => _showMandatoryOccupationModel.isStatusLokasiProfesionalMandatory;
  bool isLokasiUsahaProfesionalMandatory() => _showMandatoryOccupationModel.isLokasiUsahaProfesionalMandatory;
  bool isTotalLamaBekerjaProfesionalMandatory() => _showMandatoryOccupationModel.isTotalLamaBekerjaProfesionalMandatory;
  bool isNamaPerusahaanLainnyaMandatory() => _showMandatoryOccupationModel.isNamaPerusahaanLainnyaMandatory;
  bool isJenisPerusahaanLainnyaMandatory() => _showMandatoryOccupationModel.isJenisPerusahaanLainnyaMandatory;
  bool isSektorEkonomiLainnyaMandatory() => _showMandatoryOccupationModel.isSektorEkonomiLainnyaMandatory;
  bool isLapanganUsahaLainnyaMandatory() => _showMandatoryOccupationModel.isLapanganUsahaLainnyaMandatory;
  bool isTotalPegawaiLainnyaMandatory() => _showMandatoryOccupationModel.isTotalPegawaiLainnyaMandatory;
  bool isTotalLamaBekerjaLainnyaMandatory() => _showMandatoryOccupationModel.isTotalLamaBekerjaLainnyaMandatory;
  bool isJenisPepHighRiskLainnyaMandatory() => _showMandatoryOccupationModel.isJenisPepHighRiskLainnyaMandatory;
  bool isStatusPegawaiLainnyaMandatory() => _showMandatoryOccupationModel.isStatusPegawaiLainnyaMandatory;

  bool _editOccupation = false;
  bool _editLamaBekerjaBerjalan = false;
  bool _editNamaUsahaWiraswasta = false;
  bool _editJenisBadanUsahaWiraswasta = false;
  bool _editSektorEkonomiWiraswasta = false;
  bool _editLapanganUsahaWiraswasta = false;
  bool _editStatusLokasiWiraswasta = false;
  bool _editLokasiUsahaWiraswasta = false;
  bool _editTotalPegawaiWiraswasta = false;
  bool _editTotalLamaBekerjaWiraswasta = false;
  bool _editJenisProfesiProfesional = false;
  bool _editSektorEkonomiProfesional = false;
  bool _editLapanganUsahaProfesional = false;
  bool _editStatusLokasiProfesional = false;
  bool _editLokasiUsahaProfesional = false;
  bool _editTotalLamaBekerjaProfesional = false;
  bool _editNamaPerusahaanLainnya = false;
  bool _editJenisPerusahaanLainnya = false;
  bool _editSektorEkonomiLainnya = false;
  bool _editLapanganUsahaLainnya = false;
  bool _editTotalPegawaiLainnya = false;
  bool _editTotalLamaBekerjaLainnya = false;
  bool _editJenisPepHighRiskLainnya = false;
  bool _editStatusPegawaiLainnya = false;

  List<TypeOfBusinessModel> _listTypeOfBusiness = [
    TypeOfBusinessModel("001", "PRIVATE"),
    TypeOfBusinessModel("002", "PMA"),
    TypeOfBusinessModel("003", "PMDN"),
    TypeOfBusinessModel("004", "PMDA"),
    TypeOfBusinessModel("005", "GOVERMENT"),
    TypeOfBusinessModel("006", "BUMN"),
    TypeOfBusinessModel("007", "KOPERASI"),
    TypeOfBusinessModel("008", "YAYASAN"),
    TypeOfBusinessModel("009", "PT"),
    TypeOfBusinessModel("010", "CV"),
    TypeOfBusinessModel("011", "PT,TBK"),
    TypeOfBusinessModel("013", "BUMD"),
    TypeOfBusinessModel("014", "PDAM"),
    TypeOfBusinessModel("015", "PD PASAR"),
    TypeOfBusinessModel("016", "KOPERASI PRIMER"),
    TypeOfBusinessModel("017", "PERUSAHAAN CAMPURAN"),
  ];

  List<StatusLocationModel> _listStatusLocation = [
//    StatusLocationModel("01", "RUMAH"),
//    StatusLocationModel("02", "PERTOKOAN"),
//    StatusLocationModel("03", "PASAR"),
//    StatusLocationModel("04", "INDUSTRI"),
//    StatusLocationModel("05", "PERKANTORAN"),
//    StatusLocationModel("06", "LAINNYA")
  ];

  List<BusinessLocationModel> _listBusinessLocation = [
//    BusinessLocationModel("01", "STRATEGIS"),
//    BusinessLocationModel("02", "BIASA"),
//    BusinessLocationModel("03", "TIDAK STRATEGIS"),
//    BusinessLocationModel("04", "LAINNYA")
  ];

  List<ProfessionTypeModel> _listProfessionType = [
//    ProfessionTypeModel("01", "GURU"),
//    ProfessionTypeModel("02", "DOKTER"),
//    ProfessionTypeModel("03", "LAWYER"),
//    ProfessionTypeModel("04", "LAINNYA")
  ];

  List<CompanyTypeModel> _listCompanyType = [
    CompanyTypeModel("001", "PRIVATE"),
    CompanyTypeModel("002", "PMA"),
    CompanyTypeModel("003", "PMDN"),
    CompanyTypeModel("004", "PMDA"),
    CompanyTypeModel("005", "GOVERMENT"),
    CompanyTypeModel("006", "BUMN"),
    CompanyTypeModel("007", "KOPERASI"),
    CompanyTypeModel("008", "YAYASAN"),
    CompanyTypeModel("009", "PT"),
    CompanyTypeModel("010", "CV"),
    CompanyTypeModel("011", "PT,TBK"),
    CompanyTypeModel("013", "BUMD"),
    CompanyTypeModel("014", "PDAM"),
    CompanyTypeModel("015", "PD PASAR"),
    CompanyTypeModel("016", "KOPERASI PRIMER"),
    CompanyTypeModel("017", "PERUSAHAAN CAMPURAN"),
//    CompanyTypeModel("001", "PRIVATE"),
//    CompanyTypeModel("001", "PMA"),
//    CompanyTypeModel("001", "PMDN"),
//    CompanyTypeModel("001", "PMDA"),
//    CompanyTypeModel("001", "GOVERMENT"),
//    CompanyTypeModel("001", "BUMN"),
//    CompanyTypeModel("001", "KOPERASI"),
//    CompanyTypeModel("001", "YAYASAN"),
//    CompanyTypeModel("001", "PT"),
//    CompanyTypeModel("001", "CV"),
//    CompanyTypeModel("001", "PT,TBK"),
//    CompanyTypeModel("001", "BUMD"),
  ];
  List<EmployeeStatusModel> _listEmployeeStatus = [
    EmployeeStatusModel("00", "TRAINEE"),
    EmployeeStatusModel("01", "PERCOBAAN - AKTIVE"),
    EmployeeStatusModel("02", "TETAP - AKTIVE"),
    EmployeeStatusModel("03", "RESIGN - AKTIVE"),
    EmployeeStatusModel("05", "KONTRAK"),
    EmployeeStatusModel("07", "MUTASI")
  ];
  List<AddressModel> _listOccupationAddress = [];

  bool get isDisablePACIAAOSCONA => _isDisablePACIAAOSCONA;

  set isDisablePACIAAOSCONA(bool value) {
    this._isDisablePACIAAOSCONA = value;
    notifyListeners();
  }

  String get customerOccupationID => _customerOccupationID;

  set customerOccupationID(String value) {
    this._customerOccupationID = value;
    notifyListeners();
  }

  int get oldListSize => _oldListSize;

  set oldListSize(int value) {
    this._oldListSize = value;
    notifyListeners();
  }

  bool get autoValidate => _autoValidate;

  set autoValidate(bool value) {
    this._autoValidate = value;
    notifyListeners();
  }

  OccupationModel get occupationSelected => _occupationSelected;

  set occupationSelected(OccupationModel value) {
    this._occupationSelected = value;
    notifyListeners();
  }

  TextEditingController get controllerOccupation => _controllerOccupation;

  set controllerOccupation(TextEditingController value) {
    _controllerOccupation = value;
    notifyListeners();
  }

  TextEditingController get controllerEstablishedDate => _controllerEstablishedDate;

  TextEditingController get controllerBusinessName => _controllerBusinessName;

  TextEditingController get controllerTotalEstablishedDate => _controllerTotalEstablishedDate;

  TextEditingController get controllerStatusLocation => _controllerStatusLocation;

  set controllerStatusLocation(TextEditingController value) {
    _controllerStatusLocation = value;
    this.notifyListeners();
  }

  TextEditingController get controllerBusinessLocation => _controllerBusinessLocation;

  set controllerBusinessLocation(TextEditingController value) {
    _controllerBusinessLocation = value;
    this.notifyListeners();
  }

  TextEditingController get controllerEmployeeTotal => _controllerEmployeeTotal;

  TextEditingController get controllerSectorEconomic => _controllerSectorEconomic;

  TextEditingController get controllerProfessionType => _controllerProfessionType;

  TextEditingController get controllerBusinessField => _controllerBusinessField;

  TypeOfBusinessModel get typeOfBusinessModelSelected => _typeOfBusinessModelSelected;

  TextEditingController get controllerCompanyName => _controllerCompanyName;

  TextEditingController get controllerJenisPEP => _controllerJenisPEP;

  TextEditingController get controllerAddress => _controllerAddress;

  TextEditingController get controllerAddressType => _controllerAddressType;

  TextEditingController get controllerRT => _controllerRT;

  set typeOfBusinessModelSelected(TypeOfBusinessModel value) {
    this._typeOfBusinessModelSelected = value;
    notifyListeners();
  }

  UnmodifiableListView<OccupationModel> get listOccupation {
    return UnmodifiableListView(this._listOccupation);
  }

  UnmodifiableListView<TypeOfBusinessModel> get listTypeOfBusiness {
    return UnmodifiableListView(this._listTypeOfBusiness);
  }

  UnmodifiableListView<StatusLocationModel> get listStatusLocation {
    return UnmodifiableListView(this._listStatusLocation);
  }

  UnmodifiableListView<BusinessLocationModel> get listBusinessLocation {
    return UnmodifiableListView(this._listBusinessLocation);
  }

  StatusLocationModel get statusLocationModelSelected => _statusLocationModelSelected;

  set statusLocationModelSelected(StatusLocationModel value) {
    this._statusLocationModelSelected = value;
    notifyListeners();
  }

  BusinessLocationModel get businessLocationModelSelected => _businessLocationModelSelected;

  set businessLocationModelSelected(BusinessLocationModel value) {
    this._businessLocationModelSelected = value;
    notifyListeners();
  }

  UnmodifiableListView<ProfessionTypeModel> get listProfessionType {
    return UnmodifiableListView(this._listProfessionType);
  }

  ProfessionTypeModel get professionTypeModelSelected => _professionTypeModelSelected;

  set professionTypeModelSelected(ProfessionTypeModel value) {
    this._professionTypeModelSelected = value;
    notifyListeners();
  }

  UnmodifiableListView<CompanyTypeModel> get listCompanyType {
    return UnmodifiableListView(this._listCompanyType);
  }

  CompanyTypeModel get companyTypeModelSelected => _companyTypeModelSelected;

  set companyTypeModelSelected(CompanyTypeModel value) {
    this._companyTypeModelSelected = value;
    notifyListeners();
  }

  EmployeeStatusModel get employeeStatusModelSelected => _employeeStatusModelSelected;

  set employeeStatusModelSelected(EmployeeStatusModel value) {
    this._employeeStatusModelSelected = value;
    notifyListeners();
  }

  UnmodifiableListView<EmployeeStatusModel> get listEmployeeStatus {
    return UnmodifiableListView(this._listEmployeeStatus);
  }

  SectorEconomicModel get sectorEconomicModelSelected => _sectorEconomicModelSelected;

  set sectorEconomicModelSelected(SectorEconomicModel value) {
    this._sectorEconomicModelSelected = value;
  }

  void searchSectorEconomic(BuildContext context) async {
    SectorEconomicModel data = await Navigator.push(context, MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => SearchSectorEconomicChangeNotif(),
                child: SearchSectorEconomic())));
    if (data != null) {
      sectorEconomicModelSelected = data;
      this._controllerSectorEconomic.text = "${data.KODE} - ${data.DESKRIPSI}";
      notifyListeners();
    } else {
      return;
    }
  }

  void searchProfessionType(BuildContext context) async {
    ProfessionTypeModel data = await Navigator.push(context, MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => SearchProfesiChangeNotifier(),
                child: SearchProfesi())));
    if (data != null) {
      this._professionTypeModelSelected = data;
      this._controllerProfessionType.text = "${data.id} - ${data.desc}";
      notifyListeners();
    } else {
      return;
    }
  }

  BusinessFieldModel get businessFieldModelSelected => _businessFieldModelSelected;

  set businessFieldModelSelected(BusinessFieldModel value) {
    this._businessFieldModelSelected = value;
  }

  void searchBusinesField(BuildContext context) async {
    BusinessFieldModel data = await Navigator.push(context, MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => SearchBusinessFieldChangeNotif(),
                child: SearchBusinessField(id: this._sectorEconomicModelSelected.KODE)
            )
        )
    );
    if (data != null) {
      businessFieldModelSelected = data;
      this._controllerBusinessField.text = "${data.KODE} - ${data.DESKRIPSI}";
    } else {
      return;
    }
    notifyListeners();
  }

  PEPModel get pepModelSelected => _pepModelSelected;

  set pepModelSelected(PEPModel value) {
    this._pepModelSelected = value;
    notifyListeners();
  }

  void searchPEP(BuildContext context) async {
    PEPModel data = await Navigator.push(context, MaterialPageRoute(
            builder: (context) =>SearchPEP()));
    if (data != null) {
      pepModelSelected = data;
      this._controllerJenisPEP.text = "${data.KODE} - ${data.DESKRIPSI}";
      notifyListeners();
    } else {
      return;
    }
  }

  void searchStatusLocation(BuildContext context) async {
    StatusLocationModel data = await Navigator.push(context, MaterialPageRoute(
            builder: (context) =>
            // di edit karena set default value seperti PEP
                // ChangeNotifierProvider(
                // create: (context) => SearchStatusLocationChangeNotif(),
                // child:
                SearchStatusLocation()
                // )
    ));
    if (data != null) {
      statusLocationModelSelected = data;
      this._controllerStatusLocation.text = "${data.id} - ${data.desc}";
      notifyListeners();
    } else {
      return;
    }
  }

  void searchBusinessLocation(BuildContext context) async {
    BusinessLocationModel data = await Navigator.push(context, MaterialPageRoute(
            builder: (context) =>
            // di edit karena set default value seperti PEP
                // ChangeNotifierProvider(
                // create: (context) => SearchBusinessLocationChangeNotif(),
                // child:
                SearchBusinessLocation()
                // )
    ));
    if (data != null) {
      businessLocationModelSelected = data;
      this._controllerBusinessLocation.text = "${data.id} - ${data.desc}";
      notifyListeners();
    } else {
      return;
    }
  }

  String get flagChangeOccupation => _flagChangeOccupation;

  set flagChangeOccupation(String value) {
    this._flagChangeOccupation = value;
    notifyListeners();
  }

  void clearOccupationSelected() {
    if (this._flagChangeOccupation != "") {
      if (this._flagChangeOccupation == "OC005" ||
          this._flagChangeOccupation == "OC006") {
        this._controllerBusinessName.clear();
        typeOfBusinessModelSelected = null;
        this._controllerSectorEconomic.clear();
        sectorEconomicModelSelected = null;
        this._controllerBusinessField.clear();
        businessFieldModelSelected = null;
        statusLocationModelSelected = null;
        businessLocationModelSelected = null;
        this._controllerEmployeeTotal.clear();
        this._controllerTotalEstablishedDate.clear();
      } else if (this._flagChangeOccupation == "OC007") {
        this._professionTypeModelSelected = null;
        this._controllerSectorEconomic.clear();
        sectorEconomicModelSelected = null;
        this._controllerBusinessField.clear();
        businessFieldModelSelected = null;
        statusLocationModelSelected = null;
        businessLocationModelSelected = null;
        this._controllerTotalEstablishedDate.clear();
      } else {
        this._controllerCompanyName.clear();
        companyTypeModelSelected = null;
        this._controllerSectorEconomic.clear();
        sectorEconomicModelSelected = null;
        this._controllerBusinessField.clear();
        businessFieldModelSelected = null;
        this._controllerEmployeeTotal.clear();
        this._controllerTotalEstablishedDate.clear();
        employeeStatusModelSelected = null;
        this._controllerJenisPEP.clear();
        pepModelSelected = null;
      }
      flagChangeOccupation = this._occupationSelected.KODE;
    } else {
      flagChangeOccupation = this._occupationSelected.KODE;
    }
  }

  TextEditingController get controllerRW => _controllerRW;

  TextEditingController get controllerKelurahan => _controllerKelurahan;

  TextEditingController get controllerKecamatan => _controllerKecamatan;

  TextEditingController get controllerKota => _controllerKota;

  TextEditingController get controllerProvinsi => _controllerProvinsi;

  TextEditingController get controllerPostalCode => _controllerPostalCode;

  // Telepon 1 Area
  TextEditingController get controllerTelepon1Area => _controllerTelepon1Area;

  // Telepon 1
  TextEditingController get controllerTelepon1 => _controllerTelepon1;

  // // Telepon 2 Area
  // TextEditingController get controllerTelepon2Area => _controllerTelepon2Area;
  //
  // // Telepon 2
  // TextEditingController get controllerTelepon2 => _controllerTelepon2;
  //
  // // Kode Area Fax
  // TextEditingController get controllerFaxArea => _controllerFaxArea;
  //
  // // Fax
  // TextEditingController get controllerFax => _controllerFax;

  List<AddressModel> get listOccupationAddress => _listOccupationAddress;

  int get selectedIndex => _selectedIndex;

  set selectedIndex(int value) {
    this._selectedIndex = value;
    notifyListeners();
  }

  void addOccupationAddress(AddressModel value) {
    this._listOccupationAddress.add(value);
    notifyListeners();
  }

  void updateOccupationAddress(AddressModel value, int index) {
    this._listOccupationAddress[index] = value;
    notifyListeners();
  }

  void deleteListOccupationAddress(BuildContext context, int index) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context){
          return Theme(
            data: ThemeData(
                fontFamily: "NunitoSans",
                primaryColor: Colors.black,
                primarySwatch: primaryOrange,
                accentColor: myPrimaryColor
            ),
            child: AlertDialog(
              title: Text("Warning", style: TextStyle(fontWeight: FontWeight.bold)),
              content: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text("Apakah kamu yakin menghapus alamat ini?",),
                ],
              ),
              actions: <Widget>[
                new FlatButton(
                  onPressed: () {
                    if(_lastKnownState == "IDE"){
                      this._listOccupationAddress.removeAt(index);
                      if(selectedIndex == index){
                        selectedIndex = -1;
                        this._controllerAddress.clear();
                        this._controllerAddressType.clear();
                        this._controllerRT.clear();
                        this._controllerRW.clear();
                        this._controllerKelurahan.clear();
                        this._controllerKecamatan.clear();
                        this._controllerKota.clear();
                        this._controllerProvinsi.clear();
                        this._controllerPostalCode.clear();
                        this._controllerTelepon1Area.clear();
                        this._controllerTelepon1.clear();
                      }
                    }
                    else{
                      this._listOccupationAddress[index].active = 1;
                    }
                    // this._controllerTelepon2Area.clear();
                    // this._controllerTelepon2.clear();
                    // this._controllerFaxArea.clear();
                    // this._controllerFax.clear();
                    notifyListeners();
                    Navigator.pop(context);
                  },
                  child: new Text('Ya', style: TextStyle(color: Colors.grey),),
                ),
                new FlatButton(
                  onPressed: () => Navigator.of(context).pop(true),
                  child: new Text('Tidak'),
                ),
              ],
            ),
          );
        }
    );
  }

  void setCorrespondenceAddress(AddressModel value,int index) {
    this._controllerAddress.text = value.address;
    this._controllerAddressType.text = value.jenisAlamatModel.KODE + " - " + value.jenisAlamatModel.DESKRIPSI;
    this._controllerRT.text = value.rt;
    this._controllerRW.text = value.rw;
    this._controllerKelurahan.text = value.kelurahanModel.KEL_ID + " - " + value.kelurahanModel.KEL_NAME;
    this._controllerKecamatan.text = value.kelurahanModel.KEC_ID + " - " + value.kelurahanModel.KEC_NAME;
    this._controllerKota.text = value.kelurahanModel.KABKOT_ID + " - " + value.kelurahanModel.KABKOT_NAME;
    this._controllerProvinsi.text = value.kelurahanModel.PROV_ID + " - " + value.kelurahanModel.PROV_NAME;
    this._controllerPostalCode.text = value.kelurahanModel.ZIPCODE;
    this._controllerTelepon1Area.text = value.areaCode;
    this._controllerTelepon1.text = value.phone;
    // this._controllerTelepon2Area.text = value.phoneArea2;
    // this._controllerTelepon2.text = value.phone2;
    // this._controllerFaxArea.text = value.faxArea;
    // this._controllerFax.text = value.fax;
    for(int i=0; i < this._listOccupationAddress.length; i++){
      if(this._listOccupationAddress[i].isCorrespondence){
        this._listOccupationAddress[i].isCorrespondence = false;
      }
    }
    this._listOccupationAddress[index].isCorrespondence = true;
    notifyListeners();
  }

  void iconShowDialog(BuildContext context) {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context){
          return Theme(
            data: ThemeData(
                fontFamily: "NunitoSans"
            ),
            child: AlertDialog(
              title: Text("Information", style: TextStyle(fontWeight: FontWeight.bold)),
              content: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text(
                    "∙ Tekan 1x untuk edit",
                  ),
                  Text(
                    "∙ Tekan lama untuk memilih alamat korespondensi",
                  ),
                  SizedBox(height: MediaQuery.of(context).size.height/37,),
                  Text("* Tekan icon pada pojok kanan atas untuk melihat kembali",style: TextStyle(fontSize: 12, color: Colors.grey))
                ],
              ),
              actions: <Widget>[
                FlatButton(
                    onPressed: (){
                      Navigator.pop(context);
                      // _updateStatusShowDialogSimilarity();
                    },
                    child: Text(
                        "CLOSE",
                        style: TextStyle(
                            color: primaryOrange,
                            fontSize: 14,
                            fontWeight: FontWeight.w500,
                            letterSpacing: 1.25
                        )
                    )
                )
              ],
            ),
          );
        }
    );
  }

  void isShowDialog(BuildContext context) {
    if(this.listOccupationAddress.length == 1  || this.listOccupationAddress.length == 2) {
      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context){
            return Theme(
              data: ThemeData(
                  fontFamily: "NunitoSans"
              ),
              child: AlertDialog(
                title: Text("Information", style: TextStyle(fontWeight: FontWeight.bold)),
                content: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Text(
                      "∙ Tekan 1x untuk edit",
                    ),
                    Text(
                      "∙ Tekan lama untuk memilih alamat korespondensi",
                    ),
                    SizedBox(height: MediaQuery.of(context).size.height/37,),
                    Text("* Tekan icon pada pojok kanan atas untuk melihat kembali",style: TextStyle(fontSize: 12, color: Colors.grey))
                  ],
                ),
                actions: <Widget>[
                  FlatButton(
                      onPressed: (){
                        Navigator.pop(context);
                        // _updateStatusShowDialogSimilarity();
                      },
                      child: Text(
                          "CLOSE",
                          style: TextStyle(
                              color: primaryOrange,
                              fontSize: 14,
                              fontWeight: FontWeight.w500,
                              letterSpacing: 1.25
                          )
                      )
                  )
                ],
              ),
            );
          }
      );
    }
  }

  Future<void> getListOccupation(BuildContext context) async{ //ga kepake
    try{
      _listOccupation.clear();
      final ioc = new HttpClient();
      ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
      final _http = IOClient(ioc);
      String urlPublic = await storage.read(key: "urlPublic");
      final _response = await _http.get(
          "${urlPublic}api/occupation/get-jenis-pekerjaan",
          headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
      );
      final _data = jsonDecode(_response.body);
      for(int i=0; i < _data.length;i++){
        _listOccupation.add(OccupationModel(_data[i]['KODE'], _data[i]['DESKRIPSI']));
      }
      // setValueEdit(context);
    } catch (e) {
      print("cek list occupation ${e.toString()}");
    }
  }

  void getDefaultPEP(BuildContext context) async {
    var _providerPEP = Provider.of<SearchPEPChangeNotifier>(context, listen: false);
    if(_providerPEP.listPEP.isNotEmpty){
      if(this._controllerJenisPEP.text == ''){
        for(int i=0; i < _providerPEP.listPEP.length; i++){
          if(_providerPEP.listPEP[i].KODE == "MHN"){
            this._controllerJenisPEP.text = "${_providerPEP.listPEP[i].KODE} - ${_providerPEP.listPEP[i].DESKRIPSI}";
            this._pepModelSelected = PEPModel(_providerPEP.listPEP[i].KODE, _providerPEP.listPEP[i].DESKRIPSI);
          }
        }
      }
    }
    else{
      await _providerPEP.getPEP();
      if(_providerPEP.listPEP.isNotEmpty){
        for(int i=0; i < _providerPEP.listPEP.length; i++){
          if(_providerPEP.listPEP[i].KODE == "MHN"){
            this._controllerJenisPEP.text = "${_providerPEP.listPEP[i].KODE} - ${_providerPEP.listPEP[i].DESKRIPSI}";
            this._pepModelSelected = PEPModel(_providerPEP.listPEP[i].KODE, _providerPEP.listPEP[i].DESKRIPSI);
          }
        }
      } else {
        await _providerPEP.getPEP();
        for(int i=0; i < _providerPEP.listPEP.length; i++){
          if(_providerPEP.listPEP[i].KODE == "MHN"){
            this._controllerJenisPEP.text = "${_providerPEP.listPEP[i].KODE} - ${_providerPEP.listPEP[i].DESKRIPSI}";
            this._pepModelSelected = PEPModel(_providerPEP.listPEP[i].KODE, _providerPEP.listPEP[i].DESKRIPSI);
          }
        }
      }
    }
    // notifyListeners();
  }

  void setDefaultLocation(BuildContext context) async{
    var _provLocation = Provider.of<SearchStatusLocationChangeNotif>(context,listen: false);
    var _provBusinessLocation = Provider.of<SearchBusinessLocationChangeNotif>(context,listen: false);
    await _provLocation.getStatusLocationField();
    await _provBusinessLocation.getBusinessLocationField();
    if(statusLocationModelSelected == null){
      for(int i=0; i < _provLocation.listBusinessField.length; i++){
        if(_provLocation.listBusinessField[i].id == "06"){
          statusLocationModelSelected = _provLocation.listBusinessField[i];
          this._controllerStatusLocation.text = "${_provLocation.listBusinessField[i].id} - ${_provLocation.listBusinessField[i].desc}";
        }
      }
    }

    if(businessLocationModelSelected == null){
      for(int i=0; i < _provBusinessLocation.listBusinessField.length; i++){
        if(_provBusinessLocation.listBusinessField[i].id == "04"){
          businessLocationModelSelected = _provBusinessLocation.listBusinessField[i];
          this._controllerBusinessLocation.text = "${_provBusinessLocation.listBusinessField[i].id} - ${_provBusinessLocation.listBusinessField[i].desc}";
        }
      }
    }
    // businessLocationModelSelected = data;
    // this._controllerBusinessLocation.text = "${data.id} - ${data.desc}";
  }

  void clearForm(){
    this._controllerEstablishedDate.clear();
    this._controllerBusinessName.clear();
    this._controllerTotalEstablishedDate.clear();
    this._controllerEmployeeTotal.clear();
    this._controllerSectorEconomic.clear();
    this._controllerBusinessField.clear();
    this._controllerCompanyName.clear();
    this._controllerJenisPEP.clear();
    this._controllerAddress.clear();
    this._controllerAddressType.clear();
    this._controllerRT.clear();
    this._controllerRW.clear();
    this._controllerKelurahan.clear();
    this._controllerKecamatan.clear();
    this._controllerKota.clear();
    this._controllerProvinsi.clear();
    this._controllerPostalCode.clear();
    this._controllerTelepon1Area.clear();
    this._controllerTelepon1.clear();
    // this._controllerTelepon2Area.clear();
    // this._controllerTelepon2.clear();
    // this._controllerFaxArea.clear();
    // this._controllerFax.clear();
    this._listOccupationAddress.clear();
    this._typeOfBusinessModelSelected = null;
    this._statusLocationModelSelected = null;
    this._businessLocationModelSelected = null;
    this._professionTypeModelSelected = null;
    this._companyTypeModelSelected = null;
    this._employeeStatusModelSelected = null;
    this._sectorEconomicModelSelected = null;
    this._businessFieldModelSelected = null;
    this._pepModelSelected = null;
    this._controllerStatusLocation.clear();
    this._controllerBusinessLocation.clear();
  }

  void moreDialog(BuildContext context, index) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context){
          return Theme(
            data: ThemeData(
                fontFamily: "NunitoSans"
            ),
            child: AlertDialog(
              title: Text("Informasi", style: TextStyle(fontWeight: FontWeight.bold)),
              content: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Row(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Icon(
                          Icons.check,
                          color: Colors.green,
                          size: 22.0,
                        ),
                        SizedBox(width: 12.0),
                        Expanded(
                          child: GestureDetector(
                            onTap: (){
                              selectedIndex = index;
                              setCorrespondenceAddress(listOccupationAddress[index],index);
                              Navigator.pop(context);
                              Navigator.pop(context);
                            },
                            child: Text(
                              "Pilih sebagai Alamat Korespondensi",
                              style: TextStyle(fontSize: 14.0),
                            ),
                          ),
                        )
                      ]
                  ),
                  SizedBox(height: 12.0),
                  listOccupationAddress[index].jenisAlamatModel.KODE != "03"
                      ? listOccupationAddress[index].isSameWithIdentity
                      ? SizedBox()
                      : Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Icon(
                          Icons.delete,
                          color: Colors.red,
                          size: 22.0,
                        ),
                        SizedBox(
                          width: 12.0,
                        ),
                        GestureDetector(
                          onTap: (){
                            Navigator.pop(context);
                            deleteListOccupationAddress(context, index);
                          },
                          child: Text(
                            "Hapus",
                            style: TextStyle(fontSize: 14.0, color: Colors.red),
                          ),
                        )
                      ]
                  )
                      : SizedBox(),
                ],
              ),
              actions: <Widget>[
                FlatButton(
                    onPressed: (){
                      Navigator.pop(context);
                      // _updateStatusShowDialogSimilarity();
                    },
                    child: Text(
                        "CLOSE",
                        style: TextStyle(
                            color: primaryOrange,
                            fontSize: 14,
                            fontWeight: FontWeight.w500,
                            letterSpacing: 1.25
                        )
                    )
                )
              ],
            ),
          );
        }
    );
  }


  bool get loadData => _loadData;

  set loadData(bool value) {
    this._loadData = value;
  }

  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  void getCompanyType() async{
    this._listCompanyType.clear();
    loadData = true;
    try{
      var _result = await GetCompanyType().getCompanyTypeData();
      for(int i=0; i <_result['data'].length; i++){
        this._listCompanyType.add(
            CompanyTypeModel(_result[i]['kode'], _result[i]['deskripsi'])
        );
      }
      loadData = false;
    }
    catch(e){
      loadData = false;
      showSnackBar(e);
    }
    notifyListeners();
  }

  void getLocationStatus() async{
    this._listStatusLocation.clear();
    loadData = true;
    final ioc = new HttpClient();
    ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;

    final _http = IOClient(ioc);

    final _response = await _http.get(
      // "${BaseUrl.unit}api/parameter/get-group-object",
        "https://103.110.89.34/public/ms2dev/jenis-rehab/get_stat_lokasi",
        headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
    );

    if(_response.statusCode == 200){
      final _result = jsonDecode(_response.body);
      final _data = _result['data'];
      for(int i=0; i < _data.length;i++){
        this._listStatusLocation.add(StatusLocationModel(_data[i]['kode'], _data[i]['deskripsi']));
      }
      loadData = false;
    }
    else{
      loadData = false;
      showSnackBar("Error get status lokasi error ${_response.statusCode}");
    }
    notifyListeners();
  }

  // void getBusinessLocation() async{
  //   this._listBusinessLocation.clear();
  //   loadData = true;
  //   final ioc = new HttpClient();
  //   ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
  //
  //   final _http = IOClient(ioc);
  //   String urlPublic = await storage.read(key: "urlPublic");
  //   final _response = await _http.get(
  //     // "${BaseUrl.unit}api/parameter/get-group-object",
  //       "$urlPublic/jenis-rehab/get_lok_usaha",
  //       headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
  //   );
  //
  //   if(_response.statusCode == 200){
  //     final _result = jsonDecode(_response.body);
  //     final _data = _result['data'];
  //     for(int i=0; i < _data.length;i++){
  //       this._listBusinessLocation.add(BusinessLocationModel(_data[i]['kode'], _data[i]['deskripsi']));
  //     }
  //     loadData = false;
  //   }
  //   else{
  //     loadData = false;
  //     showSnackBar("Error get status lokasi error ${_response.statusCode}");
  //   }
  //   notifyListeners();
  // }

  // void getProfessionType() async{
  //   this._listProfessionType.clear();
  //   loadData = true;
  //   final ioc = new HttpClient();
  //   ioc.badCertificateCallback =
  //       (X509Certificate cert, String host, int port) => true;
  //
  //   final _http = IOClient(ioc);
  //
  //   final _response = await _http.get(
  //     // "${BaseUrl.unit}api/parameter/get-group-object",
  //       "https://103.110.89.34/public/ms2dev/jenis-rehab/get_jenis_profesi",
  //       headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
  //   );
  //
  //   if(_response.statusCode == 200){
  //     final _result = jsonDecode(_response.body);
  //     final _data = _result['data'];
  //     for(int i=0; i < _data.length;i++){
  //       this._listProfessionType.add(ProfessionTypeModel(_data[i]['kode'], _data[i]['deskripsi']));
  //     }
  //     loadData = false;
  //   }
  //   else{
  //     loadData = false;
  //     showSnackBar("Error get status lokasi error ${_response.statusCode}");
  //   }
  //   notifyListeners();
  // }

  void showSnackBar(String text){
    this._scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor));
  }

  // Future<void> clearDataOccupation() async {
  void clearDataOccupation() {
    isDisablePACIAAOSCONA = false;
    this._autoValidate = false;
    this._occupationSelected = null;
    this._controllerEstablishedDate.clear();
    // this._controllerBusinessName.clear();
    // this._typeOfBusinessModelSelected = null;
    this._controllerSectorEconomic.clear();
    // this._controllerBusinessField.clear();
    this._statusLocationModelSelected = null;
    this._businessLocationModelSelected = null;
    this._controllerStatusLocation.clear();
    this._controllerBusinessLocation.clear();
    this._controllerEmployeeTotal.clear();
    this._controllerTotalEstablishedDate.clear();
    // this._professionTypeModelSelected = null;
    this._businessLocationModelSelected = null;
    // Pekerjaan Lainnya
    this._controllerCompanyName.clear();
    this._controllerJenisPEP.clear();
    this._employeeStatusModelSelected = null;
    // Alamat
    this._selectedIndex = -1;
    this._listOccupationAddress = [];
    this._controllerAddressType.clear();
    this._controllerAddress.clear();
    this._controllerRT.clear();
    this._controllerRW.clear();
    this._controllerKelurahan.clear();
    this._controllerKecamatan.clear();
    this._controllerKota.clear();
    this._controllerProvinsi.clear();
    this._controllerPostalCode.clear();
    this._controllerTelepon1Area.clear();
    this._controllerTelepon1.clear();
    // this._controllerTelepon2Area.clear();
    // this._controllerTelepon2.clear();
    // this._controllerFaxArea.clear();
    // this._controllerFax.clear();
  }

  void saveToSQLite(BuildContext context, String type){
    var occupation = Provider.of<FormMFotoChangeNotifier>(context, listen: false);
    // _dbHelper.insertMS2CustIndOccupation(MS2CustIndOccupationModel("123", occupation.occupationSelected.KODE,
    //     occupation.occupationSelected.DESKRIPSI, _controllerBusinessName.text, _controllerBusinessField.text,
    //     _pepModelSelected.KODE, _pepModelSelected.DESKRIPSI, _employeeStatusModelSelected.id, _employeeStatusModelSelected.desc,
    //     _professionTypeModelSelected.id, _professionTypeModelSelected.desc, _sectorEconomicModelSelected.KODE,
    //     _sectorEconomicModelSelected.DESKRIPSI, null, null, _statusLocationModelSelected.id, _statusLocationModelSelected.desc,
    //     _businessLocationModelSelected.id, _businessLocationModelSelected.desc, int.parse(_controllerEmployeeTotal.text),
    //     int.parse(_controllerEstablishedDate.text), int.parse(_controllerTotalEstablishedDate.text),
    //     null, null, null, null, 1, null, null, null));
    print(occupation.occupationSelected.KODE);
    if(occupation.occupationSelected.KODE == "05" || occupation.occupationSelected.KODE == "07"){
      print("data A");
      _dbHelper.insertMS2CustIndOccupation(
          MS2CustIndOccupationModel(
              "123",
              _customerOccupationID,
              occupation.occupationSelected != null ? occupation.occupationSelected.KODE :"",
              occupation.occupationSelected != null ? occupation.occupationSelected.DESKRIPSI:"",
              this._controllerBusinessName.text,
              this._typeOfBusinessModelSelected != null ? this._typeOfBusinessModelSelected.id : "",
              "",
              "",
              "",
              "",
              _professionTypeModelSelected != null ? professionTypeModelSelected.id : "",
              _professionTypeModelSelected != null ? professionTypeModelSelected.desc : "",
              _sectorEconomicModelSelected != null ? _sectorEconomicModelSelected.KODE:"",
              _sectorEconomicModelSelected != null ? _sectorEconomicModelSelected.DESKRIPSI:"",
              _businessFieldModelSelected != null ? _businessFieldModelSelected.KODE : "",
              _businessFieldModelSelected != null ? _businessFieldModelSelected.DESKRIPSI : "",
              this._statusLocationModelSelected != null ? this._statusLocationModelSelected.id : "",
              this._statusLocationModelSelected != null ? this._statusLocationModelSelected.desc:"",
              _businessLocationModelSelected != null ? _businessLocationModelSelected.id : "",
              _businessLocationModelSelected != null ? _businessLocationModelSelected.desc : "",
              _controllerEmployeeTotal.text != "" ? int.parse(_controllerEmployeeTotal.text):null,
              _controllerEstablishedDate.text != "" ? int.parse(_controllerEstablishedDate.text):null,
              _controllerTotalEstablishedDate.text != "" ?  int.parse(_controllerTotalEstablishedDate.text):null,
              "",
              "",
              "",
              "",
              1,
              "",
              "",
              null,
              editOccupation ? "1" : "0",
              editNamaUsahaWiraswasta ? "1" : "0",
              editJenisBadanUsahaWiraswasta ? "1" : "0",
              "0",
              "0",
              "0",
              editSektorEkonomiWiraswasta ? "1": "0",
              editLapanganUsahaWiraswasta ? "1": "0",
              editStatusLokasiWiraswasta ? "1": "0",
              editLokasiUsahaWiraswasta ? "1": "0",
              editTotalPegawaiWiraswasta ? "1": "0",
              editLamaBekerjaBerjalan ? "1": "0",
              editTotalLamaBekerjaWiraswasta ? "1" : "0",
              "0",
              "0",
              "0",
          )
      );
    }
    else if(occupation.occupationSelected.KODE == "08"){
      print("data B");
      _dbHelper.insertMS2CustIndOccupation(
          MS2CustIndOccupationModel(
              "123",
              _customerOccupationID,
              occupation.occupationSelected != null ? occupation.occupationSelected.KODE : "",
              occupation.occupationSelected != null ? occupation.occupationSelected.DESKRIPSI : "",
             "",
             "",
             "",
             "",
             "",
             "",
              _professionTypeModelSelected != null ? professionTypeModelSelected.id : "",
              _professionTypeModelSelected != null ? professionTypeModelSelected.desc : "",
              _sectorEconomicModelSelected != null ? _sectorEconomicModelSelected.KODE : "",
              _sectorEconomicModelSelected != null ? _sectorEconomicModelSelected.DESKRIPSI : "",
              _businessFieldModelSelected != null ? _businessFieldModelSelected.KODE : "",
              _businessFieldModelSelected != null ? _businessFieldModelSelected.DESKRIPSI : "",
              _statusLocationModelSelected != null ? _statusLocationModelSelected.id : "",
              _statusLocationModelSelected != null ? _statusLocationModelSelected.desc : "",
              _businessLocationModelSelected != null ? _businessLocationModelSelected.id : "",
              _businessLocationModelSelected != null ? _businessLocationModelSelected.desc : "",
              null,
              _controllerEstablishedDate.text != "" ? int.parse(_controllerEstablishedDate.text) : null,
              _controllerTotalEstablishedDate.text != "" ? int.parse(_controllerTotalEstablishedDate.text) : null,
              "",
              "",
              "",
              "",
              1,
              "",
              "",
              null,
              editOccupation ? "1" : "0",
              "0",
              "0",
              "0",
              "0",
              editJenisProfesiProfesional ? "1" : "0",
              editSektorEkonomiProfesional ? "1": "0",
              editLapanganUsahaProfesional ? "1": "0",
              editStatusLokasiProfesional ? "1": "0",
              editLokasiUsahaProfesional ? "1": "0",
              "0",
              editLamaBekerjaBerjalan ? "1": "0",
              editTotalLamaBekerjaProfesional ? "1" : "0",
              "0",
              "0",
              "0",
          )
      );
    }
    else {
      print("data C");
      _dbHelper.insertMS2CustIndOccupation(
          MS2CustIndOccupationModel(
              "123",
              _customerOccupationID,
              occupation.occupationSelected != null ? occupation.occupationSelected.KODE : "",
              occupation.occupationSelected != null ? occupation.occupationSelected.DESKRIPSI : "",
              this._controllerCompanyName.text,
              this._companyTypeModelSelected != null ? _companyTypeModelSelected.id : "",
              _pepModelSelected != null ? _pepModelSelected.KODE : "",
              _pepModelSelected != null ? _pepModelSelected.DESKRIPSI : "",
              _employeeStatusModelSelected != null ? _employeeStatusModelSelected.id : "",
              _employeeStatusModelSelected != null ? _employeeStatusModelSelected.desc : "",
              _professionTypeModelSelected != null ? professionTypeModelSelected.id : "",
              _professionTypeModelSelected != null ? professionTypeModelSelected.desc : "",
              _sectorEconomicModelSelected != null ? _sectorEconomicModelSelected.KODE:"",
              _sectorEconomicModelSelected != null ? _sectorEconomicModelSelected.DESKRIPSI:"",
              _businessFieldModelSelected != null ? _businessFieldModelSelected.KODE : "",
              _businessFieldModelSelected != null ? _businessFieldModelSelected.DESKRIPSI : "",
              this._statusLocationModelSelected != null ? this._statusLocationModelSelected.id : "",
              this._statusLocationModelSelected != null ? this._statusLocationModelSelected.desc:"",
              _businessLocationModelSelected != null ? _businessLocationModelSelected.id : "",
              _businessLocationModelSelected != null ? _businessLocationModelSelected.desc : "",
              _controllerEmployeeTotal.text != "" ? int.parse(_controllerEmployeeTotal.text):null,
              _controllerEstablishedDate.text != "" ? int.parse(_controllerEstablishedDate.text) : null,
              _controllerTotalEstablishedDate.text != "" ? int.parse(_controllerTotalEstablishedDate.text) : null,
              "",
              "",
              "",
              "",
              1,
              "",
              "",
              null,
              editOccupation ? "1" : "0",
              editNamaPerusahaanLainnya ? "1" : "0",
              editJenisPerusahaanLainnya ? "1" : "0",
              editJenisPepHighRiskLainnya ? "1" : "0",
              editStatusPegawaiLainnya ? "1" : "0",
              "0",
              editSektorEkonomiLainnya ? "1": "0",
              editLapanganUsahaLainnya ? "1": "0",
              "0",
              "0",
              editTotalPegawaiLainnya ? "1" : "0",
              editLamaBekerjaBerjalan ? "1": "0",
              editTotalLamaBekerjaLainnya ? "1" : "0",
              "0",
              "0",
              "0",
          )
      );
    }

    List<MS2CustAddrModel> _listAddress = [];
    for(int i=0; i<_listOccupationAddress.length; i++){
      _listAddress.add(MS2CustAddrModel(
          null,
          _listOccupationAddress[i].addressID,
          _listOccupationAddress[i].foreignBusinessID,
          null,
          _listOccupationAddress[i].isCorrespondence ? "1" : "0",
          type,
          _listOccupationAddress[i].address,
          _listOccupationAddress[i].rt,
          null,
          _listOccupationAddress[i].rw,
          null,
          _listOccupationAddress[i].kelurahanModel.PROV_ID,
          _listOccupationAddress[i].kelurahanModel.PROV_NAME,
          _listOccupationAddress[i].kelurahanModel.KABKOT_ID,
          _listOccupationAddress[i].kelurahanModel.KABKOT_NAME,
          _listOccupationAddress[i].kelurahanModel.KEC_ID,
          _listOccupationAddress[i].kelurahanModel.KEC_NAME,
          _listOccupationAddress[i].kelurahanModel.KEL_ID,
          _listOccupationAddress[i].kelurahanModel.KEL_NAME,
          _listOccupationAddress[i].kelurahanModel.ZIPCODE,
          null,
          _listOccupationAddress[i].phone,
          _listOccupationAddress[i].areaCode,
          null,
          null,
          null,
          null,
          _listOccupationAddress[i].jenisAlamatModel.KODE,
          _listOccupationAddress[i].jenisAlamatModel.DESKRIPSI,
          _listOccupationAddress[i].addressLatLong != null ? _listOccupationAddress[i].addressLatLong['latitude'].toString():null,
          _listOccupationAddress[i].addressLatLong != null ? _listOccupationAddress[i].addressLatLong['longitude'].toString():null,
          _listOccupationAddress[i].addressLatLong != null ? _listOccupationAddress[i].addressLatLong['address'].toString():null,
          null, null, null, null,
          _listOccupationAddress[i].active,
          null,
          null,
          null,
          null,
          null,
          null,
          null,
          null,
          null,
          null,
          null,
          null,
          null,
          null,
          null,
          null));

      // _dbHelper.insertMS2CustAddr(MS2CustAddrModel("123", _listOccupationAddress[i].isCorrespondence.toString(), type,
      //     _listOccupationAddress[i].address, _listOccupationAddress[i].rt, null, _listOccupationAddress[i].rw, null,
      //     _listOccupationAddress[i].kelurahanModel.PROV_ID, _listOccupationAddress[i].kelurahanModel.PROV_NAME,
      //     _listOccupationAddress[i].kelurahanModel.KABKOT_ID, _listOccupationAddress[i].kelurahanModel.KABKOT_NAME,
      //     _listOccupationAddress[i].kelurahanModel.KEC_ID, _listOccupationAddress[i].kelurahanModel.KEC_NAME,
      //     _listOccupationAddress[i].kelurahanModel.KEL_ID, _listOccupationAddress[i].kelurahanModel.KEL_NAME,
      //     _listOccupationAddress[i].kelurahanModel.ZIPCODE, null, _listOccupationAddress[i].phone, _listOccupationAddress[i].areaCode,
      //     null, null, null, null, _listOccupationAddress[i].jenisAlamatModel.KODE,
      //     _listOccupationAddress[i].jenisAlamatModel.DESKRIPSI, _listOccupationAddress[i].addressLatLong['latitude'].toString(),
      //     _listOccupationAddress[i].addressLatLong['longitude'].toString(),
      //     _listOccupationAddress[i].addressLatLong['address'].toString(), null, null, null, null, 1));
    }
    _dbHelper.insertMS2CustAddr(_listAddress);
  }

  Future<bool> deleteSQLite() async{
    return await _dbHelper.deleteMS2CustIndOccupation();
  }

  Future<void> setDataFromSQLite(BuildContext context,String type,int index) async{
    //pekerjaan
    List _occupation = await _dbHelper.selectMS2CustIndOccupation();
    var _providerFoto = Provider.of<FormMFotoChangeNotifier>(context,listen: false);
    if(_occupation.isNotEmpty){
      debugPrint("customerOccupationID ${_occupation[0]['customerOccupationID']}");
      this._customerOccupationID = _occupation[0]['customerOccupationID'];
      this._occupationSelected = OccupationModel(_occupation[0]['occupation'], _occupation[0]['occupation_desc']);
      this._controllerEstablishedDate.text =_occupation[0]['no_of_year_work'].toString();
      // this._controllerBusinessName.clear();
      // this._typeOfBusinessModelSelected = null;
      if(_occupation[0]['profession_type'] != "" && _occupation[0]['profession_type'] != "null"){
        this._controllerProfessionType.text = "${_occupation[0]['profession_type']} - ${_occupation[0]['profession_desc']}";
        this._professionTypeModelSelected = ProfessionTypeModel(_occupation[0]['profession_type'], _occupation[0]['profession_desc']);
      }

      if(_occupation[0]['sector_economic'] != "" && _occupation[0]['sector_economic'] != "null"){
        this._controllerSectorEconomic.text = "${_occupation[0]['sector_economic']} - ${_occupation[0]['sector_economic_desc']}";
        this._sectorEconomicModelSelected = SectorEconomicModel(_occupation[0]['sector_economic'], _occupation[0]['sector_economic_desc']);
      }

      if(_occupation[0]['nature_of_buss'] != "" && _occupation[0]['nature_of_buss'] != "null"){
        this._businessFieldModelSelected = BusinessFieldModel(_occupation[0]['nature_of_buss'], _occupation[0]['nature_of_buss_desc']);
        this._controllerBusinessField.text = "${_occupation[0]['nature_of_buss']} - ${_occupation[0]['nature_of_buss_desc']}";
      }

      debugPrint("CHECK LOCATION STATUS OCCUPATION ${_occupation[0]['location_status']}");
      if(_occupation[0]['location_status'] != "" && _occupation[0]['location_status'] != "null"){
        this._controllerStatusLocation.text = "${_occupation[0]['location_status']} - ${_occupation[0]['location_status_desc']}";
        this._statusLocationModelSelected = StatusLocationModel(_occupation[0]['location_status'], _occupation[0]['location_status_desc']);
      }
      // this._controllerBusinessField.clear();
      // this._controllerEmployeeTotal.clear();
      _occupation[0]['no_of_year_buss'] != "null" ? this._controllerTotalEstablishedDate.text = _occupation[0]['no_of_year_buss'].toString() : this._controllerTotalEstablishedDate.text = "";
      _occupation[0]['total_emp'] != "null" ? this._controllerEmployeeTotal.text = _occupation[0]['total_emp'].toString() : this._controllerEmployeeTotal.text = "";
      // this._professionTypeModelSelected = null;
      if(_occupation[0]['buss_location'] != "" && _occupation[0]['buss_location'] != "null") {
        this._controllerBusinessLocation.text = "${_occupation[0]['buss_location']} - ${_occupation[0]['buss_location_desc']}";
        this._businessLocationModelSelected = BusinessLocationModel(_occupation[0]['buss_location'], _occupation[0]['buss_location_desc']);
      }
      // Pekerjaan Lainnya
      // this._controllerCompanyName.clear();
      if(_occupation[0]['pep_type'] != "" && _occupation[0]['pep_type'] != "null") {
        this._controllerJenisPEP.text = "${_occupation[0]['pep_type']} - ${_occupation[0]['pep_desc']}";
        this._pepModelSelected = PEPModel(_occupation[0]['pep_type'], _occupation[0]['pep_desc']);
      }
      if(_occupation[0]['emp_status'] != "" && _occupation[0]['emp_status'] != "null") {
        for(int i=0; i<_listEmployeeStatus.length; i++){
          if(_listEmployeeStatus[i].id == _occupation[0]['emp_status']){
            this._employeeStatusModelSelected = _listEmployeeStatus[i];
          }
        }
        // this._employeeStatusModelSelected = EmployeeStatusModel(_occupation[0]['emp_status'], _occupation[0]['emp_status_desc']);
      }
      if(_providerFoto.occupationSelected.KODE == "05" || _providerFoto.occupationSelected.KODE == "07"){
        this._controllerBusinessName.text = _occupation[0]['buss_name'];
      }
      else if(_providerFoto.occupationSelected.KODE != "08"){
        this._controllerCompanyName.text = _occupation[0]['buss_name'];
      }

      if(_providerFoto.occupationSelected.KODE == "05" || _providerFoto.occupationSelected.KODE == "07"){
        for(int i=0; i < this._listCompanyType.length; i++){
          if(_occupation[0]['buss_type'] == this._listTypeOfBusiness[i].id){
            this._typeOfBusinessModelSelected = this._listTypeOfBusiness[i];
          }
        }
      }
      else if(_providerFoto.occupationSelected.KODE != "08"){
        for(int i=0; i < this._listCompanyType.length; i++){
          if(_occupation[0]['buss_type'] == this._listCompanyType[i].id){
            this._companyTypeModelSelected = this._listCompanyType[i];
          }
        }
      }
    }
    //alamat
    var _check = await _dbHelper.selectMS2CustAddr(type);
    if(_check.isNotEmpty){
      for(int i=0; i<_check.length; i++){
        var jenisAlamatModel = JenisAlamatModel(_check[i]['addr_type'], _check[i]['addr_desc']);
        var kelurahanModel = KelurahanModel(
            _check[i]['kelurahan'],
            _check[i]['kelurahan_desc'],
            _check[i]['kecamatan_desc'],
            _check[i]['kabkot_desc'],
            _check[i]['provinsi_desc'],
            _check[i]['zip_code'],
            _check[i]['kecamatan'],
            _check[i]['kabkot'],
            _check[i]['provinsi']);
        var _addressFromMap = {
          "address": _check[i]['address_from_map'] != "" && _check[i]['address_from_map'] != "null" ? _check[i]['address_from_map'] : '',
          "latitude": _check[i]['latitude'],
          "longitude": _check[i]['longitude']
        };
        debugPrint("cek addressID occupation ${_check[i]['addressID']}");
        var _koresponden = _check[i]['koresponden'].toString().toLowerCase();
        _listOccupationAddress.add(AddressModel(
            jenisAlamatModel,
            _check[i]['addressID'],
            _check[i]['foreignBusinessID'],
            null,
            kelurahanModel,
            _check[i]['address'] != "" && _check[i]['address'] != "null" ? _check[i]['address'] : '',
            _check[i]['rt'] != "" && _check[i]['rt'] != "null" ? _check[i]['rt'] : '',
            _check[i]['rw'] != "" && _check[i]['rw'] != "null" ? _check[i]['rw'] : '',
            _check[i]['phone1_area'] != "" && _check[i]['phone1_area'] != "null" ? _check[i]['phone1_area'] : '',
            _check[i]['phone1'] != "" && _check[i]['phone1'] != "null" ? _check[i]['phone1'] : '',
            false,
            _addressFromMap,
            _koresponden == '1' ? true : false,
            _check[i]['active'],
            _check[i]['edit_address'] == "1" ||
            _check[i]['edit_addr_type'] == "1" ||
            _check[i]['edit_rt'] == "1" ||
            _check[i]['edit_rw'] == "1" ||
            _check[i]['edit_kelurahan'] == "1" ||
            _check[i]['edit_kecamatan'] == "1" ||
            _check[i]['edit_kabkot'] == "1" ||
            _check[i]['edit_provinsi'] == "1" ||
            _check[i]['edit_zip_code'] == "1" ||
            _check[i]['edit_address_from_map'] == "1" ||
            _check[i]['edit_phone1_area'] == "1" ||
            _check[i]['edit_phone1'] == "1" ? true : false,
            _check[i]['edit_address'] == "1",
            _check[i]['edit_addr_type'] == "1",
            _check[i]['edit_rt'] == "1",
            _check[i]['edit_rw'] == "1",
            _check[i]['edit_kelurahan'] == "1",
            _check[i]['edit_kecamatan'] == "1",
            _check[i]['edit_kabkot'] == "1",
            _check[i]['edit_provinsi'] == "1",
            _check[i]['edit_zip_code'] == "1",
            _check[i]['edit_address_from_map'] == "1",
            _check[i]['edit_phone1_area'] == "1",
            _check[i]['edit_phone1'] == "1",
        ));
        if(_check[i]['koresponden'] == "1"){
          selectedIndex = i;
          setCorrespondenceAddress(this._listOccupationAddress[i],i);
        }
      }
    }

    if(_lastKnownState == "IDE" && index != null){
      if(index != 2){
        var _providerIncome = Provider.of<FormMIncomeChangeNotifier>(context, listen: false);
        Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).isOccupationDone = true;
        Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).selectedIndex +=1;
        await _providerIncome.setDataFromSQLite(context, index);
      }
      else{
        Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).selectedIndex = 2;
      }
    }
    await checkDataDakor();
    notifyListeners();
  }

  bool get editStatusPegawaiLainnya => _editStatusPegawaiLainnya;

  set editStatusPegawaiLainnya(bool value) {
    this._editStatusPegawaiLainnya = value;
  }

  bool get editJenisPepHighRiskLainnya => _editJenisPepHighRiskLainnya;

  set editJenisPepHighRiskLainnya(bool value) {
    this._editJenisPepHighRiskLainnya = value;
  }

  bool get editTotalLamaBekerjaLainnya => _editTotalLamaBekerjaLainnya;

  set editTotalLamaBekerjaLainnya(bool value) {
    this._editTotalLamaBekerjaLainnya = value;
  }

  bool get editTotalPegawaiLainnya => _editTotalPegawaiLainnya;

  set editTotalPegawaiLainnya(bool value) {
    this._editTotalPegawaiLainnya = value;
  }

  bool get editLapanganUsahaLainnya => _editLapanganUsahaLainnya;

  set editLapanganUsahaLainnya(bool value) {
    this._editLapanganUsahaLainnya = value;
  }

  bool get editSektorEkonomiLainnya => _editSektorEkonomiLainnya;

  set editSektorEkonomiLainnya(bool value) {
    this._editSektorEkonomiLainnya = value;
  }

  bool get editJenisPerusahaanLainnya => _editJenisPerusahaanLainnya;

  set editJenisPerusahaanLainnya(bool value) {
    this._editJenisPerusahaanLainnya = value;
  }

  bool get editNamaPerusahaanLainnya => _editNamaPerusahaanLainnya;

  set editNamaPerusahaanLainnya(bool value) {
    this._editNamaPerusahaanLainnya = value;
  }

  bool get editTotalLamaBekerjaProfesional => _editTotalLamaBekerjaProfesional;

  set editTotalLamaBekerjaProfesional(bool value) {
    this._editTotalLamaBekerjaProfesional = value;
  }

  bool get editLokasiUsahaProfesional => _editLokasiUsahaProfesional;

  set editLokasiUsahaProfesional(bool value) {
    this._editLokasiUsahaProfesional = value;
  }

  bool get editStatusLokasiProfesional => _editStatusLokasiProfesional;

  set editStatusLokasiProfesional(bool value) {
    this._editStatusLokasiProfesional = value;
  }

  bool get editLapanganUsahaProfesional => _editLapanganUsahaProfesional;

  set editLapanganUsahaProfesional(bool value) {
    this._editLapanganUsahaProfesional = value;
  }

  bool get editSektorEkonomiProfesional => _editSektorEkonomiProfesional;

  set editSektorEkonomiProfesional(bool value) {
    this._editSektorEkonomiProfesional = value;
  }

  bool get editJenisProfesiProfesional => _editJenisProfesiProfesional;

  set editJenisProfesiProfesional(bool value) {
    this._editJenisProfesiProfesional = value;
  }

  bool get editTotalLamaBekerjaWiraswasta => _editTotalLamaBekerjaWiraswasta;

  set editTotalLamaBekerjaWiraswasta(bool value) {
    this._editTotalLamaBekerjaWiraswasta = value;
  }

  bool get editTotalPegawaiWiraswasta => _editTotalPegawaiWiraswasta;

  set editTotalPegawaiWiraswasta(bool value) {
    this._editTotalPegawaiWiraswasta = value;
  }

  bool get editLokasiUsahaWiraswasta => _editLokasiUsahaWiraswasta;

  set editLokasiUsahaWiraswasta(bool value) {
    this._editLokasiUsahaWiraswasta = value;
  }

  bool get editStatusLokasiWiraswasta => _editStatusLokasiWiraswasta;

  set editStatusLokasiWiraswasta(bool value) {
    this._editStatusLokasiWiraswasta = value;
  }

  bool get editLapanganUsahaWiraswasta => _editLapanganUsahaWiraswasta;

  set editLapanganUsahaWiraswasta(bool value) {
    this._editLapanganUsahaWiraswasta = value;
  }

  bool get editSektorEkonomiWiraswasta => _editSektorEkonomiWiraswasta;

  set editSektorEkonomiWiraswasta(bool value) {
    this._editSektorEkonomiWiraswasta = value;
  }

  bool get editJenisBadanUsahaWiraswasta => _editJenisBadanUsahaWiraswasta;

  set editJenisBadanUsahaWiraswasta(bool value) {
    this._editJenisBadanUsahaWiraswasta = value;
  }

  bool get editNamaUsahaWiraswasta => _editNamaUsahaWiraswasta;

  set editNamaUsahaWiraswasta(bool value) {
    this._editNamaUsahaWiraswasta = value;
  }


  bool get editOccupation => _editOccupation;

  set editOccupation(bool value) {
    this._editOccupation = value;
  }

  bool get editLamaBekerjaBerjalan => _editLamaBekerjaBerjalan;

  set editLamaBekerjaBerjalan(bool value) {
    this._editLamaBekerjaBerjalan = value;
  }

  String _custType;
  String _lastKnownState;

  String get custType => _custType;

  set custType(String value) {
    this._custType = value;
    notifyListeners();
  }

  String get lastKnownState => _lastKnownState;

  set lastKnownState(String value) {
    this._lastKnownState = value;
    notifyListeners();
  }

  Future<void> setPreference(BuildContext context) async {
    this._controllerOccupation.text = Provider.of<FormMFotoChangeNotifier>(context,listen: false).occupationSelected.DESKRIPSI;
    SharedPreferences _preference = await SharedPreferences.getInstance();
    this._custType = _preference.getString("cust_type");
    this._lastKnownState = _preference.getString("last_known_state");
    if(this._lastKnownState == "AOS" || this._lastKnownState == "CONA" || this._lastKnownState == "IA" || this._lastKnownState == "PAC") {
      this._isDisablePACIAAOSCONA = true;
    }
    setShowMandatoryOccupationModel(context);
  }

  Future<void> checkDataDakor() async{
    List _occupation = await _dbHelper.selectMS2CustIndOccupation();
    if(_occupation.isNotEmpty && _lastKnownState == "DKR"){
      editLamaBekerjaBerjalan = this._controllerEstablishedDate.text !=_occupation[0]['no_of_year_work'].toString() || _occupation[0]['edit_no_of_year_work'].toString() == "1";
      editNamaUsahaWiraswasta =  this._controllerBusinessName.text !=_occupation[0]['buss_name'].toString() || _occupation[0]['edit_buss_name'].toString() == "1";
      editJenisBadanUsahaWiraswasta = this._typeOfBusinessModelSelected != null ? this._typeOfBusinessModelSelected.id != _occupation[0][''].toString() : false|| _occupation[0][''].toString() == "1";// perlu penambahan field di db karena ini dropdown
      editSektorEkonomiWiraswasta = this._sectorEconomicModelSelected.KODE != _occupation[0]['sector_economic'].toString() || _occupation[0]['edit_sector_economic'].toString() == "1";
      editLapanganUsahaWiraswasta = this._businessFieldModelSelected != null ? this._businessFieldModelSelected.KODE != _occupation[0]['nature_of_buss'].toString() : false || _occupation[0]['edit_nature_of_buss'].toString() == "1";
      editStatusLokasiWiraswasta = this._statusLocationModelSelected != null ? this._statusLocationModelSelected.id != _occupation[0]['location_status'].toString() : false || _occupation[0]['edit_location_status'].toString() == "1";
      editLokasiUsahaWiraswasta = this._businessLocationModelSelected != null ? this._businessLocationModelSelected.id != _occupation[0]['buss_location'].toString() : false || _occupation[0]['edit_buss_location '].toString() == "1";
      editTotalPegawaiWiraswasta = this._controllerEmployeeTotal.text != _occupation[0]['total_emp'].toString() || _occupation[0]['edit_total_emp '].toString() == "1";
      editTotalLamaBekerjaWiraswasta = this._controllerTotalEstablishedDate.text != _occupation[0]['no_of_year_work'].toString() || _occupation[0]['edit_total_emp '].toString() == "1";
      editJenisProfesiProfesional = this._professionTypeModelSelected != null ? this._professionTypeModelSelected.id != _occupation[0]['profession_type'].toString() : false || _occupation[0]['edit_profession_type '].toString() == "1";
      editSektorEkonomiProfesional = this._sectorEconomicModelSelected.KODE != _occupation[0]['sector_economic'].toString() || _occupation[0]['edit_sector_economic'].toString() == "1";
      editLapanganUsahaProfesional = this._businessFieldModelSelected != null ? this._businessFieldModelSelected.KODE != _occupation[0]['nature_of_buss'].toString() : false || _occupation[0]['edit_nature_of_buss'].toString() == "1";
      editStatusLokasiProfesional = this._statusLocationModelSelected != null ? this._statusLocationModelSelected.id != _occupation[0]['location_status'].toString() : false || _occupation[0]['edit_location_status'].toString() == "1";
      editLokasiUsahaProfesional = this._businessLocationModelSelected != null ? this._businessLocationModelSelected.id != _occupation[0]['buss_location'].toString() : false || _occupation[0]['edit_buss_location '].toString() == "1";
      editTotalLamaBekerjaProfesional = this._controllerTotalEstablishedDate.text != _occupation[0]['no_of_year_work'].toString() || _occupation[0]['edit_total_emp '].toString() == "1";
      editNamaPerusahaanLainnya = this._controllerBusinessName.text !=_occupation[0]['buss_name'].toString() || _occupation[0]['edit_buss_name'].toString() == "1";
      editJenisPerusahaanLainnya = this._typeOfBusinessModelSelected != null ? this._typeOfBusinessModelSelected.id != _occupation[0][''].toString() : false|| _occupation[0][''].toString() == "1";// perlu penambahan field di db karena ini dropdown
      editSektorEkonomiLainnya = this._sectorEconomicModelSelected.KODE != _occupation[0]['sector_economic'].toString() || _occupation[0]['edit_sector_economic'].toString() == "1";
      editLapanganUsahaLainnya = this._businessFieldModelSelected != null ? this._businessFieldModelSelected.KODE != _occupation[0]['nature_of_buss'].toString() : false || _occupation[0]['edit_nature_of_buss'].toString() == "1";
      editTotalPegawaiLainnya = this._controllerEmployeeTotal.text != _occupation[0]['total_emp'].toString() || _occupation[0]['edit_total_emp '].toString() == "1";
      editTotalLamaBekerjaLainnya = this._controllerTotalEstablishedDate.text != _occupation[0]['no_of_year_work'].toString() || _occupation[0]['edit_total_emp '].toString() == "1";
      editJenisPepHighRiskLainnya = this._pepModelSelected != null ? this._pepModelSelected.KODE != _occupation[0]['pep_type'].toString() : false || _occupation[0]['edit_pep_type'].toString() == "1";
      editStatusPegawaiLainnya = this._employeeStatusModelSelected != null ? this._employeeStatusModelSelected.id != _occupation[0]['emp_status'].toString() : false || _occupation[0]['edit_emp_status'].toString() == "1";
    }
  }
}
