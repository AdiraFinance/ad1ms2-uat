import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/source_order_model.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_object_unit_change_notifier.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/io_client.dart';
import 'package:provider/provider.dart';

class SearchSourceOrderChangeNotifier with ChangeNotifier {
  bool _showClear = false;
  TextEditingController _controllerSearch = TextEditingController();
  bool _loadData = false;
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  List<SourceOrderModel> _listSourceOrder = [
    // SourceOrderModel("001", "DEALER"),
    // SourceOrderModel("002", "MERCHANT"),
    // SourceOrderModel("003", "MITRA MAXI PLUS UMRAH"),
    // SourceOrderModel("004", "DIGITAL CHANNEL"),
    // SourceOrderModel("005", "RETAIL CHANNEL"),
    // SourceOrderModel("007", "KOPERASI"),
    // SourceOrderModel("009", "KEDAY"),
    // SourceOrderModel("010", "KARYAWAN"),
    // SourceOrderModel("011", "SPG"),
  ];
  List<SourceOrderModel> _listSourceOrderTemp = [];

  bool get loadData => _loadData;

  set loadData(bool value) {
    this._loadData = value;
  }

  TextEditingController get controllerSearch => _controllerSearch;

  bool get showClear => _showClear;

  set showClear(bool value) {
    this._showClear = value;
    notifyListeners();
  }

  void changeAction(String value) {
    if (value != "") {
      showClear = true;
    } else {
      showClear = false;
    }
  }

  UnmodifiableListView<SourceOrderModel> get listSourceOrder {
    return UnmodifiableListView(this._listSourceOrder);
  }

  UnmodifiableListView<SourceOrderModel> get listSourceOrderTemp {
    return UnmodifiableListView(this._listSourceOrderTemp);
  }

  void getSourceOrder(BuildContext context) async{
    var _providerObjectUnit = Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false);
    this._listSourceOrder.clear();
    loadData = true;
    final ioc = new HttpClient();
    ioc.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;

    final _http = IOClient(ioc);

    var _body = jsonEncode({
      "refOne": "${_providerObjectUnit.prodMatrixId}"
    });
    print("sumber order = $_body");

    var storage = FlutterSecureStorage();
    String _fieldSumberOrder = await storage.read(key: "FieldSumberOrder");
    // print("${BaseUrl.urlGeneral}$_fieldSumberOrder");
    final _response = await _http.post(
      "${BaseUrl.urlGeneral}$_fieldSumberOrder",
      // "${urlPublic}pihak-ketiga/get_sumber_order",
      body: _body,
      headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
    );
    if(_response.statusCode == 200){
      final _result = jsonDecode(_response.body);
      if(_result.isEmpty){
        showSnackBar("Sumber order tidak ditemukan");
        loadData = false;
      }
      else{
        for(int i=0; i <_result.length; i++){
          this._listSourceOrder.add(
              SourceOrderModel(_result[i]['kode'], _result[i]['deskripsi'])
          );
        }
        loadData = false;
      }
    }
    else{
      showSnackBar("Error response status ${_response.statusCode}");
      this._loadData = false;
    }
    notifyListeners();
  }

  void showSnackBar(String text){
    this._scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor));
  }

  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  void searchSourceOrder(String query) {
    if(query.length < 3) {
      showSnackBar("Input minimal 3 karakter");
    } else {
      _listSourceOrderTemp.clear();
      if (query.isEmpty) {
        return;
      }

      _listSourceOrder.forEach((dataSourceOrder) {
        if (dataSourceOrder.kode.contains(query) || dataSourceOrder.deskripsi.contains(query)) {
          _listSourceOrderTemp.add(dataSourceOrder);
        }
      });
    }
    notifyListeners();
  }

  void clearSearchTemp() {
    _listSourceOrderTemp.clear();
  }
}
