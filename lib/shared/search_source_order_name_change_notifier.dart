import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/source_order_name_model.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_object_unit_change_notifier.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/io_client.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SearchSourceOrderNameChangeNotifier with ChangeNotifier {
  bool _showClear = false;
  bool _loadData = false;
  TextEditingController _controllerSearch = TextEditingController();
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;
  List<SourceOrderNameModel> _listSourceOrderName = [
//    SourceOrderNameModel("07040000", "MAKASSAR DRB"),
//    SourceOrderNameModel("07050000", "KENDARI-AHMAD YANI"),
//    SourceOrderNameModel("07060000", "PALU-EMMY SAELAN"),
//    SourceOrderNameModel("07070000", "MANADO-AHMAD YANI"),
//    SourceOrderNameModel("07080000", "GORONTALO-NANI WARTABONE"),
//    SourceOrderNameModel("07090000", "KOTAMUBAGU"),
//    SourceOrderNameModel("07120000", "MAKASSAR 2 CAR-PANAKUKANG"),
//    SourceOrderNameModel("07160000", "SENGKANG-WAJO-PANGGARU"),
//    SourceOrderNameModel("07200000", "MAMUJU-URIP SUMOHARJO"),
  ];
  List<SourceOrderNameModel> _listSourceOrderNameTemp = [];

  TextEditingController get controllerSearch => _controllerSearch;

  bool get showClear => _showClear;

  set showClear(bool value) {
    this._showClear = value;
    notifyListeners();
  }

  void  changeAction(String value) {
    if (value != "") {
      showClear = true;
    } else {
      showClear = false;
    }
  }

  UnmodifiableListView<SourceOrderNameModel> get listSourceOrderName {
    return UnmodifiableListView(this._listSourceOrderName);
  }

  UnmodifiableListView<SourceOrderNameModel> get listSourceOrderNameTemp {
    return UnmodifiableListView(this._listSourceOrderNameTemp);
  }

  bool get loadData => _loadData;

  set loadData(bool value) {
    this._loadData = value;
  }

  Future<void> getSourceOrderName(BuildContext context) async{
    this._listSourceOrderName.clear();
    this._listSourceOrderNameTemp.clear();
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    var _providerInfoObjectUnit = Provider.of<InformationObjectUnitChangeNotifier>(context, listen: false);
    loadData = true;
    final ioc = new HttpClient();
    ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
    final _http = IOClient(ioc);
    var storage = FlutterSecureStorage();

    String _fieldNamaSumberOrder = await storage.read(key: "FieldNamaSumberOrder");
    var _body = jsonEncode({
      "refOne" : _providerInfoObjectUnit.brandObjectSelected != null ? _providerInfoObjectUnit.brandObjectSelected.id.toString() : "",
      "refTwo" : _providerInfoObjectUnit.sourceOrderSelected != null ? _providerInfoObjectUnit.sourceOrderSelected.kode.toString() : "",
      "refThree" : _preferences.getString("SentraD")
    });
    final _response = await _http.post(
      "${BaseUrl.urlGeneral}$_fieldNamaSumberOrder",
      // "${urlPublic}pihak-ketiga/get_sumberorder_name",
      body: _body,
      headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
    );
    if(_response.statusCode == 200){
      final _result = jsonDecode(_response.body);
      if(_result == null){
        showSnackBar("Nama Sumber Order tidak ditemukan");
        loadData = false;
      }
      else{
        for(int i=0; i <_result.length; i++){
          this._listSourceOrderName.add(
            SourceOrderNameModel(
              _result[i]['kode'],
              _result[i]['deskripsi']
            )
          );
        }
        // print("query: $query");
        // _listSourceOrderNameTemp.clear();
        // if (query.isEmpty) {
        //   return;
        // }
        // _listSourceOrderName.forEach((dataSourceOrderName) {
        //   if (dataSourceOrderName.kode.contains(query) || dataSourceOrderName.deskripsi.contains(query)) {
        //     this._listSourceOrderNameTemp.add(dataSourceOrderName);
        //   }
        // });
        loadData = false;
      }
    }
    else{
      showSnackBar("Error response status ${_response.statusCode}");
      this._loadData = false;
    }
    notifyListeners();
  }

  void searchSourceOrderName(String query) async {
    if(query.length < 3) {
      showSnackBar("Input minimal 3 karakter");
    }
    else {
      _listSourceOrderNameTemp.clear();
      if (query.isEmpty) {
        return;
      }

      _listSourceOrderName.forEach((dataSourceOrderName) {
        if (dataSourceOrderName.kode.contains(query) || dataSourceOrderName.deskripsi.contains(query)) {
          this._listSourceOrderNameTemp.add(dataSourceOrderName);
        }
      });
      notifyListeners();
    }
  }

  void clearSearchTemp() {
    _listSourceOrderNameTemp.clear();
  }

  void showSnackBar(String text){
    this._scaffoldKey.currentState. showSnackBar(new SnackBar(
        content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor));
  }
}
