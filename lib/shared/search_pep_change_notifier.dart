import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/models/form_m_pekerjaan_model.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/io_client.dart';

import '../main.dart';

class SearchPEPChangeNotifier with ChangeNotifier {
  bool _showClear = false;
  bool _loadData = false;
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  TextEditingController _controllerSearch = TextEditingController();
  // List<PEPModel> _listPEP = [
  //   PEPModel("MGJ", "PEP TYPE 1"),
  //   PEPModel("MGK", "DEBT COLLECTOR"),
  //   PEPModel("MGL", "ENTERTAINER"),
  //   PEPModel("MGM", "LSM"),
  //   PEPModel("MGN", "ORMAS"),
  //   PEPModel("MGO", "PENGACARALAWYER"),
  //   PEPModel("MGP", "SECURITY"),
  //   PEPModel("MGQ", "PEMUKA AGAMA"),
  //   PEPModel("MGR", "MENTRI"),
  //   PEPModel("MGS", "MPR"),
  //   PEPModel("MGT", "DPR"),
  //   PEPModel("MGU", "TNI/POLISI"),
  //   PEPModel("MGV", "KEPALA NEGARA/PEMERINTAHAN & WAKIL"),
  // ];

  List<PEPModel> _listPEP = [];

  List<PEPModel> _listPEPTemp = [];

  TextEditingController get controllerSearch => _controllerSearch;

  bool get showClear => _showClear;

  set showClear(bool value) {
    this._showClear = value;
    notifyListeners();
  }

  void changeAction(String value) {
    if (value != "") {
      showClear = true;
    } else {
      showClear = false;
    }
  }

  UnmodifiableListView<PEPModel> get listPEP {
    return UnmodifiableListView(this._listPEP);
  }

  UnmodifiableListView<PEPModel> get listPEPTemp {
    return UnmodifiableListView(this._listPEPTemp);
  }

  bool get loadData => _loadData;

  set loadData(bool value) {
    this._loadData = value;
  }

  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  Future<void> getPEP() async{
    this._listPEP.clear();
    loadData = true;
    final ioc = new HttpClient();
    ioc.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;

    final _http = IOClient(ioc);

    var storage = FlutterSecureStorage();
    String _fieldPEP = await storage.read(key: "FieldPEP");
    final _response = await _http.get(
      "${BaseUrl.urlGeneral}$_fieldPEP",
      headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
        // "${urlPublic}api/occupation/get-pep-type"
    );
    // print("test ${"${BaseUrl.urlGeneral}$_fieldPEP"}");

    if(_response.statusCode == 200){
      final _result = jsonDecode(_response.body);
      // print("result pep: $_result");
      if(_result.isNotEmpty){
        for(int i = 0 ; i < _result.length ; i++) {
          _listPEP.add(PEPModel(_result[i]['KODE'], _result[i]['DESKRIPSI']));
        }
      }
      this._loadData = false;
    } else {
      showSnackBar("Error response status ${_response.statusCode}");
      this._loadData = false;
    }
    notifyListeners();
  }

  void searchPEP(String query) {
    if(query.length < 3) {
      showSnackBar("Input minimal 3 karakter");
    } else {
      _listPEPTemp.clear();
      if (query.isEmpty) {
        return;
      }

      _listPEP.forEach((dataPEP) {
        if (dataPEP.KODE.contains(query) || dataPEP.DESKRIPSI.contains(query)) {
          _listPEPTemp.add(dataPEP);
        }
      });
    }
    notifyListeners();
  }

  void clearSearchTemp() {
    _listPEPTemp.clear();
  }

  void showSnackBar(String text){
    this._scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor));
  }
}
