import 'dart:convert';

import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/coverage1_model.dart';
import 'package:ad1ms2_dev/models/product_model.dart';
import 'package:ad1ms2_dev/models/product_type_model.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/form_m_add_major_insurance_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_application_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_credit_structure_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_object_unit_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_foto_change_notif.dart';
import 'package:ad1ms2_dev/shared/list_oid_change_notifier.dart';
import 'package:ad1ms2_dev/shared/resource/get_rate_asuransi_perluasan.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SearchCoverageTypeChangeNotifier with ChangeNotifier{
  bool _showClear = false;
  TextEditingController _controllerSearch = TextEditingController();
  bool _loadData = false;
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  TextEditingController get controllerSearch => _controllerSearch;

  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  bool get showClear => _showClear;

  set showClear(bool value) {
    this._showClear = value;
    notifyListeners();
  }

  void  changeAction(String value) {
    if (value != "") {
      showClear = true;
    } else {
      showClear = false;
    }
  }

  // UnmodifiableListView<SourceOrderNameModel> get listSourceOrderName {
  //   return UnmodifiableListView(this._listSourceOrderName);
  // }
  //
  // UnmodifiableListView<SourceOrderNameModel> get listSourceOrderNameTemp {
  //   return UnmodifiableListView(this._listSourceOrderNameTemp);
  // }

  bool get loadData => _loadData;

  set loadData(bool value) {
    this._loadData = value;
  }

  void getCoverageType(BuildContext context,CoverageModel coverage1,CoverageModel coverage2,String type,ProductModel product, String periodType) async{
    // var _providerAsuransiUtama = Provider.of<FormMAddMajorInsuranceChangeNotifier>(context,listen: false);
    var _providerInfoAppl = Provider.of<InfoAppChangeNotifier>(context,listen: false);
    var _providerPhoto = Provider.of<FormMFotoChangeNotifier>(context,listen: false);
    var _providerListOID = Provider.of<ListOidChangeNotifier>(context,listen: false);
    var _providerInfoUnitObject = Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false);
    var _providerInfCreditStructure = Provider.of<InfoCreditStructureChangeNotifier>(context,listen: false);
    SharedPreferences _preference = await SharedPreferences.getInstance();
    this._loadData = true;
    var _body = jsonEncode(
        {
          "P_APPLICATION_DATE": _providerInfoAppl.controllerOrderDate.text,
          "P_INSR_COMPANY_ID": "02",
          "P_INSR_COV_1": coverage1 != null ? coverage1.PARENT_KODE : "",
          "P_INSR_COV_2": coverage2 != null ? coverage2.PARENT_KODE : "",
          "P_INSR_FIN_TYPE_ID": _preference.getString("cust_type") != "COM" ? "${_providerPhoto.typeOfFinancingModelSelected.financingTypeId}":"${_providerInfoUnitObject.typeOfFinancingModelSelected.financingTypeId}",
          "P_INSR_JENIS": type != null ? "$type" : "",
          "P_INSR_MAX_OTR": _providerInfCreditStructure.controllerObjectPrice.text.isNotEmpty ? "${double.parse(_providerInfCreditStructure.controllerObjectPrice.text.replaceAll(",", "")).floor()}" : 0, // harga object dr inf structure credit
          "P_INSR_OBJT_PURPOSE_ID": _providerInfoUnitObject.objectPurposeSelected != null ? "${_providerInfoUnitObject.objectPurposeSelected.id}" : '',
          "P_INSR_OBJT_TYPE_ID": _providerInfoUnitObject.objectTypeSelected != null ? "${_providerInfoUnitObject.objectTypeSelected.id}" : '',
          "P_INSR_PERIOD": periodType != null ?  "$periodType":"",
          "P_INSR_PRODUCT_ID": product != null ? "${product.KODE}":"",
          "P_PARA_CP_CENTRA_ID": "${_preference.getString("SentraD")}",// sentra D
          "P_PROD_TYPE_ID": _providerInfoUnitObject.productTypeSelected != null ? "${_providerInfoUnitObject.productTypeSelected.id}" : '',
        }
    );
    print("body jenis pertanggungan $_body");
    try{
      var _result = await getAsuransiPerluasan(_body);
      print(_result);
      this._loadData = false;
    }
    catch(e){
      showSnackBar(e.toString());
      this._loadData = false;
    }
  }

  void showSnackBar(String text){
    this._scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor));
  }

  // void searchSourceOrderName(String query) {
  //   if(query.length < 3) {
  //     showSnackBar("Input minimal 3 karakter");
  //   }
  //   else {
  //     _listSourceOrderNameTemp.clear();
  //     if (query.isEmpty) {
  //       return;
  //     }
  //
  //     _listSourceOrderName.forEach((dataProgram) {
  //       if (dataProgram.kode.contains(query) || dataProgram.deskripsi.contains(query)) {
  //         this._listSourceOrderNameTemp.add(dataProgram);
  //       }
  //     });
  //     notifyListeners();
  //   }
  // }
  //
  // void clearSearchTemp() {
  //   _listSourceOrderNameTemp.clear();
  // }
}