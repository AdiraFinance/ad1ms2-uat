import 'dart:convert';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/io_client.dart';

import '../../main.dart';
import '../constants.dart';
var storage = FlutterSecureStorage();
// START DEDUP
// DEDUP - MANDATORY - INDIVIDU
Future<List> getMandatoryDedupIndi()async {
  final ioc = new HttpClient();
  ioc.badCertificateCallback =
      (X509Certificate cert, String host, int port) => true;

  final _http = IOClient(ioc);
  String _mandatoryNonMandatoryDedupIndividu = await storage.read(key: "DedupMandatory");
  final _response = await _http.get(
      "${BaseUrl.urlGeneral}$_mandatoryNonMandatoryDedupIndividu",
      // "${urlPublic}api-form-web/get_dedupindividu_man",
      headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
  );
  print("getMandatoryDedupIndi ${_response.statusCode}");
  if(_response.statusCode == 200){
    final _result = jsonDecode(_response.body);
    final _data = _result['mandatory'];
    print("getMandatoryDedupIndi $_data");
    return _data;
  }
  else{
    throw Exception("Failed get data error ${_response.statusCode}");
  }
}
// DEDUP - HIDE SHOW - INDIVIDU
Future<List> getVisibleDedupIndi()async {
  final ioc = new HttpClient();
  ioc.badCertificateCallback =
      (X509Certificate cert, String host, int port) => true;

  final _http = IOClient(ioc);
  String _hideShowDedupIndividu = await storage.read(key: "DedupHideShow");
  final _response = await _http.get(
      "${BaseUrl.urlGeneral}$_hideShowDedupIndividu",
      // "${urlPublic}api-form-web/get_dedupindividu_hs",
      headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
  );
  print("getVisibleDedupIndi ${_response.statusCode}");
  if(_response.statusCode == 200){
    final _result = jsonDecode(_response.body);
    final _data = _result['hideshow'];
    print("getVisibleDedupIndi $_data");
    return _data;
  }
  else{
    throw Exception("Failed get data error ${_response.statusCode}");
  }
}
// DEDUP - MANDATORY - COMPANY
Future<List> getMandatoryDedupComp()async {
  final ioc = new HttpClient();
  ioc.badCertificateCallback =
      (X509Certificate cert, String host, int port) => true;

  final _http = IOClient(ioc);
  String _mandatoryNonMandatoryDedupCompany = await storage.read(key: "DedupCompanyMandatory");
  final _response = await _http.get(
      "${BaseUrl.urlGeneral}$_mandatoryNonMandatoryDedupCompany",
      // "${urlPublic}api-form-web/get_dedupcompany_man",
      headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
  );
  print("getMandatoryDedupComp ${_response.statusCode}");
  if(_response.statusCode == 200){
    final _result = jsonDecode(_response.body);
    final _data = _result['mandatory'];
    print("getMandatoryDedupComp $_data");
    return _data;
  }
  else{
    throw Exception("Failed get data error ${_response.statusCode}");
  }
}
// DEDUP - HIDE SHOW - COMPANY
Future<List> getVisibleDedupComp()async {
  final ioc = new HttpClient();
  ioc.badCertificateCallback =
      (X509Certificate cert, String host, int port) => true;

  final _http = IOClient(ioc);
  String _hideShowDedupCompany = await storage.read(key: "DedupCompanyHideShow");
  final _response = await _http.get(
      "${BaseUrl.urlGeneral}$_hideShowDedupCompany",
      // "${urlPublic}api-form-web/get_dedupcompany_hs",
      headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
  );
  print("getVisibleDedupComp ${_response.statusCode}");
  if(_response.statusCode == 200){
    final _result = jsonDecode(_response.body);
    final _data = _result['hideshow'];
    print("getVisibleDedupComp $_data");
    return _data;
  }
  else{
    throw Exception("Failed get data error ${_response.statusCode}");
  }
}
// END DEDUP

// START IDE
// IDE - FORM M - INDIVIDU - HIDE SHOW
Future<List> getHideShowDataIde()async {
  final ioc = new HttpClient();
  ioc.badCertificateCallback =
      (X509Certificate cert, String host, int port) => true;

  final _http = IOClient(ioc);
  String _hideShowMideIndividu = await storage.read(key: "IDEMHideShow");
  print("${BaseUrl.urlGeneral}$_hideShowMideIndividu");
  final _response = await _http.get(
    "${BaseUrl.urlGeneral}$_hideShowMideIndividu",
      // "${urlPublic}api-form-web/get_mide_individuhs",
      headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
  );
  print("getHideShowDataIde ${_response.statusCode}");
  if(_response.statusCode == 200){
    final _result = jsonDecode(_response.body);
    final _data = _result['hideshow'];
    print("getHideShowDataIde $_data");
    return _data;
  }
  else{
    throw Exception("Failed get data error ${_response.statusCode}");
  }
}
// IDE - FORM M - INDIVIDU - MANDATORY
Future<List> getMandatoryDataIde()async {
  final ioc = new HttpClient();
  ioc.badCertificateCallback =
      (X509Certificate cert, String host, int port) => true;

  final _http = IOClient(ioc);
  String _mandatoryNonMandatoryMideIndividu = await storage.read(key: "IDEMMandatory");
  final _response = await _http.get(
      "${BaseUrl.urlGeneral}$_mandatoryNonMandatoryMideIndividu",
      // "${urlPublic}api-form-web/get_mide_individuman",
      headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
  );
  print("getMandatoryDataIde ${_response.statusCode}");
  if(_response.statusCode == 200){
    final _result = jsonDecode(_response.body);
    final _data = _result['mandatory'];
    print("getMandatoryDataIde $_data");
    return _data;
  }
  else{
    throw Exception("Failed get data error ${_response.statusCode}");
  }
}
// IDE - FORM M - COMPANY - HIDE SHOW
Future<List> getHideShowDataIdeCom()async {
  final ioc = new HttpClient();
  ioc.badCertificateCallback =
      (X509Certificate cert, String host, int port) => true;

  final _http = IOClient(ioc);
  String _hideShowMideCompany = await storage.read(key: "IDEMCompanyHideShow");
  final _response = await _http.get(
      "${BaseUrl.urlGeneral}$_hideShowMideCompany",
      // "${urlPublic}api-form-web/get_midecompany_hs",
      headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
  );
  print("getHideShowDataIdeCom ${_response.statusCode}");
  if(_response.statusCode == 200){
    final _result = jsonDecode(_response.body);
    final _data = _result['hideshow'];
    print("getHideShowDataIdeCom $_data");
    return _data;
  }
  else{
    throw Exception("Failed get data error ${_response.statusCode}");
  }
}
// IDE - FORM M - COMPANY - MANDATORY
Future<List> getMandatoryDataIdeCom()async {
  final ioc = new HttpClient();
  ioc.badCertificateCallback =
      (X509Certificate cert, String host, int port) => true;

  final _http = IOClient(ioc);
  String _mandatoryNonMandatoryMideCompany = await storage.read(key: "IDEMCompanyMandatory");
  final _response = await _http.get(
      "${BaseUrl.urlGeneral}$_mandatoryNonMandatoryMideCompany",
      // "${urlPublic}api-form-web/get_midecompany_man",
      headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
  );
  print("getMandatoryDataIdeCom ${_response.statusCode}");
  if(_response.statusCode == 200){
    final _result = jsonDecode(_response.body);
    final _data = _result['mandatory'];
    print("getMandatoryDataIdeCom $_data");
    return _data;
  }
  else{
    throw Exception("Failed get data error ${_response.statusCode}");
  }
}

// IDE - FORM APP - INDIVIDU - HIDE SHOW
Future<List> getHideShowDataAppIde()async {
  final ioc = new HttpClient();
  ioc.badCertificateCallback =
      (X509Certificate cert, String host, int port) => true;

  final _http = IOClient(ioc);
  String _hideShowAppmIndividu = await storage.read(key: "IDEAppHideShow");
  final _response = await _http.get(
      "${BaseUrl.urlGeneral}$_hideShowAppmIndividu",
      // "${urlPublic}api-form-web/form_appmindividu_hs",
      headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
  );
  print("getHideShowDataAppIde ${_response.statusCode}");
  if(_response.statusCode == 200){
    final _result = jsonDecode(_response.body);
    final _data = _result['hideshow'];
    print("getHideShowDataAppIde $_data");
    return _data;
  }
  else{
    throw Exception("Failed get data error ${_response.statusCode}");
  }
}
// IDE - FORM APP - INDIVIDU - MANDATORY
Future<List> getMandatoryDataAppIde()async {
  final ioc = new HttpClient();
  ioc.badCertificateCallback =
      (X509Certificate cert, String host, int port) => true;

  final _http = IOClient(ioc);
  String _mandatoryNonMandatoryAppmIndividu = await storage.read(key: "IDEAppMandatory");
  final _response = await _http.get(
      "${BaseUrl.urlGeneral}$_mandatoryNonMandatoryAppmIndividu",
      // "${urlPublic}api-form-web/form_appmindividu_man",
      headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
  );
  print("getMandatoryDataAppIde ${_response.statusCode}");
  if(_response.statusCode == 200){
    final _result = jsonDecode(_response.body);
    final _data = _result['mandatory'];
    print("getMandatoryDataAppIde $_data");
    return _data;
  }
  else{
    throw Exception("Failed get data error ${_response.statusCode}");
  }
}
// IDE - FORM APP - COMPANY - HIDE SHOW
Future<List> getHideShowDataAppIdeCom()async {
  final ioc = new HttpClient();
  ioc.badCertificateCallback =
      (X509Certificate cert, String host, int port) => true;

  final _http = IOClient(ioc);
  String _hideShowAppCompany = await storage.read(key: "IDEAppCompanyHideShow");
  final _response = await _http.get(
      "${BaseUrl.urlGeneral}$_hideShowAppCompany",
      // "${urlPublic}api-form-web/form_appcompany_hs",
      headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
  );
  print("getHideShowDataAppIdeCom ${_response.statusCode}");
  if(_response.statusCode == 200){
    final _result = jsonDecode(_response.body);
    final _data = _result['hideshow'];
    print("getHideShowDataAppIdeCom $_data");
    return _data;
  }
  else{
    throw Exception("Failed get data error ${_response.statusCode}");
  }
}
// IDE - FORM APP - COMPANY - MANDATORY
Future<List> getMandatoryDataAppIdeCom()async {
  final ioc = new HttpClient();
  ioc.badCertificateCallback =
      (X509Certificate cert, String host, int port) => true;

  final _http = IOClient(ioc);
  String _mandatoryNonMandatoryAppCompany = await storage.read(key: "IDEAppCompanyMandatory");
  final _response = await _http.get(
      "${BaseUrl.urlGeneral}$_mandatoryNonMandatoryAppCompany",
      // "${urlPublic}api-form-web/form_appcompany_man",
      headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
  );
  print("getMandatoryDataAppIdeCom ${_response.statusCode}");
  if(_response.statusCode == 200){
    final _result = jsonDecode(_response.body);
    final _data = _result['mandatory'];
    print("getMandatoryDataAppIdeCom $_data");
    return _data;
  }
  else{
    throw Exception("Failed get data error ${_response.statusCode}");
  }
}
// END IDE

// START REGULER SURVEY
// REGULER SURVEY - FORM M - INDIVIDU - HIDE SHOW
Future<List> getHideShowFormMRegularSurvey()async {
  final ioc = new HttpClient();
  ioc.badCertificateCallback =
      (X509Certificate cert, String host, int port) => true;

  final _http = IOClient(ioc);
  String _regSurveyMHideShow = await storage.read(key: "RegSurveyMHideShow");
  final _response = await _http.get(
      "${BaseUrl.urlGeneral}$_regSurveyMHideShow",//api-form-web/get_regsurvey_mindividu_hs",
      headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
  );
  print("getHideShowFormMRegularSurvey ${_response.statusCode}");
  if(_response.statusCode == 200){
    final _result = jsonDecode(_response.body);
    final _data = _result['hideshow'];
    print("getHideShowFormMRegularSurvey $_data");
    return _data;
  }
  else{
    throw Exception("Failed get data error ${_response.statusCode}");
  }
}
// REGULER SURVEY - FORM M - INDIVIDU - MANDATORY
Future<List> getMandatoryFormMRegularSurvey()async {
  final ioc = new HttpClient();
  ioc.badCertificateCallback =
      (X509Certificate cert, String host, int port) => true;

  final _http = IOClient(ioc);
  String _regSurveyMMandatory = await storage.read(key: "RegSurveyMMandatory");
  final _response = await _http.get(
      "${BaseUrl.urlGeneral}$_regSurveyMMandatory",//api-form-web/get_regsurvey_mindividu_man",
      headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
  );
  print("getMandatoryFormMRegularSurvey ${_response.statusCode}");
  if(_response.statusCode == 200){
    final _result = jsonDecode(_response.body);
    final _data = _result['mandatory'];
    print("getMandatoryFormMRegularSurvey $_data");
    return _data;
  }
  else{
    throw Exception("Failed get data error ${_response.statusCode}");
  }
}
// REGULER SURVEY - FORM M - COMPANY - HIDE SHOW
Future<List> getHideShowFormMRegularSurveyCom()async {
  final ioc = new HttpClient();
  ioc.badCertificateCallback =
      (X509Certificate cert, String host, int port) => true;

  final _http = IOClient(ioc);
  String _regSurveyMCompanyHideShow = await storage.read(key: "RegSurveyMCompanyHideShow");
  final _response = await _http.get(
      "${BaseUrl.urlGeneral}$_regSurveyMCompanyHideShow",//api-form-web/get_regsurveycomp_hs",
      headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
  );
  print("getHideShowFormMRegularSurveyCom ${_response.statusCode}");
  if(_response.statusCode == 200){
    final _result = jsonDecode(_response.body);
    final _data = _result['hideshow'];
    print("getHideShowFormMRegularSurveyCom $_data");
    return _data;
  }
  else{
    throw Exception("Failed get data error ${_response.statusCode}");
  }
}
// REGULER SURVEY - FORM M - COMPANY - MANDATORY
Future<List> getMandatoryFormMRegularSurveyCom()async {
  final ioc = new HttpClient();
  ioc.badCertificateCallback =
      (X509Certificate cert, String host, int port) => true;

  final _http = IOClient(ioc);
  String _regSurveyMCompanyMandatory = await storage.read(key: "RegSurveyMCompanyMandatory");
  final _response = await _http.get(
      "${BaseUrl.urlGeneral}$_regSurveyMCompanyMandatory",//api-form-web/get_regsurveycomp_man",
      headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
  );
  print("getMandatoryFormMRegularSurveyCom ${_response.statusCode}");
  if(_response.statusCode == 200){
    final _result = jsonDecode(_response.body);
    final _data = _result['mandatory'];
    print("getMandatoryFormMRegularSurveyCom $_data");
    return _data;
  }
  else{
    throw Exception("Failed get data error ${_response.statusCode}");
  }
}

// REGULER SURVEY - FORM APP - INDIVIDU - HIDE SHOW
Future<List> getHideShowAppRegularSurvey()async {
  final ioc = new HttpClient();
  ioc.badCertificateCallback =
      (X509Certificate cert, String host, int port) => true;

  final _http = IOClient(ioc);
  String _regSurveyAppHideShow = await storage.read(key: "RegSurveyAppHideShow");
  final _response = await _http.get(
      "${BaseUrl.urlGeneral}$_regSurveyAppHideShow",
      // "${urlPublic}api-form-web/form_appregsurvey_hs",
      headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
  );
  print("getHideShowAppRegularSurvey ${_response.statusCode}");
  if(_response.statusCode == 200){
    final _result = jsonDecode(_response.body);
    final _data = _result['hideshow'];
    print("getHideShowAppRegularSurvey $_data");
    return _data;
  }
  else{
    throw Exception("Failed get data error ${_response.statusCode}");
  }
}
// REGULER SURVEY - FORM APP - INDIVIDU - MANDATORY
Future<List> getMandatoryAppRegularSurvey()async {
  final ioc = new HttpClient();
  ioc.badCertificateCallback =
      (X509Certificate cert, String host, int port) => true;

  final _http = IOClient(ioc);
  String _mandatoryNonMandatoryAppRegSurvey = await storage.read(key: "RegSurveyAppMandatory");
  print("${BaseUrl.urlGeneral}$_mandatoryNonMandatoryAppRegSurvey");
  final _response = await _http.get(
      "${BaseUrl.urlGeneral}$_mandatoryNonMandatoryAppRegSurvey",
      // "${urlPublic}api-form-web/form_appregsurvey_man",
      headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
  );
  print("getMandatoryAppRegularSurvey ${_response.statusCode}");
  if(_response.statusCode == 200){
    final _result = jsonDecode(_response.body);
    final _data = _result['mandatory'];
    print("getMandatoryAppRegularSurvey $_data");
    return _data;
  }
  else{
    throw Exception("Failed get data error ${_response.statusCode}");
  }
}
// REGULER SURVEY - FORM APP - COMPANY - HIDE SHOW
Future<List> getHideShowAppRegularSurveyCom()async {
  final ioc = new HttpClient();
  ioc.badCertificateCallback =
      (X509Certificate cert, String host, int port) => true;

  final _http = IOClient(ioc);
  String _regSurveyAppCompanyHideShow = await storage.read(key: "RegSurveyAppCompanyHideShow");
  final _response = await _http.get(
      "${BaseUrl.urlGeneral}$_regSurveyAppCompanyHideShow",//api-form-web/form_appregsurveycomp_hs",
      headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
  );
  print("getHideShowAppRegularSurveyCom ${_response.statusCode}");
  if(_response.statusCode == 200){
    final _result = jsonDecode(_response.body);
    final _data = _result['hideshow'];
    print("getHideShowAppRegularSurveyCom $_data");
    return _data;
  }
  else{
    throw Exception("Failed get data error ${_response.statusCode}");
  }
}
// REGULER SURVEY - FORM APP - COMPANY - MANDATORY
Future<List> getMandatoryAppRegularSurveyCom()async {
  final ioc = new HttpClient();
  ioc.badCertificateCallback =
      (X509Certificate cert, String host, int port) => true;

  final _http = IOClient(ioc);
  String _regSurveyAppCompanyMandatory = await storage.read(key: "RegSurveyAppCompanyMandatory");
  final _response = await _http.get(
      "${BaseUrl.urlGeneral}$_regSurveyAppCompanyMandatory",//api-form-web/form_appregsurveycomp_man",
      headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
  );
  print("getMandatoryAppRegularSurveyCom ${_response.statusCode}");
  if(_response.statusCode == 200){
    final _result = jsonDecode(_response.body);
    final _data = _result['mandatory'];
    print("getMandatoryAppRegularSurveyCom $_data");
    return _data;
  }
  else{
    throw Exception("Failed get data error ${_response.statusCode}");
  }
}

// REGULER SURVEY - FORM SURVEY - INDIVIDU - HIDE SHOW
Future<List> getHideShowRegularSurvey()async {
  final ioc = new HttpClient();
  ioc.badCertificateCallback =
      (X509Certificate cert, String host, int port) => true;

  final _http = IOClient(ioc);
  String _hideShowRegulerSurvey = await storage.read(key: "RegSurveySurveyHideShow");
  final _response = await _http.get(
      "${BaseUrl.urlGeneral}$_hideShowRegulerSurvey",
      // "${urlPublic}api-form-web/get_regulersurvey_hs",
      headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
  );
  print("getHideShowRegularSurvey ${_response.statusCode}");
  if(_response.statusCode == 200){
    final _result = jsonDecode(_response.body);
    final _data = _result['hideshow'];
    print("getHideShowRegularSurvey $_data");
    return _data;
  }
  else{
    throw Exception("Failed get data error ${_response.statusCode}");
  }
}
// REGULER SURVEY - FORM SURVEY - INDIVIDU - MANDATORY
Future<List> getMandatoryRegularSurvey()async {
  final ioc = new HttpClient();
  ioc.badCertificateCallback =
      (X509Certificate cert, String host, int port) => true;

  final _http = IOClient(ioc);
  String _mandatoryNonMandatoryRegulerSurvey = await storage.read(key: "RegSurveySurveyMandatory");
  final _response = await _http.get(
      "${BaseUrl.urlGeneral}$_mandatoryNonMandatoryRegulerSurvey",
      // "${urlPublic}api-form-web/get_regulersurvey_man",
      headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
  );
  print("getMandatoryRegularSurvey ${_response.statusCode}");
  if(_response.statusCode == 200){
    final _result = jsonDecode(_response.body);
    final _data = _result['mandatory'];
    print("getMandatoryRegularSurvey $_data");
    return _data;
  }
  else{
    throw Exception("Failed get data error ${_response.statusCode}");
  }
}
// REGULER SURVEY - FORM SURVEY - INDIVIDU - ENABLE DISABLE
Future<List> getEnabledRegularSurvey()async {
  final ioc = new HttpClient();
  ioc.badCertificateCallback =
      (X509Certificate cert, String host, int port) => true;

  final _http = IOClient(ioc);
  String _enableDisableRegulerSurvey = await storage.read(key: "RegSurveySurveyEnbDis");
  final _response = await _http.get(
      "${BaseUrl.urlGeneral}$_enableDisableRegulerSurvey",
      // "${urlPublic}api-form-web/get_regulersurvey_enb",
      headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
  );
  print("getEnabledRegularSurvey ${_response.statusCode}");
  if(_response.statusCode == 200){
    final _result = jsonDecode(_response.body);
    final _data = _result['enableDisable'];
    print("getEnabledRegularSurvey $_data");
    return _data;
  }
  else{
    throw Exception("Failed get data error ${_response.statusCode}");
  }
}
// REGULER SURVEY - FORM SURVEY - COMPANY - HIDE SHOW
Future<List> getHideShowComRegularSurvey()async {
  final ioc = new HttpClient();
  ioc.badCertificateCallback =
      (X509Certificate cert, String host, int port) => true;

  final _http = IOClient(ioc);
  String _regSurveySurveyCompanyHideShow = await storage.read(key: "RegSurveySurveyCompanyHideShow");
  final _response = await _http.get(
      "${BaseUrl.urlGeneral}$_regSurveySurveyCompanyHideShow",//api-form-web/get_regsurvey_surveycompany_hs",
      headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
  );
  print("getHideShowComRegularSurvey ${_response.statusCode}");
  if(_response.statusCode == 200){
    final _result = jsonDecode(_response.body);
    final _data = _result['hideshow'];
    print("getHideShowComRegularSurvey $_data");
    return _data;
  }
  else{
    throw Exception("Failed get data error ${_response.statusCode}");
  }
}
// REGULER SURVEY - FORM SURVEY - COMPANY - MANDATORY
Future<List> getMandatoryComRegularSurvey()async {
  final ioc = new HttpClient();
  ioc.badCertificateCallback =
      (X509Certificate cert, String host, int port) => true;

  final _http = IOClient(ioc);
  String _regSurveySurveyCompanyMandatory = await storage.read(key: "RegSurveySurveyCompanyMandatory");
  final _response = await _http.get(
      "${BaseUrl.urlGeneral}$_regSurveySurveyCompanyMandatory",//api-form-web/get_regsurvey_surveycompany_man",
      headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
  );
  print("getMandatoryComRegularSurvey ${_response.statusCode}");
  if(_response.statusCode == 200){
    final _result = jsonDecode(_response.body);
    final _data = _result['mandatory'];
    print("getMandatoryComRegularSurvey $_data");
    return _data;
  }
  else{
    throw Exception("Failed get data error ${_response.statusCode}");
  }
}
// REGULER SURVEY - FORM SURVEY - COMPANY - ENABLE DISABLE
Future<List> getEnabledComRegularSurvey()async {
  final ioc = new HttpClient();
  ioc.badCertificateCallback =
      (X509Certificate cert, String host, int port) => true;

  final _http = IOClient(ioc);
  String _regSurveySurveyCompanyEnbDis = await storage.read(key: "RegSurveySurveyCompanyEnbDis");
  final _response = await _http.get(
      "${BaseUrl.urlGeneral}$_regSurveySurveyCompanyEnbDis",//api-form-web/get_regsurvey_surveycompany_enb",
      headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
  );
  print("getEnabledComRegularSurvey ${_response.statusCode}");
  if(_response.statusCode == 200){
    final _result = jsonDecode(_response.body);
    final _data = _result['enableDisable'];
    print("getEnabledComRegularSurvey $_data");
    return _data;
  }
  else{
    throw Exception("Failed get data error ${_response.statusCode}");
  }
}
// END REGULER SURVEY

// START PAC
// PAC - FORM M - INDIVIDU - HIDE SHOW
Future<List> getHideShowFormMPac()async {
  final ioc = new HttpClient();
  ioc.badCertificateCallback =
      (X509Certificate cert, String host, int port) => true;

  final _http = IOClient(ioc);
  String _hideShowPACM = await storage.read(key: "PACMHideShow");
  final _response = await _http.get(
      "${BaseUrl.urlGeneral}$_hideShowPACM",//api-form-web/get_pac_mindividu_hs",
      headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
  );
  print("getHideShowFormMPac ${_response.statusCode}");
  if(_response.statusCode == 200){
    final _result = jsonDecode(_response.body);
    final _data = _result['hideshow'];
    print("getHideShowFormMPac $_data");
    return _data;
  }
  else{
    throw Exception("Failed get data error ${_response.statusCode}");
  }
}
// PAC - FORM M - INDIVIDU - MANDATORY
Future<List> getMandatoryFormMPac()async {
  final ioc = new HttpClient();
  ioc.badCertificateCallback =
      (X509Certificate cert, String host, int port) => true;

  final _http = IOClient(ioc);
  String _mandatoryPACM = await storage.read(key: "PACMMandatory");
  final _response = await _http.get(
      "${BaseUrl.urlGeneral}$_mandatoryPACM",//api-form-web/get_pac_mindividu_man",
      headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
  );
  print("getMandatoryFormMPac ${_response.statusCode}");
  if(_response.statusCode == 200){
    final _result = jsonDecode(_response.body);
    final _data = _result['mandatory'];
    print("getMandatoryFormMPac $_data");
    return _data;
  }
  else{
    throw Exception("Failed get data error ${_response.statusCode}");
  }
}
// PAC - FORM M - COMPANY - HIDE SHOW
Future<List> getHideShowFormMPacCom()async {
  final ioc = new HttpClient();
  ioc.badCertificateCallback =
      (X509Certificate cert, String host, int port) => true;

  final _http = IOClient(ioc);
  String _hideShowPACMCompany = await storage.read(key: "PACMCompanyHideShow");
  final _response = await _http.get(
      "${BaseUrl.urlGeneral}$_hideShowPACMCompany",//api-form-web/get_paccomp_hs",
      headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
  );
  print("getHideShowFormMPacCom ${_response.statusCode}");
  if(_response.statusCode == 200){
    final _result = jsonDecode(_response.body);
    final _data = _result['hideshow'];
    print("getHideShowFormMPacCom $_data");
    return _data;
  }
  else{
    throw Exception("Failed get data error ${_response.statusCode}");
  }
}
// PAC - FORM M - COMPANY - MANDATORY
Future<List> getMandatoryFormMPacCom()async {
  final ioc = new HttpClient();
  ioc.badCertificateCallback =
      (X509Certificate cert, String host, int port) => true;

  final _http = IOClient(ioc);
  String _mandatoryPACMCompany = await storage.read(key: "PACMCompanyMandatory");
  final _response = await _http.get(
      "${BaseUrl.urlGeneral}$_mandatoryPACMCompany",//api-form-web/get_paccomp_man",
      headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
  );
  print("getMandatoryFormMPacCom ${_response.statusCode}");
  if(_response.statusCode == 200){
    final _result = jsonDecode(_response.body);
    final _data = _result['mandatory'];
    print("getMandatoryFormMPacCom $_data");
    return _data;
  }
  else{
    throw Exception("Failed get data error ${_response.statusCode}");
  }
}

// PAC - FORM APP - INDIVIDU - HIDE SHOW
Future<List> getHideShowAppPAC()async {
  final ioc = new HttpClient();
  ioc.badCertificateCallback =
      (X509Certificate cert, String host, int port) => true;

  final _http = IOClient(ioc);
  String _hideShowPACApp = await storage.read(key: "PACAppHideShow");
  final _response = await _http.get(
      "${BaseUrl.urlGeneral}$_hideShowPACApp",//api-form-web/form_apppacnds_hs",
      headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
  );
  print("getHideShowAppPAC ${_response.statusCode}");
  if(_response.statusCode == 200){
    final _result = jsonDecode(_response.body);
    final _data = _result['hideshow'];
    print("getHideShowAppPAC $_data");
    return _data;
  }
  else{
    throw Exception("Failed get data error ${_response.statusCode}");
  }
}
// PAC - FORM APP - INDIVIDU - MANDATORY
Future<List> getMandatoryAppPAC()async {
  final ioc = new HttpClient();
  ioc.badCertificateCallback =
      (X509Certificate cert, String host, int port) => true;

  final _http = IOClient(ioc);
  String _mandatoryPACApp = await storage.read(key: "PACAppMandatory");
  final _response = await _http.get(
      "${BaseUrl.urlGeneral}$_mandatoryPACApp",//api-form-web/form_apppacnds_man",
      headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
  );
  print("getMandatoryAppPAC ${_response.statusCode}");
  if(_response.statusCode == 200){
    final _result = jsonDecode(_response.body);
    final _data = _result['mandatory'];
    print("getMandatoryAppPAC $_data");
    return _data;
  }
  else{
    throw Exception("Failed get data error ${_response.statusCode}");
  }
}
// PAC - FORM APP - COMPANY - HIDE SHOW
Future<List> getHideShowAppPACCom()async {
  final ioc = new HttpClient();
  ioc.badCertificateCallback =
      (X509Certificate cert, String host, int port) => true;

  final _http = IOClient(ioc);
  String _hideShowPACAppCompany = await storage.read(key: "PACAppCompanyHideShow");
  final _response = await _http.get(
      "${BaseUrl.urlGeneral}$_hideShowPACAppCompany",//api-form-web/form_apppaccomp_hs",
      headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
  );
  print("getHideShowAppPACCom ${_response.statusCode}");
  if(_response.statusCode == 200){
    final _result = jsonDecode(_response.body);
    final _data = _result['hideshow'];
    print("getHideShowAppPACCom $_data");
    return _data;
  }
  else{
    throw Exception("Failed get data error ${_response.statusCode}");
  }
}
// PAC - FORM APP - COMPANY - MANDATORY
Future<List> getMandatoryAppPACCom()async {
  final ioc = new HttpClient();
  ioc.badCertificateCallback =
      (X509Certificate cert, String host, int port) => true;

  final _http = IOClient(ioc);
  String _mandatoryPACAppCompany = await storage.read(key: "PACAppCompanyMandatory");
  final _response = await _http.get(
      "${BaseUrl.urlGeneral}$_mandatoryPACAppCompany",//api-form-web/form_apppaccomp_man",
      headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
  );
  print("getMandatoryAppPACCom ${_response.statusCode}");
  if(_response.statusCode == 200){
    final _result = jsonDecode(_response.body);
    final _data = _result['mandatory'];
    print("getMandatoryAppPACCom $_data");
    return _data;
  }
  else{
    throw Exception("Failed get data error ${_response.statusCode}");
  }
}

// PAC - FORM SURVEY - INDIVIDU - HIDE SHOW
Future<List> getHideShowPAC()async {
  final ioc = new HttpClient();
  ioc.badCertificateCallback =
      (X509Certificate cert, String host, int port) => true;

  final _http = IOClient(ioc);
  String _hideSHowPACSurvey = await storage.read(key: "PACSurveyHideSHow");
  final _response = await _http.get(
      "${BaseUrl.urlGeneral}$_hideSHowPACSurvey",//api-form-web/get_pacnds_hs",
      headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
  );
  print("getHideShowPAC ${_response.statusCode}");
  if(_response.statusCode == 200){
    final _result = jsonDecode(_response.body);
    final _data = _result['hideshow'];
    print("getHideShowPAC $_data");
    return _data;
  }
  else{
    throw Exception("Failed get data error ${_response.statusCode}");
  }
}
// PAC - FORM SURVEY - INDIVIDU - MANDATORY
Future<List> getMandatoryPAC()async {
  final ioc = new HttpClient();
  ioc.badCertificateCallback =
      (X509Certificate cert, String host, int port) => true;

  final _http = IOClient(ioc);
  String _mandatoryPACSurvey = await storage.read(key: "PACSurveyMandatory");
  final _response = await _http.get(
      "${BaseUrl.urlGeneral}$_mandatoryPACSurvey",//api-form-web/get_pacnds_man",
      headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
  );
  print("getMandatoryPAC ${_response.statusCode}");
  if(_response.statusCode == 200){
    final _result = jsonDecode(_response.body);
    final _data = _result['mandatory'];
    print("getMandatoryPAC $_data");
    return _data;
  }
  else{
    throw Exception("Failed get data error ${_response.statusCode}");
  }
}
// PAC - FORM SURVEY - INDIVIDU - ENABLE DISABLE
Future<List> getEnabledPAC()async {
  final ioc = new HttpClient();
  ioc.badCertificateCallback =
      (X509Certificate cert, String host, int port) => true;

  final _http = IOClient(ioc);
  String _enbDisPACSurvey = await storage.read(key: "PACSurveyeEnbDis");
  final _response = await _http.get(
      "${BaseUrl.urlGeneral}$_enbDisPACSurvey",//api-form-web/get_pacnds_enb",
      headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
  );
  print("getEnabledPAC ${_response.statusCode}");
  if(_response.statusCode == 200){
    final _result = jsonDecode(_response.body);
    final _data = _result['enableDisable'];
    print("getEnabledPAC $_data");
    return _data;
  }
  else{
    throw Exception("Failed get data error ${_response.statusCode}");
  }
}
// PAC - FORM SURVEY - COMPANY - HIDE SHOW
Future<List> getHideShowComPAC()async {
  final ioc = new HttpClient();
  ioc.badCertificateCallback =
      (X509Certificate cert, String host, int port) => true;

  final _http = IOClient(ioc);
  String _hideShowPACSurveyCompany = await storage.read(key: "PACSurveyCompanyHideShow");
  final _response = await _http.get(
      "${BaseUrl.urlGeneral}$_hideShowPACSurveyCompany",//api-form-web/get_pac_surveycompany_hs",
      headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
  );
  print("getHideShowComPAC ${_response.statusCode}");
  if(_response.statusCode == 200){
    final _result = jsonDecode(_response.body);
    final _data = _result['hideshow'];
    print("getHideShowComPAC $_data");
    return _data;
  }
  else{
    throw Exception("Failed get data error ${_response.statusCode}");
  }
}
// PAC - FORM SURVEY - COMPANY - MANDATORY
Future<List> getMandatoryComPAC()async {
  final ioc = new HttpClient();
  ioc.badCertificateCallback =
      (X509Certificate cert, String host, int port) => true;

  final _http = IOClient(ioc);
  String _mandatoryPACSurveyCompany = await storage.read(key: "PACSurveyCompanyMandatory");
  final _response = await _http.get(
      "${BaseUrl.urlGeneral}$_mandatoryPACSurveyCompany",//api-form-web/get_pac_surveycompany_man",
      headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
  );
  print("getMandatoryComPAC ${_response.statusCode}");
  if(_response.statusCode == 200){
    final _result = jsonDecode(_response.body);
    final _data = _result['mandatory'];
    print("getMandatoryComPAC $_data");
    return _data;
  }
  else{
    throw Exception("Failed get data error ${_response.statusCode}");
  }
}
// PAC - FORM SURVEY - COMPANY - ENABLE DISABLE
Future<List> getEnabledComPAC()async {
  final ioc = new HttpClient();
  ioc.badCertificateCallback =
      (X509Certificate cert, String host, int port) => true;

  final _http = IOClient(ioc);
  String _enbDisPACSurveyCompany = await storage.read(key: "PACSurveyCompanyEnbDis");
  final _response = await _http.get(
      "${BaseUrl.urlGeneral}$_enbDisPACSurveyCompany",//api-form-web/get_pac_surveycompany_enb",
      headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
  );
  print("getEnabledComPAC ${_response.statusCode}");
  if(_response.statusCode == 200){
    final _result = jsonDecode(_response.body);
    final _data = _result['enableDisable'];
    print("getEnabledComPAC $_data");
    return _data;
  }
  else{
    throw Exception("Failed get data error ${_response.statusCode}");
  }
}
// END PAC

// START RESURVEY
// RESURVEY - FORM M - INDIVIDU - HIDE SHOW
Future<List> getHideShowFormMResurvey()async {
  final ioc = new HttpClient();
  ioc.badCertificateCallback =
      (X509Certificate cert, String host, int port) => true;

  final _http = IOClient(ioc);
  String _hideShowResurveyM = await storage.read(key: "ResurveyMHideShow");
  final _response = await _http.get(
      "${BaseUrl.urlGeneral}$_hideShowResurveyM",//api-form-web/get_resurvey_mindividu_hs",
      headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
  );
  print("getHideShowFormMResurvey ${_response.statusCode}");
  if(_response.statusCode == 200){
    final _result = jsonDecode(_response.body);
    final _data = _result['hideshow'];
    print("getHideShowFormMResurvey $_data");
    return _data;
  }
  else{
    throw Exception("Failed get data error ${_response.statusCode}");
  }
}
// RESURVEY - FORM M - INDIVIDU - MANDATORY
Future<List> getMandatoryFormMResurvey()async {
  final ioc = new HttpClient();
  ioc.badCertificateCallback =
      (X509Certificate cert, String host, int port) => true;

  final _http = IOClient(ioc);
  String _mandatoryResurveyM = await storage.read(key: "ResurveyMMandatory");
  final _response = await _http.get(
      "${BaseUrl.urlGeneral}$_mandatoryResurveyM",//api-form-web/get_resurvey_mindividu_man",
      headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
  );
  print("getMandatoryFormMResurvey ${_response.statusCode}");
  if(_response.statusCode == 200){
    final _result = jsonDecode(_response.body);
    final _data = _result['mandatory'];
    print("getMandatoryFormMResurvey $_data");
    return _data;
  }
  else{
    throw Exception("Failed get data error ${_response.statusCode}");
  }
}
// RESURVEY - FORM M - COMPANY - HIDE SHOW
Future<List> getHideShowFormMResurveyCom()async {
  final ioc = new HttpClient();
  ioc.badCertificateCallback =
      (X509Certificate cert, String host, int port) => true;

  final _http = IOClient(ioc);
  String _hideShowResurveyMCompany = await storage.read(key: "ResurveyMCompanyHideShow");
  final _response = await _http.get(
      "${BaseUrl.urlGeneral}$_hideShowResurveyMCompany",//api-form-web/get_resurveycomp_hs",
      headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
  );
  print("getHideShowFormMResurveyCom ${_response.statusCode}");
  if(_response.statusCode == 200){
    final _result = jsonDecode(_response.body);
    final _data = _result['hideshow'];
    print("getHideShowFormMResurveyCom $_data");
    return _data;
  }
  else{
    throw Exception("Failed get data error ${_response.statusCode}");
  }
}
// RESURVEY - FORM M - COMPANY - MANDATORY
Future<List> getMandatoryFormMResurveyCom()async {
  final ioc = new HttpClient();
  ioc.badCertificateCallback =
      (X509Certificate cert, String host, int port) => true;

  final _http = IOClient(ioc);
  String _mandatoryResurveyMCompany = await storage.read(key: "ResurveyMCompanyMandatory");
  final _response = await _http.get(
      "${BaseUrl.urlGeneral}$_mandatoryResurveyMCompany",//api-form-web/get_resurveycomp_man",
      headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
  );
  print("getMandatoryFormMResurveyCom ${_response.statusCode}");
  if(_response.statusCode == 200){
    final _result = jsonDecode(_response.body);
    final _data = _result['mandatory'];
    print("getMandatoryFormMResurveyCom $_data");
    return _data;
  }
  else{
    throw Exception("Failed get data error ${_response.statusCode}");
  }
}

// RESURVEY - FORM APP - INDIVIDU - HIDE SHOW
Future<List> getHideShowAppResurvey()async {
  final ioc = new HttpClient();
  ioc.badCertificateCallback =
      (X509Certificate cert, String host, int port) => true;

  final _http = IOClient(ioc);
  String _hideShowResurveyApp = await storage.read(key: "ResurveyAppHideShow");
  final _response = await _http.get(
      "${BaseUrl.urlGeneral}$_hideShowResurveyApp",//api-form-web/form_appresurvey_hs",
      headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
  );
  print("getHideShowAppResurvey ${_response.statusCode}");
  if(_response.statusCode == 200){
    final _result = jsonDecode(_response.body);
    final _data = _result['hideshow'];
    print("getHideShowAppResurvey $_data");
    return _data;
  }
  else{
    throw Exception("Failed get data error ${_response.statusCode}");
  }
}
// RESURVEY - FORM APP - INDIVIDU - MANDATORY
Future<List> getMandatoryAppResurvey()async {
  final ioc = new HttpClient();
  ioc.badCertificateCallback =
      (X509Certificate cert, String host, int port) => true;

  final _http = IOClient(ioc);
  String _mandatoryResurveyApp = await storage.read(key: "ResurveyAppMandatory");
  final _response = await _http.get(
      "${BaseUrl.urlGeneral}$_mandatoryResurveyApp",//api-form-web/form_appresurvey_man",
      headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
  );
  print("getMandatoryAppResurvey ${_response.statusCode}");
  if(_response.statusCode == 200){
    final _result = jsonDecode(_response.body);
    final _data = _result['mandatory'];
    print("getMandatoryAppResurvey $_data");
    return _data;
  }
  else{
    throw Exception("Failed get data error ${_response.statusCode}");
  }
}
// RESURVEY - FORM APP - COMPANY - HIDE SHOW
Future<List> getHideShowAppResurveyCom()async {
  final ioc = new HttpClient();
  ioc.badCertificateCallback =
      (X509Certificate cert, String host, int port) => true;

  final _http = IOClient(ioc);
  String _hideShowResurveyAppCompany = await storage.read(key: "ResurveyAppCompanyHideShow");
  final _response = await _http.get(
      "${BaseUrl.urlGeneral}$_hideShowResurveyAppCompany",//api-form-web/form_appresurveycomp_hs",
      headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
  );
  print("getHideShowAppResurveyCom ${_response.statusCode}");
  if(_response.statusCode == 200){
    final _result = jsonDecode(_response.body);
    final _data = _result['hideshow'];
    print("getHideShowAppResurveyCom $_data");
    return _data;
  }
  else{
    throw Exception("Failed get data error ${_response.statusCode}");
  }
}
// RESURVEY - FORM APP - COMPANY - MANDATORY
Future<List> getMandatoryAppResurveyCom()async {
  final ioc = new HttpClient();
  ioc.badCertificateCallback =
      (X509Certificate cert, String host, int port) => true;

  final _http = IOClient(ioc);
  String _mandatoryResurveyAppCompany = await storage.read(key: "ResurveyAppCompanyMandatory");
  final _response = await _http.get(
      "${BaseUrl.urlGeneral}$_mandatoryResurveyAppCompany",//api-form-web/form_appresurveycomp_man",
      headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
  );
  print("getMandatoryAppResurveyCom ${_response.statusCode}");
  if(_response.statusCode == 200){
    final _result = jsonDecode(_response.body);
    final _data = _result['mandatory'];
    print("getMandatoryAppResurveyCom $_data");
    return _data;
  }
  else{
    throw Exception("Failed get data error ${_response.statusCode}");
  }
}

// RESURVEY - FORM SURVEY - INDIVIDU - HIDE SHOW
Future<List> getHideShowResurvey()async {
  final ioc = new HttpClient();
  ioc.badCertificateCallback =
      (X509Certificate cert, String host, int port) => true;

  final _http = IOClient(ioc);
  String _hideShowResurveySurvey = await storage.read(key: "ResurveySurveyHideShow");
  print("${BaseUrl.urlGeneral}$_hideShowResurveySurvey");
  final _response = await _http.get(
      "${BaseUrl.urlGeneral}$_hideShowResurveySurvey",//api-form-web/get_resurvey_hs",
      headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
  );
  print("getHideShowResurvey ${_response.statusCode}");
  if(_response.statusCode == 200){
    final _result = jsonDecode(_response.body);
    final _data = _result['hideshow'];
    print("getHideShowResurvey $_data");
    return _data;
  }
  else{
    throw Exception("Failed get data error ${_response.statusCode}");
  }
}
// RESURVEY - FORM SURVEY - INDIVIDU - MANDATORY
Future<List> getMandatoryResurvey()async {
  final ioc = new HttpClient();
  ioc.badCertificateCallback =
      (X509Certificate cert, String host, int port) => true;

  final _http = IOClient(ioc);
  String _mandatoryResurveySurvey = await storage.read(key: "ResurveySurveyMandatory");
  print("${BaseUrl.urlGeneral}$_mandatoryResurveySurvey");
  final _response = await _http.get(
      "${BaseUrl.urlGeneral}$_mandatoryResurveySurvey",//api-form-web/get_resurvey_man",
      headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
  );
  print("getMandatoryResurvey ${_response.statusCode}");
  if(_response.statusCode == 200){
    final _result = jsonDecode(_response.body);
    final _data = _result['mandatory'];
    print("getMandatoryResurvey $_data");
    return _data;
  }
  else{
    throw Exception("Failed get data error ${_response.statusCode}");
  }
}
// RESURVEY - FORM SURVEY - INDIVIDU - ENABLE DISABLE
Future<List> getEnabledResurvey()async {
  final ioc = new HttpClient();
  ioc.badCertificateCallback =
      (X509Certificate cert, String host, int port) => true;

  final _http = IOClient(ioc);
  String _enbDisResurveySurvey = await storage.read(key: "ResurveySurveyEnbDis");
  final _response = await _http.get(
      "${BaseUrl.urlGeneral}$_enbDisResurveySurvey",//api-form-web/get_resurvey_enb",
      headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
  );
  print("getEnabledResurvey ${_response.statusCode}");
  if(_response.statusCode == 200){
    // print("18 2 ${_response.body}");
    final _result = jsonDecode(_response.body);
    final _data = _result['enableDisable'];
    print("getEnabledResurvey $_data");
    return _data;
  }
  else{
    throw Exception("Failed get data error ${_response.statusCode}");
  }
}
// RESURVEY - FORM SURVEY - COMPANY - HIDE SHOW
Future<List> getHideShowComResurvey()async {
  final ioc = new HttpClient();
  ioc.badCertificateCallback =
      (X509Certificate cert, String host, int port) => true;

  final _http = IOClient(ioc);
  String _hideShowResurveySurveyCompany = await storage.read(key: "ResurveySurveyCompanyHideShow");
  final _response = await _http.get(
      "${BaseUrl.urlGeneral}$_hideShowResurveySurveyCompany",//api-form-web/get_resurvey_surveycompany_hs",
      headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
  );
  print("getHideShowComResurvey ${_response.statusCode}");
  if(_response.statusCode == 200){
    final _result = jsonDecode(_response.body);
    final _data = _result['hideshow'];
    print("getHideShowComResurvey $_data");
    return _data;
  }
  else{
    throw Exception("Failed get data error ${_response.statusCode}");
  }
}
// RESURVEY - FORM SURVEY - COMPANY - MANDATORY
Future<List> getMandatoryComResurvey()async {
  final ioc = new HttpClient();
  ioc.badCertificateCallback =
      (X509Certificate cert, String host, int port) => true;

  final _http = IOClient(ioc);
  String _mandatoryResurveySurveyCompany = await storage.read(key: "ResurveySurveyCompanyMandatory");
  final _response = await _http.get(
      "${BaseUrl.urlGeneral}$_mandatoryResurveySurveyCompany",//api-form-web/get_resurvey_surveycompany_man",
      headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
  );
  print("getMandatoryComResurvey ${_response.statusCode}");
  if(_response.statusCode == 200){
    final _result = jsonDecode(_response.body);
    final _data = _result['mandatory'];
    print("getMandatoryComResurvey $_data");
    return _data;
  }
  else{
    throw Exception("Failed get data error ${_response.statusCode}");
  }
}
// RESURVEY - FORM SURVEY - COMPANY - ENABLE DISABLE
Future<List> getEnabledComResurvey()async {
  final ioc = new HttpClient();
  ioc.badCertificateCallback =
      (X509Certificate cert, String host, int port) => true;

  final _http = IOClient(ioc);
  String _enbDisResurveySurveyCompany = await storage.read(key: "ResurveySurveyCompanyEnbDis");
  final _response = await _http.get(
      "${BaseUrl.urlGeneral}$_enbDisResurveySurveyCompany",//api-form-web/get_resurvey_surveycompany_enb",
      headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
  );
  print("getEnabledComResurvey ${_response.statusCode}");
  if(_response.statusCode == 200){
    final _result = jsonDecode(_response.body);
    final _data = _result['enableDisable'];
    print("getEnabledComResurvey $_data");
    return _data;
  }
  else{
    throw Exception("Failed get data error ${_response.statusCode}");
  }
}
// END RESURVEY

// START DAKOR
// DAKOR - FORM M - INDIVIDU - HIDE SHOW
Future<List> getHideShowFormMDakor()async {
  final ioc = new HttpClient();
  ioc.badCertificateCallback =
      (X509Certificate cert, String host, int port) => true;

  final _http = IOClient(ioc);
  String _hideShowDakorM = await storage.read(key: "DakorMHideShow");
  final _response = await _http.get(
      "${BaseUrl.urlGeneral}$_hideShowDakorM",//api-form-web/get_dakor_mindividu_hs",
      headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
  );
  print("getHideShowFormMDakor ${_response.statusCode}");
  if(_response.statusCode == 200){
    final _result = jsonDecode(_response.body);
    final _data = _result['hideshow'];
    print("getHideShowFormMDakor $_data");
    return _data;
  }
  else{
    throw Exception("Failed get data error ${_response.statusCode}");
  }
}
// DAKOR - FORM M - INDIVIDU - MANDATORY
Future<List> getMandatoryFormMDakor()async {
  final ioc = new HttpClient();
  ioc.badCertificateCallback =
      (X509Certificate cert, String host, int port) => true;

  final _http = IOClient(ioc);
  String _mandatoryDakorM = await storage.read(key: "DakorMMandatory");
  final _response = await _http.get(
      "${BaseUrl.urlGeneral}$_mandatoryDakorM",//api-form-web/get_dakor_mindividu_man",
      headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
  );
  print("getMandatoryFormMDakor ${_response.statusCode}");
  if(_response.statusCode == 200){
    final _result = jsonDecode(_response.body);
    final _data = _result['mandatory'];
    print("getMandatoryFormMDakor $_data");
    return _data;
  }
  else{
    throw Exception("Failed get data error ${_response.statusCode}");
  }
}
// DAKOR - FORM M - COMPANY - HIDE SHOW
Future<List> getHideShowFormMDakorCom()async {
  final ioc = new HttpClient();
  ioc.badCertificateCallback =
      (X509Certificate cert, String host, int port) => true;

  final _http = IOClient(ioc);
  String _hideShowDakorMCompany = await storage.read(key: "DakorMCompanyHideShow");
  final _response = await _http.get(
      "${BaseUrl.urlGeneral}$_hideShowDakorMCompany",//api-form-web/get_dakorcomp_hs",
      headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
  );
  print("getHideShowFormMDakorCom ${_response.statusCode}");
  if(_response.statusCode == 200){
    final _result = jsonDecode(_response.body);
    final _data = _result['hideshow'];
    print("getHideShowFormMDakorCom $_data");
    return _data;
  }
  else{
    throw Exception("Failed get data error ${_response.statusCode}");
  }
}
// DAKOR - FORM M - COMPANY - MANDATORY
Future<List> getMandatoryFormMDakorCom()async {
  final ioc = new HttpClient();
  ioc.badCertificateCallback =
      (X509Certificate cert, String host, int port) => true;

  final _http = IOClient(ioc);
  String _mandatoryDakorMCompany = await storage.read(key: "DakorMCompanyMandatory");
  final _response = await _http.get(
      "${BaseUrl.urlGeneral}$_mandatoryDakorMCompany",//api-form-web/get_dakorcomp_man",
      headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
  );
  print("getMandatoryFormMDakorCom ${_response.statusCode}");
  if(_response.statusCode == 200){
    final _result = jsonDecode(_response.body);
    final _data = _result['mandatory'];
    print("getMandatoryFormMDakorCom $_data");
    return _data;
  }
  else{
    throw Exception("Failed get data error ${_response.statusCode}");
  }
}

// DAKOR - FORM APP - INDIVIDU - HIDE SHOW
Future<List> getHideShowAppDakor()async {
  final ioc = new HttpClient();
  ioc.badCertificateCallback =
      (X509Certificate cert, String host, int port) => true;

  final _http = IOClient(ioc);
  String _hideShowDakorApp = await storage.read(key: "DakorAppHideShow");
  final _response = await _http.get(
      "${BaseUrl.urlGeneral}$_hideShowDakorApp",//api-form-web/form_appdakor_hs",
      headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
  );
  print("getHideShowAppDakor ${_response.statusCode}");
  if(_response.statusCode == 200){
    final _result = jsonDecode(_response.body);
    final _data = _result['hideshow'];
    print("getHideShowAppDakor $_data");
    return _data;
  }
  else{
    throw Exception("Failed get data error ${_response.statusCode}");
  }
}
// DAKOR - FORM APP - INDIVIDU - MANDATORY
Future<List> getMandatoryAppDakor()async {
  final ioc = new HttpClient();
  ioc.badCertificateCallback =
      (X509Certificate cert, String host, int port) => true;

  final _http = IOClient(ioc);
  String _mandatoryDakorApp = await storage.read(key: "DakorAppMandatory");
  final _response = await _http.get(
      "${BaseUrl.urlGeneral}$_mandatoryDakorApp",//api-form-web/form_appdakor_man",
      headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
  );
  print("getMandatoryAppDakor ${_response.statusCode}");
  if(_response.statusCode == 200){
    final _result = jsonDecode(_response.body);
    final _data = _result['mandatory'];
    print("getMandatoryAppDakor $_data");
    return _data;
  }
  else{
    throw Exception("Failed get data error ${_response.statusCode}");
  }
}
// DAKOR - FORM APP - COMPANY - HIDE SHOW
Future<List> getHideShowAppDakorCom()async {
  final ioc = new HttpClient();
  ioc.badCertificateCallback =
      (X509Certificate cert, String host, int port) => true;

  final _http = IOClient(ioc);
  String _hideShowDakorAppCompany = await storage.read(key: "DakorAppCompanyHideShow");
  final _response = await _http.get(
      "${BaseUrl.urlGeneral}$_hideShowDakorAppCompany",//api-form-web/form_appdakorcomp_hs",
      headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
  );
  print("getHideShowAppDakorCom ${_response.statusCode}");
  if(_response.statusCode == 200){
    final _result = jsonDecode(_response.body);
    final _data = _result['hideshow'];
    print("getHideShowAppDakorCom $_data");
    return _data;
  }
  else{
    throw Exception("Failed get data error ${_response.statusCode}");
  }
}
// DAKOR - FORM APP - COMPANY - MANDATORY
Future<List> getMandatoryAppDakorCom()async {
  final ioc = new HttpClient();
  ioc.badCertificateCallback =
      (X509Certificate cert, String host, int port) => true;

  final _http = IOClient(ioc);
  String _mandatoryDakorAppCompany = await storage.read(key: "DakorAppCompanyMandatory");
  final _response = await _http.get(
      "${BaseUrl.urlGeneral}$_mandatoryDakorAppCompany",//api-form-web/form_appdakorcomp_man",
      headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
  );
  print("getMandatoryAppDakorCom ${_response.statusCode}");
  if(_response.statusCode == 200){
    final _result = jsonDecode(_response.body);
    final _data = _result['mandatory'];
    print("getMandatoryAppDakorCom $_data");
    return _data;
  }
  else{
    throw Exception("Failed get data error ${_response.statusCode}");
  }
}

// DAKOR - FORM SURVEY - INDIVIDU - HIDE SHOW
Future<List> getHideShowDakor()async {
  print("19 dijalankan");
  final ioc = new HttpClient();
  ioc.badCertificateCallback =
      (X509Certificate cert, String host, int port) => true;

  final _http = IOClient(ioc);
  String _hideShowDakorSurvey = await storage.read(key: "DakorSurveyHideShow");
  final _response = await _http.get(
      "${BaseUrl.urlGeneral}$_hideShowDakorSurvey",//api-form-web/get_dakor_hs",
      headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
  );
  print("getHideShowDakor ${_response.statusCode}");
  if(_response.statusCode == 200){
    final _result = jsonDecode(_response.body);
    final _data = _result['model'];
    print("getHideShowDakor $_data");
    return _data;
  }
  else{
    throw Exception("Failed get data error ${_response.statusCode}");
  }
}
// DAKOR - FORM SURVEY - INDIVIDU - MANDATORY
Future<List> getMandatoryDakor()async {
  final ioc = new HttpClient();
  ioc.badCertificateCallback =
      (X509Certificate cert, String host, int port) => true;

  final _http = IOClient(ioc);
  String _mandatoryDakorSurvey = await storage.read(key: "DakorSurveyMandatory");
  final _response = await _http.get(
      "${BaseUrl.urlGeneral}$_mandatoryDakorSurvey",//api-form-web/get_dakor_man",
      headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
  );
  print("getMandatoryDakor ${_response.statusCode}");
  if(_response.statusCode == 200){
    final _result = jsonDecode(_response.body);
    final _data = _result['model'];
    print("getMandatoryDakor $_data");
    return _data;
  }
  else{
    throw Exception("Failed get data error ${_response.statusCode}");
  }
}
// DAKOR - FORM SURVEY - INDIVIDU - ENABLE DISABLE
Future<List> getEnabledDakor()async {
  final ioc = new HttpClient();
  ioc.badCertificateCallback =
      (X509Certificate cert, String host, int port) => true;

  final _http = IOClient(ioc);
  String _enbDisDakorSurvey = await storage.read(key: "DakorSurveyEnbDis");
  final _response = await _http.get(
      "${BaseUrl.urlGeneral}$_enbDisDakorSurvey",//api-form-web/get_dakor_enb",
      headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
  );
  print("getEnabledDakor ${_response.statusCode}");
  if(_response.statusCode == 200){
    final _result = jsonDecode(_response.body);
    final _data = _result['model'];
    print("getEnabledDakor $_data");
    return _data;
  }
  else{
    throw Exception("Failed get data error ${_response.statusCode}");
  }
}
// DAKOR - FORM SURVEY - COMPANY - HIDE SHOW
Future<List> getHideShowComDakor()async {
  final ioc = new HttpClient();
  ioc.badCertificateCallback =
      (X509Certificate cert, String host, int port) => true;

  final _http = IOClient(ioc);
  String _hideShowDakorSurveyCompany = await storage.read(key: "DakorSurveyCompanyHideShow");
  final _response = await _http.get(
      "${BaseUrl.urlGeneral}$_hideShowDakorSurveyCompany",//api-form-web/get_dakor_surveycompany_hs",
      headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
  );
  print("getHideShowComDakor ${_response.statusCode}");
  if(_response.statusCode == 200){
    final _result = jsonDecode(_response.body);
    final _data = _result['hideshow'];
    print("getHideShowComDakor $_data");
    return _data;
  }
  else{
    throw Exception("Failed get data error ${_response.statusCode}");
  }
}
// DAKOR - FORM SURVEY - COMPANY - MANDATORY
Future<List> getMandatoryComDakor()async {
  final ioc = new HttpClient();
  ioc.badCertificateCallback =
      (X509Certificate cert, String host, int port) => true;

  final _http = IOClient(ioc);
  String _mandatoryDakorSurveyCompany = await storage.read(key: "DakorSurveyCompanyMandatory");
  final _response = await _http.get(
      "${BaseUrl.urlGeneral}$_mandatoryDakorSurveyCompany",//api-form-web/get_dakor_surveycompany_man",
      headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
  );
  print("getMandatoryComDakor ${_response.statusCode}");
  if(_response.statusCode == 200){
    final _result = jsonDecode(_response.body);
    final _data = _result['mandatory'];
    print("getMandatoryComDakor $_data");
    return _data;
  }
  else{
    throw Exception("Failed get data error ${_response.statusCode}");
  }
}
// DAKOR - FORM SURVEY - COMPANY - ENABLE DISABLE
Future<List> getEnabledComDakor()async {
  final ioc = new HttpClient();
  ioc.badCertificateCallback =
      (X509Certificate cert, String host, int port) => true;

  final _http = IOClient(ioc);
  String _enbDisDakorSurveyCompany = await storage.read(key: "DakorSurveyCompanyEnbDis");
  final _response = await _http.get(
      "${BaseUrl.urlGeneral}$_enbDisDakorSurveyCompany",//api-form-web/get_dakor_surveycompany_enb",
      headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
  );
  print("getEnabledComDakor ${_response.statusCode}");
  if(_response.statusCode == 200){
    final _result = jsonDecode(_response.body);
    final _data = _result['enableDisable'];
    print("getEnabledComDakor $_data");
    return _data;
  }
  else{
    throw Exception("Failed get data error ${_response.statusCode}");
  }
}
// END DAKOR