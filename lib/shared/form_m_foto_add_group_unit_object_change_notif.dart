import 'dart:collection';
import 'dart:io';

import 'package:ad1ms2_dev/models/form_m_foto_model.dart';
import 'package:ad1ms2_dev/models/show_mandatory_m_foto_model.dart';
import 'package:ad1ms2_dev/screens/detail_image.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/dashboard/dashboard_change_notif.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:geolocator/geolocator.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';

import '../main.dart';
import 'form_m_foto_change_notif.dart';
import 'group_unit_object_model.dart';

class FormMFotoAddGroupUnitObject with ChangeNotifier {
  GlobalKey<FormState> _key = GlobalKey<FormState>();
  bool _setData = false;
  bool _autoValidateAddGroupUnitObject = false;
  final _imagePicker = ImagePicker();
  GroupObjectUnit _groupObjectUnitSelected;
  GroupObjectUnit _groupObjectUnitTemp;
  List<ObjectUnit> _listObjectUnit = [];
  ObjectUnit _objectUnitSelected;
  ObjectUnit _objectUnitTemp;
  List<ImageFileModel> _listFotoGroupObjectUnit = [];
  int _lengthListFileImage = 0;
  String _pathFile;

  String get pathFile => _pathFile;

  List<GroupObjectUnit> _listGroupObjectUnit = [
    GroupObjectUnit("001", "Motor"),
    GroupObjectUnit("002", "Mobil"),
    GroupObjectUnit("003", "Durable"),
    GroupObjectUnit("007", "Jasa")
  ];

  bool get autoValidateAddGroupUnitObject => _autoValidateAddGroupUnitObject;

  set autoValidateAddGroupUnitObject(bool value) {
    this._autoValidateAddGroupUnitObject = value;
    notifyListeners();
  }

  GroupObjectUnit get groupObjectUnitSelected => _groupObjectUnitSelected;

  set groupObjectUnitSelected(GroupObjectUnit value) {
    this._groupObjectUnitSelected = value;
    notifyListeners();
  }

  void setDataListObjectUnit(BuildContext context, bool status) {
    if (this._groupObjectUnitSelected.id == "001" || this._groupObjectUnitSelected.id == "002") {
      _listObjectUnit = [
        ObjectUnit("011", "Baru"),
        ObjectUnit("012", "Bekas"),
      ];
      if(status) _removeIdentical(context);
    } else if(this._groupObjectUnitSelected.id == "003") {
      _listObjectUnit = [
        ObjectUnit("005", "Durable"),
      ];
    } else {
      _listObjectUnit = [
        ObjectUnit("013", "Jasa"),
      ];
    }
  }

  // void addFile() async { //old
  //   var _image = await _imagePicker.getImage(
  //       source: ImageSource.camera,
  //       maxHeight: 1920.0,
  //       maxWidth: 1080.0,
  //       imageQuality: 10);
  //   savePhoto(_image);
  // }

  void addFileCamera(BuildContext context, int index) async {
    var _image = await _imagePicker.getImage(
        source: ImageSource.camera,
        maxHeight: 480.0,
        maxWidth: 640.0,
        imageQuality: 100);
    savePhoto(context, _image, index);
  }

  void addFileGallery(BuildContext context, int index) async {
    var _image = await _imagePicker.getImage(
        source: ImageSource.gallery,
        maxHeight: 480.0,
        maxWidth: 640.0,
        imageQuality: 100);
    savePhoto(context, _image, index);
  }

  void showBottomSheetChooseFile(BuildContext context, int index) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return Theme(
            data: ThemeData(fontFamily: "NunitoSans"),
            child: Container(
              child: new Wrap(
                children: <Widget>[
                  FlatButton(
                      onPressed: () {
                        addFileCamera(context, index);
                        Navigator.pop(context);
                      },
                      child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Icon(
                              Icons.photo_camera,
                              color: myPrimaryColor,
                              size: 22.0,
                            ),
                            SizedBox(
                              width: 12.0,
                            ),
                            Text("Camera",
                              style: TextStyle(fontSize: 18.0),
                            )
                          ])),
                  FlatButton(
                      onPressed: () {
                        addFileGallery(context, index);
                        Navigator.pop(context);
                      },
                      child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Icon(
                              Icons.photo,
                              color: myPrimaryColor,
                              size: 22.0,
                            ),
                            SizedBox(
                              width: 12.0,
                            ),
                            Text("Gallery",
                              style: TextStyle(fontSize: 18.0),
                            )
                          ])),
                ],
              ),
            ),
          );
        });
  }

  ObjectUnit get objectUnitSelected => _objectUnitSelected;

  set objectUnitSelected(ObjectUnit value) {
    this._objectUnitSelected = value;
    notifyListeners();
  }

  UnmodifiableListView<ObjectUnit> get listObjectUnit {
    List<ObjectUnit> _emptyList = [];
    if (this._groupObjectUnitSelected != null) {
      return UnmodifiableListView(this._listObjectUnit);
    } else {
      return UnmodifiableListView(_emptyList);
    }
  }

  UnmodifiableListView<GroupObjectUnit> get listGroupObjectUnit {
    return UnmodifiableListView(this._listGroupObjectUnit);
  }

  GlobalKey<FormState> get key => _key;

  List<ImageFileModel> get listFotoGroupObjectUnit => _listFotoGroupObjectUnit;

  bool check() {
    final _form = this._key.currentState;
    if (_form.validate()) {
      // jika bekas wajib take photo
      if (this._objectUnitSelected.id == "012") {
        if (this._listFotoGroupObjectUnit.isEmpty) {
          return true;
        } else {
          return false;
        }
      } else {
        return false;
      }
    } else {
      return true;
    }
  }

  void actionImageSelected(String actionSelected, int index,BuildContext context) {
    if (actionSelected == TitlePopUpMenuButton.Delete) {
      deleteFile(index);
      this._listFotoGroupObjectUnit.removeAt(index);
      notifyListeners();
    } else if (actionSelected == TitlePopUpMenuButton.Detail) {
      Navigator.push(context, MaterialPageRoute(builder: (context) => DetailImage(imageFile: this._listFotoGroupObjectUnit[index].imageFile),));
    }
  }

  bool get setData => _setData;

  set setData(bool value) {
    this._setData = value;
    notifyListeners();
  }

  get groupObjectUnitTemp => _groupObjectUnitTemp;

  set groupObjectUnitTemp(GroupObjectUnit value) {
    this._groupObjectUnitTemp = value;
  }

  Future<void> setValueForEdit(BuildContext context, GroupUnitObjectModel groupUnitObjectModel, int flag) async {
    if(flag != 0){
      for (int i = 0; i < this._listGroupObjectUnit.length; i++) {
        if (groupUnitObjectModel.groupObjectUnit.id == this._listGroupObjectUnit[i].id) {
          this._groupObjectUnitSelected = this._listGroupObjectUnit[i];
        }
      }

      this._groupObjectUnitTemp = groupUnitObjectModel.groupObjectUnit;
      this._objectUnitTemp = groupUnitObjectModel.objectUnit;
      setDataListObjectUnit(context, false);

      for (int i = 0; i < this._listObjectUnit.length; i++) {
        if (groupUnitObjectModel.objectUnit.id == this._listObjectUnit[i].id) {
          this._objectUnitSelected = this._listObjectUnit[i];
        }
      }
      if (groupUnitObjectModel.listImageGroupObjectUnit.isNotEmpty) {
        for (int i = 0; i < groupUnitObjectModel.listImageGroupObjectUnit.length;i++){
          this._listFotoGroupObjectUnit.add(groupUnitObjectModel.listImageGroupObjectUnit[i]);
        }
        this._lengthListFileImage = groupUnitObjectModel.listImageGroupObjectUnit.length;
      }
    }
  }

  void _removeIdentical(BuildContext context){
    var _providerListGroupUnit = Provider.of<FormMFotoChangeNotifier>(context, listen: false);
    if(this._groupObjectUnitSelected.id == "001") { //motor
      for (int i = 0; i < _providerListGroupUnit.listGroupUnitObject.length; i++) {
        if(_providerListGroupUnit.listGroupUnitObject[i].groupObjectUnit.id == "001"){
          print("motor");
          if (_providerListGroupUnit.listGroupUnitObject[i].objectUnit.id == "011") {
            _listObjectUnit.removeAt(0);
          }
          else {
            _listObjectUnit.removeAt(1);
          }
        }
      }
    }
    else if(this._groupObjectUnitSelected.id == "002"){ //mobil
      for (int i = 0; i < _providerListGroupUnit.listGroupUnitObject.length; i++) {
        if(_providerListGroupUnit.listGroupUnitObject[i].groupObjectUnit.id == "002"){
          print("mobil");
          if (_providerListGroupUnit.listGroupUnitObject[i].objectUnit.id == "011") {
            _listObjectUnit.removeAt(0);
          }
          else {
            _listObjectUnit.removeAt(1);
          }
        }
      }
    }
  }

  get objectUnitTemp => _objectUnitTemp;

  set objectUnitTemp(ObjectUnit value) {
    this._objectUnitTemp = value;
  }

  int get lengthListFileImage => _lengthListFileImage;

  void savePhoto(BuildContext context, PickedFile data, int index) async{
    var _providerFoto = Provider.of<FormMFotoChangeNotifier>(context, listen: false);
    int _listIndex = index != null ? index+1 : _providerFoto.listGroupUnitObject.length+1;
    String _date = formatDateListOID(DateTime.now()).replaceAll("-", "");
    String _time = formatTime(DateTime.now()).replaceAll(":", "");
    try{
      if (data != null) {
        File _imageFile = File(data.path);
        //resize img
        final filePath = _imageFile.absolute.path;
        final lastIndex = filePath.lastIndexOf(new RegExp(r'.jp'));
        final splitted = filePath.substring(0, (lastIndex));
        final outPath = "${splitted}_out${filePath.substring(lastIndex)}";
        File _result = await FlutterImageCompress.compressAndGetFile(
          _imageFile.absolute.path, outPath,
          quality: 50,
        );
        // List<String> split = data.path.split("/");
        // String fileName = split[split.length-1];
        // File _fileLocation = await _imageFile.copy("$globalPath/FOTO_GROUP_UNIT_${_listIndex}_${this._listFotoGroupObjectUnit.length+1}.JPG");
        File _fileLocation = await _result.copy("$globalPath/FOTO_GROUP_UNIT_${_date}_$_time.JPG");
        _pathFile = _fileLocation.path;
        Position _position = await Geolocator().getCurrentPosition(desiredAccuracy: LocationAccuracy.bestForNavigation);
        this._listFotoGroupObjectUnit.add(ImageFileModel(_fileLocation, _position.latitude, _position.longitude, _pathFile, ""));
        if (this._autoValidateAddGroupUnitObject)
          this._autoValidateAddGroupUnitObject = false;
        notifyListeners();
      } else {
        return;
      }
//      Uint8List bytes = this._fileDocumentUnitObject['file'].readAsBytesSync();
//      await file.writeAsBytes(bytes);
    }
    catch(e){
      print(e);
    }
  }

  void deleteFile(int index) async{
    final file =  File("${this._listFotoGroupObjectUnit[index].path}");
    await file.delete();
  }

  ShowMandatoryFotoModel _showMandatoryFotoModel;

  void setShowMandatoryFotoModel(BuildContext context){
    _showMandatoryFotoModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryFotoModel;
  }

  bool isFotoGroupUnitObjectMandatory() => _showMandatoryFotoModel.isFotoGroupUnitObjectMandatory;
}
