import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/brand_object_model.dart';
import 'package:ad1ms2_dev/models/brand_type_model_genre_model.dart';
import 'package:ad1ms2_dev/models/model_object_model.dart';
import 'package:ad1ms2_dev/models/object_type_model.dart';
import 'package:ad1ms2_dev/models/object_usage_model.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_object_unit_change_notifier.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/form_m_foto_change_notif.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/io_client.dart';
import 'package:provider/provider.dart';

class SearchBrandObjectChangeNotifier with ChangeNotifier {
  bool _showClear = false;
  TextEditingController _controllerSearch = TextEditingController();
  List<BrandObjectModel> _listBrandObject = [];
  List<BrandObjectModel> _listBrandObjectTemp = [];
  List<BrandTypeModelGenreModel> _listBrandTypeModelGenreModel = [];
  List<BrandTypeModelGenreModel> _listBrandTypeModelGenreModelTemp = [];
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  bool _loadData = false;

  TextEditingController get controllerSearch => _controllerSearch;

  bool get showClear => _showClear;

  void addDataBrandObject(String id) {
    // print(id);
    // if (id == "001") {
    //   _listBrandObject.add(BrandObjectModel("01", "Jepang", "HONDA"));
    //   _listBrandObject.add(BrandObjectModel("02", "Jepang", "YAMAHA"));
    //   _listBrandObject.add(BrandObjectModel("03", "China", "HAPPY"));
    //   _listBrandObject.add(BrandObjectModel("04", "China", "JIALING"));
    //   _listBrandObject.add(
    //     BrandObjectModel("05", "Korea", "KANZEN"),
    //   );
    //   _listBrandObject.add(BrandObjectModel("06", "Korea", "SANEX"));
    // } else if (id == "002") {
    //   _listBrandObject.add(BrandObjectModel("11", "Jepang", "MITSUBISHI"));
    //   _listBrandObject.add(BrandObjectModel("12", "Jepang", "TOYOTA"));
    //   _listBrandObject.add(BrandObjectModel("13", "Jepang", "ISUZU"));
    //   _listBrandObject.add(BrandObjectModel("14", "Jepang", "NISSAN"));
    //   _listBrandObject.add(BrandObjectModel("15", "Jepang", "DAIHATSU"));
    //   _listBrandObject.add(BrandObjectModel("16", "Jepang", "MAZDA"));
    // }
  }

  set showClear(bool value) {
    this._showClear = value;
    notifyListeners();
  }

  void changeAction(String value) {
    if (value != "") {
      showClear = true;
    } else {
      showClear = false;
    }
  }

  bool get loadData => _loadData;

  set loadData(bool value) {
    this._loadData = value;
    notifyListeners();
  }

  void _getBrandObject(BuildContext context, int flagByBrandModelType, String flag, String query, String kodeGroupObject, String kodeObject, String prodMatrix) async{
    var _providerInfoObject = Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false);
    var _providerFoto = Provider.of<FormMFotoChangeNotifier>(context,listen: false);
    this._listBrandTypeModelGenreModel.clear();
    loadData = true;
    final ioc = new HttpClient();
    ioc.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;

    final _http = IOClient(ioc);
    var _body = jsonEncode({
      "P_FLAG" : "$flagByBrandModelType", // 1. brand 2. type 3. model
      "P_OBJECT_GROUP_ID" : kodeGroupObject, // group objek "${_providerInfoObject.groupObjectSelected.KODE}"
      "P_OBJECT_ID": kodeObject, // "${_providerInfoObject.objectSelected.id}"
      "P_OJK_BUSS_DETAIL_ID": flag == "COM" ? "${_providerInfoObject.businessActivitiesTypeModelSelected.id}" : "${_providerFoto.jenisKegiatanUsahaSelected.id}",
      "P_OJK_BUSS_ID" : flag == "COM" ? "${_providerInfoObject.businessActivitiesModelSelected.id}" : "${_providerFoto.kegiatanUsahaSelected.id}",
      "P_PROD_MATRIX_ID" : prodMatrix, // prod matrix id "${_providerInfoObject.prodMatrixId}"
      "P_SEARCH" : "$query" // search query
    });

    print("body brand object $_body");

    var storage = FlutterSecureStorage();
    String _fieldModelObjek = await storage.read(key: "FieldModelObjek");
    final _response = await _http.post(
      "${BaseUrl.urlGeneral}$_fieldModelObjek",
        // "${urlPublic}api/parameter/get-brand-type-model-genre",
        body: _body,
        headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
    );
    if(_response.statusCode == 200){
      final _result = jsonDecode(_response.body);
      final _data = _result['data'];
      print("result brand object = $_data");
      if(_data.isEmpty){
        showSnackBar("Merk Objek tidak ditemukan");
        loadData = false;
      }
      else{
        for(int i=0; i <_data.length; i++){
          this._listBrandTypeModelGenreModel.add(
              BrandTypeModelGenreModel(
                  BrandObjectModel(_data[i]['BRAND_ID'],_data[i]['BRAND_NAME']),
                  ObjectTypeModel(_data[i]['TYPE_ID'],_data[i]['TYPE_NAME']),
                  ModelObjectModel(_data[i]['MODEL_ID'],_data[i]['MODEL_NAME']),
                  ObjectUsageModel(_data[i]['GENRE_ID'],_data[i]['GENRE_NAME'])
              )
          );
        }
        loadData = false;
      }
    }
    else{
      showSnackBar("Error response status ${_response.statusCode}");
      this._loadData = false;
    }
    notifyListeners();
  }

  void searchBrandObject(BuildContext context, int flagByBrandModelType, String flag, String query, String kodeGroupObject, String kodeObject, String prodMatrix) {
    if(query.length < 3) {
      showSnackBar("Input minimal 3 karakter");
    } else {
      _getBrandObject(context, flagByBrandModelType, flag, query, kodeGroupObject, kodeObject, prodMatrix);
    }
    // notifyListeners();
  }

  void clearSearchTemp() {
    _listBrandObjectTemp.clear();
  }

  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  void showSnackBar(String text){
    this._scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor));
  }

  UnmodifiableListView<BrandObjectModel> get listBrandObject {
    return UnmodifiableListView(this._listBrandObject);
  }

  UnmodifiableListView<BrandTypeModelGenreModel> get listBrandTypeModelGenreModel {
    return UnmodifiableListView(this._listBrandTypeModelGenreModel);
  }
}
