import 'dart:convert';
import 'dart:io';
import 'package:ad1ms2_dev/screens/dashboard.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:http/io_client.dart';
import '../main.dart';

class ChangePasswordChangeNotifier with ChangeNotifier {
    TextEditingController _controllerOldPassword = TextEditingController();
    TextEditingController _controllerNewPassword = TextEditingController();
    TextEditingController _controllerConfirmNewPassword = TextEditingController();
    bool _secureText = true;
    bool _secureTextNewPass = true;
    bool _secureTextConfirm = true;
    bool _autoValidate = false;
    bool _changePasswordProcess = false;
    GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
    GlobalKey<FormState> _key = GlobalKey<FormState>();

    TextEditingController get controllerOldPassword => _controllerOldPassword;
    TextEditingController get controllerNewPassword => _controllerNewPassword;
    TextEditingController get controllerConfirmNewPassword => _controllerConfirmNewPassword;
    bool get secureText => _secureText;
    bool get secureTextNewPass => _secureTextNewPass;
    bool get secureTextConfirm => _secureTextConfirm;
    bool get changePasswordProcess => _changePasswordProcess;

    set changePasswordProcess(bool value) {
        _changePasswordProcess = value;
        notifyListeners();
    }

    set secureText(bool value) {
        this._secureText = value;
        notifyListeners();
    }

    set secureTextNewPass(bool value) {
        this._secureTextNewPass = value;
        notifyListeners();
    }

    set secureTextConfirm(bool value) {
        this._secureTextConfirm = value;
        notifyListeners();
    }

    bool get autoValidate => _autoValidate;

    set autoValidate(bool value) {
        this._autoValidate = value;
        notifyListeners();
    }

    GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;
    GlobalKey<FormState> get key => _key;

    showHidePass() {
        _secureText = !_secureText;
        notifyListeners();
    }

    showHidePassNewPass() {
        _secureTextNewPass = !_secureTextNewPass;
        notifyListeners();
    }

    showHidePassConfirm() {
        _secureTextConfirm = !_secureTextConfirm;
        notifyListeners();
    }

    check(BuildContext context) async{
        changePasswordProcess = true;
        final ioc = new HttpClient();
        ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
        final _http = IOClient(ioc);
        final _form = _key.currentState;
//        var _body = jsonEncode({
//            "login":_controllerEmail.text,
//            "password": _controllerPassword.text
//        });
//        print(_body);
    try{
      if(_form.validate()){
        final _response = await _http.post(
          "https://103.110.89.34/public/ms2dev/api/account/authuser",
          body: {
            "oldPassword":_controllerOldPassword.text,
            "password": _controllerNewPassword.text
          },
           headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
        );
        print(jsonDecode(_response.body));
        final _result = jsonDecode(_response.body);
        if(_result['result']){
          print("sukses");
          Navigator.of(context).pushReplacement(MaterialPageRoute(
              builder: (context) => Dashboard()));
        } else {
          print("gagal");
          showSnackBar(_result['message']);
        }
      } else {
        autoValidate = true;
      }
    } catch(e){
      print(e);
      showSnackBar(e);
    }
    changePasswordProcess = false;
  }

    showSnackBar(String text){
        this._scaffoldKey.currentState.showSnackBar(new SnackBar(
            content: Text("$text"), behavior: SnackBarBehavior.floating,
            backgroundColor: snackbarColor, duration: Duration(seconds: 2))
        );
    }

}
