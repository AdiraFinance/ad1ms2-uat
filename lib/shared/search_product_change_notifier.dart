import 'dart:collection';
import 'dart:convert';
import 'dart:io';
import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/product_model.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_object_unit_change_notifier.dart';
import 'change_notifier_app/information_collatelar_change_notifier.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/io_client.dart';
import 'package:provider/provider.dart';

class SearchProductChangeNotifier with ChangeNotifier {
    bool _showClear = false;
    bool _loadData = false;
    GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
    TextEditingController _controllerSearch = TextEditingController();

    List<ProductModel> _listProduct = [
        // ProductModel("01", "Product A"),
        // ProductModel("02", "Product B"),
        // ProductModel("03", "Product C")
    ];
    List<ProductModel> _listProductTemp = [];

    TextEditingController get controllerSearch => _controllerSearch;

    bool get showClear => _showClear;

    set showClear(bool value) {
        this._showClear = value;
        notifyListeners();
    }

    bool get loadData => _loadData;

    set loadData(bool value) {
        this._loadData = value;
    }

    GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

    void changeAction(String value) {
        if (value != "") {
            showClear = true;
        } else {
            showClear = false;
        }
    }

    UnmodifiableListView<ProductModel> get listProductModel {
        return UnmodifiableListView(this._listProduct);
    }

    UnmodifiableListView<ProductModel> get listProductTemp {
        return UnmodifiableListView(this._listProductTemp);
    }

    void getProduct(BuildContext context, String flag, String company) async{
        var _providerUnit = Provider.of<InformationObjectUnitChangeNotifier>(context, listen: false);
        var _providerColla = Provider.of<InformationCollateralChangeNotifier>(context, listen: false);
        this._listProduct.clear();
        loadData = true;
        final ioc = new HttpClient();
        ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
        final _http = IOClient(ioc);
        var _body = jsonEncode({
            "P_INSR_COMPANY_CRIT_HDR_ID": company,
            "P_INSR_LEVEL": flag,
            "P_PARA_OBJECT_ID": flag == "1" ? _providerUnit.objectSelected.id : _providerColla.objectSelected.id
        });
        print("body product asurance $_body");

        var storage = FlutterSecureStorage();
        String _fieldProduk = await storage.read(key: "FieldProduk");
        final _response = await _http.post(
            "${BaseUrl.urlGeneral}$_fieldProduk",
            // "${urlPublic}api/asuransi/get-insurance-product",
            body: _body,
            headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
        );

        if(_response.statusCode == 200){
            final _result = jsonDecode(_response.body);
            if(_result['data'].isNotEmpty){
                for(int i = 0 ; i < _result['data'].length ; i++) {
                    _listProduct.add(ProductModel(_result['data'][i]['KODE'], _result['data'][i]['DESKRIPSI']));
                }
            }
            this._loadData = false;
        } else {
            showSnackBar("Error response status ${_response.statusCode}");
            this._loadData = false;
        }
        notifyListeners();
    }

    void searchProduct(String query) {
        if(query.length < 3) {
            showSnackBar("Input minimal 3 karakter");
        } else {
            _listProductTemp.clear();
            if (query.isEmpty) {
                return;
            }

            _listProduct.forEach((dataProduct) {
                if (dataProduct.KODE.contains(query) || dataProduct.DESKRIPSI.contains(query)) {
                    _listProductTemp.add(dataProduct);
                }
            });
        }
        notifyListeners();
    }

    void clearSearchTemp() {
        _listProductTemp.clear();
    }

    void showSnackBar(String text){
        this._scaffoldKey.currentState.showSnackBar(new SnackBar(
            content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor));
    }
}
