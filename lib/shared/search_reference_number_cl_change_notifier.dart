import 'dart:async';
import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/models/check_mpl_activation_model.dart';
import 'package:ad1ms2_dev/models/list_disbursement_model.dart';
import 'package:ad1ms2_dev/models/list_voucher_model.dart';
import 'package:ad1ms2_dev/models/reference_number_cl_model.dart';
import 'package:ad1ms2_dev/models/reference_number_model.dart';
import 'package:ad1ms2_dev/models/product_lme_model.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_object_unit_change_notifier.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/form_m_credit_limit_change_notifier.dart';
import 'package:ad1ms2_dev/shared/format_currency.dart';
import 'package:ad1ms2_dev/shared/list_oid_change_notifier.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/io_client.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../main.dart';

class SearchReferenceNumberCLChangeNotifier with ChangeNotifier {
  bool _showClear = false;
  bool _loadData = false;
  FormatCurrency _formatCurrency = FormatCurrency();
  TextEditingController _controllerSearch = TextEditingController();
  List<ListVoucher> _listVoucherCL = [];
  List<ListDisbursement> _listDisbursementCL = [];
  List<CheckMplActivationModel> _listCheckMplActivation = [];
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  TextEditingController get controllerSearch => _controllerSearch;
  var storage = FlutterSecureStorage();
  List<ProductLMEModel> _listProductLME = ProductLMEList().productLMEList;

  UnmodifiableListView<ProductLMEModel> get listProductLME {
    return UnmodifiableListView(this._listProductLME);
  }

  bool get showClear => _showClear;

  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  FormatCurrency get formatCurrency => _formatCurrency;

  set showClear(bool value) {
    this._showClear = value;
    notifyListeners();
  }

  bool get loadData => _loadData;

  set loadData(bool value) {
    this._loadData = value;
    notifyListeners();
  }

  void changeAction(String value) {
    if (value != "") {
      showClear = true;
    } else {
      showClear = false;
    }
  }

  UnmodifiableListView<ListDisbursement> get listDisbursementCL {
    return UnmodifiableListView(this._listDisbursementCL);
  }

  UnmodifiableListView<ListVoucher> get listVoucherCL {
    return UnmodifiableListView(this._listVoucherCL);
  }

  UnmodifiableListView<CheckMplActivationModel> get listCheckMplActivation {
    return UnmodifiableListView(this._listCheckMplActivation);
  }

  // void getToken(BuildContext context, String query) async {
  //   loadData = true;
  //   final ioc = new HttpClient();
  //   ioc.badCertificateCallback =
  //       (X509Certificate cert, String host, int port) => true;
  //
  //   final _http = IOClient(ioc);
  //
  //   var _body = jsonEncode({
  //     "grant_type": "client_credentials",
  //   });
  //
  //   String _username = 'ad1gate';
  //   String _password = 'ad1gatePassw0rd';
  //   String basicAuth = 'Basic ' + base64Encode(utf8.encode('$_username:$_password'));
  //   String _creditLimitToken = await storage.read(key: "CreditLimitToken");
  //   final _response = await _http.post(
  //       "${BaseUrl.urlLME}$_creditLimitToken",
  //       // "${urlLme}oauth/token",
  //       body: _body,
  //       headers: {
  //         "Content-Type":"application/x-www-form-urlencoded",
  //         "Authorization":"bearer $token"
  //       }
  //   );
  //
  //   if(_response.statusCode == 200){
  //     final _result = jsonDecode(_response.body);
  //     print(_result);
  //     if(_result.isNotEmpty){
  //       token = _result['access_token'];
  //       loadData = false;
  //       getReferenceNumberCL(context, "", query);
  //     }
  //     else{
  //       showSnackBar("Token gagal didapatkan");
  //       loadData = false;
  //     }
  //   }
  //   else{
  //     showSnackBar("Error get token response ${_response.statusCode}");
  //     loadData = false;
  //   }
  // }

  void getReferenceNumberCL(BuildContext context, String jenisPenawaran, String query) async{
    // var _dataTypeOffer = Provider.of<FormMCreditLimitChangeNotifier>(context, listen: false).typeOfferSelected.KODE;
    if(jenisPenawaran == "001") {
      // Inq Voucher
      // if(query.length < 3) {
      //   showSnackBar("Input minimal 3 karakter");
      // } else {
        var _date = dateFormat3.format(DateTime.now());
        SharedPreferences _preference = await SharedPreferences.getInstance();

        loadData = true;
        final ioc = new HttpClient();
        ioc.badCertificateCallback =
            (X509Certificate cert, String host, int port) => true;

        final _http = IOClient(ioc);
        var _oid = Provider.of<ListOidChangeNotifier>(context, listen: false).listOIDPersonalSelected.AC_CUST_ID.toString();

        var _body = jsonEncode({
          "oid": _oid,
          "source_reff_id": "MS2${_preference.getString("username")}-$_oid",
          "source_channel_id": "REVAMP",
          "req_date": _date.toString()
        });
        // {
        //   "oid" : Provider.of<ListOidChangeNotifier>(context, listen: false).listOIdPersonalSelected.AC_CUST_ID,
        //   "source_reff_id" : "MS2${_preference.getString("username")}",
        //   "req_date" : dateFormat3.format(DateTime.now()),
        //   "source_channel_id" : "Ad1ms2"
        // }
        print("body inq voucher: $_body");
        String _creditLimitinqVoucher = await storage.read(key: "CreditLimitInqVoucher");
        try {
          final _response = await _http.post(
              "${BaseUrl.urlLME}$_creditLimitinqVoucher",
              // "${urlLme}InqService/inqVoucher",
              body: _body,
              headers: {
                "Content-Type":"application/json", "Authorization":"bearer $token"
                // "Authorization":"Bearer $token"
              }
          ).timeout(Duration(seconds: 10));

          if(_response.statusCode == 200){
            final _result = jsonDecode(_response.body);
            print("result inq voucher: $_result");
            if(_result['response_code'] == "00") {
              if(_result.isNotEmpty){
                List<String> _productIdDesc = [];
                for(int i=0; i < _result['list_voucher'].length; i++) {
                  for(int j=0; j < this._listProductLME.length; j++) {
                    if(this._listProductLME[j].productId == _result['list_voucher'][i]['product_type'].toString()) {
                      _productIdDesc.add(this._listProductLME[j].productDesc);
                    }
                  }
                }
                for(int i = 0; i < _result['list_voucher'].length; i++){
                  print("list: ${_result['list_voucher'][i]['voucher_code']}");
                  this._listVoucherCL.add(ListVoucher(
                    _result['list_voucher'][i]['voucher_code'].toString(),
                    _result['list_voucher'][i]['contract_no'].toString(),
                    _result['list_voucher'][i]['product_type'].toString(),
                    _productIdDesc[i],
                    _result['list_voucher'][i]['plafond_installment'].toString(),
                    _result['list_voucher'][i]['tenor'].toString(),
                    _result['list_voucher'][i]['disburse_type'].toString(),
                    _result['list_voucher'][i]['start_date'].toString(),
                    _result['list_voucher'][i]['end_date'].toString()
                  ));
                }
                loadData = false;
              }
              else{
                showSnackBar("Nomor referensi tidak ditemukan");
                loadData = false;
              }
            } else {
              showSnackBar("${_result['response_desc']}");
              loadData = false;
            }
          }
          else{
            showSnackBar("Error get nomor referensi response ${_response.statusCode}");
            loadData = false;
          }
        }
        on TimeoutException catch(_){
          showSnackBar("Request Timeout");
          this._loadData = false;
        }
        catch(e){
          showSnackBar("Error $e");
          this._loadData = false;
        }
      // }
    }
    else if(jenisPenawaran == "002") {
      // Check Mpl Activation
      // if(query.length < 3) {
      //   showSnackBar("Input minimal 3 karakter");
      // } else {
        this._listCheckMplActivation.clear();
        var _date = dateFormat3.format(DateTime.now());
        SharedPreferences _preference = await SharedPreferences.getInstance();

        loadData = true;
        final ioc = new HttpClient();
        ioc.badCertificateCallback =
            (X509Certificate cert, String host, int port) => true;

        final _http = IOClient(ioc);
        var _oid = Provider.of<ListOidChangeNotifier>(context, listen: false).listOIDPersonalSelected.AC_CUST_ID.toString();

        try {
          var _body = jsonEncode({
            "oid": _oid,
            "lme_id": "",
            "branch_code": _preference.getString("branchid").toString(),
            "source_reff_id": "MS2${_preference.getString("username")}-$_oid",
            "req_date": _date.toString(),
            "source_channel_id": "MS2"
          });
          print("body check mpl activation: $_body");
          String _creditLimitCHeckMplActivation = await storage.read(key: "CreditLimitCheckMplActivation");
          final _response = await _http.post(
              "${BaseUrl.urlLME}$_creditLimitCHeckMplActivation",
              // "${urlLme}InqService/checkMplActivation",
              body: _body,
              headers: {
                "Content-Type":"application/json", "Authorization":"bearer $token"
                // "Authorization":"Bearer $token"
              }
          ).timeout(Duration(seconds: 10));

          if(_response.statusCode == 200){
            final _result = jsonDecode(_response.body);
            if(_result['response_code'] == "00") {
              if(_result.isNotEmpty){
                for(int i = 0; i < _result['list_disbursement'].length; i++){
                  this._listDisbursementCL.add(ListDisbursement(
                    _result['list_disbursement'][i]['disbursement_no'].toString(),
                    _result['list_disbursement'][i]['application_no'].toString(),
                    _result['list_disbursement'][i]['installment_amount'].toString(),
                    _result['list_disbursement'][i]['product_id'].toString(),
                    _result['list_disbursement'][i]['ph_amount'].toString(),
                    _result['list_disbursement'][i]['tenor'].toString(),
                    _result['list_disbursement'][i]['elligible_status'].toString(),
                    _result['list_disbursement'][i]['grading_nasabah'].toString(),
                    _result['list_disbursement'][i]['disbursement_date'].toString(),
                    _result['list_disbursement'][i]['disbursement_channel'].toString(),
                    _result['list_disbursement'][i]['transaction_status'].toString()
                  ));
                }
                loadData = false;
              }
              else{
                showSnackBar("No reference tidak ditemukan");
                loadData = false;
              }
            } else {
              showSnackBar("${_result['response_desc']}");
              loadData = false;
            }
          }
          else{
            showSnackBar("Error get no reference response ${_response.statusCode}");
            loadData = false;
          }
        }
        on TimeoutException catch(_){
          showSnackBar("Request Timeout");
          this._loadData = false;
        }
        catch(e){
          showSnackBar("Error $e");
          this._loadData = false;
        }
      // }
    }
    notifyListeners();
  }

  void showSnackBar(String text){
    this._scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor));
  }
}
