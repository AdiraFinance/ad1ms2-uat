import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/form_m_company_alamat_model.dart';
import 'package:ad1ms2_dev/models/form_m_informasi_alamat_model.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/io_client.dart';

class SearchKelurahanChangeNotif with ChangeNotifier {
  bool _showClear = false;
  bool _loadData = false;
  TextEditingController _controllerSearch = TextEditingController();
  List<KelurahanModel> _listKelurahan = [];
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  void getAddressFull(String query) async{
    this._listKelurahan.clear();
    this._loadData = true;
    final ioc = new HttpClient();
    ioc.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;

    final _http = IOClient(ioc);
    var _body = jsonEncode({
      "P_FLAG":"4",
      "P_SEARCH": query
    });

    var storage = FlutterSecureStorage();
    String _fieldKelurahan = await storage.read(key: "FieldKelurahan");
    final _response = await _http.post(
      "${BaseUrl.urlGeneral}$_fieldKelurahan",
        // "${urlPublic}api/address/get-alamat-full",
        body: _body,
        headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
    );
    if(_response.statusCode == 200){
      final _result = jsonDecode(_response.body);
      final _data = _result['data'];
      if(_data.isEmpty){
        showSnackBar("Address not found");
        this._loadData = false;
      }
      else{
        for(int i=0; i <_data.length; i++){
          _listKelurahan.add(
              KelurahanModel(_data[i]['KEL_ID'], _data[i]['KEL_NAME'].toString().replaceAll("'", "`"), _data[i]['KEC_NAME'].toString().replaceAll("'", "`"),
                  _data[i]['KABKOT_NAME'].toString().replaceAll("'", "`"), _data[i]['PROV_NAME'].toString().replaceAll("'", "`"), _data[i]['ZIPCODE'],
                  _data[i]['KEC_ID'], _data[i]['KABKOT_ID'], _data[i]['PROV_ID']
              )
          );
        }
        this._loadData = false;
      }
    }
    else{
      showSnackBar("Error response status ${_response.statusCode}");
      this._loadData = false;
    }
    notifyListeners();
  }

  TextEditingController get controllerSearch => _controllerSearch;

  bool get showClear => _showClear;

  set showClear(bool value) {
    this._showClear = value;
    notifyListeners();
  }

  void changeAction(String value) {
    if (value != "") {
      showClear = true;
    } else {
      showClear = false;
    }
  }

  UnmodifiableListView<KelurahanModel> get listKelurahan {
    return UnmodifiableListView(this._listKelurahan);
  }

  void showSnackBar(String text){
    this._scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor));
  }

  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  bool get loadData => _loadData;

  set loadData(bool value) {
    this._loadData = value;
    notifyListeners();
  }
}
