import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/models/form_m_informasi_alamat_model.dart';
import 'package:ad1ms2_dev/screens/form_m/search_kelurahan.dart';
import 'package:ad1ms2_dev/shared/form_m_occupation_change_notif.dart';
import 'package:ad1ms2_dev/shared/search_kelurahan_change_notif.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:http/io_client.dart';
import 'package:provider/provider.dart';

//tidak terpakai
class FormMAddOccupationAddressChangeNotifier with ChangeNotifier {
  bool _autoValidate = false;
  bool _isSameWithIdentity = false;
  TextEditingController _controllerAlamat = TextEditingController();
  TextEditingController _controllerRT = TextEditingController();
  TextEditingController _controllerRW = TextEditingController();
  TextEditingController _controllerKelurahan = TextEditingController();
  TextEditingController _controllerKecamatan = TextEditingController();
  TextEditingController _controllerKota = TextEditingController();
  TextEditingController _controllerProvinsi = TextEditingController();
  TextEditingController _controllerKodeArea = TextEditingController();
  TextEditingController _controllerTlpn = TextEditingController();
  TextEditingController _controllerPostalCode = TextEditingController();
  JenisAlamatModel _jenisAlamatSelected;
  JenisAlamatModel _jenisAlamatSelectedTemp;
  KelurahanModel _kelurahanSelected;
  KelurahanModel _kelurahanSelectedTemp;
  AddressModel _addressModelTemp;
  String _alamatTemp,
      _rtTemp,
      _rwTemp,
      _kelurahanTemp,
      _kecamatanTemp,
      _kotaTemp,
      _provinsiTemp,
      _areaCodeTemp,
      _phoneTemp,
      _postalCodeTemp;
  GlobalKey<FormState> _key = GlobalKey<FormState>();
  bool _enableTfAddress = true;
  bool _enableTfRT = true;
  bool _enableTfRW = true;
  bool _enableTfKelurahan = true;
  bool _enableTfKecamatan = true;
  bool _enableTfKota = true;
  bool _enableTfProv = true;
  bool _enableTfPostalCode = true;
  bool _enableTfAreaCode = true;
  bool _enableTfPhone = true;
  bool _isCorrespondence;
  Map _addressFromMap;

  List<JenisAlamatModel> _listJenisAlamat = [];

  Future<void> addDataListAddressType(AddressModel data, BuildContext context, int flag, bool isSameWithIdentity,int index) async{
    var _provider = Provider.of<FormMOccupationChangeNotif>(context, listen: false);
    try {
      this._listJenisAlamat.clear();
      final ioc = new HttpClient();
      ioc.badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;

      final _http = IOClient(ioc);
      final _response = await _http.get("https://103.110.89.34/public/ms2dev/api/address/get-address-type");
      final _data = jsonDecode(_response.body);
      if (flag == 0) {
        print(" di add $flag");
        // add
        if (_provider.listOccupationAddress.isEmpty) {
          for(int i=0; i < _data['data'].length;i++){
            if(_data['data'][i]['KODE'] == "03"){
              _listJenisAlamat.add(JenisAlamatModel(_data['data'][i]['KODE'], _data['data'][i]['DESKRIPSI']));
            }
          }
        }
        else {
          for(int i=0; i < _data['data'].length;i++){
            if(_data['data'][i]['KODE'] != "03"){
              _listJenisAlamat.add(JenisAlamatModel(_data['data'][i]['KODE'], _data['data'][i]['DESKRIPSI']));
            }
          }
        }
      }
      else {
        // edit
        if (_provider.listOccupationAddress[index].jenisAlamatModel.KODE == '03') {
          for(int i=0; i < _data['data'].length;i++){
            if(_data['data'][i]['KODE'] == "03"){
              _listJenisAlamat.add(JenisAlamatModel(_data['data'][i]['KODE'], _data['data'][i]['DESKRIPSI']));
            }
          }
        } else {
          for(int i=0; i < _data['data'].length;i++){
            if(_data['data'][i]['KODE'] != "03"){
              _listJenisAlamat.add(JenisAlamatModel(_data['data'][i]['KODE'], _data['data'][i]['DESKRIPSI']));
            }
          }
        }
        setValueForEdit(data, context, isSameWithIdentity);
      }
      notifyListeners();
    } catch (e) {
      print(e);
    }
  }

//  void addDataListAddressType(BuildContext context, int index) {
//    var _provider =
//        Provider.of<FormMOccupationChangeNotif>(context, listen: false);
//    if (index == null) {
//      if (_provider.listOccupationAddress.isEmpty) {
//        _listJenisAlamat.add(JenisAlamatModel("01", "Identitas"));
//      } else {
////        bool _identitasIsExist = false;
////        for (int i = 0; i < _provider.listOccupationAddress.length; i++) {
////          if (_provider.listOccupationAddress[i].jenisAlamatModel.id == "01") {
////            _identitasIsExist = true;
////          }
////        }
////        if (_identitasIsExist) {
//        _listJenisAlamat = [
//          JenisAlamatModel("02", "Domisili 1"),
//          JenisAlamatModel("03", "Domisili 2"),
//          JenisAlamatModel("04", "Domisili 3"),
//          JenisAlamatModel("05", "Kantor 1"),
//          JenisAlamatModel("05", "Kantor 2"),
//          JenisAlamatModel("07", "Kantor 3")
//        ];
////        } else {
////          _listJenisAlamat.add(JenisAlamatModel("01", "Identitas"));
////        }
//      }
//    } else {
//      if (_provider.listOccupationAddress[index].jenisAlamatModel.KODE == '01') {
//        _listJenisAlamat.add(JenisAlamatModel("01", "Identitas"));
//      } else {
//        _listJenisAlamat = [
//          JenisAlamatModel("02", "Domisili 1"),
//          JenisAlamatModel("03", "Domisili 2"),
//          JenisAlamatModel("04", "Domisili 3"),
//          JenisAlamatModel("05", "Kantor 1"),
//          JenisAlamatModel("05", "Kantor 2"),
//          JenisAlamatModel("07", "Kantor 3")
//        ];
//      }
//    }
//  }

  bool get autoValidate => _autoValidate;

  set autoValidate(bool value) {
    this._autoValidate = value;
    notifyListeners();
  }

  TextEditingController get controllerProvinsi => _controllerProvinsi;

  TextEditingController get controllerKota => _controllerKota;

  TextEditingController get controllerKecamatan => _controllerKecamatan;

  TextEditingController get controllerKelurahan => _controllerKelurahan;

  TextEditingController get controllerRW => _controllerRW;

  TextEditingController get controllerRT => _controllerRT;

  TextEditingController get controllerAlamat => _controllerAlamat;

  TextEditingController get controllerKodeArea => _controllerKodeArea;

  TextEditingController get controllerTlpn => _controllerTlpn;

  TextEditingController get controllerPostalCode => _controllerPostalCode;

  UnmodifiableListView<JenisAlamatModel> get listJenisAlamat {
    return UnmodifiableListView(this._listJenisAlamat);
  }

  JenisAlamatModel get jenisAlamatSelected => _jenisAlamatSelected;

  set jenisAlamatSelected(JenisAlamatModel value) {
    this._jenisAlamatSelected = value;
    notifyListeners();
  }

  KelurahanModel get kelurahanSelected => _kelurahanSelected;

  set kelurahanSelected(KelurahanModel value) {
    this._kelurahanSelected = value;
    notifyListeners();
  }

  JenisAlamatModel get jenisAlamatSelectedTemp => _jenisAlamatSelectedTemp;

  KelurahanModel get kelurahanSelectedTemp => _kelurahanSelectedTemp;

  void searchKelurahan(BuildContext context) async {
    KelurahanModel data = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => SearchKelurahanChangeNotif(),
                child: SearchKelurahan())));
    if (data != null) {
      kelurahanSelected = data;
      this._controllerKelurahan.text = data.KEL_NAME;
      this._controllerKecamatan.text = data.KEC_NAME;
      this._controllerKota.text = data.KABKOT_NAME;
      this._controllerProvinsi.text = data.PROV_NAME;
      this._controllerPostalCode.text = data.ZIPCODE;
      notifyListeners();
    } else {
      return;
    }
  }

  GlobalKey<FormState> get key => _key;

  void check(BuildContext context, int flag, int index) {
    final _form = _key.currentState;
    if (flag == 0) {
      if (_form.validate()) {
        // Provider.of<FormMOccupationChangeNotif>(context, listen: false)
        //     .addOccupationAddress(AddressModel(
        //         this._jenisAlamatSelected,
        //         this._kelurahanSelected,
        //         this._controllerAlamat.text,
        //         this._controllerRT.text,
        //         this._controllerRW.text,
        //         this._controllerKodeArea.text,
        //         this._controllerTlpn.text,
        //         this._isSameWithIdentity,
        //         this._addressFromMap,false));
        Navigator.pop(context);
      } else {
        autoValidate = true;
      }
    } else {
      if (_form.validate()) {
        // Provider.of<FormMOccupationChangeNotif>(context, listen: false)
        //     .updateOccupationAddress(
        //         AddressModel(
        //             this._jenisAlamatSelected,
        //             this._kelurahanSelected,
        //             this._controllerAlamat.text,
        //             this._controllerRT.text,
        //             this._controllerRW.text,
        //             this._controllerKodeArea.text,
        //             this._controllerTlpn.text,
        //             this._isSameWithIdentity,
        //             this._addressFromMap,
        //             this._isCorrespondence),
        //         index);
        Navigator.pop(context);
      } else {
        autoValidate = true;
      }
    }
  }

  void checkValidCodeArea(String value) {
    if (value == "08") {
      Future.delayed(Duration(milliseconds: 100), () {
        this._controllerKodeArea.clear();
      });
    } else {
      return;
    }
  }

  setValueForEdit(AddressModel data, BuildContext context,bool isSameWithIdentity){
    if (isSameWithIdentity) {
      setValueIsSameWithIdentity(isSameWithIdentity, context, data);
    }
    else {
      for (int i = 0; i < this._listJenisAlamat.length; i++) {
        if (data.jenisAlamatModel.KODE == this._listJenisAlamat[i].KODE) {
          this._jenisAlamatSelected = this._listJenisAlamat[i];
          this._jenisAlamatSelectedTemp = this._listJenisAlamat[i];
        }
      }
      this._kelurahanSelected = data.kelurahanModel;
      this._controllerAlamat.text = data.address;
      this._alamatTemp = this._controllerAlamat.text;
      this._controllerRT.text = data.rt;
      this._rtTemp = this._controllerRT.text;
      this._controllerRW.text = data.rw;
      this._rwTemp = this._controllerRW.text;
      this._controllerKelurahan.text = data.kelurahanModel.KEL_NAME;
      this._kelurahanTemp = this._controllerKelurahan.text;
      this._controllerKecamatan.text = data.kelurahanModel.KEC_NAME;
      this._kecamatanTemp = this._controllerKecamatan.text;
      this._controllerKota.text = data.kelurahanModel.KABKOT_NAME;
      this._kotaTemp = this._controllerKota.text;
      this._controllerProvinsi.text = data.kelurahanModel.PROV_NAME;
      this._provinsiTemp = this._controllerProvinsi.text;
      this._controllerKodeArea.text = data.areaCode;
      this._areaCodeTemp = this._controllerKodeArea.text;
      this._controllerTlpn.text = data.phone;
      this._phoneTemp = this._controllerTlpn.text;
      this._controllerPostalCode.text = data.kelurahanModel.ZIPCODE;
      this._postalCodeTemp = this._controllerPostalCode.text;
      this._isCorrespondence = data.isCorrespondence;
    }
  }


//  Future<void> setValueForEdit(AddressModel data, BuildContext context,
//      int index, bool isSameWithIdentity) async {
//    addDataListAddressType(context, index);
//    if (isSameWithIdentity) {
//      setValueIsSameWithIdentity(isSameWithIdentity, context, data);
//    } else {
//      _addressModelTemp = data;
//      for (int i = 0; i < this._listJenisAlamat.length; i++) {
//        if (data.jenisAlamatModel.KODE == this._listJenisAlamat[i].KODE) {
//          this._jenisAlamatSelected = this._listJenisAlamat[i];
//          this._jenisAlamatSelectedTemp = this._listJenisAlamat[i];
//        }
//      }
//      this._kelurahanSelected = data.kelurahanModel;
//      this._controllerAlamat.text = data.address;
//      this._alamatTemp = this._controllerAlamat.text;
//      this._controllerRT.text = data.rt;
//      this._rtTemp = this._controllerRT.text;
//      this._controllerRW.text = data.rw;
//      this._rwTemp = this._controllerRW.text;
//      this._controllerKelurahan.text = data.kelurahanModel.kelurahan;
//      this._kelurahanTemp = this._controllerKelurahan.text;
//      this._controllerKecamatan.text = data.kelurahanModel.kecamatan;
//      this._kecamatanTemp = this._controllerKecamatan.text;
//      this._controllerKota.text = data.kelurahanModel.kota;
//      this._kotaTemp = this._controllerKota.text;
//      this._controllerProvinsi.text = data.kelurahanModel.provinsi;
//      this._provinsiTemp = this._controllerProvinsi.text;
//      this._controllerKodeArea.text = data.areaCode;
//      this._areaCodeTemp = this._controllerKodeArea.text;
//      this._controllerTlpn.text = data.phone;
//      this._phoneTemp = this._controllerTlpn.text;
//      this._controllerPostalCode.text = data.kelurahanModel.postalCode;
//      this._postalCodeTemp = this._controllerPostalCode.text;
//      this._isCorrespondence = data.isCorrespondence;
//    }
//  }

  get postalCodeTemp => _postalCodeTemp;

  get areaCOdeTemp => _areaCodeTemp;

  get provinsiTemp => _provinsiTemp;

  get kotaTemp => _kotaTemp;

  get kecamatanTemp => _kecamatanTemp;

  get kelurahanTemp => _kelurahanTemp;

  get rwTemp => _rwTemp;

  get rtTemp => _rtTemp;

  get phoneTemp => _phoneTemp;

  String get alamatTemp => _alamatTemp;

  bool get isSameWithIdentity => _isSameWithIdentity;

  set isSameWithIdentity(bool value) {
    this._isSameWithIdentity = value;
    notifyListeners();
  }

  void setValueIsSameWithIdentity(
      bool value, BuildContext context, AddressModel model) {
    var _provider =
        Provider.of<FormMOccupationChangeNotif>(context, listen: false);
    AddressModel data;
    if (_addressModelTemp == null) {
      if (value) {
        for (int i = 0; i < _provider.listOccupationAddress.length; i++) {
          if (_provider.listOccupationAddress[i].jenisAlamatModel.KODE == "01") {
            // data = _provider.listOccupationAddress[i];
          }
        }
        if (model != null) {
          for (int i = 0; i < _listJenisAlamat.length; i++) {
            if (model.jenisAlamatModel.KODE == _listJenisAlamat[i].KODE) {
              this._jenisAlamatSelected = _listJenisAlamat[i];
              this._jenisAlamatSelectedTemp = this._jenisAlamatSelected;
              this._isSameWithIdentity = value;
            }
          }
        }
        this._controllerAlamat.text = data.address;
        this._controllerRT.text = data.rt;
        this._controllerRW.text = data.rw;
        this._kelurahanSelected = data.kelurahanModel;
        this._controllerAlamat.text = data.address;
        this._alamatTemp = this._controllerAlamat.text;
        this._controllerRT.text = data.rt;
        this._rtTemp = this._controllerRT.text;
        this._controllerRW.text = data.rw;
        this._rwTemp = this._controllerRW.text;
        this._controllerKelurahan.text = data.kelurahanModel.KEL_NAME;
        this._kelurahanTemp = this._controllerKelurahan.text;
        this._controllerKecamatan.text = data.kelurahanModel.KEC_NAME;
        this._kecamatanTemp = this._controllerKecamatan.text;
        this._controllerKota.text = data.kelurahanModel.KABKOT_NAME;
        this._kotaTemp = this._controllerKota.text;
        this._controllerProvinsi.text = data.kelurahanModel.PROV_NAME;
        this._provinsiTemp = this._controllerProvinsi.text;
        this._controllerKodeArea.text = data.areaCode;
        this._areaCodeTemp = this._controllerKodeArea.text;
        this._controllerTlpn.text = data.phone;
        this._phoneTemp = this._controllerTlpn.text;
        this._controllerPostalCode.text = data.kelurahanModel.ZIPCODE;
        this._postalCodeTemp = this._controllerPostalCode.text;
        this._controllerKodeArea.text = data.areaCode;
        this._controllerTlpn.text = data.phone;
        enableTfAddress = !value;
        enableTfRT = !value;
        enableTfRW = !value;
        enableTfKelurahan = !value;
        enableTfKecamatan = !value;
        enableTfKota = !value;
        enableTfProv = !value;
        enableTfPostalCode = !value;
        enableTfAreaCode = !value;
        enableTfPhone = !value;
      } else {
        this._controllerAlamat.clear();
        this._controllerRT.clear();
        this._controllerRW.clear();
        this._kelurahanSelected = null;
        this._controllerAlamat.clear();
        this._controllerRT.clear();
        this._controllerRW.clear();
        this._controllerKelurahan.clear();
        this._controllerKecamatan.clear();
        this._controllerKota.clear();
        this._controllerProvinsi.clear();
        this._controllerKodeArea.clear();
        this._controllerTlpn.clear();
        this._controllerPostalCode.clear();
        this._controllerKodeArea.clear();
        this._controllerTlpn.clear();
        enableTfAddress = !value;
        enableTfRT = !value;
        enableTfRW = !value;
        enableTfKelurahan = !value;
        enableTfKecamatan = !value;
        enableTfKota = !value;
        enableTfProv = !value;
        enableTfPostalCode = !value;
        enableTfAreaCode = !value;
        enableTfPhone = !value;
      }
    } else {
      if (value) {
        for (int i = 0; i < _provider.listOccupationAddress.length; i++) {
          if (_provider.listOccupationAddress[i].jenisAlamatModel.KODE == "01") {
            // data = _provider.listOccupationAddress[i];
          }
        }
        this._controllerAlamat.text = data.address;
        this._controllerRT.text = data.rt;
        this._controllerRW.text = data.rw;
        this._kelurahanSelected = data.kelurahanModel;
        this._controllerAlamat.text = data.address;
        this._controllerRT.text = data.rt;
        this._controllerRW.text = data.rw;
        this._controllerKelurahan.text = data.kelurahanModel.KEL_NAME;
        this._controllerKecamatan.text = data.kelurahanModel.KEC_NAME;
        this._controllerKota.text = data.kelurahanModel.KABKOT_NAME;
        this._controllerProvinsi.text = data.kelurahanModel.PROV_NAME;
        this._controllerKodeArea.text = data.areaCode;
        this._controllerTlpn.text = data.phone;
        this._controllerPostalCode.text = data.kelurahanModel.ZIPCODE;
        this._controllerKodeArea.text = data.areaCode;
        this._controllerTlpn.text = data.phone;
        enableTfAddress = !value;
        enableTfRT = !value;
        enableTfRW = !value;
        enableTfKelurahan = !value;
        enableTfKecamatan = !value;
        enableTfKota = !value;
        enableTfProv = !value;
        enableTfPostalCode = !value;
        enableTfAreaCode = !value;
        enableTfPhone = !value;
      } else {
        this._controllerAlamat.clear();
        this._controllerRT.clear();
        this._controllerRW.clear();
        this._kelurahanSelected = null;
        this._controllerAlamat.clear();
        this._controllerRT.clear();
        this._controllerRW.clear();
        this._controllerKelurahan.clear();
        this._controllerKecamatan.clear();
        this._controllerKota.clear();
        this._controllerProvinsi.clear();
        this._controllerKodeArea.clear();
        this._controllerTlpn.clear();
        this._controllerPostalCode.clear();
        this._controllerKodeArea.clear();
        this._controllerTlpn.clear();
        enableTfAddress = !value;
        enableTfRT = !value;
        enableTfRW = !value;
        enableTfKelurahan = !value;
        enableTfKecamatan = !value;
        enableTfKota = !value;
        enableTfProv = !value;
        enableTfPostalCode = !value;
        enableTfAreaCode = !value;
        enableTfPhone = !value;
      }
    }
  }

  bool get enableTfPhone => _enableTfPhone;

  set enableTfPhone(bool value) {
    this._enableTfPhone = value;
  }

  bool get enableTfAreaCode => _enableTfAreaCode;

  set enableTfAreaCode(bool value) {
    this._enableTfAreaCode = value;
  }

  bool get enableTfPostalCode => _enableTfPostalCode;

  set enableTfPostalCode(bool value) {
    this._enableTfPostalCode = value;
  }

  bool get enableTfProv => _enableTfProv;

  set enableTfProv(bool value) {
    this._enableTfProv = value;
  }

  bool get enableTfKota => _enableTfKota;

  set enableTfKota(bool value) {
    this._enableTfKota = value;
  }

  bool get enableTfKecamatan => _enableTfKecamatan;

  set enableTfKecamatan(bool value) {
    this._enableTfKecamatan = value;
  }

  bool get enableTfKelurahan => _enableTfKelurahan;

  set enableTfKelurahan(bool value) {
    this._enableTfKelurahan = value;
  }

  bool get enableTfRW => _enableTfRW;

  set enableTfRW(bool value) {
    this._enableTfRW = value;
  }

  bool get enableTfRT => _enableTfRT;

  set enableTfRT(bool value) {
    this._enableTfRT = value;
  }

  bool get enableTfAddress => _enableTfAddress;

  set enableTfAddress(bool value) {
    this._enableTfAddress = value;
  }

  get areaCodeTemp => _areaCodeTemp;

  set areaCodeTemp(value) {
    this._areaCodeTemp = value;
  }
}
