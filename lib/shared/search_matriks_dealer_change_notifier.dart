import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/matriks_dealer_model.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_application_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_object_unit_change_notifier.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/io_client.dart';
import 'package:provider/provider.dart';

class SearchMatriksDealerChangeNotifier with ChangeNotifier {
  bool _showClear = false;
  TextEditingController _controllerSearch = TextEditingController();
  bool _loadData = false;
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  List<MatriksDealerModel> _listMatriksDealer = [
    // ThirdPartyTypeModel("001", "DEALER"),
    // ThirdPartyTypeModel("001", "MERCHANT"),
    // ThirdPartyTypeModel("001", "MITRA MAXI PLUS UMROH"),
    // ThirdPartyTypeModel("001", "RETAIL CHANNEL"),
    // ThirdPartyTypeModel("001", "NON DEALER"),
  ];
  List<MatriksDealerModel>  _listMatriksDealerTemp = [];

  TextEditingController get controllerSearch => _controllerSearch;

  bool get showClear => _showClear;

  set showClear(bool value) {
    this._showClear = value;
    notifyListeners();
  }

  void changeAction(String value) {
    if (value != "") {
      showClear = true;
    } else {
      showClear = false;
    }
  }

  UnmodifiableListView<MatriksDealerModel> get listMatriksDealer {
    return UnmodifiableListView(this._listMatriksDealer);
  }

  UnmodifiableListView<MatriksDealerModel> get listMatriksDealerTemp {
    return UnmodifiableListView(this._listMatriksDealerTemp);
  }

  bool get loadData => _loadData;

  set loadData(bool value) {
    this._loadData = value;
  }

  void getMatriksDealer(BuildContext context) async{
    this._listMatriksDealer.clear();
    loadData = true;
    final ioc = new HttpClient();
    ioc.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;

    final _http = IOClient(ioc);

    // var formatDate = DateFormat(_providerAplikasi.controllerOrderDate.text);
    // var date = _providerAplikasi.controllerOrderDate;

    var _body = jsonEncode({
      "refOne": "",
      "refTwo": Provider.of<InformationObjectUnitChangeNotifier>(context, listen: false).objectSelected.id,
      "refThree": Provider.of<InfoAppChangeNotifier>(context,listen: false).controllerOrderDate.text
    });

    var storage = FlutterSecureStorage();
    String urlPublic = await storage.read(key: "urlPublic");
    final _response = await _http.post(
      // "${BaseUrl.unit}api/parameter/get-group-object",
        "${urlPublic}pihak-ketiga/get_info_dealermatrix",
        body: _body,
        headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
    );
    if(_response.statusCode == 200){
      final _result = jsonDecode(_response.body);
      if(_result.isEmpty){
        showSnackBar("Matriks Dealer tidak ditemukan");
        loadData = false;
      }
      else{
        for(int i=0; i <_result.length; i++){
          this._listMatriksDealer.add(
              MatriksDealerModel(_result[i]['kode'], _result[i]['deskripsi'])
          );
        }
        loadData = false;
      }
    }
    else{
      showSnackBar("Error response status ${_response.statusCode}");
      this._loadData = false;
    }
    notifyListeners();
  }

  void showSnackBar(String text){
    this._scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor));
  }

  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  void searchMatriksDealer(String query) {
    if(query.length < 3) {
      showSnackBar("Input minimal 3 karakter");
    } else {
      _listMatriksDealerTemp.clear();
      if (query.isEmpty) {
        return;
      }

      _listMatriksDealer.forEach((dataMatriksDealer) {
        if (dataMatriksDealer.kode.contains(query) || dataMatriksDealer.deskripsi.contains(query)) {
          _listMatriksDealerTemp.add(dataMatriksDealer);
        }
      });
    }
    notifyListeners();
  }

  void clearSearchTemp() {
    _listMatriksDealerTemp.clear();
  }
}
