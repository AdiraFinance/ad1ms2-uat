import 'dart:collection';
import 'dart:io';

import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/models/show_mandatory_info_dokumen_model.dart';
import 'package:ad1ms2_dev/shared/dashboard/dashboard_change_notif.dart';
import 'package:ad1ms2_dev/shared/resource/get_list_meta_data.dart';
import 'package:ad1ms2_dev/models/info_document_detail_model.dart';
import 'package:ad1ms2_dev/models/info_document_model.dart';
import 'package:ad1ms2_dev/models/info_document_type_model.dart';
import 'package:ad1ms2_dev/models/ms2_document_model.dart';
import 'package:ad1ms2_dev/screens/search_document_type.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_app_document_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_document_type_change_notifier.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../main.dart';
import '../choose_image_picker.dart';
import '../constants.dart';
import '../date_picker.dart';

class AddInfoDocumentChangeNotifier with ChangeNotifier{
    GlobalKey<FormState> _key = GlobalKey<FormState>();
    bool _autoValidate = false;
    bool _autoValidateFile = false;
    List<InfoDocumentModel> _listDocument = [];
    InfoDocumentDetailModel _documentDetailSelected;
    InfoDocumentTypeModel _documentTypeSelected;
    TextEditingController _controllerDocumentType = TextEditingController();
    TextEditingController _controllerDateDocument = TextEditingController();
    DateTime _initialDateDocument = DateTime(dateNow.year, dateNow.month, dateNow.day);
    ChooseImagePicker _chooseImagePicker = ChooseImagePicker();
    Map _fileDocumentUnitObject;
    String _pathFile;
    String _custType;
    String _lastKnownState;
    String _orderSupportingDocumentID = "NEW";

    List<InfoDocumentModel> get listDocument => _listDocument;

    // List<InfoDocumentTypeModel> _listDocumentType = [
    //     InfoDocumentTypeModel("A01", "KTP PEMOHON"),
    //     InfoDocumentTypeModel("A02", "KARTU KELUARGA"),
    //     InfoDocumentTypeModel("A03", "SKCK")
    // ];
    List<InfoDocumentTypeModel> _listDocumentType = [];

    set custType(String value) {
        this._custType = value;
        notifyListeners();
    }

    String get lastKnownState => _lastKnownState;

    set lastKnownState(String value) {
        this._lastKnownState = value;
        notifyListeners();
    }

    InfoDocumentTypeModel get documentTypeSelected {
        return this._documentTypeSelected;
    }

    set documentTypeSelected(InfoDocumentTypeModel value) {
        this._documentTypeSelected = value;
        notifyListeners();
    }

    UnmodifiableListView<InfoDocumentTypeModel> get listDocumentType {
        return UnmodifiableListView(this._listDocumentType);
    }

    InfoDocumentDetailModel get documentDetailSelected => _documentDetailSelected;

    set documentDetailSelected(InfoDocumentDetailModel value) {
        this._documentDetailSelected = value;
        notifyListeners();
    }

    TextEditingController get controllerDateDocument {
        return this._controllerDateDocument;
    }

    set controllerDateDocument(TextEditingController value) {
        this._controllerDateDocument = value;
        this.notifyListeners();
    }

    Map get fileDocumentUnitObject => _fileDocumentUnitObject;

    set fileDocumentUnitObject(Map value) {
        this._fileDocumentUnitObject = value;
        notifyListeners();
    }

    bool get autoValidate => _autoValidate;

    set autoValidate(bool value) {
        this._autoValidate = value;
        notifyListeners();
    }

    bool get autoValidateFile => _autoValidateFile;

    set autoValidateFile(bool value) {
        this._autoValidateFile = value;
        notifyListeners();
    }

    TextEditingController get controllerDocumentType => _controllerDocumentType;

    set controllerDocumentType(TextEditingController value) {
        this._controllerDocumentType = value;
        notifyListeners();
    }

    GlobalKey<FormState> get key => _key;

    String get orderSupportingDocumentID => _orderSupportingDocumentID;

    set orderSupportingDocumentID(String value) {
        this._orderSupportingDocumentID = value;
    }

    void selectDocumentDate(BuildContext context) async {
        // DatePickerShared _datePickerShared = DatePickerShared();
        // var _datePickerSelected = await _datePickerShared.selectStartDate(
        //     context, this._initialDateDocument,
        //     canAccessNextDay: false);
        // if (_datePickerSelected != null) {
        //     this.controllerDateDocument.text = dateFormat.format(_datePickerSelected);
        //     this._initialDateDocument = _datePickerSelected;
        //     notifyListeners();
        // } else {
        //     return;
        // }
        var _datePickerSelected = await selectDateLastToday(context, _initialDateDocument);
        if (_datePickerSelected != null) {
            this.controllerDateDocument.text = dateFormat.format(_datePickerSelected);
            this._initialDateDocument = _datePickerSelected;
            notifyListeners();
        } else {
            return;
        }
    }

    void _imageFromCamera(BuildContext context) async {
        Map _imagePicker = await _chooseImagePicker.imageFromCameraInfoDocument(context);
        if (_imagePicker != null) {
            _fileDocumentUnitObject = _imagePicker;
            if (this._autoValidateFile) this._autoValidateFile = false;
            Position _position = await Geolocator().getCurrentPosition(desiredAccuracy: LocationAccuracy.bestForNavigation);
            _fileDocumentUnitObject['latitude'] = _position.latitude;
            _fileDocumentUnitObject['longitude'] = _position.longitude;
            notifyListeners();
        } else {
            return;
        }
    }

    void _fileFormGallery(BuildContext context) async {
        Map _file = await _chooseImagePicker.fileFromGalleryInfoDocument(context);
        if (_file != null) {
            _fileDocumentUnitObject = _file;
            if (this._autoValidateFile) this._autoValidateFile = false;
            Position _position = await Geolocator().getCurrentPosition(desiredAccuracy: LocationAccuracy.bestForNavigation);
            _fileDocumentUnitObject['latitude'] = _position.latitude;
            _fileDocumentUnitObject['longitude'] = _position.longitude;
            notifyListeners();
        } else {
            return;
        }
    }

    String splitTypeFile(String file){
        List split = file.split(".");
        String typeFile = split[split.length-1];
        return typeFile.replaceAll("'", "");
    }

    void showBottomSheetChooseFile(context) {
        showModalBottomSheet(context: context,
            builder: (BuildContext bc) {
                return Theme(
                    data: ThemeData(fontFamily: "NunitoSans"),
                    child: Container(
                        child: new Wrap(
                            children: <Widget>[
                                FlatButton(
                                    onPressed: () {
                                        _imageFromCamera(context);
                                        Navigator.pop(context);
                                    },
                                    child: Row(
                                        mainAxisSize: MainAxisSize.max,
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: <Widget>[
                                            Icon(Icons.photo_camera, color: myPrimaryColor, size: 22.0),
                                            SizedBox(width: 12.0),
                                            Text("Camera",style: TextStyle(fontSize: 18.0),
                                            )
                                        ])),
                                FlatButton(
                                    onPressed: () {
                                        _fileFormGallery(context);
                                        Navigator.pop(context);
                                    },
                                    child: Row(
                                        mainAxisSize: MainAxisSize.max,
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: <Widget>[
                                            Icon(Icons.photo, color: myPrimaryColor, size: 22.0),
                                            SizedBox(width: 12.0),
                                            Text("Gallery",style: TextStyle(fontSize: 18.0),
                                            )
                                        ])
                                ),
                            ],
                        ),
                    ),
                );
            });
    }

    String _symbolName;

    void check(BuildContext context, int flag, int index) async{
        final _form = _key.currentState;
        if (flag == 0) {
            if (this._fileDocumentUnitObject == null) {
                this._autoValidateFile = true;
                notifyListeners();
            }
            if (_form.validate()) {
                File _myFile = this._fileDocumentUnitObject['file'];
                File _path = await _myFile.copy("$globalPath/${this._fileDocumentUnitObject['file_name']}");
                _pathFile = _path.path;
                _documentDetailSelected = InfoDocumentDetailModel(_fileDocumentUnitObject['file_name'], _fileDocumentUnitObject['file'], _pathFile);
                Provider.of<InfoDocumentChangeNotifier>(context, listen: false)
                    .addListInfoDocument(InfoDocumentModel(
                    this._documentTypeSelected,
                    this._orderSupportingDocumentID,
                    // InfoDocumentTypeModel("a", "b", 1,2,3),
                    // this._fileDocumentUnitObject['file_name'],
                    this._documentDetailSelected,
                    this._initialDateDocument.toString(),
                    this._documentDetailSelected.path,
                    _fileDocumentUnitObject['latitude'],
                    _fileDocumentUnitObject['longitude'],
                    _symbolName,
                    "",
                    this._documentDetailSelected.fileName
                ));
                Navigator.pop(context);
            } else {
                autoValidate = true;
            }
        } else {
//            if (_form.validate()) {
//                Provider.of<InfoAdditionalInsuranceChangeNotifier>(context, listen: false)
//                    .updateListInfoAdditionalInsurance(AdditionalInsuranceModel(
//                    this._insuranceTypeSelected,
//                    this._companySelected,
//                    this._productSelected,), context, index);
//                Navigator.pop(context);
//            } else {
//                autoValidate = true;
//            }
        }
    }

    Future<void> setValueForEditDocument(BuildContext context, InfoDocumentModel data, int flag) async {
//        for (int i = 0; i < this._listDocumentType.length; i++) {
//            if (data.documentType.docTypeId == this._listDocumentType[i].docTypeId) {
//                this._documentTypeSelected = this._listDocumentType[i];
//            }
//        }
        getDataFromDashboard(context);
        setPreference();
        if(flag == 1){
            this._orderSupportingDocumentID = data.orderSupportingDocumentID;
            this._documentTypeSelected = data.documentType;
            this._controllerDocumentType.text = "${data.documentType.docTypeId} - ${data.documentType.docTypeName}";
            this._controllerDateDocument.text = data.date;
            this._documentDetailSelected = data.documentDetail;
            splitTypeFile(data.documentDetail.file.toString());
        }
    }

    void searchDocumentType(BuildContext context,int flag) async{
        InfoDocumentTypeModel data = await Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => ChangeNotifierProvider(
                    create: (context) => SearchDocumentTypeChangeNotifier(),
                    child: SearchDocumentType(flag))));
        if (data != null) {
            this._documentTypeSelected = data;
            this._controllerDocumentType.text = "${data.docTypeId} - ${data.docTypeName}";
            getListMeta(data.docTypeId);
            notifyListeners();
        } else {
            return;
        }
    }

    void getListMeta(String id) async{
        try{
            List _data = await getListMetadata(id);
            _symbolName = _data[0]['symbolName'];
        }
        catch(e){
            print(e);
        }
    }

    void setPreference() async {
        SharedPreferences _preference = await SharedPreferences.getInstance();
        this._custType = _preference.getString("cust_type");
        this._lastKnownState = _preference.getString("last_known_state");
    }

    ShowMandatoryInfoDokumenModel _showMandatoryInfoDokumenIdeModel;
    ShowMandatoryInfoDokumenModel _showMandatoryInfoDokumenIdeCompanyModel;
    void showMandatoryIdeModel(BuildContext context){
        _showMandatoryInfoDokumenIdeModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoDokumenIdeModel;
        _showMandatoryInfoDokumenIdeCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoDokumenIdeCompanyModel;
    }

    ShowMandatoryInfoDokumenModel _showMandatoryInfoDokumenRegulerSurveyModel;
    ShowMandatoryInfoDokumenModel _showMandatoryInfoDokumenRegulerSurveyCompanyModel;
    void showMandatoryRegulerSurveyModel(BuildContext context){
        _showMandatoryInfoDokumenRegulerSurveyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoDokumenRegulerSurveyModel;
        _showMandatoryInfoDokumenRegulerSurveyCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoDokumenRegulerSurveyCompanyModel;
    }

    ShowMandatoryInfoDokumenModel _showMandatoryInfoDokumenPacModel;
    ShowMandatoryInfoDokumenModel _showMandatoryInfoDokumenPacCompanyModel;
    void showMandatoryPacModel(BuildContext context){
        _showMandatoryInfoDokumenPacModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoDokumenPacModel;
        _showMandatoryInfoDokumenPacCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoDokumenPacCompanyModel;
    }

    ShowMandatoryInfoDokumenModel _showMandatoryInfoDokumenResurveyModel;
    ShowMandatoryInfoDokumenModel _showMandatoryInfoDokumenResurveyCompanyModel;
    void showMandatoryResurveyModel(BuildContext context){
        _showMandatoryInfoDokumenResurveyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoDokumenResurveyModel;
        _showMandatoryInfoDokumenResurveyCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoDokumenResurveyCompanyModel;
    }

    ShowMandatoryInfoDokumenModel _showMandatoryInfoDokumenDakorModel;
    ShowMandatoryInfoDokumenModel _showMandatoryInfoDokumenDakorCompanyModel;
    void showMandatoryDakorModel(BuildContext context){
        _showMandatoryInfoDokumenDakorModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoDokumenDakorModel;
        _showMandatoryInfoDokumenDakorCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoDokumenDakorCompanyModel;
    }

    void getDataFromDashboard(BuildContext context){
        showMandatoryIdeModel(context);
        showMandatoryRegulerSurveyModel(context);
        showMandatoryPacModel(context);
        showMandatoryResurveyModel(context);
        showMandatoryDakorModel(context);
    }

    bool isDocumentTypeVisible() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoDokumenIdeModel.isDocumentTypeVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoDokumenRegulerSurveyModel.isDocumentTypeVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "IA" || _lastKnownState == "PAC" || _lastKnownState == "CONA" || _lastKnownState == "PAC" || _lastKnownState == "CONA") {
                value = _showMandatoryInfoDokumenPacModel.isDocumentTypeVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoDokumenResurveyModel.isDocumentTypeVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoDokumenDakorModel.isDocumentTypeVisible;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoDokumenIdeCompanyModel.isDocumentTypeVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoDokumenRegulerSurveyCompanyModel.isDocumentTypeVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "IA" || _lastKnownState == "PAC" || _lastKnownState == "CONA" || _lastKnownState == "PAC" || _lastKnownState == "CONA") {
                value = _showMandatoryInfoDokumenPacCompanyModel.isDocumentTypeVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoDokumenResurveyCompanyModel.isDocumentTypeVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoDokumenDakorCompanyModel.isDocumentTypeVisible;
            }
        }
        return value;
    }

    bool isDateDocumentVisible() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoDokumenIdeModel.isDateDocumentVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoDokumenRegulerSurveyModel.isDateDocumentVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "IA" || _lastKnownState == "PAC" || _lastKnownState == "CONA" || _lastKnownState == "PAC" || _lastKnownState == "CONA") {
                value = _showMandatoryInfoDokumenPacModel.isDateDocumentVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoDokumenResurveyModel.isDateDocumentVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoDokumenDakorModel.isDateDocumentVisible;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoDokumenIdeCompanyModel.isDateDocumentVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoDokumenRegulerSurveyCompanyModel.isDateDocumentVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "IA" || _lastKnownState == "PAC" || _lastKnownState == "CONA" || _lastKnownState == "PAC" || _lastKnownState == "CONA") {
                value = _showMandatoryInfoDokumenPacCompanyModel.isDateDocumentVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoDokumenResurveyCompanyModel.isDateDocumentVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoDokumenDakorCompanyModel.isDateDocumentVisible;
            }
        }
        return value;
    }

    bool isBrowseDocumentVisible() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoDokumenIdeModel.isBrowseDocumentVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoDokumenRegulerSurveyModel.isBrowseDocumentVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "IA" || _lastKnownState == "PAC" || _lastKnownState == "CONA") {
                value = _showMandatoryInfoDokumenPacModel.isBrowseDocumentVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoDokumenResurveyModel.isBrowseDocumentVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoDokumenDakorModel.isBrowseDocumentVisible;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoDokumenIdeCompanyModel.isBrowseDocumentVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoDokumenRegulerSurveyCompanyModel.isBrowseDocumentVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "IA" || _lastKnownState == "PAC" || _lastKnownState == "CONA") {
                value = _showMandatoryInfoDokumenPacCompanyModel.isBrowseDocumentVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoDokumenResurveyCompanyModel.isBrowseDocumentVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoDokumenDakorCompanyModel.isBrowseDocumentVisible;
            }
        }
        return value;
    }

    bool isDocumentTypeMandatory() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoDokumenIdeModel.isDocumentTypeMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoDokumenRegulerSurveyModel.isDocumentTypeMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "IA" || _lastKnownState == "PAC" || _lastKnownState == "CONA") {
                value = _showMandatoryInfoDokumenPacModel.isDocumentTypeMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoDokumenResurveyModel.isDocumentTypeMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoDokumenDakorModel.isDocumentTypeMandatory;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoDokumenIdeCompanyModel.isDocumentTypeMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoDokumenRegulerSurveyCompanyModel.isDocumentTypeMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "IA" || _lastKnownState == "PAC" || _lastKnownState == "CONA") {
                value = _showMandatoryInfoDokumenPacCompanyModel.isDocumentTypeMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoDokumenResurveyCompanyModel.isDocumentTypeMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoDokumenDakorCompanyModel.isDocumentTypeMandatory;
            }
        }
        return value;
    }

    bool isDateDocumentMandatory() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoDokumenIdeModel.isDateDocumentMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoDokumenRegulerSurveyModel.isDateDocumentMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "IA" || _lastKnownState == "PAC" || _lastKnownState == "CONA") {
                value = _showMandatoryInfoDokumenPacModel.isDateDocumentMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoDokumenResurveyModel.isDateDocumentMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoDokumenDakorModel.isDateDocumentMandatory;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoDokumenIdeCompanyModel.isDateDocumentMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoDokumenRegulerSurveyCompanyModel.isDateDocumentMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "IA" || _lastKnownState == "PAC" || _lastKnownState == "CONA") {
                value = _showMandatoryInfoDokumenPacCompanyModel.isDateDocumentMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoDokumenResurveyCompanyModel.isDateDocumentMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoDokumenDakorCompanyModel.isDateDocumentMandatory;
            }
        }
        return value;
    }

    bool isBrowseDocumentMandatory() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoDokumenIdeModel.isBrowseDocumentMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoDokumenRegulerSurveyModel.isBrowseDocumentMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "IA" || _lastKnownState == "PAC" || _lastKnownState == "CONA") {
                value = _showMandatoryInfoDokumenPacModel.isBrowseDocumentMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoDokumenResurveyModel.isBrowseDocumentMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoDokumenDakorModel.isBrowseDocumentMandatory;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoDokumenIdeCompanyModel.isBrowseDocumentMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoDokumenRegulerSurveyCompanyModel.isBrowseDocumentMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "IA" || _lastKnownState == "PAC" || _lastKnownState == "CONA") {
                value = _showMandatoryInfoDokumenPacCompanyModel.isBrowseDocumentMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoDokumenResurveyCompanyModel.isBrowseDocumentMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoDokumenDakorCompanyModel.isBrowseDocumentMandatory;
            }
        }
        return value;
    }
}