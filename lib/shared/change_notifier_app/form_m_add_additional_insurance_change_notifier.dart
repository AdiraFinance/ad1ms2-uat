import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/models/additional_insurance_model.dart';
import 'package:ad1ms2_dev/models/coverage_type_model.dart';
import 'package:ad1ms2_dev/models/form_m_pekerjaan_model.dart';
import 'package:ad1ms2_dev/models/insurance_type_model.dart';
import 'package:ad1ms2_dev/models/product_model.dart';
import 'package:ad1ms2_dev/models/show_mandatory_info_asuransi_tambahan_model.dart';
import 'package:ad1ms2_dev/screens/search_company_insurance.dart';
import 'package:ad1ms2_dev/screens/search_product.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_credit_structure_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_major_insurance_change_notifier.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/dashboard/dashboard_change_notif.dart';
import 'package:ad1ms2_dev/shared/format_currency.dart';
import 'package:ad1ms2_dev/shared/regex_input_formatter.dart';
import 'package:ad1ms2_dev/shared/search_company_insurance_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_product_change_notifier.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/io_client.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:ad1ms2_dev/shared/form_m_individu_change_notifier/form_m_parent_individu_change_notifier.dart';

import '../../main.dart';
import '../form_m_foto_change_notif.dart';
import 'info_additional_insurance_change_notifier.dart';
import 'info_application_change_notifier.dart';
import 'information_collatelar_change_notifier.dart';
import 'information_object_unit_change_notifier.dart';

class FormMAddAdditionalInsuranceChangeNotifier with ChangeNotifier{
    GlobalKey<FormState> _key = GlobalKey<FormState>();
    List<AdditionalInsuranceModel> _listAdditionalInsurance = [];
    bool _autoValidate = false;
    bool _loadData = false;
    bool _flagDisableNilaiPertanggungan = true;
    GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
    InsuranceTypeModel _insuranceTypeSelected;
    CompanyTypeInsuranceModel _companySelected;
    TextEditingController _controllerCompany = TextEditingController();
    ProductModel _productSelected;
    TextEditingController _controllerProduct = TextEditingController();
    TextEditingController _controllerPeriodType = TextEditingController();
    CoverageTypeModel _coverageTypeSelected;
    TextEditingController _controllerCoverageType = TextEditingController();
    TextEditingController _controllerCoverageValue = TextEditingController();
    TextEditingController _controllerUpperLimit = TextEditingController();
    TextEditingController _controllerLowerLimit = TextEditingController();
    TextEditingController _controllerUpperLimitRate = TextEditingController();
    TextEditingController _controllerLowerLimitRate = TextEditingController();
    TextEditingController _controllerPriceCash = TextEditingController();
    TextEditingController _controllerPriceCredit = TextEditingController();
    TextEditingController _controllerTotalPrice = TextEditingController();
    TextEditingController _controllerTotalPriceRate = TextEditingController();
    RegExInputFormatter _amountValidator = RegExInputFormatter.withRegex('^[0-9]{0,13}(\\.[0-9]{0,2})?\$');
    FormatCurrency _formatCurrency = FormatCurrency();
    var storage = FlutterSecureStorage();
    String _custType;
    String _lastKnownState;
    DbHelper _dbHelper = DbHelper();
    String _orderProductInsuranceID;
    String _colaType;
    bool _disableJenisPenawaran = false;
    bool _isDisablePACIAAOSCONA = false;

    bool _isEdit = false;
    bool _insuranceTypeDakor = false;
    bool _companyDakor = false;
    bool _productDakor = false;
    bool _periodDakor = false;
    bool _coverageTypeDakor = false;
    bool _coverageValueDakor = false;
    bool _upperLimitRateDakor = false;
    bool _upperLimitValueDakor = false;
    bool _lowerLimitRateDakor = false;
    bool _lowerLimitValueDakor = false;
    bool _cashDakor = false;
    bool _creditDakor = false;
    bool _totalPriceRateDakor = false;
    bool _totalPriceDakor = false;

    bool get isDisablePACIAAOSCONA => _isDisablePACIAAOSCONA;

    set isDisablePACIAAOSCONA(bool value) {
      this._isDisablePACIAAOSCONA = value;
      notifyListeners();
    }

    bool get disableJenisPenawaran => _disableJenisPenawaran;

    set disableJenisPenawaran(bool value) {
      this._disableJenisPenawaran = value;
      notifyListeners();
    }

    String get colaType => _colaType;

    set colaType(String value) {
      this._colaType = value;
      notifyListeners();
    }

  String get custType => _custType;

    set custType(String value) {
        this._custType = value;
        notifyListeners();
    }

    String get lastKnownState => _lastKnownState;

    set lastKnownState(String value) {
        this._lastKnownState = value;
        notifyListeners();
    }

    bool get flagDisableNilaiPertanggungan => _flagDisableNilaiPertanggungan;

    set flagDisableNilaiPertanggungan(bool value) {
        this._flagDisableNilaiPertanggungan = value;
        notifyListeners();
    }

    FormatCurrency get formatCurrency => _formatCurrency;

    RegExInputFormatter get amountValidator => _amountValidator;

    set amountValidator(RegExInputFormatter value) {
        this._amountValidator = value;
        notifyListeners();
    }

    List<AdditionalInsuranceModel> get listFormAdditionalInsuranceObject => _listAdditionalInsurance;

    List<InsuranceTypeModel> _listInsuranceType = [
        InsuranceTypeModel(null, "1","Asuransi Tambahan")
    ];

    bool get loadData => _loadData;

    set loadData(bool value) {
        this._loadData = value;
    }

    InsuranceTypeModel get insuranceTypeSelected {
        return this._insuranceTypeSelected;
    }

    set insuranceTypeSelected(InsuranceTypeModel value) {
        this._insuranceTypeSelected = value;
        notifyListeners();
    }

    GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

    UnmodifiableListView<InsuranceTypeModel> get listInsurance {
        return UnmodifiableListView(this._listInsuranceType);
    }

    String get orderProductInsuranceID => _orderProductInsuranceID;

    set orderProductInsuranceID(String value) {
      this._orderProductInsuranceID = value;
    }

  void searchCompanyInsurance(BuildContext context) async {
        CompanyTypeInsuranceModel _data = await Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => ChangeNotifierProvider(
                    create: (context) => SearchCompanyInsuranceChangeNotifier(),
                    child: SearchCompanyInsurance("1"))));
        if (_data != null) {
            this._companySelected = _data;
            this._controllerCompany.text = "${_data.KODE} - ${_data.DESKRIPSI}";
            notifyListeners();
        }
    }

    CompanyTypeInsuranceModel get companySelected => _companySelected;

    set companySelected(CompanyTypeInsuranceModel value) {
        _companySelected = value;
        notifyListeners();
    }

    TextEditingController get controllerCompany {
        return this._controllerCompany;
    }

    set controllerCompany(TextEditingController value) {
        _controllerCompany = value;
    }

    void searchProduct(BuildContext context) async {
        if(_companySelected != null){
            ProductModel _data = await Navigator.push(context,
                MaterialPageRoute(
                    builder: (context) => ChangeNotifierProvider(
                        create: (context) => SearchProductChangeNotifier(),
                        child: SearchProduct(flag: "1", company: _companySelected.PARENT_KODE.toString()))
                )
            );
            if (_data != null) {
                bool _check = await _cekPrimeKPM(context, _data.KODE);
                if(_check){
                    this._productSelected = _data;
                    this._controllerProduct.text = "${_data.KODE} - ${_data.DESKRIPSI}";
                    getLowerUpperLimit(context);
                }
                else {
                    showSnackBar("Proses Cek KPM Prima Gagal");
                    this._productSelected = null;
                    this._controllerProduct.clear();
                }
                notifyListeners();
            }
        }
        else {
            showSnackBar("Silahkan pilih perusahaan");
        }
    }

    Future<bool> _cekPrimeKPM(BuildContext context, String product) async{
        this._loadData = true;
        bool _cekPrime = false;
        var _providerInfoUnitObject = Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false);
        try{
            final ioc = new HttpClient();
            ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
            final _http = IOClient(ioc);

            var _body = jsonEncode(
                {
                    "jenisProduk": _providerInfoUnitObject.productTypeSelected.id,
                    "levelAsuransi": this._insuranceTypeSelected.KODE,
                    "produkAsuransi": product
                }
            );
            print("body kpm = $_body");

            String _cekPrimeKPM = await storage.read(key: "CekPrimeKPM");
            final _response = await _http.post(
                "${BaseUrl.urlGeneral}$_cekPrimeKPM",
                body: _body,
                headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
            ).timeout(Duration(seconds: 30));
            final _result = jsonDecode(_response.body);
            print("result kpm = $_result");
            if(_response.statusCode == 200){
                if(_result['message'] == "SUCCESS" || _result['message'] == "Success"){
                    _cekPrime = true;
                }
                else {
                    _cekPrime = false;
                }
            }
            this._loadData = false;
        }
        catch(e){
            print("catch = ${e.toString()}");
            this._loadData = false;
            _cekPrime = false;
        }
        notifyListeners();
        return _cekPrime;
    }

    ProductModel get productSelected => _productSelected;

    set productSelected(ProductModel value) {
        _productSelected = value;
        notifyListeners();
    }

    TextEditingController get controllerProduct {
        return this._controllerProduct;
    }

    set controllerProduct(TextEditingController value) {
        _controllerProduct = value;
    }

    TextEditingController get controllerPeriodType {
        return this._controllerPeriodType;
    }

    set controllerPeriodType(TextEditingController value) {
        _controllerPeriodType = value;
    }

    TextEditingController get controllerCoverageType {
        return this._controllerCoverageType;
    }

    set controllerCoverageType(TextEditingController value) {
        _controllerCoverageType = value;
    }

    TextEditingController get controllerCoverageValue {
        return this._controllerCoverageValue;
    }

    set controllerCoverageValue(TextEditingController value) {
        _controllerCoverageValue = value;
    }

    TextEditingController get controllerUpperLimit {
        return this._controllerUpperLimit;
    }

    set controllerUpperLimit(TextEditingController value) {
        _controllerUpperLimit = value;
    }

    TextEditingController get controllerLowerLimit {
        return this._controllerLowerLimit;
    }

    set controllerLowerLimit(TextEditingController value) {
        _controllerLowerLimit = value;
    }

    TextEditingController get controllerUpperLimitRate {
        return this._controllerUpperLimitRate;
    }

    set controllerUpperLimitRate(TextEditingController value) {
        _controllerUpperLimitRate = value;
    }

    TextEditingController get controllerLowerLimitRate {
        return this._controllerLowerLimitRate;
    }

    set controllerLowerLimitRate(TextEditingController value) {
        _controllerLowerLimitRate = value;
    }

    TextEditingController get controllerPriceCash {
        return this._controllerPriceCash;
    }

    set controllerPriceCash(TextEditingController value) {
        _controllerPriceCash = value;
    }

    TextEditingController get controllerPriceCredit{
        return this._controllerPriceCredit;
    }

    set controllerPriceCredit(TextEditingController value) {
        _controllerPriceCredit = value;
    }

    TextEditingController get controllerTotalPrice{
        return this._controllerTotalPrice;
    }

    set controllerTotalPrice(TextEditingController value) {
        _controllerTotalPrice = value;
    }

    TextEditingController get controllerTotalPriceRate{
        return this._controllerTotalPriceRate;
    }

    set controllerTotalPriceRate(TextEditingController value) {
        _controllerTotalPriceRate = value;
    }

    bool get autoValidate => _autoValidate;

    set autoValidate(bool value) {
        this._autoValidate = value;
        notifyListeners();
    }

    CoverageTypeModel get coverageTypeSelected => _coverageTypeSelected;

    set coverageTypeSelected(CoverageTypeModel value) {
        _coverageTypeSelected = value;
        notifyListeners();
    }

    GlobalKey<FormState> get key => _key;

    void check(BuildContext context, int flag, int index) {
        final _form = _key.currentState;
        var _providerCollateral = Provider.of<InformationCollateralChangeNotifier>(context, listen: false);
        if (flag == 0) {
            print("halo");
            if (_form.validate()) {
                calculateTotalCost();
                Provider.of<InfoAdditionalInsuranceChangeNotifier>(context, listen: false)
                    .addListInfoAdditionalInsurance(AdditionalInsuranceModel(
                    this._insuranceTypeSelected,
                    _providerCollateral.collateralTypeModel.id,
                    this._companySelected,
                    this._productSelected,
                    this._controllerPeriodType.text,
                    this._coverageTypeSelected,
                    this._controllerCoverageValue.text,
                    this._controllerUpperLimitRate.text,
                    this._controllerLowerLimitRate.text,
                    this._controllerUpperLimit.text,
                    this._controllerLowerLimit.text,
                    this._controllerPriceCash.text,
                    this._controllerPriceCredit.text,
                    this._controllerTotalPriceRate.text,
                    this._controllerTotalPrice.text,
                    this._isEdit,
                    this._insuranceTypeDakor,
                    this._companyDakor,
                    this._productDakor,
                    this._periodDakor,
                    this._coverageTypeDakor,
                    this._coverageValueDakor,
                    this._upperLimitRateDakor,
                    this._upperLimitValueDakor,
                    this._lowerLimitRateDakor,
                    this._lowerLimitValueDakor,
                    this._cashDakor,
                    this._creditDakor,
                    this._totalPriceRateDakor,
                    this._totalPriceDakor,
                    "NEW"
                ));
                Navigator.pop(context);
            } else {
                autoValidate = true;
            }
        } else {
            checkDataDakor(index);
            if (_form.validate()) {
                calculateTotalCost();
                Provider.of<InfoAdditionalInsuranceChangeNotifier>(context, listen: false)
                    .updateListInfoAdditionalInsurance(AdditionalInsuranceModel(
                    this._insuranceTypeSelected,
                    this._colaType,
                    this._companySelected,
                    this._productSelected,
                    this._controllerPeriodType.text,
                    this._coverageTypeSelected,
                    this._controllerCoverageValue.text,
                    this._controllerUpperLimitRate.text,
                    this._controllerLowerLimitRate.text,
                    this._controllerUpperLimit.text,
                    this._controllerLowerLimit.text,
                    this._controllerPriceCash.text,
                    this._controllerPriceCredit.text,
                    this._controllerTotalPriceRate.text,
                    this._controllerTotalPrice.text,
                    this._isEdit,
                    this._insuranceTypeDakor,
                    this._companyDakor,
                    this._productDakor,
                    this._periodDakor,
                    this._coverageTypeDakor,
                    this._coverageValueDakor,
                    this._upperLimitRateDakor,
                    this._upperLimitValueDakor,
                    this._lowerLimitRateDakor,
                    this._lowerLimitValueDakor,
                    this._cashDakor,
                    this._creditDakor,
                    this._totalPriceRateDakor,
                    this._totalPriceDakor,
                    this._orderProductInsuranceID
                ), context, index);
                Navigator.pop(context);
            } else {
                autoValidate = true;
            }
        }
    }

    Future<void> setValueForEditAdditionalInsurance(AdditionalInsuranceModel data, int index) async {
        for (int i = 0; i < this._listInsuranceType.length; i++) {
            if (data.insuranceType.KODE == this._listInsuranceType[i].KODE) {
                this._insuranceTypeSelected = this._listInsuranceType[i];
                // this._jenisAlamatSelectedTemp = this._listInsuranceType[i];
            }
        }
        this._colaType = data.colaType;
        this._companySelected = data.company;
        this._controllerCompany.text = data.company.KODE +" - "+data.company.DESKRIPSI;
        this._productSelected = data.product;
        this._controllerProduct.text = data.product.KODE +" - "+data.product.DESKRIPSI;
        this._controllerPeriodType.text = data.periodType;
        this._coverageTypeSelected = data.coverageType;
        this._controllerCoverageType.text = data.coverageType.KODE +" - "+data.coverageType.DESKRIPSI;
        this._controllerCoverageValue.text = data.coverageValue;
        this._controllerUpperLimit.text = data.upperLimit;
        this._controllerLowerLimit.text = data.lowerLimit;
        this._controllerUpperLimitRate.text = data.upperLimitRate;
        this._controllerLowerLimitRate.text = data.lowerLimitRate;
        this._controllerPriceCash.text = data.priceCash;
        this._controllerPriceCredit.text = data.priceCredit;
        this._controllerTotalPriceRate.text = data.totalPriceRate;
        this._controllerTotalPrice.text = data.totalPrice;
        this._orderProductInsuranceID = data.orderProductInsuranceID;
        formatting();
        checkDataDakor(index);
        notifyListeners();
    }

    void calculateTotalCost() {
        if(_controllerUpperLimitRate.text.isNotEmpty && _controllerLowerLimitRate.text.isNotEmpty){
            var _coverageValue = _controllerCoverageValue.text.isNotEmpty ? double.parse(_controllerCoverageValue.text.replaceAll(",", "")) : 0;
            var _cashCost = _controllerPriceCash.text.isNotEmpty ? double.parse(_controllerPriceCash.text.replaceAll(",", "")) : 0;
            var _creditCost = _controllerPriceCredit.text.isNotEmpty ? double.parse(_controllerPriceCredit.text.replaceAll(",", "")) : 0;
            var _upperLimit = _controllerUpperLimit.text.isNotEmpty ? double.parse(_controllerUpperLimit.text.replaceAll(",", "")) : 0;
            var _lowerLimit = _controllerLowerLimit.text.isNotEmpty ? double.parse(_controllerLowerLimit.text.replaceAll(",", "")) : 0;
            var _totalCost = _cashCost + _creditCost;
            if(_controllerPriceCash.text.isEmpty || _controllerPriceCredit.text.isEmpty){
                if(_totalCost > _upperLimit){
                    showSnackBar("Total biaya harus dibawah batas atas");
                    this._controllerPriceCash.clear();
                    this._controllerPriceCredit.clear();
                    this._controllerTotalPriceRate.clear();
                    this._controllerTotalPrice.clear();
                    this._controllerTotalPrice.text = _formatCurrency.formatCurrency(_totalCost.toString());
                    var _totalCostRate = _totalCost / (_coverageValue/100);
                    this._controllerTotalPriceRate.text = _formatCurrency.formatCurrency(_totalCostRate.toString());
                }
            }
            else if(_totalCost != 0) {
                if(_totalCost <= _upperLimit && _totalCost >= _lowerLimit) {
                    this._controllerTotalPrice.text = _formatCurrency.formatCurrency(_totalCost.toString());
                    var _totalCostRate = _totalCost / (_coverageValue/100);
                    this._controllerTotalPriceRate.text = _formatCurrency.formatCurrency(_totalCostRate.toString());
                } else {
                    showSnackBar("Total biaya harus diantara batas atas dan bawah");
                    this._controllerPriceCash.clear();
                    this._controllerPriceCredit.clear();
                    this._controllerTotalPriceRate.clear();
                    this._controllerTotalPrice.clear();
                }
            }
            else {
                showSnackBar("Harap isi Biaya Tunai dan Biaya Kredit");
                this._controllerPriceCash.clear();
                this._controllerPriceCredit.clear();
                this._controllerTotalPriceRate.clear();
                this._controllerTotalPrice.clear();
            }
        }
        notifyListeners();
    }

    void formatting() {
        _controllerCoverageValue.text = formatCurrency.formatCurrency(_controllerCoverageValue.text);
        _controllerUpperLimit.text = _formatCurrency.formatCurrency(_controllerUpperLimit.text);
        _controllerLowerLimit.text = _formatCurrency.formatCurrency(_controllerLowerLimit.text);
        _controllerPriceCash.text = formatCurrency.formatCurrency(_controllerPriceCash.text);
        _controllerPriceCredit.text = formatCurrency.formatCurrency(_controllerPriceCredit.text);
        _controllerTotalPrice.text = formatCurrency.formatCurrency(_controllerTotalPrice.text);
    }

    void getListInsuranceType(BuildContext context, int flag, AdditionalInsuranceModel data, int index) async{
        _listInsuranceType.clear();
        this._insuranceTypeSelected = null;
        loadData = true;

        try{
            final ioc = new HttpClient();
            ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
            final _http = IOClient(ioc);
            var _body = jsonEncode({
                "P_INSR_COMPANY_ID" : "02",
                "P_INSR_LEVEL" : "2",
                "P_INSR_PRODUCT_ID" :"005",
                "P_PARA_OBJECT_ID" :"004"
//                Provider.of<InformationObjectUnitChangeNotifier>(context, listen: false).objectSelected.id
            });

            String urlPublic = await storage.read(key: "urlPublic");
            final _response = await _http.post(
                "${BaseUrl.urlGeneral}",
                // "${urlPublic}api/asuransi/get-insurance-type",
                body: _body,
                headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
            );

            print(_response.statusCode);
            final _data = jsonDecode(_response.body);
            for(int i=0; i < _data['data'].length;i++){
                _listInsuranceType.add(InsuranceTypeModel(_data['data'][i]['PARENT_KODE'], _data['data'][i]['KODE'], _data['data'][i]['DESKRIPSI'].toString().trim()));
            }
            print(_listInsuranceType.length);
            loadData = false;
            if(flag != 0){
                setValueForEditAdditionalInsurance(data, index);
            }
        } catch (e) {
            loadData = false;
        }
        notifyListeners();
    }

    Future<void> getLowerUpperLimit(BuildContext context) async{
        try{
            if(this._productSelected != null){
                loadData = true;
                final ioc = new HttpClient();
                ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
                final _http = IOClient(ioc);
                var _providerPhoto = Provider.of<FormMFotoChangeNotifier>(context,listen: false);
                var _providerInfoAppl = Provider.of<InfoAppChangeNotifier>(context,listen: false);
                var _providerInfoUnitObject = Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false);
                var _providerInfoCollateral = Provider.of<InformationCollateralChangeNotifier>(context,listen: false);
                SharedPreferences _preference = await SharedPreferences.getInstance();
                this._loadData = true;
                var _body = jsonEncode(
                    {
                        "P_APPLICATION_DATE": _providerInfoAppl.controllerOrderDate.text,
                        "P_INSR_COMPANY_ID": this._companySelected.KODE,
                        "P_INSR_FIN_TYPE_ID": _preference.getString("cust_type") != "COM" ? "${_providerPhoto.typeOfFinancingModelSelected.financingTypeId}":"${_providerInfoUnitObject.typeOfFinancingModelSelected.financingTypeId}",
                        "P_INSR_GENRE_ID": _providerInfoUnitObject.objectUsageModel.id,
                        "P_INSR_OBJT_ID": _providerInfoUnitObject.objectSelected.id,
                        "P_INSR_PROD_MATRIX": _providerInfoUnitObject.prodMatrixId,
                        "P_INSR_PERIOD": "${this._controllerPeriodType.text}",
                        "P_INSR_PRODUCT_ID": "${this._productSelected.KODE}",
                        "P_PARA_CP_CENTRA_ID": "${_preference.getString("SentraD")}",// sentra D
                    }
                );
                print('_body getLowerUpperLimit $_body');
                var _providerParentIndividu = Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false);
                DateTime _timeStartValidate = DateTime.now();
                _providerParentIndividu.insertLog(context, _timeStartValidate, DateTime.now(), _body, "Calculate Batas Atas dan Batas Bawah Tambahan", "Calculate Batas Atas dan Batas Bawah Tambahan");

                String _batasAtasBawahTambahan = await storage.read(key: "BatasAtasBawahTambahan");
                final _response = await _http.post(
                    "${BaseUrl.urlGeneral}$_batasAtasBawahTambahan",
                    body: _body,
                    headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
                ).timeout(Duration(seconds: 30));
                final _result = jsonDecode(_response.body);
                print("result getLowerUpperLimit = $_result");
                final _data = _result['data'];
                if(_response.statusCode == 200){
                    if(_data[0]['PARA_LOWER_LIMIT'].toString() == "0" && _data[0]['PARA_UPPER_LIMIT'].toString() == "0"){
                        showSnackBar("Mappingan untuk rate asuransi belum ada");
                    }
                    else {
                        this._coverageTypeSelected = CoverageTypeModel(_data[0]['PARA_INSR_SOURCE_TYPE_ID'], _data[0]['PARA_INSR_SOURCE_TYPE_NAME']);
                        this._controllerCoverageType.text = "${_data[0]['PARA_INSR_SOURCE_TYPE_ID']} - ${_data[0]['PARA_INSR_SOURCE_TYPE_NAME']}";
                        getNilaiPertanggungan(context, _data[0]);
                        // double _valueUpperLimit = double.parse(this._controllerUpperLimitRate.text = _data[0]['PARA_UPPER_LIMIT'].toString());
                        // double _valueLowerLimit = double.parse(this._controllerLowerLimitRate.text = _data[0]['PARA_LOWER_LIMIT'].toString());
                        // double _valueCoverage = double.parse(this._controllerCoverageValue.text.replaceAll(",", ""));
                        // double _resultValueUpperLimit = _valueUpperLimit * _valueCoverage / 100;
                        // double _resultValueLowerLimit = _valueLowerLimit * _valueCoverage / 100;
                        // this._controllerUpperLimit.text = this._formatCurrency.formatCurrency(_resultValueUpperLimit.toString());
                        // this._controllerLowerLimit.text = this._formatCurrency.formatCurrency(_resultValueLowerLimit.toString());
                        loadData = false;
                    }
                }
                else{
                    loadData = false;
                    // showSnackBar("Error ${_response.statusCode} api $_batasAtasBawahTambahan");
                }
            } else {
                showSnackBar("Silahkan pilih produk");
            }
        }
        catch(e){
            loadData = false;
            // showSnackBar("Error ${e.toString()}");
        }
        loadData = false;
        notifyListeners();
    }

    void getNilaiPertanggungan(BuildContext context, Map data) async {
        var _providerInfoUnitObject = Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false);
        var _providerInfoCollateral = Provider.of<InformationCollateralChangeNotifier>(context,listen: false);
        var _providerInfCreditStructure = Provider.of<InfoCreditStructureChangeNotifier>(context,listen: false);
        var _providerAsuransiUtama = Provider.of<InfoMajorInsuranceChangeNotifier>(context, listen: false);
        var _providerAsuransiTambahan = Provider.of<InfoAdditionalInsuranceChangeNotifier>(context, listen: false);
        int _allCredit = 0;
        int _creditInsrLife = 0;

        //fee credit
        for(int i=0; i<_providerInfCreditStructure.listInfoFeeCreditStructure.length; i++){
          if(_providerInfCreditStructure.listInfoFeeCreditStructure[i].feeTypeModel.id == "01" || _providerInfCreditStructure.listInfoFeeCreditStructure[i].feeTypeModel.id == "02"){
            _allCredit += double.parse(_providerInfCreditStructure.listInfoFeeCreditStructure[i].creditCost.replaceAll(",", "")).toInt();
          }
        }
        //asuransi utama, perluasan
        for(int i=0; i<_providerAsuransiUtama.listFormMajorInsurance.length; i++){
          _allCredit += double.parse(_providerAsuransiUtama.listFormMajorInsurance[i].priceCredit.replaceAll(",", "")).toInt();
        }
        //asuransi tambahan
        for(int i=0; i<_providerAsuransiTambahan.listFormAdditionalInsurance.length; i++){
          if(_providerAsuransiTambahan.listFormAdditionalInsurance[i].product.KODE != "007")
            _allCredit += double.parse(_providerAsuransiTambahan.listFormAdditionalInsurance[i].priceCredit.replaceAll(",", "")).toInt();
        }

        for(int i=0; i<_providerAsuransiTambahan.listFormAdditionalInsurance.length; i++){
          if(_providerAsuransiTambahan.listFormAdditionalInsurance[i].product.KODE == "007")
          _creditInsrLife += double.parse(_providerAsuransiTambahan.listFormAdditionalInsurance[i].priceCredit.replaceAll(",", "")).toInt();
        }
        print("total all credit = $_allCredit");

        this._loadData = true;
        final ioc = new HttpClient();
        ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
        final _http = IOClient(ioc);
        print("showroom ${_providerInfoCollateral.controllerHargaJualShowroom.text}");
        var _body = jsonEncode(
            {
                "all_Credit": _allCredit,
                "appraisal_Price": _providerInfoCollateral.controllerTaksasiPrice.text.isNotEmpty ? int.parse(_providerInfoCollateral.controllerTaksasiPrice.text.replaceAll(",", "").split(".")[0]) : 0,
                "building_Price": _providerInfoCollateral.controllerBuildingArea.text.isNotEmpty ? int.parse(_providerInfoCollateral.controllerBuildingArea.text.replaceAll(",", "").split(".")[0]) : 0,
                "colla_Same_Unit": _providerInfoCollateral.radioValueIsCollateralSameWithUnitOto == 0 ? false : true,
                "colla_Type_ID": _providerInfoCollateral.collateralTypeModel != null ? _providerInfoCollateral.collateralTypeModel.id : "",
                "credit_Insr_Life": this._insuranceTypeSelected != null && this._coverageTypeSelected != null ? this._insuranceTypeSelected.KODE == "1" && this._coverageTypeSelected.KODE == "007" ? 0 : _creditInsrLife : _creditInsrLife,
                "harga_Objek": int.parse(_providerInfCreditStructure.controllerObjectPrice.text.replaceAll(",", "").split(".")[0]),
                "jaminan_DP": _providerInfoCollateral.controllerDPJaminan.text.isNotEmpty ? int.parse(_providerInfoCollateral.controllerDPJaminan.text.replaceAll(",", "").split(".")[0]) : 0,
                "jenis_Pertanggungan": this._coverageTypeSelected != null ? this._coverageTypeSelected.KODE : "",
                "jenis_Rehab": _providerInfoUnitObject.rehabTypeSelected != null ? _providerInfoUnitObject.rehabTypeSelected.id : "",
                "jumlah_Pinjaman": int.parse(_providerInfCreditStructure.controllerTotalLoan.text.replaceAll(",", "").split(".")[0]),
                "level_Asuransi": this._insuranceTypeSelected != null ? this._insuranceTypeSelected.KODE : "",
                "limit_Asuransi_Perluasan": "",
                "nett_DP": int.parse(_providerInfCreditStructure.controllerNetDP.text.replaceAll(",", "").split(".")[0]),
                "showroom_Price": _providerInfoCollateral.collateralTypeModel != null ? _providerInfoCollateral.collateralTypeModel.id != "003" ? int.parse(_providerInfoCollateral.controllerHargaJualShowroom.text.replaceAll(",", "").split(".")[0]) : 0 : 0,
                "total_Price": int.parse(_providerInfCreditStructure.controllerTotalPrice.text.replaceAll(",", "").split(".")[0])
            }
        );
        print("body nilai pertanggunan = $_body");
        var _providerParentIndividu = Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false);
        DateTime _timeStartValidate = DateTime.now();
        _providerParentIndividu.insertLog(context, _timeStartValidate, DateTime.now(), _body, "Calculate Nilai Pertanggungan Tambahan", "Calculate Nilai Pertanggungan Tambahan");

        try{
            String _fieldNilaiPertanggungan = await storage.read(key: "FieldNilaiPertanggungan");
            print("${BaseUrl.urlGeneral}$_fieldNilaiPertanggungan");
            final _response = await _http.post(
                "${BaseUrl.urlGeneral}$_fieldNilaiPertanggungan",
                body: _body,
                headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
            ).timeout(Duration(seconds: 30));

            final _result = jsonDecode(_response.body);
            print("cek result nilai pertanggungan $_result");
            if(_response.statusCode == 200){
              if(_result["data"]['new_id_jenis_pertanggungan'] != "" && _result["data"]['new_desc_jenis_pertanggungan'] != ""){
                this._coverageTypeSelected = CoverageTypeModel(_result["data"]['new_id_jenis_pertanggungan'], _result["data"]['new_desc_jenis_pertanggungan']);
                this._controllerCoverageType.text = "${_result["data"]['PARA_INSR_SOURCE_TYPE_ID']} - ${_result["data"]['PARA_INSR_SOURCE_TYPE_NAME']}";
              }
              this._controllerCoverageValue.text = formatCurrency.formatCurrency(_result['data']['nilai_Pertanggungan'].toString());
              this._flagDisableNilaiPertanggungan = _result['data']['flag_Dsble_Nilai_Pertanggungan']; // true = field disable | false = field enable
              _countLimitLowerUpper(data);
            }
            else {
                this._controllerCoverageValue.text = "0.00";
            }
            this._loadData = false;
        }
        catch(e){
            this._loadData = false;
            showSnackBar("error FieldNilaiPertanggungan ${e.toString()}");
        }
        notifyListeners();
    }

    void _countLimitLowerUpper(Map data){
        double _valueUpperLimit = double.parse(this._controllerUpperLimitRate.text = data['PARA_UPPER_LIMIT'].toString());
        double _valueLowerLimit = double.parse(this._controllerLowerLimitRate.text = data['PARA_LOWER_LIMIT'].toString());
        double _valueCoverage = double.parse(this._controllerCoverageValue.text.replaceAll(",", ""));
        double _resultValueUpperLimit = _valueUpperLimit * _valueCoverage / 100;
        double _resultValueLowerLimit = _valueLowerLimit * _valueCoverage / 100;
        this._controllerUpperLimit.text = this._formatCurrency.formatCurrency(_resultValueUpperLimit.round().toString());//this._formatCurrency.formatCurrency(_resultValueUpperLimit.toString());
        this._controllerLowerLimit.text = this._formatCurrency.formatCurrency(_resultValueLowerLimit.round().toString());
        this._controllerPriceCredit.text = this._formatCurrency.formatCurrency(_resultValueLowerLimit.round().toString());
    }

    Future <void> setPreference(BuildContext context,int flag,AdditionalInsuranceModel data,int index) async {
        getDataFromDashboard(context);
        SharedPreferences _preference = await SharedPreferences.getInstance();
        var _providerInfoCreditStructure = Provider.of<InfoCreditStructureChangeNotifier>(context,listen: false);
        var _providerUnitObject = Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false);
        // this._controllerCoverageValue.text = _providerInfoCreditStructure.controllerObjectPrice.text;
        this._insuranceTypeSelected = this._listInsuranceType[0];
        this._custType = _preference.getString("cust_type");
        this._lastKnownState = _preference.getString("last_known_state");
        this._disableJenisPenawaran = false;
        if(_preference.getString("jenis_penawaran") == "002"){
          this._disableJenisPenawaran = true;
        }
        if(this._lastKnownState == "AOS" || this._lastKnownState == "CONA" || this._lastKnownState == "IA" || this._lastKnownState == "PAC") {
          this._isDisablePACIAAOSCONA = true;
        }
        // if (int.parse(_providerInfoCreditStructure.periodOfTimeSelected) <= 6){
        //     if (_providerUnitObject.rehabTypeSelected != null){
        //         if(_providerUnitObject.rehabTypeSelected.id == "001"){
        //             this._controllerPeriodType.text = "12";
        //         } else{
        //             this._controllerPeriodType.text = "6";
        //         }
        //     }else{
        //         this._controllerPeriodType.text = "6";
        //     }
        // }
        // else {
        //     this._controllerPeriodType.text = ((int.parse(_providerInfoCreditStructure.periodOfTimeSelected)/6).ceil()*6).toString();
        // }
        await getPeriode(context, _providerInfoCreditStructure.periodOfTimeSelected);
        if(flag != 0){
            setValueForEditAdditionalInsurance(data, index);
        }
    }

    Future<void> getPeriode(BuildContext context, String tenor) async{
        this._loadData = true;
        var _providerInfoUnitObject = Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false);
        try{
            final ioc = new HttpClient();
            ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
            final _http = IOClient(ioc);

            var _body = jsonEncode(
                {
                    "P_JENIS_REHAB": _providerInfoUnitObject.rehabTypeSelected != null ?_providerInfoUnitObject.rehabTypeSelected.id : "",
                    "P_TENOR": int.parse(tenor)
                }
            );
            print("body period = $_body");
            String _fieldJenisAsuransi = await storage.read(key: "FieldPeriodeAsuransi");
            final _response = await _http.post(
                "${BaseUrl.urlGeneral}$_fieldJenisAsuransi",
                body: _body,
                headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
            ).timeout(Duration(seconds: 30));
            final _result = jsonDecode(_response.body);
            print("result period = $_result");
            List _data = _result['data'];
            if(_response.statusCode == 200){
                this._controllerPeriodType.text = _data[0];
            }
            this._loadData = false;
        }
        catch(e){
            this._loadData = false;
        }
        notifyListeners();
    }

    ShowMandatoryInfoAsuransiTambahanModel _showMandatoryInfoAsuransiTambahanIdeModel;
    ShowMandatoryInfoAsuransiTambahanModel _showMandatoryInfoAsuransiTambahanIdeCompanyModel;
    void showMandatoryIdeModel(BuildContext context){
        _showMandatoryInfoAsuransiTambahanIdeModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoAsuransiTambahanIdeModel;
        _showMandatoryInfoAsuransiTambahanIdeCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoAsuransiTambahanIdeCompanyModel;
    }

    ShowMandatoryInfoAsuransiTambahanModel _showMandatoryInfoAsuransiTambahanRegulerSurveyModel;
    ShowMandatoryInfoAsuransiTambahanModel _showMandatoryInfoAsuransiTambahanRegulerSurveyCompanyModel;
    void showMandatoryRegulerSurveyModel(BuildContext context){
        _showMandatoryInfoAsuransiTambahanRegulerSurveyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoAsuransiTambahanRegulerSurveyModel;
        _showMandatoryInfoAsuransiTambahanRegulerSurveyCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoAsuransiTambahanRegulerSurveyCompanyModel;
    }

    ShowMandatoryInfoAsuransiTambahanModel _showMandatoryInfoAsuransiTambahanPacModel;
    ShowMandatoryInfoAsuransiTambahanModel _showMandatoryInfoAsuransiTambahanPacCompanyModel;
    void showMandatoryPacModel(BuildContext context){
        _showMandatoryInfoAsuransiTambahanPacModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoAsuransiTambahanPacModel;
        _showMandatoryInfoAsuransiTambahanPacCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoAsuransiTambahanPacCompanyModel;
    }

    ShowMandatoryInfoAsuransiTambahanModel _showMandatoryInfoAsuransiTambahanResurveyModel;
    ShowMandatoryInfoAsuransiTambahanModel _showMandatoryInfoAsuransiTambahanResurveyCompanyModel;
    void showMandatoryResurveyModel(BuildContext context){
        _showMandatoryInfoAsuransiTambahanResurveyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoAsuransiTambahanResurveyModel;
        _showMandatoryInfoAsuransiTambahanResurveyCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoAsuransiTambahanResurveyCompanyModel;
    }

    ShowMandatoryInfoAsuransiTambahanModel _showMandatoryInfoAsuransiTambahanDakorModel;
    ShowMandatoryInfoAsuransiTambahanModel _showMandatoryInfoAsuransiTambahanDakorCompanyModel;
    void showMandatoryDakorModel(BuildContext context){
        _showMandatoryInfoAsuransiTambahanDakorModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoAsuransiTambahanDakorModel;
        _showMandatoryInfoAsuransiTambahanDakorCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoAsuransiTambahanDakorCompanyModel;
    }

    void getDataFromDashboard(BuildContext context){
        showMandatoryIdeModel(context);
        showMandatoryRegulerSurveyModel(context);
        showMandatoryPacModel(context);
        showMandatoryResurveyModel(context);
        showMandatoryDakorModel(context);
    }

    bool isInsuranceTypeSelectedVisible() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiTambahanIdeModel.isInsuranceTypeSelectedVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiTambahanRegulerSurveyModel.isInsuranceTypeSelectedVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiTambahanPacModel.isInsuranceTypeSelectedVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiTambahanResurveyModel.isInsuranceTypeSelectedVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiTambahanDakorModel.isInsuranceTypeSelectedVisible;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiTambahanIdeCompanyModel.isInsuranceTypeSelectedVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiTambahanRegulerSurveyCompanyModel.isInsuranceTypeSelectedVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiTambahanPacCompanyModel.isInsuranceTypeSelectedVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiTambahanResurveyCompanyModel.isInsuranceTypeSelectedVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiTambahanDakorCompanyModel.isInsuranceTypeSelectedVisible;
            }
        }
        return value;
    }

    bool isCompanyVisible() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiTambahanIdeModel.isCompanyVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiTambahanRegulerSurveyModel.isCompanyVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiTambahanPacModel.isCompanyVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiTambahanResurveyModel.isCompanyVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiTambahanDakorModel.isCompanyVisible;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiTambahanIdeCompanyModel.isCompanyVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiTambahanRegulerSurveyCompanyModel.isCompanyVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiTambahanPacCompanyModel.isCompanyVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiTambahanResurveyCompanyModel.isCompanyVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiTambahanDakorCompanyModel.isCompanyVisible;
            }
        }
        return value;
    }

    bool isProductVisible() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiTambahanIdeModel.isProductVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiTambahanRegulerSurveyModel.isProductVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiTambahanPacModel.isProductVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiTambahanResurveyModel.isProductVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiTambahanDakorModel.isProductVisible;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiTambahanIdeCompanyModel.isProductVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiTambahanRegulerSurveyCompanyModel.isProductVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiTambahanPacCompanyModel.isProductVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiTambahanResurveyCompanyModel.isProductVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiTambahanDakorCompanyModel.isProductVisible;
            }
        }
        return value;
    }

    bool isPeriodTypeVisible() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiTambahanIdeModel.isPeriodTypeVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiTambahanRegulerSurveyModel.isPeriodTypeVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiTambahanPacModel.isPeriodTypeVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiTambahanResurveyModel.isPeriodTypeVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiTambahanDakorModel.isPeriodTypeVisible;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiTambahanIdeCompanyModel.isPeriodTypeVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiTambahanRegulerSurveyCompanyModel.isPeriodTypeVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiTambahanPacCompanyModel.isPeriodTypeVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiTambahanResurveyCompanyModel.isPeriodTypeVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiTambahanDakorCompanyModel.isPeriodTypeVisible;
            }
        }
        return value;
    }

    bool isCoverageTypeVisible() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiTambahanIdeModel.isCoverageTypeVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiTambahanRegulerSurveyModel.isCoverageTypeVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiTambahanPacModel.isCoverageTypeVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiTambahanResurveyModel.isCoverageTypeVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiTambahanDakorModel.isCoverageTypeVisible;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiTambahanIdeCompanyModel.isCoverageTypeVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiTambahanRegulerSurveyCompanyModel.isCoverageTypeVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiTambahanPacCompanyModel.isCoverageTypeVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiTambahanResurveyCompanyModel.isCoverageTypeVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiTambahanDakorCompanyModel.isCoverageTypeVisible;
            }
        }
        return value;
    }

    bool isCoverageValueVisible() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiTambahanIdeModel.isCoverageValueVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiTambahanRegulerSurveyModel.isCoverageValueVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiTambahanPacModel.isCoverageValueVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiTambahanResurveyModel.isCoverageValueVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiTambahanDakorModel.isCoverageValueVisible;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiTambahanIdeCompanyModel.isCoverageValueVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiTambahanRegulerSurveyCompanyModel.isCoverageValueVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiTambahanPacCompanyModel.isCoverageValueVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiTambahanResurveyCompanyModel.isCoverageValueVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiTambahanDakorCompanyModel.isCoverageValueVisible;
            }
        }
        return value;
    }

    bool isUpperLimitRateVisible() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiTambahanIdeModel.isUpperLimitRateVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiTambahanRegulerSurveyModel.isUpperLimitRateVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiTambahanPacModel.isUpperLimitRateVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiTambahanResurveyModel.isUpperLimitRateVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiTambahanDakorModel.isUpperLimitRateVisible;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiTambahanIdeCompanyModel.isUpperLimitRateVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiTambahanRegulerSurveyCompanyModel.isUpperLimitRateVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiTambahanPacCompanyModel.isUpperLimitRateVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiTambahanResurveyCompanyModel.isUpperLimitRateVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiTambahanDakorCompanyModel.isUpperLimitRateVisible;
            }
        }
        return value;
    }

    bool isUpperLimitVisible() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiTambahanIdeModel.isUpperLimitVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiTambahanRegulerSurveyModel.isUpperLimitVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiTambahanPacModel.isUpperLimitVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiTambahanResurveyModel.isUpperLimitVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiTambahanDakorModel.isUpperLimitVisible;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiTambahanIdeCompanyModel.isUpperLimitVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiTambahanRegulerSurveyCompanyModel.isUpperLimitVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiTambahanPacCompanyModel.isUpperLimitVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiTambahanResurveyCompanyModel.isUpperLimitVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiTambahanDakorCompanyModel.isUpperLimitVisible;
            }
        }
        return value;
    }

    bool isLowerLimitRateVisible() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiTambahanIdeModel.isLowerLimitRateVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiTambahanRegulerSurveyModel.isLowerLimitRateVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiTambahanPacModel.isLowerLimitRateVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiTambahanResurveyModel.isLowerLimitRateVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiTambahanDakorModel.isLowerLimitRateVisible;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiTambahanIdeCompanyModel.isLowerLimitRateVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiTambahanRegulerSurveyCompanyModel.isLowerLimitRateVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiTambahanPacCompanyModel.isLowerLimitRateVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiTambahanResurveyCompanyModel.isLowerLimitRateVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiTambahanDakorCompanyModel.isLowerLimitRateVisible;
            }
        }
        return value;
    }

    bool isLowerLimitVisible() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiTambahanIdeModel.isLowerLimitVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiTambahanRegulerSurveyModel.isLowerLimitVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiTambahanPacModel.isLowerLimitVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiTambahanResurveyModel.isLowerLimitVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiTambahanDakorModel.isLowerLimitVisible;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiTambahanIdeCompanyModel.isLowerLimitVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiTambahanRegulerSurveyCompanyModel.isLowerLimitVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiTambahanPacCompanyModel.isLowerLimitVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiTambahanResurveyCompanyModel.isLowerLimitVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiTambahanDakorCompanyModel.isLowerLimitVisible;
            }
        }
        return value;
    }

    bool isPriceCashVisible() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiTambahanIdeModel.isPriceCashVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiTambahanRegulerSurveyModel.isPriceCashVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiTambahanPacModel.isPriceCashVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiTambahanResurveyModel.isPriceCashVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiTambahanDakorModel.isPriceCashVisible;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiTambahanIdeCompanyModel.isPriceCashVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiTambahanRegulerSurveyCompanyModel.isPriceCashVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiTambahanPacCompanyModel.isPriceCashVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiTambahanResurveyCompanyModel.isPriceCashVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiTambahanDakorCompanyModel.isPriceCashVisible;
            }
        }
        return value;
    }

    bool isPriceCreditVisible() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiTambahanIdeModel.isPriceCreditVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiTambahanRegulerSurveyModel.isPriceCreditVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiTambahanPacModel.isPriceCreditVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiTambahanResurveyModel.isPriceCreditVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiTambahanDakorModel.isPriceCreditVisible;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiTambahanIdeCompanyModel.isPriceCreditVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiTambahanRegulerSurveyCompanyModel.isPriceCreditVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiTambahanPacCompanyModel.isPriceCreditVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiTambahanResurveyCompanyModel.isPriceCreditVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiTambahanDakorCompanyModel.isPriceCreditVisible;
            }
        }
        return value;
    }

    bool isTotalPriceRateVisible() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiTambahanIdeModel.isTotalPriceRateVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiTambahanRegulerSurveyModel.isTotalPriceRateVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiTambahanPacModel.isTotalPriceRateVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiTambahanResurveyModel.isTotalPriceRateVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiTambahanDakorModel.isTotalPriceRateVisible;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiTambahanIdeCompanyModel.isTotalPriceRateVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiTambahanRegulerSurveyCompanyModel.isTotalPriceRateVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiTambahanPacCompanyModel.isTotalPriceRateVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiTambahanResurveyCompanyModel.isTotalPriceRateVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiTambahanDakorCompanyModel.isTotalPriceRateVisible;
            }
        }
        return value;
    }

    bool isTotalPriceVisible() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiTambahanIdeModel.isTotalPriceVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiTambahanRegulerSurveyModel.isTotalPriceVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiTambahanPacModel.isTotalPriceVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiTambahanResurveyModel.isTotalPriceVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiTambahanDakorModel.isTotalPriceVisible;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiTambahanIdeCompanyModel.isTotalPriceVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiTambahanRegulerSurveyCompanyModel.isTotalPriceVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiTambahanPacCompanyModel.isTotalPriceVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiTambahanResurveyCompanyModel.isTotalPriceVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiTambahanDakorCompanyModel.isTotalPriceVisible;
            }
        }
        return value;
    }

    bool isInsuranceTypeSelectedMandatory() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiTambahanIdeModel.isInsuranceTypeSelectedMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiTambahanRegulerSurveyModel.isInsuranceTypeSelectedMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiTambahanPacModel.isInsuranceTypeSelectedMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiTambahanResurveyModel.isInsuranceTypeSelectedMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiTambahanDakorModel.isInsuranceTypeSelectedMandatory;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiTambahanIdeCompanyModel.isInsuranceTypeSelectedMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiTambahanRegulerSurveyCompanyModel.isInsuranceTypeSelectedMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiTambahanPacCompanyModel.isInsuranceTypeSelectedMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiTambahanResurveyCompanyModel.isInsuranceTypeSelectedMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiTambahanDakorCompanyModel.isInsuranceTypeSelectedMandatory;
            }
        }
        return value;
    }

    bool isCompanyMandatory() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiTambahanIdeModel.isCompanyMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiTambahanRegulerSurveyModel.isCompanyMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiTambahanPacModel.isCompanyMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiTambahanResurveyModel.isCompanyMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiTambahanDakorModel.isCompanyMandatory;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiTambahanIdeCompanyModel.isCompanyMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiTambahanRegulerSurveyCompanyModel.isCompanyMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiTambahanPacCompanyModel.isCompanyMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiTambahanResurveyCompanyModel.isCompanyMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiTambahanDakorCompanyModel.isCompanyMandatory;
            }
        }
        return value;
    }

    bool isProductMandatory() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiTambahanIdeModel.isProductMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiTambahanRegulerSurveyModel.isProductMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiTambahanPacModel.isProductMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiTambahanResurveyModel.isProductMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiTambahanDakorModel.isProductMandatory;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiTambahanIdeCompanyModel.isProductMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiTambahanRegulerSurveyCompanyModel.isProductMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiTambahanPacCompanyModel.isProductMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiTambahanResurveyCompanyModel.isProductMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiTambahanDakorCompanyModel.isProductMandatory;
            }
        }
        return value;
    }

    bool isPeriodTypeMandatory() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiTambahanIdeModel.isPeriodTypeMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiTambahanRegulerSurveyModel.isPeriodTypeMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiTambahanPacModel.isPeriodTypeMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiTambahanResurveyModel.isPeriodTypeMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiTambahanDakorModel.isPeriodTypeMandatory;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiTambahanIdeCompanyModel.isPeriodTypeMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiTambahanRegulerSurveyCompanyModel.isPeriodTypeMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiTambahanPacCompanyModel.isPeriodTypeMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiTambahanResurveyCompanyModel.isPeriodTypeMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiTambahanDakorCompanyModel.isPeriodTypeMandatory;
            }
        }
        return value;
    }

    bool isCoverageTypeMandatory() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiTambahanIdeModel.isCoverageTypeMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiTambahanRegulerSurveyModel.isCoverageTypeMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiTambahanPacModel.isCoverageTypeMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiTambahanResurveyModel.isCoverageTypeMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiTambahanDakorModel.isCoverageTypeMandatory;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiTambahanIdeCompanyModel.isCoverageTypeMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiTambahanRegulerSurveyCompanyModel.isCoverageTypeMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiTambahanPacCompanyModel.isCoverageTypeMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiTambahanResurveyCompanyModel.isCoverageTypeMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiTambahanDakorCompanyModel.isCoverageTypeMandatory;
            }
        }
        return value;
    }

    bool isCoverageValueMandatory() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiTambahanIdeModel.isCoverageValueMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiTambahanRegulerSurveyModel.isCoverageValueMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiTambahanPacModel.isCoverageValueMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiTambahanResurveyModel.isCoverageValueMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiTambahanDakorModel.isCoverageValueMandatory;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiTambahanIdeCompanyModel.isCoverageValueMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiTambahanRegulerSurveyCompanyModel.isCoverageValueMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiTambahanPacCompanyModel.isCoverageValueMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiTambahanResurveyCompanyModel.isCoverageValueMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiTambahanDakorCompanyModel.isCoverageValueMandatory;
            }
        }
        return value;
    }

    bool isUpperLimitRateMandatory() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiTambahanIdeModel.isUpperLimitRateMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiTambahanRegulerSurveyModel.isUpperLimitRateMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiTambahanPacModel.isUpperLimitRateMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiTambahanResurveyModel.isUpperLimitRateMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiTambahanDakorModel.isUpperLimitRateMandatory;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiTambahanIdeCompanyModel.isUpperLimitRateMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiTambahanRegulerSurveyCompanyModel.isUpperLimitRateMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiTambahanPacCompanyModel.isUpperLimitRateMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiTambahanResurveyCompanyModel.isUpperLimitRateMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiTambahanDakorCompanyModel.isUpperLimitRateMandatory;
            }
        }
        return value;
    }

    bool isUpperLimitMandatory() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiTambahanIdeModel.isUpperLimitMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiTambahanRegulerSurveyModel.isUpperLimitMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiTambahanPacModel.isUpperLimitMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiTambahanResurveyModel.isUpperLimitMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiTambahanDakorModel.isUpperLimitMandatory;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiTambahanIdeCompanyModel.isUpperLimitMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiTambahanRegulerSurveyCompanyModel.isUpperLimitMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiTambahanPacCompanyModel.isUpperLimitMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiTambahanResurveyCompanyModel.isUpperLimitMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiTambahanDakorCompanyModel.isUpperLimitMandatory;
            }
        }
        return value;
    }

    bool isLowerLimitRateMandatory() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiTambahanIdeModel.isLowerLimitRateMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiTambahanRegulerSurveyModel.isLowerLimitRateMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiTambahanPacModel.isLowerLimitRateMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiTambahanResurveyModel.isLowerLimitRateMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiTambahanDakorModel.isLowerLimitRateMandatory;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiTambahanIdeCompanyModel.isLowerLimitRateMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiTambahanRegulerSurveyCompanyModel.isLowerLimitRateMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiTambahanPacCompanyModel.isLowerLimitRateMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiTambahanResurveyCompanyModel.isLowerLimitRateMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiTambahanDakorCompanyModel.isLowerLimitRateMandatory;
            }
        }
        return value;
    }

    bool isLowerLimitMandatory() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiTambahanIdeModel.isLowerLimitMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiTambahanRegulerSurveyModel.isLowerLimitMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiTambahanPacModel.isLowerLimitMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiTambahanResurveyModel.isLowerLimitMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiTambahanDakorModel.isLowerLimitMandatory;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiTambahanIdeCompanyModel.isLowerLimitMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiTambahanRegulerSurveyCompanyModel.isLowerLimitMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiTambahanPacCompanyModel.isLowerLimitMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiTambahanResurveyCompanyModel.isLowerLimitMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiTambahanDakorCompanyModel.isLowerLimitMandatory;
            }
        }
        return value;
    }

    bool isPriceCashMandatory() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiTambahanIdeModel.isPriceCashMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiTambahanRegulerSurveyModel.isPriceCashMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiTambahanPacModel.isPriceCashMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiTambahanResurveyModel.isPriceCashMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiTambahanDakorModel.isPriceCashMandatory;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiTambahanIdeCompanyModel.isPriceCashMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiTambahanRegulerSurveyCompanyModel.isPriceCashMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiTambahanPacCompanyModel.isPriceCashMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiTambahanResurveyCompanyModel.isPriceCashMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiTambahanDakorCompanyModel.isPriceCashMandatory;
            }
        }
        return value;
    }

    bool isPriceCreditMandatory() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiTambahanIdeModel.isPriceCreditMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiTambahanRegulerSurveyModel.isPriceCreditMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiTambahanPacModel.isPriceCreditMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiTambahanResurveyModel.isPriceCreditMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiTambahanDakorModel.isPriceCreditMandatory;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiTambahanIdeCompanyModel.isPriceCreditMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiTambahanRegulerSurveyCompanyModel.isPriceCreditMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiTambahanPacCompanyModel.isPriceCreditMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiTambahanResurveyCompanyModel.isPriceCreditMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiTambahanDakorCompanyModel.isPriceCreditMandatory;
            }
        }
        return value;
    }

    bool isTotalPriceRateMandatory() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiTambahanIdeModel.isTotalPriceRateMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiTambahanRegulerSurveyModel.isTotalPriceRateMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiTambahanPacModel.isTotalPriceRateMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiTambahanResurveyModel.isTotalPriceRateMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiTambahanDakorModel.isTotalPriceRateMandatory;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiTambahanIdeCompanyModel.isTotalPriceRateMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiTambahanRegulerSurveyCompanyModel.isTotalPriceRateMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiTambahanPacCompanyModel.isTotalPriceRateMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiTambahanResurveyCompanyModel.isTotalPriceRateMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiTambahanDakorCompanyModel.isTotalPriceRateMandatory;
            }
        }
        return value;
    }

    bool isTotalPriceMandatory() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiTambahanIdeModel.isTotalPriceMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiTambahanRegulerSurveyModel.isTotalPriceMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiTambahanPacModel.isTotalPriceMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiTambahanResurveyModel.isTotalPriceMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiTambahanDakorModel.isTotalPriceMandatory;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiTambahanIdeCompanyModel.isTotalPriceMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiTambahanRegulerSurveyCompanyModel.isTotalPriceMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiTambahanPacCompanyModel.isTotalPriceMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiTambahanResurveyCompanyModel.isTotalPriceMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiTambahanDakorCompanyModel.isTotalPriceMandatory;
            }
        }
        return value;
    }

    void checkDataDakor(int index) async{
        var _check = await _dbHelper.selectMS2ApplInsuranceAdditional();
        if(_check.isNotEmpty && _lastKnownState == "DKR"){
            insuranceTypeDakor = this._insuranceTypeSelected.KODE.toString() != _check[index]['insuran_type'].toString() || _check[index]['edit_insuran_type_parent'] == "1";
            companyDakor = this._companySelected.KODE != _check[index]['company_id'] || _check[index]['edit_company_id'] == "1";
            productDakor = this._productSelected.KODE != _check[index]['product'] || _check[index]['edit_product'] == "1";
            periodDakor = this._controllerPeriodType.text.toString() != _check[index]['period'].toString() || _check[index]['edit_period'] == "1";
            coverageTypeDakor = this._controllerCoverageType.text.toString() != _check[index]['coverage_type'].toString() || _check[index]['edit_coverage_type'] == "1";
            coverageValueDakor = this._controllerCoverageValue.text.toString() != _check[index]['coverage_amt'].toString() || _check[index]['edit_coverage_amt'] == "1";
            upperLimitRateDakor =  this._controllerUpperLimitRate.text.toString() != _check[index]['upper_limit_pct'].toString() || _check[index]['edit_upper_limit_pct'] == "1";
            upperLimitValueDakor = this._controllerUpperLimit.text.toString() != _check[index]['upper_limit_amt'].toString() || _check[index]['edit_upper_limit_amt'] == "1";
            lowerLimitRateDakor = this._controllerLowerLimitRate.text.toString() != _check[index]['lower_limit_pct'].toString() || _check[index]['edit_lower_limit_pct'] == "1";
            lowerLimitValueDakor = this._controllerLowerLimit.text.toString() != _check[index]['lower_limit_amt'].toString() || _check[index]['edit_lower_limit_amt'] == "1";
            cashDakor = double.parse(this._controllerPriceCash.text.replaceAll(",", "")).toString().split(".")[0] != _check[index]['cash_amt'].toString() || _check[index]['edit_cash_amt'] == "1";
            creditDakor = double.parse(this._controllerPriceCredit.text.replaceAll(",", "")).toString().split(".")[0] != _check[index]['credit_amt'].toString() || _check[index]['edit_credit_amt'] == "1";
            totalPriceRateDakor = this._controllerTotalPriceRate.text != _check[index]['total_pct'].toString() || _check[index]['edit_total_pct'] == "1";
            totalPriceDakor = double.parse(this._controllerTotalPrice.text.replaceAll(",", "")).toString().split(".")[0] != _check[index]['total_amt'].toString() || _check[index]['edit_total_amt'] == "1";

            if(insuranceTypeDakor || companyDakor || productDakor || periodDakor || coverageTypeDakor || coverageValueDakor ||
                upperLimitRateDakor || upperLimitValueDakor || lowerLimitRateDakor ||
                lowerLimitValueDakor || cashDakor || creditDakor || totalPriceRateDakor || totalPriceDakor){
                isEdit = true;
            } else {
                isEdit = false;
            }
        }
        notifyListeners();
    }

    bool get totalPriceDakor => _totalPriceDakor;

    set totalPriceDakor(bool value) {
      _totalPriceDakor = value;
      notifyListeners();
    }

    bool get totalPriceRateDakor => _totalPriceRateDakor;

    set totalPriceRateDakor(bool value) {
      _totalPriceRateDakor = value;
      notifyListeners();
    }

    bool get creditDakor => _creditDakor;

    set creditDakor(bool value) {
      _creditDakor = value;
      notifyListeners();
    }

    bool get cashDakor => _cashDakor;

    set cashDakor(bool value) {
      _cashDakor = value;
      notifyListeners();
    }

    bool get lowerLimitValueDakor => _lowerLimitValueDakor;

    set lowerLimitValueDakor(bool value) {
      _lowerLimitValueDakor = value;
      notifyListeners();
    }

    bool get lowerLimitRateDakor => _lowerLimitRateDakor;

    set lowerLimitRateDakor(bool value) {
      _lowerLimitRateDakor = value;
      notifyListeners();
    }

    bool get upperLimitValueDakor => _upperLimitValueDakor;

    set upperLimitValueDakor(bool value) {
      _upperLimitValueDakor = value;
      notifyListeners();
    }

    bool get upperLimitRateDakor => _upperLimitRateDakor;

    set upperLimitRateDakor(bool value) {
      _upperLimitRateDakor = value;
      notifyListeners();
    }

    bool get coverageValueDakor => _coverageValueDakor;

    set coverageValueDakor(bool value) {
      _coverageValueDakor = value;
      notifyListeners();
    }

    bool get coverageTypeDakor => _coverageTypeDakor;

    set coverageTypeDakor(bool value) {
      _coverageTypeDakor = value;
      notifyListeners();
    }

    bool get periodDakor => _periodDakor;

    set periodDakor(bool value) {
      _periodDakor = value;
      notifyListeners();
    }

    bool get productDakor => _productDakor;

    set productDakor(bool value) {
      _productDakor = value;
      notifyListeners();
    }

    bool get companyDakor => _companyDakor;

    set companyDakor(bool value) {
      _companyDakor = value;
      notifyListeners();
    }

    bool get insuranceTypeDakor => _insuranceTypeDakor;

    set insuranceTypeDakor(bool value) {
      _insuranceTypeDakor = value;
      notifyListeners();
    }

    bool get isEdit => _isEdit;

    set isEdit(bool value) {
      _isEdit = value;
      notifyListeners();
    }

    void showSnackBar(String text){
        this._scaffoldKey.currentState.showSnackBar(new SnackBar(
            content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor));
    }

    Future<void> getLowerUpperLimitUpdated(BuildContext context, AdditionalInsuranceModel dataInsurance, int index) async {
      // try{
      //   if(dataInsurance.product != null){
          loadData = true;
          final ioc = new HttpClient();
          ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
          final _http = IOClient(ioc);
          var _providerPhoto = Provider.of<FormMFotoChangeNotifier>(context,listen: false);
          var _providerInfoAppl = Provider.of<InfoAppChangeNotifier>(context,listen: false);
          var _providerInfoUnitObject = Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false);
          SharedPreferences _preference = await SharedPreferences.getInstance();
          this._loadData = true;
          var _body = jsonEncode(
              {
                "P_APPLICATION_DATE": _providerInfoAppl.controllerOrderDate.text,
                "P_INSR_COMPANY_ID": dataInsurance.company.KODE,
                "P_INSR_FIN_TYPE_ID": _preference.getString("cust_type") != "COM" ? "${_providerPhoto.typeOfFinancingModelSelected.financingTypeId}":"${_providerInfoUnitObject.typeOfFinancingModelSelected.financingTypeId}",
                "P_INSR_GENRE_ID": _providerInfoUnitObject.objectUsageModel.id,
                "P_INSR_OBJT_ID": _providerInfoUnitObject.objectSelected.id,
                "P_INSR_PROD_MATRIX": _providerInfoUnitObject.prodMatrixId,
                "P_INSR_PERIOD": dataInsurance.periodType,
                "P_INSR_PRODUCT_ID": dataInsurance.product.KODE,
                "P_PARA_CP_CENTRA_ID": "${_preference.getString("SentraD")}",
              }
          );
          print('_body getLowerUpperLimit $_body');
          var _providerParentIndividu = Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false);
          DateTime _timeStartValidate = DateTime.now();
          _providerParentIndividu.insertLog(context, _timeStartValidate, DateTime.now(), _body, "Re-Calculate Batas Atas dan Batas Bawah", "Re-Calculate Batas Atas dan Batas Bawah");

          String _batasAtasBawahTambahan = await storage.read(key: "BatasAtasBawahTambahan");
          final _response = await _http.post(
              "${BaseUrl.urlGeneral}$_batasAtasBawahTambahan",
              body: _body,
              headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
          ).timeout(Duration(seconds: 30));
          final _result = jsonDecode(_response.body);
          print("result getLowerUpperLimit = $_result");
          final _data = _result['data'];
          if(_response.statusCode == 200){
            if(_data[0]['PARA_LOWER_LIMIT'].toString() == "0" && _data[0]['PARA_UPPER_LIMIT'].toString() == "0"){
              showSnackBar("Mappingan untuk rate asuransi belum ada");
            }
            else {
              dataInsurance.coverageType = CoverageTypeModel(_data[0]['PARA_INSR_SOURCE_TYPE_ID'], _data[0]['PARA_INSR_SOURCE_TYPE_NAME']);
              getNilaiPertanggunganUpdated(context, _data[0], dataInsurance, index);
              loadData = false;
            }
          }
          else{
            loadData = false;
          }
        // } else {
        //   showSnackBar("Silahkan pilih produk");
        // }
      // }
      // catch(e){
      //   loadData = false;
      // }
      loadData = false;
      notifyListeners();
    }

    Future<void> getNilaiPertanggunganUpdated(BuildContext context, Map data, AdditionalInsuranceModel dataInsurance, int index) async {
      var _providerInfoUnitObject = Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false);
      var _providerInfoCollateral = Provider.of<InformationCollateralChangeNotifier>(context,listen: false);
      var _providerInfCreditStructure = Provider.of<InfoCreditStructureChangeNotifier>(context,listen: false);
      var _providerAsuransiUtama = Provider.of<InfoMajorInsuranceChangeNotifier>(context, listen: false);
      var _providerAsuransiTambahan = Provider.of<InfoAdditionalInsuranceChangeNotifier>(context, listen: false);
      int _allCredit = 0;
      int _creditInsrLife = 0;

      //fee credit
      for(int i=0; i<_providerInfCreditStructure.listInfoFeeCreditStructure.length; i++){
        if(_providerInfCreditStructure.listInfoFeeCreditStructure[i].feeTypeModel.id == "01" || _providerInfCreditStructure.listInfoFeeCreditStructure[i].feeTypeModel.id == "02"){
          _allCredit += double.parse(_providerInfCreditStructure.listInfoFeeCreditStructure[i].creditCost.replaceAll(",", "")).toInt();
        }
      }
      //asuransi utama, perluasan
      for(int i=0; i<_providerAsuransiUtama.listFormMajorInsurance.length; i++){
        _allCredit += double.parse(_providerAsuransiUtama.listFormMajorInsurance[i].priceCredit.replaceAll(",", "")).toInt();
      }
      //asuransi tambahan
      for(int i=0; i<_providerAsuransiTambahan.listFormAdditionalInsurance.length; i++){
        if(_providerAsuransiTambahan.listFormAdditionalInsurance[i].product.KODE != "007")
          _allCredit += double.parse(_providerAsuransiTambahan.listFormAdditionalInsurance[i].priceCredit.replaceAll(",", "")).toInt();
      }

      for(int i=0; i<_providerAsuransiTambahan.listFormAdditionalInsurance.length; i++){
        if(_providerAsuransiTambahan.listFormAdditionalInsurance[i].product.KODE == "007")
          _creditInsrLife += double.parse(_providerAsuransiTambahan.listFormAdditionalInsurance[i].priceCredit.replaceAll(",", "")).toInt();
      }
      print("total all credit = $_allCredit");

      this._loadData = true;
      final ioc = new HttpClient();
      ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
      final _http = IOClient(ioc);
      print("showroom ${_providerInfoCollateral.controllerHargaJualShowroom.text}");
      var _body = jsonEncode(
          {
            "all_Credit": _allCredit,
            "appraisal_Price": _providerInfoCollateral.controllerTaksasiPrice.text.isNotEmpty ? int.parse(_providerInfoCollateral.controllerTaksasiPrice.text.replaceAll(",", "").split(".")[0]) : 0,
            "building_Price": _providerInfoCollateral.controllerBuildingArea.text.isNotEmpty ? int.parse(_providerInfoCollateral.controllerBuildingArea.text.replaceAll(",", "").split(".")[0]) : 0,
            "colla_Same_Unit": _providerInfoCollateral.radioValueIsCollateralSameWithUnitOto == 0 ? false : true,
            "colla_Type_ID": _providerInfoCollateral.collateralTypeModel != null ? _providerInfoCollateral.collateralTypeModel.id : "",
            "credit_Insr_Life": dataInsurance.insuranceType != null && dataInsurance.coverageType != null ? dataInsurance.insuranceType.KODE == "1" && dataInsurance.coverageType.KODE == "007" ? 0 : _creditInsrLife : _creditInsrLife,
            "harga_Objek": int.parse(_providerInfCreditStructure.controllerObjectPrice.text.replaceAll(",", "").split(".")[0]),
            "jaminan_DP": _providerInfoCollateral.controllerDPJaminan.text.isNotEmpty ? int.parse(_providerInfoCollateral.controllerDPJaminan.text.replaceAll(",", "").split(".")[0]) : 0,
            "jenis_Pertanggungan": dataInsurance.coverageType != null ? dataInsurance.coverageType.KODE : "",
            "jenis_Rehab": _providerInfoUnitObject.rehabTypeSelected != null ? _providerInfoUnitObject.rehabTypeSelected.id : "",
            "jumlah_Pinjaman": int.parse(_providerInfCreditStructure.controllerTotalLoan.text.replaceAll(",", "").split(".")[0]),
            "level_Asuransi": dataInsurance.insuranceType != null ? dataInsurance.insuranceType.KODE : "",
            "limit_Asuransi_Perluasan": "",
            "nett_DP": int.parse(_providerInfCreditStructure.controllerNetDP.text.replaceAll(",", "").split(".")[0]),
            "showroom_Price": _providerInfoCollateral.collateralTypeModel != null ? _providerInfoCollateral.collateralTypeModel.id != "003" ? int.parse(_providerInfoCollateral.controllerHargaJualShowroom.text.replaceAll(",", "").split(".")[0]) : 0 : 0,
            "total_Price": int.parse(_providerInfCreditStructure.controllerTotalPrice.text.replaceAll(",", "").split(".")[0])
          }
      );
      print("body nilai pertanggunan = $_body");
      var _providerParentIndividu = Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false);
      DateTime _timeStartValidate = DateTime.now();
      _providerParentIndividu.insertLog(context, _timeStartValidate, DateTime.now(), _body, "Re-Calculate Nilai Pertanggungan Tambahan", "Re-Calculate Nilai Pertanggungan Tambahan");

      try{
        String _fieldNilaiPertanggungan = await storage.read(key: "FieldNilaiPertanggungan");
        print("${BaseUrl.urlGeneral}$_fieldNilaiPertanggungan");
        final _response = await _http.post(
            "${BaseUrl.urlGeneral}$_fieldNilaiPertanggungan",
            body: _body,
            headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
        ).timeout(Duration(seconds: 30));

        final _result = jsonDecode(_response.body);
        print("cek result nilai pertanggungan $_result");
        if(_response.statusCode == 200){
          if(_result["data"]['new_id_jenis_pertanggungan'] != "" && _result["data"]['new_desc_jenis_pertanggungan'] != ""){
            dataInsurance.coverageType = CoverageTypeModel(_result["data"]['new_id_jenis_pertanggungan'], _result["data"]['new_desc_jenis_pertanggungan']);
          }
          if(dataInsurance.coverageValue.replaceAll(",", "") != _result['data']['nilai_Pertanggungan'].toString()) {
            dataInsurance.coverageValue = formatCurrency.formatCurrency(_result['data']['nilai_Pertanggungan'].toString());
            this._flagDisableNilaiPertanggungan = _result['data']['flag_Dsble_Nilai_Pertanggungan']; // true = field disable | false = field enable
            countLimitLowerUpperUpdated(context, data, dataInsurance, index);
          }
        }
        else {
          dataInsurance.coverageValue = "0";
        }
        this._loadData = false;
      }
      catch(e){
        this._loadData = false;
        showSnackBar("error Field Nilai Pertanggungan ${e.toString()}");
      }
      notifyListeners();
    }

    Future<void> countLimitLowerUpperUpdated(BuildContext context, Map data, AdditionalInsuranceModel dataInsurance, int index) {
      double _valueUpperLimit = double.parse(dataInsurance.upperLimitRate = data['PARA_UPPER_LIMIT'].toString());
      double _valueLowerLimit = double.parse(dataInsurance.lowerLimitRate = data['PARA_LOWER_LIMIT'].toString());
      double _valueCoverage = double.parse(dataInsurance.coverageValue.replaceAll(",", ""));
      double _resultValueUpperLimit = _valueUpperLimit * _valueCoverage / 100;
      double _resultValueLowerLimit = _valueLowerLimit * _valueCoverage / 100;
      dataInsurance.upperLimit = this._formatCurrency.formatCurrency(_resultValueUpperLimit.round().toString()); //this._formatCurrency.formatCurrency(_resultValueUpperLimit.toString());
      dataInsurance.lowerLimit = this._formatCurrency.formatCurrency(_resultValueLowerLimit.round().toString());
      dataInsurance.priceCash = "0";
      dataInsurance.priceCredit = this._formatCurrency.formatCurrency(_resultValueLowerLimit.round().toString());
      var _coverageValue = dataInsurance.coverageValue.isNotEmpty ? double.parse(dataInsurance.coverageValue.replaceAll(",", "")) : 0;
      var _cashCost = dataInsurance.priceCash.isNotEmpty ? double.parse(dataInsurance.priceCash.replaceAll(",", "")) : 0;
      var _creditCost = dataInsurance.priceCredit.isNotEmpty ? double.parse(dataInsurance.priceCredit.replaceAll(",", "")) : 0;
      var _totalCost = _cashCost + _creditCost;
      dataInsurance.totalPrice = _formatCurrency.formatCurrency(_totalCost.toString());
      var _totalCostRate = _totalCost / (_coverageValue/100);
      dataInsurance.totalPriceRate = _formatCurrency.formatCurrency(_totalCostRate.toString());
      checkUpdated(context, dataInsurance, index);
    }

    Future<void> checkUpdated(BuildContext context, AdditionalInsuranceModel dataInsurance, int index) {
      checkDataDakorUpdated(dataInsurance, index);
      Provider.of<InfoAdditionalInsuranceChangeNotifier>(context, listen: false)
          .updateListInfoAdditionalInsurance(AdditionalInsuranceModel(
          dataInsurance.insuranceType,
          dataInsurance.colaType,
          dataInsurance.company,
          dataInsurance.product,
          dataInsurance.periodType,
          dataInsurance.coverageType,
          dataInsurance.coverageValue,
          dataInsurance.upperLimitRate,
          dataInsurance.lowerLimitRate,
          dataInsurance.upperLimit,
          dataInsurance.lowerLimit,
          dataInsurance.priceCash,
          dataInsurance.priceCredit,
          dataInsurance.totalPriceRate,
          dataInsurance.totalPrice,
          dataInsurance.isEdit,
          dataInsurance.insuranceTypeDakor,
          dataInsurance.companyDakor,
          dataInsurance.productDakor,
          dataInsurance.periodDakor,
          dataInsurance.coverageTypeDakor,
          dataInsurance.coverageValueDakor,
          dataInsurance.upperLimitRateDakor,
          dataInsurance.upperLimitValueDakor,
          dataInsurance.lowerLimitRateDakor,
          dataInsurance.lowerLimitValueDakor,
          dataInsurance.cashDakor,
          dataInsurance.creditDakor,
          dataInsurance.totalPriceRateDakor,
          dataInsurance.totalPriceDakor,
          dataInsurance.orderProductInsuranceID
      ), context, index);
    }

    void checkDataDakorUpdated(AdditionalInsuranceModel dataInsurance, int index) async {
      var _check = await _dbHelper.selectMS2ApplInsuranceAdditional();
      if(_check.isNotEmpty && _lastKnownState == "DKR"){
        insuranceTypeDakor = dataInsurance.insuranceType.KODE != _check[index]['insuran_type'].toString() || _check[index]['edit_insuran_type_parent'] == "1";
        companyDakor = dataInsurance.company.KODE != _check[index]['company_id'] || _check[index]['edit_company_id'] == "1";
        productDakor = dataInsurance.product.KODE != _check[index]['product'] || _check[index]['edit_product'] == "1";
        periodDakor = dataInsurance.periodType != _check[index]['period'].toString() || _check[index]['edit_period'] == "1";
        coverageTypeDakor = dataInsurance.coverageType.toString() != _check[index]['coverage_type'].toString() || _check[index]['edit_coverage_type'] == "1";
        coverageValueDakor = dataInsurance.coverageValue != _check[index]['coverage_amt'].toString() || _check[index]['edit_coverage_amt'] == "1";
        upperLimitRateDakor = dataInsurance.upperLimitRate != _check[index]['upper_limit_pct'].toString() || _check[index]['edit_upper_limit_pct'] == "1";
        upperLimitValueDakor = dataInsurance.upperLimit != _check[index]['upper_limit_amt'].toString() || _check[index]['edit_upper_limit_amt'] == "1";
        lowerLimitRateDakor = dataInsurance.lowerLimitRate != _check[index]['lower_limit_pct'].toString() || _check[index]['edit_lower_limit_pct'] == "1";
        lowerLimitValueDakor = dataInsurance.lowerLimit != _check[index]['lower_limit_amt'].toString() || _check[index]['edit_lower_limit_amt'] == "1";
        cashDakor = double.parse(dataInsurance.priceCash.replaceAll(",", "")).toString().split(".")[0] != _check[index]['cash_amt'].toString() || _check[index]['edit_cash_amt'] == "1";
        creditDakor = double.parse(dataInsurance.priceCredit.replaceAll(",", "")).toString().split(".")[0] != _check[index]['credit_amt'].toString() || _check[index]['edit_credit_amt'] == "1";
        totalPriceRateDakor = dataInsurance.totalPriceRate != _check[index]['total_pct'].toString() || _check[index]['edit_total_pct'] == "1";
        totalPriceDakor = double.parse(dataInsurance.totalPrice.replaceAll(",", "")).toString().split(".")[0] != _check[index]['total_amt'].toString() || _check[index]['edit_total_amt'] == "1";

        if(insuranceTypeDakor || companyDakor || productDakor || periodDakor || coverageTypeDakor || coverageValueDakor ||
            upperLimitRateDakor || upperLimitValueDakor || lowerLimitRateDakor ||
            lowerLimitValueDakor || cashDakor || creditDakor || totalPriceRateDakor || totalPriceDakor){
          isEdit = true;
        } else {
          isEdit = false;
        }
      }
      notifyListeners();
    }
}