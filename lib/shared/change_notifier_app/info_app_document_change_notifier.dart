import 'dart:io';

import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/models/info_document_detail_model.dart';
import 'package:ad1ms2_dev/models/info_document_model.dart';
import 'package:ad1ms2_dev/models/info_document_type_model.dart';
import 'package:ad1ms2_dev/models/ms2_document_model.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_object_unit_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/marketing_notes_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/taksasi_unit_change_notifier.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/form_m_individu_change_notifier/form_m_parent_individu_change_notifier.dart';
import 'package:ad1ms2_dev/shared/resource/submit_data_partial.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../form_m_company_parent_change_notifier.dart';

class InfoDocumentChangeNotifier with ChangeNotifier{
    List<InfoDocumentModel> _listDocument = [];
    bool _autoValidate = false;
    DbHelper _dbHelper = DbHelper();
    SubmitDataPartial _submitDataPartial = SubmitDataPartial();
    String _lastKnownState = "IDE";

    String get lastKnownState => _lastKnownState;

    set lastKnownState(String value) {
      this._lastKnownState = value;
    }

  List<InfoDocumentModel> get listInfoDocument => _listDocument;

    bool get autoValidate => _autoValidate;

    set autoValidate(bool value) {
        this._autoValidate = value;
        notifyListeners();
    }

    void deleteListInfoDocument(int index) {
        deleteFile(index);
        this._listDocument.removeAt(index);
        notifyListeners();
    }

    void updateListInfoDocument(
        InfoDocumentModel mInfoDocument, BuildContext context, int index) {
        this._listDocument[index] = mInfoDocument;
        notifyListeners();
    }

    void addListInfoDocument(InfoDocumentModel value) {
        _listDocument.add(value);
        if (this._autoValidate) autoValidate = false;
        notifyListeners();
    }

    void clearInfoDocument(){
        this._autoValidate = false;
        this._listDocument.clear();
    }

    Future<void> saveToSQLite(BuildContext context) async{
        List<MS2DocumentModel> _listData = [];
        for(int i=0; i<_listDocument.length; i++){
            _listData.add(MS2DocumentModel(
                "a",
                _listDocument[i].orderSupportingDocumentID,
                "b",
                _listDocument[i].fileHeaderID,
                _listDocument[i].documentDetail.path,
                null,
                null,
                null,
                null,
                _listDocument[i].documentType.docTypeId,
                _listDocument[i].documentType.docTypeName,
                _listDocument[i].documentType.mandatory,
                _listDocument[i].documentType.display,
                _listDocument[i].documentType.flag_unit,
                _listDocument[i].date,
                _listDocument[i].latitude.toString(),
                _listDocument[i].longitude.toString()
            ));
        }
        _dbHelper.insertMS2Document(_listData);
        await _submitDataPartial.submitPhoto(context, "4");
        // String _message = await _submitDataPartial.submitDocument(10,_listData);
        // return _message;
    }

    void deleteFile(int index) async{
        var file = File("${this._listDocument[index].documentDetail.path}");
        await file.delete();
    }

    Future<bool> deleteSQLite() async{
        debugPrint("DELETE DOCUMENT");
        return await _dbHelper.deleteMS2Document();
    }

    Future<void> setDataFromSQLite(BuildContext context, int index) async{
        SharedPreferences _preferences = await SharedPreferences.getInstance();
        var _check = await _dbHelper.selectMS2Document();
        this._lastKnownState = _preferences.getString("last_known_state");
        print("set data document $_check");
        if(_check.isNotEmpty){
            for(int i=0; i<_check.length; i++){
                File _imageFile = File(_check[i]['file_name']);
                List _splitFileName = _imageFile.path.split("/");

                InfoDocumentTypeModel documentType = InfoDocumentTypeModel(_check[i]['document_type_id'], _check[i]['document_type_desc'], _check[i]['mandatory'], _check[i]['display'], _check[i]['flag_unit']);
                InfoDocumentDetailModel documentDetail = InfoDocumentDetailModel(_splitFileName[_splitFileName.length-1], _imageFile, _imageFile.path);
                _listDocument.add(InfoDocumentModel(
                        documentType,
                        _check[i]['orderSupportingDocumentID'],
                        documentDetail,
                        _check[i]['upload_date'],//dateFormat.format(DateTime.parse(_check[i]['upload_date'])),
                        documentDetail.path, //_check[i]['path'], //ga kepake
                        _check[i]['latitude'] != "null" ? double.parse(_check[i]['latitude']) : null,
                        _check[i]['longitude'] != "null" ? double.parse(_check[i]['longitude']) : null,
                        null,
                        _check[i]['file_header_id'],
                        documentDetail.fileName //_check[i]['file_name']
                    )
                );
            }
        }
        var _providerTaksasi = Provider.of<TaksasiUnitChangeNotifier>(context,listen: false);
        var _providerMarketingNotes = Provider.of<MarketingNotesChangeNotifier>(context,listen: false);
        if(_preferences.getString("last_known_state") == "IDE" && index != null){
            if(index != 10){
                if(_preferences.getString("cust_type") == "PER"){
                    Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).isDocumentInfoDone = true;
                    if(Provider.of<TaksasiUnitChangeNotifier>(context,listen: false).isVisible){
                        await _providerTaksasi.setDataTaksasi(context, index);
                        Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).selectedIndex += 1;
                    }
                    else {
                        await _providerMarketingNotes.setDataFromSQLite();
                        Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).selectedIndex += 2;
                    }
                } else {
                    Provider.of<FormMParentCompanyChangeNotifier>(context,listen: false).isDocumentInfoDone = true;
                    if(Provider.of<TaksasiUnitChangeNotifier>(context,listen: false).isVisible){
                        await _providerTaksasi.setDataTaksasi(context, index);
                        Provider.of<FormMParentCompanyChangeNotifier>(context,listen: false).selectedIndex += 1;
                    }
                    else {
                        await _providerMarketingNotes.setDataFromSQLite();
                        Provider.of<FormMParentCompanyChangeNotifier>(context,listen: false).selectedIndex += 2;
                    }
                }
            }
            else{
                if(_preferences.getString("cust_type") == "PER"){
                    Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).selectedIndex = 10;
                } else {
                    Provider.of<FormMParentCompanyChangeNotifier>(context,listen: false).selectedIndex = 10;
                }
            }
        }
        notifyListeners();
    }
}