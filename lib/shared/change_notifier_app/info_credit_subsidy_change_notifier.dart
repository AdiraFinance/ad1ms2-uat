import 'dart:convert';

import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/models/add_detail_installment_credit_subsidy.dart';
import 'package:ad1ms2_dev/models/credit_subsidy_model.dart';
import 'package:ad1ms2_dev/models/cutting_method_model.dart';
import 'package:ad1ms2_dev/models/ms2_appl_refund_model.dart';
import 'package:ad1ms2_dev/models/subsidy_type_model.dart';
import 'package:ad1ms2_dev/screens/app/add_installment_credit_subsidy.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_app_document_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_credit_structure_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_individu_change_notifier/form_m_parent_individu_change_notifier.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../main.dart';
import '../constants.dart';
import '../form_m_company_parent_change_notifier.dart';
import '../format_currency.dart';
import 'package:http/http.dart' as http;

class InfoCreditSubsidyChangeNotifier with ChangeNotifier{
    List<CreditSubsidyModel> _listCreditSubsidy = [];
    bool _autoValidate = false;
    DbHelper _dbHelper = DbHelper();
    String _custType;
    String _lastKnownState;
    bool _isDisablePACIAAOSCONA = false;
    var storage = FlutterSecureStorage();
    List<CreditSubsidyModel> get listInfoCreditSubsidy => _listCreditSubsidy;
    FormatCurrency _formatCurrency = FormatCurrency();

    FormatCurrency get formatCurrency => _formatCurrency;

    bool get autoValidate => _autoValidate;

    set autoValidate(bool value) {
        this._autoValidate = value;
        notifyListeners();
    }

    Future<void> setPreference() async {
        SharedPreferences _preference = await SharedPreferences.getInstance();
        this._custType = _preference.getString("cust_type");
        this._lastKnownState = _preference.getString("last_known_state");
        if(this._lastKnownState == "AOS" || this._lastKnownState == "CONA" || this._lastKnownState == "IA" || this._lastKnownState == "PAC") {
            this._isDisablePACIAAOSCONA = true;
        }
    }

    void deleteListInfoCreditSubsidy(BuildContext context, int index) {
        showDialog(
            context: context,
            barrierDismissible: true,
            builder: (BuildContext context){
                return Theme(
                    data: ThemeData(
                        fontFamily: "NunitoSans",
                        primaryColor: Colors.black,
                        primarySwatch: primaryOrange,
                        accentColor: myPrimaryColor
                    ),
                    child: AlertDialog(
                        title: Text("Warning", style: TextStyle(fontWeight: FontWeight.bold)),
                        content: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                                Text("Apakah kamu yakin menghapus data ini?",),
                            ],
                        ),
                        actions: <Widget>[
                            new FlatButton(
                                onPressed: () {
                                    this._listCreditSubsidy.removeAt(index);
                                    notifyListeners();
                                    Navigator.pop(context);
                                },
                                child: new Text('Ya', style: TextStyle(color: Colors.grey),),
                            ),
                            new FlatButton(
                                onPressed: () => Navigator.of(context).pop(true),
                                child: new Text('Tidak'),
                            ),
                        ],
                    ),
                );
            }
        );
    }

    void updateListInfoCreditSubsidy(CreditSubsidyModel mInfoCreditSubsidy, BuildContext context, int index) {
        this._listCreditSubsidy[index] = mInfoCreditSubsidy;
        notifyListeners();
    }

    void addListInfoCreditSubsidy(CreditSubsidyModel value) {
        _listCreditSubsidy.add(value);
        if (this._autoValidate) autoValidate = false;
        notifyListeners();
    }

    void clearInfoCreditSubsidy(){
        this._autoValidate = false;
        this._listCreditSubsidy.clear();
        isDisablePACIAAOSCONA = false;
    }

    void saveToSQLite() async {
        List<MS2ApplRefundModel> _listApplRefund = [];
        SharedPreferences _preferences = await SharedPreferences.getInstance();
        // for(int i=0; i<_listCreditSubsidy.length; i++){
        //     _listApplRefund.add(MS2ApplRefundModel(
        //         "1",
        //         this._listCreditSubsidy[i].giver.toString(),
        //         this._listCreditSubsidy[i].type.id,
        //         this._listCreditSubsidy[i].type.text,
        //         this._listCreditSubsidy[i].cuttingMethod.id,
        //         this._listCreditSubsidy[i].cuttingMethod.text,
        //         null, // refund_amt
        //         int.parse(this._listCreditSubsidy[i].rateBeforeEff),
        //         int.parse(this._listCreditSubsidy[i].rateBeforeFlat),
        //         null,
        //         int.parse(this._listCreditSubsidy[i].totalInstallment),
        //         1,
        //         DateTime.now().toString(),
        //         _preferences.getString("username"),
        //         null,
        //         null,
        //         null,
        //         null
        //     ));
        // }
        if(_listCreditSubsidy.isNotEmpty) {
            for(int i=0; i<_listCreditSubsidy.length; i++){
                _listCreditSubsidy[i].type.id != "05"
                    ?
                _listApplRefund.add(MS2ApplRefundModel(
                    "123",
                    this._listCreditSubsidy[i].orderSubsidyID != null ? this._listCreditSubsidy[i].orderSubsidyID : "NEW",
                    this._listCreditSubsidy[i].giver.toString(),
                    this._listCreditSubsidy[i].type != null ? this._listCreditSubsidy[i].type.id : "",
                    this._listCreditSubsidy[i].type != null ? this._listCreditSubsidy[i].type.text : "",
                    this._listCreditSubsidy[i].cuttingMethod != null ? this._listCreditSubsidy[i].cuttingMethod.id : "",
                    this._listCreditSubsidy[i].cuttingMethod != null ? this._listCreditSubsidy[i].cuttingMethod.text : "",
                    this._listCreditSubsidy[i].value.isNotEmpty ? double.parse(this._listCreditSubsidy[i].value.replaceAll(",", "")) : 0, // refund_amt
                    this._listCreditSubsidy[i].rateBeforeEff != "" ? double.parse(this._listCreditSubsidy[i].rateBeforeEff.replaceAll(",", "")) : null, //bunga
                    this._listCreditSubsidy[i].rateBeforeFlat != "" ? double.parse(this._listCreditSubsidy[i].rateBeforeFlat.replaceAll(",", "")) : null, //bunga
                    null,
                    null,
                    1,
                    DateTime.now().toString(),
                    _preferences.getString("username"),
                    null,
                    null,
                    null,
                    this._listCreditSubsidy[i].claimValue.isNotEmpty ? double.parse(this._listCreditSubsidy[i].claimValue.replaceAll(",", "")) : 0, //claim val
                    null,
                    this._listCreditSubsidy[i].giverDakor ? "1" : "0",
                    this._listCreditSubsidy[i].typeDakor ? "1" : "0",
                    this._listCreditSubsidy[i].cuttingMethodDakor ? "1" : "0",
                    this._listCreditSubsidy[i].cuttingValueDakor ? "1" : "0",
                ))
                    :
                _listApplRefund.add(MS2ApplRefundModel(
                    "123",
                    this._listCreditSubsidy[i].orderSubsidyID != null ? this._listCreditSubsidy[i].orderSubsidyID : "NEW",
                    this._listCreditSubsidy[i].giver.toString(),
                    this._listCreditSubsidy[i].type != null ? this._listCreditSubsidy[i].type.id : "",
                    this._listCreditSubsidy[i].type != null ? this._listCreditSubsidy[i].type.text : "",
                    this._listCreditSubsidy[i].cuttingMethod != null ? this._listCreditSubsidy[i].cuttingMethod.id : "",
                    this._listCreditSubsidy[i].cuttingMethod != null ? this._listCreditSubsidy[i].cuttingMethod.text : "",
                    double.parse(this._listCreditSubsidy[i].value.replaceAll(",", "")), // refund_amt
                    this._listCreditSubsidy[i].rateBeforeEff != "" ? double.parse(this._listCreditSubsidy[i].rateBeforeEff.replaceAll(",", "")) : null, //bunga
                    this._listCreditSubsidy[i].rateBeforeFlat != "" ? double.parse(this._listCreditSubsidy[i].rateBeforeFlat.replaceAll(",", "")) : null, //bunga
                    null,
                    null,
                    1,
                    DateTime.now().toString(),
                    _preferences.getString("username"),
                    null,
                    null,
                    null,
                    this._listCreditSubsidy[i].claimValue.isNotEmpty ? double.parse(this._listCreditSubsidy[i].claimValue.replaceAll(",", "")) : 0, //claim val
                    this._listCreditSubsidy[i].installmentDetail != null ? this._listCreditSubsidy[i].installmentDetail : [],
                    this._listCreditSubsidy[i].giverDakor ? "1" : "0",
                    this._listCreditSubsidy[i].typeDakor ? "1" : "0",
                    this._listCreditSubsidy[i].cuttingMethodDakor ? "1" : "0",
                    this._listCreditSubsidy[i].cuttingValueDakor ? "1" : "0",
                ));
            }
        }
        _dbHelper.insertMS2ApplRefund(_listApplRefund);
    }

    Future<bool> deleteSQLIte() async{
        return await _dbHelper.deleteMS2ApplRefundHeader();
    }

    Future<void> setDataFromSQLite(BuildContext context, int index) async{
        SharedPreferences _preferences = await SharedPreferences.getInstance();
        var _check = await _dbHelper.selectMS2ApplRefund();
        if(_check.isNotEmpty){
            for(int i=0;i<_check.length; i++){
                SubsidyTypeModel _type = SubsidyTypeModel(_check[i]['type_refund'], _check[i]['type_refund_desc']);
                CuttingMethodModel _cutting = CuttingMethodModel(_check[i]['deduction_method'], _check[i]['deduction_method_desc']);
                debugPrint("CEK ID HEADER ${_check[i]['subsidy_id']}");
                var _detail = await _dbHelper.selectMS2ApplRefundDetail(_check[i]['subsidy_id'].toString());
                print("ini detail = $_detail");
                if(_detail.isNotEmpty){
                    List<AddInstallmentDetailModel> _installmentDetail = [];
                    for(int j=0; j<_detail.length; j++){
                        _installmentDetail.add(AddInstallmentDetailModel(_detail[j]['ac_installment_no'].toString(), _detail[j]['ac_installment_amt'].toString(),_detail[j]['orderSubsidyDetailID']));
                    }
                    print('CEK_EFF_RATE ${ _check[i]['eff_rate_bef'].toString()}');
                    print('CEK_FLAT_RATE ${ _check[i]['flat_rate_bef'].toString()}');
                    _listCreditSubsidy.add(CreditSubsidyModel(
                        _check[i]['giver_refund'],
                        _check[i]['orderSubsidyID'],
                        _type,
                        _check[i]['deduction_method'] != "" ? _cutting : null,
                        _check[i]['refund_amt'].toString(),
                        _check[i]['refund_amt_klaim'].toString(),
                        _check[i]['eff_rate_bef'].toString(),//bunga
                        _check[i]['flat_rate_bef'].toString(),// bunga
                        null,
                        null,
                        _installmentDetail,
                        _check[i]['edit_giver_refund'] == "1" || _check[i]['edit_type_refund'] == "1" || _check[i]['edit_deduction_method'] == "1" || _check[i]['edit_refund_amt'] == "1",
                        _check[i]['edit_giver_refund'] == "1",
                        _check[i]['edit_type_refund'] == "1",
                        _check[i]['edit_deduction_method'] == "1",
                        _check[i]['edit_refund_amt'] == "1",
                        _check[i]['edit_refund_amt_klaim'] == "1",
                        _check[i]['edit_eff_rate_bef'] == "1",
                        _check[i]['edit_flat_rate_bef'] == "1",
                        _check[i]['edit_installment_amt'] == "1",
                    ));
                }
                else {
                    _listCreditSubsidy.add(
                        CreditSubsidyModel(
                            _check[i]['giver_refund'],
                            _check[i]['orderSubsidyID'],
                            _type,
                            _check[i]['deduction_method'] != "" ? _cutting : null,
                            _check[i]['refund_amt'].toString(),
                            _check[i]['refund_amt_klaim'].toString(),
                            _check[i]['eff_rate_bef'].toString(),//bunga
                            _check[i]['flat_rate_bef'].toString(),// bunga
                            null,
                            null,
                            [],
                            _check[i]['edit_giver_refund'] == "1" || _check[i]['edit_type_refund'] == "1" || _check[i]['edit_deduction_method'] == "1" || _check[i]['edit_refund_amt'] == "1",
                            _check[i]['edit_giver_refund'] == "1",
                            _check[i]['edit_type_refund'] == "1",
                            _check[i]['edit_deduction_method'] == "1",
                            _check[i]['edit_refund_amt'] == "1",
                            _check[i]['edit_refund_amt_klaim'] == "1",
                            _check[i]['edit_eff_rate_bef'] == "1",
                            _check[i]['edit_flat_rate_bef'] == "1",
                            _check[i]['edit_installment_amt'] == "1",
                        ));
                }
            }
        }
        var _providerDocument = Provider.of<InfoDocumentChangeNotifier>(context,listen: false);
        if(_preferences.getString("last_known_state") == "IDE" && index != null){
            if(index != 9){
                if(_preferences.getString("cust_type") == "PER"){
                    Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).isCreditSubsidyDone = true;
                    Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).selectedIndex += 1;
                } else {
                    Provider.of<FormMParentCompanyChangeNotifier>(context,listen: false).isCreditSubsidyDone = true;
                    Provider.of<FormMParentCompanyChangeNotifier>(context,listen: false).selectedIndex += 1;
                }
                await _providerDocument.setDataFromSQLite(context, index);
            }
            else{
                if(_preferences.getString("cust_type") == "PER"){
                    Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).selectedIndex = 9;
                } else {
                    Provider.of<FormMParentCompanyChangeNotifier>(context,listen: false).selectedIndex = 9;
                }
            }
        }
        notifyListeners();
    }

    bool get isDisablePACIAAOSCONA => _isDisablePACIAAOSCONA;

    set isDisablePACIAAOSCONA(bool value) {
        this._isDisablePACIAAOSCONA = value;
        notifyListeners();
    }

    String get lastKnownState => _lastKnownState;

    set lastKnownState(String value) {
        this._lastKnownState = value;
        notifyListeners();
    }

    String get custType => _custType;

    set custType(String value) {
        this._custType = value;
        notifyListeners();
    }

    Future<void> getSukuBungaSebelumEffFlat(BuildContext context) async{
        var _providerStructureCredit = Provider.of<InfoCreditStructureChangeNotifier>(context,listen: false);
        String _sukuBungaSebelumEffFlat = await storage.read(key: "SukuBungaSebelumEffFlat");

        for(int i=0; i < this._listCreditSubsidy.length; i++){
            double _interestDisc = 0;
            if(this._listCreditSubsidy[i].cuttingMethod != null){
                if(this._listCreditSubsidy[i].cuttingMethod.id == "01"){
                    _interestDisc = this._listCreditSubsidy[i].value != "" ? double.parse(this._listCreditSubsidy[i].value.replaceAll(",", "")) : 0;
                }
                else if(this._listCreditSubsidy[i].cuttingMethod.id == "02"){
                    _interestDisc = this._listCreditSubsidy[i].claimValue != "" ? double.parse(this._listCreditSubsidy[i].claimValue.replaceAll(",", "")) : 0;
                }
                else{
                    _interestDisc += this._listCreditSubsidy[i].value != "" ? double.parse(this._listCreditSubsidy[i].value.replaceAll(",", "")) : 0;
                    _interestDisc += this._listCreditSubsidy[i].claimValue != "" ? double.parse(this._listCreditSubsidy[i].claimValue.replaceAll(",", "")) : 0;
                }
            }

            var _body = jsonEncode({
                "DECAPPL_INTR": _providerStructureCredit.interestAmount,
                "DECAPPL_INTR_DISC": _interestDisc,
                "DECAPPL_INTR_RULE": _providerStructureCredit.paymentMethodSelected.id,
                "DECAPPL_INT_TYPE": _providerStructureCredit.installmentTypeSelected.id,
                "DECPERIOD": _providerStructureCredit.installmentTypeSelected.id == "04" || _providerStructureCredit.installmentTypeSelected.id == "05" ? 1 : 0,
                "DECTOP": int.parse(_providerStructureCredit.periodOfTimeSelected),
                "DECTOTALPRINCIPAL": double.parse(_providerStructureCredit.controllerTotalLoan.text.replaceAll(",", "")).toInt()
            });
            debugPrint(_body);

            try{
                final _response = await http.post(
                    "${BaseUrl.urlGeneral}$_sukuBungaSebelumEffFlat",
                    body: _body,
                    headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
                );
                if(_response.statusCode == 200){
                    debugPrint("CEK_SUBSIDY ${_response.body}");
                    var _bodyData = jsonDecode(_response.body);
                    List _data = _bodyData['data'];
                    if(_data.isNotEmpty){
                        updateListInfoCreditSubsidy(
                            CreditSubsidyModel(
                                this._listCreditSubsidy[i].giver,
                                this._listCreditSubsidy[i].orderSubsidyID,//this._orderSubsidyID,
                                this._listCreditSubsidy[i].type,
                                this._listCreditSubsidy[i].cuttingMethod,
                                this._listCreditSubsidy[i].value,
                                this._listCreditSubsidy[i].claimValue,
                                _data[0]['DECEFFRATE'].toString(),
                                _data[0]['DECFLATRATE'].toString(),
                                this._listCreditSubsidy[i].totalInstallment,
                                this._listCreditSubsidy[i].installmentIndex,//installmentIndexSelected,
                                this._listCreditSubsidy[i].installmentDetail,//listDetail,
                                this._listCreditSubsidy[i].isEditDakor,
                                this._listCreditSubsidy[i].giverDakor,
                                this._listCreditSubsidy[i].typeDakor,
                                this._listCreditSubsidy[i].cuttingMethodDakor,
                                this._listCreditSubsidy[i].cuttingValueDakor,
                                this._listCreditSubsidy[i].claimValueDakor,
                                this._listCreditSubsidy[i].rateBeforeEffDakor,
                                this._listCreditSubsidy[i].rateBeforeFlatDakor,
                                this._listCreditSubsidy[i].totalInstallmentDakor,
                            ),
                            context,
                            i
                        );
                    }
                    else{
                        throw "Gagal get Suku Bunga Sebelum Eff Flat";
                    }
                }
                else{
                    throw "Error get Suku Bunga Sebelum Eff Flat ${_response.statusCode}";
                }
            }
            catch(e){
                throw "Error get Suku Bunga Sebelum Eff Flat ${e.toString()}";
            }
        }
    }
}