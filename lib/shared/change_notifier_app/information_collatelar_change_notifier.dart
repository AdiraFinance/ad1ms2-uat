import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/models/birth_place_model.dart';
import 'package:ad1ms2_dev/models/brand_object_model.dart';
import 'package:ad1ms2_dev/models/brand_type_model_genre_model.dart';
import 'package:ad1ms2_dev/models/certificate_type_model.dart';
import 'package:ad1ms2_dev/models/collateral_type_model.dart';
import 'package:ad1ms2_dev/models/collateral_usage_model.dart';
import 'package:ad1ms2_dev/models/floor_type_model.dart';
import 'package:ad1ms2_dev/models/form_m_informasi_alamat_model.dart';
import 'package:ad1ms2_dev/models/form_m_informasi_nasabah_model.dart';
import 'package:ad1ms2_dev/models/foundation_type_model.dart';
import 'package:ad1ms2_dev/models/group_object_model.dart';
import 'package:ad1ms2_dev/models/lookup_utj_model.dart';
import 'package:ad1ms2_dev/models/model_object_model.dart';
import 'package:ad1ms2_dev/models/ms2_appl_objt_coll_oto_model.dart';
import 'package:ad1ms2_dev/models/ms2_appl_objt_coll_prop_model.dart';
import 'package:ad1ms2_dev/models/ms2_cust_addr_model.dart';
import 'package:ad1ms2_dev/models/object_model.dart';
import 'package:ad1ms2_dev/models/object_type_model.dart';
import 'package:ad1ms2_dev/models/object_usage_model.dart';
import 'package:ad1ms2_dev/models/product_type_model.dart';
import 'package:ad1ms2_dev/models/property_type_model.dart';
import 'package:ad1ms2_dev/models/show_mandatory_info_colla_oto_model.dart';
import 'package:ad1ms2_dev/models/show_mandatory_info_colla_properti_model.dart';
import 'package:ad1ms2_dev/models/street_type_model.dart';
import 'package:ad1ms2_dev/models/type_of_roof_model.dart';
import 'package:ad1ms2_dev/models/wall_type_model.dart';
import 'package:ad1ms2_dev/screens/form_m/search_kelurahan.dart';
import 'package:ad1ms2_dev/screens/lookup_utj.dart';
import 'package:ad1ms2_dev/screens/map_page.dart';
import 'package:ad1ms2_dev/screens/search_birth_place.dart';
import 'package:ad1ms2_dev/screens/search_brand_object.dart';
import 'package:ad1ms2_dev/screens/search_colla_merk_jenis_model.dart';
import 'package:ad1ms2_dev/screens/search_collateral_type.dart';
import 'package:ad1ms2_dev/screens/search_collateral_usage.dart';
import 'package:ad1ms2_dev/screens/search_group_object.dart';
import 'package:ad1ms2_dev/screens/search_model_object.dart';
import 'package:ad1ms2_dev/screens/search_object.dart';
import 'package:ad1ms2_dev/screens/search_object_type.dart';
import 'package:ad1ms2_dev/screens/search_product_type.dart';
import 'package:ad1ms2_dev/screens/search_usage_object.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_application_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_object_unit_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/search_collateral_type_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/taksasi_unit_change_notifier.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/dashboard/dashboard_change_notif.dart';
import 'package:ad1ms2_dev/shared/date_picker.dart';
import 'package:ad1ms2_dev/shared/form_m_company_rincian_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_foto_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_info_nasabah_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_informasi_alamat_change_notif.dart';
import 'package:ad1ms2_dev/shared/format_currency.dart';
import 'package:ad1ms2_dev/shared/regex_input_formatter.dart';
import 'package:ad1ms2_dev/shared/resource/address_type_resource.dart';
import 'package:ad1ms2_dev/shared/resource/get_product_matrix.dart';
import 'package:ad1ms2_dev/shared/resource/submit_data_partial.dart';
import 'package:ad1ms2_dev/shared/search_birth_place_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_brand_object_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_collateral_usage_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_group_object_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_model_object_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_object_type_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_object_usage_change_notifier.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/io_client.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../main.dart';
import '../form_m_company_manajemen_pic_change_notif.dart';
import '../form_m_lookup_utj_change_notif.dart';
import '../search_colla_merk_jenis_model_change_notifier.dart';
import '../search_kelurahan_change_notif.dart';
import '../search_object_change_notifier.dart';
import '../search_product_type_change_notifier.dart';
import 'information_salesman_change_notifier.dart';

class InformationCollateralChangeNotifier with ChangeNotifier{
  CollateralTypeModel _collateralTypeModel;
  TextEditingController _controllerCollateralType = TextEditingController();
  FormatCurrency _formatCurrency = FormatCurrency();
  RegExInputFormatter _amountValidator = RegExInputFormatter.withRegex('^[0-9]{0,13}(\\.[0-9]{0,2})?\$');
  String _prodMatrixId;
  DbHelper _dbHelper = DbHelper();
  bool _loadData = false;
  String _custType;
  String _lastKnownState;
  LookupUTJModel _lookupUTJModel;
  SubmitDataPartial _submitDataPartial = SubmitDataPartial();
  String _collateralID = "";
  bool _disableJenisPenawaran = false;
  bool _isDisablePACIAAOSCONA = false;

  bool get disableJenisPenawaran => _disableJenisPenawaran;

  set disableJenisPenawaran(bool value) {
    this._disableJenisPenawaran = value;
    notifyListeners();
  }

  String get custType => _custType;

  set custType(String value) {
    this._custType = value;
    notifyListeners();
  }

  String get lastKnownState => _lastKnownState;

  set lastKnownState(String value) {
    this._lastKnownState = value;
    notifyListeners();
  }
  var storage = FlutterSecureStorage();

  // Jenis Atap
  TypeOfRoofModel _typeOfRoofModelSelected;

  TypeOfRoofModel get typeOfRoodModelSelected => _typeOfRoofModelSelected;

  set typeOfRoodModelSelected(TypeOfRoofModel value) {
    this._typeOfRoofModelSelected = value;
    notifyListeners();
  }

  // Jenis Dinding
  WallTypeModel _wallTypeModelSelected;

  WallTypeModel get wallTypeModelSelected => _wallTypeModelSelected;

  set wallTypeModelSelected(WallTypeModel value) {
    this._wallTypeModelSelected = value;
    notifyListeners();
  }

  // Jenis Lantai
  FloorTypeModel _floorTypeModelSelected;

  FloorTypeModel get floorTypeModelSelected => _floorTypeModelSelected;

  set floorTypeModelSelected(FloorTypeModel value) {
    this._floorTypeModelSelected = value;
    notifyListeners();
  }

  // Jenis Fondasi
  FoundationTypeModel _foundationTypeModelSelected;

  FoundationTypeModel get foundationTypeModelSelected => _foundationTypeModelSelected;

  set foundationTypeModelSelected(FoundationTypeModel value) {
    this._foundationTypeModelSelected = value;
    notifyListeners();
  }

  // Tipe Jalan
  StreetTypeModel _streetTypeModelSelected;

  StreetTypeModel get streetTypeModelSelected => _streetTypeModelSelected;

  set streetTypeModelSelected(StreetTypeModel value) {
    this._streetTypeModelSelected = value;
    notifyListeners();
  }

  //automotive
  TextEditingController _controllerGroupObject = TextEditingController();
  TextEditingController _controllerObject = TextEditingController();
  TextEditingController _controllerTypeProduct = TextEditingController();
  TextEditingController _controllerBrandObject = TextEditingController();
  TextEditingController _controllerObjectType = TextEditingController();
  TextEditingController _controllerModelObject = TextEditingController();
  TextEditingController _controllerUsageObjectModel = TextEditingController();
  TextEditingController _controllerUsageCollateralOto = TextEditingController();
  TextEditingController _controllerPoliceNumber = TextEditingController();
  TextEditingController _controllerFrameNumber = TextEditingController();
  TextEditingController _controllerMachineNumber = TextEditingController();
  TextEditingController _controllerBPKPNumber = TextEditingController();
  // TextEditingController _controllerDpGuarantee = TextEditingController();

  TextEditingController _controllerGradeUnit = TextEditingController();
  TextEditingController _controllerFasilitasUTJ = TextEditingController();
  TextEditingController _controllerNamaBidder = TextEditingController();
  TextEditingController _controllerHargaJualShowroom = TextEditingController();
  TextEditingController _controllerPerlengkapanTambahan = TextEditingController();
  TextEditingController _controllerMPAdira = TextEditingController();
  TextEditingController _controllerMPAdiraUpload = TextEditingController();
  TextEditingController _controllerHargaPasar = TextEditingController();
  TextEditingController _controllerRekondisiFisik = TextEditingController();
  TextEditingController _controllerPHMaxAutomotive = TextEditingController();
  TextEditingController _controllerTaksasiPriceAutomotive = TextEditingController();
  TextEditingController _controllerIdentityNumberAuto = TextEditingController();
  TextEditingController _controllerNameOnCollateralAuto = TextEditingController();
  TextEditingController _controllerBirthDateAuto = TextEditingController();
  TextEditingController _controllerBirthPlaceValidWithIdentityAuto = TextEditingController();
  TextEditingController _controllerBirthPlaceValidWithIdentityLOVAuto = TextEditingController();

  TextEditingController get controllerGradeUnit => _controllerGradeUnit;
  TextEditingController get controllerFasilitasUTJ => _controllerFasilitasUTJ;
  TextEditingController get controllerNamaBidder => _controllerNamaBidder;
  TextEditingController get controllerHargaJualShowroom => _controllerHargaJualShowroom;
  TextEditingController get controllerPerlengkapanTambahan => _controllerPerlengkapanTambahan;
  TextEditingController get controllerMPAdira => _controllerMPAdira;
  TextEditingController get controllerMPAdiraUpload => _controllerMPAdiraUpload;
  TextEditingController get controllerHargaPasar => _controllerHargaPasar;
  TextEditingController get controllerRekondisiFisik => _controllerRekondisiFisik;
  TextEditingController get controllerPHMaxAutomotive => _controllerPHMaxAutomotive;
  TextEditingController get controllerTaksasiPriceAutomotive => _controllerTaksasiPriceAutomotive;
  TextEditingController get controllerIdentityNumberAuto => _controllerIdentityNumberAuto;
  TextEditingController get controllerNameOnCollateralAuto => _controllerNameOnCollateralAuto;
  TextEditingController get controllerBirthDateAuto => _controllerBirthDateAuto;
  TextEditingController get controllerBirthPlaceValidWithIdentityAuto => _controllerBirthPlaceValidWithIdentityAuto;
  TextEditingController get controllerBirthPlaceValidWithIdentityLOVAuto => _controllerBirthPlaceValidWithIdentityLOVAuto;
  TextEditingController get controllerPoliceNumber => _controllerPoliceNumber;
  TextEditingController get controllerFrameNumber => _controllerFrameNumber;
  TextEditingController get controllerMachineNumber => _controllerMachineNumber;
  TextEditingController get controllerBPKPNumber => _controllerBPKPNumber;
  // TextEditingController get controllerDpGuarantee => _controllerDpGuarantee; // per tanggal 23.07.2021 tidak dipakai karena dijadikan 1 controller dengan collateralDp

  //property
  TextEditingController _controllerIdentityType = TextEditingController();
  TextEditingController _controllerIdentityNumber = TextEditingController();
  TextEditingController _controllerNameOnCollateral = TextEditingController();
  TextEditingController _controllerBirthPlaceValidWithIdentity1 = TextEditingController();
  TextEditingController _controllerBirthPlaceValidWithIdentityLOV = TextEditingController();
  TextEditingController _controllerCertificateNumber = TextEditingController();
  TextEditingController _controllerBuildingArea = TextEditingController();
  TextEditingController _controllerSurfaceArea = TextEditingController();

  TextEditingController _controllerTaksasiPrice = TextEditingController();
  TextEditingController _controllerSifatJaminan = TextEditingController();
  TextEditingController _controllerBuktiKepemilikan = TextEditingController();
  TextEditingController _controllerNamaPemegangHak = TextEditingController();
  TextEditingController _controllerNoSuratUkur = TextEditingController();
  TextEditingController _controllerDPJaminan = TextEditingController();
  TextEditingController _controllerPHMax = TextEditingController();
  TextEditingController _controllerJarakFasumPositif = TextEditingController();
  TextEditingController _controllerJarakFasumNegatif = TextEditingController();
  TextEditingController _controllerHargaTanah = TextEditingController();
  TextEditingController _controllerHargaNJOP = TextEditingController();
  TextEditingController _controllerHargaBangunan = TextEditingController();
  TextEditingController _controllerJumlahRumahDalamRadius = TextEditingController();
  TextEditingController _controllerMasaHakBerlaku = TextEditingController();
  TextEditingController _controllerNoIMB = TextEditingController();
  TextEditingController _controllerLuasBangunanIMB = TextEditingController();
  TextEditingController _controllerLTV = TextEditingController();
  TextEditingController _controllerBirthDateProp = TextEditingController();

  TextEditingController get controllerTaksasiPrice => _controllerTaksasiPrice;
  TextEditingController get controllerSifatJaminan => _controllerSifatJaminan;
  TextEditingController get controllerBuktiKepemilikan =>_controllerBuktiKepemilikan;
  TextEditingController get controllerNamaPemegangHak =>_controllerNamaPemegangHak;
  TextEditingController get controllerNoSuratUkur => _controllerNoSuratUkur;
  TextEditingController get controllerDPJaminan => _controllerDPJaminan;
  TextEditingController get controllerPHMax => _controllerPHMax;
  TextEditingController get controllerJarakFasumPositif =>_controllerJarakFasumPositif;
  TextEditingController get controllerJarakFasumNegatif =>_controllerJarakFasumNegatif;
  TextEditingController get controllerHargaTanah => _controllerHargaTanah;
  TextEditingController get controllerHargaNJOP => _controllerHargaNJOP;
  TextEditingController get controllerHargaBangunan => _controllerHargaBangunan;
  TextEditingController get controllerJumlahRumahDalamRadius =>_controllerJumlahRumahDalamRadius;
  TextEditingController get controllerMasaHakBerlaku =>_controllerMasaHakBerlaku;
  TextEditingController get controllerNoIMB =>_controllerNoIMB;
  TextEditingController get controllerLuasBangunanIMB =>_controllerLuasBangunanIMB;
  TextEditingController get controllerLTV =>_controllerLTV;
  TextEditingController get controllerBirthDateProp => _controllerBirthDateProp;

  TextEditingController _controllerCertificateReleaseDate = TextEditingController();
  TextEditingController _controllerCertificateReleaseYear = TextEditingController();
  TextEditingController _controllerDateOfMeasuringLetter = TextEditingController();
  TextEditingController _controllerCertificateOfMeasuringLetter = TextEditingController();
  TextEditingController _controllerDateOfIMB = TextEditingController();
  TextEditingController _controllerUsageCollateralProperty = TextEditingController();
  TextEditingController _controllerAddress = TextEditingController();
  TextEditingController _controllerRT = TextEditingController();
  TextEditingController _controllerRW = TextEditingController();
  TextEditingController _controllerKelurahan = TextEditingController();
  TextEditingController _controllerKecamatan = TextEditingController();
  TextEditingController _controllerKota = TextEditingController();
  TextEditingController _controllerProv = TextEditingController();
  TextEditingController _controllerPostalCode = TextEditingController();
  TextEditingController _controllerAddressFromMap = TextEditingController();

  //automotive
  int _radioValueIsCollaNameSameWithApplicantOto = 1;
  int _radioValueIsCollateralSameWithUnitOto = 1;
  int _radioValueYellowPlat = 0;
  int _radioValueBuiltUpNonATPM = 0;
  int _radioValueWorthyOrUnworthy = 0;
  int _radioValueForAllUnitOto = 1;
  IdentityModel _identityTypeSelectedAuto;
  GroupObjectModel _groupObjectSelected;
  ObjectModel _objectSelected;
  ProductTypeModel _productTypeSelected;
  BrandObjectModel _brandObjectSelected;
  ObjectTypeModel _objectTypeSelected;
  ModelObjectModel _modelObjectSelected;
  ObjectUsageModel _objectUsageSelected;
  CollateralUsageModel _collateralUsageOtoSelected;
  String _yearProductionSelected;
  String _yearRegistrationSelected;

  //property
  int _radioValueIsCollateralSameWithApplicantProperty = 1;
  int _radioValueAccessCar = 0;
  int _radioValueForAllUnitProperty = 0;
  CertificateTypeModel _certificateTypeSelected;
  PropertyTypeModel _propertyTypeSelected;
  DateTime _initialDateCertificateRelease = DateTime(dateNow.year,dateNow.month,dateNow.day);
  DateTime _initialDateOfMeasuringLetter = DateTime(dateNow.year,dateNow.month,dateNow.day);
  DateTime _initialDateOfIMB = DateTime(dateNow.year,dateNow.month,dateNow.day);
  TypeOfRoofModel _typeOfRoofSelected;
  WallTypeModel _wallTypeSelected;
  FoundationTypeModel _foundationTypeSelected;
  FloorTypeModel _floorTypeSelected;
  StreetTypeModel _streetTypeSelected;
  CollateralUsageModel _collateralUsagePropertySelected;
  JenisAlamatModel _addressTypeSelected;
  bool _enableTf = true;
  KelurahanModel _kelurahanModel;
  String _addressID = "NEW";
  String _foreignBusinessID = "NEW";

  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  GlobalKey<FormState> _keyAuto = GlobalKey<FormState>();
  GlobalKey<FormState> _keyProp = GlobalKey<FormState>();
  bool _autoValidateAuto = false;
  bool _autoValidateProp = false;

  bool _flag = false;
  BirthPlaceModel _birthPlaceSelected;
  BirthPlaceModel _birthPlaceAutoSelected;
  Map _addressFromMap;

  List<IdentityModel> _listIdentity = IdentityType().lisIdentityModel;
  IdentityModel _identityModel;

  // Load Data
  bool get loadData => _loadData;

  set loadData(bool value) {
    this._loadData = value;
  }

  List<String> _listProductionYear = [];
  List<String> _listRegistrationYear = [];

  List<CertificateTypeModel> _listCertificateType = [
    CertificateTypeModel("001", "SHM"),
    CertificateTypeModel("002", "SHGB"),
    CertificateTypeModel("003", "HAK PAKAI"),
    CertificateTypeModel("004", "HAK GUNA USAHA"),
  ];

  List<PropertyTypeModel> _listPropertyType = [
    PropertyTypeModel("R63", "RUMAH"),
    PropertyTypeModel("434", "RUKAN"),
    PropertyTypeModel("SX9", "RUKO"),
    PropertyTypeModel("2AR", "APARTEMEN"),
    PropertyTypeModel("2VP", "RUSUN"),
  ];

  List<TypeOfRoofModel> _listTypeOfRoof = [
    // TypeOfRoofModel("001", "JENIS ATAP 1"),
    // TypeOfRoofModel("002", "JENIS ATAP 2"),
    // TypeOfRoofModel("003", "JENIS ATAP 3")
  ];

  List<WallTypeModel> _listWallType = [
    // WallTypeModel("001", "JENIS DINDING A"),
    // WallTypeModel("002", "JENIS DINDING B"),
    // WallTypeModel("003", "JENIS DINDING C")
  ];

  List<FoundationTypeModel> _listFoundationType = [
    // FoundationTypeModel("001", "JENIS PONDASI 1"),
    // FoundationTypeModel("002", "JENIS PONDASI 2"),
    // FoundationTypeModel("003", "JENIS PONDASI 3")
  ];

  List<FloorTypeModel> _listFloorType = [
    // FloorTypeModel("001", "JENIS LANTAI A"),
    // FloorTypeModel("002", "JENIS LANTAI B"),
    // FloorTypeModel("003", "JENIS LANTAI C")
  ];

  List<StreetTypeModel> _listStreetType = [
    StreetTypeModel("001", "TIPE JALAN 1"),
    StreetTypeModel("002", "TIPE JALAN 2"),
    StreetTypeModel("003", "TIPE JALAN 3")
  ];

  List<JenisAlamatModel> _listAddressType = [];
  List<AddressModel> _listAddress = [];

  // Kelurahan
  KelurahanModel get kelurahanSelected => _kelurahanModel;

  set kelurahanSelected(KelurahanModel value) {
    this._kelurahanModel = value;
    notifyListeners();
  }

  bool get autoValidateAuto => _autoValidateAuto;

  set autoValidateAuto(bool value) {
    this._autoValidateAuto = value;
    notifyListeners();
  }

  bool get autoValidateProp => _autoValidateProp;

  set autoValidateProp(bool value) {
    this._autoValidateProp = value;
    notifyListeners();
  }

  CollateralTypeModel get collateralTypeModel => _collateralTypeModel;

  set collateralTypeModel(CollateralTypeModel value) {
    this._collateralTypeModel = value;
  }

  int get radioValueIsCollateralSameWithUnitOto =>
      _radioValueIsCollateralSameWithUnitOto;

  set radioValueIsCollateralSameWithUnitOto(int value) {
    this._radioValueIsCollateralSameWithUnitOto = value;
    notifyListeners();
  }

  IdentityModel get identityTypeSelectedAuto => _identityTypeSelectedAuto;

  set identityTypeSelectedAuto(IdentityModel value) {
    this._identityTypeSelectedAuto = value;
    notifyListeners();
  }

  TextEditingController get controllerCollateralType => _controllerCollateralType;
  TextEditingController get controllerGroupObject => _controllerGroupObject;
  TextEditingController get controllerObject => _controllerObject;
  TextEditingController get controllerTypeProduct => _controllerTypeProduct;
  TextEditingController get controllerBrandObject => _controllerBrandObject;
  TextEditingController get controllerObjectType => _controllerObjectType;
  TextEditingController get controllerModelObject => _controllerModelObject;
  TextEditingController get controllerUsageObjectModel => _controllerUsageObjectModel;
  TextEditingController get controllerUsageCollateralOto => _controllerUsageCollateralOto;

  UnmodifiableListView<IdentityModel> get listIdentityType => UnmodifiableListView(_listIdentity);

  GroupObjectModel get groupObjectSelected => _groupObjectSelected;

  set groupObjectSelected(GroupObjectModel value) {
    this._groupObjectSelected = value;
  }

  ObjectModel get objectSelected => _objectSelected;

  set objectSelected(ObjectModel value) {
    _objectSelected = value;
    notifyListeners();
  }

  ProductTypeModel get productTypeSelected => _productTypeSelected;

  set productTypeSelected(ProductTypeModel value) {
    _productTypeSelected = value;
    notifyListeners();
  }

  BrandObjectModel get brandObjectSelected => _brandObjectSelected;

  set brandObjectSelected(BrandObjectModel value) {
    this._brandObjectSelected = value;
  }

  ObjectTypeModel get objectTypeSelected => _objectTypeSelected;

  set objectTypeSelected(ObjectTypeModel value) {
    this._objectTypeSelected = value;
  }

  ModelObjectModel get modelObjectSelected => _modelObjectSelected;

  set modelObjectSelected(ModelObjectModel value) {
    this._modelObjectSelected = value;
  }

  ObjectUsageModel get objectUsageSelected => _objectUsageSelected;

  set objectUsageSelected(ObjectUsageModel value) {
    this._objectUsageSelected = value;
  }

  String get yearProductionSelected => _yearProductionSelected;

  set yearProductionSelected(String value) {
    this._yearProductionSelected = value;
    notifyListeners();
  }

  String get yearRegistrationSelected => _yearRegistrationSelected;

  set yearRegistrationSelected(String value) {
    this._yearRegistrationSelected = value;
    notifyListeners();
  }

  int get radioValueYellowPlat => _radioValueYellowPlat;

  set radioValueYellowPlat(int value) {
    this._radioValueYellowPlat = value;
    notifyListeners();
  }

  int get radioValueBuiltUpNonATPM => _radioValueBuiltUpNonATPM;

  set radioValueBuiltUpNonATPM(int value) {
    this._radioValueBuiltUpNonATPM = value;
    notifyListeners();
  }

  int get radioValueWorthyOrUnworthy => _radioValueWorthyOrUnworthy;

  set radioValueWorthyOrUnworthy(int value) {
    this._radioValueWorthyOrUnworthy = value;
    notifyListeners();
  }

  CollateralUsageModel get collateralUsageOtoSelected => _collateralUsageOtoSelected;

  set collateralUsageOtoSelected(CollateralUsageModel value) {
    this._collateralUsageOtoSelected = value;
    notifyListeners();
  }

  int get radioValueForAllUnitOto => _radioValueForAllUnitOto;

  set radioValueForAllUnitOto(int value) {
    this._radioValueForAllUnitOto = value;
    notifyListeners();
  }

  bool get enableTf => _enableTf;

  set enableTf(bool value) {
    this._enableTf = value;
    notifyListeners();
  }

  int get radioValueIsCollaNameSameWithApplicantOto => _radioValueIsCollaNameSameWithApplicantOto;

  set radioValueIsCollaNameSameWithApplicantOto(int value) {
    this._radioValueIsCollaNameSameWithApplicantOto = value;
    notifyListeners();
  }

  String get addressID => _addressID;

  set addressID(String value) {
    this._addressID = value;
  }

  String get foreignBusinessID => _foreignBusinessID;

  set foreignBusinessID(String value) {
    this._foreignBusinessID = value;
  }

  UnmodifiableListView<String> get listProductionYear => UnmodifiableListView(_listProductionYear);

  UnmodifiableListView<String> get listRegistrationYear => UnmodifiableListView(_listRegistrationYear);

  void getMPAdiraUpload(BuildContext context) async {
    String _mpAdiraUpload = await storage.read(key: "MPAdiraUpload");
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    try{
      final ioc = new HttpClient();
      ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
      final _http = IOClient(ioc);

      var _body = jsonEncode({
          "cpSentraId": _preferences.getString("SentraD"),
          "objectTypeId": this._objectTypeSelected.id,
          "objectBrandId": this._brandObjectSelected.id,
          "objectGroupId": this._groupObjectSelected.KODE,
          "objectModelId": this._modelObjectSelected.id,
          "mfgYear": this._yearProductionSelected,
          "applDate": Provider.of<InfoAppChangeNotifier>(context,listen: false).controllerOrderDate.text
      });
      print("body mp adira upload = $_body");
      final _response = await _http.post(
          "${BaseUrl.urlGeneral}$_mpAdiraUpload",
          body: _body,
          headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
      );
      final _data = jsonDecode(_response.body);
      print("data mp adira upload =  $_data");
      if(_response.statusCode == 302 || _response.statusCode == 200){
        this._controllerMPAdiraUpload.text = formatCurrency.formatCurrency(_data['mpAdiraUpload'].toString());
      } else {
        this._controllerMPAdiraUpload.text = "0.00";
      }
    } catch (e) {
      print(e.toString());
    }
    notifyListeners();
  }

  void getTypeOfRoof(BuildContext context,List data) async {
    if(this._collateralTypeModel.id != "003"){
      debugPrint("masuk colla prop");
      _listTypeOfRoof.clear();
      this._typeOfRoofSelected = null;
      loadData = true;
      String _jenisAtap = await storage.read(key: "JenisAtap");

      try{
        final ioc = new HttpClient();
        ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
        final _http = IOClient(ioc);
        final _response = await _http.get(
          "${BaseUrl.urlGeneral}$_jenisAtap",
          headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
            // "${urlPublic}api/colaproperties/get-jenis-atap"
        );
        final _data = jsonDecode(_response.body);
        print("cek roof $_data");

        for(int i=0; i < _data['data'].length;i++){
          _listTypeOfRoof.add(TypeOfRoofModel(_data['data'][i]['kode'], _data['data'][i]['deskripsi']));
        }
        this._typeOfRoofModelSelected = this._listTypeOfRoof[0];
        loadData = false;
        getWallType(context,data);
      } catch (e) {
        loadData = false;
        debugPrint(e.toString());
      }
      notifyListeners();
    }
  }

  void getWallType(BuildContext context,List data) async {
    // this._wallTypeModelSelected = this._listWallType[0];
    _listWallType.clear();
    this._wallTypeSelected = null;
    loadData = true;
    String _jenisDinding = await storage.read(key: "JenisDinding");

    try{
      final ioc = new HttpClient();
      ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
      final _http = IOClient(ioc);
      final _response = await _http.get(
        "${BaseUrl.urlGeneral}$_jenisDinding",
        headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
          // "${urlPublic}api/colaproperties/get-jenis-dinding"
      );
      final _data = jsonDecode(_response.body);

      for(int i=0; i < _data['data'].length;i++){
        _listWallType.add(WallTypeModel(_data['data'][i]['kode'], _data['data'][i]['deskripsi']));
      }
      loadData = false;
      getFloorType(context,data);
    } catch (e) {
      loadData = false;
    }
    notifyListeners();
  }

  void getFloorType(BuildContext context,List data) async {
    // this._floorTypeModelSelected = this._listFloorType[0];
    _listFloorType.clear();
    this._floorTypeSelected = null;
    loadData = true;
    String _jenisLantai = await storage.read(key: "JenisLantai");

    try{
      final ioc = new HttpClient();
      ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
      final _http = IOClient(ioc);
      final _response = await _http.get(
        "${BaseUrl.urlGeneral}$_jenisLantai",
        headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
          // "${urlPublic}api/colaproperties/get-jenis-lantai"
      );
      final _data = jsonDecode(_response.body);

      for(int i=0; i < _data['data'].length;i++){
        _listFloorType.add(FloorTypeModel(_data['data'][i]['kode'], _data['data'][i]['deskripsi']));
      }
      loadData = false;
      getFoundationType(context,data);
    } catch (e) {
      loadData = false;
    }
    notifyListeners();
  }

  void getFoundationType(BuildContext context,List data) async {
    // this._foundationTypeModelSelected = this._listFoundationType[0];
    _listFoundationType.clear();
    this._foundationTypeSelected = null;
    loadData = true;
    String _fieldJenisFondasi = await storage.read(key: "FieldJenisFondasi");

    try{
      final ioc = new HttpClient();
      ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
      final _http = IOClient(ioc);
      final _response = await _http.get(
        "${BaseUrl.urlGeneral}$_fieldJenisFondasi",
        headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
          // "${urlPublic}api/colaproperties/get-jenis-fondasi"
      );
      final _data = jsonDecode(_response.body);

      for(int i=0; i < _data['data'].length;i++){
        _listFoundationType.add(FoundationTypeModel(_data['data'][i]['kode'], _data['data'][i]['deskripsi']));
      }
      loadData = false;
      getStreetType(context,data);
    } catch (e) {
      loadData = false;
    }
    notifyListeners();
  }

  void getStreetType(BuildContext context,List data) async {
    // this._streetTypeModelSelected = this._listStreetType[0];
    _listStreetType.clear();
    this._streetTypeSelected = null;
    loadData = true;
    String _fieldTipeJalan = await storage.read(key: "FieldTipeJalan");
    try{
      final ioc = new HttpClient();
      ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
      final _http = IOClient(ioc);
      final _response = await _http.get(
          "${BaseUrl.urlGeneral}$_fieldTipeJalan",
          headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
      );
      final _data = jsonDecode(_response.body);
      for(int i=0; i < _data['data'].length;i++){
        _listStreetType.add(StreetTypeModel(_data['data'][i]['kode'], _data['data'][i]['deskripsi'].trim()));
      }
      loadData = false;
      _setValueFromSQLiteProp(context,data);
    } catch (e) {
      loadData = false;
    }
    notifyListeners();
  }

  void searchCollateralType(BuildContext context) async{
    CollateralTypeModel _data = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => SearchCollateralTypeChangeNotifier(),
                child: SearchCollateralType()
            )
        )
    );
    if(_data != null){
      clearDataInfoCollateral();
      _listIdentity = IdentityType().lisIdentityModel;
      _collateralTypeModel = _data;
      print("cekkkkkkk  ${_data.id} - ${_data.name}");
      this._controllerCollateralType.text = "${_data.id} - ${_data.name}";
      if(_data.id == "001"){
        _addYearProductionAndRegistration();
      }
      else if(_data.id == "002"){
        getTypeOfRoof(context,null);
      }
      notifyListeners();
    }
  }

  void searchKelurahan(BuildContext context) async {
    KelurahanModel data = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => SearchKelurahanChangeNotif(),
                child: SearchKelurahan(
//                  flag: 0,
                ))));
    if (data != null) {
      kelurahanSelected = data;
      this._controllerKelurahan.text = data.KEL_NAME;
      this._controllerKecamatan.text = data.KEC_NAME;
      this._controllerKota.text = data.KABKOT_NAME;
      this._controllerProv.text = data.PROV_NAME;
      this._controllerPostalCode.text = data.ZIPCODE;
      notifyListeners();
    } else {
      return;
    }
  }

  void _addYearProductionAndRegistration(){
    this._listProductionYear.clear();
    this._listRegistrationYear.clear();
    int _yearNow = DateTime.now().year + 1;
    int _lastYear = _yearNow - 10;
    for(int i=_lastYear; i <= _yearNow; i++){
      this._listProductionYear.add(i.toString());
      this._listRegistrationYear.add(i.toString());
    }
    notifyListeners();
  }

  void setSameWithApplicantAutomotive(BuildContext context){
    var _provider = Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false);
    if(this._radioValueIsCollateralSameWithUnitOto == 0){
      if(_provider.objectSelected != null){
        if(_provider.objectSelected.id == "001" || _provider.objectSelected.id == "003"){
          if(this._controllerGradeUnit.text.isEmpty){
            this._controllerGradeUnit.text = "-";
          }
          if(this._controllerFasilitasUTJ.text.isEmpty){
            this._controllerFasilitasUTJ.text = "-";
          }
          if(this._controllerNamaBidder.text.isEmpty){
            this._controllerNamaBidder.text = "- / 0.00 / 0.00";
          }
        }
      }
      if(this._controllerHargaJualShowroom.text.isEmpty){
        this._controllerHargaJualShowroom.text = "0.00";
      }
      if(this._controllerMPAdiraUpload.text.isEmpty){
        this._controllerMPAdiraUpload.text = "0.00";
      }
      if(this._controllerPHMaxAutomotive.text.isEmpty){
        this._controllerPHMaxAutomotive.text = "0.00";
      }
      if(this._controllerTaksasiPriceAutomotive.text.isEmpty){
        this._controllerTaksasiPriceAutomotive.text = "0.00";
      }
      if(_provider.groupObjectSelected != null){
        groupObjectSelected = _provider.groupObjectSelected;
        this._controllerGroupObject.text = "${groupObjectSelected.KODE} - ${groupObjectSelected.DESKRIPSI}";
      }
      if(_provider.objectSelected != null){
        objectSelected = _provider.objectSelected;
        this._controllerObject.text = "${objectSelected.id} - ${objectSelected.name}";
      }
      if(_provider.productTypeSelected != null){
        productTypeSelected = _provider.productTypeSelected;
        this._controllerTypeProduct.text = "${productTypeSelected.id} - ${productTypeSelected.name}";
      }
      if(_provider.brandObjectSelected != null){
        brandObjectSelected = _provider.brandObjectSelected;
        this._controllerBrandObject.text = "${brandObjectSelected.id} - ${brandObjectSelected.name}";
      }
      if(_provider.modelObjectSelected != null){
        modelObjectSelected = _provider.modelObjectSelected;
        this._controllerModelObject.text = "${modelObjectSelected.id} - ${modelObjectSelected.name}";
      }
      if(_provider.objectUsageModel != null){
        objectUsageSelected = _provider.objectUsageModel; // field pemakaian objek
        this._controllerUsageObjectModel.text = "${objectUsageSelected.id} - ${objectUsageSelected.name}";
      }
      if(_provider.objectTypeSelected != null){
        objectTypeSelected = _provider.objectTypeSelected; // ke hide
        this._controllerObjectType.text = "${objectTypeSelected.id} - ${objectTypeSelected.name}"; // ke hide
      }
      if(_provider.objectPurposeSelected != null){
        collateralUsageOtoSelected = CollateralUsageModel(_provider.objectPurposeSelected.id, _provider.objectPurposeSelected.name); // field tujuan penggunaan colalteral
        this._controllerUsageCollateralOto.text = "${_collateralUsageOtoSelected.id} - ${_collateralUsageOtoSelected.name}";
      }
      notifyListeners();
    }
    else{
      groupObjectSelected = null;
      objectSelected = null;
      productTypeSelected = null;
      modelObjectSelected = null;
      brandObjectSelected = null;
      objectTypeSelected = null;
      objectUsageSelected = null;
      collateralUsageOtoSelected = null;
      this._controllerGroupObject.clear();
      this._controllerObject.clear();
      this._controllerTypeProduct.clear();
      this._controllerBrandObject.clear();
      this._controllerObjectType.clear();
      this._controllerModelObject.clear();
      this._controllerUsageObjectModel.clear();
      this._controllerUsageCollateralOto.clear();
      notifyListeners();
    }
  }

  void searchGroupObject(BuildContext context, String flag) async {
    GroupObjectModel data = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => SearchGroupObjectChangeNotifier(),
                child: SearchGroupObject(flag: flag, unitColla: "2")))); // Nanti unit Colla dirubah jadi 2 kalau API sudah naik
    if (data != null) {
      this._groupObjectSelected = data;
      this._controllerGroupObject.text = "${data.KODE} - ${data.DESKRIPSI}";
      notifyListeners();
    } else {
      return;
    }
  }

  void searchObject(BuildContext context) async {
    if(this._groupObjectSelected != null){
      ObjectModel data = await Navigator.push(context,
          MaterialPageRoute(
              builder: (context) => ChangeNotifierProvider(
                  create: (context) => SearchObjectChangeNotifier(),
                  child: SearchObject(kodeObject: this._groupObjectSelected.KODE, unitColla: "2"))));
      if (data != null) {
        this._objectSelected = data;
        this._controllerObject.text = "${data.id} - ${data.name}";
        notifyListeners();
      } else {
        return;
      }
    }
    else{
      showSnackBar("Grup Objek tidak boleh kosong");
    }
  }

  void searchProductType(BuildContext context,String flag) async {
    ProductTypeModel data = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => SearchProductTypeChangeNotifier(),
                child: SearchProductType(flag: flag, kodeGroupObject: this._groupObjectSelected.KODE, kodeObject: this._objectSelected.id))));
    if (data != null) {
      this._productTypeSelected = data;
      this._controllerTypeProduct.text = "${data.id} - ${data.name}";
      notifyListeners();
      var _getProdMatrixID = await getProdMatrixID(context, flag);
      if(_getProdMatrixID['status']){
        _prodMatrixId = _getProdMatrixID['value'];
      } else {
        showSnackBar(_getProdMatrixID['message']);
      }
      // _getProdMatrixID(context, flag);
    } else {
      return;
    }
  }

  void searchBrandObject(BuildContext context, String flag, int flagByBrandModelType) async { // ga kepake
    BrandTypeModelGenreModel data = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => SearchBrandObjectChangeNotifier(),
                child: SearchBrandObject(flag: flag, flagByBrandModelType: flagByBrandModelType, kodeGroupObject: this._groupObjectSelected.KODE, kodeObject: this._objectSelected.id, prodMatrix: this._prodMatrixId))));
    if (data != null) {
      this._brandObjectSelected = data.brandObjectModel;
      this._controllerBrandObject.text = "${_brandObjectSelected.id} - ${_brandObjectSelected.name}";
      this._objectTypeSelected = data.objectTypeModel;
      this._controllerObjectType.text = "${_objectTypeSelected.id} - ${_objectTypeSelected.name}";
      this._modelObjectSelected = data.modelObjectModel;
      this._controllerModelObject.text = "${_modelObjectSelected.id} - ${_modelObjectSelected.name}";
      this._objectUsageSelected = data.objectUsageModel;
      this._controllerUsageObjectModel.text = "${_objectUsageSelected.id} - ${_objectUsageSelected.name}";
      notifyListeners();
    } else {
      return;
    }
  }

  void searchObjectType(BuildContext context, String flag, int flagByBrandModelType) async { // ga kepake
    BrandTypeModelGenreModel data = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => SearchObjectTypeChangeNotifier(),
                child: SearchObjectType(flagByBrandModelType: flagByBrandModelType, flag: flag, kodeGroupObject: this._groupObjectSelected.KODE, kodeObject: this._objectSelected.id, prodMatrix: this._prodMatrixId))));
    if (data != null) {
      this._brandObjectSelected = data.brandObjectModel;
      this._controllerBrandObject.text = "${_brandObjectSelected.id} - ${_brandObjectSelected.name}";
      this._objectTypeSelected = data.objectTypeModel;
      this._controllerObjectType.text = "${_objectTypeSelected.id} - ${_objectTypeSelected.name}";
      this._modelObjectSelected = data.modelObjectModel;
      this._controllerModelObject.text = "${_modelObjectSelected.id} - ${_modelObjectSelected.name}";
      this._objectUsageSelected = data.objectUsageModel;
      this._controllerUsageObjectModel.text = "${_objectUsageSelected.id} - ${_objectUsageSelected.name}";
      notifyListeners();
    } else {
      return;
    }
  }

  void searchModelObject(BuildContext context, String flag, int flagByBrandModelType) async { // ga kepake
    BrandTypeModelGenreModel data = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => SearchModelObjectChangeNotifier(),
                child: SearchModelObject(flag: flag, flagByBrandModelType: flagByBrandModelType, kodeGroupObject: this._groupObjectSelected.KODE, kodeObject: this._objectSelected.id, prodMatrix: this._prodMatrixId))));
    if (data != null) {
      this._brandObjectSelected = data.brandObjectModel;
      this._controllerBrandObject.text = "${_brandObjectSelected.id} - ${_brandObjectSelected.name}";
      this._objectTypeSelected = data.objectTypeModel;
      this._controllerObjectType.text = "${_objectTypeSelected.id} - ${_objectTypeSelected.name}";
      this._modelObjectSelected = data.modelObjectModel;
      this._controllerModelObject.text = "${_modelObjectSelected.id} - ${_modelObjectSelected.name}";
      this._objectUsageSelected = data.objectUsageModel;
      this._controllerUsageObjectModel.text = "${_objectUsageSelected.id} - ${_objectUsageSelected.name}";
      notifyListeners();
    } else {
      return;
    }
  }

  void searchMerkJenisModelObject(BuildContext context, int type) async{
    // type 1 = merk, 2 = jenis, 3 = model
    if(this._groupObjectSelected != null){
      BrandTypeModelGenreModel data = await Navigator.push(
          context, MaterialPageRoute(
          builder: (context) => ChangeNotifierProvider(
              create: (context) => SearchCollaMerkJenisModelChangeNotifier(),
              child: SearchCollaMerkJenisModel(type: type, groupObject: this._groupObjectSelected.KODE)
          ))
      );
      if (data != null) {
        if(type == 1){
          this._brandObjectSelected = data.brandObjectModel;
          this._controllerBrandObject.text = "${_brandObjectSelected.id} - ${_brandObjectSelected.name}";
        }
        else if(type == 2){
          this._objectTypeSelected = data.objectTypeModel;
          this._controllerObjectType.text = "${_objectTypeSelected.id} - ${_objectTypeSelected.name}";
        }
        else if(type == 3){
          this._modelObjectSelected = data.modelObjectModel;
          this._controllerModelObject.text = "${_modelObjectSelected.id} - ${_modelObjectSelected.name}";
          this._objectUsageSelected = data.objectUsageModel;
          this._controllerUsageObjectModel.text = "${_objectUsageSelected.id} - ${_objectUsageSelected.name}";
        }
        notifyListeners();
      }
      else {
        return;
      }
    }
    else{
      showSnackBar("Grup Objek tidak boleh kosong");
    }
  }

  void searchObjectUsage(BuildContext context) async {
    ObjectUsageModel data = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => SearchObjectUsageChangeNotifier(),
                child: SearchUsageObject())));
    if (data != null) {
      this._objectUsageSelected = data;
      this._controllerUsageObjectModel.text = "${data.id} - ${data.name}";
      notifyListeners();
    } else {
      return;
    }
  }

  void searchUsageCollateral(BuildContext context,int flag) async {
    CollateralUsageModel data = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => SearchCollateralUsageChangeNotifier(),
                child: SearchCollateralUsage())));
    if (data != null) {
      if(flag == 1){
        this._collateralUsageOtoSelected = data;
        this._controllerUsageCollateralOto.text = "${data.id} - ${data.name}";
      }
      else{
        this._collateralUsagePropertySelected = data;
        this._controllerUsageCollateralProperty.text = "${data.id} - ${data.name}";
      }
      notifyListeners();
    } else {
      return;
    }
  }

  int get radioValueIsCollateralSameWithApplicantProperty => _radioValueIsCollateralSameWithApplicantProperty;

  set radioValueIsCollateralSameWithApplicantProperty(int value) {
    this._radioValueIsCollateralSameWithApplicantProperty = value;
    notifyListeners();
  }

  int get radioValueAccessCar => _radioValueAccessCar;

  set radioValueAccessCar(int value) {
    this._radioValueAccessCar = value;
    notifyListeners();
  }

  int get radioValueForAllUnitProperty => _radioValueForAllUnitProperty;

  set radioValueForAllUnitProperty(int value) {
    this._radioValueForAllUnitProperty = value;
    notifyListeners();
  }

  TextEditingController get controllerBirthPlaceValidWithIdentityLOV => _controllerBirthPlaceValidWithIdentityLOV;

  TextEditingController get controllerBirthPlaceValidWithIdentity1 => _controllerBirthPlaceValidWithIdentity1;

  TextEditingController get controllerNameOnCollateral => _controllerNameOnCollateral;

  TextEditingController get controllerIdentityNumber => _controllerIdentityNumber;

  TextEditingController get controllerIdentityType => _controllerIdentityType;

  TextEditingController get controllerCertificateNumber => _controllerCertificateNumber;

  CertificateTypeModel get certificateTypeSelected => _certificateTypeSelected;

  TextEditingController get controllerBuildingArea => _controllerBuildingArea;

  TextEditingController get controllerSurfaceArea => _controllerSurfaceArea;

  TextEditingController get controllerCertificateReleaseDate => _controllerCertificateReleaseDate;

  TextEditingController get controllerCertificateReleaseYear => _controllerCertificateReleaseYear;

  TextEditingController get controllerDateOfMeasuringLetter => _controllerDateOfMeasuringLetter;

  TextEditingController get controllerCertificateOfMeasuringLetter => _controllerCertificateOfMeasuringLetter;

  TextEditingController get controllerDateOfIMB => _controllerDateOfIMB;

  TextEditingController get controllerUsageCollateralProperty => _controllerUsageCollateralProperty;

  TextEditingController get controllerAddress => _controllerAddress;

  TextEditingController get controllerRT => _controllerRT;

  TextEditingController get controllerRW => _controllerRW;

  TextEditingController get controllerKelurahan => _controllerKelurahan;

  TextEditingController get controllerKecamatan => _controllerKecamatan;

  TextEditingController get controllerKota => _controllerKota;

  TextEditingController get controllerProv => _controllerProv;

  TextEditingController get controllerPostalCode => _controllerPostalCode;

  TextEditingController get controllerAddressFromMap => _controllerAddressFromMap;

  set certificateTypeSelected(CertificateTypeModel value) {
    this._certificateTypeSelected = value;
    notifyListeners();
  }

  PropertyTypeModel get propertyTypeSelected => _propertyTypeSelected;

  set propertyTypeSelected(PropertyTypeModel value) {
    this._propertyTypeSelected = value;
    notifyListeners();
  }

  TypeOfRoofModel get typeOfRoofSelected => _typeOfRoofSelected;

  set typeOfRoofSelected(TypeOfRoofModel value) {
    this._typeOfRoofSelected = value;
    notifyListeners();
  }

  StreetTypeModel get streetTypeSelected => _streetTypeSelected;

  set streetTypeSelected(StreetTypeModel value) {
    this._streetTypeSelected = value;
    notifyListeners();
  }

  FloorTypeModel get floorTypeSelected => _floorTypeSelected;

  set floorTypeSelected(FloorTypeModel value) {
    this._floorTypeSelected = value;
    notifyListeners();
  }

  FoundationTypeModel get foundationTypeSelected => _foundationTypeSelected;

  set foundationTypeSelected(FoundationTypeModel value) {
    this._foundationTypeSelected = value;
    notifyListeners();
  }

  WallTypeModel get wallTypeSelected => _wallTypeSelected;

  set wallTypeSelected(WallTypeModel value) {
    this._wallTypeSelected = value;
    notifyListeners();
  }

  CollateralUsageModel get collateralUsagePropertySelected => _collateralUsagePropertySelected;

  set collateralUsagePropertySelected(CollateralUsageModel value) {
    this._collateralUsagePropertySelected = value;
    notifyListeners();
  }

  JenisAlamatModel get addressTypeSelected => _addressTypeSelected;

  set addressTypeSelected(JenisAlamatModel value) {
    this._addressTypeSelected = value;
    notifyListeners();
  }

  UnmodifiableListView<CertificateTypeModel> get listCertificateType => UnmodifiableListView(_listCertificateType);

  UnmodifiableListView<PropertyTypeModel> get listPropertyType => UnmodifiableListView(_listPropertyType);

  UnmodifiableListView<TypeOfRoofModel> get listTypeOfRoof => UnmodifiableListView(_listTypeOfRoof);

  DateTime get initialDateCertificateRelease => _initialDateCertificateRelease;

  UnmodifiableListView<WallTypeModel> get listWallType => UnmodifiableListView(_listWallType);

  UnmodifiableListView<FoundationTypeModel> get listFoundationType => UnmodifiableListView(_listFoundationType);

  UnmodifiableListView<FloorTypeModel> get listFloorType => UnmodifiableListView(_listFloorType);

  UnmodifiableListView<StreetTypeModel> get listStreetType => UnmodifiableListView(_listStreetType);


  List<JenisAlamatModel> get listAddressType => _listAddressType;


  BirthPlaceModel get birthPlaceSelected => _birthPlaceSelected;

  set birthPlaceSelected(BirthPlaceModel value) {
    this._birthPlaceSelected = value;
  }

  void sameWithApplicantProperty(BuildContext context){
    var _provider = Provider.of<FormMInfoNasabahChangeNotif>(context,listen: false);
    var _providerCom = Provider.of<FormMCompanyRincianChangeNotifier>(context,listen: false);
    if(this._radioValueIsCollateralSameWithApplicantProperty == 0){
      if(this._custType == "PER"){
        this._initialDateForBirthDateProp = _provider.initialDateForTglLahir;
        this._listIdentity.clear();
        this._listIdentity.add(_provider.identitasModel);
        this._identityModel = this._listIdentity[0];
        this._controllerIdentityType.text = _identityModel.name;
        this._controllerIdentityNumber.text = _provider.controllerNoIdentitas.text;
        this._controllerNameOnCollateral.text = _provider.controllerNamaLengkapSesuaiIdentitas.text;
        this._controllerBirthDateProp.text = _provider.controllerTglLahir.text;
        this._controllerBirthPlaceValidWithIdentity1.text = _provider.controllerTempatLahirSesuaiIdentitas.text;
        this._controllerBirthPlaceValidWithIdentityLOV.text = _provider.controllerTempatLahirSesuaiIdentitasLOV.text;
        _birthPlaceSelected = _provider.birthPlaceSelected;
        // print("identitas: ${this._identityTypeSelectedAuto.id} - ${this._identityTypeSelectedAuto.name}");
        // print("no identitas: ${this._controllerIdentityNumberAuto.text}");
        // print("nama: ${this._controllerNameOnCollateralAuto.text}");
        // print("tanggal: ${this._controllerBirthDateAuto.text}");
        // print("lahir: ${this._controllerBirthPlaceValidWithIdentityAuto.text}");
        // print("lahir lov: ${this._birthPlaceSelected.KABKOT_ID} - ${this._birthPlaceSelected.KABKOT_NAME} dan ${this._controllerBirthPlaceValidWithIdentityLOV.text}");
      } else {
        this._controllerIdentityNumber.text = _providerCom.controllerNPWP.text;
        this._controllerNameOnCollateral.text = _providerCom.controllerInstitutionName.text;
        this._controllerBirthDateProp.text = _providerCom.controllerDateEstablishment.text;
      }
      addDataListJenisAlamat(context);
      notifyListeners();
    }
    else{
      this._controllerIdentityType.clear();
      this._controllerIdentityNumber.clear();
      this._controllerBirthDateProp.clear();
      this._controllerBirthPlaceValidWithIdentity1.clear();
      this._controllerBirthPlaceValidWithIdentityLOV.clear();
      this._addressTypeSelected = null;
      this._controllerAddress.clear();
      this._controllerRT.clear();
      this._controllerRW.clear();
      this._kelurahanModel = null;
      this._controllerKelurahan.clear();
      this._controllerKecamatan.clear();
      this._controllerKota.clear();
      this._controllerProv.clear();
      this._controllerPostalCode.clear();
      this._enableTf = true;
      if(this._custType != "PER"){
        this._listIdentity.clear();
        this._listIdentity.add(IdentityModel("999", "NPWP"));
        this._identityTypeSelectedAuto = this._listIdentity[0];
      }
      addListAddressType();
      notifyListeners();
    }
  }

  void sameWithApplicantAutomotive(BuildContext context) async{
    var _provider = Provider.of<FormMInfoNasabahChangeNotif>(context,listen: false);
    var _providerCom = Provider.of<FormMCompanyRincianChangeNotifier>(context,listen: false);
    SharedPreferences _preference = await SharedPreferences.getInstance();
    this._custType = _preference.getString("cust_type");
    if(this._radioValueIsCollaNameSameWithApplicantOto == 0){
      if(this._custType == "PER"){
        for(int i = 0; i < this._listIdentity.length; i++){
          if(this._listIdentity[i].id == _provider.identitasModel.id){
            this._identityTypeSelectedAuto = this._listIdentity[i];
          }
        }
        this._listIdentity.clear();
        this._listIdentity.add(_provider.identitasModel);
        this._identityTypeSelectedAuto = this._listIdentity[0];
        this._controllerIdentityNumberAuto.text = _provider.controllerNoIdentitas.text;
        this._controllerNameOnCollateralAuto.text = _provider.controllerNamaLengkapSesuaiIdentitas.text;
        this._initialDateForBirthDateAuto = _provider.initialDateForTglLahir;
        this._controllerBirthDateAuto.text = _provider.controllerTglLahir.text;
        this._controllerBirthPlaceValidWithIdentityAuto.text = _provider.controllerTempatLahirSesuaiIdentitas.text;
        this._controllerBirthPlaceValidWithIdentityLOVAuto.text = _provider.controllerTempatLahirSesuaiIdentitasLOV.text;
        this._birthPlaceAutoSelected = _provider.birthPlaceSelected;
        // print("identitas: ${this._identityTypeSelectedAuto.id} - ${this._identityTypeSelectedAuto.name}");
        // print("no identitas: ${this._controllerIdentityNumberAuto.text}");
        // print("nama: ${this._controllerNameOnCollateralAuto.text}");
        // print("tanggal: ${this._controllerBirthDateAuto.text}");
        // print("lahir: ${this._controllerBirthPlaceValidWithIdentityAuto.text}");
        // print("lahir lov: ${this._birthPlaceAutoSelected.KABKOT_ID} - ${this._birthPlaceAutoSelected.KABKOT_NAME} dan ${this._controllerBirthPlaceValidWithIdentityLOVAuto.text}");
        // print("cek data ${this._birthPlaceAutoSelected.KABKOT_ID}");
        // print("cek data name ${this._birthPlaceAutoSelected.KABKOT_NAME}");
      }
      else{
        this._controllerIdentityNumberAuto.text = _providerCom.controllerNPWP.text;
        this._controllerNameOnCollateralAuto.text = _providerCom.controllerInstitutionName.text;
        this._initialDateForBirthDateAuto = _providerCom.initialDateForDateEstablishment;
        this._controllerBirthDateAuto.text = _providerCom.controllerDateEstablishment.text;
      }
    }
    else{
      _listIdentity = IdentityType().lisIdentityModel;
      this._controllerIdentityNumberAuto.clear();
      this._controllerNameOnCollateralAuto.clear();
      this._controllerBirthDateAuto.clear();
      this._initialDateForBirthDateAuto = DateTime(dateNow.year, dateNow.month, dateNow.day);
      this._controllerBirthPlaceValidWithIdentityAuto.clear();
      this._controllerBirthPlaceValidWithIdentityLOVAuto.clear();
      this._birthPlaceAutoSelected = null;
      this._identityTypeSelectedAuto = null;
      if(this._custType != "PER"){
        this._listIdentity.clear();
        this._listIdentity.add(IdentityModel("999", "NPWP"));
        this._identityTypeSelectedAuto = this._listIdentity[0];
      }
    }
    notifyListeners();
  }

  void selectCertificateReleaseDate(BuildContext context) async {
    // final DateTime _picked = await showDatePicker(
    //     context: context,
    //     initialDate: _initialDateCertificateRelease,
    //     firstDate: DateTime(
    //         DateTime.now().year-50, DateTime.now().month, DateTime.now().day),
    //     lastDate: DateTime(
    //         DateTime.now().year, DateTime.now().month, DateTime.now().day));
    // if (_picked != null) {
    //   this._initialDateCertificateRelease = _picked;
    //   this._controllerCertificateReleaseDate.text = dateFormat.format(_picked);
    //   notifyListeners();
    // } else {
    //   return;
    // }
    var _picked = await selectDate(context, _initialDateCertificateRelease);
    if (_picked != null) {
      this._initialDateCertificateRelease = _picked;
      this._controllerCertificateReleaseDate.text = dateFormat.format(_picked);
      notifyListeners();
    } else {
      return;
    }
  }

  void selectDateOfMeasuringLetter(BuildContext context) async {
    // final DateTime _picked = await showDatePicker(
    //     context: context,
    //     initialDate: _initialDateOfMeasuringLetter,
    //     firstDate: DateTime(
    //         DateTime.now().year-50, DateTime.now().month, DateTime.now().day),
    //     lastDate: DateTime(
    //         DateTime.now().year, DateTime.now().month, DateTime.now().day));
    // if (_picked != null) {
    //   this._initialDateOfMeasuringLetter = _picked;
    //   this._controllerDateOfMeasuringLetter.text = dateFormat.format(_picked);
    //   notifyListeners();
    // } else {
    //   return;
    // }
    var _picked = await selectDate(context, _initialDateOfMeasuringLetter);
    if (_picked != null) {
      this._initialDateOfMeasuringLetter = _picked;
      this._controllerDateOfMeasuringLetter.text = dateFormat.format(_picked);
      notifyListeners();
    } else {
      return;
    }
  }


  DateTime get initialDateOfMeasuringLetter => _initialDateOfMeasuringLetter;

  void selectDateOfIMB(BuildContext context) async {
    // final DateTime _picked = await showDatePicker(
    //     context: context,
    //     initialDate: _initialDateOfIMB,
    //     firstDate: DateTime(
    //         DateTime.now().year-5, DateTime.now().month, DateTime.now().day),
    //     lastDate: DateTime(
    //         DateTime.now().year + 5, DateTime.now().month, DateTime.now().day));
    // if (_picked != null) {
    //   this._initialDateOfIMB = _picked;
    //   this._controllerDateOfIMB.text = dateFormat.format(_picked);
    //   notifyListeners();
    // } else {
    //   return;
    // }
    var _picked = await selectDate(context, _initialDateOfIMB);
    if (_picked != null) {
      this._initialDateOfIMB = _picked;
      this._controllerDateOfIMB.text = dateFormat.format(_picked);
      notifyListeners();
    } else {
      return;
    }
  }


  DateTime get initialDateOfIMB => _initialDateOfIMB;

  void addListAddressType() async{
    this._listAddressType.clear();
    this._listAddressType = await getAddressType(10);
    if(addressTypeSelected != null){
      for (int i = 0; i < this._listAddressType.length; i++) {
        if (this.addressTypeSelected.KODE == this._listAddressType[i].KODE) {
          this.addressTypeSelected = this._listAddressType[i];
        }
      }
    }
    notifyListeners();
  }

  void addDataListJenisAlamat(BuildContext context) async {
    var _provider = Provider.of<FormMInfoAlamatChangeNotif>(context, listen: false);
    this._listAddress.clear();
    this._listAddressType.clear();
    if(this._radioValueIsCollateralSameWithApplicantProperty == 0){
      for(int i=0; i < _provider.listAlamatKorespondensi.length; i++){
        this._listAddressType.add(_provider.listAlamatKorespondensi[i].jenisAlamatModel);
        this._listAddress.add(_provider.listAlamatKorespondensi[i]);
      }
      setValueAddress(0);
    }
    else{
      addListAddressType();
//        this._listAddressType = await getAddressType(10);
//      _listAddressType = [
//        JenisAlamatModel("01", "Identitas"),
//        JenisAlamatModel("02", "Domisili 1"),
//        JenisAlamatModel("03", "Domisili 2"),
//        JenisAlamatModel("04", "Domisili 3"),
//        JenisAlamatModel("05", "Kantor 1"),
//        JenisAlamatModel("05", "Kantor 2"),
//        JenisAlamatModel("07", "Kantor 3")
//      ];
    }
  }
  int _isKorespondensi = 0;

  int get isKorespondensi => _isKorespondensi;

  set isKorespondensi(int value) {
    this._isKorespondensi = value;
  }

  void setValueAddress(int flag){
    if(flag == 0){
      // Tidak paham di hapus tidak terjadi apa2
      // for(int i=0; i < this._listAddress.length; i++){
      //   if(this._listAddress[i].isCorrespondence){
      //     addressTypeSelected = this._listAddress[i].jenisAlamatModel;
      //     this._controllerAddress.text = this._listAddress[i].address;
      //     this._controllerRT.text = this._listAddress[i].rt;
      //     this._controllerRW.text = this._listAddress[i].rw;
      //     this._kelurahanModel = this._listAddress[i].kelurahanModel;
      //     this._controllerKelurahan.text = this._kelurahanModel.KEL_NAME;
      //     this._controllerKecamatan.text = this._kelurahanModel.KEC_NAME;
      //     this._controllerKota.text = this._kelurahanModel.KABKOT_NAME;
      //     this._controllerProv.text = this._kelurahanModel.PROV_NAME;
      //     this._controllerPostalCode.text = this._kelurahanModel.ZIPCODE;
      //     enableTf = false;
      //   }
      // }
    }
    else{
      for(int i=0; i < this._listAddress.length; i++){
        if(this._addressTypeSelected.KODE == this._listAddress[i].jenisAlamatModel.KODE){
          if(this._listAddress[i].isCorrespondence) _isKorespondensi = 1;
          this._controllerAddress.text = this._listAddress[i].address;
          this._controllerRT.text = this._listAddress[i].rt;
          this._controllerRW.text = this._listAddress[i].rw;
          this._kelurahanModel = this._listAddress[i].kelurahanModel;
          this._controllerKelurahan.text = this._kelurahanModel.KEL_NAME;
          this._controllerKecamatan.text = this._kelurahanModel.KEC_NAME;
          this._controllerKota.text = this._kelurahanModel.KABKOT_NAME;
          this._controllerProv.text = this._kelurahanModel.PROV_NAME;
          this._controllerPostalCode.text = this._kelurahanModel.ZIPCODE;
          enableTf = false;
        }
      }
    }
  }

  GlobalKey<FormState> get keyFormAuto => _keyAuto;

  GlobalKey<FormState> get keyFormProp => _keyProp;

  bool get flag => _flag;

  set flag(bool value) {
    _flag = value;
    notifyListeners();
  }

  void check(BuildContext context, int type) {
    print("check jalan");
    var _form;
    if(type == 1){
      // colla oto
      autoValidateProp = false;
      _form = _keyAuto.currentState;
      if(this._controllerGradeUnit.text.isEmpty) { // dipakai untuk ngasih info lakukan lookup utj untuk melengkapi data
        showDialog(
            context: context,
            barrierDismissible: false,
            builder: (BuildContext context){
              return Theme(
                data: ThemeData(
                    fontFamily: "NunitoSans",
                    primaryColor: Colors.black,
                    primarySwatch: primaryOrange,
                    accentColor: myPrimaryColor
                ),
                child: AlertDialog(
                  title: Text("Informasi", style: TextStyle(fontWeight: FontWeight.bold)),
                  content: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Text("Harap lakukan Lookup UTJ untuk melengkapi data.",),
                    ],
                  ),
                  actions: <Widget>[
                    new FlatButton(
                      onPressed: () => Navigator.of(context).pop(true),
                      child: new Text('Close'),
                    ),
                    // new FlatButton(
                    //   onPressed: () => Navigator.of(context).pop(true),
                    //   child: new Text('Tidak'),
                    // ),
                  ],
                ),
              );
            }
        );
      }
      if (_form.validate() && collateralTypeModel != null) {
        checkDataDakor();
        flag = true;
        autoValidateAuto = false;
        Navigator.pop(context);
      } else {
        flag = false;
        autoValidateAuto = true;
      }
    }
    else if(type == 2) {
      // colla prop
      autoValidateAuto = false;
      _form = _keyProp.currentState;
      if (_form.validate() && collateralTypeModel != null) {
        checkDataDakor();
        flag = true;
        autoValidateProp = false;
        Navigator.pop(context);
      } else {
        flag = false;
        autoValidateProp = true;
      }
    }
    else {
      flag = true;
      Navigator.pop(context);
    }
    Provider.of<TaksasiUnitChangeNotifier>(context,listen: false).setIsVisible(context);
    notifyListeners();
  }

  Future<bool> onBackPress() async{
    var _form;
    if(collateralTypeModel == null){
      return true;
    }
    if(collateralTypeModel.id == "001"){
      autoValidateProp = false;
      _form = _keyAuto.currentState;
      if (_form.validate() && collateralTypeModel != null) {
        checkDataDakor();
        flag = true;
        autoValidateAuto = false;
      } else {
        flag = false;
        autoValidateAuto = true;
      }
    }
    else if(collateralTypeModel.id == "002") {
      autoValidateAuto = false;
      _form = _keyProp.currentState;
      if (_form.validate() && collateralTypeModel != null) {
        checkDataDakor();
        flag = true;
        autoValidateProp = false;
      } else {
        flag = false;
        autoValidateProp = true;
      }
    }
    else {
      flag = true;
    }
    return true;
  }

  void clearDataInfoCollateral(){
    _autoValidateAuto = false;
    _autoValidateProp = false;
    _flag = false;

    // AUTOMOTIVE
    _collateralTypeModel = null;
    _controllerCollateralType.text = "";
    _radioValueIsCollateralSameWithUnitOto = 1;
    _radioValueIsCollaNameSameWithApplicantOto = 1;
    _controllerGroupObject.text = "";
    groupObjectSelected = null;
    _controllerObject.clear();
    objectSelected = null;
    _controllerTypeProduct.clear();
    productTypeSelected = null;
    _controllerBrandObject.text = "";
    brandObjectSelected = null;
    _controllerObjectType.text = "";
    objectTypeSelected = null;
    _controllerModelObject.text = "";
    modelObjectSelected = null;
    _controllerUsageObjectModel.text = "";
    objectUsageSelected = null;
    _yearProductionSelected = null;
    _yearRegistrationSelected = null;
    _radioValueYellowPlat = 0;
    _radioValueBuiltUpNonATPM = 0;
    _radioValueWorthyOrUnworthy = 0;
    _controllerUsageCollateralOto.text = "";
    _radioValueForAllUnitOto = 0;
    this._identityTypeSelectedAuto = null;
    this._controllerIdentityNumberAuto.clear();
    this._controllerNameOnCollateralAuto.clear();
    this._initialDateForBirthDateAuto = DateTime(dateNow.year, dateNow.month, dateNow.day);
    this._controllerBirthDateAuto.clear();
    this._controllerBirthPlaceValidWithIdentityAuto.clear();
    this._controllerBirthPlaceValidWithIdentityLOVAuto.clear();
    this._birthPlaceAutoSelected = null;

    this._controllerPoliceNumber.clear();
    this._controllerFrameNumber.clear();
    this._controllerMachineNumber.clear();
    this._controllerBPKPNumber.clear();
    _controllerGradeUnit.text = "";
    _controllerFasilitasUTJ.text = "";
    _controllerNamaBidder.text = "";
    _controllerHargaJualShowroom.text = "";
    _controllerPerlengkapanTambahan.text = "";
    _controllerMPAdira.text = "";
    _controllerMPAdiraUpload.clear();
    _controllerHargaPasar.text = "";
    _controllerRekondisiFisik.text = "";
    // _controllerDpGuarantee.clear();
    _controllerPHMaxAutomotive.text = "";
    _controllerTaksasiPriceAutomotive.text = "";

    // _certificateTypeSelected = null;
    // _propertyTypeSelected = null;
    // _typeOfRoofSelected = null;
    // _wallTypeSelected = null;
    // _foundationTypeSelected = null;
    // _floorTypeSelected = null;
    // _streetTypeSelected = null;
    // _collateralUsagePropertySelected = null;
    // _addressTypeSelected = null;
    // _enableTf = true;
    // _kelurahanModel = null;
    // END AUTOMOTIVE

    //  PROPERTI
    _radioValueIsCollateralSameWithApplicantProperty = 1;
    _controllerIdentityType.text = "";
    _controllerIdentityNumber.text = "";
    _controllerNameOnCollateral.text = "";
    _controllerBirthPlaceValidWithIdentity1.text = "";
    _controllerBirthPlaceValidWithIdentityLOV.text = "";
    this._controllerBirthDateProp.clear();
    this._initialDateForBirthDateProp = DateTime(dateNow.year, dateNow.month, dateNow.day);
    _controllerCertificateNumber.text = "";
    _certificateTypeSelected = null;
    _propertyTypeSelected = null;
    _controllerBuildingArea.text = "";
    _controllerSurfaceArea.text = "";
    _controllerCertificateReleaseDate.text = "";
    _controllerCertificateReleaseYear.text = "";
    _controllerDateOfMeasuringLetter.text = "";
    _controllerCertificateOfMeasuringLetter.text = "";
    _typeOfRoofSelected = null;
    _wallTypeSelected = null;
    this._identityModel = null;
    _floorTypeSelected = null;
    _foundationTypeSelected = null;
    _streetTypeSelected = null;
    _radioValueAccessCar = 0;
    _controllerDateOfIMB.text = "";
    _collateralUsagePropertySelected = null;
    _controllerUsageCollateralProperty.text = "";
    _radioValueForAllUnitProperty = 0;
    _addressTypeSelected = null;
    _controllerAddress.text = "";
    _controllerRT.text = "";
    _controllerRW.text = "";
    _controllerKelurahan.text = "";
    _controllerKecamatan.text = "";
    _controllerKota.text = "";
    _controllerProv.text = "";
    _controllerPostalCode.text = "";

    _controllerTaksasiPrice.text = "";
    _controllerSifatJaminan.text = "";
    _controllerBuktiKepemilikan.text = "";
    _controllerNamaPemegangHak.text = "";
    _controllerNoSuratUkur.text = "";
    _controllerDPJaminan.text = "";
    _controllerPHMax.text = "";
    _controllerJarakFasumPositif.text = "";
    _controllerJarakFasumNegatif.text = "";
    _controllerHargaTanah.text = "";
    _controllerHargaNJOP.text = "";
    _controllerHargaBangunan.text = "";
    _controllerJumlahRumahDalamRadius.text = "";
    _controllerMasaHakBerlaku.text = "";
    _controllerNoIMB.text = "";
    _controllerLuasBangunanIMB.text = "";
    _controllerLTV.text = "";
//  END PROPERTI
    disableJenisPenawaran = false;
    isDisablePACIAAOSCONA = false;
  }

  FormatCurrency get formatCurrency => _formatCurrency;

  RegExInputFormatter get amountValidator => _amountValidator;

  BirthPlaceModel get birthPlaceAutoSelected => _birthPlaceAutoSelected;

  set birthPlaceAutoSelected(BirthPlaceModel value) {
    this._birthPlaceAutoSelected = value;
    notifyListeners();
  }

  void searchBirthPlace(BuildContext context,int flag) async{
    BirthPlaceModel data = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => SearchBirthPlaceChangeNotifier(),
                child: SearchBirthPlace())));
    if (data != null) {
      if(flag == 0){
        this._birthPlaceAutoSelected = data;
        this._controllerBirthPlaceValidWithIdentityLOVAuto.text = "${data.KABKOT_ID} - ${data.KABKOT_NAME}";
        notifyListeners();
      }
     else{
        this._birthPlaceSelected = data;
        this._controllerBirthPlaceValidWithIdentityLOV.text = "${data.KABKOT_ID} - ${data.KABKOT_NAME}";
        notifyListeners();
     }
    }
    else {
      return;
    }
  }

  IdentityModel get identityModel => _identityModel;

  set identityModel(IdentityModel value) {
    this._identityModel = value;
    notifyListeners();
  }

  DateTime _initialDateForBirthDateAuto = DateTime(dateNow.year, dateNow.month, dateNow.day);
  DateTime _initialDateForBirthDateProp = DateTime(dateNow.year, dateNow.month, dateNow.day);

  DateTime get initialDateForBirthDateAuto => _initialDateForBirthDateAuto;
  DateTime get initialDateForBirthDateProp => _initialDateForBirthDateProp;

  void selectBirthDate(BuildContext context,int flag) async {
    // final DateTime picked = await showDatePicker(
    //     context: context,
    //     initialDate: flag == 0 ? _initialDateForBirthDateAuto : _initialDateForBirthDateProp,
    //     firstDate: DateTime(1920, 1, 1),
    //     lastDate:DateTime(dateNow.year, dateNow.month, dateNow.day));
    // if (picked != null) {
    //   if(flag == 0){
    //     this._controllerBirthDateAuto.text = dateFormat.format(picked);
    //     this._initialDateForBirthDateAuto = picked;
    //   }
    //   else{
    //     this._controllerBirthDateProp.text = dateFormat.format(picked);
    //     this._initialDateForBirthDateProp = picked;
    //   }
    //   notifyListeners();
    // } else {
    //   return;
    // }
    var picked = await selectDateLast10Year(context, flag == 0 ? _initialDateForBirthDateAuto : _initialDateForBirthDateProp);
    if (picked != null) {
      if(flag == 0){
        this._controllerBirthDateAuto.text = dateFormat.format(picked);
        this._initialDateForBirthDateAuto = picked;
      }
      else{
        this._controllerBirthDateProp.text = dateFormat.format(picked);
        this._initialDateForBirthDateProp = picked;
      }
      notifyListeners();
    } else {
      return;
    }
  }

  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  void showSnackBar(String text){
    this._scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor));
  }

  MS2ApplObjtCollOtoModel _modelOto;

  MS2ApplObjtCollOtoModel get modelOto => _modelOto;

  set modelOto(MS2ApplObjtCollOtoModel value) {
    this._modelOto = value;
  }

  Future<void> saveToSQLiteOto(BuildContext context) async {
    print("save otto");
    SharedPreferences _preferences = await SharedPreferences.getInstance();

    modelOto = MS2ApplObjtCollOtoModel(
        null,
        collateralID,
        1,
        this._radioValueIsCollateralSameWithUnitOto,
        this._radioValueIsCollaNameSameWithApplicantOto,
        this._identityTypeSelectedAuto != null ? this._identityTypeSelectedAuto.id : "",
        this._identityTypeSelectedAuto != null ? this._identityTypeSelectedAuto.name : "",
        this._controllerIdentityNumberAuto.text != "" ? this._controllerIdentityNumberAuto.text : "",
        this._controllerNameOnCollateralAuto.text != "" ? this._controllerNameOnCollateralAuto.text : "",
        this._controllerBirthDateAuto.text.isNotEmpty ? this._initialDateForBirthDateAuto.toString() : "",  //this._controllerBirthDateAuto.text != "" ? this._controllerBirthDateAuto.text : "",
        this._controllerBirthPlaceValidWithIdentityAuto.text != "" ? this._controllerBirthPlaceValidWithIdentityAuto.text : "",
        this._birthPlaceAutoSelected != null ? this._birthPlaceAutoSelected.KABKOT_ID : "",
        this._birthPlaceAutoSelected != null ? this._birthPlaceAutoSelected.KABKOT_NAME : "",
        this._groupObjectSelected != null ? this._groupObjectSelected.KODE : "",
        this._groupObjectSelected != null ? this._groupObjectSelected.DESKRIPSI : "",
        this._objectSelected != null ? this._objectSelected.id : "",
        this._objectSelected != null ? this._objectSelected.name : "",
        this._productTypeSelected != null ? this._productTypeSelected.id : "",
        this._productTypeSelected != null ? this._productTypeSelected.name : "",
        this._brandObjectSelected != null ? this._brandObjectSelected.id : "",
        this._brandObjectSelected != null ? this._brandObjectSelected.name : "",
        this._objectTypeSelected != null ? this._objectTypeSelected.id : "",
        this._objectTypeSelected != null ? this._objectTypeSelected.name : "",
        this._modelObjectSelected != null ? this._modelObjectSelected.id :"",
        this._modelObjectSelected != null ? this._modelObjectSelected.name :"",
        this._objectUsageSelected != null ? this._objectUsageSelected.id : "", //dari model
        this._objectUsageSelected != null ? this._objectUsageSelected.name : "", //dari model
        this._yearProductionSelected != null ? int.parse(this._yearProductionSelected) : null,
        this._yearRegistrationSelected != null ? int.parse(this._yearRegistrationSelected) : null,
        this._radioValueYellowPlat,
        this._radioValueBuiltUpNonATPM,
        this._controllerBPKPNumber.text != "" ? this._controllerBPKPNumber.text : "",
        this._controllerFrameNumber.text != "" ? this._controllerFrameNumber.text : "",
        this._controllerMachineNumber.text != "" ? this._controllerMachineNumber.text : "",
        this._controllerPoliceNumber.text != "" ? this._controllerPoliceNumber.text : "",
        this._controllerGradeUnit.text != "" ? this._controllerGradeUnit.text : "",
        this._controllerFasilitasUTJ.text != "" ? this._controllerFasilitasUTJ.text : "",
        this._controllerNamaBidder.text != "" ? this._controllerNamaBidder.text : "",
        this._controllerHargaJualShowroom.text != "" ? double.parse(this._controllerHargaJualShowroom.text.replaceAll(",", "")) : "",
        this._controllerPerlengkapanTambahan.text != "" ? this._controllerPerlengkapanTambahan.text : "",
        this._controllerMPAdira.text != "" ? double.parse(this._controllerMPAdira.text.replaceAll(",", "")) : null,
        this._controllerRekondisiFisik.text != "" ? double.parse(this._controllerRekondisiFisik.text.replaceAll(",", "")) : null,
        this._radioValueWorthyOrUnworthy,
        null,
        this._controllerDPJaminan.text != "" ? double.parse(this._controllerDPJaminan.text.replaceAll(",", "")) : null,
        this._controllerPHMaxAutomotive.text != "" ? double.parse(this._controllerPHMaxAutomotive.text.replaceAll(",", "")) : null,
        this._controllerTaksasiPriceAutomotive.text != "" ? double.parse(this._controllerTaksasiPriceAutomotive.text.replaceAll(",", "")) : null,
        this._collateralUsageOtoSelected != null ? this._collateralUsageOtoSelected.id : "", //dari cola
        this._collateralUsageOtoSelected != null ? this._collateralUsageOtoSelected.name : "", //dari cola
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        1,
        DateTime.now().toString(),
        _preferences.getString("username"),
        null,
        null,
        null,
        null,
        this._controllerMPAdiraUpload.text != "" ? int.parse(this._controllerMPAdiraUpload.text.replaceAll(",", "").split(".")[0]) : null,
        "0",
        this._isIdentityTypeSelectedAutoChanges ? "1" : "0",
        this._isRadioValueIsCollaNameSameWithApplicantOtoChanges ? "1" : "",
        this._isIdentityTypeSelectedAutoChanges ? "1" : "0",
        this._isIdentityNumberAutoChanges ? "1" : "0",
        this._isNameOnCollateralAutoChanges ? "1" : "0",
        this._isBirthDateAutoChanges ? "1" : "0",
        this._isBirthPlaceValidWithIdentityAutoChanges ? "1" : "0",
        this._isBirthPlaceValidWithIdentityLOVAutoChanges ? "1" : "0",
        this._isGroupObjectChanges ? "1" : "0",
        this._isObjectChanges ? "1" : "0",
        this._isTypeProductChanges ? "1" : "0",
        this._isBrandObjectChanges ? "1" : "0",
        this._isObjectTypeChanges ? "1" : "0",
        this._isModelObjectChanges ? "1" : "0",
        this._isUsageObjectModelChanges ? "1" : "0",
        this._isYearProductionSelectedChanges ? "1" : "0",
        this._isYearRegistrationSelectedChanges ? "1" : "0",
        this._isRadioValueYellowPlatChanges ? "1" : "0",
        this._isRadioValueBuiltUpNonATPMChanges ? "1" : "0",
        this._isBPKPNumberChanges ? "1" : "0",
        this._isFrameNumberChanges ? "1" : "0",
        this._isMachineNumberChanges ? "1" : "0",
        this._isPoliceNumberChanges ? "1" : "0",
        this._isGradeUnitChanges ? "1" : "0",
        this._isFasilitasUTJChanges ? "1" : "0",
        this._isNamaBidderChanges ? "1" : "0",
        this._isHargaJualShowroomChanges ? "1" : "0",
        this._isPerlengkapanTambahanChanges ? "1" : "0",
        this._isMPAdiraChanges ? "1" : "0",
        this._isMPAdiraUploadChanges ? "1" : "0",
        this._isRekondisiFisikChanges ? "1" : "0",
        this._isRadioValueWorthyOrUnworthyChanges ? "1" : "0",
        "0",
        this._isDpGuaranteeChanges ? "1" : "0",
        this._isPHMaxAutomotiveChanges ? "1" : "0",
        this._isTaksasiPriceAutomotiveChanges ? "1" : "0",
        this._isUsageCollateralOtoChanges ? "1" : "0",
        "0",
        "0",
        "0",
        "0",
        "0",
        "0",
        "0",
        "0",
    );
    debugPrint("ini modelku  = ${_modelOto.object_purpose}");
    debugPrint("ini modelku  desc = ${_modelOto.object_purpose_desc}");

    await _dbHelper.insertMS2ApplObjtCollOto(modelOto);
    // String _message = await _submitDataPartial.submitColla(context, 6, _modelOto, null, []);
    // return _message;
  }

  Future<bool> deleteSQLiteCollaOto() async{
    return await _dbHelper.deleteApplCollaOto();
  }

  MS2ApplObjtCollPropModel _modelProp;

  MS2ApplObjtCollPropModel get modelProp => _modelProp;

  set modelProp(MS2ApplObjtCollPropModel value) {
    this._modelProp = value;
  }

  List<MS2CustAddrModel> _listAddressPropColla = [];

  List<MS2CustAddrModel> get listAddressPropColla => _listAddressPropColla;

  Future<void> saveToSQLiteProperti(BuildContext context,String type) async {
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    var _providerDetailCustomer = Provider.of<FormMInfoNasabahChangeNotif>(context,listen: false);
    var _providerDetailPIC = Provider.of<FormMCompanyManajemenPICChangeNotifier>(context,listen: false);

    // print(this._typeOfRoofSelected.id);
    // print(this._typeOfRoofSelected.name);
    // print(this._wallTypeSelected.id);
    // print(this._wallTypeSelected.name);
    // print(this._floorTypeSelected.id);
    // print(this._floorTypeSelected.name);
    // print(this._foundationTypeSelected.id);
    // print(this._foundationTypeSelected.name);

    // _dbHelper.insertMS2ApplObjtCollProp(MS2ApplObjtCollPropModel(
    //   null,
    //   null,
    //   this._radioValueIsCollateralSameWithApplicantProperty,
    //   this._radioValueIsCollateralSameWithApplicantProperty == 1 ? this._identityModel != null ? this._identityModel.id : null : _preferences.getString("cust_type") != "COM" ? _providerDetailCustomer.identitasModel.id : _providerDetailPIC.typeIdentitySelected.id,
    //   this._radioValueIsCollateralSameWithApplicantProperty == 1 ? this._identityModel != null ? this._identityModel.name : null : _preferences.getString("cust_type") != "COM" ? _providerDetailCustomer.identitasModel.name : _providerDetailPIC.typeIdentitySelected.text,
    //   this._controllerIdentityNumber.text != "" ? this._controllerIdentityNumber.text : "",
    //   this._controllerNameOnCollateral.text != "" ? this._controllerNameOnCollateral.text : "",
    //   this._controllerBirthDateProp.text != "" ? this._controllerBirthDateProp.text : "",
    //   this._controllerBirthPlaceValidWithIdentity1.text != "" ? this._controllerBirthPlaceValidWithIdentity1.text : "",
    //   this._birthPlaceSelected != null ? this._birthPlaceSelected.KABKOT_ID : null,
    //   this._birthPlaceSelected != null ? this._birthPlaceSelected.KABKOT_NAME : null,
    //   this._controllerCertificateNumber.text != "" ? this._controllerCertificateNumber.text : "",
    //   this._certificateTypeSelected != null ? this._certificateTypeSelected.id:"",
    //   this._certificateTypeSelected != null ? this._certificateTypeSelected.name:"",
    //   this._propertyTypeSelected != null ? this._propertyTypeSelected.id : "",
    //   this._propertyTypeSelected != null ? this._propertyTypeSelected.name : "",
    //   this._controllerBuildingArea.text != "" ? double.parse(this._controllerBuildingArea.text.replaceAll(",", "")) : 0,
    //   this._controllerSurfaceArea.text != "" ? double.parse(this._controllerSurfaceArea.text.replaceAll(",", "")) : 0,
    //   this._controllerDPJaminan.text != "" ? double.parse(this._controllerDPJaminan.text.replaceAll(",", "")) : 0,
    //   this._controllerPHMax.text != "" ? double.parse(this._controllerPHMax.text.replaceAll(",", "")) : "",
    //   this._controllerPHMax.text != "" ? double.parse(this._controllerTaksasiPrice.text.replaceAll(",", "")) : "",
    //   this._collateralUsagePropertySelected != null ? this._collateralUsagePropertySelected.id : "",
    //   this._collateralUsagePropertySelected != null ? this._collateralUsagePropertySelected.name : "",
    //   this._controllerJarakFasumPositif.text != "" ? double.parse(this._controllerJarakFasumPositif.text.replaceAll(",", "")) : 0,
    //   this._controllerJarakFasumNegatif.text != "" ? double.parse(this._controllerJarakFasumNegatif.text.replaceAll(",", "")) : 0,
    //   this._controllerHargaTanah.text != "" ? double.parse(this._controllerHargaTanah.text.replaceAll(",", "")) : 0,
    //   this._controllerHargaNJOP.text != "" ? double.parse(this._controllerHargaNJOP.text.replaceAll(",", "")) : 0,
    //   this._controllerHargaBangunan.text != "" ? double.parse(this._controllerHargaBangunan.text.replaceAll(",", "")) : 0,
    //   this._typeOfRoofSelected != null ? this._typeOfRoofSelected.id : "",
    //   this._typeOfRoofSelected != null ? this._typeOfRoofSelected.name : "",
    //   this._wallTypeSelected != null ? this._wallTypeSelected.id : "",
    //   this._wallTypeSelected != null ? this._wallTypeSelected.name : "",
    //   this._floorTypeSelected != null ? this._floorTypeSelected.id : "",
    //   this._floorTypeSelected != null ? this._floorTypeSelected.name : "",
    //   this._foundationTypeSelected != null ? this._foundationTypeSelected.id:"",
    //   this._foundationTypeSelected != null ? this._foundationTypeSelected.name:"",
    //   this._streetTypeSelected != null ? this._streetTypeSelected.id : "",
    //   this._streetTypeSelected != null ? this._streetTypeSelected.name : "",
    //   this._radioValueAccessCar,
    //   this._controllerJumlahRumahDalamRadius.text != "" ? int.parse(this._controllerJumlahRumahDalamRadius.text.replaceAll(",", "")) : 0,
    //   this._controllerSifatJaminan.text != "" ? this._controllerSifatJaminan.text : "",
    //   this._controllerBuktiKepemilikan.text != "" ? this._controllerBuktiKepemilikan.text : "",
    //   this._controllerCertificateReleaseDate.text != "" ? this._controllerCertificateReleaseDate.text : "",
    //   this._controllerCertificateReleaseYear.text != "" ? int.parse(this._controllerCertificateReleaseYear.text.replaceAll(",", "")) : 0,
    //   this._controllerNamaPemegangHak.text != "" ? this._controllerNamaPemegangHak.text : "",
    //   this._controllerNoSuratUkur.text != "" ? this._controllerNoSuratUkur.text : "",
    //   this._controllerDateOfMeasuringLetter.text != "" ? this._controllerDateOfMeasuringLetter.text : "",
    //   this._controllerCertificateOfMeasuringLetter.text != "" ? this._controllerCertificateOfMeasuringLetter.text : "",
    //   this._controllerMasaHakBerlaku.text != "" ? this._controllerMasaHakBerlaku.text : "",
    //   this._controllerNoIMB.text != "" ? this._controllerNoIMB.text : "",
    //   this._controllerDateOfIMB.text != "" ? this._controllerDateOfIMB.text : "",
    //   this._controllerLuasBangunanIMB.text != "" ? double.parse(this._controllerLuasBangunanIMB.text.replaceAll(",", "")) : 0,
    //   this._controllerLTV.text != "" ? double.parse(this._controllerLTV.text.replaceAll(",", "")): 0,
    //   null,
    //   1,
    //   DateTime.now().toString(),
    //   _preferences.getString("username"),
    //   null,
    //   null,
    // ));
    modelProp = MS2ApplObjtCollPropModel(
      null,
      collateralID,
      _radioValueForAllUnitProperty,
      this._radioValueIsCollateralSameWithApplicantProperty,
      this._radioValueIsCollateralSameWithApplicantProperty == 1 ? this._identityModel != null ? this._identityModel.id : null : _preferences.getString("cust_type") != "COM" ? _providerDetailCustomer.identitasModel.id : _providerDetailPIC.typeIdentitySelected.id,
      this._radioValueIsCollateralSameWithApplicantProperty == 1 ? this._identityModel != null ? this._identityModel.name : null : _preferences.getString("cust_type") != "COM" ? _providerDetailCustomer.identitasModel.name : _providerDetailPIC.typeIdentitySelected.text,
      this._controllerIdentityNumber.text != "" ? this._controllerIdentityNumber.text : "",
      this._controllerNameOnCollateral.text != "" ? this._controllerNameOnCollateral.text : "",
      this._controllerBirthDateProp.text != "" ? this._controllerBirthDateProp.text : "",
      this._controllerBirthPlaceValidWithIdentity1.text != "" ? this._controllerBirthPlaceValidWithIdentity1.text : "",
      this._birthPlaceSelected != null ? this._birthPlaceSelected.KABKOT_ID : null,
      this._birthPlaceSelected != null ? this._birthPlaceSelected.KABKOT_NAME : null,
      this._controllerCertificateNumber.text != "" ? this._controllerCertificateNumber.text : "",
      this._certificateTypeSelected != null ? this._certificateTypeSelected.id:"",
      this._certificateTypeSelected != null ? this._certificateTypeSelected.name:"",
      this._propertyTypeSelected != null ? this._propertyTypeSelected.id : "",
      this._propertyTypeSelected != null ? this._propertyTypeSelected.name : "",
      this._controllerBuildingArea.text != "" ? double.parse(this._controllerBuildingArea.text.replaceAll(",", "")) : 0,
      this._controllerSurfaceArea.text != "" ? double.parse(this._controllerSurfaceArea.text.replaceAll(",", "")) : 0,
      this._controllerDPJaminan.text != "" ? double.parse(this._controllerDPJaminan.text.replaceAll(",", "")) : 0,
      this._controllerPHMax.text != "" ? double.parse(this._controllerPHMax.text.replaceAll(",", "")) : "",
      this._controllerTaksasiPrice.text != "" ? double.parse(this._controllerTaksasiPrice.text.replaceAll(",", "")) : "",
      this._collateralUsagePropertySelected != null ? this._collateralUsagePropertySelected.id : "",
      this._collateralUsagePropertySelected != null ? this._collateralUsagePropertySelected.name : "",
      this._controllerJarakFasumPositif.text != "" ? double.parse(this._controllerJarakFasumPositif.text.replaceAll(",", "")) : 0,
      this._controllerJarakFasumNegatif.text != "" ? double.parse(this._controllerJarakFasumNegatif.text.replaceAll(",", "")) : 0,
      this._controllerHargaTanah.text != "" ? double.parse(this._controllerHargaTanah.text.replaceAll(",", "")) : 0,
      this._controllerHargaNJOP.text != "" ? double.parse(this._controllerHargaNJOP.text.replaceAll(",", "")) : 0,
      this._controllerHargaBangunan.text != "" ? double.parse(this._controllerHargaBangunan.text.replaceAll(",", "")) : 0,
      this._typeOfRoofSelected != null ? this._typeOfRoofSelected.id : "",
      this._typeOfRoofSelected != null ? this._typeOfRoofSelected.name : "",
      this._wallTypeSelected != null ? this._wallTypeSelected.id : "",
      this._wallTypeSelected != null ? this._wallTypeSelected.name : "",
      this._floorTypeSelected != null ? this._floorTypeSelected.id : "",
      this._floorTypeSelected != null ? this._floorTypeSelected.name : "",
      this._foundationTypeSelected != null ? this._foundationTypeSelected.id:"",
      this._foundationTypeSelected != null ? this._foundationTypeSelected.name:"",
      this._streetTypeSelected != null ? this._streetTypeSelected.id : "",
      this._streetTypeSelected != null ? this._streetTypeSelected.name : "",
      this._radioValueAccessCar,
      this._controllerJumlahRumahDalamRadius.text != "" ? int.parse(this._controllerJumlahRumahDalamRadius.text.replaceAll(",", "")) : 0,
      this._controllerSifatJaminan.text != "" ? this._controllerSifatJaminan.text : "",
      this._controllerBuktiKepemilikan.text != "" ? this._controllerBuktiKepemilikan.text : "",
      this._controllerCertificateReleaseDate.text != "" ? this._controllerCertificateReleaseDate.text : "",
      this._controllerCertificateReleaseYear.text != "" ? int.parse(this._controllerCertificateReleaseYear.text.replaceAll(",", "")) : 0,
      this._controllerNamaPemegangHak.text != "" ? this._controllerNamaPemegangHak.text : "",
      this._controllerNoSuratUkur.text != "" ? this._controllerNoSuratUkur.text : "",
      this._controllerDateOfMeasuringLetter.text != "" ? this._controllerDateOfMeasuringLetter.text : "",
      this._controllerCertificateOfMeasuringLetter.text != "" ? this._controllerCertificateOfMeasuringLetter.text : "",
      this._controllerMasaHakBerlaku.text != "" ? this._controllerMasaHakBerlaku.text : "",
      this._controllerNoIMB.text != "" ? this._controllerNoIMB.text : "",
      this._controllerDateOfIMB.text != "" ? this._controllerDateOfIMB.text : "",
      this._controllerLuasBangunanIMB.text != "" ? double.parse(this._controllerLuasBangunanIMB.text.replaceAll(",", "")) : 0,
      this._controllerLTV.text != "" ? double.parse(this._controllerLTV.text.replaceAll(",", "")): 0,
      null,
      1,
      DateTime.now().toString(),
      _preferences.getString("username"),
      null,
      null,
      "0",
      this._isRadioValueIsCollateralSameWithApplicantPropertyChanges ? "1" : "0",
      this._isIdentityModelChanges ? "1" : "0",
      this._isIdentityNumberPropChanges ? "1" : "0",
      this._isNameOnCollateralChanges ? "1" : "0",
      this._isBirthDatePropChanges ? "1" : "0",
      this._isBirthPlaceValidWithIdentity1Changes ? "1" : "0",
      this._isBirthPlaceValidWithIdentityLOVChanges ? "1" : "0",
      this._isCertificateNumberChanges ? "1" : "0",
      this._isCertificateTypeSelectedChanges ? "1" : "0",
      this._isPropertyTypeSelectedChanges ? "1" : "0",
      this._isBuildingAreaChanges ? "1" : "0",
      this._isSurfaceAreaChanges ? "1" : "0",
      this._isDPJaminanChanges ? "1" : "0",
      this._isPHMaxChanges ? "1" : "0",
      this._isTaksasiPriceChanges ? "1" : "0",
      this._isUsageCollateralPropertyChanges ? "1" : "0",
      this._isJarakFasumPositifChanges ? "1" : "0",
      this._isJarakFasumNegatifChanges ? "1" : "0",
      this._isHargaTanahChanges ? "1" : "0",
      this._isHargaNJOPChanges ? "1" : "0",
      this._isHargaBangunanChanges ? "1" : "0",
      this._isTypeOfRoofSelectedChanges ? "1" : "0",
      this._isWallTypeSelectedChanges ? "1" : "0",
      this._isFloorTypeSelectedChanges ? "1" : "0",
      this._isFoundationTypeSelectedChanges ? "1" : "0",
      this._isStreetTypeSelectedChanges ? "1" : "0",
      this._isRadioValueAccessCarChanges ? "1" : "0",
      this._isJumlahRumahDalamRadiusChanges ? "1" : "0",
      this._isSifatJaminanChanges ? "1" : "0",
      this._isBuktiKepemilikanChanges ? "1" : "0",
      this._isCertificateReleaseDateChanges ? "1" : "0",
      this._isCertificateReleaseYearChanges ? "1" : "0",
      this._isNamaPemegangHakChanges ? "1" : "0",
      this._isNoSuratUkurChanges ? "1" : "0",
      this._isDateOfMeasuringLetterChanges ? "1" : "0",
      this._isCertificateOfMeasuringLetterChanges ? "1" : "0",
      this._isMasaHakBerlakuChanges ? "1" : "0",
      this._isNoIMBChanges ? "1" : "0",
      this._isDateOfIMBChanges ? "1" : "0",
      this._isLuasBangunanIMBChanges ? "1" : "0",
      this._isLTVChanges ? "1" : "0",
      "0",
    );
    _dbHelper.insertMS2ApplObjtCollProp(modelProp);

    // merubah nama listAddress menjadi listAddressPropColla dan membuat getter untuk di akses dr submit partial
    listAddressPropColla.add(MS2CustAddrModel(
        "000101",
        this._addressID,
        this._foreignBusinessID,
        null,
        "0",
        type,
        this._controllerAddress.text,
        this._controllerRT.text,
        null,
        this._controllerRW.text,
        null,
        this._kelurahanModel != null ? this._kelurahanModel.PROV_ID : "",
        this._kelurahanModel != null ? this._kelurahanModel.PROV_NAME : "",
        this._kelurahanModel != null ? this._kelurahanModel.KABKOT_ID : "",
        this._kelurahanModel != null ? this._kelurahanModel.KABKOT_NAME:"",
        this._kelurahanModel != null ? this._kelurahanModel.KEC_ID:"",
        this._kelurahanModel != null ? this._kelurahanModel.KEC_NAME: "",
        this._kelurahanModel != null ? this._kelurahanModel.KEL_ID : "",
        this._kelurahanModel != null ? this._kelurahanModel.KEL_NAME: "",
        this._kelurahanModel != null ? this._kelurahanModel.ZIPCODE : "" ,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        this._addressTypeSelected != null ? this._addressTypeSelected.KODE : "",
        this._addressTypeSelected != null ? this._addressTypeSelected.DESKRIPSI : "",
        this._addressFromMap != null ? this._addressFromMap['latitude'].toString() : "",
        this._addressFromMap != null ? this._addressFromMap['longitude'].toString() : "",
        this._addressFromMap != null ? this._addressFromMap['address'] : "",
        DateTime.now().toString(),
        _preferences.getString("username"),
        null,
        null,
        1,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null
    ));
    _dbHelper.insertMS2CustAddr(listAddressPropColla);
    // String _message = await _submitDataPartial.submitColla(context, 6, null, _model, _listAddress);
    // return _message;
  }

  Future<bool> deleteSQLiteCollaProp() async{
    return await _dbHelper.deleteApplCollaProp();
  }

  Map get addressFromMap => _addressFromMap;

  set addressFromMap(Map value) {
    this._addressFromMap = value;
    notifyListeners();
  }

  void setLocationAddressByMap(BuildContext context) async{
    Map _result = await Navigator.push(context, MaterialPageRoute(builder: (context) => MapPage()));
    if(_result != null){
      _addressFromMap = _result;
      this._controllerAddressFromMap.text = _result["address"];
      notifyListeners();
    }
  }

  void formatting() {
    _controllerHargaJualShowroom.text = formatCurrency.formatCurrency(_controllerHargaJualShowroom.text);
    _controllerMPAdira.text = formatCurrency.formatCurrency(_controllerMPAdira.text);
    _controllerHargaPasar.text = formatCurrency.formatCurrency(_controllerHargaPasar.text);
    _controllerDPJaminan.text = formatCurrency.formatCurrency(_controllerDPJaminan.text);
    _controllerTaksasiPriceAutomotive.text = formatCurrency.formatCurrency(_controllerTaksasiPriceAutomotive.text);
    _controllerRekondisiFisik.text = formatCurrency.formatCurrency(_controllerRekondisiFisik.text);
    if(this._objectSelected != null) {
      if(this._objectSelected.id == "002" || this._objectSelected.id == "004"){
        if(this._controllerMPAdira.text == "0" || this._controllerMPAdira.text == "0.00"){
          this._controllerMPAdira.clear();
          try{
            showSnackBar("Nilai MP Adira tidak boleh 0");
          } catch(e){
            print("error snackbar ${e.toString()}");
          }
        }
        else{
          _controllerMPAdira.text = formatCurrency.formatCurrency(_controllerMPAdira.text);
        }
      }
      else{
        _controllerMPAdira.text = formatCurrency.formatCurrency(_controllerMPAdira.text);
      }
    }
    else{
      _controllerMPAdira.text = formatCurrency.formatCurrency(_controllerMPAdira.text);
    }
    // Property
    _controllerTaksasiPrice.text = formatCurrency.formatCurrency(_controllerTaksasiPrice.text);
    _controllerPHMax.text = formatCurrency.formatCurrency(_controllerPHMax.text);
    _controllerHargaTanah.text = formatCurrency.formatCurrency(_controllerHargaTanah.text);
    _controllerHargaNJOP.text = formatCurrency.formatCurrency(_controllerHargaNJOP.text);
    _controllerHargaBangunan.text = formatCurrency.formatCurrency(_controllerHargaBangunan.text);

    countPHMax();
    countTaksasi();
  }

  String get collateralID => _collateralID;

  set collateralID(String value) {
    this._collateralID = value;
  }

  Future<void> setDataCollaOtoFromSQLite(BuildContext context) async{
    debugPrint("COLLA JALAN");
    List _dataCollaOto = await _dbHelper.selectDataCollaOto();
    List _dataCollaProp = await _dbHelper.selectDataCollaProp();
    _listIdentity.clear();
    _listIdentity = IdentityType().lisIdentityModel;

    if(_dataCollaProp.isEmpty && _dataCollaOto.isEmpty){
      debugPrint("durable");
      var _providerObjectUnit = Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false);
      if(_providerObjectUnit.groupObjectSelected != null && _providerObjectUnit.objectSelected != null && Provider.of<InformationSalesmanChangeNotifier>(context, listen: false).listInfoSales.isNotEmpty){
        if(_providerObjectUnit.groupObjectSelected.KODE != "001" && _providerObjectUnit.groupObjectSelected.KODE != "002" && _providerObjectUnit.groupObjectSelected.KODE != "007"){
          this._collateralTypeModel = CollateralTypeModel("003", "DURABLE/TIDAK ADA");
          this._controllerCollateralType.text = "${this._collateralTypeModel.id} - ${this._collateralTypeModel.name}";
        }
      }
      // getTypeOfRoof(context,null);
    }
    else{
      debugPrint("oto atau prop");
      if(_dataCollaOto.isNotEmpty){
        print("cek data oto $_dataCollaOto");
        // print("set data sqlite colla oto");
        this._collateralTypeModel = CollateralTypeModel("001", "AUTOMOTIVE");
        this._controllerCollateralType.text = "${this._collateralTypeModel.id} - ${this._collateralTypeModel.name}";
        if(_dataCollaOto[0]['flag_coll_name_is_appl'] != "null" && _dataCollaOto[0]['flag_coll_name_is_appl'] != ""){
          this._radioValueIsCollaNameSameWithApplicantOto = _dataCollaOto[0]['flag_coll_name_is_appl']; // 0 = ya | 1 = tidak
        }
        this._radioValueIsCollateralSameWithUnitOto = _dataCollaOto[0]['flag_coll_is_unit'] != "null" ? _dataCollaOto[0]['flag_coll_is_unit'] : 1; // 0 = ya | 1 = tidak
        this._radioValueWorthyOrUnworthy = _dataCollaOto[0]['result'];
        if(this._radioValueIsCollaNameSameWithApplicantOto == 0){
          sameWithApplicantAutomotive(context);
        }
        else {
          for(int i = 0 ; i < listIdentityType.length; i++) {
            if(listIdentityType[i].id == _dataCollaOto[0]['id_type']) {
              this._identityTypeSelectedAuto = listIdentityType[i];
            }
          }
          this._controllerIdentityNumberAuto.text = _dataCollaOto[0]['id_no'];
          this._controllerNameOnCollateralAuto.text = _dataCollaOto[0]['colla_name'];
          this._initialDateForBirthDateAuto = _dataCollaOto[0]['date_of_birth'] != "null" && _dataCollaOto[0]['date_of_birth'] != "" ? DateTime.parse(_dataCollaOto[0]['date_of_birth']) : DateTime(dateNow.year, dateNow.month, dateNow.day);
          this._controllerBirthDateAuto.text = _dataCollaOto[0]['date_of_birth'] != "null" && _dataCollaOto[0]['date_of_birth'] != "" ? dateFormat.format(DateTime.parse(_dataCollaOto[0]['date_of_birth'])) : "";
          this._controllerBirthPlaceValidWithIdentityAuto.text = _dataCollaOto[0]['place_of_birth'];
          if(_dataCollaOto[0]['place_of_birth_kabkota'] != "" && _dataCollaOto[0]['place_of_birth_kabkota'] != "null"){
            this._birthPlaceAutoSelected = BirthPlaceModel(_dataCollaOto[0]['place_of_birth_kabkota'], _dataCollaOto[0]['place_of_birth_kabkota_desc']);
            this._controllerBirthPlaceValidWithIdentityLOVAuto.text = "${_dataCollaOto[0]['place_of_birth_kabkota']} - ${_dataCollaOto[0]['place_of_birth_kabkota_desc']}";
          }
        }
        if(this._radioValueIsCollateralSameWithUnitOto == 0){
          setSameWithApplicantAutomotive(context);
        }
        else {
          this._groupObjectSelected = GroupObjectModel(_dataCollaOto[0]['group_object'], _dataCollaOto[0]['group_object_desc']);
          this._controllerGroupObject.text = _dataCollaOto[0]['group_object'] != "" && _dataCollaOto[0]['group_object'] != "null" ? "${groupObjectSelected.KODE} - ${groupObjectSelected.DESKRIPSI}" : "";

          this._objectSelected = ObjectModel(_dataCollaOto[0]['object'], _dataCollaOto[0]['object_desc']);
          this._controllerObject.text = _dataCollaOto[0]['object'] != "" && _dataCollaOto[0]['object'] != "null" ? "${objectSelected.id} - ${objectSelected.name}" : "";

          this._productTypeSelected = ProductTypeModel(_dataCollaOto[0]['product_type'], _dataCollaOto[0]['product_type_desc']);
          this._controllerTypeProduct.text = _dataCollaOto[0]['product_type'] != "" && _dataCollaOto[0]['product_type'] != "null" ? "${productTypeSelected.id} - ${productTypeSelected.name}": "";

          this._brandObjectSelected = BrandObjectModel(_dataCollaOto[0]['brand_object'], _dataCollaOto[0]['brand_object_desc']);
          this._controllerBrandObject.text = _dataCollaOto[0]['brand_object'] != "" && _dataCollaOto[0]['brand_object'] != "null" ? "${brandObjectSelected.id} - ${brandObjectSelected.name}" : "";

          this._modelObjectSelected = ModelObjectModel(_dataCollaOto[0]['object_model'], _dataCollaOto[0]['object_model_desc']);
          this._controllerModelObject.text = _dataCollaOto[0]['object_model'] != "" && _dataCollaOto[0]['object_model'] != "null" ? "${modelObjectSelected.id} - ${modelObjectSelected.name}" : "";

          // field pemakaian objek
          this._objectUsageSelected = ObjectUsageModel(_dataCollaOto[0]['object_purpose'], _dataCollaOto[0]['object_purpose_desc']); // ke hide
          this._controllerUsageObjectModel.text = _dataCollaOto[0]['object_purpose'] != "" && _dataCollaOto[0]['object_purpose'] != "null" ? "${objectUsageSelected.id} - ${objectUsageSelected.name}" : ""; // ke hide

          this._objectTypeSelected = ObjectTypeModel(_dataCollaOto[0]['object_type'], _dataCollaOto[0]['object_type_desc']);
          this._controllerObjectType.text = _dataCollaOto[0]['object_type'] != "" && _dataCollaOto[0]['object_type'] != "null" ? "${objectTypeSelected.id} - ${objectTypeSelected.name}" : "";

          // field tujuan penggunaan colla
          this._collateralUsageOtoSelected = CollateralUsageModel(_dataCollaOto[0]['cola_purpose'], _dataCollaOto[0]['cola_purpose_desc']);
          this._controllerUsageCollateralOto.text = _dataCollaOto[0]['cola_purpose'] != "" && _dataCollaOto[0]['cola_purpose'] != "null" ? "${_collateralUsageOtoSelected.id} - ${_collateralUsageOtoSelected.name}" : "";
        }
        _addYearProductionAndRegistration();
        for(int i=0; i < this._listRegistrationYear.length; i++){
          if(this._listRegistrationYear[i] == _dataCollaOto[0]['registration_year'].toString()){
            this._yearRegistrationSelected = this._listRegistrationYear[i];
          }
        }
        for(int i=0; i < this._listProductionYear.length; i++){
          if(this._listProductionYear[i] == _dataCollaOto[0]['mfg_year'].toString()){
            this._yearProductionSelected = this._listProductionYear[i];
          }
        }
        this._radioValueYellowPlat = _dataCollaOto[0]['yellow_plate'];
        this._radioValueBuiltUpNonATPM = _dataCollaOto[0]['built_up'];
        this._controllerPoliceNumber.text = _dataCollaOto[0]['police_no'];
        this._controllerFrameNumber.text = _dataCollaOto[0]['frame_no'];
        this._controllerMachineNumber.text = _dataCollaOto[0]['engine_no'];
        this._controllerBPKPNumber.text = _dataCollaOto[0]['bpkp_no'];
        this._controllerGradeUnit.text = _dataCollaOto[0]['grade_unit'];
        this._controllerFasilitasUTJ.text = _dataCollaOto[0]['utj_facilities'];
        this._controllerNamaBidder.text = _dataCollaOto[0]['bidder_name'];
        this._controllerHargaJualShowroom.text = _dataCollaOto[0]['showroom_price'].toString() != "" && _dataCollaOto[0]['showroom_price'].toString() != "null" ? _dataCollaOto[0]['showroom_price'].toString() : '0.00';
        this._controllerPerlengkapanTambahan.text = _dataCollaOto[0]['add_equip'];
        this._controllerMPAdira.text = _dataCollaOto[0]['mp_adira'] != "" && _dataCollaOto[0]['mp_adira'] != "null" ? _dataCollaOto[0]['mp_adira'].toString() : '';
        this._controllerMPAdiraUpload.text = _dataCollaOto[0]['mp_adira_upld'] != "" && _dataCollaOto[0]['mp_adira_upld'] != "null" ? _dataCollaOto[0]['mp_adira_upld'].toString() : '0.00';
        this._controllerNamaBidder.text = _dataCollaOto[0]['bidder_name'];
        // this._controllerHargaPasar.text = _dataCollaOto[0][''];
        this._controllerRekondisiFisik.text = _dataCollaOto[0]['rekondisi_fisik'] != "" && _dataCollaOto[0]['rekondisi_fisik']!= "null" ? _dataCollaOto[0]['rekondisi_fisik'].toString() : '';
        this._controllerDPJaminan.text = _dataCollaOto[0]['dp_jaminan'] != "" && _dataCollaOto[0]['dp_jaminan'] != "null" ? _dataCollaOto[0]['dp_jaminan'].toString() : '';
        this._controllerPHMaxAutomotive.text = _dataCollaOto[0]['max_ph'].toString() != "" && _dataCollaOto[0]['max_ph'].toString() != "null" ? _dataCollaOto[0]['max_ph'].toString() : '0.00';
        this._controllerTaksasiPriceAutomotive.text = _dataCollaOto[0]['max_ph'].toString() != "" && _dataCollaOto[0]['taksasi_price'].toString() != "null" ? _dataCollaOto[0]['taksasi_price'].toString() : '0.00';
        this._radioValueWorthyOrUnworthy = _dataCollaOto[0]['result'];
        this._collateralUsageOtoSelected = CollateralUsageModel(_dataCollaOto[0]['cola_purpose'], _dataCollaOto[0]['cola_purpose_desc']);
        this._controllerUsageCollateralOto.text = "${this._collateralUsageOtoSelected.id} - ${this._collateralUsageOtoSelected.name}";
        collateralID = _dataCollaOto[0]['collateralID'];
        //set data lookuputj
        Provider.of<FormMLookupUTJChangeNotif>(context,listen: false).controllerPoliceNumber.text = this._controllerPoliceNumber.text;
        Provider.of<FormMLookupUTJChangeNotif>(context,listen: false).controllerFrameNumber.text = this._controllerFrameNumber.text;
        Provider.of<FormMLookupUTJChangeNotif>(context,listen: false).controllerMachineNumber.text = this._controllerMachineNumber.text;
        Provider.of<FormMLookupUTJChangeNotif>(context,listen: false).controllerBPKPNumber.text = this._controllerBPKPNumber.text;
        // getMPAdiraUpload(context);
      }
      else{
        getTypeOfRoof(context,_dataCollaProp);
      }
    }
    formatting();
    checkDataDakor();
    Provider.of<TaksasiUnitChangeNotifier>(context,listen: false).setIsVisible(context);
    notifyListeners();
  }

  void _setValueFromSQLiteProp(BuildContext context,List dataCollaProp) async{
    // print("set data sqlite colla prop $dataCollaProp");
    this._collateralTypeModel = CollateralTypeModel("002", "PROPERTY");
    this._controllerCollateralType.text = "${this._collateralTypeModel.id} - ${this._collateralTypeModel.name}";
    this._radioValueIsCollateralSameWithApplicantProperty = dataCollaProp[0]['flag_coll_name_is_appl'];
    sameWithApplicantProperty(context);
    this._birthPlaceSelected = BirthPlaceModel(dataCollaProp[0]['place_of_birth_kabkota'], dataCollaProp[0]['place_of_birth_kabkota_desc']);
    this._controllerBirthPlaceValidWithIdentityLOV.text = "${this._birthPlaceSelected.KABKOT_ID} - ${this._birthPlaceSelected.KABKOT_NAME}";
    this._controllerCertificateNumber.text = dataCollaProp[0]['sertifikat_no'];
    for(int i=0; i < _listCertificateType.length; i++){
      if(dataCollaProp[0]['seritifikat_type'] == _listCertificateType[i].id){
        this._certificateTypeSelected = _listCertificateType[i];
      }
    }
    for(int i=0; i < _listPropertyType.length; i++){
      if(dataCollaProp[0]['property_type'] == _listPropertyType[i].id){
        this._propertyTypeSelected = _listPropertyType[i];
      }
    }
    this._controllerBuildingArea.text = dataCollaProp[0]['building_area'].toString();
    this._controllerSurfaceArea.text = dataCollaProp[0]['land_area'].toString();
    this._controllerTaksasiPrice.text = dataCollaProp[0]['taksasi_price'].toString();
    this._controllerSifatJaminan.text = dataCollaProp[0]['guarantee_character'];
    this._controllerBuktiKepemilikan.text = dataCollaProp[0]['grntr_evdnce_ownr'];
    this._controllerCertificateReleaseDate.text = dataCollaProp[0]['pblsh_sertifikat_date'];
    this._controllerCertificateReleaseYear.text = dataCollaProp[0]['pblsh_sertifikat_year'].toString();
    this._controllerNamaPemegangHak.text = dataCollaProp[0]['license_name'];
    this._controllerNoSuratUkur.text = dataCollaProp[0]['no_srt_ukur'];
    this._controllerDateOfMeasuringLetter.text = dataCollaProp[0]['tgl_srt_ukur'];
    this._controllerCertificateOfMeasuringLetter.text = dataCollaProp[0]['srtfkt_pblsh_by'];
    this._controllerDPJaminan.text = dataCollaProp[0]['dp_guarantee'].toString();
    this._controllerPHMax.text = dataCollaProp[0]['max_ph'].toString();
    this._controllerJarakFasumPositif.text = dataCollaProp[0]['jarak_fasum_positif'].toString();
    this._controllerJarakFasumNegatif.text = dataCollaProp[0]['jarak_fasum_negatif'].toString();
    this._controllerHargaTanah.text = dataCollaProp[0]['land_price'].toString();
    this._controllerHargaNJOP.text = dataCollaProp[0]['njop_price'].toString();
    this._controllerHargaBangunan.text = dataCollaProp[0]['building_price'].toString();
    for(int i=0; i < _listTypeOfRoof.length; i++){
      if(dataCollaProp[0]['roof_type'] == _listTypeOfRoof[i].id){
        _typeOfRoofSelected = _listTypeOfRoof[i];
      }
    }
    for(int i=0; i < _listWallType.length; i++){
      if(dataCollaProp[0]['wall_type'] == _listWallType[i].id){
        _wallTypeSelected = _listWallType[i];
      }
    }
    for(int i=0; i < _listFloorType.length; i++){
      if(dataCollaProp[0]['floor_type'] == _listFloorType[i].id){
        _floorTypeSelected = _listFloorType[i];
      }
    }
    for(int i=0; i < _listFoundationType.length; i++){
      if(dataCollaProp[0]['foundation_type'] == _listFoundationType[i].id){
        _foundationTypeSelected = _listFoundationType[i];
      }
    }
    for(int i=0; i < _listStreetType.length; i++){
      if(dataCollaProp[0]['road_type'] == _listStreetType[i].id){
        this._streetTypeSelected = _listStreetType[i];
      }
    }
    collateralID = dataCollaProp[0]['collateralID'];
    this._radioValueAccessCar = dataCollaProp[0]['flag_car_pas'];
    this._controllerJumlahRumahDalamRadius.text = dataCollaProp[0]['no_of_house'].toString();
    this._controllerMasaHakBerlaku.text = dataCollaProp[0]['expire_right'];
    this._controllerNoIMB.text = dataCollaProp[0]['imb_no'];
    this._controllerDateOfIMB.text = dataCollaProp[0]['imb_date'];
    this._controllerLuasBangunanIMB.text = dataCollaProp[0]['size_of_imb'].toString();
    this._collateralUsageOtoSelected = CollateralUsageModel(dataCollaProp[0]['cola_purpose'], dataCollaProp[0]['cola_purpose_desc']);
    this._controllerUsageCollateralProperty.text = "${this._collateralUsageOtoSelected.id} - ${this._collateralUsageOtoSelected.name}";
    this._controllerLTV.text = dataCollaProp[0]['ltv'].toString();

    //alamat
    var _check = await _dbHelper.selectMS2CustAddr("6");
    for(int i=0; i<_check.length; i++){
      var jenisAlamatModel = JenisAlamatModel(_check[i]['addr_type'], _check[i]['addr_desc']);
      var kelurahanModel = KelurahanModel(
          _check[i]['kelurahan'],
          _check[i]['kelurahan_desc'],
          _check[i]['kecamatan_desc'],
          _check[i]['kabkot_desc'],
          _check[i]['provinsi_desc'],
          _check[i]['zip_code'],
          _check[i]['kecamatan'],
          _check[i]['kabkot'],
          _check[i]['provinsi']
      );
      var _addressMap = {
        "address": _check[i]['address_from_map'] != "" && _check[i]['address_from_map'] != "null" ? _check[i]['address_from_map'] : '',
        "latitude": _check[i]['latitude'],
        "longitude": _check[i]['longitude']
      };
      this._addressID = _check[i]['addressID'];
      this._foreignBusinessID = _check[i]['foreignBusinessID'];
      this._addressTypeSelected = jenisAlamatModel;
      this._controllerAddress.text = _check[i]['address'] != "" && _check[i]['address'] != "null" ? _check[i]['address'] : '';
      this._controllerRT.text = _check[i]['rt'] != "" && _check[i]['rt'] != "null" ? _check[i]['rt'] : '';
      this._controllerRW.text = _check[i]['rw'] != "" && _check[i]['rw'] != "null" ? _check[i]['rw'] : '';
      this._kelurahanModel = kelurahanModel;
      this._controllerKelurahan.text = kelurahanModel.KEL_NAME;
      this._controllerKecamatan.text = kelurahanModel.KEC_NAME;
      this._controllerKota.text = kelurahanModel.KABKOT_NAME;
      this._controllerProv.text = kelurahanModel.PROV_NAME;
      this._controllerPostalCode.text = kelurahanModel.ZIPCODE;
      this._addressFromMap = _addressMap;
    }
  }

  Future<void> setPreference(BuildContext context, String object) async {
    getDataFromDashboard(context);
    var _providerFoto = Provider.of<FormMFotoChangeNotifier>(context,listen: false);
    var _providerInfoApp = Provider.of<InfoAppChangeNotifier>(context,listen: false);
    var _unitObject = Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false);
    SharedPreferences _preference = await SharedPreferences.getInstance();
    this._custType = _preference.getString("cust_type");
    this._lastKnownState = _preference.getString("last_known_state");
    //cek unit bekas
    if(this._custType != "PER"){
      this._listIdentity.clear();
      this._listIdentity.add(IdentityModel("999", "NPWP"));
      this._identityTypeSelectedAuto = this._listIdentity[0];
    }
    if(_unitObject.groupObjectSelected.KODE == "001" || _unitObject.groupObjectSelected.KODE == "002"){
      this._radioValueIsCollateralSameWithUnitOto = 0;
      setSameWithApplicantAutomotive(context);
    } else {
      this._radioValueIsCollateralSameWithUnitOto = 1;
    }
    // set data field untuk semua unit
    // ya = false = 0 | tidak = true = 1
    if(this._custType == "PER") {
      if(_providerFoto.jenisKonsepSelected != null) {
        if(_providerFoto.jenisKonsepSelected.id == "02") {
          this._radioValueForAllUnitOto = 0;
        } else {
          this._radioValueForAllUnitOto = 1;
        }
      }
    } else {
      if(_providerInfoApp.conceptTypeModelSelected != null) {
        if(_providerInfoApp.conceptTypeModelSelected.id == "02") {
          this._radioValueForAllUnitOto = 0;
        } else {
          this._radioValueForAllUnitOto = 1;
        }
      }
    }
    // if(object == "001" || object == "003"){
    //   this._controllerGradeUnit.text = "-";
    //   this._controllerFasilitasUTJ.text = "-";
    //   this._controllerNamaBidder.text = "- / 0.00 / 0.00";
    //   if(this._controllerHargaJualShowroom.text.isEmpty){
    //     this._controllerHargaJualShowroom.text = "0.00";
    //   }
    //   if(this._controllerMPAdiraUpload.text.isEmpty){
    //     this._controllerMPAdiraUpload.text = "0.00";
    //   }
    //   if(this._controllerPHMaxAutomotive.text.isEmpty){
    //     this._controllerPHMaxAutomotive.text = "0.00";
    //   }
    //   if(this._controllerTaksasiPriceAutomotive.text.isEmpty){
    //     this._controllerTaksasiPriceAutomotive.text = "0.00";
    //   }
    // }
    this._isDisablePACIAAOSCONA = false;
    if(this._lastKnownState == "AOS" || this._lastKnownState == "CONA" || this._lastKnownState == "IA" || this._lastKnownState == "PAC") {
      this._isDisablePACIAAOSCONA = true;
    }
    this._disableJenisPenawaran = false;
    if(_preference.getString("jenis_penawaran") == "002"){
      this._disableJenisPenawaran = true;
    }
  }

  // Inf Colla Oto
  ShowMandatoryInfoCollaOtoModel _showMandatoryInfoCollaOtoIdeModel;
  ShowMandatoryInfoCollaOtoModel _showMandatoryInfoCollaOtoIdeCompanyModel;
  void showMandatoryInfoCollaOtoIdeModel(BuildContext context){
    _showMandatoryInfoCollaOtoIdeModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoCollaOtoIdeModel;
    _showMandatoryInfoCollaOtoIdeCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoCollaOtoIdeCompanyModel;
  }

  ShowMandatoryInfoCollaOtoModel _showMandatoryInfoCollaOtoRegulerSurveyModel;
  ShowMandatoryInfoCollaOtoModel _showMandatoryInfoCollaOtoRegulerSurveyCompanyModel;
  void showMandatoryInfoCollaOtoRegulerSurveyModel(BuildContext context){
    _showMandatoryInfoCollaOtoRegulerSurveyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoCollaOtoRegulerSurveyModel;
    _showMandatoryInfoCollaOtoRegulerSurveyCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoCollaOtoRegulerSurveyCompanyModel;
  }

  ShowMandatoryInfoCollaOtoModel _showMandatoryInfoCollaOtoPacModel;
  ShowMandatoryInfoCollaOtoModel _showMandatoryInfoCollaOtoPacCompanyModel;
  void showMandatoryInfoCollaOtoPacModel(BuildContext context){
    _showMandatoryInfoCollaOtoPacModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoCollaOtoPacModel;
    _showMandatoryInfoCollaOtoPacCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoCollaOtoPacCompanyModel;
  }

  ShowMandatoryInfoCollaOtoModel _showMandatoryInfoCollaOtoResurveyModel;
  ShowMandatoryInfoCollaOtoModel _showMandatoryInfoCollaOtoResurveyCompanyModel;
  void showMandatoryInfoCollaOtoResurveyModel(BuildContext context){
    _showMandatoryInfoCollaOtoResurveyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoCollaOtoResurveyModel;
    _showMandatoryInfoCollaOtoResurveyCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoCollaOtoResurveyCompanyModel;
  }

  ShowMandatoryInfoCollaOtoModel _showMandatoryInfoCollaOtoDakorModel;
  ShowMandatoryInfoCollaOtoModel _showMandatoryInfoCollaOtoDakorCompanyModel;
  void showMandatoryInfoCollaOtoDakorModel(BuildContext context){
    _showMandatoryInfoCollaOtoDakorModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoCollaOtoDakorModel;
    _showMandatoryInfoCollaOtoDakorCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoCollaOtoDakorCompanyModel;
  }

  void getDataFromDashboard(BuildContext context){
    showMandatoryInfoCollaOtoIdeModel(context);
    showMandatoryInfoCollaOtoRegulerSurveyModel(context);
    showMandatoryInfoCollaOtoPacModel(context);
    showMandatoryInfoCollaOtoResurveyModel(context);
    showMandatoryInfoCollaOtoDakorModel(context);

    showMandatoryInfoCollaPropIdeModel(context);
    showMandatoryInfoCollaPropRegulerSurveyModel(context);
    showMandatoryInfoCollaPropPacModel(context);
    showMandatoryInfoCollaPropResurveyModel(context);
    showMandatoryInfoCollaPropDakorModel(context);
  }

  // Nama Jaminan = Pemohon
  bool isRadioValueIsCollaNameSameWithApplicantOtoVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeModel.isRadioValueIsCollaNameSameWithApplicantOtoVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyModel.isRadioValueIsCollaNameSameWithApplicantOtoVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacModel.isRadioValueIsCollaNameSameWithApplicantOtoVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyModel.isRadioValueIsCollaNameSameWithApplicantOtoVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorModel.isRadioValueIsCollaNameSameWithApplicantOtoVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeCompanyModel.isRadioValueIsCollaNameSameWithApplicantOtoVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyCompanyModel.isRadioValueIsCollaNameSameWithApplicantOtoVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacCompanyModel.isRadioValueIsCollaNameSameWithApplicantOtoVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyCompanyModel.isRadioValueIsCollaNameSameWithApplicantOtoVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorCompanyModel.isRadioValueIsCollaNameSameWithApplicantOtoVisible;
      }
    }
    return value;
  }

// Jenis Identitas
  bool isIdentityTypeSelectedAutoVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeModel.isIdentityTypeSelectedAutoVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyModel.isIdentityTypeSelectedAutoVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacModel.isIdentityTypeSelectedAutoVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyModel.isIdentityTypeSelectedAutoVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorModel.isIdentityTypeSelectedAutoVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeCompanyModel.isIdentityTypeSelectedAutoVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyCompanyModel.isIdentityTypeSelectedAutoVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacCompanyModel.isIdentityTypeSelectedAutoVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyCompanyModel.isIdentityTypeSelectedAutoVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorCompanyModel.isIdentityTypeSelectedAutoVisible;
      }
    }
    return value;
  }

// No Identitas
  bool isIdentityNumberAutoVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeModel.isIdentityNumberAutoVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyModel.isIdentityNumberAutoVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacModel.isIdentityNumberAutoVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyModel.isIdentityNumberAutoVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorModel.isIdentityNumberAutoVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeCompanyModel.isIdentityNumberAutoVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyCompanyModel.isIdentityNumberAutoVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacCompanyModel.isIdentityNumberAutoVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyCompanyModel.isIdentityNumberAutoVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorCompanyModel.isIdentityNumberAutoVisible;
      }
    }
    return value;
  }

// Nama Pada Jaminan
  bool isNameOnCollateralAutoVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeModel.isNameOnCollateralAutoVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyModel.isNameOnCollateralAutoVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacModel.isNameOnCollateralAutoVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyModel.isNameOnCollateralAutoVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorModel.isNameOnCollateralAutoVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeCompanyModel.isNameOnCollateralAutoVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyCompanyModel.isNameOnCollateralAutoVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacCompanyModel.isNameOnCollateralAutoVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyCompanyModel.isNameOnCollateralAutoVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorCompanyModel.isNameOnCollateralAutoVisible;
      }
    }
    return value;
  }

// Tanggal Lahir
  bool isBirthDateAutoVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeModel.isBirthDateAutoVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyModel.isBirthDateAutoVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacModel.isBirthDateAutoVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyModel.isBirthDateAutoVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorModel.isBirthDateAutoVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeCompanyModel.isBirthDateAutoVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyCompanyModel.isBirthDateAutoVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacCompanyModel.isBirthDateAutoVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyCompanyModel.isBirthDateAutoVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorCompanyModel.isBirthDateAutoVisible;
      }
    }
    return value;
  }

// Tempat Lahir Sesuai Identitas
  bool isBirthPlaceValidWithIdentityAutoVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeModel.isBirthPlaceValidWithIdentityAutoVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyModel.isBirthPlaceValidWithIdentityAutoVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacModel.isBirthPlaceValidWithIdentityAutoVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyModel.isBirthPlaceValidWithIdentityAutoVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorModel.isBirthPlaceValidWithIdentityAutoVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeCompanyModel.isBirthPlaceValidWithIdentityAutoVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyCompanyModel.isBirthPlaceValidWithIdentityAutoVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacCompanyModel.isBirthPlaceValidWithIdentityAutoVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyCompanyModel.isBirthPlaceValidWithIdentityAutoVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorCompanyModel.isBirthPlaceValidWithIdentityAutoVisible;
      }
    }
    return value;
  }

// Tempat Lahir Sesuai Identitas LOV
  bool isBirthPlaceValidWithIdentityLOVAutoVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeModel.isBirthPlaceValidWithIdentityLOVAutoVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyModel.isBirthPlaceValidWithIdentityLOVAutoVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacModel.isBirthPlaceValidWithIdentityLOVAutoVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyModel.isBirthPlaceValidWithIdentityLOVAutoVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorModel.isBirthPlaceValidWithIdentityLOVAutoVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeCompanyModel.isBirthPlaceValidWithIdentityLOVAutoVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyCompanyModel.isBirthPlaceValidWithIdentityLOVAutoVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacCompanyModel.isBirthPlaceValidWithIdentityLOVAutoVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyCompanyModel.isBirthPlaceValidWithIdentityLOVAutoVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorCompanyModel.isBirthPlaceValidWithIdentityLOVAutoVisible;
      }
    }
    return value;
  }

// Collateral = Unit
  bool isRadioValueIsCollateralSameWithUnitOtoVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeModel.isRadioValueIsCollateralSameWithUnitOtoVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyModel.isRadioValueIsCollateralSameWithUnitOtoVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacModel.isRadioValueIsCollateralSameWithUnitOtoVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyModel.isRadioValueIsCollateralSameWithUnitOtoVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorModel.isRadioValueIsCollateralSameWithUnitOtoVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeCompanyModel.isRadioValueIsCollateralSameWithUnitOtoVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyCompanyModel.isRadioValueIsCollateralSameWithUnitOtoVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacCompanyModel.isRadioValueIsCollateralSameWithUnitOtoVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyCompanyModel.isRadioValueIsCollateralSameWithUnitOtoVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorCompanyModel.isRadioValueIsCollateralSameWithUnitOtoVisible;
      }
    }
    return value;
  }

// Group Object
  bool isGroupObjectVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeModel.isGroupObjectVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyModel.isGroupObjectVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacModel.isGroupObjectVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyModel.isGroupObjectVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorModel.isGroupObjectVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeCompanyModel.isGroupObjectVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyCompanyModel.isGroupObjectVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacCompanyModel.isGroupObjectVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyCompanyModel.isGroupObjectVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorCompanyModel.isGroupObjectVisible;
      }
    }
    return value;
  }

// Object
  bool isObjectVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeModel.isObjectVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyModel.isObjectVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacModel.isObjectVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyModel.isObjectVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorModel.isObjectVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeCompanyModel.isObjectVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyCompanyModel.isObjectVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacCompanyModel.isObjectVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyCompanyModel.isObjectVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorCompanyModel.isObjectVisible;
      }
    }
    return value;
  }

// Jenis Produk
  bool isTypeProductVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeModel.isTypeProductVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyModel.isTypeProductVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacModel.isTypeProductVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyModel.isTypeProductVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorModel.isTypeProductVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeCompanyModel.isTypeProductVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyCompanyModel.isTypeProductVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacCompanyModel.isTypeProductVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyCompanyModel.isTypeProductVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorCompanyModel.isTypeProductVisible;
      }
    }
    return value;
  }

// Merk Produk
  bool isBrandObjectVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeModel.isBrandObjectVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyModel.isBrandObjectVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacModel.isBrandObjectVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyModel.isBrandObjectVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorModel.isBrandObjectVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeCompanyModel.isBrandObjectVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyCompanyModel.isBrandObjectVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacCompanyModel.isBrandObjectVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyCompanyModel.isBrandObjectVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorCompanyModel.isBrandObjectVisible;
      }
    }
    return value;
  }

// Jenis Objek
  bool isObjectTypeVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeModel.isObjectTypeVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyModel.isObjectTypeVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacModel.isObjectTypeVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyModel.isObjectTypeVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorModel.isObjectTypeVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeCompanyModel.isObjectTypeVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyCompanyModel.isObjectTypeVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacCompanyModel.isObjectTypeVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyCompanyModel.isObjectTypeVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorCompanyModel.isObjectTypeVisible;
      }
    }
    return value;
  }

// Model Objek
  bool isModelObjectVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeModel.isModelObjectVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyModel.isModelObjectVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacModel.isModelObjectVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyModel.isModelObjectVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorModel.isModelObjectVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeCompanyModel.isModelObjectVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyCompanyModel.isModelObjectVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacCompanyModel.isModelObjectVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyCompanyModel.isModelObjectVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorCompanyModel.isModelObjectVisible;
      }
    }
    return value;
  }

// Pemakaian Objek
  bool isUsageObjectModelVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeModel.isUsageObjectModelVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyModel.isUsageObjectModelVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacModel.isUsageObjectModelVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyModel.isUsageObjectModelVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorModel.isUsageObjectModelVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeCompanyModel.isUsageObjectModelVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyCompanyModel.isUsageObjectModelVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacCompanyModel.isUsageObjectModelVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyCompanyModel.isUsageObjectModelVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorCompanyModel.isUsageObjectModelVisible;
      }
    }
    return value;
  }

// Tahun Pembuatan
  bool isYearProductionSelectedVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeModel.isYearProductionSelectedVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyModel.isYearProductionSelectedVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacModel.isYearProductionSelectedVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyModel.isYearProductionSelectedVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorModel.isYearProductionSelectedVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeCompanyModel.isYearProductionSelectedVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyCompanyModel.isYearProductionSelectedVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacCompanyModel.isYearProductionSelectedVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyCompanyModel.isYearProductionSelectedVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorCompanyModel.isYearProductionSelectedVisible;
      }
    }
    return value;
  }

// Tahun Registrasi
  bool isYearRegistrationSelectedVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeModel.isYearRegistrationSelectedVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyModel.isYearRegistrationSelectedVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacModel.isYearRegistrationSelectedVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyModel.isYearRegistrationSelectedVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorModel.isYearRegistrationSelectedVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeCompanyModel.isYearRegistrationSelectedVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyCompanyModel.isYearRegistrationSelectedVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacCompanyModel.isYearRegistrationSelectedVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyCompanyModel.isYearRegistrationSelectedVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorCompanyModel.isYearRegistrationSelectedVisible;
      }
    }
    return value;
  }

// Plat Kuning
  bool isRadioValueYellowPlatVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeModel.isRadioValueYellowPlatVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyModel.isRadioValueYellowPlatVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacModel.isRadioValueYellowPlatVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyModel.isRadioValueYellowPlatVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorModel.isRadioValueYellowPlatVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeCompanyModel.isRadioValueYellowPlatVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyCompanyModel.isRadioValueYellowPlatVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacCompanyModel.isRadioValueYellowPlatVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyCompanyModel.isRadioValueYellowPlatVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorCompanyModel.isRadioValueYellowPlatVisible;
      }
    }
    return value;
  }

// Built Up Non ATPM
  bool isRadioValueBuiltUpNonATPMVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeModel.isRadioValueBuiltUpNonATPMVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyModel.isRadioValueBuiltUpNonATPMVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacModel.isRadioValueBuiltUpNonATPMVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyModel.isRadioValueBuiltUpNonATPMVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorModel.isRadioValueBuiltUpNonATPMVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeCompanyModel.isRadioValueBuiltUpNonATPMVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyCompanyModel.isRadioValueBuiltUpNonATPMVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacCompanyModel.isRadioValueBuiltUpNonATPMVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyCompanyModel.isRadioValueBuiltUpNonATPMVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorCompanyModel.isRadioValueBuiltUpNonATPMVisible;
      }
    }
    return value;
  }

// No Polisi
  bool isPoliceNumberVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeModel.isPoliceNumberVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyModel.isPoliceNumberVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacModel.isPoliceNumberVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyModel.isPoliceNumberVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorModel.isPoliceNumberVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeCompanyModel.isPoliceNumberVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyCompanyModel.isPoliceNumberVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacCompanyModel.isPoliceNumberVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyCompanyModel.isPoliceNumberVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorCompanyModel.isPoliceNumberVisible;
      }
    }
    return value;
  }

// No Rangka
  bool isFrameNumberVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeModel.isFrameNumberVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyModel.isFrameNumberVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacModel.isFrameNumberVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyModel.isFrameNumberVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorModel.isFrameNumberVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeCompanyModel.isFrameNumberVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyCompanyModel.isFrameNumberVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacCompanyModel.isFrameNumberVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyCompanyModel.isFrameNumberVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorCompanyModel.isFrameNumberVisible;
      }
    }
    return value;
  }

// No Mesin
  bool isMachineNumberVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeModel.isMachineNumberVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyModel.isMachineNumberVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacModel.isMachineNumberVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyModel.isMachineNumberVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorModel.isMachineNumberVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeCompanyModel.isMachineNumberVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyCompanyModel.isMachineNumberVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacCompanyModel.isMachineNumberVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyCompanyModel.isMachineNumberVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorCompanyModel.isMachineNumberVisible;
      }
    }
    return value;
  }

// No BPKB
  bool isBPKPNumberVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeModel.isBPKPNumberVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyModel.isBPKPNumberVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacModel.isBPKPNumberVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyModel.isBPKPNumberVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorModel.isBPKPNumberVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeCompanyModel.isBPKPNumberVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyCompanyModel.isBPKPNumberVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacCompanyModel.isBPKPNumberVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyCompanyModel.isBPKPNumberVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorCompanyModel.isBPKPNumberVisible;
      }
    }
    return value;
  }

// Grade Unit
  bool isGradeUnitVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeModel.isGradeUnitVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyModel.isGradeUnitVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacModel.isGradeUnitVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyModel.isGradeUnitVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorModel.isGradeUnitVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeCompanyModel.isGradeUnitVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyCompanyModel.isGradeUnitVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacCompanyModel.isGradeUnitVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyCompanyModel.isGradeUnitVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorCompanyModel.isGradeUnitVisible;
      }
    }
    return value;
  }

// Fasilitas UTJ
  bool isFasilitasUTJVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeModel.isFasilitasUTJVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyModel.isFasilitasUTJVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacModel.isFasilitasUTJVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyModel.isFasilitasUTJVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorModel.isFasilitasUTJVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeCompanyModel.isFasilitasUTJVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyCompanyModel.isFasilitasUTJVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacCompanyModel.isFasilitasUTJVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyCompanyModel.isFasilitasUTJVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorCompanyModel.isFasilitasUTJVisible;
      }
    }
    return value;
  }

// Nama Bidder
  bool isNamaBidderVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeModel.isNamaBidderVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyModel.isNamaBidderVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacModel.isNamaBidderVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyModel.isNamaBidderVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorModel.isNamaBidderVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeCompanyModel.isNamaBidderVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyCompanyModel.isNamaBidderVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacCompanyModel.isNamaBidderVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyCompanyModel.isNamaBidderVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorCompanyModel.isNamaBidderVisible;
      }
    }
    return value;
  }

// Harga Jual Showroom
  bool isHargaJualShowroomVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeModel.isHargaJualShowroomVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyModel.isHargaJualShowroomVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacModel.isHargaJualShowroomVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyModel.isHargaJualShowroomVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorModel.isHargaJualShowroomVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeCompanyModel.isHargaJualShowroomVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyCompanyModel.isHargaJualShowroomVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacCompanyModel.isHargaJualShowroomVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyCompanyModel.isHargaJualShowroomVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorCompanyModel.isHargaJualShowroomVisible;
      }
    }
    return value;
  }

// Perlengkapan Tambahan
  bool isPerlengkapanTambahanVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeModel.isPerlengkapanTambahanVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyModel.isPerlengkapanTambahanVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacModel.isPerlengkapanTambahanVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyModel.isPerlengkapanTambahanVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorModel.isPerlengkapanTambahanVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeCompanyModel.isPerlengkapanTambahanVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyCompanyModel.isPerlengkapanTambahanVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacCompanyModel.isPerlengkapanTambahanVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyCompanyModel.isPerlengkapanTambahanVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorCompanyModel.isPerlengkapanTambahanVisible;
      }
    }
    return value;
  }

// MP Adira
  bool isMPAdiraVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeModel.isMPAdiraVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyModel.isMPAdiraVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacModel.isMPAdiraVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyModel.isMPAdiraVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorModel.isMPAdiraVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeCompanyModel.isMPAdiraVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyCompanyModel.isMPAdiraVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacCompanyModel.isMPAdiraVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyCompanyModel.isMPAdiraVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorCompanyModel.isMPAdiraVisible;
      }
    }
    return value;
  }

// MP Adira Upload
  bool isMPAdiraUploadVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeModel.isMPAdiraVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyModel.isMPAdiraVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacModel.isMPAdiraVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyModel.isMPAdiraVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorModel.isMPAdiraVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeCompanyModel.isMPAdiraVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyCompanyModel.isMPAdiraVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacCompanyModel.isMPAdiraVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyCompanyModel.isMPAdiraVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorCompanyModel.isMPAdiraVisible;
      }
    }
    return value;
  }

// Rekondisi Fisik
  bool isRekondisiFisikVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeModel.isRekondisiFisikVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyModel.isRekondisiFisikVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacModel.isRekondisiFisikVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyModel.isRekondisiFisikVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorModel.isRekondisiFisikVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeCompanyModel.isRekondisiFisikVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyCompanyModel.isRekondisiFisikVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacCompanyModel.isRekondisiFisikVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyCompanyModel.isRekondisiFisikVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorCompanyModel.isRekondisiFisikVisible;
      }
    }
    return value;
  }

// DP Jaminan
  bool isDpGuaranteeVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeModel.isDpGuaranteeVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyModel.isDpGuaranteeVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacModel.isDpGuaranteeVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyModel.isDpGuaranteeVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorModel.isDpGuaranteeVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeCompanyModel.isDpGuaranteeVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyCompanyModel.isDpGuaranteeVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacCompanyModel.isDpGuaranteeVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyCompanyModel.isDpGuaranteeVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorCompanyModel.isDpGuaranteeVisible;
      }
    }
    return value;
  }

// PH Maksimal
  bool isPHMaxAutomotiveVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeModel.isPHMaxAutomotiveVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyModel.isPHMaxAutomotiveVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacModel.isPHMaxAutomotiveVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyModel.isPHMaxAutomotiveVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorModel.isPHMaxAutomotiveVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeCompanyModel.isPHMaxAutomotiveVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyCompanyModel.isPHMaxAutomotiveVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacCompanyModel.isPHMaxAutomotiveVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyCompanyModel.isPHMaxAutomotiveVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorCompanyModel.isPHMaxAutomotiveVisible;
      }
    }
    return value;
  }

// Harga Taksasi
  bool isTaksasiPriceAutomotiveVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeModel.isTaksasiPriceAutomotiveVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyModel.isTaksasiPriceAutomotiveVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacModel.isTaksasiPriceAutomotiveVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyModel.isTaksasiPriceAutomotiveVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorModel.isTaksasiPriceAutomotiveVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeCompanyModel.isTaksasiPriceAutomotiveVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyCompanyModel.isTaksasiPriceAutomotiveVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacCompanyModel.isTaksasiPriceAutomotiveVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyCompanyModel.isTaksasiPriceAutomotiveVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorCompanyModel.isTaksasiPriceAutomotiveVisible;
      }
    }
    return value;
  }

// Kesimpulan
  bool isRadioValueWorthyOrUnworthyVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeModel.isRadioValueWorthyOrUnworthyVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyModel.isRadioValueWorthyOrUnworthyVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacModel.isRadioValueWorthyOrUnworthyVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyModel.isRadioValueWorthyOrUnworthyVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorModel.isRadioValueWorthyOrUnworthyVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeCompanyModel.isRadioValueWorthyOrUnworthyVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyCompanyModel.isRadioValueWorthyOrUnworthyVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacCompanyModel.isRadioValueWorthyOrUnworthyVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyCompanyModel.isRadioValueWorthyOrUnworthyVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorCompanyModel.isRadioValueWorthyOrUnworthyVisible;
      }
    }
    return value;
  }

// Tujuan Penggunaan Collateral
  bool isUsageCollateralOtoVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeModel.isUsageCollateralOtoVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyModel.isUsageCollateralOtoVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacModel.isUsageCollateralOtoVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyModel.isUsageCollateralOtoVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorModel.isUsageCollateralOtoVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeCompanyModel.isUsageCollateralOtoVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyCompanyModel.isUsageCollateralOtoVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacCompanyModel.isUsageCollateralOtoVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyCompanyModel.isUsageCollateralOtoVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorCompanyModel.isUsageCollateralOtoVisible;
      }
    }
    return value;
  }

// Untuk Semua Unit
  bool isRadioValueForAllUnitOtoVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeModel.isRadioValueForAllUnitOtoVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyModel.isRadioValueForAllUnitOtoVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacModel.isRadioValueForAllUnitOtoVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyModel.isRadioValueForAllUnitOtoVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorModel.isRadioValueForAllUnitOtoVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeCompanyModel.isRadioValueForAllUnitOtoVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyCompanyModel.isRadioValueForAllUnitOtoVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacCompanyModel.isRadioValueForAllUnitOtoVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyCompanyModel.isRadioValueForAllUnitOtoVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorCompanyModel.isRadioValueForAllUnitOtoVisible;
      }
    }
    return value;
  }

  // Nama Jaminan = Pemohon
  bool isRadioValueIsCollaNameSameWithApplicantOtoMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeModel.isRadioValueIsCollaNameSameWithApplicantOtoMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyModel.isRadioValueIsCollaNameSameWithApplicantOtoMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacModel.isRadioValueIsCollaNameSameWithApplicantOtoMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyModel.isRadioValueIsCollaNameSameWithApplicantOtoMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorModel.isRadioValueIsCollaNameSameWithApplicantOtoMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeCompanyModel.isRadioValueIsCollaNameSameWithApplicantOtoMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyCompanyModel.isRadioValueIsCollaNameSameWithApplicantOtoMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacCompanyModel.isRadioValueIsCollaNameSameWithApplicantOtoMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyCompanyModel.isRadioValueIsCollaNameSameWithApplicantOtoMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorCompanyModel.isRadioValueIsCollaNameSameWithApplicantOtoMandatory;
      }
    }
    return value;
  }

// Jenis Identitas
  bool isIdentityTypeSelectedAutoMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeModel.isIdentityTypeSelectedAutoMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyModel.isIdentityTypeSelectedAutoMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacModel.isIdentityTypeSelectedAutoMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyModel.isIdentityTypeSelectedAutoMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorModel.isIdentityTypeSelectedAutoMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeCompanyModel.isIdentityTypeSelectedAutoMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyCompanyModel.isIdentityTypeSelectedAutoMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacCompanyModel.isIdentityTypeSelectedAutoMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyCompanyModel.isIdentityTypeSelectedAutoMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorCompanyModel.isIdentityTypeSelectedAutoMandatory;
      }
    }
    return value;
  }

// No Identitas
  bool isIdentityNumberAutoMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeModel.isIdentityNumberAutoMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyModel.isIdentityNumberAutoMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacModel.isIdentityNumberAutoMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyModel.isIdentityNumberAutoMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorModel.isIdentityNumberAutoMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeCompanyModel.isIdentityNumberAutoMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyCompanyModel.isIdentityNumberAutoMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacCompanyModel.isIdentityNumberAutoMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyCompanyModel.isIdentityNumberAutoMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorCompanyModel.isIdentityNumberAutoMandatory;
      }
    }
    return value;
  }

// Nama Pada Jaminan
  bool isNameOnCollateralAutoMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeModel.isNameOnCollateralAutoMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyModel.isNameOnCollateralAutoMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacModel.isNameOnCollateralAutoMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyModel.isNameOnCollateralAutoMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorModel.isNameOnCollateralAutoMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeCompanyModel.isNameOnCollateralAutoMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyCompanyModel.isNameOnCollateralAutoMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacCompanyModel.isNameOnCollateralAutoMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyCompanyModel.isNameOnCollateralAutoMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorCompanyModel.isNameOnCollateralAutoMandatory;
      }
    }
    return value;
  }

// Tanggal Lahir
  bool isBirthDateAutoMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeModel.isBirthDateAutoMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyModel.isBirthDateAutoMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacModel.isBirthDateAutoMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyModel.isBirthDateAutoMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorModel.isBirthDateAutoMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeCompanyModel.isBirthDateAutoMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyCompanyModel.isBirthDateAutoMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacCompanyModel.isBirthDateAutoMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyCompanyModel.isBirthDateAutoMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorCompanyModel.isBirthDateAutoMandatory;
      }
    }
    return value;
  }

// Tempat Lahir Sesuai Identitas
  bool isBirthPlaceValidWithIdentityAutoMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeModel.isBirthPlaceValidWithIdentityAutoMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyModel.isBirthPlaceValidWithIdentityAutoMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacModel.isBirthPlaceValidWithIdentityAutoMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyModel.isBirthPlaceValidWithIdentityAutoMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorModel.isBirthPlaceValidWithIdentityAutoMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeCompanyModel.isBirthPlaceValidWithIdentityAutoMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyCompanyModel.isBirthPlaceValidWithIdentityAutoMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacCompanyModel.isBirthPlaceValidWithIdentityAutoMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyCompanyModel.isBirthPlaceValidWithIdentityAutoMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorCompanyModel.isBirthPlaceValidWithIdentityAutoMandatory;
      }
    }
    return value;
  }

// Tempat Lahir Sesuai Identitas LOV
  bool isBirthPlaceValidWithIdentityLOVAutoMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeModel.isBirthPlaceValidWithIdentityLOVAutoMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyModel.isBirthPlaceValidWithIdentityLOVAutoMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacModel.isBirthPlaceValidWithIdentityLOVAutoMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyModel.isBirthPlaceValidWithIdentityLOVAutoMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorModel.isBirthPlaceValidWithIdentityLOVAutoMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeCompanyModel.isBirthPlaceValidWithIdentityLOVAutoMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyCompanyModel.isBirthPlaceValidWithIdentityLOVAutoMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacCompanyModel.isBirthPlaceValidWithIdentityLOVAutoMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyCompanyModel.isBirthPlaceValidWithIdentityLOVAutoMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorCompanyModel.isBirthPlaceValidWithIdentityLOVAutoMandatory;
      }
    }
    return value;
  }

// Collateral = Unit
  bool isRadioValueIsCollateralSameWithUnitOtoMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeModel.isRadioValueIsCollateralSameWithUnitOtoMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyModel.isRadioValueIsCollateralSameWithUnitOtoMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacModel.isRadioValueIsCollateralSameWithUnitOtoMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyModel.isRadioValueIsCollateralSameWithUnitOtoMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorModel.isRadioValueIsCollateralSameWithUnitOtoMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeCompanyModel.isRadioValueIsCollateralSameWithUnitOtoMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyCompanyModel.isRadioValueIsCollateralSameWithUnitOtoMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacCompanyModel.isRadioValueIsCollateralSameWithUnitOtoMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyCompanyModel.isRadioValueIsCollateralSameWithUnitOtoMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorCompanyModel.isRadioValueIsCollateralSameWithUnitOtoMandatory;
      }
    }
    return value;
  }

// Group Object
  bool isGroupObjectMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeModel.isGroupObjectMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyModel.isGroupObjectMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacModel.isGroupObjectMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyModel.isGroupObjectMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorModel.isGroupObjectMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeCompanyModel.isGroupObjectMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyCompanyModel.isGroupObjectMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacCompanyModel.isGroupObjectMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyCompanyModel.isGroupObjectMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorCompanyModel.isGroupObjectMandatory;
      }
    }
    return value;
  }

// Object
  bool isObjectMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeModel.isObjectMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyModel.isObjectMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacModel.isObjectMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyModel.isObjectMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorModel.isObjectMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeCompanyModel.isObjectMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyCompanyModel.isObjectMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacCompanyModel.isObjectMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyCompanyModel.isObjectMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorCompanyModel.isObjectMandatory;
      }
    }
    return value;
  }

// Jenis Produk
  bool isTypeProductMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeModel.isTypeProductMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyModel.isTypeProductMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacModel.isTypeProductMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyModel.isTypeProductMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorModel.isTypeProductMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeCompanyModel.isTypeProductMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyCompanyModel.isTypeProductMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacCompanyModel.isTypeProductMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyCompanyModel.isTypeProductMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorCompanyModel.isTypeProductMandatory;
      }
    }
    return value;
  }

// Merk Produk
  bool isBrandObjectMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeModel.isBrandObjectMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyModel.isBrandObjectMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacModel.isBrandObjectMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyModel.isBrandObjectMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorModel.isBrandObjectMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeCompanyModel.isBrandObjectMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyCompanyModel.isBrandObjectMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacCompanyModel.isBrandObjectMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyCompanyModel.isBrandObjectMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorCompanyModel.isBrandObjectMandatory;
      }
    }
    return value;
  }

// Jenis Objek
  bool isObjectTypeMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeModel.isObjectTypeMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyModel.isObjectTypeMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacModel.isObjectTypeMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyModel.isObjectTypeMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorModel.isObjectTypeMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeCompanyModel.isObjectTypeMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyCompanyModel.isObjectTypeMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacCompanyModel.isObjectTypeMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyCompanyModel.isObjectTypeMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorCompanyModel.isObjectTypeMandatory;
      }
    }
    return value;
  }

// Model Objek
  bool isModelObjectMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeModel.isModelObjectMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyModel.isModelObjectMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacModel.isModelObjectMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyModel.isModelObjectMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorModel.isModelObjectMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeCompanyModel.isModelObjectMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyCompanyModel.isModelObjectMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacCompanyModel.isModelObjectMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyCompanyModel.isModelObjectMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorCompanyModel.isModelObjectMandatory;
      }
    }
    return value;
  }

// Pemakaian Objek
  bool isUsageObjectModelMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeModel.isUsageObjectModelMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyModel.isUsageObjectModelMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacModel.isUsageObjectModelMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyModel.isUsageObjectModelMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorModel.isUsageObjectModelMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeCompanyModel.isUsageObjectModelMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyCompanyModel.isUsageObjectModelMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacCompanyModel.isUsageObjectModelMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyCompanyModel.isUsageObjectModelMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorCompanyModel.isUsageObjectModelMandatory;
      }
    }
    return value;
  }

// Tahun Pembuatan
  bool isYearProductionSelectedMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeModel.isYearProductionSelectedMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyModel.isYearProductionSelectedMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacModel.isYearProductionSelectedMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyModel.isYearProductionSelectedMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorModel.isYearProductionSelectedMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeCompanyModel.isYearProductionSelectedMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyCompanyModel.isYearProductionSelectedMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacCompanyModel.isYearProductionSelectedMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyCompanyModel.isYearProductionSelectedMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorCompanyModel.isYearProductionSelectedMandatory;
      }
    }
    return value;
  }

// Tahun Registrasi
  bool isYearRegistrationSelectedMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeModel.isYearRegistrationSelectedMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyModel.isYearRegistrationSelectedMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacModel.isYearRegistrationSelectedMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyModel.isYearRegistrationSelectedMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorModel.isYearRegistrationSelectedMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeCompanyModel.isYearRegistrationSelectedMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyCompanyModel.isYearRegistrationSelectedMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacCompanyModel.isYearRegistrationSelectedMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyCompanyModel.isYearRegistrationSelectedMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorCompanyModel.isYearRegistrationSelectedMandatory;
      }
    }
    return value;
  }

// Plat Kuning
  bool isRadioValueYellowPlatMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeModel.isRadioValueYellowPlatMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyModel.isRadioValueYellowPlatMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacModel.isRadioValueYellowPlatMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyModel.isRadioValueYellowPlatMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorModel.isRadioValueYellowPlatMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeCompanyModel.isRadioValueYellowPlatMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyCompanyModel.isRadioValueYellowPlatMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacCompanyModel.isRadioValueYellowPlatMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyCompanyModel.isRadioValueYellowPlatMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorCompanyModel.isRadioValueYellowPlatMandatory;
      }
    }
    return value;
  }

// Built Up Non ATPM
  bool isRadioValueBuiltUpNonATPMMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeModel.isRadioValueBuiltUpNonATPMMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyModel.isRadioValueBuiltUpNonATPMMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacModel.isRadioValueBuiltUpNonATPMMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyModel.isRadioValueBuiltUpNonATPMMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorModel.isRadioValueBuiltUpNonATPMMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeCompanyModel.isRadioValueBuiltUpNonATPMMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyCompanyModel.isRadioValueBuiltUpNonATPMMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacCompanyModel.isRadioValueBuiltUpNonATPMMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyCompanyModel.isRadioValueBuiltUpNonATPMMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorCompanyModel.isRadioValueBuiltUpNonATPMMandatory;
      }
    }
    return value;
  }

// No Polisi
  bool isPoliceNumberMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeModel.isPoliceNumberMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyModel.isPoliceNumberMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacModel.isPoliceNumberMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyModel.isPoliceNumberMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorModel.isPoliceNumberMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeCompanyModel.isPoliceNumberMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyCompanyModel.isPoliceNumberMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacCompanyModel.isPoliceNumberMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyCompanyModel.isPoliceNumberMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorCompanyModel.isPoliceNumberMandatory;
      }
    }
    return value;
  }

// No Rangka
  bool isFrameNumberMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeModel.isFrameNumberMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyModel.isFrameNumberMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacModel.isFrameNumberMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyModel.isFrameNumberMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorModel.isFrameNumberMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeCompanyModel.isFrameNumberMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyCompanyModel.isFrameNumberMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacCompanyModel.isFrameNumberMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyCompanyModel.isFrameNumberMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorCompanyModel.isFrameNumberMandatory;
      }
    }
    return value;
  }

// No Mesin
  bool isMachineNumberMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeModel.isMachineNumberMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyModel.isMachineNumberMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacModel.isMachineNumberMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyModel.isMachineNumberMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorModel.isMachineNumberMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeCompanyModel.isMachineNumberMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyCompanyModel.isMachineNumberMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacCompanyModel.isMachineNumberMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyCompanyModel.isMachineNumberMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorCompanyModel.isMachineNumberMandatory;
      }
    }
    return value;
  }

// No BPKB
  bool isBPKPNumberMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeModel.isBPKPNumberMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyModel.isBPKPNumberMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacModel.isBPKPNumberMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyModel.isBPKPNumberMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorModel.isBPKPNumberMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeCompanyModel.isBPKPNumberMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyCompanyModel.isBPKPNumberMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacCompanyModel.isBPKPNumberMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyCompanyModel.isBPKPNumberMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorCompanyModel.isBPKPNumberMandatory;
      }
    }
    return value;
  }

// Grade Unit
  bool isGradeUnitMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeModel.isGradeUnitMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyModel.isGradeUnitMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacModel.isGradeUnitMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyModel.isGradeUnitMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorModel.isGradeUnitMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeCompanyModel.isGradeUnitMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyCompanyModel.isGradeUnitMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacCompanyModel.isGradeUnitMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyCompanyModel.isGradeUnitMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorCompanyModel.isGradeUnitMandatory;
      }
    }
    return value;
  }

// Fasilitas UTJ
  bool isFasilitasUTJMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeModel.isFasilitasUTJMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyModel.isFasilitasUTJMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacModel.isFasilitasUTJMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyModel.isFasilitasUTJMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorModel.isFasilitasUTJMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeCompanyModel.isFasilitasUTJMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyCompanyModel.isFasilitasUTJMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacCompanyModel.isFasilitasUTJMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyCompanyModel.isFasilitasUTJMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorCompanyModel.isFasilitasUTJMandatory;
      }
    }
    return value;
  }

// Nama Bidder
  bool isNamaBidderMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeModel.isNamaBidderMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyModel.isNamaBidderMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacModel.isNamaBidderMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyModel.isNamaBidderMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorModel.isNamaBidderMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeCompanyModel.isNamaBidderMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyCompanyModel.isNamaBidderMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacCompanyModel.isNamaBidderMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyCompanyModel.isNamaBidderMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorCompanyModel.isNamaBidderMandatory;
      }
    }
    return value;
  }

// Harga Jual Showroom
  bool isHargaJualShowroomMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeModel.isHargaJualShowroomMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyModel.isHargaJualShowroomMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacModel.isHargaJualShowroomMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyModel.isHargaJualShowroomMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorModel.isHargaJualShowroomMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeCompanyModel.isHargaJualShowroomMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyCompanyModel.isHargaJualShowroomMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacCompanyModel.isHargaJualShowroomMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyCompanyModel.isHargaJualShowroomMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorCompanyModel.isHargaJualShowroomMandatory;
      }
    }
    return value;
  }

// Perlengkapan Tambahan
  bool isPerlengkapanTambahanMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeModel.isPerlengkapanTambahanMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyModel.isPerlengkapanTambahanMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacModel.isPerlengkapanTambahanMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyModel.isPerlengkapanTambahanMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorModel.isPerlengkapanTambahanMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeCompanyModel.isPerlengkapanTambahanMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyCompanyModel.isPerlengkapanTambahanMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacCompanyModel.isPerlengkapanTambahanMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyCompanyModel.isPerlengkapanTambahanMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorCompanyModel.isPerlengkapanTambahanMandatory;
      }
    }
    return value;
  }

// MP Adira
  bool isMPAdiraMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeModel.isMPAdiraMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyModel.isMPAdiraMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacModel.isMPAdiraMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyModel.isMPAdiraMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorModel.isMPAdiraMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeCompanyModel.isMPAdiraMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyCompanyModel.isMPAdiraMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacCompanyModel.isMPAdiraMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyCompanyModel.isMPAdiraMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorCompanyModel.isMPAdiraMandatory;
      }
    }
    return value;
  }

// MP Adira Upload
  bool isMPAdiraUploadMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeModel.isMPAdiraMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyModel.isMPAdiraMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacModel.isMPAdiraMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyModel.isMPAdiraMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorModel.isMPAdiraMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeCompanyModel.isMPAdiraMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyCompanyModel.isMPAdiraMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacCompanyModel.isMPAdiraMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyCompanyModel.isMPAdiraMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorCompanyModel.isMPAdiraMandatory;
      }
    }
    return value;
  }

// Rekondisi Fisik
  bool isRekondisiFisikMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeModel.isRekondisiFisikMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyModel.isRekondisiFisikMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacModel.isRekondisiFisikMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyModel.isRekondisiFisikMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorModel.isRekondisiFisikMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeCompanyModel.isRekondisiFisikMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyCompanyModel.isRekondisiFisikMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacCompanyModel.isRekondisiFisikMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyCompanyModel.isRekondisiFisikMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorCompanyModel.isRekondisiFisikMandatory;
      }
    }
    return value;
  }

// DP Jaminan
  bool isDpGuaranteeMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeModel.isDpGuaranteeMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyModel.isDpGuaranteeMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacModel.isDpGuaranteeMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyModel.isDpGuaranteeMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorModel.isDpGuaranteeMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeCompanyModel.isDpGuaranteeMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyCompanyModel.isDpGuaranteeMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacCompanyModel.isDpGuaranteeMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyCompanyModel.isDpGuaranteeMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorCompanyModel.isDpGuaranteeMandatory;
      }
    }
    return value;
  }

// PH Maksimal
  bool isPHMaxAutomotiveMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeModel.isPHMaxAutomotiveMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyModel.isPHMaxAutomotiveMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacModel.isPHMaxAutomotiveMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyModel.isPHMaxAutomotiveMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorModel.isPHMaxAutomotiveMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeCompanyModel.isPHMaxAutomotiveMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyCompanyModel.isPHMaxAutomotiveMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacCompanyModel.isPHMaxAutomotiveMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyCompanyModel.isPHMaxAutomotiveMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorCompanyModel.isPHMaxAutomotiveMandatory;
      }
    }
    return value;
  }

// Harga Taksasi
  bool isTaksasiPriceAutomotiveMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeModel.isTaksasiPriceAutomotiveMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyModel.isTaksasiPriceAutomotiveMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacModel.isTaksasiPriceAutomotiveMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyModel.isTaksasiPriceAutomotiveMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorModel.isTaksasiPriceAutomotiveMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeCompanyModel.isTaksasiPriceAutomotiveMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyCompanyModel.isTaksasiPriceAutomotiveMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacCompanyModel.isTaksasiPriceAutomotiveMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyCompanyModel.isTaksasiPriceAutomotiveMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorCompanyModel.isTaksasiPriceAutomotiveMandatory;
      }
    }
    return value;
  }

// Kesimpulan
  bool isRadioValueWorthyOrUnworthyMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeModel.isRadioValueWorthyOrUnworthyMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyModel.isRadioValueWorthyOrUnworthyMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacModel.isRadioValueWorthyOrUnworthyMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyModel.isRadioValueWorthyOrUnworthyMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorModel.isRadioValueWorthyOrUnworthyMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeCompanyModel.isRadioValueWorthyOrUnworthyMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyCompanyModel.isRadioValueWorthyOrUnworthyMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacCompanyModel.isRadioValueWorthyOrUnworthyMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyCompanyModel.isRadioValueWorthyOrUnworthyMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorCompanyModel.isRadioValueWorthyOrUnworthyMandatory;
      }
    }
    return value;
  }

// Tujuan Penggunaan Collateral
  bool isUsageCollateralOtoMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeModel.isUsageCollateralOtoMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyModel.isUsageCollateralOtoMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacModel.isUsageCollateralOtoMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyModel.isUsageCollateralOtoMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorModel.isUsageCollateralOtoMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeCompanyModel.isUsageCollateralOtoMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyCompanyModel.isUsageCollateralOtoMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacCompanyModel.isUsageCollateralOtoMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyCompanyModel.isUsageCollateralOtoMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorCompanyModel.isUsageCollateralOtoMandatory;
      }
    }
    return value;
  }

// Untuk Semua Unit
  bool isRadioValueForAllUnitOtoMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeModel.isRadioValueForAllUnitOtoMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyModel.isRadioValueForAllUnitOtoMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacModel.isRadioValueForAllUnitOtoMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyModel.isRadioValueForAllUnitOtoMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorModel.isRadioValueForAllUnitOtoMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaOtoIdeCompanyModel.isRadioValueForAllUnitOtoMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaOtoRegulerSurveyCompanyModel.isRadioValueForAllUnitOtoMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaOtoPacCompanyModel.isRadioValueForAllUnitOtoMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaOtoResurveyCompanyModel.isRadioValueForAllUnitOtoMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaOtoDakorCompanyModel.isRadioValueForAllUnitOtoMandatory;
      }
    }
    return value;
  }
  // End Inf Colla Oto

  // Inf Colla Prop
  ShowMandatoryInfoCollaPropModel _showMandatoryInfoCollaPropIdeModel;
  ShowMandatoryInfoCollaPropModel _showMandatoryInfoCollaPropIdeCompanyModel;
  void showMandatoryInfoCollaPropIdeModel(BuildContext context){
    _showMandatoryInfoCollaPropIdeModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoCollaPropIdeModel;
    _showMandatoryInfoCollaPropIdeCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoCollaPropIdeCompanyModel;
  }

  ShowMandatoryInfoCollaPropModel _showMandatoryInfoCollaPropRegulerSurveyModel;
  ShowMandatoryInfoCollaPropModel _showMandatoryInfoCollaPropRegulerSurveyCompanyModel;
  void showMandatoryInfoCollaPropRegulerSurveyModel(BuildContext context){
    _showMandatoryInfoCollaPropRegulerSurveyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoCollaPropRegulerSurveyModel;
    _showMandatoryInfoCollaPropRegulerSurveyCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoCollaPropRegulerSurveyCompanyModel;
  }

  ShowMandatoryInfoCollaPropModel _showMandatoryInfoCollaPropPacModel;
  ShowMandatoryInfoCollaPropModel _showMandatoryInfoCollaPropPacCompanyModel;
  void showMandatoryInfoCollaPropPacModel(BuildContext context){
    _showMandatoryInfoCollaPropPacModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoCollaPropPacModel;
    _showMandatoryInfoCollaPropPacCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoCollaPropPacCompanyModel;
  }

  ShowMandatoryInfoCollaPropModel _showMandatoryInfoCollaPropResurveyModel;
  ShowMandatoryInfoCollaPropModel _showMandatoryInfoCollaPropResurveyCompanyModel;
  void showMandatoryInfoCollaPropResurveyModel(BuildContext context){
    _showMandatoryInfoCollaPropResurveyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoCollaPropResurveyModel;
    _showMandatoryInfoCollaPropResurveyCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoCollaPropResurveyCompanyModel;
  }

  ShowMandatoryInfoCollaPropModel _showMandatoryInfoCollaPropDakorModel;
  ShowMandatoryInfoCollaPropModel _showMandatoryInfoCollaPropDakorCompanyModel;
  void showMandatoryInfoCollaPropDakorModel(BuildContext context){
    _showMandatoryInfoCollaPropDakorModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoCollaPropDakorModel;
    _showMandatoryInfoCollaPropDakorCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoCollaPropDakorCompanyModel;
  }

  // Nama Jaminan = Pemohon
  bool isRadioValueIsCollateralSameWithApplicantPropertyVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isRadioValueIsCollateralSameWithApplicantPropertyVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isRadioValueIsCollateralSameWithApplicantPropertyVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isRadioValueIsCollateralSameWithApplicantPropertyVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isRadioValueIsCollateralSameWithApplicantPropertyVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isRadioValueIsCollateralSameWithApplicantPropertyVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isRadioValueIsCollateralSameWithApplicantPropertyVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isRadioValueIsCollateralSameWithApplicantPropertyVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isRadioValueIsCollateralSameWithApplicantPropertyVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isRadioValueIsCollateralSameWithApplicantPropertyVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isRadioValueIsCollateralSameWithApplicantPropertyVisible;
      }
    }
    return value;
  }

// Jenis Identitas
  bool isIdentityModelVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isIdentityModelVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isIdentityModelVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isIdentityModelVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isIdentityModelVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isIdentityModelVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isIdentityModelVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isIdentityModelVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isIdentityModelVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isIdentityModelVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isIdentityModelVisible;
      }
    }
    return value;
  }

// No Identitas
  bool isIdentityNumberVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isIdentityNumberVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isIdentityNumberVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isIdentityNumberVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isIdentityNumberVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isIdentityNumberVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isIdentityNumberVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isIdentityNumberVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isIdentityNumberVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isIdentityNumberVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isIdentityNumberVisible;
      }
    }
    return value;
  }

// Nama Pada Jaminan
  bool isNameOnCollateralVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isNameOnCollateralVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isNameOnCollateralVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isNameOnCollateralVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isNameOnCollateralVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isNameOnCollateralVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isNameOnCollateralVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isNameOnCollateralVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isNameOnCollateralVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isNameOnCollateralVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isNameOnCollateralVisible;
      }
    }
    return value;
  }

// Tanggal Lahir
  bool isBirthDatePropVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isBirthDatePropVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isBirthDatePropVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isBirthDatePropVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isBirthDatePropVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isBirthDatePropVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isBirthDatePropVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isBirthDatePropVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isBirthDatePropVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isBirthDatePropVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isBirthDatePropVisible;
      }
    }
    return value;
  }

// Tempat Lahir Sesuai Identitas
  bool isBirthPlaceValidWithIdentity1Visible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isBirthPlaceValidWithIdentity1Visible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isBirthPlaceValidWithIdentity1Visible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isBirthPlaceValidWithIdentity1Visible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isBirthPlaceValidWithIdentity1Visible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isBirthPlaceValidWithIdentity1Visible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isBirthPlaceValidWithIdentity1Visible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isBirthPlaceValidWithIdentity1Visible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isBirthPlaceValidWithIdentity1Visible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isBirthPlaceValidWithIdentity1Visible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isBirthPlaceValidWithIdentity1Visible;
      }
    }
    return value;
  }

// Tempat Lahir Sesuai Identitas LOV
  bool isBirthPlaceValidWithIdentityLOVVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isBirthPlaceValidWithIdentityLOVVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isBirthPlaceValidWithIdentityLOVVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isBirthPlaceValidWithIdentityLOVVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isBirthPlaceValidWithIdentityLOVVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isBirthPlaceValidWithIdentityLOVVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isBirthPlaceValidWithIdentityLOVVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isBirthPlaceValidWithIdentityLOVVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isBirthPlaceValidWithIdentityLOVVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isBirthPlaceValidWithIdentityLOVVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isBirthPlaceValidWithIdentityLOVVisible;
      }
    }
    return value;
  }

// No Sertifikat
  bool isCertificateNumberVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isCertificateNumberVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isCertificateNumberVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isCertificateNumberVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isCertificateNumberVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isCertificateNumberVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isCertificateNumberVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isCertificateNumberVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isCertificateNumberVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isCertificateNumberVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isCertificateNumberVisible;
      }
    }
    return value;
  }

// Jenis Sertifikat
  bool isCertificateTypeSelectedVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isCertificateTypeSelectedVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isCertificateTypeSelectedVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isCertificateTypeSelectedVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isCertificateTypeSelectedVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isCertificateTypeSelectedVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isCertificateTypeSelectedVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isCertificateTypeSelectedVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isCertificateTypeSelectedVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isCertificateTypeSelectedVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isCertificateTypeSelectedVisible;
      }
    }
    return value;
  }

// Jenis Properti
  bool isPropertyTypeSelectedVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isPropertyTypeSelectedVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isPropertyTypeSelectedVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isPropertyTypeSelectedVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isPropertyTypeSelectedVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isPropertyTypeSelectedVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isPropertyTypeSelectedVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isPropertyTypeSelectedVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isPropertyTypeSelectedVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isPropertyTypeSelectedVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isPropertyTypeSelectedVisible;
      }
    }
    return value;
  }

// Luas Bangunan
  bool isBuildingAreaVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isBuildingAreaVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isBuildingAreaVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isBuildingAreaVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isBuildingAreaVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isBuildingAreaVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isBuildingAreaVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isBuildingAreaVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isBuildingAreaVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isBuildingAreaVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isBuildingAreaVisible;
      }
    }
    return value;
  }

// Luas Tanah
  bool isSurfaceAreaVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isSurfaceAreaVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isSurfaceAreaVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isSurfaceAreaVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isSurfaceAreaVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isSurfaceAreaVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isSurfaceAreaVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isSurfaceAreaVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isSurfaceAreaVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isSurfaceAreaVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isSurfaceAreaVisible;
      }
    }
    return value;
  }

// Harga Taksasi
  bool isTaksasiPriceVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isTaksasiPriceVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isTaksasiPriceVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isTaksasiPriceVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isTaksasiPriceVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isTaksasiPriceVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isTaksasiPriceVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isTaksasiPriceVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isTaksasiPriceVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isTaksasiPriceVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isTaksasiPriceVisible;
      }
    }
    return value;
  }

// Sifat Jaminan
  bool isSifatJaminanVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isSifatJaminanVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isSifatJaminanVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isSifatJaminanVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isSifatJaminanVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isSifatJaminanVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isSifatJaminanVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isSifatJaminanVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isSifatJaminanVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isSifatJaminanVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isSifatJaminanVisible;
      }
    }
    return value;
  }

// Bukti Kepemilikan Jaminan
  bool isBuktiKepemilikanVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isBuktiKepemilikanVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isBuktiKepemilikanVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isBuktiKepemilikanVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isBuktiKepemilikanVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isBuktiKepemilikanVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isBuktiKepemilikanVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isBuktiKepemilikanVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isBuktiKepemilikanVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isBuktiKepemilikanVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isBuktiKepemilikanVisible;
      }
    }
    return value;
  }

// Tanggal Penerbitan Sertifikat
  bool isCertificateReleaseDateVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isCertificateReleaseDateVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isCertificateReleaseDateVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isCertificateReleaseDateVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isCertificateReleaseDateVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isCertificateReleaseDateVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isCertificateReleaseDateVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isCertificateReleaseDateVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isCertificateReleaseDateVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isCertificateReleaseDateVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isCertificateReleaseDateVisible;
      }
    }
    return value;
  }

// Tahun Penerbitan Sertifikat
  bool isCertificateReleaseYearVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isCertificateReleaseYearVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isCertificateReleaseYearVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isCertificateReleaseYearVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isCertificateReleaseYearVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isCertificateReleaseYearVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isCertificateReleaseYearVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isCertificateReleaseYearVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isCertificateReleaseYearVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isCertificateReleaseYearVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isCertificateReleaseYearVisible;
      }
    }
    return value;
  }

// Nama Pemegang Hak
  bool isNamaPemegangHakVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isNamaPemegangHakVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isNamaPemegangHakVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isNamaPemegangHakVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isNamaPemegangHakVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isNamaPemegangHakVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isNamaPemegangHakVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isNamaPemegangHakVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isNamaPemegangHakVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isNamaPemegangHakVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isNamaPemegangHakVisible;
      }
    }
    return value;
  }

// Nomor Surat Ukur
  bool isNoSuratUkurVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isNoSuratUkurVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isNoSuratUkurVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isNoSuratUkurVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isNoSuratUkurVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isNoSuratUkurVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isNoSuratUkurVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isNoSuratUkurVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isNoSuratUkurVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isNoSuratUkurVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isNoSuratUkurVisible;
      }
    }
    return value;
  }

// Tanggal Surat Ukur
  bool isDateOfMeasuringLetterVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isDateOfMeasuringLetterVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isDateOfMeasuringLetterVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isDateOfMeasuringLetterVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isDateOfMeasuringLetterVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isDateOfMeasuringLetterVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isDateOfMeasuringLetterVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isDateOfMeasuringLetterVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isDateOfMeasuringLetterVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isDateOfMeasuringLetterVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isDateOfMeasuringLetterVisible;
      }
    }
    return value;
  }

  // Sertifikat dan Surat Ukur
  bool isCertificateOfMeasuringLetterVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isCertificateOfMeasuringLetterVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isCertificateOfMeasuringLetterVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isCertificateOfMeasuringLetterVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isCertificateOfMeasuringLetterVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isCertificateOfMeasuringLetterVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isCertificateOfMeasuringLetterVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isCertificateOfMeasuringLetterVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isCertificateOfMeasuringLetterVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isCertificateOfMeasuringLetterVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isCertificateOfMeasuringLetterVisible;
      }
    }
    return value;
  }

// DP Jaminan
  bool isDPJaminanVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isDPJaminanVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isDPJaminanVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isDPJaminanVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isDPJaminanVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isDPJaminanVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isDPJaminanVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isDPJaminanVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isDPJaminanVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isDPJaminanVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isDPJaminanVisible;
      }
    }
    return value;
  }

// PH Maksimal
  bool isPHMaxVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isPHMaxVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isPHMaxVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isPHMaxVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isPHMaxVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isPHMaxVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isPHMaxVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isPHMaxVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isPHMaxVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isPHMaxVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isPHMaxVisible;
      }
    }
    return value;
  }

// Jarak Fasum Positif
  bool isJarakFasumPositifVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isJarakFasumPositifVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isJarakFasumPositifVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isJarakFasumPositifVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isJarakFasumPositifVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isJarakFasumPositifVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isJarakFasumPositifVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isJarakFasumPositifVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isJarakFasumPositifVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isJarakFasumPositifVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isJarakFasumPositifVisible;
      }
    }
    return value;
  }

// Jarak Fasum Negatif
  bool isJarakFasumNegatifVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isJarakFasumNegatifVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isJarakFasumNegatifVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isJarakFasumNegatifVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isJarakFasumNegatifVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isJarakFasumNegatifVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isJarakFasumNegatifVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isJarakFasumNegatifVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isJarakFasumNegatifVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isJarakFasumNegatifVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isJarakFasumNegatifVisible;
      }
    }
    return value;
  }

// Harga Tanah
  bool isHargaTanahVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isHargaTanahVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isHargaTanahVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isHargaTanahVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isHargaTanahVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isHargaTanahVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isHargaTanahVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isHargaTanahVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isHargaTanahVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isHargaTanahVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isHargaTanahVisible;
      }
    }
    return value;
  }

// Harga NJOP
  bool isHargaNJOPVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isHargaNJOPVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isHargaNJOPVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isHargaNJOPVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isHargaNJOPVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isHargaNJOPVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isHargaNJOPVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isHargaNJOPVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isHargaNJOPVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isHargaNJOPVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isHargaNJOPVisible;
      }
    }
    return value;
  }

// Harga Bangunan
  bool isHargaBangunanVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isHargaBangunanVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isHargaBangunanVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isHargaBangunanVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isHargaBangunanVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isHargaBangunanVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isHargaBangunanVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isHargaBangunanVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isHargaBangunanVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isHargaBangunanVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isHargaBangunanVisible;
      }
    }
    return value;
  }

// Jenis Atap
  bool isTypeOfRoofSelectedVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isTypeOfRoofSelectedVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isTypeOfRoofSelectedVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isTypeOfRoofSelectedVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isTypeOfRoofSelectedVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isTypeOfRoofSelectedVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isTypeOfRoofSelectedVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isTypeOfRoofSelectedVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isTypeOfRoofSelectedVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isTypeOfRoofSelectedVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isTypeOfRoofSelectedVisible;
      }
    }
    return value;
  }

// Jenis Dinding
  bool isWallTypeSelectedVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isWallTypeSelectedVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isWallTypeSelectedVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isWallTypeSelectedVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isWallTypeSelectedVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isWallTypeSelectedVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isWallTypeSelectedVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isWallTypeSelectedVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isWallTypeSelectedVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isWallTypeSelectedVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isWallTypeSelectedVisible;
      }
    }
    return value;
  }

// Jenis Lantai
  bool isFloorTypeSelectedVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isFloorTypeSelectedVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isFloorTypeSelectedVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isFloorTypeSelectedVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isFloorTypeSelectedVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isFloorTypeSelectedVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isFloorTypeSelectedVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isFloorTypeSelectedVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isFloorTypeSelectedVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isFloorTypeSelectedVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isFloorTypeSelectedVisible;
      }
    }
    return value;
  }

// Jenis Fondasi
  bool isFoundationTypeSelectedVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isFoundationTypeSelectedVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isFoundationTypeSelectedVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isFoundationTypeSelectedVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isFoundationTypeSelectedVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isFoundationTypeSelectedVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isFoundationTypeSelectedVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isFoundationTypeSelectedVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isFoundationTypeSelectedVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isFoundationTypeSelectedVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isFoundationTypeSelectedVisible;
      }
    }
    return value;
  }

// Tipe Jalan
  bool isStreetTypeSelectedVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isStreetTypeSelectedVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isStreetTypeSelectedVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isStreetTypeSelectedVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isStreetTypeSelectedVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isStreetTypeSelectedVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isStreetTypeSelectedVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isStreetTypeSelectedVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isStreetTypeSelectedVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isStreetTypeSelectedVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isStreetTypeSelectedVisible;
      }
    }
    return value;
  }

// Dilewati Mobil
  bool isRadioValueAccessCarVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isRadioValueAccessCarVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isRadioValueAccessCarVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isRadioValueAccessCarVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isRadioValueAccessCarVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isRadioValueAccessCarVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isRadioValueAccessCarVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isRadioValueAccessCarVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isRadioValueAccessCarVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isRadioValueAccessCarVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isRadioValueAccessCarVisible;
      }
    }
    return value;
  }

// Jumlah Rumah Dalam Radius
  bool isJumlahRumahDalamRadiusVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isJumlahRumahDalamRadiusVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isJumlahRumahDalamRadiusVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isJumlahRumahDalamRadiusVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isJumlahRumahDalamRadiusVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isJumlahRumahDalamRadiusVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isJumlahRumahDalamRadiusVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isJumlahRumahDalamRadiusVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isJumlahRumahDalamRadiusVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isJumlahRumahDalamRadiusVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isJumlahRumahDalamRadiusVisible;
      }
    }
    return value;
  }

// Masa Berlaku Hak
  bool isMasaHakBerlakuVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isMasaHakBerlakuVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isMasaHakBerlakuVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isMasaHakBerlakuVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isMasaHakBerlakuVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isMasaHakBerlakuVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isMasaHakBerlakuVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isMasaHakBerlakuVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isMasaHakBerlakuVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isMasaHakBerlakuVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isMasaHakBerlakuVisible;
      }
    }
    return value;
  }

// No IMB
  bool isNoIMBVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isNoIMBVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isNoIMBVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isNoIMBVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isNoIMBVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isNoIMBVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isNoIMBVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isNoIMBVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isNoIMBVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isNoIMBVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isNoIMBVisible;
      }
    }
    return value;
  }

// Tanggal IMB
  bool isDateOfIMBVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isDateOfIMBVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isDateOfIMBVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isDateOfIMBVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isDateOfIMBVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isDateOfIMBVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isDateOfIMBVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isDateOfIMBVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isDateOfIMBVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isDateOfIMBVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isDateOfIMBVisible;
      }
    }
    return value;
  }

// Luas Bangunan Berdasarkan IMB
  bool isLuasBangunanIMBVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isLuasBangunanIMBVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isLuasBangunanIMBVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isLuasBangunanIMBVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isLuasBangunanIMBVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isLuasBangunanIMBVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isLuasBangunanIMBVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isLuasBangunanIMBVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isLuasBangunanIMBVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isLuasBangunanIMBVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isLuasBangunanIMBVisible;
      }
    }
    return value;
  }

// Tujuan Penggunaan Jaminan/Collateral
  bool isUsageCollateralPropertyVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isUsageCollateralPropertyVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isUsageCollateralPropertyVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isUsageCollateralPropertyVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isUsageCollateralPropertyVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isUsageCollateralPropertyVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isUsageCollateralPropertyVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isUsageCollateralPropertyVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isUsageCollateralPropertyVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isUsageCollateralPropertyVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isUsageCollateralPropertyVisible;
      }
    }
    return value;
  }

// LTV
  bool isLTVVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isLTVVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isLTVVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isLTVVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isLTVVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isLTVVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isLTVVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isLTVVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isLTVVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isLTVVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isLTVVisible;
      }
    }
    return value;
  }

// Untuk Semua Unit
  bool isRadioValueForAllUnitPropertyVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isRadioValueForAllUnitPropertyVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isRadioValueForAllUnitPropertyVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isRadioValueForAllUnitPropertyVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isRadioValueForAllUnitPropertyVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isRadioValueForAllUnitPropertyVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isRadioValueForAllUnitPropertyVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isRadioValueForAllUnitPropertyVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isRadioValueForAllUnitPropertyVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isRadioValueForAllUnitPropertyVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isRadioValueForAllUnitPropertyVisible;
      }
    }
    return value;
  }

  // Jenis Alamat
  bool isAddressTypeSelectedVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isAddressTypeSelectedVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isAddressTypeSelectedVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isAddressTypeSelectedVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isAddressTypeSelectedVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isAddressTypeSelectedVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isAddressTypeSelectedVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isAddressTypeSelectedVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isAddressTypeSelectedVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isAddressTypeSelectedVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isAddressTypeSelectedVisible;
      }
    }
    return value;
  }

// Alamat
  bool isAddressVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isAddressVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isAddressVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isAddressVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isAddressVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isAddressVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isAddressVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isAddressVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isAddressVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isAddressVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isAddressVisible;
      }
    }
    return value;
  }

// RT
  bool isRTVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isRTVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isRTVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isRTVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isRTVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isRTVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isRTVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isRTVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isRTVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isRTVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isRTVisible;
      }
    }
    return value;
  }

// RW
  bool isRWVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isRWVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isRWVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isRWVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isRWVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isRWVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isRWVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isRWVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isRWVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isRWVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isRWVisible;
      }
    }
    return value;
  }

// Kelurahan
  bool isKelurahanVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isKelurahanVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isKelurahanVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isKelurahanVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isKelurahanVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isKelurahanVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isKelurahanVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isKelurahanVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isKelurahanVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isKelurahanVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isKelurahanVisible;
      }
    }
    return value;
  }

// Kecamatan
  bool isKecamatanVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isKecamatanVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isKecamatanVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isKecamatanVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isKecamatanVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isKecamatanVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isKecamatanVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isKecamatanVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isKecamatanVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isKecamatanVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isKecamatanVisible;
      }
    }
    return value;
  }

// Kabupaten Kota
  bool isKotaVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isKotaVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isKotaVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isKotaVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isKotaVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isKotaVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isKotaVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isKotaVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isKotaVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isKotaVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isKotaVisible;
      }
    }
    return value;
  }

// Provinsi
  bool isProvVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isProvVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isProvVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isProvVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isProvVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isProvVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isProvVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isProvVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isProvVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isProvVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isProvVisible;
      }
    }
    return value;
  }

// Kode Pos
  bool isPostalCodeVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isPostalCodeVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isPostalCodeVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isPostalCodeVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isPostalCodeVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isPostalCodeVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isPostalCodeVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isPostalCodeVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isPostalCodeVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isPostalCodeVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isPostalCodeVisible;
      }
    }
    return value;
  }

// Geolocation Alamat
  bool isAddressFromMapVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isAddressFromMapVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isAddressFromMapVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isAddressFromMapVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isAddressFromMapVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isAddressFromMapVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isAddressFromMapVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isAddressFromMapVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isAddressFromMapVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isAddressFromMapVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isAddressFromMapVisible;
      }
    }
    return value;
  }

  // Nama Jaminan = Pemohon
  bool isRadioValueIsCollateralSameWithApplicantPropertyMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isRadioValueIsCollateralSameWithApplicantPropertyMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isRadioValueIsCollateralSameWithApplicantPropertyMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isRadioValueIsCollateralSameWithApplicantPropertyMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isRadioValueIsCollateralSameWithApplicantPropertyMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isRadioValueIsCollateralSameWithApplicantPropertyMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isRadioValueIsCollateralSameWithApplicantPropertyMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isRadioValueIsCollateralSameWithApplicantPropertyMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isRadioValueIsCollateralSameWithApplicantPropertyMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isRadioValueIsCollateralSameWithApplicantPropertyMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isRadioValueIsCollateralSameWithApplicantPropertyMandatory;
      }
    }
    return value;
  }

// Jenis Identitas
  bool isIdentityModelMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isIdentityModelMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isIdentityModelMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isIdentityModelMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isIdentityModelMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isIdentityModelMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isIdentityModelMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isIdentityModelMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isIdentityModelMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isIdentityModelMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isIdentityModelMandatory;
      }
    }
    return value;
  }

// No Identitas
  bool isIdentityNumberMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isIdentityNumberMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isIdentityNumberMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isIdentityNumberMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isIdentityNumberMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isIdentityNumberMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isIdentityNumberMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isIdentityNumberMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isIdentityNumberMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isIdentityNumberMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isIdentityNumberMandatory;
      }
    }
    return value;
  }

// Nama Pada Jaminan
  bool isNameOnCollateralMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isNameOnCollateralMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isNameOnCollateralMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isNameOnCollateralMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isNameOnCollateralMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isNameOnCollateralMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isNameOnCollateralMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isNameOnCollateralMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isNameOnCollateralMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isNameOnCollateralMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isNameOnCollateralMandatory;
      }
    }
    return value;
  }

// Tanggal Lahir
  bool isBirthDatePropMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isBirthDatePropMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isBirthDatePropMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isBirthDatePropMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isBirthDatePropMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isBirthDatePropMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isBirthDatePropMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isBirthDatePropMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isBirthDatePropMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isBirthDatePropMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isBirthDatePropMandatory;
      }
    }
    return value;
  }

// Tempat Lahir Sesuai Identitas
  bool isBirthPlaceValidWithIdentity1Mandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isBirthPlaceValidWithIdentity1Mandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isBirthPlaceValidWithIdentity1Mandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isBirthPlaceValidWithIdentity1Mandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isBirthPlaceValidWithIdentity1Mandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isBirthPlaceValidWithIdentity1Mandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isBirthPlaceValidWithIdentity1Mandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isBirthPlaceValidWithIdentity1Mandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isBirthPlaceValidWithIdentity1Mandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isBirthPlaceValidWithIdentity1Mandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isBirthPlaceValidWithIdentity1Mandatory;
      }
    }
    return value;
  }

// Tempat Lahir Sesuai Identitas LOV
  bool isBirthPlaceValidWithIdentityLOVMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isBirthPlaceValidWithIdentityLOVMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isBirthPlaceValidWithIdentityLOVMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isBirthPlaceValidWithIdentityLOVMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isBirthPlaceValidWithIdentityLOVMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isBirthPlaceValidWithIdentityLOVMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isBirthPlaceValidWithIdentityLOVMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isBirthPlaceValidWithIdentityLOVMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isBirthPlaceValidWithIdentityLOVMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isBirthPlaceValidWithIdentityLOVMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isBirthPlaceValidWithIdentityLOVMandatory;
      }
    }
    return value;
  }

// No Sertifikat
  bool isCertificateNumberMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isCertificateNumberMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isCertificateNumberMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isCertificateNumberMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isCertificateNumberMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isCertificateNumberMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isCertificateNumberMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isCertificateNumberMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isCertificateNumberMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isCertificateNumberMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isCertificateNumberMandatory;
      }
    }
    return value;
  }

// Jenis Sertifikat
  bool isCertificateTypeSelectedMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isCertificateTypeSelectedMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isCertificateTypeSelectedMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isCertificateTypeSelectedMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isCertificateTypeSelectedMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isCertificateTypeSelectedMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isCertificateTypeSelectedMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isCertificateTypeSelectedMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isCertificateTypeSelectedMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isCertificateTypeSelectedMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isCertificateTypeSelectedMandatory;
      }
    }
    return value;
  }

// Jenis Properti
  bool isPropertyTypeSelectedMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isPropertyTypeSelectedMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isPropertyTypeSelectedMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isPropertyTypeSelectedMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isPropertyTypeSelectedMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isPropertyTypeSelectedMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isPropertyTypeSelectedMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isPropertyTypeSelectedMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isPropertyTypeSelectedMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isPropertyTypeSelectedMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isPropertyTypeSelectedMandatory;
      }
    }
    return value;
  }

// Luas Bangunan
  bool isBuildingAreaMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isBuildingAreaMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isBuildingAreaMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isBuildingAreaMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isBuildingAreaMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isBuildingAreaMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isBuildingAreaMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isBuildingAreaMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isBuildingAreaMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isBuildingAreaMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isBuildingAreaMandatory;
      }
    }
    return value;
  }

// Luas Tanah
  bool isSurfaceAreaMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isSurfaceAreaMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isSurfaceAreaMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isSurfaceAreaMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isSurfaceAreaMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isSurfaceAreaMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isSurfaceAreaMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isSurfaceAreaMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isSurfaceAreaMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isSurfaceAreaMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isSurfaceAreaMandatory;
      }
    }
    return value;
  }

// Harga Taksasi
  bool isTaksasiPriceMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isTaksasiPriceMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isTaksasiPriceMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isTaksasiPriceMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isTaksasiPriceMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isTaksasiPriceMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isTaksasiPriceMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isTaksasiPriceMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isTaksasiPriceMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isTaksasiPriceMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isTaksasiPriceMandatory;
      }
    }
    return value;
  }

// Sifat Jaminan
  bool isSifatJaminanMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isSifatJaminanMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isSifatJaminanMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isSifatJaminanMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isSifatJaminanMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isSifatJaminanMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isSifatJaminanMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isSifatJaminanMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isSifatJaminanMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isSifatJaminanMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isSifatJaminanMandatory;
      }
    }
    return value;
  }

// Bukti Kepemilikan Jaminan
  bool isBuktiKepemilikanMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isBuktiKepemilikanMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isBuktiKepemilikanMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isBuktiKepemilikanMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isBuktiKepemilikanMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isBuktiKepemilikanMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isBuktiKepemilikanMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isBuktiKepemilikanMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isBuktiKepemilikanMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isBuktiKepemilikanMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isBuktiKepemilikanMandatory;
      }
    }
    return value;
  }

// Tanggal Penerbitan Sertifikat
  bool isCertificateReleaseDateMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isCertificateReleaseDateMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isCertificateReleaseDateMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isCertificateReleaseDateMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isCertificateReleaseDateMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isCertificateReleaseDateMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isCertificateReleaseDateMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isCertificateReleaseDateMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isCertificateReleaseDateMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isCertificateReleaseDateMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isCertificateReleaseDateMandatory;
      }
    }
    return value;
  }

// Tahun Penerbitan Sertifikat
  bool isCertificateReleaseYearMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isCertificateReleaseYearMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isCertificateReleaseYearMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isCertificateReleaseYearMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isCertificateReleaseYearMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isCertificateReleaseYearMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isCertificateReleaseYearMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isCertificateReleaseYearMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isCertificateReleaseYearMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isCertificateReleaseYearMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isCertificateReleaseYearMandatory;
      }
    }
    return value;
  }

// Nama Pemegang Hak
  bool isNamaPemegangHakMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isNamaPemegangHakMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isNamaPemegangHakMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isNamaPemegangHakMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isNamaPemegangHakMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isNamaPemegangHakMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isNamaPemegangHakMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isNamaPemegangHakMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isNamaPemegangHakMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isNamaPemegangHakMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isNamaPemegangHakMandatory;
      }
    }
    return value;
  }

// Nomor Surat Ukur
  bool isNoSuratUkurMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isNoSuratUkurMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isNoSuratUkurMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isNoSuratUkurMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isNoSuratUkurMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isNoSuratUkurMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isNoSuratUkurMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isNoSuratUkurMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isNoSuratUkurMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isNoSuratUkurMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isNoSuratUkurMandatory;
      }
    }
    return value;
  }

// Tanggal Surat Ukur
  bool isDateOfMeasuringLetterMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isDateOfMeasuringLetterMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isDateOfMeasuringLetterMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isDateOfMeasuringLetterMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isDateOfMeasuringLetterMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isDateOfMeasuringLetterMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isDateOfMeasuringLetterMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isDateOfMeasuringLetterMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isDateOfMeasuringLetterMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isDateOfMeasuringLetterMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isDateOfMeasuringLetterMandatory;
      }
    }
    return value;
  }

  // Sertifikat dan Surat Ukur
  bool isCertificateOfMeasuringLetterMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isCertificateOfMeasuringLetterMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isCertificateOfMeasuringLetterMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isCertificateOfMeasuringLetterMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isCertificateOfMeasuringLetterMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isCertificateOfMeasuringLetterMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isCertificateOfMeasuringLetterMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isCertificateOfMeasuringLetterMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isCertificateOfMeasuringLetterMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isCertificateOfMeasuringLetterMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isCertificateOfMeasuringLetterMandatory;
      }
    }
    return value;
  }

// DP Jaminan
  bool isDPJaminanMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isDPJaminanMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isDPJaminanMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isDPJaminanMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isDPJaminanMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isDPJaminanMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isDPJaminanMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isDPJaminanMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isDPJaminanMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isDPJaminanMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isDPJaminanMandatory;
      }
    }
    return value;
  }

// PH Maksimal
  bool isPHMaxMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isPHMaxMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isPHMaxMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isPHMaxMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isPHMaxMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isPHMaxMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isPHMaxMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isPHMaxMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isPHMaxMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isPHMaxMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isPHMaxMandatory;
      }
    }
    return value;
  }

// Jarak Fasum Positif
  bool isJarakFasumPositifMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isJarakFasumPositifMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isJarakFasumPositifMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isJarakFasumPositifMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isJarakFasumPositifMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isJarakFasumPositifMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isJarakFasumPositifMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isJarakFasumPositifMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isJarakFasumPositifMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isJarakFasumPositifMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isJarakFasumPositifMandatory;
      }
    }
    return value;
  }

// Jarak Fasum Negatif
  bool isJarakFasumNegatifMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isJarakFasumNegatifMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isJarakFasumNegatifMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isJarakFasumNegatifMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isJarakFasumNegatifMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isJarakFasumNegatifMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isJarakFasumNegatifMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isJarakFasumNegatifMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isJarakFasumNegatifMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isJarakFasumNegatifMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isJarakFasumNegatifMandatory;
      }
    }
    return value;
  }

// Harga Tanah
  bool isHargaTanahMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isHargaTanahMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isHargaTanahMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isHargaTanahMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isHargaTanahMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isHargaTanahMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isHargaTanahMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isHargaTanahMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isHargaTanahMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isHargaTanahMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isHargaTanahMandatory;
      }
    }
    return value;
  }

// Harga NJOP
  bool isHargaNJOPMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isHargaNJOPMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isHargaNJOPMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isHargaNJOPMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isHargaNJOPMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isHargaNJOPMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isHargaNJOPMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isHargaNJOPMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isHargaNJOPMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isHargaNJOPMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isHargaNJOPMandatory;
      }
    }
    return value;
  }

// Harga Bangunan
  bool isHargaBangunanMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isHargaBangunanMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isHargaBangunanMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isHargaBangunanMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isHargaBangunanMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isHargaBangunanMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isHargaBangunanMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isHargaBangunanMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isHargaBangunanMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isHargaBangunanMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isHargaBangunanMandatory;
      }
    }
    return value;
  }

// Jenis Atap
  bool isTypeOfRoofSelectedMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isTypeOfRoofSelectedMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isTypeOfRoofSelectedMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isTypeOfRoofSelectedMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isTypeOfRoofSelectedMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isTypeOfRoofSelectedMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isTypeOfRoofSelectedMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isTypeOfRoofSelectedMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isTypeOfRoofSelectedMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isTypeOfRoofSelectedMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isTypeOfRoofSelectedMandatory;
      }
    }
    return value;
  }

// Jenis Dinding
  bool isWallTypeSelectedMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isWallTypeSelectedMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isWallTypeSelectedMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isWallTypeSelectedMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isWallTypeSelectedMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isWallTypeSelectedMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isWallTypeSelectedMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isWallTypeSelectedMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isWallTypeSelectedMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isWallTypeSelectedMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isWallTypeSelectedMandatory;
      }
    }
    return value;
  }

// Jenis Lantai
  bool isFloorTypeSelectedMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isFloorTypeSelectedMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isFloorTypeSelectedMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isFloorTypeSelectedMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isFloorTypeSelectedMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isFloorTypeSelectedMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isFloorTypeSelectedMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isFloorTypeSelectedMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isFloorTypeSelectedMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isFloorTypeSelectedMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isFloorTypeSelectedMandatory;
      }
    }
    return value;
  }

// Jenis Fondasi
  bool isFoundationTypeSelectedMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isFoundationTypeSelectedMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isFoundationTypeSelectedMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isFoundationTypeSelectedMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isFoundationTypeSelectedMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isFoundationTypeSelectedMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isFoundationTypeSelectedMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isFoundationTypeSelectedMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isFoundationTypeSelectedMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isFoundationTypeSelectedMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isFoundationTypeSelectedMandatory;
      }
    }
    return value;
  }

// Tipe Jalan
  bool isStreetTypeSelectedMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isStreetTypeSelectedMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isStreetTypeSelectedMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isStreetTypeSelectedMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isStreetTypeSelectedMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isStreetTypeSelectedMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isStreetTypeSelectedMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isStreetTypeSelectedMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isStreetTypeSelectedMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isStreetTypeSelectedMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isStreetTypeSelectedMandatory;
      }
    }
    return value;
  }

// Dilewati Mobil
  bool isRadioValueAccessCarMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isRadioValueAccessCarMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isRadioValueAccessCarMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isRadioValueAccessCarMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isRadioValueAccessCarMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isRadioValueAccessCarMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isRadioValueAccessCarMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isRadioValueAccessCarMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isRadioValueAccessCarMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isRadioValueAccessCarMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isRadioValueAccessCarMandatory;
      }
    }
    return value;
  }

// Jumlah Rumah Dalam Radius
  bool isJumlahRumahDalamRadiusMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isJumlahRumahDalamRadiusMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isJumlahRumahDalamRadiusMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isJumlahRumahDalamRadiusMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isJumlahRumahDalamRadiusMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isJumlahRumahDalamRadiusMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isJumlahRumahDalamRadiusMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isJumlahRumahDalamRadiusMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isJumlahRumahDalamRadiusMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isJumlahRumahDalamRadiusMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isJumlahRumahDalamRadiusMandatory;
      }
    }
    return value;
  }

// Masa Berlaku Hak
  bool isMasaHakBerlakuMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isMasaHakBerlakuMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isMasaHakBerlakuMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isMasaHakBerlakuMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isMasaHakBerlakuMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isMasaHakBerlakuMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isMasaHakBerlakuMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isMasaHakBerlakuMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isMasaHakBerlakuMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isMasaHakBerlakuMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isMasaHakBerlakuMandatory;
      }
    }
    return value;
  }

// No IMB
  bool isNoIMBMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isNoIMBMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isNoIMBMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isNoIMBMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isNoIMBMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isNoIMBMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isNoIMBMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isNoIMBMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isNoIMBMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isNoIMBMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isNoIMBMandatory;
      }
    }
    return value;
  }

// Tanggal IMB
  bool isDateOfIMBMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isDateOfIMBMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isDateOfIMBMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isDateOfIMBMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isDateOfIMBMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isDateOfIMBMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isDateOfIMBMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isDateOfIMBMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isDateOfIMBMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isDateOfIMBMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isDateOfIMBMandatory;
      }
    }
    return value;
  }

// Luas Bangunan Berdasarkan IMB
  bool isLuasBangunanIMBMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isLuasBangunanIMBMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isLuasBangunanIMBMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isLuasBangunanIMBMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isLuasBangunanIMBMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isLuasBangunanIMBMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isLuasBangunanIMBMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isLuasBangunanIMBMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isLuasBangunanIMBMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isLuasBangunanIMBMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isLuasBangunanIMBMandatory;
      }
    }
    return value;
  }

// Tujuan Penggunaan Jaminan/Collateral
  bool isUsageCollateralPropertyMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isUsageCollateralPropertyMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isUsageCollateralPropertyMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isUsageCollateralPropertyMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isUsageCollateralPropertyMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isUsageCollateralPropertyMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isUsageCollateralPropertyMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isUsageCollateralPropertyMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isUsageCollateralPropertyMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isUsageCollateralPropertyMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isUsageCollateralPropertyMandatory;
      }
    }
    return value;
  }

// LTV
  bool isLTVMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isLTVMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isLTVMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isLTVMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isLTVMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isLTVMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isLTVMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isLTVMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isLTVMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isLTVMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isLTVMandatory;
      }
    }
    return value;
  }

// Untuk Semua Unit
  bool isRadioValueForAllUnitPropertyMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isRadioValueForAllUnitPropertyMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isRadioValueForAllUnitPropertyMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isRadioValueForAllUnitPropertyMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isRadioValueForAllUnitPropertyMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isRadioValueForAllUnitPropertyMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isRadioValueForAllUnitPropertyMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isRadioValueForAllUnitPropertyMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isRadioValueForAllUnitPropertyMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isRadioValueForAllUnitPropertyMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isRadioValueForAllUnitPropertyMandatory;
      }
    }
    return value;
  }

  // Jenis Alamat
  bool isAddressTypeSelectedMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isAddressTypeSelectedMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isAddressTypeSelectedMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isAddressTypeSelectedMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isAddressTypeSelectedMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isAddressTypeSelectedMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isAddressTypeSelectedMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isAddressTypeSelectedMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isAddressTypeSelectedMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isAddressTypeSelectedMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isAddressTypeSelectedMandatory;
      }
    }
    return value;
  }

// Alamat
  bool isAddressMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isAddressMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isAddressMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isAddressMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isAddressMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isAddressMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isAddressMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isAddressMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isAddressMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isAddressMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isAddressMandatory;
      }
    }
    return value;
  }

// RT
  bool isRTMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isRTMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isRTMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isRTMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isRTMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isRTMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isRTMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isRTMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isRTMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isRTMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isRTMandatory;
      }
    }
    return value;
  }

// RW
  bool isRWMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isRWMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isRWMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isRWMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isRWMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isRWMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isRWMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isRWMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isRWMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isRWMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isRWMandatory;
      }
    }
    return value;
  }

// Kelurahan
  bool isKelurahanMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isKelurahanMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isKelurahanMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isKelurahanMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isKelurahanMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isKelurahanMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isKelurahanMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isKelurahanMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isKelurahanMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isKelurahanMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isKelurahanMandatory;
      }
    }
    return value;
  }

// Kecamatan
  bool isKecamatanMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isKecamatanMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isKecamatanMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isKecamatanMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isKecamatanMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isKecamatanMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isKecamatanMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isKecamatanMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isKecamatanMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isKecamatanMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isKecamatanMandatory;
      }
    }
    return value;
  }

// Kabupaten Kota
  bool isKotaMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isKotaMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isKotaMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isKotaMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isKotaMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isKotaMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isKotaMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isKotaMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isKotaMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isKotaMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isKotaMandatory;
      }
    }
    return value;
  }

// Provinsi
  bool isProvMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isProvMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isProvMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isProvMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isProvMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isProvMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isProvMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isProvMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isProvMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isProvMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isProvMandatory;
      }
    }
    return value;
  }

// Kode Pos
  bool isPostalCodeMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isPostalCodeMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isPostalCodeMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isPostalCodeMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isPostalCodeMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isPostalCodeMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isPostalCodeMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isPostalCodeMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isPostalCodeMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isPostalCodeMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isPostalCodeMandatory;
      }
    }
    return value;
  }

// Geolocation Alamat
  bool isAddressFromMapMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeModel.isAddressFromMapMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyModel.isAddressFromMapMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacModel.isAddressFromMapMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyModel.isAddressFromMapMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorModel.isAddressFromMapMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoCollaPropIdeCompanyModel.isAddressFromMapMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoCollaPropRegulerSurveyCompanyModel.isAddressFromMapMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoCollaPropPacCompanyModel.isAddressFromMapMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoCollaPropResurveyCompanyModel.isAddressFromMapMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoCollaPropDakorCompanyModel.isAddressFromMapMandatory;
      }
    }
    return value;
  }// End Inf Colla Prop

  // Otomotif
  bool _isRadioValueIsCollaNameSameWithApplicantOtoChanges = false;
  bool _isIdentityTypeSelectedAutoChanges = false;
  bool _isIdentityNumberAutoChanges = false;
  bool _isNameOnCollateralAutoChanges = false;
  bool _isBirthDateAutoChanges = false;
  bool _isBirthPlaceValidWithIdentityAutoChanges = false;
  bool _isBirthPlaceValidWithIdentityLOVAutoChanges = false;
  bool _isRadioValueIsCollateralSameWithUnitOtoChanges = false;
  bool _isGroupObjectChanges = false;
  bool _isObjectChanges = false;
  bool _isTypeProductChanges = false;
  bool _isBrandObjectChanges = false;
  bool _isObjectTypeChanges = false;
  bool _isModelObjectChanges = false;
  bool _isUsageObjectModelChanges = false;
  bool _isYearProductionSelectedChanges = false;
  bool _isYearRegistrationSelectedChanges = false;
  bool _isRadioValueYellowPlatChanges = false;
  bool _isRadioValueBuiltUpNonATPMChanges = false;
  bool _isPoliceNumberChanges = false;
  bool _isFrameNumberChanges = false;
  bool _isMachineNumberChanges = false;
  bool _isBPKPNumberChanges = false;
  bool _isGradeUnitChanges = false;
  bool _isFasilitasUTJChanges = false;
  bool _isNamaBidderChanges = false;
  bool _isHargaJualShowroomChanges = false;
  bool _isPerlengkapanTambahanChanges = false;
  bool _isMPAdiraChanges = false;
  bool _isMPAdiraUploadChanges = false;
  bool _isRekondisiFisikChanges = false;
  bool _isDpGuaranteeChanges = false;
  bool _isPHMaxAutomotiveChanges = false;
  bool _isTaksasiPriceAutomotiveChanges = false;
  bool _isRadioValueWorthyOrUnworthyChanges = false;
  bool _isUsageCollateralOtoChanges = false;
  bool _isRadioValueForAllUnitOtoChanges = false;
  // Property
  bool _isRadioValueIsCollateralSameWithApplicantPropertyChanges = false;
  bool _isIdentityModelChanges = false;
  bool _isIdentityNumberPropChanges = false;
  bool _isNameOnCollateralChanges = false;
  bool _isBirthDatePropChanges = false;
  bool _isBirthPlaceValidWithIdentity1Changes = false;
  bool _isBirthPlaceValidWithIdentityLOVChanges = false;
  bool _isCertificateNumberChanges = false;
  bool _isCertificateTypeSelectedChanges = false;
  bool _isPropertyTypeSelectedChanges = false;
  bool _isBuildingAreaChanges = false;
  bool _isSurfaceAreaChanges = false;
  bool _isTaksasiPriceChanges = false;
  bool _isSifatJaminanChanges = false;
  bool _isBuktiKepemilikanChanges = false;
  bool _isCertificateReleaseDateChanges = false;
  bool _isCertificateReleaseYearChanges = false;
  bool _isNamaPemegangHakChanges = false;
  bool _isNoSuratUkurChanges = false;
  bool _isDateOfMeasuringLetterChanges = false;
  bool _isCertificateOfMeasuringLetterChanges = false;
  bool _isDPJaminanChanges = false;
  bool _isPHMaxChanges = false;
  bool _isJarakFasumPositifChanges = false;
  bool _isJarakFasumNegatifChanges = false;
  bool _isHargaTanahChanges = false;
  bool _isHargaNJOPChanges = false;
  bool _isHargaBangunanChanges = false;
  bool _isTypeOfRoofSelectedChanges = false;
  bool _isWallTypeSelectedChanges = false;
  bool _isFloorTypeSelectedChanges = false;
  bool _isFoundationTypeSelectedChanges = false;
  bool _isStreetTypeSelectedChanges = false;
  bool _isRadioValueAccessCarChanges = false;
  bool _isJumlahRumahDalamRadiusChanges = false;
  bool _isMasaHakBerlakuChanges = false;
  bool _isNoIMBChanges = false;
  bool _isDateOfIMBChanges = false;
  bool _isLuasBangunanIMBChanges = false;
  bool _isUsageCollateralPropertyChanges = false;
  bool _isLTVChanges = false;
  bool _isRadioValueForAllUnitPropertyChanges = false;
  bool _isAddressTypeDakor = false;
  bool _isAddressDakor = false;
  bool _isRTDakor = false;
  bool _isRWDakor = false;
  bool _isKelurahanDakor = false;
  bool _isKecamatanDakor = false;
  bool _isKabKotDakor = false;
  bool _isProvinsiDakor = false;
  bool _isKodePosDakor = false;
  bool _isDataLatLongDakor = false;

  void checkDataDakor() async{
    var _check = await _dbHelper.selectDataCollaOto();
    var _properti = await _dbHelper.selectDataCollaProp();
    var _propAddress = await _dbHelper.selectMS2CustAddr("6");
    if(_lastKnownState == "DKR") {
      if(_check.isNotEmpty){
        // Otomotif
        _isRadioValueIsCollaNameSameWithApplicantOtoChanges = this._radioValueIsCollaNameSameWithApplicantOto != _check[0]['flag_coll_name_is_appl'] || _check[0]['edit_flag_coll_name_is_appl'] == "1";
        _isIdentityTypeSelectedAutoChanges = this._identityTypeSelectedAuto.id != _check[0]['id_type'] || _check[0]['edit_id_type'] == "1";
        _isIdentityNumberAutoChanges = this._controllerIdentityNumberAuto.text != _check[0]['id_no'] || _check[0]['edit_id_no'] == "1";
        _isNameOnCollateralAutoChanges = this._controllerNameOnCollateralAuto.text != _check[0]['colla_name'] || _check[0]['edit_colla_name'] == "1";
        _isBirthDateAutoChanges = this._initialDateForBirthDateAuto != DateTime.parse(_check[0]['date_of_birth']) || _check[0]['edit_date_of_birth'] == "1";
        _isBirthPlaceValidWithIdentityAutoChanges = this._controllerBirthPlaceValidWithIdentityAuto.text != _check[0]['place_of_birth'] || _check[0]['edit_place_of_birth'] == "1";
        _isBirthPlaceValidWithIdentityLOVAutoChanges = this._birthPlaceAutoSelected != null ? this._birthPlaceAutoSelected.KABKOT_ID != _check[0]['place_of_birth_kabkota'] : false || _check[0]['edit_place_of_birth_kabkota'] == "1";
        _isRadioValueIsCollateralSameWithUnitOtoChanges = this._radioValueIsCollateralSameWithUnitOto != _check[0]['flag_coll_is_unit'] || _check[0]['edit_flag_coll_is_unit'] == "1";
        _isGroupObjectChanges = this._groupObjectSelected.KODE != _check[0]['group_object'] || _check[0]['edit_group_object'] == "1";
        _isObjectChanges = this._objectSelected.id != _check[0]['object'] || _check[0]['edit_object'] == "1";
        _isTypeProductChanges = false; //this._productTypeSelected.id != _check[0]['product_type'] || _check[0]['edit_product_type'] == "1";
        _isBrandObjectChanges = this._brandObjectSelected.id != _check[0]['brand_object'] || _check[0]['edit_brand_object'] == "1";
        _isObjectTypeChanges = this._objectTypeSelected.id != _check[0]['object_type'] || _check[0]['edit_object_type'] == "1";
        _isModelObjectChanges = this._modelObjectSelected.id != _check[0]['object_model'] || _check[0]['edit_object_model'] == "1";
        _isUsageObjectModelChanges = this._objectUsageSelected.id != _check[0]['object_purpose'] || _check[0]['edit_object_purpose'] == "1";
        _isYearProductionSelectedChanges = this._yearProductionSelected != _check[0]['mfg_year'].toString() || _check[0]['edit_mfg_year'] == "1";
        _isYearRegistrationSelectedChanges = this._yearRegistrationSelected != _check[0]['registration_year'].toString() || _check[0]['edit_registration_year'] == "1";
        _isRadioValueYellowPlatChanges = this._radioValueYellowPlat != _check[0]['yellow_plate'] || _check[0]['edit_yellow_plate'] == "1";
        _isRadioValueBuiltUpNonATPMChanges = this._radioValueBuiltUpNonATPM != _check[0]['built_up'] || _check[0]['edit_built_up'] == "1";
        _isPoliceNumberChanges = this._controllerPoliceNumber.text != _check[0]['police_no'] || _check[0]['edit_police_no'] == "1";
        _isFrameNumberChanges = this._controllerFrameNumber.text != _check[0]['frame_no'] || _check[0]['edit_frame_no'] == "1";
        _isMachineNumberChanges = this._controllerMachineNumber.text != _check[0]['engine_no'] || _check[0]['edit_engine_no'] == "1";
        _isBPKPNumberChanges = this._controllerBPKPNumber.text.isEmpty ? false : this._controllerBPKPNumber.text != _check[0]['bpkb_no'] || _check[0]['edit_bpkb_no'] == "1";
        _isGradeUnitChanges = this._controllerGradeUnit.text != _check[0]['grade_unit'] || _check[0]['edit_grade_unit'] == "1";
        _isFasilitasUTJChanges = this._controllerFasilitasUTJ.text != _check[0]['utj_facilities'] || _check[0]['edit_utj_facilities'] == "1";
        _isNamaBidderChanges = this._controllerNamaBidder.text != _check[0]['bidder_name'] || _check[0]['edit_bidder_name'] == "1";
        _isHargaJualShowroomChanges = double.parse(this._controllerHargaJualShowroom.text.replaceAll(",", "")) != _check[0]['showroom_price'] || _check[0]['edit_showroom_price'] == "1";
        _isPerlengkapanTambahanChanges = this._controllerPerlengkapanTambahan.text != _check[0]['add_equip'] || _check[0]['edit_add_equip'] == "1";
        _isMPAdiraChanges = double.parse(this._controllerMPAdira.text.replaceAll(",", "")) != _check[0]['mp_adira'] || _check[0]['edit_mp_adira'] == "1";
        _isMPAdiraUploadChanges = double.parse(this._controllerMPAdiraUpload.text.replaceAll(",", "")) != _check[0]['mp_adira_upld'] || _check[0]['edit_mp_adira_upld'] == "1";
        _isRekondisiFisikChanges = double.parse(this._controllerRekondisiFisik.text.replaceAll(",", "")) != _check[0]['rekondisi_fisik'] || _check[0]['edit_rekondisi_fisik'] == "1";
        _isDpGuaranteeChanges = double.parse(this._controllerDPJaminan.text.replaceAll(",", "")) != _check[0]['dp_jaminan'] || _check[0]['edit_dp_jaminan'] == "1";
        _isPHMaxAutomotiveChanges = double.parse(this._controllerPHMaxAutomotive.text.replaceAll(",", "")) != _check[0]['max_ph'] || _check[0]['edit_max_ph'] == "1";
        _isTaksasiPriceAutomotiveChanges = _check[0]['taksasi_price'] != "null" ? double.parse(this._controllerTaksasiPriceAutomotive.text.replaceAll(",", "")) != _check[0]['taksasi_price'] : false || _check[0]['edit_taksasi_price'] == "1";
        _isRadioValueWorthyOrUnworthyChanges = this._radioValueWorthyOrUnworthy != _check[0]['result'] || _check[0]['edit_result'] == "1";
        _isUsageCollateralOtoChanges = this._collateralUsageOtoSelected.id != _check[0]['cola_purpose'] || _check[0]['edit_cola_purpose'] == "1";
        // _isRadioValueForAllUnitOtoChanges = this._radioValueForAllUnitOto != _check[0][''] || _check[0][''] == "1";
      }
      else if(_properti.isNotEmpty){
        // Property
        _isRadioValueIsCollateralSameWithApplicantPropertyChanges = this._radioValueIsCollateralSameWithApplicantProperty != _properti[0]['flag_coll_name_is_appl'] || _properti[0]['edit_flag_coll_name_is_appl'] == "1";
        _isIdentityModelChanges = this._identityModel.id != _properti[0]['id_type'] || _properti[0]['edit_id_type'] == "1";
        _isIdentityNumberPropChanges = this._controllerIdentityNumber.text != _properti[0]['id_no'] || _properti[0]['edit_id_no'] == "1";
        _isNameOnCollateralChanges = this._controllerNameOnCollateral.text != _properti[0]['cola_name'] || _properti[0]['edit_cola_name'] == "1";
        _isBirthDatePropChanges = this._controllerBirthDateProp != _properti[0]['date_of_birth'] || _properti[0]['edit_date_of_birth'] == "1";
        _isBirthPlaceValidWithIdentity1Changes = this._controllerBirthPlaceValidWithIdentity1.text != _properti[0]['place_of_birth'] || _properti[0]['edit_place_of_birth'] == "1";
        _isBirthPlaceValidWithIdentityLOVChanges = this._controllerBirthPlaceValidWithIdentityLOV.text != _properti[0]['place_of_birth_kabkota'] || _properti[0]['edit_place_of_birth_kabkota'] == "1";
        _isCertificateNumberChanges = this._controllerCertificateNumber.text != _properti[0]['sertifikat_no'] || _properti[0]['edit_sertifikat_no'] == "1";
        _isCertificateTypeSelectedChanges = this._certificateTypeSelected.id != _properti[0]['sertifikat_type'] || _properti[0]['edit_sertifikat_type'] == "1";
        _isPropertyTypeSelectedChanges = this._propertyTypeSelected.id != _properti[0]['property_type'] || _properti[0]['edit_property_type'] == "1";
        _isBuildingAreaChanges = this._controllerBuildingArea.text != _properti[0]['building_area'] || _properti[0]['edit_building_area'] == "1";
        _isSurfaceAreaChanges = this._controllerSurfaceArea.text != _properti[0]['land_area'] || _properti[0]['edit_land_area'] == "1";
        _isTaksasiPriceChanges = this._controllerTaksasiPrice.text != _properti[0]['taksasi_price'] || _properti[0]['edit_taksasi_price'] == "1";
        _isSifatJaminanChanges = this._controllerSifatJaminan.text != _properti[0]['guarantee_character'] || _properti[0]['edit_guarantee_character'] == "1";
        _isBuktiKepemilikanChanges = this._controllerBuktiKepemilikan.text != _properti[0]['grntr_evdnce_ownr'] || _properti[0]['edit_grntr_evdnce_ownr'] == "1";
        _isCertificateReleaseDateChanges = this._controllerCertificateReleaseDate.text != _properti[0]['pblsh_sertifikat_date'] || _properti[0]['edit_pblsh_sertifikat_date'] == "1";
        _isCertificateReleaseYearChanges = this._controllerCertificateReleaseYear.text != _properti[0]['pblsh_sertifikat_year'] || _properti[0]['edit_pblsh_sertifikat_year'] == "1";
        _isNamaPemegangHakChanges = this._controllerNamaPemegangHak.text != _properti[0]['license_name'] || _properti[0]['edit_license_name'] == "1";
        _isNoSuratUkurChanges = this._controllerNoSuratUkur.text != _properti[0]['no_srt_ukur'] || _properti[0]['edit_no_srt_ukur'] == "1";
        _isDateOfMeasuringLetterChanges = this._controllerDateOfMeasuringLetter.text != _properti[0]['tgl_srt_ukur'] || _properti[0]['edit_tgl_srt_ukur'] == "1";
        _isCertificateOfMeasuringLetterChanges = this._controllerCertificateOfMeasuringLetter.text != _properti[0]['srtfkt_pblsh_by'] || _properti[0]['edit_srtfkt_pblsh_by'] == "1";
        _isDPJaminanChanges = this._controllerDPJaminan.text != _properti[0]['dp_guarantee'] || _properti[0]['edit_dp_guarantee'] == "1";
        _isPHMaxChanges = this._controllerPHMax.text != _properti[0]['max_ph'] || _properti[0]['edit_max_ph'] == "1";
        _isJarakFasumPositifChanges = this._controllerJarakFasumPositif.text != _properti[0]['jarak_fasum_positif'] || _properti[0]['edit_jarak_fasum_positif'] == "1";
        _isJarakFasumNegatifChanges = this._controllerJarakFasumNegatif.text != _properti[0]['jarak_fasum_negatif'] || _properti[0]['edit_jarak_fasum_negatif'] == "1";
        _isHargaTanahChanges = this._controllerHargaTanah.text != _properti[0]['land_price'] || _properti[0]['edit_land_price'] == "1";
        _isHargaNJOPChanges = this._controllerHargaNJOP.text != _properti[0]['njop_price'] || _properti[0]['edit_njop_price'] == "1";
        _isHargaBangunanChanges = this._controllerHargaBangunan.text != _properti[0]['building_price'] || _properti[0]['edit_building_price'] == "1";
        _isTypeOfRoofSelectedChanges = this._typeOfRoofSelected.id != _properti[0]['roof_type'] || _properti[0]['edit_roof_type'] == "1";
        _isWallTypeSelectedChanges = this._wallTypeSelected.id != _properti[0]['wall_type'] || _properti[0]['edit_wall_type'] == "1";
        _isFloorTypeSelectedChanges = this._floorTypeSelected.id != _properti[0]['floor_type'] || _properti[0]['edit_floor_type'] == "1";
        _isFoundationTypeSelectedChanges = this._foundationTypeSelected.id != _properti[0]['foundation_type'] || _properti[0]['edit_foundation_type'] == "1";
        _isStreetTypeSelectedChanges = this._streetTypeSelected.id != _properti[0]['road_type'] || _properti[0]['edit_road_type'] == "1";
        _isRadioValueAccessCarChanges = this._radioValueAccessCar != _properti[0]['flag_car_pas'] || _properti[0]['edit_flag_car_pas'] == "1";
        _isJumlahRumahDalamRadiusChanges = this._controllerJumlahRumahDalamRadius.text != _properti[0]['no_of_house'] || _properti[0]['edit_no_of_house'] == "1";
        _isMasaHakBerlakuChanges = this._controllerMasaHakBerlaku.text != _properti[0]['expire_right'] || _properti[0]['edit_expire_right'] == "1";
        _isNoIMBChanges = this._controllerNoIMB.text != _properti[0]['imb_no'] || _properti[0]['edit_imb_no'] == "1";
        _isDateOfIMBChanges = this._controllerDateOfIMB.text != _properti[0]['imb_date'] || _properti[0]['edit_imb_date'] == "1";
        _isLuasBangunanIMBChanges = this._controllerLuasBangunanIMB.text != _properti[0]['size_of_imb'] || _properti[0]['edit_size_of_imb'] == "1";
        _isUsageCollateralPropertyChanges = this._controllerUsageCollateralProperty.text != _properti[0]['cola_purpose'] || _properti[0]['edit_cola_purpose'] == "1";
        _isLTVChanges = this._controllerLTV.text != _properti[0]['ltv'] || _properti[0]['edit_ltv'] == "1";
        // _isRadioValueForAllUnitPropertyChanges = this._radioValueForAllUnitProperty != _properti[0][''] || _properti[0][''] == "1";

        if(_propAddress.isNotEmpty){
          if(this._addressTypeSelected != null){
            _isAddressTypeDakor = this._addressTypeSelected.DESKRIPSI != _propAddress[0]['addr_desc'] || _propAddress[0]['edit_addr_type'] == "1";
          }
          _isAddressDakor = this._controllerAddress.text != _propAddress[0]['address'] || _propAddress[0]['edit_address'] == "1";
          _isRTDakor = this._controllerRT.text != _propAddress[0]['rt'] || _propAddress[0]['edit_rt'] == "1";
          _isRWDakor = this._controllerRW.text != _propAddress[0]['rw'] || _propAddress[0]['edit_rw'] == "1";
          _isKelurahanDakor = this._controllerKelurahan.text != _propAddress[0]['kelurahan_desc'] || _propAddress[0]['edit_kelurahan'] == "1";
          _isKecamatanDakor = this._controllerKecamatan.text != _propAddress[0]['kecamatan_desc'] || _propAddress[0]['edit_kecamatan'] == "1";
          _isKabKotDakor = this._controllerKota.text != _propAddress[0]['kabkot_desc'] || _propAddress[0]['edit_kabkot'] == "1";
          _isProvinsiDakor = this._controllerProv.text != _propAddress[0]['provinsi_desc'] || _propAddress[0]['edit_provinsi'] == "1";
          _isKodePosDakor = this._controllerPostalCode.text != _propAddress[0]['zip_code'] || _propAddress[0]['edit_zip_code'] == "1";
          _isDataLatLongDakor = this._controllerAddressFromMap.text != _propAddress[0]['address_from_map'] || _propAddress[0]['edit_address_from_map'] == "1";
        }
      }
    }
  }

  bool get isRadioValueIsCollaNameSameWithApplicantOtoChanges => _isRadioValueIsCollaNameSameWithApplicantOtoChanges;

  set isRadioValueIsCollaNameSameWithApplicantOtoChanges(bool value) {
    this._isRadioValueIsCollaNameSameWithApplicantOtoChanges = value;
    notifyListeners();
  }

  bool get isRadioValueForAllUnitOtoChanges => _isRadioValueForAllUnitOtoChanges;

  set isRadioValueForAllUnitOtoChanges(bool value) {
    this._isRadioValueForAllUnitOtoChanges = value;
    notifyListeners();
  }

  bool get isUsageCollateralOtoChanges => _isUsageCollateralOtoChanges;

  set isUsageCollateralOtoChanges(bool value) {
    this._isUsageCollateralOtoChanges = value;
    notifyListeners();
  }

  bool get isRadioValueWorthyOrUnworthyChanges => _isRadioValueWorthyOrUnworthyChanges;

  set isRadioValueWorthyOrUnworthyChanges(bool value) {
    this._isRadioValueWorthyOrUnworthyChanges = value;
    notifyListeners();
  }

  bool get isTaksasiPriceAutomotiveChanges => _isTaksasiPriceAutomotiveChanges;

  set isTaksasiPriceAutomotiveChanges(bool value) {
    this._isTaksasiPriceAutomotiveChanges = value;
    notifyListeners();
  }

  bool get isPHMaxAutomotiveChanges => _isPHMaxAutomotiveChanges;

  set isPHMaxAutomotiveChanges(bool value) {
    this._isPHMaxAutomotiveChanges = value;
    notifyListeners();
  }

  bool get isDpGuaranteeChanges => _isDpGuaranteeChanges;

  set isDpGuaranteeChanges(bool value) {
    this._isDpGuaranteeChanges = value;
    notifyListeners();
  }

  bool get isRekondisiFisikChanges => _isRekondisiFisikChanges;

  set isRekondisiFisikChanges(bool value) {
    this._isRekondisiFisikChanges = value;
    notifyListeners();
  }

  bool get isMPAdiraChanges => _isMPAdiraChanges;

  set isMPAdiraChanges(bool value) {
    this._isMPAdiraChanges = value;
    notifyListeners();
  }

  bool get isMPAdiraUploadChanges => _isMPAdiraUploadChanges;

  set isMPAdiraUploadChanges(bool value) {
    this._isMPAdiraUploadChanges = value;
    notifyListeners();
  }

  bool get isPerlengkapanTambahanChanges => _isPerlengkapanTambahanChanges;

  set isPerlengkapanTambahanChanges(bool value) {
    this._isPerlengkapanTambahanChanges = value;
    notifyListeners();
  }

  bool get isHargaJualShowroomChanges => _isHargaJualShowroomChanges;

  set isHargaJualShowroomChanges(bool value) {
    this._isHargaJualShowroomChanges = value;
    notifyListeners();
  }

  bool get isNamaBidderChanges => _isNamaBidderChanges;

  set isNamaBidderChanges(bool value) {
    this._isNamaBidderChanges = value;
    notifyListeners();
  }

  bool get isFasilitasUTJChanges => _isFasilitasUTJChanges;

  set isFasilitasUTJChanges(bool value) {
    this._isFasilitasUTJChanges = value;
    notifyListeners();
  }

  bool get isGradeUnitChanges => _isGradeUnitChanges;

  set isGradeUnitChanges(bool value) {
    this._isGradeUnitChanges = value;
    notifyListeners();
  }

  bool get isBPKPNumberChanges => _isBPKPNumberChanges;

  set isBPKPNumberChanges(bool value) {
    this._isBPKPNumberChanges = value;
    notifyListeners();
  }

  bool get isMachineNumberChanges => _isMachineNumberChanges;

  set isMachineNumberChanges(bool value) {
    this._isMachineNumberChanges = value;
    notifyListeners();
  }

  bool get isFrameNumberChanges => _isFrameNumberChanges;

  set isFrameNumberChanges(bool value) {
    this._isFrameNumberChanges = value;
    notifyListeners();
  }

  bool get isPoliceNumberChanges => _isPoliceNumberChanges;

  set isPoliceNumberChanges(bool value) {
    this._isPoliceNumberChanges = value;
    notifyListeners();
  }

  bool get isRadioValueBuiltUpNonATPMChanges => _isRadioValueBuiltUpNonATPMChanges;

  set isRadioValueBuiltUpNonATPMChanges(bool value) {
    this._isRadioValueBuiltUpNonATPMChanges = value;
    notifyListeners();
  }

  bool get isRadioValueYellowPlatChanges => _isRadioValueYellowPlatChanges;

  set isRadioValueYellowPlatChanges(bool value) {
    this._isRadioValueYellowPlatChanges = value;
    notifyListeners();
  }

  bool get isYearRegistrationSelectedChanges => _isYearRegistrationSelectedChanges;

  set isYearRegistrationSelectedChanges(bool value) {
    this._isYearRegistrationSelectedChanges = value;
    notifyListeners();
  }

  bool get isYearProductionSelectedChanges => _isYearProductionSelectedChanges;

  set isYearProductionSelectedChanges(bool value) {
    this._isYearProductionSelectedChanges = value;
    notifyListeners();
  }

  bool get isUsageObjectModelChanges => _isUsageObjectModelChanges;

  set isUsageObjectModelChanges(bool value) {
    this._isUsageObjectModelChanges = value;
    notifyListeners();
  }

  bool get isModelObjectChanges => _isModelObjectChanges;

  set isModelObjectChanges(bool value) {
    this._isModelObjectChanges = value;
    notifyListeners();
  }

  bool get isObjectTypeChanges => _isObjectTypeChanges;

  set isObjectTypeChanges(bool value) {
    this._isObjectTypeChanges = value;
    notifyListeners();
  }

  bool get isBrandObjectChanges => _isBrandObjectChanges;

  set isBrandObjectChanges(bool value) {
    this._isBrandObjectChanges = value;
    notifyListeners();
  }

  bool get isTypeProductChanges => _isTypeProductChanges;

  set isTypeProductChanges(bool value) {
    this._isTypeProductChanges = value;
    notifyListeners();
  }

  bool get isObjectChanges => _isObjectChanges;

  set isObjectChanges(bool value) {
    this._isObjectChanges = value;
    notifyListeners();
  }

  bool get isGroupObjectChanges => _isGroupObjectChanges;

  set isGroupObjectChanges(bool value) {
    this._isGroupObjectChanges = value;
    notifyListeners();
  }

  bool get isRadioValueIsCollateralSameWithUnitOtoChanges => _isRadioValueIsCollateralSameWithUnitOtoChanges;

  set isRadioValueIsCollateralSameWithUnitOtoChanges(bool value) {
    this._isRadioValueIsCollateralSameWithUnitOtoChanges = value;
    notifyListeners();
  }

  bool get isBirthPlaceValidWithIdentityAutoChanges => _isBirthPlaceValidWithIdentityAutoChanges;

  set isBirthPlaceValidWithIdentityAutoChanges(bool value) {
    this._isBirthPlaceValidWithIdentityAutoChanges = value;
    notifyListeners();
  }

  bool get isBirthPlaceValidWithIdentityLOVAutoChanges => _isBirthPlaceValidWithIdentityLOVAutoChanges;

  set isBirthPlaceValidWithIdentityLOVAutoChanges(bool value) {
    this._isBirthPlaceValidWithIdentityLOVAutoChanges = value;
    notifyListeners();
  }

  bool get isBirthDateAutoChanges => _isBirthDateAutoChanges;

  set isBirthDateAutoChanges(bool value) {
    this._isBirthDateAutoChanges = value;
    notifyListeners();
  }

  bool get isNameOnCollateralAutoChanges => _isNameOnCollateralAutoChanges;

  set isNameOnCollateralAutoChanges(bool value) {
    this._isNameOnCollateralAutoChanges = value;
    notifyListeners();
  }

  bool get isIdentityNumberAutoChanges => _isIdentityNumberAutoChanges;

  set isIdentityNumberAutoChanges(bool value) {
    this._isIdentityNumberAutoChanges = value;
    notifyListeners();
  }

  bool get isIdentityTypeSelectedAutoChanges => _isIdentityTypeSelectedAutoChanges;

  set isIdentityTypeSelectedAutoChanges(bool value) {
    this._isIdentityTypeSelectedAutoChanges = value;
    notifyListeners();
  }

  // Properti
  bool get isRadioValueForAllUnitPropertyChanges => _isRadioValueForAllUnitPropertyChanges;

  set isRadioValueForAllUnitPropertyChanges(bool value) {
    this._isRadioValueForAllUnitPropertyChanges = value;
    notifyListeners();
  }

  bool get isLTVChanges => _isLTVChanges;

  set isLTVChanges(bool value) {
    this._isLTVChanges = value;
    notifyListeners();
  }

  bool get isUsageCollateralPropertyChanges => _isUsageCollateralPropertyChanges;

  set isUsageCollateralPropertyChanges(bool value) {
    this._isUsageCollateralPropertyChanges = value;
    notifyListeners();
  }

  bool get isLuasBangunanIMBChanges => _isLuasBangunanIMBChanges;

  set isLuasBangunanIMBChanges(bool value) {
    this._isLuasBangunanIMBChanges = value;
    notifyListeners();
  }

  bool get isDateOfIMBChanges => _isDateOfIMBChanges;

  set isDateOfIMBChanges(bool value) {
    this._isDateOfIMBChanges = value;
    notifyListeners();
  }

  bool get isNoIMBChanges => _isNoIMBChanges;

  set isNoIMBChanges(bool value) {
    this._isNoIMBChanges = value;
    notifyListeners();
  }

  bool get isMasaHakBerlakuChanges => _isMasaHakBerlakuChanges;

  set isMasaHakBerlakuChanges(bool value) {
    this._isMasaHakBerlakuChanges = value;
    notifyListeners();
  }

  bool get isJumlahRumahDalamRadiusChanges => _isJumlahRumahDalamRadiusChanges;

  set isJumlahRumahDalamRadiusChanges(bool value) {
    this._isJumlahRumahDalamRadiusChanges = value;
    notifyListeners();
  }

  bool get isRadioValueAccessCarChanges => _isRadioValueAccessCarChanges;

  set isRadioValueAccessCarChanges(bool value) {
    this._isRadioValueAccessCarChanges = value;
    notifyListeners();
  }

  bool get isStreetTypeSelectedChanges => _isStreetTypeSelectedChanges;

  set isStreetTypeSelectedChanges(bool value) {
    this._isStreetTypeSelectedChanges = value;
    notifyListeners();
  }

  bool get isFoundationTypeSelectedChanges => _isFoundationTypeSelectedChanges;

  set isFoundationTypeSelectedChanges(bool value) {
    this._isFoundationTypeSelectedChanges = value;
    notifyListeners();
  }

  bool get isFloorTypeSelectedChanges => _isFloorTypeSelectedChanges;

  set isFloorTypeSelectedChanges(bool value) {
    this._isFloorTypeSelectedChanges = value;
    notifyListeners();
  }

  bool get isWallTypeSelectedChanges => _isWallTypeSelectedChanges;

  set isWallTypeSelectedChanges(bool value) {
    this._isWallTypeSelectedChanges = value;
    notifyListeners();
  }

  bool get isTypeOfRoofSelectedChanges => _isTypeOfRoofSelectedChanges;

  set isTypeOfRoofSelectedChanges(bool value) {
    this._isTypeOfRoofSelectedChanges = value;
    notifyListeners();
  }

  bool get isHargaBangunanChanges => _isHargaBangunanChanges;

  set isHargaBangunanChanges(bool value) {
    this._isHargaBangunanChanges = value;
    notifyListeners();
  }

  bool get isHargaNJOPChanges => _isHargaNJOPChanges;

  set isHargaNJOPChanges(bool value) {
    this._isHargaNJOPChanges = value;
    notifyListeners();
  }

  bool get isJarakFasumNegatifChanges => _isJarakFasumNegatifChanges;

  set isJarakFasumNegatifChanges(bool value) {
    this._isJarakFasumNegatifChanges = value;
    notifyListeners();
  }

  bool get isJarakFasumPositifChanges => _isJarakFasumPositifChanges;

  set isJarakFasumPositifChanges(bool value) {
    this._isJarakFasumPositifChanges = value;
    notifyListeners();
  }

  bool get isPHMaxChanges => _isPHMaxChanges;

  set isPHMaxChanges(bool value) {
    this._isPHMaxChanges = value;
    notifyListeners();
  }

  bool get isDPJaminanChanges => _isDPJaminanChanges;

  set isDPJaminanChanges(bool value) {
    this._isDPJaminanChanges = value;
    notifyListeners();
  }

  bool get isCertificateOfMeasuringLetterChanges => _isCertificateOfMeasuringLetterChanges;

  set isCertificateOfMeasuringLetterChanges(bool value) {
    this._isCertificateOfMeasuringLetterChanges = value;
    notifyListeners();
  }

  bool get isDateOfMeasuringLetterChanges => _isDateOfMeasuringLetterChanges;

  set isDateOfMeasuringLetterChanges(bool value) {
    this._isDateOfMeasuringLetterChanges = value;
    notifyListeners();
  }

  bool get isNoSuratUkurChanges => _isNoSuratUkurChanges;

  set isNoSuratUkurChanges(bool value) {
    this._isNoSuratUkurChanges = value;
    notifyListeners();
  }

  bool get isNamaPemegangHakChanges => _isNamaPemegangHakChanges;

  set isNamaPemegangHakChanges(bool value) {
    this._isNamaPemegangHakChanges = value;
    notifyListeners();
  }

  bool get isCertificateReleaseYearChanges => _isCertificateReleaseYearChanges;

  set isCertificateReleaseYearChanges(bool value) {
    this._isCertificateReleaseYearChanges = value;
    notifyListeners();
  }

  bool get isCertificateReleaseDateChanges => _isCertificateReleaseDateChanges;

  set isCertificateReleaseDateChanges(bool value) {
    this._isCertificateReleaseDateChanges = value;
    notifyListeners();
  }

  bool get isBuktiKepemilikanChanges => _isBuktiKepemilikanChanges;

  set isBuktiKepemilikanChanges(bool value) {
    this._isBuktiKepemilikanChanges = value;
    notifyListeners();
  }

  bool get isSifatJaminanChanges => _isSifatJaminanChanges;

  set isSifatJaminanChanges(bool value) {
    this._isSifatJaminanChanges = value;
    notifyListeners();
  }

  bool get isTaksasiPriceChanges => _isTaksasiPriceChanges;

  set isTaksasiPriceChanges(bool value) {
    this._isTaksasiPriceChanges = value;
    notifyListeners();
  }

  bool get isSurfaceAreaChanges => _isSurfaceAreaChanges;

  set isSurfaceAreaChanges(bool value) {
    this._isSurfaceAreaChanges = value;
    notifyListeners();
  }

  bool get isBuildingAreaChanges => _isBuildingAreaChanges;

  set isBuildingAreaChanges(bool value) {
    this._isBuildingAreaChanges = value;
    notifyListeners();
  }

  bool get isPropertyTypeSelectedChanges => _isPropertyTypeSelectedChanges;

  set isPropertyTypeSelectedChanges(bool value) {
    this._isPropertyTypeSelectedChanges = value;
    notifyListeners();
  }

  bool get isCertificateTypeSelectedChanges => _isCertificateTypeSelectedChanges;

  set isCertificateTypeSelectedChanges(bool value) {
    this._isCertificateTypeSelectedChanges = value;
    notifyListeners();
  }

  bool get isCertificateNumberChanges => _isCertificateNumberChanges;

  set isCertificateNumberChanges(bool value) {
    this._isCertificateNumberChanges = value;
    notifyListeners();
  }

  bool get isBirthPlaceValidWithIdentityLOVChanges => _isBirthPlaceValidWithIdentityLOVChanges;

  set isBirthPlaceValidWithIdentityLOVChanges(bool value) {
    this._isBirthPlaceValidWithIdentityLOVChanges = value;
    notifyListeners();
  }

  bool get isBirthPlaceValidWithIdentity1Changes => _isBirthPlaceValidWithIdentity1Changes;

  set isBirthPlaceValidWithIdentity1Changes(bool value) {
    this._isBirthPlaceValidWithIdentity1Changes = value;
    notifyListeners();
  }

  bool get isBirthDatePropChanges => _isBirthDatePropChanges;

  set isBirthDatePropChanges(bool value) {
    this._isBirthDatePropChanges = value;
    notifyListeners();
  }

  bool get isNameOnCollateralChanges => _isNameOnCollateralChanges;

  set isNameOnCollateralChanges(bool value) {
    this._isNameOnCollateralChanges = value;
    notifyListeners();
  }

  bool get isIdentityModelChanges => _isIdentityModelChanges;

  set isIdentityModelChanges(bool value) {
    this._isIdentityModelChanges = value;
    notifyListeners();
  }

  bool get isRadioValueIsCollateralSameWithApplicantPropertyChanges => _isRadioValueIsCollateralSameWithApplicantPropertyChanges;

  set isRadioValueIsCollateralSameWithApplicantPropertyChanges(bool value) {
    this._isRadioValueIsCollateralSameWithApplicantPropertyChanges = value;
    notifyListeners();
  }

  bool get isHargaTanahChanges => _isHargaTanahChanges;

  set isHargaTanahChanges(bool value) {
    this._isHargaTanahChanges = value;
    notifyListeners();
  }

  bool get isIdentityNumberPropChanges => _isIdentityNumberPropChanges;

  set isIdentityNumberPropChanges(bool value) {
    this._isIdentityNumberPropChanges = value;
    notifyListeners();
  }

  bool get isDataLatLongDakor => _isDataLatLongDakor;

  set isDataLatLongDakor(bool value) {
    _isDataLatLongDakor = value;
    notifyListeners();
  }

  bool get isKodePosDakor => _isKodePosDakor;

  set isKodePosDakor(bool value) {
    _isKodePosDakor = value;
    notifyListeners();
  }

  bool get isProvinsiDakor => _isProvinsiDakor;

  set isProvinsiDakor(bool value) {
    _isProvinsiDakor = value;
    notifyListeners();
  }

  bool get isKabKotDakor => _isKabKotDakor;

  set isKabKotDakor(bool value) {
    _isKabKotDakor = value;
    notifyListeners();
  }

  bool get isKecamatanDakor => _isKecamatanDakor;

  set isKecamatanDakor(bool value) {
    _isKecamatanDakor = value;
    notifyListeners();
  }

  bool get isKelurahanDakor => _isKelurahanDakor;

  set isKelurahanDakor(bool value) {
    _isKelurahanDakor = value;
    notifyListeners();
  }

  bool get isRWDakor => _isRWDakor;

  set isRWDakor(bool value) {
    _isRWDakor = value;
    notifyListeners();
  }

  bool get isRTDakor => _isRTDakor;

  set isRTDakor(bool value) {
    _isRTDakor = value;
    notifyListeners();
  }

  bool get isAddressDakor => _isAddressDakor;

  set isAddressDakor(bool value) {
    _isAddressDakor = value;
    notifyListeners();
  }

  bool get isAddressTypeDakor => _isAddressTypeDakor;

  set isAddressTypeDakor(bool value) {
    _isAddressTypeDakor = value;
    notifyListeners();
  }

  LookupUTJModel get lookupUTJModel => _lookupUTJModel;

  set lookupUTJModel(LookupUTJModel value) {
    _lookupUTJModel = value;
    notifyListeners();
  }

  void countPHMax(){
    var _showroom = this._controllerHargaJualShowroom.text.isNotEmpty ? double.parse(this._controllerHargaJualShowroom.text.replaceAll(",", "")) : 0.0;
    var _dp = this._controllerDPJaminan.text.isNotEmpty ? double.parse(this._controllerDPJaminan.text.replaceAll(",", "")) : 0.0;
    if(_dp > _showroom){
      this._controllerDPJaminan.text = "0.00";
      showSnackBar("Nilai DP tidak boleh melebihi nilai Showroom");
    } else {
      var _totalPrice = _showroom - _dp;
      this._controllerPHMaxAutomotive.text = _formatCurrency.formatCurrency(_totalPrice.toString());
    }
    notifyListeners();
  }

  void countTaksasi(){
    var _mpAdira = this._controllerMPAdira.text.isNotEmpty ? double.parse(this._controllerMPAdira.text.replaceAll(",", "")) : 0.0;
    var _rekondisiFisik = this._controllerRekondisiFisik.text.isNotEmpty ? double.parse(this._controllerRekondisiFisik.text.replaceAll(",", "")) : 0.0;
    if(_rekondisiFisik > _mpAdira){
      this._controllerRekondisiFisik.text = "0.00";
      showSnackBar("Nilai Rekondisi fisik tidak boleh melebihi nilai MP Adira");
    } else {
      var _totalPrice = _mpAdira - _rekondisiFisik;
      this._controllerTaksasiPriceAutomotive.text = _formatCurrency.formatCurrency(_totalPrice.toString());
    }
    notifyListeners();
  }

  void searchLookupUTJ(BuildContext context) async{ // khusus bekas
    var _providerLookUpUTJ = Provider.of<FormMLookupUTJChangeNotif>(context, listen: false);
    _providerLookUpUTJ.controllerPoliceNumber.text = this._controllerPoliceNumber.text;
    _providerLookUpUTJ.controllerFrameNumber.text = this._controllerFrameNumber.text;
    _providerLookUpUTJ.controllerMachineNumber.text = this._controllerMachineNumber.text;
    _providerLookUpUTJ.controllerBPKPNumber.text = this._controllerBPKPNumber.text;
    LookupUTJModel _data = await Navigator.push(context, MaterialPageRoute(builder: (context) => LookUpUTJ()));
    if(_data != null){
      this._lookupUTJModel = _data;
      if(_lookupUTJModel.COLA_PLAT_NO == null && _lookupUTJModel.COLA_FRAME_NO == null && _lookupUTJModel.COLA_ENG_NO == null && _lookupUTJModel.COLA_BPKB_NO == null){
        this._controllerPoliceNumber.text = _providerLookUpUTJ.controllerPoliceNumber.text;
        this._controllerFrameNumber.text = _providerLookUpUTJ.controllerFrameNumber.text;
        this._controllerMachineNumber.text = _providerLookUpUTJ.controllerMachineNumber.text;
        this._controllerBPKPNumber.text = _providerLookUpUTJ.controllerBPKPNumber.text;
        this._controllerGradeUnit.text = "-";
        this._controllerFasilitasUTJ.text = "-";
        this._controllerNamaBidder.text = "- / 0.00 / 0.00";
        // this._controllerHargaJualShowroom.text = "0.00";
      }
      else{
        this._controllerPoliceNumber.text = _lookupUTJModel.COLA_PLAT_NO;
        this._controllerFrameNumber.text = _lookupUTJModel.COLA_FRAME_NO;
        this._controllerMachineNumber.text = _lookupUTJModel.COLA_ENG_NO;
        this._controllerBPKPNumber.text = _lookupUTJModel.COLA_BPKB_NO;
        for(int i=0; i<_listProductionYear.length; i++){
          if(_lookupUTJModel.COLA_YR_PRODUCT.replaceAll(".0", "") == _listProductionYear[i]){
            _yearProductionSelected = _listProductionYear[i];
          }
        }
        this._controllerGradeUnit.text = _lookupUTJModel.AREC_GRADE;
        this._controllerFasilitasUTJ.text = _lookupUTJModel.FAS_UKJ;
        this._controllerNamaBidder.text = "${_lookupUTJModel.NAMA_BIDDER} / ${_formatCurrency.formatCurrency(_lookupUTJModel.HARGA_PASAR.replaceAll(",", ""))} / ${_formatCurrency.formatCurrency(_lookupUTJModel.HARGA_JUAL.replaceAll(",", ""))}";
        // this._controllerHargaJualShowroom.text = _formatCurrency.formatCurrency(_lookupUTJModel.HARGA_PASAR.replaceAll(",", ""));
        this._controllerMPAdira.text = _formatCurrency.formatCurrency(_lookupUTJModel.HARGA_JUAL.replaceAll(",", ""));
        this._controllerPerlengkapanTambahan.text = "-";
        this._controllerRekondisiFisik.text = "0";
        this._controllerDPJaminan.text = "0";
        countPHMax();
        countTaksasi();
      }
    }
    // check(context, 1);
    notifyListeners();
  }

  bool get isDisablePACIAAOSCONA => _isDisablePACIAAOSCONA;

  set isDisablePACIAAOSCONA(bool value) {
    this._isDisablePACIAAOSCONA = value;
    notifyListeners();
  }
}