import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/form_m_pekerjaan_model.dart';
import 'package:ad1ms2_dev/models/karoseri_model.dart';
import 'package:ad1ms2_dev/models/ms2_objt_karoseri_model.dart';
import 'package:ad1ms2_dev/screens/form_m/menu_detail_loan.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_credit_subsidy_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_individu_change_notifier/form_m_parent_individu_change_notifier.dart';
import 'package:ad1ms2_dev/shared/resource/submit_data_partial.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:ad1ms2_dev/models/karoseri_object_model.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../form_m_company_parent_change_notifier.dart';
import '../format_currency.dart';
import 'info_major_insurance_change_notifier.dart';

class InfoObjectKaroseriChangeNotifier with ChangeNotifier{
    List<KaroseriObjectModel> _listKaroseriObject = [];
    bool _autoValidate = false;
    DbHelper _dbHelper = DbHelper();
    bool _isKaroseriVisible = false;
    bool _isVisible = true;
    SubmitDataPartial _submitDataPartial = SubmitDataPartial();
    FormatCurrency _formatCurrency = FormatCurrency();
    String _custType;
    String _lastKnownState;
    bool _isDisablePACIAAOSCONA = false;

    List<KaroseriObjectModel> get listFormKaroseriObject => _listKaroseriObject;

    bool get autoValidate => _autoValidate;

    set autoValidate(bool value) {
        this._autoValidate = value;
        notifyListeners();
    }

    FormatCurrency get formatCurrency => _formatCurrency;

    void addListInfoObjKaroseri(KaroseriObjectModel value) {
        _listKaroseriObject.add(value);
        if (this._autoValidate) autoValidate = false;
        notifyListeners();
    }

    Future<void> setPreference() async {
        SharedPreferences _preference = await SharedPreferences.getInstance();
        this._custType = _preference.getString("cust_type");
        this._lastKnownState = _preference.getString("last_known_state");
        if(this._lastKnownState == "AOS" || this._lastKnownState == "CONA" || this._lastKnownState == "IA" || this._lastKnownState == "PAC") {
            this._isDisablePACIAAOSCONA = true;
        }
    }

    void deleteListInfoObjKaroseri(BuildContext context, int index) {
        showDialog(
            context: context,
            barrierDismissible: true,
            builder: (BuildContext context){
                return Theme(
                    data: ThemeData(
                        fontFamily: "NunitoSans",
                        primaryColor: Colors.black,
                        primarySwatch: primaryOrange,
                        accentColor: myPrimaryColor
                    ),
                    child: AlertDialog(
                        title: Text("Warning", style: TextStyle(fontWeight: FontWeight.bold)),
                        content: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                                Text("Apakah kamu yakin menghapus karoseri ini?",),
                            ],
                        ),
                        actions: <Widget>[
                            new FlatButton(
                                onPressed: () {
                                    this._listKaroseriObject.removeAt(index);
                                    notifyListeners();
                                    Navigator.pop(context);
                                },
                                child: new Text('Ya', style: TextStyle(color: Colors.grey),),
                            ),
                            new FlatButton(
                                onPressed: () => Navigator.of(context).pop(true),
                                child: new Text('Tidak'),
                            ),
                        ],
                    ),
                );
            }
        );
    }

    void updateListInfoObjKaroseri(KaroseriObjectModel mInfoObjKaroseri, BuildContext context, int index) {
        this._listKaroseriObject[index] = mInfoObjKaroseri;
        notifyListeners();
    }

    void clearDataInfoObjectKaroseri(){
        this._autoValidate = false;
        this._listKaroseriObject.clear();
        isDisablePACIAAOSCONA = false;
    }

    List<MS2ObjtKaroseriModel> _listObjtKaroseri = [];

    List<MS2ObjtKaroseriModel> get listObjtKaroseri => _listObjtKaroseri;

    Future<void> saveToSQLite() async {
        print("save karoseri");
        SharedPreferences _preferences = await SharedPreferences.getInstance();
        List<MS2ObjtKaroseriModel> _listData = [];
        for(int i=0; i<this._listKaroseriObject.length; i++){
            _listData.add(MS2ObjtKaroseriModel(
                "1",
                this._listKaroseriObject[i].orderKaroseriID != null ? this._listKaroseriObject[i].orderKaroseriID : "NEW",
                this._listKaroseriObject[i].PKSKaroseri != null ? this._listKaroseriObject[i].PKSKaroseri : "",
                this._listKaroseriObject[i].company != null ? this._listKaroseriObject[i].company.id : "",
                this._listKaroseriObject[i].company != null ? this._listKaroseriObject[i].company.desc : "",
                this._listKaroseriObject[i].karoseri != null ? this._listKaroseriObject[i].karoseri.kode : "",
                this._listKaroseriObject[i].karoseri != null ? this._listKaroseriObject[i].karoseri.deskripsi : "",
                this._listKaroseriObject[i].price != null ? double.parse(this._listKaroseriObject[i].price.replaceAll(",", "")) : 0,
                this._listKaroseriObject[i].jumlahKaroseri != null ? int.parse(this._listKaroseriObject[i].jumlahKaroseri) : 0,
                this._listKaroseriObject[i].totalPrice != null ? double.parse(this._listKaroseriObject[i].totalPrice.replaceAll(",", "")) : 0,
                DateTime.now().toString(),
                _preferences.getString("username"),
                null,
                null,
                1,
                null,
                0,
                this._listKaroseriObject[i].isRadioValuePKSKaroseriChanges ? "1" : "0",
                this._listKaroseriObject[i].isCompanyChanges ? "1" : "0",
                this._listKaroseriObject[i].isKaroseriChanges ? "1" : "0",
                this._listKaroseriObject[i].isJumlahKaroseriChanges ? "1" : "0",
                this._listKaroseriObject[i].isPriceChanges ? "1" : "0",
                this._listKaroseriObject[i].isTotalPriceChanges ? "1" : "0",
            ));
        }
        _dbHelper.insertMS2ObjtKaroseri(_listData);
        // if(_listObjtKaroseri.isNotEmpty){
        //     String _message = await _submitDataPartial.submitKaroseri(7,_listObjtKaroseri);
        //     return _message;
        // }
        // else{
        //     return "Skip karoseri";
        // }
    }

    Future<bool> deleteSQLite() async{
        return await _dbHelper.deleteMS2ObjtKaroseri();
    }

    Future<void> setDataFromSQLite(BuildContext context, int index) async{
        SharedPreferences _preferences = await SharedPreferences.getInstance();
        var _check = await _dbHelper.selectMS2ObjtKaroseri();
        if(_check.isNotEmpty){
            for(int i=0; i<_check.length; i++){
                var _company = CompanyTypeModel(_check[i]['comp_karoseri'], _check[i]['comp_karoseri_desc']);
                var _karoseri = KaroseriModel(_check[i]['karoseri'], _check[i]['karoseri_desc']);
                _listKaroseriObject.add(
                    KaroseriObjectModel(
                        _check[i]['pks_karoseri'],
                        _company,
                        _karoseri,
                        _check[i]['karoseri_qty'].toString(),
                        _check[i]['price'].toString(),
                        _check[i]['total_karoseri'].toString(),
                        _check[i]['edit_pks_karoseri'] == "1" || _check[i]['edit_comp_karoseri'] == "1" ||
                        _check[i]['edit_karoseri'] == "1" || _check[i]['edit_karoseri_qty'] == "1" ||
                        _check[i]['edit_price'] == "1" || _check[i]['edit_total_karoseri'] == "1",
                        _check[i]['edit_pks_karoseri'] == "1",
                        _check[i]['edit_comp_karoseri'] == "1",
                        _check[i]['edit_karoseri'] == "1",
                        _check[i]['edit_karoseri_qty'] == "1",
                        _check[i]['edit_price'] == "1",
                        _check[i]['edit_total_karoseri'] == "1",
                        _check[i]['orderKaroseriID']
                    )
                );
            }
        }

        if(_preferences.getString("last_known_state") == "IDE" && index != null){
            MenuDetailLoanState _menu = MenuDetailLoanState();
            if(index != 7){
                if(_preferences.getString("cust_type") == "PER"){
                    Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).isInfoObjKaroseriDone = true;
                    Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).selectedIndex +=1;
                } else {
                    Provider.of<FormMParentCompanyChangeNotifier>(context,listen: false).isInfoObjKaroseriDone = true;
                    Provider.of<FormMParentCompanyChangeNotifier>(context,listen: false).selectedIndex +=1;
                }
                debugPrint("KAROSERI_NEXT_STATE");
                await _menu.setNextState(context, index);
            }
            else{
                if(_preferences.getString("cust_type") == "PER"){
                    Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).selectedIndex = 7;
                } else {
                    Provider.of<FormMParentCompanyChangeNotifier>(context,listen: false).selectedIndex = 7;
                }
            }
        }
        notifyListeners();
    }

    bool get isKaroseriVisible => _isKaroseriVisible;

    set isKaroseriVisible(bool value) {
      this._isKaroseriVisible = value;
      notifyListeners();
    }

    void checkKaroseriNeedVisible(String kode){
        if(kode == "002"){
            isKaroseriVisible = true;
        }
        else{
            isKaroseriVisible = false;
        }
    }

    bool get isVisible => _isVisible;

    set isVisible(bool value) {
      this._isVisible = value;
    }

    void setIsVisible(String id){
        if(id != null){
            if(id == "002"){
                isVisible = true;
            }
            else{
                isVisible =  false;
            }
        }
    }

    String get lastKnownState => _lastKnownState;

    set lastKnownState(String value) {
        this.lastKnownState = value;
        notifyListeners();
    }

    String get custType => _custType;

    set custType(String value) {
        this._custType = value;
        notifyListeners();
    }

    bool get isDisablePACIAAOSCONA => _isDisablePACIAAOSCONA;

    set isDisablePACIAAOSCONA(bool value) {
        this._isDisablePACIAAOSCONA = value;
        notifyListeners();
    }
}