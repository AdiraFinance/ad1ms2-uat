import 'dart:collection';

import 'package:ad1ms2_dev/models/collateral_type_model.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';

class SearchCollateralTypeChangeNotifier with ChangeNotifier{
  bool _showClear = false;
  TextEditingController _controllerSearch = TextEditingController();

  List<CollateralTypeModel> _listCollateralType = [
    CollateralTypeModel("001", "AUTOMOTIVE"),
    CollateralTypeModel("002", "PROPERTY"),
  ];

  List<CollateralTypeModel> _listCollateralTypeDurable = [
    CollateralTypeModel("003", "DURABLE/TIDAK ADA"),
  ];

  List<CollateralTypeModel> _listCollateralTypePaket = [
    CollateralTypeModel("001", "AUTOMOTIVE"),
    CollateralTypeModel("002", "PROPERTY"),
    CollateralTypeModel("003", "DURABLE/TIDAK ADA"),
  ];

  UnmodifiableListView<CollateralTypeModel> get listCollateralType {
    return UnmodifiableListView(this._listCollateralType);
  }

  UnmodifiableListView<CollateralTypeModel> get listCollateralTypeDurable {
    return UnmodifiableListView(this._listCollateralTypeDurable);
  }

  UnmodifiableListView<CollateralTypeModel> get listCollateralTypePaket {
    return UnmodifiableListView(this._listCollateralTypePaket);
  }

  TextEditingController get controllerSearch => _controllerSearch;

  bool get showClear => _showClear;

  set showClear(bool value) {
    this._showClear = value;
    notifyListeners();
  }

  void changeAction(String value) {
    if (value != "") {
      showClear = true;
    } else {
      showClear = false;
    }
  }
}