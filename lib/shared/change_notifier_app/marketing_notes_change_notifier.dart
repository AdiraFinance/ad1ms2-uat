import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/models/ms2_application_model.dart';
import 'package:ad1ms2_dev/models/show_mandatory_marketing_notes_model.dart';
import 'package:ad1ms2_dev/shared/dashboard/dashboard_change_notif.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../main.dart';
import 'info_credit_income_change_notifier.dart';
import 'info_credit_structure_change_notifier.dart';

class MarketingNotesChangeNotifier with ChangeNotifier{
  bool _autoValidate = false;
  TextEditingController _controllerMarketingNotes = TextEditingController();
  bool _isMarketingNotesDakor = false;
  String _custType;
  String _lastKnownState;
  DbHelper _dbHelper = DbHelper();
  bool _isMarketingNotesChanges = false;
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  bool _isDisablePACIAAOSCONA = false;

  bool get isDisablePACIAAOSCONA => _isDisablePACIAAOSCONA;

  set isDisablePACIAAOSCONA(bool value) {
    this._isDisablePACIAAOSCONA = value;
    notifyListeners();
  }

  TextEditingController get controllerMarketingNotes => _controllerMarketingNotes;

  void clearMarketingNotes(){
    this._autoValidate = false;
    this._controllerMarketingNotes.clear();
    isDisablePACIAAOSCONA = false;
  }

  bool get autoValidate => _autoValidate;

  set autoValidate(bool value) {
    this._autoValidate = value;
    notifyListeners();
  }

  bool get isMarketingNotesDakor => _isMarketingNotesDakor;

  set isMarketingNotesDakor(bool value) {
    _isMarketingNotesDakor = value;
    notifyListeners();
  }

  String get custType => _custType;

  set custType(String value) {
    this._custType = value;
    notifyListeners();
  }

  String get lastKnownState => _lastKnownState;

  set lastKnownState(String value) {
    this._lastKnownState = value;
    notifyListeners();
  }

  bool get isMarketingNotesChanges => _isMarketingNotesChanges;

  set isMarketingNotesChanges(bool value) {
    this._isMarketingNotesChanges = value;
  }

  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  Future<void> saveToSQLite() async {
    var _model = MS2ApplicationModel(
      null,null,null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null,
      this._controllerMarketingNotes.text,
      null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null,
      this._isMarketingNotesChanges ? "1" : "0"
    );
    _dbHelper.updateMS2Application(_model);
    // String _message = await _submitDataPartial.submitApplication(5,_model);
    // return _message;
  }

  Future<void> setDataFromSQLite() async{
    print("masuk set data sqlite marketing notes");
    var _check = await _dbHelper.selectMS2Application();
    if(_check.isNotEmpty){
      print("data marketing notes: ${_check[0]['marketing_notes']}");
      if(_check[0]['marketing_notes'] != "null" && _check[0]['marketing_notes'] != ""){
        this._controllerMarketingNotes.text = _check[0]['marketing_notes'];
      }
    }
    checkDataDakor();
    notifyListeners();
  }

  Future<void> checkDataDakor() async{
    var _check = await _dbHelper.selectMS2Application();
    if(_check.isNotEmpty && _lastKnownState == "DKR"){
      isMarketingNotesChanges = this._controllerMarketingNotes.text != _check[0]['marketing_notes'] || _check[0]['edit_marketing_notes'] == "1";
    }
  }

  Future<void> setPreference(BuildContext context) async {
    SharedPreferences _preference = await SharedPreferences.getInstance();
    this._custType = _preference.getString("cust_type");
    this._lastKnownState = _preference.getString("last_known_state");
    if(this._lastKnownState == "AOS" || this._lastKnownState == "CONA" || this._lastKnownState == "IA" || this._lastKnownState == "PAC") {
      this._isDisablePACIAAOSCONA = true;
    }
    getDataFromDashboard(context);
    try{
      await Provider.of<InfoCreditStructureChangeNotifier>(context,listen: false).calculateCreditNew(context);
      await Provider.of<InfoCreditIncomeChangeNotifier>(context, listen: false).getDSRNew(context);
    }
    catch(e){
      _showSnackBar(e);
    }
  }

  void _showSnackBar(String text){
    this._scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor,duration: Duration(seconds: 5),));
  }

  ShowMandatoryMarketingNotesModel _showMandatoryMarketingNotesIdeModel;
  ShowMandatoryMarketingNotesModel _showMandatoryMarketingNotesIdeCompanyModel;
  void showMandatoryIdeModel(BuildContext context){
    _showMandatoryMarketingNotesIdeModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryMarketingNotesIdeModel;
    _showMandatoryMarketingNotesIdeCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryMarketingNotesIdeCompanyModel;
  }

  ShowMandatoryMarketingNotesModel _showMandatoryMarketingNotesRegulerSurveyModel;
  ShowMandatoryMarketingNotesModel _showMandatoryMarketingNotesRegulerSurveyCompanyModel;
  void showMandatoryRegulerSurveyModel(BuildContext context){
    _showMandatoryMarketingNotesRegulerSurveyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryMarketingNotesRegulerSurveyModel;
    _showMandatoryMarketingNotesRegulerSurveyCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryMarketingNotesRegulerSurveyCompanyModel;
  }

  ShowMandatoryMarketingNotesModel _showMandatoryMarketingNotesPacModel;
  ShowMandatoryMarketingNotesModel _showMandatoryMarketingNotesPacCompanyModel;
  void showMandatoryPacModel(BuildContext context){
    _showMandatoryMarketingNotesPacModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryMarketingNotesPacModel;
    _showMandatoryMarketingNotesPacCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryMarketingNotesPacCompanyModel;
  }

  ShowMandatoryMarketingNotesModel _showMandatoryMarketingNotesResurveyModel;
  ShowMandatoryMarketingNotesModel _showMandatoryMarketingNotesResurveyCompanyModel;
  void showMandatoryResurveyModel(BuildContext context){
    _showMandatoryMarketingNotesResurveyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryMarketingNotesResurveyModel;
    _showMandatoryMarketingNotesResurveyCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryMarketingNotesResurveyCompanyModel;
  }

  ShowMandatoryMarketingNotesModel _showMandatoryMarketingNotesDakorModel;
  ShowMandatoryMarketingNotesModel _showMandatoryMarketingNotesDakorCompanyModel;
  void showMandatoryDakorModel(BuildContext context){
    _showMandatoryMarketingNotesDakorModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryMarketingNotesDakorModel;
    _showMandatoryMarketingNotesDakorCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryMarketingNotesDakorCompanyModel;
  }

  void getDataFromDashboard(BuildContext context){
    showMandatoryIdeModel(context);
    showMandatoryRegulerSurveyModel(context);
    showMandatoryPacModel(context);
    showMandatoryResurveyModel(context);
    showMandatoryDakorModel(context);
  }

  bool isMarketingNotesVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryMarketingNotesIdeModel.isMarketingNotesVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryMarketingNotesRegulerSurveyModel.isMarketingNotesVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryMarketingNotesPacModel.isMarketingNotesVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryMarketingNotesResurveyModel.isMarketingNotesVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryMarketingNotesDakorModel.isMarketingNotesVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryMarketingNotesIdeCompanyModel.isMarketingNotesVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryMarketingNotesRegulerSurveyCompanyModel.isMarketingNotesVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryMarketingNotesPacCompanyModel.isMarketingNotesVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryMarketingNotesResurveyCompanyModel.isMarketingNotesVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryMarketingNotesDakorCompanyModel.isMarketingNotesVisible;
      }
    }
    return value;
  }

  bool isMarketingNotesMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryMarketingNotesIdeModel.isMarketingNotesMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryMarketingNotesRegulerSurveyModel.isMarketingNotesMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryMarketingNotesPacModel.isMarketingNotesMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryMarketingNotesResurveyModel.isMarketingNotesMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryMarketingNotesDakorModel.isMarketingNotesMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryMarketingNotesIdeCompanyModel.isMarketingNotesMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryMarketingNotesRegulerSurveyCompanyModel.isMarketingNotesMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryMarketingNotesPacCompanyModel.isMarketingNotesMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryMarketingNotesResurveyCompanyModel.isMarketingNotesMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryMarketingNotesDakorCompanyModel.isMarketingNotesMandatory;
      }
    }
    return value;
  }
}