import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/models/add_detail_installment_credit_subsidy.dart';
import 'package:ad1ms2_dev/models/credit_subsidy_model.dart';
import 'package:ad1ms2_dev/models/cutting_method_model.dart';
import 'package:ad1ms2_dev/models/installment_index_model.dart';
import 'package:ad1ms2_dev/models/show_mandatory_info_subsidi_model.dart';
import 'package:ad1ms2_dev/models/subsidy_type_model.dart';
import 'package:ad1ms2_dev/models/type_model.dart';
import 'package:ad1ms2_dev/screens/app/add_installment_credit_subsidy.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_credit_structure_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_credit_subsidy_change_notifier.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/dashboard/dashboard_change_notif.dart';
import 'package:ad1ms2_dev/shared/regex_input_formatter.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/io_client.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../main.dart';
import '../format_currency.dart';
import '../info_wmp_change_notifier.dart';
import 'add_installment_credit_subsidy_change_notifier.dart';
import 'package:http/http.dart' as http;

class AddCreditSubsidyChangeNotifier with ChangeNotifier{
    GlobalKey<FormState> _key = GlobalKey<FormState>();
    List<SubsidyTypeModel> _listType = [];
    List<AddInstallmentDetailModel> _listDetail = [];
    List<CreditSubsidyModel> _listCreditSubsidy = [];
    String _radioGiver = "00";
    SubsidyTypeModel _typeSelected;
    String _setVisibility;
    CuttingMethodModel _cuttingMethodSelected;
    TextEditingController _controllerValue = TextEditingController();
    TextEditingController _controllerClaimValue = TextEditingController();
    TextEditingController _controllerRateBeforeEff = TextEditingController();
    TextEditingController _controllerRateBeforeFlat = TextEditingController();
    TextEditingController _controllerTotalInstallment = TextEditingController();
    String _installmentIndexSelected;
    bool _autoValidate = false;
    int _totalInstallmentIndex;
    RegExInputFormatter _amountValidator = RegExInputFormatter.withRegex('^[0-9]{0,9}(\\.[0-9]{0,2})?\$');
    FormatCurrency _formatCurrency = FormatCurrency();
    var storage = FlutterSecureStorage();
    DbHelper _dbHelper = DbHelper();
    String _orderSubsidyID;
    String _custType;
    String _lastKnownState;
    bool _isDisablePACIAAOSCONA = false;

    bool _isEditDakor = false;
    bool _giverDakor = false;
    bool _typeDakor = false;
    bool _cuttingMethodDakor = false;
    bool _cuttingValueDakor = false;
    bool _claimValueDakor = false;
    bool _rateBeforeEffDakor = false;
    bool _rateBeforeFlatDakor = false;
    bool _totalInstallmentDakor = false;

    GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
    GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

    RegExInputFormatter get amountValidator => _amountValidator;

    FormatCurrency get formatCurrency => _formatCurrency;

    bool get isDisablePACIAAOSCONA => _isDisablePACIAAOSCONA;

    set isDisablePACIAAOSCONA(bool value) {
        this._isDisablePACIAAOSCONA = value;
        notifyListeners();
    }

    bool _loadData = false;

    bool get loadData => _loadData;

    set loadData(bool value) {
        this._loadData = value;
        notifyListeners();
    }

    bool get totalInstallmentDakor => _totalInstallmentDakor;

    set totalInstallmentDakor(bool value) {
      this._totalInstallmentDakor = value;
      notifyListeners();
    }

    String get orderSubsidyID => _orderSubsidyID;

    set orderSubsidyID(String value) {
        this._orderSubsidyID = value;
        notifyListeners();
    }

    List<CreditSubsidyModel> get listCreditSubsidy => _listCreditSubsidy;

    List<AddInstallmentDetailModel> get listDetail => _listDetail;

    String get radioValueGiver {
        return _radioGiver;
    }

    set radioValueGiver(String value) {
        this._radioGiver = value;
        notifyListeners();
    }

    SubsidyTypeModel get typeSelected {
        return this._typeSelected;
    }

    set typeSelected(SubsidyTypeModel value) {
        changeType();
        this._controllerRateBeforeEff.text = "0";
        this._controllerRateBeforeFlat.text = "0";
        this._typeSelected = value;
        _setVisibility = value.id;
        notifyListeners();
    }

    UnmodifiableListView<SubsidyTypeModel> get listType {
        return UnmodifiableListView(this._listType);
    }

    List<CuttingMethodModel> _listCuttingMethod = [
        // CuttingMethodModel("01", "Potong Pencairan"),
        // CuttingMethodModel("02", "Klaim"),
        // CuttingMethodModel("03", "Potong Pencairan & Klaim")
    ];

    CuttingMethodModel get cuttingMethodSelected {
        return this._cuttingMethodSelected;
    }

    set cuttingMethodSelected(CuttingMethodModel value) {
        clearValue();
        this._cuttingMethodSelected = value;
        if(value.id == "01"){
            this._controllerClaimValue.text = "0";
        } else if (value.id == "02"){
            this._controllerValue.text = "0";
        }
        notifyListeners();
    }

    UnmodifiableListView<CuttingMethodModel> get listCuttingMethod {
        return UnmodifiableListView(this._listCuttingMethod);
    }

    TextEditingController get controllerValue => _controllerValue;

    set controllerValue(TextEditingController value) {
        _controllerValue = value;
        notifyListeners();
    }

    TextEditingController get controllerClaimValue => _controllerClaimValue;

    set controllerClaimValue(TextEditingController value) {
        _controllerClaimValue = value;
        notifyListeners();
    }

    TextEditingController get controllerRateBeforeEff => _controllerRateBeforeEff;

    set controllerRateBeforeEff(TextEditingController value) {
        _controllerRateBeforeEff = value;
        notifyListeners();
    }

    TextEditingController get controllerRateBeforeFlat => _controllerRateBeforeFlat;

    set controllerRateBeforeFlat(TextEditingController value) {
        _controllerRateBeforeFlat = value;
        notifyListeners();
    }

    TextEditingController get controllerTotalInstallment =>
      _controllerTotalInstallment;

    set controllerTotalInstallment(TextEditingController value) {
        _controllerTotalInstallment = value;
        notifyListeners();
    }

    List<String> _listInstallmentIndex = [];

    String get installmentIndexSelected {
        return this._installmentIndexSelected;
    }

    set installmentIndexSelected(String value) {
        this._installmentIndexSelected = value;
        notifyListeners();
    }

    UnmodifiableListView<String> get listInstallmentIndexSelected {
        return UnmodifiableListView(this._listInstallmentIndex);
    }

    bool get autoValidate => _autoValidate;

    set autoValidate(bool value) {
        this._autoValidate = value;
        notifyListeners();
    }

    GlobalKey<FormState> get key => _key;

    void formattingPrice(){
        this._controllerValue.text = formatCurrency.formatCurrency(this._controllerValue.text);
        this._controllerClaimValue.text = formatCurrency.formatCurrency(this._controllerClaimValue.text);
        this._controllerTotalInstallment.text = formatCurrency.formatCurrency(this._controllerTotalInstallment.text);
        notifyListeners();
    }

    void addDetailInstallment(BuildContext context) async {
        // AddInstallmentDetailModel _data = await Navigator.push(context,
        //     MaterialPageRoute(
        //         builder: (context) => ChangeNotifierProvider(
        //             create: (context) => AddInstallmentCreditSubsidyChangeNotifier(),
        //             child: AddInstallment(),
        //         ),
        //         fullscreenDialog: true
        //     ));
        // if (_data != null) {
        //     this._listDetail.add(_data);
        //     notifyListeners();
        // }
        // final _form = _key.currentState;
        if (installmentIndexSelected != null && _controllerTotalInstallment.text.isNotEmpty) {
            double _detail = 0;
            double _val1 = this._controllerValue.text.isNotEmpty ? double.parse(this._controllerValue.text.replaceAll(",", "")) : 0;
            double _val2 = this._controllerClaimValue.text.isNotEmpty ? double.parse(this._controllerClaimValue.text.replaceAll(",", "")) : 0;
            double _total = _val1 + _val2;
            _detail += double.parse(this._controllerTotalInstallment.text.replaceAll(",", ""));
            for(int i=0; i<this._listDetail.length; i++){
                var _value = double.parse(this._listDetail[i].installmentSubsidy.replaceAll(",", ""));
                _detail += _value;
            }
            if(_detail > _total){
                showDialog(
                    context: context,
                    barrierDismissible: true,
                    builder: (BuildContext context){
                        return Theme(
                            data: ThemeData(
                                fontFamily: "NunitoSans"
                            ),
                            child: AlertDialog(
                                title: Text("Warning", style: TextStyle(fontWeight: FontWeight.bold)),
                                content: Text("Nilai angsuran tidak boleh melebihi nilai potongan pencairan dan nilai klaim"),
                                actions: <Widget>[
                                    FlatButton(
                                        onPressed: (){
                                            Navigator.pop(context);
                                        },
                                        child: Text("Close",
                                            style: TextStyle(
                                                color: primaryOrange,
                                                fontSize: 14,
                                                fontWeight: FontWeight.w500,
                                                letterSpacing: 1.25
                                            )
                                        )
                                    )
                                ],
                            ),
                        );
                    }
                );
            }
            else {
                this._listDetail.add(AddInstallmentDetailModel(_installmentIndexSelected, _controllerTotalInstallment.text,null));
            }
            listInstallmentIndex();
            notifyListeners();
        } else {
            autoValidate = true;
        }
    }

    void listInstallmentIndex(){
        this._controllerTotalInstallment.clear();
        this._installmentIndexSelected = null;
        this._listInstallmentIndex.clear();
        for(int i=1; i<=_totalInstallmentIndex; i++){
            _listInstallmentIndex.add(i.toString());
        }
        for(int j=0;j<_listDetail.length;j++){
            for(int i=0; i<_listInstallmentIndex.length; i++){
                if(_listInstallmentIndex[i] == _listDetail[j].installmentIndex){
                    _listInstallmentIndex.removeAt(i);
                }
            }
        }
    }

    void deleteIndex(BuildContext context, index){
        this._listDetail.removeAt(index);
        listInstallmentIndex();
        notifyListeners();
    }

    void changeType(){
         this._typeSelected = null;
         this._cuttingMethodSelected = null;
         this._controllerValue.clear();
         this._controllerClaimValue.clear();
         this._controllerRateBeforeEff.clear();
         this._controllerRateBeforeFlat.clear();
         this._controllerTotalInstallment.clear();
         this._installmentIndexSelected = null;
         this._listDetail.clear();
         this._isEditDakor = false;
         this._giverDakor = false;
         this._typeDakor = false;
         this._cuttingMethodDakor = false;
         this._cuttingValueDakor = false;
         this._claimValueDakor = false;
         this._rateBeforeEffDakor = false;
         this._rateBeforeFlatDakor = false;
    }

    void clearValue(){
        this._controllerValue.clear();
        this._controllerClaimValue.clear();
        isDisablePACIAAOSCONA = false;
    }

    void check(BuildContext context, int flag, int index) {
        final _form = _key.currentState;
        var _wmp = Provider.of<InfoWMPChangeNotifier>(context, listen: false);
        for(int j=0; j<_wmp.listWMP.length; j++){
            if(this._typeSelected.id == _wmp.listWMP[j].typeSubsidi){
                double _val1 = _wmp.listWMP[j].amount.isNotEmpty ? double.parse(_wmp.listWMP[j].amount.replaceAll(",", "")) : 0;
                double _val2 = this._controllerClaimValue.text.isNotEmpty ? double.parse(this._controllerClaimValue.text.replaceAll(",", "")) : 0;
                if(_val2 > _val1){
                    showDialog(
                        context: context,
                        barrierDismissible: true,
                        builder: (BuildContext context){
                            return Theme(
                                data: ThemeData(
                                    fontFamily: "NunitoSans"
                                ),
                                child: AlertDialog(
                                    title: Text("Warning", style: TextStyle(fontWeight: FontWeight.bold)),
                                    content: Text("Nilai klaim tidak boleh melebihi ${formatCurrency.formatCurrency(_wmp.listWMP[j].amount)} dari WMP"),
                                    actions: <Widget>[
                                        FlatButton(
                                            onPressed: (){
                                                Navigator.pop(context);
                                            },
                                            child: Text("Close",
                                                style: TextStyle(
                                                    color: primaryOrange,
                                                    fontSize: 14,
                                                    fontWeight: FontWeight.w500,
                                                    letterSpacing: 1.25
                                                )
                                            )
                                        )
                                    ],
                                ),
                            );
                        }
                    );
                    this._controllerClaimValue.clear();
                }
            }
        }
        if(_form.validate()){
            if (flag == 0) {
                Provider.of<InfoCreditSubsidyChangeNotifier>(context, listen: false)
                    .addListInfoCreditSubsidy(CreditSubsidyModel(
                    this.radioValueGiver,
                    null,
                    this._typeSelected,
                    this._cuttingMethodSelected,
                    this._controllerValue.text,
                    this._controllerClaimValue.text,
                    this._controllerRateBeforeEff.text,
                    this._controllerRateBeforeFlat.text,
                    this._controllerTotalInstallment.text,
                    this._installmentIndexSelected,
                    this._listDetail,
                    this._isEditDakor,
                    this._giverDakor,
                    this._typeDakor,
                    this._cuttingMethodDakor,
                    this._cuttingValueDakor,
                    this._claimValueDakor,
                    this._rateBeforeEffDakor,
                    this._rateBeforeFlatDakor,
                    this._totalInstallmentDakor,
                ));
                Navigator.pop(context);
            }
            else {
                checkDataDakor(index);
                Provider.of<InfoCreditSubsidyChangeNotifier>(context, listen: false)
                    .updateListInfoCreditSubsidy(CreditSubsidyModel(
                    this.radioValueGiver,
                    this._orderSubsidyID,
                    this._typeSelected,
                    this._cuttingMethodSelected,
                    this._controllerValue.text,
                    this._controllerClaimValue.text,
                    this._controllerRateBeforeEff.text,
                    this._controllerRateBeforeFlat.text,
                    this._controllerTotalInstallment.text,
                    this._installmentIndexSelected,
                    this._listDetail,
                    this._isEditDakor,
                    this._giverDakor,
                    this._typeDakor,
                    this._cuttingMethodDakor,
                    this._cuttingValueDakor,
                    this._claimValueDakor,
                    this._rateBeforeEffDakor,
                    this._rateBeforeFlatDakor,
                    this._totalInstallmentDakor,
                ), context, index);
                Navigator.pop(context);
            }
        }
        else {
            autoValidate = true;
        }
    }

    Future<void> setValueForEditInfoCreditSubsidy(BuildContext context, CreditSubsidyModel data, int flag, int periodTime, int index) async {
        this._totalInstallmentIndex = periodTime;
        setPreference();
        getDataFromDashboard(context);
        if(flag != 0){
            orderSubsidyID = data.orderSubsidyID;
            this._radioGiver = data.giver;
            getListType(context, data, flag, index);
        } else {
            getListCuttingMethod(data, flag, index);
        }
        listInstallmentIndex();
    }

    Future<void> getListType(BuildContext context, CreditSubsidyModel data, int flag, int index) async{
        _listType.clear();
        changeType();
        this._typeSelected = null;
        var _wmp = Provider.of<InfoWMPChangeNotifier>(context, listen: false);
        String _fieldJenis = await storage.read(key: "FieldJenis");
        try{
            final ioc = new HttpClient();
            ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
            final _http = IOClient(ioc);
            var _body = jsonEncode({
                "refOne": _radioGiver
            });

            final _response = await _http.post(
                "${BaseUrl.urlGeneral}$_fieldJenis",
                // "${urlPublic}jenis-rehab/get_jenis_subsidi",
                body: _body,
                headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
            );
            final _data = jsonDecode(_response.body);
            for(int i=0; i < _data.length;i++){
                if(this._radioGiver == "0"){
                    for(int j=0; j<_wmp.listWMP.length; j++){
                        if(_data[i]['kode'] == _wmp.listWMP[j].typeSubsidi){
                            this._listType.add(SubsidyTypeModel(_data[i]['kode'], _data[i]['deskripsi']));
                        }
                    }
                } else {
                    this._listType.add(SubsidyTypeModel(_data[i]['kode'], _data[i]['deskripsi']));
                }
            }
            getListCuttingMethod(data, flag, index);
        } catch (e) {
        }
        if(flag == 0){
            notifyListeners();
        }
    }

    // OLD
    Future<void> getListCuttingMethod(CreditSubsidyModel data, int flag, int index) async{
        this._listCuttingMethod.clear();
        this._cuttingMethodSelected = null;
        String _fieldMetodePemotongan = await storage.read(key: "FieldMetodePemotongan");
        try{
            final ioc = new HttpClient();
            ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
            final _http = IOClient(ioc);
            final _response = await _http.get(
                "${BaseUrl.urlGeneral}$_fieldMetodePemotongan",
                headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
                // "${urlPublic}jenis-rehab/get_method_subsidi"
            );
            final _data = jsonDecode(_response.body);
            for(int i=0; i < _data['data'].length;i++){
                _listCuttingMethod.add(CuttingMethodModel(_data['data'][i]['kode'], _data['data'][i]['deskripsi']));
            }
            if(flag != 0){
                setDataEdit(data, flag, index);
            }
        } catch (e) {
        }
        notifyListeners();
    }
     // void getListCuttingMethod() async {
     //     _listCuttingMethod.clear();
     //     this._cuttingMethodSelected = null;
     //     String _fieldMetodePemotongan = await storage.read(key: "FieldMetodePemotongan");
     //     try{
     //         final ioc = new HttpClient();
     //         ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
     //         final _http = IOClient(ioc);
     //         final _response = await _http.get(
     //             "${BaseUrl.urlGeneral}$_fieldMetodePemotongan",
     //             headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
     //             // "${BaseUrl.urlGeneral}ms2/jenis-rehab/get_method_subsidi",
     //             // "${urlPublic}jenis-rehab/get_method_subsidi"
     //         );
     //         final _data = jsonDecode(_response.body);
     //         print("response metode pemotongan: ${_data}");
     //         for(int i=0; i < _data['data'].length;i++){
     //             _listCuttingMethod.add(CuttingMethodModel(_data['data'][i]['kode'], _data['data'][i]['deskripsi']));
     //         }
     //         // setDataEdit(data, flag, index);
     //     } catch (e) {
     //     }
     // }

    void setDataEdit(CreditSubsidyModel data, int flag, int index){
        for (int i = 0; i < this._listType.length; i++) {
            if (data.type.id == this._listType[i].id) {
                this._typeSelected = this._listType[i];
            }
        }
        if(data.cuttingMethod != null){
            for (int i = 0; i < this._listCuttingMethod.length; i++) {
                if (data.cuttingMethod.id == this._listCuttingMethod[i].id) {
                    this._cuttingMethodSelected = this._listCuttingMethod[i];
                }
            }
        }
        this._controllerValue.text = formatCurrency.formatCurrency(data.value);
        this._controllerClaimValue.text = formatCurrency.formatCurrency(data.claimValue);
        // if(data.type.id != "05"){
            this._controllerRateBeforeEff.text = data.rateBeforeEff;
            this._controllerRateBeforeFlat.text = data.rateBeforeFlat;
        // }
        // else {
            this._controllerTotalInstallment.text = data.totalInstallment;
            for (int i = 0; i < this._listInstallmentIndex.length; i++) {
                if (data.installmentIndex == this._listInstallmentIndex[i]) {
                    this._installmentIndexSelected = this._listInstallmentIndex[i];
                }
            }
            if(flag != 0){
              this._listDetail = data.installmentDetail;
            }
            listInstallmentIndex();
        // }
        checkDataDakor(index);
        formattingPrice();
        notifyListeners();
    }

    void checkDataDakor(int index) async{
        var _check = await _dbHelper.selectMS2ApplRefund();
        if(_check.isNotEmpty && _lastKnownState == "DKR"){
            // _isEditDakor
            giverDakor = this.radioValueGiver != _check[index]['giver_refund'] || _check[index]['edit_giver_refund'] == "1";
            typeDakor = this._typeSelected.id != _check[index]['type_refund'] || _check[index]['edit_type_refund'] == "1";
            cuttingMethodDakor = this._cuttingMethodSelected.id != _check[index]['deduction_method'] || _check[index]['edit_deduction_method'] == "1";
            cuttingValueDakor = double.parse(this._controllerValue.text.replaceAll(",", "")).toString().split(".")[0] != _check[index]['refund_amt'].toString() || _check[index]['edit_refund_amt'] == "1";
            //claimValue
            // _rateBeforeEffDakor
            // _rateBeforeFlatDakor

            if(giverDakor || typeDakor || cuttingMethodDakor || cuttingValueDakor|| claimValueDakor || rateBeforeEffDakor || rateBeforeFlatDakor){
                isEditDakor = true;
            } else {
                isEditDakor = false;
            }
        }
        notifyListeners();
    }

    bool get rateBeforeFlatDakor => _rateBeforeFlatDakor;

    set rateBeforeFlatDakor(bool value) {
      _rateBeforeFlatDakor = value;
      notifyListeners();
    }

    bool get rateBeforeEffDakor => _rateBeforeEffDakor;

    set rateBeforeEffDakor(bool value) {
      _rateBeforeEffDakor = value;
      notifyListeners();
    }

    bool get claimValueDakor => _claimValueDakor;

    set claimValueDakor(bool value) {
      _claimValueDakor = value;
      notifyListeners();
    }

    bool get cuttingValueDakor => _cuttingValueDakor;

    set cuttingValueDakor(bool value) {
      _cuttingValueDakor = value;
      notifyListeners();
    }

    bool get cuttingMethodDakor => _cuttingMethodDakor;

    set cuttingMethodDakor(bool value) {
      _cuttingMethodDakor = value;
      notifyListeners();
    }

    bool get typeDakor => _typeDakor;

    set typeDakor(bool value) {
      _typeDakor = value;
      notifyListeners();
    }

    bool get giverDakor => _giverDakor;

    set giverDakor(bool value) {
      _giverDakor = value;
      notifyListeners();
    }

    bool get isEditDakor => _isEditDakor;

    set isEditDakor(bool value) {
      _isEditDakor = value;
      notifyListeners();
    }

    String get custType => _custType;

    set custType(String value) {
        this._custType = value;
        notifyListeners();
    }

    String get lastKnownState => _lastKnownState;

    set lastKnownState(String value) {
        this._lastKnownState = value;
        notifyListeners();
    }

    void setPreference() async {
        SharedPreferences _preference = await SharedPreferences.getInstance();
        this._custType = _preference.getString("cust_type");
        this._lastKnownState = _preference.getString("last_known_state");
        if(this._lastKnownState == "AOS" || this._lastKnownState == "CONA" || this._lastKnownState == "IA" || this._lastKnownState == "PAC") {
            this._isDisablePACIAAOSCONA = true;
        }
    }

    ShowMandatoryInfoSubsidiModel _showMandatoryInfoSubsidiIdeModel;
    ShowMandatoryInfoSubsidiModel _showMandatoryInfoSubsidiIdeCompanyModel;
    void showMandatoryIdeModel(BuildContext context){
        _showMandatoryInfoSubsidiIdeModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoSubsidiIdeModel;
        _showMandatoryInfoSubsidiIdeCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoSubsidiIdeCompanyModel;
    }

    ShowMandatoryInfoSubsidiModel _showMandatoryInfoSubsidiRegulerSurveyModel;
    ShowMandatoryInfoSubsidiModel _showMandatoryInfoSubsidiRegulerSurveyCompanyModel;
    void showMandatoryRegulerSurveyModel(BuildContext context){
        _showMandatoryInfoSubsidiRegulerSurveyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoSubsidiRegulerSurveyModel;
        _showMandatoryInfoSubsidiRegulerSurveyCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoSubsidiRegulerSurveyCompanyModel;
    }

    ShowMandatoryInfoSubsidiModel _showMandatoryInfoSubsidiPacModel;
    ShowMandatoryInfoSubsidiModel _showMandatoryInfoSubsidiPacCompanyModel;
    void showMandatoryPacModel(BuildContext context){
        _showMandatoryInfoSubsidiPacModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoSubsidiPacModel;
        _showMandatoryInfoSubsidiPacCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoSubsidiPacCompanyModel;
    }

    ShowMandatoryInfoSubsidiModel _showMandatoryInfoSubsidiResurveyModel;
    ShowMandatoryInfoSubsidiModel _showMandatoryInfoSubsidiResurveyCompanyModel;
    void showMandatoryResurveyModel(BuildContext context){
        _showMandatoryInfoSubsidiResurveyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoSubsidiResurveyModel;
        _showMandatoryInfoSubsidiResurveyCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoSubsidiResurveyCompanyModel;
    }

    ShowMandatoryInfoSubsidiModel _showMandatoryInfoSubsidiDakorModel;
    ShowMandatoryInfoSubsidiModel _showMandatoryInfoSubsidiDakorCompanyModel;
    void showMandatoryDakorModel(BuildContext context){
        _showMandatoryInfoSubsidiDakorModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoSubsidiDakorModel;
        _showMandatoryInfoSubsidiDakorCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoSubsidiDakorCompanyModel;
    }

    void getDataFromDashboard(BuildContext context){
        showMandatoryIdeModel(context);
        showMandatoryRegulerSurveyModel(context);
        showMandatoryPacModel(context);
        showMandatoryResurveyModel(context);
        showMandatoryDakorModel(context);
    }

    bool isRadioValueGiverVisible() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoSubsidiIdeModel.isRadioValueGiverVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoSubsidiRegulerSurveyModel.isRadioValueGiverVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoSubsidiPacModel.isRadioValueGiverVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoSubsidiResurveyModel.isRadioValueGiverVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoSubsidiDakorModel.isRadioValueGiverVisible;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoSubsidiIdeCompanyModel.isRadioValueGiverVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoSubsidiRegulerSurveyCompanyModel.isRadioValueGiverVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoSubsidiPacCompanyModel.isRadioValueGiverVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoSubsidiResurveyCompanyModel.isRadioValueGiverVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoSubsidiDakorCompanyModel.isRadioValueGiverVisible;
            }
        }
        return value;
    }

    bool isTypeSelectedVisible() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoSubsidiIdeModel.isTypeSelectedVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoSubsidiRegulerSurveyModel.isTypeSelectedVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoSubsidiPacModel.isTypeSelectedVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoSubsidiResurveyModel.isTypeSelectedVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoSubsidiDakorModel.isTypeSelectedVisible;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoSubsidiIdeCompanyModel.isTypeSelectedVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoSubsidiRegulerSurveyCompanyModel.isTypeSelectedVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoSubsidiPacCompanyModel.isTypeSelectedVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoSubsidiResurveyCompanyModel.isTypeSelectedVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoSubsidiDakorCompanyModel.isTypeSelectedVisible;
            }
        }
        return value;
    }

    bool isCuttingMethodSelectedVisible() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoSubsidiIdeModel.isCuttingMethodSelectedVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoSubsidiRegulerSurveyModel.isCuttingMethodSelectedVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoSubsidiPacModel.isCuttingMethodSelectedVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoSubsidiResurveyModel.isCuttingMethodSelectedVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoSubsidiDakorModel.isCuttingMethodSelectedVisible;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoSubsidiIdeCompanyModel.isCuttingMethodSelectedVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoSubsidiRegulerSurveyCompanyModel.isCuttingMethodSelectedVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoSubsidiPacCompanyModel.isCuttingMethodSelectedVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoSubsidiResurveyCompanyModel.isCuttingMethodSelectedVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoSubsidiDakorCompanyModel.isCuttingMethodSelectedVisible;
            }
        }
        return value;
    }

    bool isValueVisible() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoSubsidiIdeModel.isValueVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoSubsidiRegulerSurveyModel.isValueVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoSubsidiPacModel.isValueVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoSubsidiResurveyModel.isValueVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoSubsidiDakorModel.isValueVisible;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoSubsidiIdeCompanyModel.isValueVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoSubsidiRegulerSurveyCompanyModel.isValueVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoSubsidiPacCompanyModel.isValueVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoSubsidiResurveyCompanyModel.isValueVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoSubsidiDakorCompanyModel.isValueVisible;
            }
        }
        return value;
    }

    bool isClaimValueVisible() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoSubsidiIdeModel.isClaimValueVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoSubsidiRegulerSurveyModel.isClaimValueVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoSubsidiPacModel.isClaimValueVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoSubsidiResurveyModel.isClaimValueVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoSubsidiDakorModel.isClaimValueVisible;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoSubsidiIdeCompanyModel.isClaimValueVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoSubsidiRegulerSurveyCompanyModel.isClaimValueVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoSubsidiPacCompanyModel.isClaimValueVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoSubsidiResurveyCompanyModel.isClaimValueVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoSubsidiDakorCompanyModel.isClaimValueVisible;
            }
        }
        return value;
    }

    bool isRateBeforeEffVisible() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoSubsidiIdeModel.isRateBeforeEffVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoSubsidiRegulerSurveyModel.isRateBeforeEffVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoSubsidiPacModel.isRateBeforeEffVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoSubsidiResurveyModel.isRateBeforeEffVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoSubsidiDakorModel.isRateBeforeEffVisible;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoSubsidiIdeCompanyModel.isRateBeforeEffVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoSubsidiRegulerSurveyCompanyModel.isRateBeforeEffVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoSubsidiPacCompanyModel.isRateBeforeEffVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoSubsidiResurveyCompanyModel.isRateBeforeEffVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoSubsidiDakorCompanyModel.isRateBeforeEffVisible;
            }
        }
        return value;
    }

    bool isRateBeforeFlatVisible() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoSubsidiIdeModel.isRateBeforeFlatVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoSubsidiRegulerSurveyModel.isRateBeforeFlatVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoSubsidiPacModel.isRateBeforeFlatVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoSubsidiResurveyModel.isRateBeforeFlatVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoSubsidiDakorModel.isRateBeforeFlatVisible;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoSubsidiIdeCompanyModel.isRateBeforeFlatVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoSubsidiRegulerSurveyCompanyModel.isRateBeforeFlatVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoSubsidiPacCompanyModel.isRateBeforeFlatVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoSubsidiResurveyCompanyModel.isRateBeforeFlatVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoSubsidiDakorCompanyModel.isRateBeforeFlatVisible;
            }
        }
        return value;
    }

    bool isTotalInstallmentVisible() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoSubsidiIdeModel.isTotalInstallmentVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoSubsidiRegulerSurveyModel.isTotalInstallmentVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoSubsidiPacModel.isTotalInstallmentVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoSubsidiResurveyModel.isTotalInstallmentVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoSubsidiDakorModel.isTotalInstallmentVisible;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoSubsidiIdeCompanyModel.isTotalInstallmentVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoSubsidiRegulerSurveyCompanyModel.isTotalInstallmentVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoSubsidiPacCompanyModel.isTotalInstallmentVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoSubsidiResurveyCompanyModel.isTotalInstallmentVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoSubsidiDakorCompanyModel.isTotalInstallmentVisible;
            }
        }
        return value;
    }

    bool isInstallmentIndexSelectedVisible() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoSubsidiIdeModel.isInstallmentIndexSelectedVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoSubsidiRegulerSurveyModel.isInstallmentIndexSelectedVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoSubsidiPacModel.isInstallmentIndexSelectedVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoSubsidiResurveyModel.isInstallmentIndexSelectedVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoSubsidiDakorModel.isInstallmentIndexSelectedVisible;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoSubsidiIdeCompanyModel.isInstallmentIndexSelectedVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoSubsidiRegulerSurveyCompanyModel.isInstallmentIndexSelectedVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoSubsidiPacCompanyModel.isInstallmentIndexSelectedVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoSubsidiResurveyCompanyModel.isInstallmentIndexSelectedVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoSubsidiDakorCompanyModel.isInstallmentIndexSelectedVisible;
            }
        }
        return value;
    }

    bool isRadioValueGiverMandatory() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoSubsidiIdeModel.isRadioValueGiverMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoSubsidiRegulerSurveyModel.isRadioValueGiverMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoSubsidiPacModel.isRadioValueGiverMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoSubsidiResurveyModel.isRadioValueGiverMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoSubsidiDakorModel.isRadioValueGiverMandatory;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoSubsidiIdeCompanyModel.isRadioValueGiverMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoSubsidiRegulerSurveyCompanyModel.isRadioValueGiverMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoSubsidiPacCompanyModel.isRadioValueGiverMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoSubsidiResurveyCompanyModel.isRadioValueGiverMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoSubsidiDakorCompanyModel.isRadioValueGiverMandatory;
            }
        }
        return value;
    }

    bool isTypeSelectedMandatory() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoSubsidiIdeModel.isTypeSelectedMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoSubsidiRegulerSurveyModel.isTypeSelectedMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoSubsidiPacModel.isTypeSelectedMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoSubsidiResurveyModel.isTypeSelectedMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoSubsidiDakorModel.isTypeSelectedMandatory;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoSubsidiIdeCompanyModel.isTypeSelectedMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoSubsidiRegulerSurveyCompanyModel.isTypeSelectedMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoSubsidiPacCompanyModel.isTypeSelectedMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoSubsidiResurveyCompanyModel.isTypeSelectedMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoSubsidiDakorCompanyModel.isTypeSelectedMandatory;
            }
        }
        return value;
    }

    bool isCuttingMethodSelectedMandatory() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoSubsidiIdeModel.isCuttingMethodSelectedMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoSubsidiRegulerSurveyModel.isCuttingMethodSelectedMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoSubsidiPacModel.isCuttingMethodSelectedMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoSubsidiResurveyModel.isCuttingMethodSelectedMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoSubsidiDakorModel.isCuttingMethodSelectedMandatory;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoSubsidiIdeCompanyModel.isCuttingMethodSelectedMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoSubsidiRegulerSurveyCompanyModel.isCuttingMethodSelectedMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoSubsidiPacCompanyModel.isCuttingMethodSelectedMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoSubsidiResurveyCompanyModel.isCuttingMethodSelectedMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoSubsidiDakorCompanyModel.isCuttingMethodSelectedMandatory;
            }
        }
        return value;
    }

    bool isValueMandatory() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoSubsidiIdeModel.isValueMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoSubsidiRegulerSurveyModel.isValueMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoSubsidiPacModel.isValueMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoSubsidiResurveyModel.isValueMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoSubsidiDakorModel.isValueMandatory;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoSubsidiIdeCompanyModel.isValueMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoSubsidiRegulerSurveyCompanyModel.isValueMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoSubsidiPacCompanyModel.isValueMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoSubsidiResurveyCompanyModel.isValueMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoSubsidiDakorCompanyModel.isValueMandatory;
            }
        }
        return value;
    }

    bool isClaimValueMandatory() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoSubsidiIdeModel.isClaimValueMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoSubsidiRegulerSurveyModel.isClaimValueMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoSubsidiPacModel.isClaimValueMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoSubsidiResurveyModel.isClaimValueMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoSubsidiDakorModel.isClaimValueMandatory;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoSubsidiIdeCompanyModel.isClaimValueMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoSubsidiRegulerSurveyCompanyModel.isClaimValueMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoSubsidiPacCompanyModel.isClaimValueMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoSubsidiResurveyCompanyModel.isClaimValueMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoSubsidiDakorCompanyModel.isClaimValueMandatory;
            }
        }
        return value;
    }

    bool isRateBeforeEffMandatory() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoSubsidiIdeModel.isRateBeforeEffMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoSubsidiRegulerSurveyModel.isRateBeforeEffMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoSubsidiPacModel.isRateBeforeEffMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoSubsidiResurveyModel.isRateBeforeEffMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoSubsidiDakorModel.isRateBeforeEffMandatory;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoSubsidiIdeCompanyModel.isRateBeforeEffMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoSubsidiRegulerSurveyCompanyModel.isRateBeforeEffMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoSubsidiPacCompanyModel.isRateBeforeEffMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoSubsidiResurveyCompanyModel.isRateBeforeEffMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoSubsidiDakorCompanyModel.isRateBeforeEffMandatory;
            }
        }
        return value;
    }

    bool isRateBeforeFlatMandatory() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoSubsidiIdeModel.isRateBeforeFlatMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoSubsidiRegulerSurveyModel.isRateBeforeFlatMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoSubsidiPacModel.isRateBeforeFlatMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoSubsidiResurveyModel.isRateBeforeFlatMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoSubsidiDakorModel.isRateBeforeFlatMandatory;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoSubsidiIdeCompanyModel.isRateBeforeFlatMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoSubsidiRegulerSurveyCompanyModel.isRateBeforeFlatMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoSubsidiPacCompanyModel.isRateBeforeFlatMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoSubsidiResurveyCompanyModel.isRateBeforeFlatMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoSubsidiDakorCompanyModel.isRateBeforeFlatMandatory;
            }
        }
        return value;
    }

    bool isTotalInstallmentMandatory() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoSubsidiIdeModel.isTotalInstallmentMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoSubsidiRegulerSurveyModel.isTotalInstallmentMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoSubsidiPacModel.isTotalInstallmentMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoSubsidiResurveyModel.isTotalInstallmentMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoSubsidiDakorModel.isTotalInstallmentMandatory;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoSubsidiIdeCompanyModel.isTotalInstallmentMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoSubsidiRegulerSurveyCompanyModel.isTotalInstallmentMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoSubsidiPacCompanyModel.isTotalInstallmentMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoSubsidiResurveyCompanyModel.isTotalInstallmentMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoSubsidiDakorCompanyModel.isTotalInstallmentMandatory;
            }
        }
        return value;
    }

    bool isInstallmentIndexSelectedMandatory() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoSubsidiIdeModel.isInstallmentIndexSelectedMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoSubsidiRegulerSurveyModel.isInstallmentIndexSelectedMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoSubsidiPacModel.isInstallmentIndexSelectedMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoSubsidiResurveyModel.isInstallmentIndexSelectedMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoSubsidiDakorModel.isInstallmentIndexSelectedMandatory;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoSubsidiIdeCompanyModel.isInstallmentIndexSelectedMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoSubsidiRegulerSurveyCompanyModel.isInstallmentIndexSelectedMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoSubsidiPacCompanyModel.isInstallmentIndexSelectedMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoSubsidiResurveyCompanyModel.isInstallmentIndexSelectedMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoSubsidiDakorCompanyModel.isInstallmentIndexSelectedMandatory;
            }
        }
        return value;
    }

    void getEffFlatIntereset(BuildContext context) async{
      if(this._typeSelected.id != "06"){
        if(this._cuttingMethodSelected != null){
          if(this._cuttingMethodSelected.id == "03"){
            if(this._controllerClaimValue.text != "" && this._controllerValue.text != ""){
              try{
                loadData = true;
                await _getSukuBungaSebelumEffFlat(context);
                loadData = false;
              }
              catch(e){
                loadData = false;
                _showSnackBar(e.toString());
              }
            }
          }
          else{
            try{
              loadData = true;
              await _getSukuBungaSebelumEffFlat(context);
              loadData = false;
            }
            catch(e){
              loadData = false;
              _showSnackBar(e.toString());
            }
          }
        }
      }
    }

    Future<String> _getSukuBungaSebelumEffFlat(BuildContext context) async{
      var _providerStructureCredit = Provider.of<InfoCreditStructureChangeNotifier>(context,listen: false);
      String _fieldSukuBungaSebelumEffDanFlat = await storage.read(key: "FieldSukuBungaSebelumEffDanFlat");

      double _interestDisc = 0;
      if(this._cuttingMethodSelected.id == "01"){
        _interestDisc = this._controllerValue.text != "" ? double.parse(this._controllerValue.text.replaceAll(",", "")) : 0;
      }
      else if(this._cuttingMethodSelected.id == "02"){
        _interestDisc = this._controllerClaimValue.text != "" ? double.parse(this._controllerClaimValue.text.replaceAll(",", "")) : 0;
      }
      else{
        _interestDisc += this._controllerClaimValue.text != "" ? double.parse(this._controllerClaimValue.text.replaceAll(",", "")) : 0;
        _interestDisc += this._controllerValue.text != "" ? double.parse(this._controllerValue.text.replaceAll(",", "")) : 0;
      }

      debugPrint("INTEREST_DISC $_interestDisc");

      var _body = jsonEncode({
        "DECAPPL_INTR": _providerStructureCredit.interestAmount,
        "DECAPPL_INTR_DISC": _interestDisc,
        "DECAPPL_INTR_RULE": _providerStructureCredit.paymentMethodSelected.id,
        "DECAPPL_INT_TYPE": _providerStructureCredit.installmentTypeSelected.id,
        "DECPERIOD": _providerStructureCredit.installmentTypeSelected.id == "04" || _providerStructureCredit.installmentTypeSelected.id == "05" ? 1 : 0,
        "DECTOP": int.parse(_providerStructureCredit.periodOfTimeSelected),
        "DECTOTALPRINCIPAL": double.parse(_providerStructureCredit.controllerTotalLoan.text.replaceAll(",", "")).toInt()
      });
      debugPrint(_body);
      String _sukuBungaSebelumEffFlat = await storage.read(key: "SukuBungaSebelumEffFlat");

      try{
        final _response = await http.post(
            "${BaseUrl.urlGeneral}$_sukuBungaSebelumEffFlat",
            body: _body,
            headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
        );
        if(_response.statusCode == 200){
          debugPrint("CEK_SUBSIDY ${_response.body}");
          var _bodyData = jsonDecode(_response.body);
          List _data = _bodyData['data'];
          if(_data.isNotEmpty){
            this.controllerRateBeforeEff.text = _data[0]['DECEFFRATE'].toString();
            this.controllerRateBeforeFlat.text = _data[0]['DECFLATRATE'].toString();
            return _bodyData['status'];
          }
          else{
            throw "Gagal get Suku Bunga Sebelum Eff Flat";
          }
        }
        else{
          throw "Error get Suku Bunga Sebelum Eff Flat ${_response.statusCode}";
        }
      }
      catch(e){
        throw "Error get Suku Bunga Sebelum Eff Flat ${e.toString()}";
      }
    }

    void _showSnackBar(String text){
      this._scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor,duration: Duration(seconds: 5),));
    }
}