import 'package:ad1ms2_dev/models/add_detail_installment_credit_subsidy.dart';
import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';

class AddInstallmentCreditSubsidyChangeNotifier with ChangeNotifier {
    GlobalKey<FormState> _key = GlobalKey<FormState>();
    AddInstallmentDetailModel _addInstallmentDetailModel;
    TextEditingController _controllerInstallmentIndex = TextEditingController();
    TextEditingController _controllerInstallmentSubsidy = TextEditingController();
    bool _autoValidate = false;

    GlobalKey<FormState> get key => _key;

    AddInstallmentDetailModel get addInstallmentDetailModel =>
        _addInstallmentDetailModel;

    bool get autoValidate => _autoValidate;

    set autoValidate(bool value) {
        _autoValidate = value;
        notifyListeners();
    }

    TextEditingController get controllerInstallmentSubsidy =>
        _controllerInstallmentSubsidy;

    set controllerInstallmentSubsidy(TextEditingController value) {
        _controllerInstallmentSubsidy = value;
        notifyListeners();
    }

    TextEditingController get controllerInstallmentIndex =>
        _controllerInstallmentIndex;

    set controllerInstallmentIndex(TextEditingController value) {
        _controllerInstallmentIndex = value;
        notifyListeners();
    }

    addDetail(BuildContext context) {
        _addInstallmentDetailModel = AddInstallmentDetailModel(
            controllerInstallmentIndex.text, controllerInstallmentSubsidy.text,null);
        Navigator.pop(context, _addInstallmentDetailModel);
    }

}