import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/coverage1_model.dart';
import 'package:ad1ms2_dev/models/coverage_type_model.dart';
import 'package:ad1ms2_dev/models/form_m_pekerjaan_model.dart';
import 'package:ad1ms2_dev/models/insurance_type_model.dart';
import 'package:ad1ms2_dev/models/ms2_appl_insurance_model.dart';
import 'package:ad1ms2_dev/models/product_model.dart';
import 'package:ad1ms2_dev/models/sub_type_model.dart';
import 'package:ad1ms2_dev/shared/format_currency.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:ad1ms2_dev/models/major_insurance_model.dart';
import 'package:http/io_client.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../constants.dart';
import '../search_document_type_change_notifier.dart';
import 'info_additional_insurance_change_notifier.dart';
import 'info_application_change_notifier.dart';
import 'information_collatelar_change_notifier.dart';
import 'information_object_unit_change_notifier.dart';

class InfoMajorInsuranceChangeNotifier with ChangeNotifier{
    List<MajorInsuranceModel> _listMajorInsurance = [];
    bool _autoValidate = false;
    bool _flag = false;
    DbHelper _dbHelper = DbHelper();
    List<MajorInsuranceModel> get listFormMajorInsurance => _listMajorInsurance;
    bool _type = false;
    bool _isDurable = false;
    String _messageError = '';
    List _listMandatoryAsuransi = [];
    List _listMandatoryAsuransiUtama = [];
    FormatCurrency _formatCurrency = FormatCurrency();
    bool _isCheckData = false;
    String _custType;
    String _lastKnownState;
    bool _disableJenisPenawaran = false;

    String get custType => _custType;

    set custType(String value) {
        this._custType = value;
        notifyListeners();
    }

    String get lastKnownState => _lastKnownState;

    set lastKnownState(String value) {
        this._lastKnownState = value;
        notifyListeners();
    }

    bool get disableJenisPenawaran => _disableJenisPenawaran;

    set disableJenisPenawaran(bool value) {
        this._disableJenisPenawaran = value;
        notifyListeners();
    }

    bool get isCheckData => _isCheckData;

    set isCheckData(bool value) {
        this._isCheckData = value;
        notifyListeners();
    }

    FormatCurrency get formatCurrency => _formatCurrency;

    bool get autoValidate => _autoValidate;

    set autoValidate(bool value) {
        this._autoValidate = value;
        notifyListeners();
    }

    List get listMandatoryAsuransi => _listMandatoryAsuransi;

    set listMandatoryAsuransi(List value) {
      this._listMandatoryAsuransi = value;
    }

    void checkMandatory(BuildContext context) async{
        this._listMandatoryAsuransi.clear();
        List _dataCL = await _dbHelper.selectMS2LME();
        SharedPreferences _preferences = await SharedPreferences.getInstance();
        var _providerInfoAppl = Provider.of<InfoAppChangeNotifier>(context,listen: false);
        var _providerUnitObject = Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false);
        var _providerColla = Provider.of<InformationCollateralChangeNotifier>(context,listen: false);
        final ioc = new HttpClient();
        ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
        final _http = IOClient(ioc);

        var _body = jsonEncode({
            "P_APPL_DATE": _providerInfoAppl.controllerOrderDate.text,
            "P_BR_ID": _preferences.getString("branchid"),
            "P_OBJECT_ID": _providerColla.objectSelected != null ? _providerColla.objectSelected.id : "",
            "P_PROD_TYPE_ID": _providerUnitObject.productTypeSelected != null ? _providerUnitObject.productTypeSelected.id : ""
        });
        print("body mandatory asuransi = $_body");

        String _validateMandatoryInsurance = await storage.read(key: "ValidateMandatoryInsurance");
        try{
            final _response = await _http.post(
                "${BaseUrl.urlGeneral}$_validateMandatoryInsurance",
                body: _body,
                headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
            );

            if(_response.statusCode == 200){
                final _result = jsonDecode(_response.body);
                if(_preferences.getString("jenis_penawaran") == "002") {
                    if(_preferences.getString("jenis_penawaran") == "002" && _dataCL[0]['pencairan_ke'] == "0"){
                        if(Provider.of<InformationCollateralChangeNotifier>(context, listen: false).collateralTypeModel.id != "003"){
                            for(int i=0;i<_result["data"].length;i++){
                                if(_result["data"][i]['P_MANDATORY'] == "1"){
                                    this._listMandatoryAsuransi.add("Tambahan");
                                }
                                else if(_result["data"][i]['P_MANDATORY'] == "2"){
                                    this._listMandatoryAsuransi.add("Utama");
                                }
                                else if(_result["data"][i]['P_MANDATORY'] == "3"){
                                    this._listMandatoryAsuransi.add("Perluasan");
                                }
                            }
                        }
                    } else if(_preferences.getString("jenis_penawaran") == "002" && _dataCL[0]['pencairan_ke'] == "1"){
                        if(Provider.of<InformationCollateralChangeNotifier>(context, listen: false).collateralTypeModel.id != "003"){
                            for(int i=0;i<_result["data"].length;i++){
                                if(_result["data"][i]['P_MANDATORY'] == "1"){
                                    this._listMandatoryAsuransi.add("Tambahan");
                                }
                                else if(_result["data"][i]['P_MANDATORY'] == "2"){
                                    this._listMandatoryAsuransi.add("Utama");
                                }
                                else if(_result["data"][i]['P_MANDATORY'] == "3"){
                                    this._listMandatoryAsuransi.add("Perluasan");
                                }
                            }
                        }
                    }
                } else {
                    if(Provider.of<InformationCollateralChangeNotifier>(context, listen: false).collateralTypeModel.id != "003"){
                        for(int i=0;i<_result["data"].length;i++){
                            if(_result["data"][i]['P_MANDATORY'] == "1"){
                                this._listMandatoryAsuransi.add("Tambahan");
                            }
                            else if(_result["data"][i]['P_MANDATORY'] == "2"){
                                this._listMandatoryAsuransi.add("Utama");
                            }
                            else if(_result["data"][i]['P_MANDATORY'] == "3"){
                                this._listMandatoryAsuransi.add("Perluasan");
                            }
                        }
                    }
                }
                // if(this._listMandatoryAsuransi.isNotEmpty){
                //     showDialog(
                //         context: context,
                //         barrierDismissible: false,
                //         builder: (BuildContext context){
                //             return Theme(
                //                 data: ThemeData(
                //                     fontFamily: "NunitoSans",
                //                     primaryColor: Colors.black,
                //                     primarySwatch: primaryOrange,
                //                     accentColor: myPrimaryColor
                //                 ),
                //                 child: AlertDialog(
                //                     title: Text("Informasi", style: TextStyle(fontWeight: FontWeight.bold)),
                //                     content: Column(
                //                         crossAxisAlignment: CrossAxisAlignment.start,
                //                         mainAxisSize: MainAxisSize.min,
                //                         children: <Widget>[
                //                             Text("Mandatory untuk asuransi adalah $_listMandatoryAsuransi"),
                //                         ],
                //                     ),
                //                     actions: <Widget>[
                //                         FlatButton(
                //                             onPressed: () {
                //                                 Navigator.of(context).pop(true);
                //                             },
                //                             child: Text('Close'),
                //                         ),
                //                     ],
                //                 ),
                //             );
                //         }
                //     );
                // }
            }
            Provider.of<InfoAdditionalInsuranceChangeNotifier>(context, listen: false).getMandatory(context);
            getDataColla(context);
        } catch(e){
            print(e.toString());
        }
    }

    void getDataColla(BuildContext context){
        _isDurable = Provider.of<InformationCollateralChangeNotifier>(context, listen: false).collateralTypeModel.id == "003";
        // per tanggal 4/6/21 di comment karena tidak dipakai
        // if(Provider.of<InformationCollateralChangeNotifier>(context, listen: false).collateralTypeModel.id != "003"){
        //     _type = Provider.of<InformationCollateralChangeNotifier>(context, listen: false).groupObjectSelected.KODE == "002";
        // } else {
        //     _type = false;
        // }
        // if(this._flag) {
        //     this._isCheckData = false;
        // } else {
        //     this._isCheckData = true;
        // }
    }

    void deleteListInfoMajorInsurance(BuildContext context, int index) {
        showDialog(
            context: context,
            barrierDismissible: true,
            builder: (BuildContext context){
                return Theme(
                    data: ThemeData(
                        fontFamily: "NunitoSans",
                        primaryColor: Colors.black,
                        primarySwatch: primaryOrange,
                        accentColor: myPrimaryColor
                    ),
                    child: AlertDialog(
                        title: Text("Warning", style: TextStyle(fontWeight: FontWeight.bold)),
                        content: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                                Text("Apakah kamu yakin menghapus asuransi utama ini?",),
                            ],
                        ),
                        actions: <Widget>[
                            new FlatButton(
                                onPressed: () {
                                    this._listMajorInsurance.removeAt(index);
                                    notifyListeners();
                                    Navigator.pop(context);
                                },
                                child: new Text('Ya', style: TextStyle(color: Colors.grey),),
                            ),
                            new FlatButton(
                                onPressed: () => Navigator.of(context).pop(true),
                                child: new Text('Tidak'),
                            ),
                        ],
                    ),
                );
            }
        );
    }

    void updateListInfoMajorInsurance(MajorInsuranceModel mInfoMajorInsurance, BuildContext context, int index) {
        this._listMajorInsurance[index] = mInfoMajorInsurance;
        notifyListeners();
    }

    void addListInfoMajorInsurance(MajorInsuranceModel value) {
        _listMajorInsurance.add(value);
        if (this._autoValidate) autoValidate = false;
        notifyListeners();
    }

    void clearMajorInsurance(){
        this._autoValidate = false;
        this._flag = false;
        this._listMajorInsurance.clear();
        this._isCheckData = false;
        disableJenisPenawaran = false;
    }

    bool get flag => _flag;

    set flag(bool value) {
        this._flag = value;
        notifyListeners();
    }

    String get messageError => _messageError;

    set messageError(String value) {
        this._messageError = value;
    }

    Future<bool> onBackPress() async{
        _listMandatoryAsuransiUtama.clear();
        // if(!_isDurable){
        //     _listMandatoryAsuransiUtama.add("Utama");
        // }
        for(int i=0;i<_listMandatoryAsuransi.length;i++){
            if(_listMandatoryAsuransi[i] != "Tambahan"){
                _listMandatoryAsuransiUtama.add(_listMandatoryAsuransi[i]);
            }
        }
        for(int i=0; i<_listMajorInsurance.length; i++){
            for(int j=0; j<_listMandatoryAsuransiUtama.length; j++){
                if(_listMajorInsurance[i].insuranceType.KODE == "2" && _listMandatoryAsuransiUtama[j] == "Utama"){
                    _listMandatoryAsuransiUtama.removeAt(j);
                }
                else if(_listMajorInsurance[i].insuranceType.KODE == "3" && _listMandatoryAsuransiUtama[j] == "Perluasan"){
                    _listMandatoryAsuransiUtama.removeAt(j);
                }
            }
        }
        if(_listMandatoryAsuransiUtama.isNotEmpty){
            flag = true;
            this._isCheckData = false;
            this._messageError = "Asuransi $_listMandatoryAsuransiUtama wajib ditambahkan";
        }
        else{
            flag = false;
            this._isCheckData = true;
            this._messageError = "";
        }
        // if (_listMajorInsurance.length != 0) {
        //     if(_type){
        //         for(int i=0; i<_listMajorInsurance.length; i++){
        //             flag = true;
        //             this._messageError = "Asuransi Perluaan wajib ditambahkan";
        //             if(_listMajorInsurance[i].insuranceType.KODE == "3"){
        //                 flag = false;
        //                 this._messageError = "";
        //             }
        //         }
        //     } else {
        //         flag = false;
        //         this._messageError = "";
        //     }
        // } else {
        //     flag = true;
        //     if(_type){
        //         this._messageError = "Asuransi Utama dan Perluasan wajib ditambahkan";
        //     }
        //     else{
        //         this._messageError = "Asuransi Utama wajib ditambahkan";
        //     }
        // }
        // if(_isDurable){
        //     flag = false;
        //     this._messageError = "";
        // }
        return true;
    }

    Future<void> setPreference() async {
        SharedPreferences _preference = await SharedPreferences.getInstance();
        this._custType = _preference.getString("cust_type");
        this._lastKnownState = _preference.getString("last_known_state");
        this._disableJenisPenawaran = false;
        if(_preference.getString("jenis_penawaran") == "002"){
            this._disableJenisPenawaran = true;
        }
        notifyListeners();
    }

    Future<void> saveToSQLite() async {
        print("save asuransi utama");
        List<MS2ApplInsuranceModel> _listApplInsurance = [];
        SharedPreferences _preferences = await SharedPreferences.getInstance();
        if(_listMajorInsurance.isNotEmpty) {
            for(int i=0; i<_listMajorInsurance.length; i++){
                _listApplInsurance.add(MS2ApplInsuranceModel(
                    "1",
                    this._listMajorInsurance[i].orderProductInsuranceID != null ? this._listMajorInsurance[i].orderProductInsuranceID : "NEW",
                    this._listMajorInsurance[i].colaType,
                    this._listMajorInsurance[i].numberOfColla,
                    this._listMajorInsurance[i].insuranceType != null ? this._listMajorInsurance[i].insuranceType.KODE : "",
                    this._listMajorInsurance[i].company != null ? this._listMajorInsurance[i].company.KODE : "",
                    this._listMajorInsurance[i].product != null ? this._listMajorInsurance[i].product.KODE : "",
                    this._listMajorInsurance[i].product != null ? this._listMajorInsurance[i].product.DESKRIPSI : "",
                    this._listMajorInsurance[i].periodType != "" ? int.parse(this._listMajorInsurance[i].periodType) : 0,
                    this._listMajorInsurance[i].type != null ?  this._listMajorInsurance[i].type : "",
                    this._listMajorInsurance[i].coverage1 != null ? this._listMajorInsurance[i].coverage1.KODE : null,
                    this._listMajorInsurance[i].coverage2 != null ? this._listMajorInsurance[i].coverage2.KODE : null,
                    this._listMajorInsurance[i].subType != null ? this._listMajorInsurance[i].subType.KODE : null,
                    this._listMajorInsurance[i].subType != null ? this._listMajorInsurance[i].subType.DESKRIPSI : null,
                    this._listMajorInsurance[i].coverageType != null ? this._listMajorInsurance[i].coverageType.KODE : "",
                    this._listMajorInsurance[i].coverageType != null ? this._listMajorInsurance[i].coverageType.DESKRIPSI : "",
                    this._listMajorInsurance[i].coverageValue != "" ? double.parse(this._listMajorInsurance[i].coverageValue.replaceAll(",", "")) : 0,
                    this._listMajorInsurance[i].upperLimitRate != "" ? double.parse(this._listMajorInsurance[i].upperLimitRate.replaceAll(",", "")) : 0,
                    this._listMajorInsurance[i].upperLimit != "" ? double.parse(this._listMajorInsurance[i].upperLimit.replaceAll(",", "")) : 0,
                    this._listMajorInsurance[i].lowerLimitRate != "" ? double.parse(this._listMajorInsurance[i].lowerLimitRate.replaceAll(",", "")) : 0,
                    this._listMajorInsurance[i].lowerLimit != "" ? double.parse(this._listMajorInsurance[i].lowerLimit.replaceAll(",", "")) : 0,
                    this._listMajorInsurance[i].priceCash != "" ? double.parse(this._listMajorInsurance[i].priceCash.replaceAll(",", "")) : 0,
                    this._listMajorInsurance[i].priceCredit != "" ? double.parse(this._listMajorInsurance[i].priceCredit.replaceAll(",", "")) : 0,
                    this._listMajorInsurance[i].totalPriceSplit != "" ? double.parse(this._listMajorInsurance[i].totalPriceSplit.replaceAll(",", "")) : 0,
                    this._listMajorInsurance[i].totalPriceRate != "" ? double.parse(this._listMajorInsurance[i].totalPriceRate.replaceAll(",", "")) : 0,
                    this._listMajorInsurance[i].totalPrice != "" ? double.parse(this._listMajorInsurance[i].totalPrice.replaceAll(",", "")) : 0,
                    DateTime.now().toString(),
                    _preferences.getString("username"),
                    null,
                    null,
                    1,
                    i+1,
                    this._listMajorInsurance[i].insuranceType != null ? this._listMajorInsurance[i].insuranceType.DESKRIPSI : "",
                    this._listMajorInsurance[i].insuranceType != null ? this._listMajorInsurance[i].insuranceType.PARENT_KODE : "",
                    this._listMajorInsurance[i].company != null ? this._listMajorInsurance[i].company.DESKRIPSI : "",
                    this._listMajorInsurance[i].company != null ? this._listMajorInsurance[i].company.PARENT_KODE.toString() : "",
                    this._listMajorInsurance[i].coverage1 != null ? this._listMajorInsurance[i].coverage1.PARENT_KODE : null,
                    this._listMajorInsurance[i].coverage1 != null ? this._listMajorInsurance[i].coverage1.DESKRIPSI : null,
                    this._listMajorInsurance[i].coverage2 != null ? this._listMajorInsurance[i].coverage2.PARENT_KODE : null,
                    this._listMajorInsurance[i].coverage2 != null ? this._listMajorInsurance[i].coverage2.DESKRIPSI : null,
                    this._listMajorInsurance[i].insuranceTypeDakor ? "1" : "0",
                    this._listMajorInsurance[i].installmentNoDakor ? "1" : "0",
                    this._listMajorInsurance[i].companyDakor ? "1" : "0",
                    this._listMajorInsurance[i].productDakor ? "1" : "0",
                    this._listMajorInsurance[i].periodDakor ? "1" : "0",
                    this._listMajorInsurance[i].typeDakor ? "1" : "0",
                    this._listMajorInsurance[i].coverage1Dakor ? "1" : "0",
                    this._listMajorInsurance[i].coverage2Dakor ? "1" : "0",
                    this._listMajorInsurance[i].subTypeDakor ? "1" : "0",
                    this._listMajorInsurance[i].coverageTypeDakor ? "1" : "0",
                    this._listMajorInsurance[i].coverageValueDakor ? "1" : "0",
                    this._listMajorInsurance[i].upperLimitRateDakor ? "1" : "0",
                    this._listMajorInsurance[i].upperLimitValueDakor ? "1" : "0",
                    this._listMajorInsurance[i].lowerLimitRateDakor ? "1" : "0",
                    this._listMajorInsurance[i].lowerLimitValueDakor ? "1" : "0",
                    this._listMajorInsurance[i].cashDakor ? "1" : "0",
                    this._listMajorInsurance[i].creditDakor ? "1" : "0",
                    this._listMajorInsurance[i].totalPriceSplitDakor ? "1" : "0",
                    this._listMajorInsurance[i].totalPriceRateDakor ? "1" : "0",
                    this._listMajorInsurance[i].totalPriceDakor ? "1" : "0",
                ));
            }
        }
        _dbHelper.insertMS2ApplInsuranceMain(_listApplInsurance);
    }

    Future<bool> deleteSQLite() async{
        return (await _dbHelper.deleteMS2ApplInsuranceMain() && await _dbHelper.deleteMS2ApplInsurancePeruluasan());
    }

    Future<void> setDataFromSQLite(BuildContext context) async{
        var _check = await _dbHelper.selectMS2ApplInsuranceMain();
        print("data major insurance = $_check");
        if(_check.isNotEmpty){
            for(int i=0; i<_check.length; i++){
                InsuranceTypeModel insurance = InsuranceTypeModel(_check[i]['insuran_type_parent'].toString(), _check[i]['insuran_type'].toString(), _check[i]['insuran_type_desc']);
                CompanyTypeInsuranceModel company = CompanyTypeInsuranceModel(
                    _check[i]['company_parent'] == "" || _check[i]['company_parent'] == "null" ? null : int.parse(_check[i]['company_parent']),
                    _check[i]['company_id'],
                    _check[i]['company_desc']
                );
                ProductModel product = ProductModel(_check[i]['product'], _check[i]['product_desc']);
                CoverageModel coverage1 = CoverageModel(_check[i]['type_1_parent'].toString(), _check[i]['type_1'], _check[i]['type_1_desc']);
                CoverageModel coverage2 = _check[i]['type_2_parent'] != "null" && _check[i]['type_2_parent'] != "" ? CoverageModel(_check[i]['type_2_parent'].toString(), _check[i]['type_2'], _check[i]['type_2_desc']) : CoverageModel("", "", "");
                SubTypeModel subType = _check[i]['sub_type'] != "null" && _check[i]['sub_type'] != "" ? SubTypeModel(_check[i]['sub_type'], _check[i]['sub_type_coverage']) : SubTypeModel("", "");
                CoverageTypeModel coverageType = CoverageTypeModel(_check[i]['coverage_type'], _check[i]['coverage_type_desc']);
                _listMajorInsurance.add(MajorInsuranceModel(
                    insurance,
                    _check[i]['cola_type'].toString(),
                    _check[i]['insuran_index'].toString(),
                    company,
                    product,
                    _check[i]['period'].toString(),
                    _check[i]['type'],
                    coverage1,
                    coverage2,
                    subType,
                    coverageType,
                    _check[i]['coverage_amt'].toString(),
                    _check[i]['upper_limit_amt'].toString(),
                    _check[i]['lower_limit_amt'].toString(),
                    _check[i]['upper_limit_pct'].toString(),
                    _check[i]['lower_limit_pct'].toString(),
                    _check[i]['cash_amt'].toString(),
                    _check[i]['credit_amt'].toString(),
                    _check[i]['total_split'].toString(),
                    _check[i]['total_pct'].toString(),
                    _check[i]['total_amt'].toString(),
                    _check[i]['edit_insuran_type_parent'] == "1" || _check[i]['edit_insuran_index'] == "1" || _check[i]['edit_company_id'] == "1" ||
                    _check[i]['edit_product'] == "1" || _check[i]['edit_period'] == "1" || _check[i]['edit_type'] == "1" || _check[i]['edit_type_1'] == "1" ||
                    _check[i]['edit_type_2'] == "1" ||  _check[i]['edit_sub_type'] == "1" ||_check[i]['edit_coverage_type'] == "1" ||
                    _check[i]['edit_coverage_amt'] == "1" || _check[i]['edit_upper_limit_pct'] == "1" || _check[i]['edit_upper_limit_amt'] == "1" ||
                    _check[i]['edit_lower_limit_pct'] == "1" || _check[i]['edit_lower_limit_amt'] == "1" || _check[i]['edit_cash_amt'] == "1" ||
                    _check[i]['edit_credit_amt'] == "1" || _check[i]['edit_total_split'] == "1" || _check[i]['edit_total_pct'] == "1" || _check[i]['edit_total_amt'] == "1",
                    _check[i]['edit_insuran_type_parent'] == "1",
                    _check[i]['edit_insuran_index'] == "1",
                    _check[i]['edit_company_id'] == "1",
                    _check[i]['edit_product'] == "1",
                    _check[i]['edit_period'] == "1",
                    _check[i]['edit_type'] == "1",
                    _check[i]['edit_type_1'] == "1",
                    _check[i]['edit_type_2'] == "1",
                    _check[i]['edit_sub_type'] == "1",
                    _check[i]['edit_coverage_type'] == "1",
                    _check[i]['edit_coverage_amt'] == "1",
                    _check[i]['edit_upper_limit_pct'] == "1",
                    _check[i]['edit_upper_limit_amt'] == "1",
                    _check[i]['edit_lower_limit_pct'] == "1",
                    _check[i]['edit_lower_limit_amt'] == "1",
                    _check[i]['edit_cash_amt'] == "1",
                    _check[i]['edit_credit_amt'] == "1",
                    _check[i]['edit_total_split'] == "1",
                    _check[i]['edit_total_pct'] == "1",
                    _check[i]['edit_total_amt'] == "1",
                    _check[i]['orderProductInsuranceID']
                ));
            }
        }
        notifyListeners();
    }
}