import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/source_order_name_model.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/io_client.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SearchSourceOrderNameDSAChangeNotifier with ChangeNotifier {
  bool _showClear = false;
  bool _loadData = false;
  TextEditingController _controllerSearch = TextEditingController();
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  List<SourceOrderNameModel> _listSourceOrderNameDSA = [];
  List<SourceOrderNameModel> _listSourceOrderNameDSATemp = [];

  TextEditingController get controllerSearch => _controllerSearch;

  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  bool get showClear => _showClear;

  set showClear(bool value) {
    this._showClear = value;
    notifyListeners();
  }

  void  changeAction(String value) {
    if (value != "") {
      showClear = true;
    } else {
      showClear = false;
    }
  }

  UnmodifiableListView<SourceOrderNameModel> get listSourceOrderNameDSA {
    return UnmodifiableListView(this._listSourceOrderNameDSA);
  }

  UnmodifiableListView<SourceOrderNameModel> get listSourceOrderNameDSATemp {
    return UnmodifiableListView(this._listSourceOrderNameDSATemp);
  }

  bool get loadData => _loadData;

  set loadData(bool value) {
    this._loadData = value;
  }

  void getSourceOrderNameDSA() async{
    this._listSourceOrderNameDSA.clear();
    this._listSourceOrderNameDSATemp.clear();
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    loadData = true;
    final ioc = new HttpClient();
    ioc.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;
    final _http = IOClient(ioc);
    var storage = FlutterSecureStorage();

    String _namaSumberOrderDSA = await storage.read(key: "NamaSumberOrderDSA");
    var _body = jsonEncode({
      "SZBRANCH": _preferences.getString("branchid"),
      "SZSO": "DSA"
    });
    final _response = await _http.post(
        "${BaseUrl.urlGeneral}$_namaSumberOrderDSA",
        body: _body,
        headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
    );
    if(_response.statusCode == 200){
      final _result = jsonDecode(_response.body);
      List _data = _result['RC1'];
      if(_data.isEmpty){
        showSnackBar("${_result['strmessage']}");
        loadData = false;
      }
      else{
        for(int i=0; i <_data.length; i++){
          this._listSourceOrderNameDSA.add(
            SourceOrderNameModel(
              _data[i]['ERP_BP_PARTNEREXTERNAL'],
              _data[i]['ERP_BP_NAME']
            )
          );
        }
        loadData = false;
      }
    }
    else{
      showSnackBar("Error response status ${_response.statusCode}");
      this._loadData = false;
    }
    notifyListeners();
  }

  void searchSourceOrderNameDSA(String query) async {
    if(query.length < 3) {
      showSnackBar("Input minimal 3 karakter");
    }
    else {
      _listSourceOrderNameDSATemp.clear();
      if (query.isEmpty) {
        return;
      }

      _listSourceOrderNameDSA.forEach((dataSourceOrderNameDSA) {
        if (dataSourceOrderNameDSA.kode.contains(query) || dataSourceOrderNameDSA.deskripsi.contains(query)) {
          this._listSourceOrderNameDSATemp.add(dataSourceOrderNameDSA);
        }
      });
      if(this._listSourceOrderNameDSATemp.isEmpty) {
        this._listSourceOrderNameDSA.clear();
      }
      notifyListeners();
    }
  }

  void clearSearchTemp() {
    _listSourceOrderNameDSATemp.clear();
    getSourceOrderNameDSA();
  }

  void showSnackBar(String text){
    this._scaffoldKey.currentState. showSnackBar(new SnackBar(
        content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor));
  }
}
