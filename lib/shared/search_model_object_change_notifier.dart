import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/brand_object_model.dart';
import 'package:ad1ms2_dev/models/brand_type_model_genre_model.dart';
import 'package:ad1ms2_dev/models/model_object_model.dart';
import 'package:ad1ms2_dev/models/object_type_model.dart';
import 'package:ad1ms2_dev/models/object_usage_model.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_object_unit_change_notifier.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/form_m_foto_change_notif.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/io_client.dart';
import 'package:provider/provider.dart';

class SearchModelObjectChangeNotifier with ChangeNotifier {
  bool _showClear = false;
  TextEditingController _controllerSearch = TextEditingController();
  List<ModelObjectModel> _listModelObject = [];
  List<BrandTypeModelGenreModel> _listBrandTypeModelGenreModel = [];
  bool _loadData = false;
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  TextEditingController get controllerSearch => _controllerSearch;

  void setDataModelObject(String id) {
    // if (id == "001") {
    //   _listModelObject.add(ModelObjectModel(
    //       "212", "Vario 125", "Scootermatic", "Honda", "Motor"));
    //   _listModelObject.add(ModelObjectModel(
    //       "213", "Vario 150", "Scootermatic", "Honda", "Motor"));
    //   _listModelObject.add(
    //       ModelObjectModel("214", "N-Max", "Scootermatic", "Yamaha", "Motor"));
    // } else if (id == "002") {
    //   _listModelObject
    //       .add(ModelObjectModel("311", "MUX", "SUV", "ISUZU", "Mobil"));
    //   _listModelObject.add(
    //       ModelObjectModel("312", "Innova Reborn", "MPV", "TOYOTA", "Mobil"));
    // }
  }

  bool get showClear => _showClear;

  set showClear(bool value) {
    this._showClear = value;
    notifyListeners();
  }

  void changeAction(String value) {
    if (value != "") {
      showClear = true;
    } else {
      showClear = false;
    }
  }

  UnmodifiableListView<ModelObjectModel> get listModelObject {
    return UnmodifiableListView(this._listModelObject);
  }

  void _getModelObject(BuildContext context, int flagByBrandModelType, String flag, String query, String kodeGroupObject, String kodeObject, String prodMatrix) async{
    var _providerInfoObject = Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false);
    var _providerFoto = Provider.of<FormMFotoChangeNotifier>(context,listen: false);
    this._listBrandTypeModelGenreModel.clear();
    notifyListeners();
    loadData = true;
    final ioc = new HttpClient();
    ioc.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;

    final _http = IOClient(ioc);
    var _body = jsonEncode({
      "P_FLAG" : "$flagByBrandModelType",//1. brand 2. type 3. model
      "P_OBJECT_GROUP_ID" : kodeGroupObject, //group objek "${_providerInfoObject.groupObjectSelected.KODE}"
      "P_OBJECT_ID": kodeObject, // "${_providerInfoObject.objectSelected.id}"
      "P_OJK_BUSS_DETAIL_ID" : flag == "COM" ? "${_providerInfoObject.businessActivitiesTypeModelSelected.id}" : "${_providerFoto.jenisKegiatanUsahaSelected.id}",
      "P_OJK_BUSS_ID" : flag == "COM" ? "${_providerInfoObject.businessActivitiesModelSelected.id}" : "${_providerFoto.kegiatanUsahaSelected.id}",
      "P_PROD_MATRIX_ID" : prodMatrix, // prod matrix id "${_providerInfoObject.prodMatrixId}"
      "P_SEARCH" : "$query" // search query
    });

    print(_body);

    var storage = FlutterSecureStorage();
    String _fieldModelObjek = await storage.read(key: "FieldModelObjek");
    final _response = await _http.post(
        "${BaseUrl.urlGeneral}$_fieldModelObjek",
        // "${urlPublic}api/parameter/get-brand-type-model-genre",
        body: _body,
        headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
    );
    if(_response.statusCode == 200){
      final _result = jsonDecode(_response.body);
      final _data = _result['data'];
      if(_data.isEmpty){
        showSnackBar("Model Objek tidak ditemukan");
        loadData = false;
      }
      else{
        for(int i=0; i <_data.length; i++){
          this._listBrandTypeModelGenreModel.add(
              BrandTypeModelGenreModel(
                  BrandObjectModel(_data[i]['BRAND_ID'],_data[i]['BRAND_NAME']),
                  ObjectTypeModel(_data[i]['TYPE_ID'],_data[i]['TYPE_NAME']),
                  ModelObjectModel(_data[i]['MODEL_ID'],_data[i]['MODEL_NAME']),
                  ObjectUsageModel(_data[i]['GENRE_ID'],_data[i]['GENRE_NAME'])
              )
          );
        }
        loadData = false;
      }
    }
    else{
      showSnackBar("Error response status ${_response.statusCode}");
      this._loadData = false;
    }
    notifyListeners();
  }

  void searchObjectType(BuildContext context, int flagByBrandModelType, String flag, String query, String kodeGroupObject, String kodeObject, String prodMatrix) {
    if(query.length < 3) {
      showSnackBar("Input minimal 3 karakter");
    } else {
      _getModelObject(context, flagByBrandModelType, flag, query, kodeGroupObject, kodeObject, prodMatrix);
    }
    // notifyListeners();
  }

  void showSnackBar(String text){
    this._scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor));
  }

  bool get loadData => _loadData;

  set loadData(bool value) {
    this._loadData = value;
  }

  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  UnmodifiableListView<BrandTypeModelGenreModel> get listBrandTypeModelGenreModel {
    return UnmodifiableListView(this._listBrandTypeModelGenreModel);
  }
}
